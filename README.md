<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)
- [Appoly](https://www.appoly.co.uk)
- [OP.GG](https://op.gg)
- [云软科技](http://www.yunruan.ltd/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct  

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


業務管理 business_management
    姓名 name (string)
    地址 address (string)
    電話 phone (string)
    身分證字號 id_number (string)
    出生年月日 birthday (date)
    銀行帳戶 bank_account (string)
    管理費設定 management_fee_set (string)
        代管定額 escrow_quota (integer)
        包廂定額 box_quota (integer)

----------------------------------------------------
合約 contract
    期數 contract_number_of_periods (string)
    類別 contract_category (string)
    業務簽回 contract_business_sign_back (string)
    媒合日期 contract_match_date (string)

房東 landlord
    房東_媒合編號 landlord_match_number (string)
    房東_現有房客 landlord_existing_tenant (string)
    房東_契約起日 landlord_contract_date (string)
    房東_契約訖日 landlord_contract_expiry_date (string)
    房東_物件編號 landlord_item_number (string)
    房東_內政部編號 landlord_ministry_of_the_interior_number (string)
    房東_類型 landlord_types_of (string)

房客 tenant
    房客_案件編號 tenant_case_number (string)
    房客_內政部編號 tenant_ministry_of_the_interior_number (string)

租評表租金 rent_evaluation_form
    租評表_市價租金 rent_evaluation_form_market_rent (string)
    租評表_評定租金 rent_evaluation_form_assess_rent (string)

實際簽約租押金 actual_contract_rental_deposit
    實際簽約_給房東租金 actual_contract_rent_to_landlord (string)
    實際簽約_房客自付租金 actual_contract_tenant_pays_rent (string)
    實際簽約_租金補助 actual_contract_rent_subsidy (string)
    實際簽約_押金 actual_contract_deposit (string)

給房東 to_the_landlord
    租金日 to_the_landlord_rent_day (string)

包/代管費 package_escrow_fee
    包_代管費月分 package_escrow_fee_month_minute (string)
    包_代管費金額 package_escrow_fee_amount (string)
    包_代管費期數 package_escrow_fee_number_of_periods (string)

租金補助 rent_subsidy
    租金補助_月分 rent_subsidy_month_minute (string)
    租金補助_金額 rent_subsidy_amount (string)
    租金補助_期數 rent_subsidy_number_of_periods (string)

開發/媒合費 development_matching_fees
    開發_媒合費_月份 development_matching_fees_month (string)
    開發_媒合費_金額 development_matching_fees_amount (string)

公證費 notary_fees
    公證日 notary_fees_notarization_date (string)
    公證_月份 notary_fees_month (string)
    公證_金額 notary_fees_amount (string)

修繕費 repair_cost
    修繕費_月分 repair_cost_month_minute (string)
    修繕費_金額 repair_cost_amount (string)

保險費 insurance
    保險費_月分 insurance_month_minute (string)
    保險費_金額 insurance_amount (string)

保險期間（最多三年） insurance_period_up_to_three_years
    保險期間_最多三年_年度 insurance_period_three_year (string)
    保險期間_最多三年_起訖日 insurance_period_three_start_and_end_date (string)

備註1 remark_1 (string)
備註2 remark_2 (string)

--------------------------------------------------
欄位說明


    0-----
    [0] => "期數" contract_number_of_periods
    [1] => "類別" contract_category
    [2] => "業務簽回" contract_business_sign_back
    [3] => "媒合日期" contract_match_date
    [37] => 備註1 remark_1
    [38] => 備註2 remark_2

    1------------------
    [4] => 房東_媒合編號 landlord_match_number
    [5] => 房東_現有房客 landlord_existing_tenant
    [6] => 房東_契約起日 landlord_contract_date
    [7] => 房東_契約訖日 landlord_contract_expiry_date
    [8] => 房東_物件編號 landlord_item_number
    [9] => 房東_內政部編號 landlord_ministry_of_the_interior_number
    [10] => 房東_類型 landlord_types_of
    [11] => 房客_案件編號 tenant_case_number
    [12] => 房客_內政部編號 tenant_ministry_of_the_interior_number
    [13] => 租評表_市價租金 rent_evaluation_form_market_rent
    [14] => 租評表_評定租金 rent_evaluation_form_assess_rent
    [15] => 實際簽約_給房東租金 actual_contract_rent_to_landlord
    [16] => 實際簽約_房客自付租金 actual_contract_tenant_pays_rent
    [17] => 實際簽約_租金補助 actual_contract_rent_subsidy
    [18] => 實際簽約_押金 actual_contract_deposit
    [19] => 租金日 to_the_landlord_rent_day
    [20] => 包_代管費月分 package_escrow_fee_month_minute
    [21] => 包_代管費金額 package_escrow_fee_amount
    [22] => 包_代管費期數 package_escrow_fee_number_of_periods
    [23] => 租金補助_月分 rent_subsidy_month_minute
    [24] => 租金補助_金額 rent_subsidy_amount
    [25] => 租金補助_期數 rent_subsidy_number_of_periods
    [26] => 開發_媒合費_月份 development_matching_fees_month
    [27] => 開發_媒合費_金額 development_matching_fees_amount
    [28] => 公證日 notary_fees_notarization_date
    [29] => 公證_月份 notary_fees_month
    [30] => 公證_金額 notary_fees_amount
    [31] => 修繕費_月分 repair_cost_month_minute
    [32] => 修繕費_金額 repair_cost_amount
    [33] => 保險費_月分 insurance_month_minute
    [34] => 保險費_金額 insurance_amount
    [35] => 保險期間_最多三年_年度 insurance_period_three_year
    [36] => 保險期間_最多三年_起訖日 insurance_period_three_start_and_end_date

