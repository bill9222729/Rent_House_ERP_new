<?php
//
//namespace App\Console\Commands;
//
//use App\Models\contract;
//use Google_Client;
//use Google_Service_Drive;
//use Google_Service_Drive_DriveFile;
//use Illuminate\Console\Command;
//
//class UploadToDriver extends Command
//{
//    /**
//     * The name and signature of the console command.
//     *
//     * @var string
//     */
//    protected $signature = 'upload:google-driver';
//
//    /**
//     * The console command description.
//     *
//     * @var string
//     */
//    protected $description = 'Command description';
//    /**
//     * @var Google_Client
//     */
//    private $gClient;
//    /**
//     * @var mixed
//     */
//    private $accessToken;
//
//    /**
//     * Create a new command instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        parent::__construct();
//        $this->accessToken = env('GOOGLE_ACCESS_TOKEN');
//        $google_redirect_url = route('glogin');
//        $this->gClient = new Google_Client();
//        $this->gClient->setApplicationName('test');
//        $this->gClient->setClientId(env('GOOGLE_CLIENT_ID'));
//        $this->gClient->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
//        $this->gClient->setRedirectUri($google_redirect_url);
//        $this->gClient->setDeveloperKey(env('GOOGLE_API_KEY'));
//        $this->gClient->setScopes(array(
//            'https://www.googleapis.com/auth/drive.file',
//            'https://www.googleapis.com/auth/drive'
//        ));
//        $this->gClient->setAccessType("offline");
//        $this->gClient->setApprovalPrompt("force");
//    }
//
//    /**
//     * Execute the console command.
//     *
//     * @return int
//     */
//    public function handle()
//    {
//        $contracts = contract::where('file_upload', '!=', null)->get();
//        $service = new Google_Service_Drive($this->gClient);
//        $this->gClient->setAccessToken($this->accessToken);
//        if ($this->gClient->isAccessTokenExpired()) {
//
//            // save refresh token to some variable
//            $refreshTokenSaved = $this->gClient->getRefreshToken();
//            // update access token
//            $this->gClient->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
//            // // pass access token to some variable
//            $updatedAccessToken = $this->gClient->getAccessToken();
//            // // append refresh token
//            $updatedAccessToken['refresh_token'] = $refreshTokenSaved;
//            //Set the new acces token
//            $this->gClient->setAccessToken($updatedAccessToken);
//
//            $$this->accessToken=$updatedAccessToken;
//        }
//        $counter = 0;
//        foreach ($contracts as $contract)
//        {
//            $files = explode(',', $contract->file_upload);
//            foreach ($files as $contractFile)
//            {
//                if (!$files) {
//                    continue;
//                }
//                $counter++;
//                $fileMetadata = new Google_Service_Drive_DriveFile(array(
//                    'name' => $contracts->landlord_match_number,
//                    'mimeType' => 'application/vnd.google-apps.folder'));
//                $folder = $service->files->create($fileMetadata, array(
//                    'fields' => 'id'));
//                printf("Folder ID: %s\n", $folder->id);
//                $data = file_get_contents(storage_path($contractFile));
//                $file = new Google_Service_Drive_DriveFile(array(
//                    'name' => "file$counter",
//                    'parents' => array($folder->id)
//                ));
//                $service->files->create($file, array(
//                    'data' => $data,
//                    'mimeType' => 'application/octet-stream',
//                    'uploadType' => 'media'
//                ));
//            }
//
//        }
//
//
//    }
//}
