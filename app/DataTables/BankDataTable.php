<?php

namespace App\DataTables;

use App\Models\Bank;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class BankDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'banks.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Bank $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Bank $model)
    {
        $query = $model->newQuery();

        if (request()->input('payday')) {
            $query->whereHas('contract', function ($q) {
                $q->where('contracts.to_the_landlord_rent_day', request()->input('payday'));
            });
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'bank_code' => new Column(['title' => __('models/banks.fields.bank_code'), 'data' => 'bank_code']),
            'bank_account' => new Column(['title' => __('models/banks.fields.bank_account'), 'data' => 'bank_account']),
            'tax' => new Column(['title' => __('models/banks.fields.tax'), 'data' => 'tax']),
            'price' => new Column(['title' => __('models/banks.fields.price'), 'data' => 'price']),
            'user_number' => new Column(['title' => __('models/banks.fields.user_number'), 'data' => 'user_number']),
            'company_stack_code' => new Column(['title' => __('models/banks.fields.company_stack_code'), 'data' => 'company_stack_code']),
            'Creator' => new Column(['title' => __('models/banks.fields.Creator'), 'data' => 'Creator']),
            'search' => new Column(['title' => __('models/banks.fields.search'), 'data' => 'search']),
            'bank_noted' => new Column(['title' => __('models/banks.fields.bank_noted'), 'data' => 'bank_noted'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'banks_datatable_' . time();
    }
}
