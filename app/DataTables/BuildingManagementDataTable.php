<?php

namespace App\DataTables;

use App\Models\BuildingManagement;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\Auth;
use App\Models\contract;

class BuildingManagementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query->with(['contract']));

        return $dataTable->addColumn('action', 'building_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\BuildingManagement $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(BuildingManagement $model)
    {

        $user_data = Auth::user();
        if ($user_data->role == 'admin') {
            return $model->newQuery();
        }

        if ($user_data->role != 'admin') {

            $user_name = $user_data->name;
            $all_contract = contract::where('contract_business_sign_back', $user_name)->get();

            $all_contract_num = [];

            foreach ($all_contract as $key => $value) {
                $all_contract_num[] = $value->landlord_match_number;
            }
            // dd($all_contract_num);
            // $test = $model->newQuery()->whereIn('num',$all_contract_num)->get()->toArray();
            // dd($test);
            return $model->newQuery()
                ->whereIn('num', $all_contract_num)
                ->orWhere('status', '黑名單')
                ->orWhere('status', '警示客戶')
                ->orWhere('status', '退出計畫');
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => 'frtip',
                'stateSave' => true,
                'order' => [[4, 'desc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $lessorNameRender = '`<a href="/buildingManagements/${full.id}/">${data}</a>`';
        $reTypeRender = '`${full.sublet == 1 ? "包租" : ""}${full.rent == 1 ? "代管" : ""}</a>`';
        $addressRender = '`${data}${full.address_cityarea}${full.address_street}${full.address_ln ? full.address_ln : ""}${full.address_aly? full.address_aly : ""}${full.address_num? full.address_num + "號" : ""}${full.address_num_hyphen? full.address_num_hyphen + "樓": ""} ${full.h_info_suite? full.h_info_suite + "室/房" : ""}`';

        return [
            'status' => new Column(['title' => '物件狀態', 'data' => 'status']),
            'Lessor_name' => new Column(['title' => '房東名稱', 'data' => 'Lessor_name', 'render' => $lessorNameRender]),
            'sales' => new Column(['title' => '業務', 'data' => 'sales']),
            'sublet' => new Column(['title' => '包租/代管', 'data' => 'sublet', 'render' => $reTypeRender]),
            'apply_date' => new Column(['title' => '申請日期', 'data' => 'apply_date']),
            // 'request_form_type' => new Column(['title' => __('models/buildingManagements.fields.request_form_type'), 'data' => 'request_form_type']),
            'num' => new Column(['title' => '物件編號', 'data' => 'num']),
            'p_code' => new Column(['title' => '內政部編號', 'data' => 'p_code']),
            'address_city' => new Column(['title' => '地址', 'data' => 'address_city', 'render' => $addressRender]),
            // 'Lessor_ID_num' => new Column(['title' => '身分證號', 'data' => 'Lessor_ID_num'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'building_managements_datatable_' . time();
    }
}
