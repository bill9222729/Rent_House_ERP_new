<?php

namespace App\DataTables;

use App\Models\FixHistory;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class FixHistoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'fix_histories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\FixHistory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(FixHistory $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'sales' => new Column(['title' => __('models/fixHistories.fields.sales'), 'data' => 'sales']),
            'pay_date' => new Column(['title' => __('models/fixHistories.fields.pay_date'), 'data' => 'pay_date']),
            'item_code' => new Column(['title' => __('models/fixHistories.fields.item_code'), 'data' => 'item_code']),
            'item_address' => new Column(['title' => __('models/fixHistories.fields.item_address'), 'data' => 'item_address']),
            'pay_year' => new Column(['title' => __('models/fixHistories.fields.pay_year'), 'data' => 'pay_year']),
            'call_fix_date' => new Column(['title' => __('models/fixHistories.fields.call_fix_date'), 'data' => 'call_fix_date']),
            'item_owner' => new Column(['title' => __('models/fixHistories.fields.item_owner'), 'data' => 'item_owner']),
            'owner_phone' => new Column(['title' => __('models/fixHistories.fields.owner_phone'), 'data' => 'owner_phone']),
            'client_name' => new Column(['title' => __('models/fixHistories.fields.client_name'), 'data' => 'client_name']),
            'client_phone' => new Column(['title' => __('models/fixHistories.fields.client_phone'), 'data' => 'client_phone']),
            'fix_item' => new Column(['title' => __('models/fixHistories.fields.fix_item'), 'data' => 'fix_item']),
            'fix_source' => new Column(['title' => __('models/fixHistories.fields.fix_source'), 'data' => 'fix_source']),
            'check_date' => new Column(['title' => __('models/fixHistories.fields.check_date'), 'data' => 'check_date']),
            'fix_date' => new Column(['title' => __('models/fixHistories.fields.fix_date'), 'data' => 'fix_date']),
            'fix_done' => new Column(['title' => __('models/fixHistories.fields.fix_done'), 'data' => 'fix_done']),
            'fix_record' => new Column(['title' => __('models/fixHistories.fields.fix_record'), 'data' => 'fix_record']),
            'fix_price' => new Column(['title' => __('models/fixHistories.fields.fix_price'), 'data' => 'fix_price']),
            'receipt' => new Column(['title' => __('models/fixHistories.fields.receipt'), 'data' => 'receipt']),
            'final_price' => new Column(['title' => __('models/fixHistories.fields.final_price'), 'data' => 'final_price']),
            'balance' => new Column(['title' => __('models/fixHistories.fields.balance'), 'data' => 'balance']),
            'owner_start_date' => new Column(['title' => __('models/fixHistories.fields.owner_start_date'), 'data' => 'owner_start_date']),
            'owner_end_date' => new Column(['title' => __('models/fixHistories.fields.owner_end_date'), 'data' => 'owner_end_date']),
            'change_start_date' => new Column(['title' => __('models/fixHistories.fields.change_start_date'), 'data' => 'change_start_date']),
            'change_end_date' => new Column(['title' => __('models/fixHistories.fields.change_end_date'), 'data' => 'change_end_date'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'fix_histories_datatable_' . time();
    }
}
