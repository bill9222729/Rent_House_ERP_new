<?php

namespace App\DataTables;

use App\Models\History;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class HistoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'histories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\History $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(History $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'language' => [
                    'url' => 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Chinese-traditional.json'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'work_time' => new Column(['title' => __('models/history.fields.work_time'), 'data' => 'work_time']),
            'title' => new Column(['title' => __('models/history.fields.title'), 'data' => 'title']),

            'fill_user_id',
            'guest_agree',
            'agree_time'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'histories_datatable_' . time();
    }
}
