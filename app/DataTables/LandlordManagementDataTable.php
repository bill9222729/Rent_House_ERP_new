<?php

namespace App\DataTables;

use App\Models\LandlordManagement;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class LandlordManagementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'landlord_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\LandlordManagement $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(LandlordManagement $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'name' => new Column(['title' => __('models/landlordManagements.fields.name'), 'data' => 'name']),
            // 'id_num' => new Column(['title' => __('models/landlordManagements.fields.id_num'), 'data' => 'id_num']),
            // 'address' => new Column(['title' => __('models/landlordManagements.fields.address'), 'data' => 'address']),
            // 'phone' => new Column(['title' => __('models/landlordManagements.fields.phone'), 'data' => 'phone']),
            // 'birthday' => new Column(['title' => __('models/landlordManagements.fields.birthday'), 'data' => 'birthday']),
            'landlord_match_number' => new Column(['title' => __('models/landlordManagements.fields.landlord_match_number'), 'data' => 'landlord_match_number']),
            'landlord_existing_tenant' => new Column(['title' => __('models/landlordManagements.fields.landlord_existing_tenant'), 'data' => 'landlord_existing_tenant']),
            'landlord_contract_date' => new Column(['title' => __('models/landlordManagements.fields.landlord_contract_date'), 'data' => 'landlord_contract_date']),
            'landlord_contract_expiry_date' => new Column(['title' => __('models/landlordManagements.fields.landlord_contract_expiry_date'), 'data' => 'landlord_contract_expiry_date']),
            'landlord_item_number' => new Column(['title' => __('models/landlordManagements.fields.landlord_item_number'), 'data' => 'landlord_item_number']),
            'landlord_ministry_of_the_interior_number' => new Column(['title' => __('models/landlordManagements.fields.landlord_ministry_of_the_interior_number'), 'data' => 'landlord_ministry_of_the_interior_number']),
            'landlord_types_of' => new Column(['title' => __('models/landlordManagements.fields.landlord_types_of'), 'data' => 'landlord_types_of']),
            'bank_acc' => new Column(['title' => __('models/landlordManagements.fields.bank_acc'), 'data' => 'bank_acc'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'landlord_managements_datatable_' . time();
    }
}
