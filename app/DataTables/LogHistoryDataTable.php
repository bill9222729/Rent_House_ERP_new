<?php

namespace App\DataTables;

use App\Models\LogHistory;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class LogHistoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'log_histories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\LogHistory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(LogHistory $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'user' => new Column(['title' => __('models/logHistories.fields.user'), 'data' => 'user']),
            'created_at' => new Column(['title' => "時間", 'data' => 'created_at']),
            'url' => new Column(['title' => __('models/logHistories.fields.url'), 'data' => 'url']),
            // 'request_body' => new Column(['title' => __('models/logHistories.fields.request_body'), 'data' => 'request_body']),
            // 'user_id' => new Column(['title' => __('models/logHistories.fields.user_id'), 'data' => 'user_id']),
            'method' => new Column(['title' => __('models/logHistories.fields.method'), 'data' => 'method'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'log_histories_datatable_' . time();
    }
}
