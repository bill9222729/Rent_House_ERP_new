<?php

namespace App\DataTables;

use App\Models\ReturnFormat;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ReturnFormatDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'return_formats.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReturnFormat $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReturnFormat $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            't_id' => new Column(['title' => __('models/returnFormats.fields.t_id'), 'data' => 't_id']),
            't_code' => new Column(['title' => __('models/returnFormats.fields.t_code'), 'data' => 't_code']),
            'tax' => new Column(['title' => __('models/returnFormats.fields.tax'), 'data' => 'tax']),
            'bank_account' => new Column(['title' => __('models/returnFormats.fields.bank_account'), 'data' => 'bank_account']),
            'user_name' => new Column(['title' => __('models/returnFormats.fields.user_name'), 'data' => 'user_name']),
            't_price' => new Column(['title' => __('models/returnFormats.fields.t_price'), 'data' => 't_price']),
            't_status' => new Column(['title' => __('models/returnFormats.fields.t_status'), 'data' => 't_status']),
            'name' => new Column(['title' => __('models/returnFormats.fields.name'), 'data' => 'name']),
            'item_address' => new Column(['title' => __('models/returnFormats.fields.item_address'), 'data' => 'item_address']),
            'item_month_price' => new Column(['title' => __('models/returnFormats.fields.item_month_price'), 'data' => 'item_month_price']),
            'item_price_support' => new Column(['title' => __('models/returnFormats.fields.item_price_support'), 'data' => 'item_price_support']),
            'watch_price' => new Column(['title' => __('models/returnFormats.fields.watch_price'), 'data' => 'watch_price']),
            'parking_price' => new Column(['title' => __('models/returnFormats.fields.parking_price'), 'data' => 'parking_price']),
            'fix_price' => new Column(['title' => __('models/returnFormats.fields.fix_price'), 'data' => 'fix_price']),
            't_cost' => new Column(['title' => __('models/returnFormats.fields.t_cost'), 'data' => 't_cost']),
            'other_price' => new Column(['title' => __('models/returnFormats.fields.other_price'), 'data' => 'other_price']),
            'total_price' => new Column(['title' => __('models/returnFormats.fields.total_price'), 'data' => 'total_price']),
            'note' => new Column(['title' => __('models/returnFormats.fields.note'), 'data' => 'note'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'return_formats_datatable_' . time();
    }
}
