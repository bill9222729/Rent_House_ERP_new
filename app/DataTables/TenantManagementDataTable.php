<?php

namespace App\DataTables;

use App\Models\TenantManagement;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class TenantManagementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'tenant_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\TenantManagement $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TenantManagement $model)
    {

        if (Auth::user()->role == 'admin') {
            return $model->newQuery();
        }
        return $model->newQuery()
            ->where('status', '黑名單')

            // ->orWhere('?', Auth::user()->name)
            ->orWhere('status', '警示客戶')
            ->orWhere('status', '退出');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order' => [[4, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $reTypeRender = '`${full.sublet == 1 ? "包租" : ""}${full.rent == 1 ? "代管" : ""}</a>`';
        $lessorNameRender = '`<a href="/tenantManagements/${full.id}/">${data}</a>`';
        $applyDateRender = '`${data.split(" ")[0]}`';
        return [
            'status' => new Column(['title' => '房客狀態', 'data' => 'status']),
            'lessee_name'=> new Column(['title' => '租客名稱', 'data' => 'lessee_name', 'render' => $lessorNameRender]),
            'sales'=> new Column(['title' => '業務', 'data' => 'sales']),
            'sublet' => new Column(['title' => '包租／代管', 'data' => 'sublet', 'render' => $reTypeRender]),
            'apply_date' => new Column(['title' => '申請日期', 'data' => 'apply_date', 'render' => $applyDateRender]),
            'tenant_case_number' => new Column(['title' => '案件編號', 'data' => 'tenant_case_number']),
            'tenant_ministry_of_the_interior_number' => new Column(['title' => '內政部編號', 'data' => 'tenant_ministry_of_the_interior_number']),
            'qualifications' => new Column(['title' => '承租人及家庭成員資格', 'data' => 'qualifications', 'render'=>'qualifications_render(data,type,full,meta)'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tenant_managements_datatable_' . time();
    }
}
