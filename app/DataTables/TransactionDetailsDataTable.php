<?php

namespace App\DataTables;

use App\Models\TransactionDetails;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class TransactionDetailsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'transaction_details.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\TransactionDetails $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TransactionDetails $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'account' => new Column(['title' => __('models/transactionDetails.fields.account'), 'data' => 'account']),
            't_date' => new Column(['title' => __('models/transactionDetails.fields.t_date'), 'data' => 't_date']),
            't_time' => new Column(['title' => __('models/transactionDetails.fields.t_time'), 'data' => 't_time']),
            'a_date' => new Column(['title' => __('models/transactionDetails.fields.a_date'), 'data' => 'a_date']),
            'detail' => new Column(['title' => __('models/transactionDetails.fields.detail'), 'data' => 'detail']),
            'expense' => new Column(['title' => __('models/transactionDetails.fields.expense'), 'data' => 'expense']),
            'income' => new Column(['title' => __('models/transactionDetails.fields.income'), 'data' => 'income']),
            'balance' => new Column(['title' => __('models/transactionDetails.fields.balance'), 'data' => 'balance']),
            'remark_1' => new Column(['title' => __('models/transactionDetails.fields.remark_1'), 'data' => 'remark_1']),
            'remark_2' => new Column(['title' => __('models/transactionDetails.fields.remark_2'), 'data' => 'remark_2']),
            'remark_3' => new Column(['title' => __('models/transactionDetails.fields.remark_3'), 'data' => 'remark_3']),
            'ticket_num' => new Column(['title' => __('models/transactionDetails.fields.ticket_num'), 'data' => 'ticket_num'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'transaction_details_datatable_' . time();
    }
}
