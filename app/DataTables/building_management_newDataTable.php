<?php

namespace App\DataTables;

use App\Models\building_management_new;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class building_management_newDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'building_management_news.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\building_management_new $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(building_management_new $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'building_address_city' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_city'), 'data' => 'building_address_city']),
            'building_address_city_area' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_city_area'), 'data' => 'building_address_city_area']),
            'building_address_street' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_street'), 'data' => 'building_address_street']),
            'building_address_ln' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_ln'), 'data' => 'building_address_ln']),
            'building_address_aly' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_aly'), 'data' => 'building_address_aly']),
            'building_address_num' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_num'), 'data' => 'building_address_num']),
            'building_address_num_hyphen' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_num_hyphen'), 'data' => 'building_address_num_hyphen']),
            'building_address_floor' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_floor'), 'data' => 'building_address_floor']),
            'building_address_floor_sub' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_floor_sub'), 'data' => 'building_address_floor_sub']),
            'building_address_room_num' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_room_num'), 'data' => 'building_address_room_num']),
            'building_address_building_floor' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_building_floor'), 'data' => 'building_address_building_floor']),
            'building_address_building_desc' => new Column(['title' => __('models/buildingManagementNews.fields.building_address_building_desc'), 'data' => 'building_address_building_desc']),
            'building_located_city' => new Column(['title' => __('models/buildingManagementNews.fields.building_located_city'), 'data' => 'building_located_city']),
            'building_located_city_area' => new Column(['title' => __('models/buildingManagementNews.fields.building_located_city_area'), 'data' => 'building_located_city_area']),
            'building_located_lot' => new Column(['title' => __('models/buildingManagementNews.fields.building_located_lot'), 'data' => 'building_located_lot']),
            'building_located_land_num' => new Column(['title' => __('models/buildingManagementNews.fields.building_located_land_num'), 'data' => 'building_located_land_num']),
            'building_num' => new Column(['title' => __('models/buildingManagementNews.fields.building_num'), 'data' => 'building_num']),
            'building_rent_type' => new Column(['title' => __('models/buildingManagementNews.fields.building_rent_type'), 'data' => 'building_rent_type']),
            'building_pattren' => new Column(['title' => __('models/buildingManagementNews.fields.building_pattren'), 'data' => 'building_pattren']),
            'building_type' => new Column(['title' => __('models/buildingManagementNews.fields.building_type'), 'data' => 'building_type']),
            'building_age' => new Column(['title' => __('models/buildingManagementNews.fields.building_age'), 'data' => 'building_age']),
            'building_complete_date' => new Column(['title' => __('models/buildingManagementNews.fields.building_complete_date'), 'data' => 'building_complete_date']),
            'building_rooms' => new Column(['title' => __('models/buildingManagementNews.fields.building_rooms'), 'data' => 'building_rooms']),
            'building_livingrooms' => new Column(['title' => __('models/buildingManagementNews.fields.building_livingrooms'), 'data' => 'building_livingrooms']),
            'building_bathrooms' => new Column(['title' => __('models/buildingManagementNews.fields.building_bathrooms'), 'data' => 'building_bathrooms']),
            'building_compartment_material' => new Column(['title' => __('models/buildingManagementNews.fields.building_compartment_material'), 'data' => 'building_compartment_material']),
            'building_total_square_meter' => new Column(['title' => __('models/buildingManagementNews.fields.building_total_square_meter'), 'data' => 'building_total_square_meter']),
            'building_use_square_meter' => new Column(['title' => __('models/buildingManagementNews.fields.building_use_square_meter'), 'data' => 'building_use_square_meter']),
            'building_materials' => new Column(['title' => __('models/buildingManagementNews.fields.building_materials'), 'data' => 'building_materials']),
            'building_rent' => new Column(['title' => __('models/buildingManagementNews.fields.building_rent'), 'data' => 'building_rent']),
            'building_deposit_amount' => new Column(['title' => __('models/buildingManagementNews.fields.building_deposit_amount'), 'data' => 'building_deposit_amount']),
            'building_is_including_electricity_fees' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_electricity_fees'), 'data' => 'building_is_including_electricity_fees']),
            'building_is_cooking' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_cooking'), 'data' => 'building_is_cooking']),
            'building_is_including_water_fees' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_water_fees'), 'data' => 'building_is_including_water_fees']),
            'building_is_including_parking_space' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_parking_space'), 'data' => 'building_is_including_parking_space']),
            'building_is_including_accessible_equipment' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_accessible_equipment'), 'data' => 'building_is_including_accessible_equipment']),
            'building_is_including_natural_gas' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_natural_gas'), 'data' => 'building_is_including_natural_gas']),
            'building_is_including_cable' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_cable'), 'data' => 'building_is_including_cable']),
            'building_is_including_internet' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_internet'), 'data' => 'building_is_including_internet']),
            'building_is_including_cleaning_fee' => new Column(['title' => __('models/buildingManagementNews.fields.building_is_including_cleaning_fee'), 'data' => 'building_is_including_cleaning_fee']),
            'building_offer_tv' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_tv'), 'data' => 'building_offer_tv']),
            'building_offer_refrigerator' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_refrigerator'), 'data' => 'building_offer_refrigerator']),
            'building_offer_cable' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_cable'), 'data' => 'building_offer_cable']),
            'building_offer_air_conditioner' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_air_conditioner'), 'data' => 'building_offer_air_conditioner']),
            'building_offer_geyser' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_geyser'), 'data' => 'building_offer_geyser']),
            'building_offer_internet' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_internet'), 'data' => 'building_offer_internet']),
            'building_offer_washing_machine' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_washing_machine'), 'data' => 'building_offer_washing_machine']),
            'building_offer_natural_gas' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_natural_gas'), 'data' => 'building_offer_natural_gas']),
            'building_offer_bed' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_bed'), 'data' => 'building_offer_bed']),
            'building_offer_wardrobe' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_wardrobe'), 'data' => 'building_offer_wardrobe']),
            'building_offer_desk' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_desk'), 'data' => 'building_offer_desk']),
            'building_offer_chair' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_chair'), 'data' => 'building_offer_chair']),
            'building_offer_sofa' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_sofa'), 'data' => 'building_offer_sofa']),
            'building_offer_other' => new Column(['title' => __('models/buildingManagementNews.fields.building_offer_other'), 'data' => 'building_offer_other']),
            'building_use' => new Column(['title' => __('models/buildingManagementNews.fields.building_use'), 'data' => 'building_use']),
            'apply_date' => new Column(['title' => __('models/buildingManagementNews.fields.apply_date'), 'data' => 'apply_date']),
            'case_no' => new Column(['title' => __('models/buildingManagementNews.fields.case_no'), 'data' => 'case_no']),
            'case_type_chartering' => new Column(['title' => __('models/buildingManagementNews.fields.case_type_chartering'), 'data' => 'case_type_chartering']),
            'case_type_escrow' => new Column(['title' => __('models/buildingManagementNews.fields.case_type_escrow'), 'data' => 'case_type_escrow']),
            'case_state' => new Column(['title' => __('models/buildingManagementNews.fields.case_state'), 'data' => 'case_state']),
            'name' => new Column(['title' => __('models/buildingManagementNews.fields.name'), 'data' => 'name']),
            'gender' => new Column(['title' => __('models/buildingManagementNews.fields.gender'), 'data' => 'gender']),
            'birthday' => new Column(['title' => __('models/buildingManagementNews.fields.birthday'), 'data' => 'birthday']),
            'is_foreigner' => new Column(['title' => __('models/buildingManagementNews.fields.is_foreigner'), 'data' => 'is_foreigner']),
            'id_no' => new Column(['title' => __('models/buildingManagementNews.fields.id_no'), 'data' => 'id_no']),
            'passport_code' => new Column(['title' => __('models/buildingManagementNews.fields.passport_code'), 'data' => 'passport_code']),
            'house_no' => new Column(['title' => __('models/buildingManagementNews.fields.house_no'), 'data' => 'house_no']),
            'tal_day' => new Column(['title' => __('models/buildingManagementNews.fields.tal_day'), 'data' => 'tal_day']),
            'tal_night' => new Column(['title' => __('models/buildingManagementNews.fields.tal_night'), 'data' => 'tal_night']),
            'cellphone' => new Column(['title' => __('models/buildingManagementNews.fields.cellphone'), 'data' => 'cellphone']),
            'email' => new Column(['title' => __('models/buildingManagementNews.fields.email'), 'data' => 'email']),
            'residence_city' => new Column(['title' => __('models/buildingManagementNews.fields.residence_city'), 'data' => 'residence_city']),
            'residence_city_area' => new Column(['title' => __('models/buildingManagementNews.fields.residence_city_area'), 'data' => 'residence_city_area']),
            'residence_addr' => new Column(['title' => __('models/buildingManagementNews.fields.residence_addr'), 'data' => 'residence_addr']),
            'mailing_city' => new Column(['title' => __('models/buildingManagementNews.fields.mailing_city'), 'data' => 'mailing_city']),
            'mailing_city_area' => new Column(['title' => __('models/buildingManagementNews.fields.mailing_city_area'), 'data' => 'mailing_city_area']),
            'mailing_addr' => new Column(['title' => __('models/buildingManagementNews.fields.mailing_addr'), 'data' => 'mailing_addr']),
            'legal_agent_num' => new Column(['title' => __('models/buildingManagementNews.fields.legal_agent_num'), 'data' => 'legal_agent_num']),
            'single_proxy_reason' => new Column(['title' => __('models/buildingManagementNews.fields.single_proxy_reason'), 'data' => 'single_proxy_reason']),
            'first_legal_agent_name' => new Column(['title' => __('models/buildingManagementNews.fields.first_legal_agent_name'), 'data' => 'first_legal_agent_name']),
            'first_legal_agent_tel' => new Column(['title' => __('models/buildingManagementNews.fields.first_legal_agent_tel'), 'data' => 'first_legal_agent_tel']),
            'first_legal_agent_phone' => new Column(['title' => __('models/buildingManagementNews.fields.first_legal_agent_phone'), 'data' => 'first_legal_agent_phone']),
            'first_legal_agent_addr' => new Column(['title' => __('models/buildingManagementNews.fields.first_legal_agent_addr'), 'data' => 'first_legal_agent_addr']),
            'second_legal_agent_name' => new Column(['title' => __('models/buildingManagementNews.fields.second_legal_agent_name'), 'data' => 'second_legal_agent_name']),
            'second_legal_agent_tel' => new Column(['title' => __('models/buildingManagementNews.fields.second_legal_agent_tel'), 'data' => 'second_legal_agent_tel']),
            'second_legal_agent_phone' => new Column(['title' => __('models/buildingManagementNews.fields.second_legal_agent_phone'), 'data' => 'second_legal_agent_phone']),
            'second_legal_agent_addr' => new Column(['title' => __('models/buildingManagementNews.fields.second_legal_agent_addr'), 'data' => 'second_legal_agent_addr']),
            'bank_name' => new Column(['title' => __('models/buildingManagementNews.fields.bank_name'), 'data' => 'bank_name']),
            'bank_branch_code' => new Column(['title' => __('models/buildingManagementNews.fields.bank_branch_code'), 'data' => 'bank_branch_code']),
            'bank_owner_name' => new Column(['title' => __('models/buildingManagementNews.fields.bank_owner_name'), 'data' => 'bank_owner_name']),
            'bank_account' => new Column(['title' => __('models/buildingManagementNews.fields.bank_account'), 'data' => 'bank_account'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'building_management_news_datatable_' . time();
    }
}
