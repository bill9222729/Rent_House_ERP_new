<?php

namespace App\DataTables;

use App\Models\business_management;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class business_managementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'business_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\business_management $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(business_management $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => new Column(['title' => __('models/businessManagements.fields.name'), 'data' => 'name']),
            'address' => new Column(['title' => __('models/businessManagements.fields.address'), 'data' => 'address']),
            'phone' => new Column(['title' => __('models/businessManagements.fields.phone'), 'data' => 'phone']),
            'id_number' => new Column(['title' => __('models/businessManagements.fields.id_number'), 'data' => 'id_number']),
            'birthday' => new Column(['title' => __('models/businessManagements.fields.birthday'), 'data' => 'birthday']),
            'bank_account' => new Column(['title' => __('models/businessManagements.fields.bank_account'), 'data' => 'bank_account']),
            'management_fee_set' => new Column(['title' => __('models/businessManagements.fields.management_fee_set'), 'data' => 'management_fee_set']),
            'match_fee_set' => new Column(['title' => __('models/businessManagements.fields.match_fee_set'), 'data' => 'match_fee_set']),
            'escrow_quota' => new Column(['title' => __('models/businessManagements.fields.escrow_quota'), 'data' => 'escrow_quota']),
            'box_quota' => new Column(['title' => __('models/businessManagements.fields.box_quota'), 'data' => 'box_quota'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'business_managements_datatable_' . time();
    }
}
