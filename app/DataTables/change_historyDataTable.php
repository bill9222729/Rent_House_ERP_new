<?php

namespace App\DataTables;

use App\Models\change_history;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class change_historyDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'change_histories.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\change_history $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(change_history $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'building_id' => new Column(['title' => __('models/changeHistories.fields.building_id'), 'data' => 'building_id']),
            'apply_date' => new Column(['title' => __('models/changeHistories.fields.apply_date'), 'data' => 'apply_date']),
            'change_date' => new Column(['title' => __('models/changeHistories.fields.change_date'), 'data' => 'change_date']),
            'lessor_type' => new Column(['title' => __('models/changeHistories.fields.lessor_type'), 'data' => 'lessor_type']),
            'name' => new Column(['title' => __('models/changeHistories.fields.name'), 'data' => 'name']),
            'phone' => new Column(['title' => __('models/changeHistories.fields.phone'), 'data' => 'phone']),
            'reason' => new Column(['title' => __('models/changeHistories.fields.reason'), 'data' => 'reason']),
            'change_note' => new Column(['title' => __('models/changeHistories.fields.change_note'), 'data' => 'change_note']),
            'edit_member_id' => new Column(['title' => __('models/changeHistories.fields.edit_member_id'), 'data' => 'edit_member_id'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'change_histories_datatable_' . time();
    }
}
