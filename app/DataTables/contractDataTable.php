<?php

namespace App\DataTables;

use App\Models\contract;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class contractDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'contracts.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\contract $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(contract $model)
    {
        if (Auth::user()->role === 'sales') {

            $user_name = Auth::user()->name;
            return $model->newQuery()->where('contract_business_sign_back', $user_name)->join('tenant_management', 'contracts.tenant_case_number', '=', 'tenant_management.tenant_case_number');
        }

        $data = $model->newQuery()
            ->with('buildingManagement')->with('tenantManagement')
            ->join('tenant_management', 'contracts.tenant_case_number', '=', 'tenant_management.tenant_case_number')
            ->select(
                'contracts.*',
                'tenant_management.lessee_name AS lessee_name'
            );

        Log::info("[" . __METHOD__ . "][" . __LINE__ . "]" . json_encode($data));
        return $data;
        // return $model->newQuery()->with('buildingManagement')->with('tenantManagement');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => 'frtip',
                'stateSave' => true,
                'order' => [[0, 'desc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $landlordMatchNumberRender = '`<a href="/contracts/${full.id}/">${data}</a>`';
        $addressRender = '`${full.building_management.address_city}${full.building_management.address_cityarea}${full.building_management.address_street}${full.building_management.address_ln ? full.building_management.address_ln + "巷" : ""}${full.building_management.address_aly? full.building_management.address_aly : ""}${full.building_management.address_num? full.building_management.address_num + "號" : ""}${full.building_management.address_num_hyphen? full.building_management.address_num_hyphen + "樓": ""} ${full.building_management.h_info_suite? full.building_management.h_info_suite + "室/房" : ""}`';
        $landlordContractRender = '`${full.landlord_contract_date} ~ ${full.landlord_contract_expiry_date}`';

        return [
            'contract_number_of_periods' => new Column(['title' => __('models/contracts.fields.contract_number_of_periods'), 'data' => 'contract_number_of_periods']),
            'contract_category' => new Column(['title' => __('models/contracts.fields.contract_category'), 'data' => 'contract_category']),
            // 'contract_match_date' => new Column(['title' => __('models/contracts.fields.contract_match_date'), 'data' => 'contract_match_date']),
            'landlord_match_number' => new Column(['title' => '媒合編號', 'data' => 'landlord_match_number', 'render' => $landlordMatchNumberRender]),
            'item_owner' => new Column(['title' => __('models/contracts.fields.item_owner'), 'data' => 'item_owner']),
            'item_code' => new Column(['title' => __('models/contracts.fields.item_code'), 'data' => 'item_code']),
            'landlord_name' => new Column(['title' => "房客名稱", 'data' => 'landlord_name']),
            'landlord_existing_tenant' => new Column(['title' => '物件地址', 'data' => 'landlord_existing_tenant', 'render' => $addressRender]),
            'landlord_contract_date' => new Column(['title' => '契約起日', 'data' => 'landlord_contract_date', 'render' => $landlordContractRender]),
            'landlord_contract_expiry_date' => new Column(['title' => '契約訖日', 'data' => 'landlord_contract_expiry_date']),
            'price' => new Column(['title' => __('models/contracts.fields.price'), 'data' => 'price']),
            'contract_business_sign_back' => new Column(['title' => __('models/contracts.fields.contract_business_sign_back'), 'data' => 'contract_business_sign_back']),
            //            'buildAddress' => new Column(['title' => '物件地址', 'data' => 'building_management.located_build_address']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contracts_datatable_' . time();
    }
}
