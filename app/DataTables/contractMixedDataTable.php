<?php

namespace App\DataTables;

use App\Models\contract;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class contractMixedDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\contract $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(contract $model)
    {
        return $model->newQuery()->with(['buildingManagement']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'landlord_match_number' => new Column(['title' => __('models/contracts.fields.landlord_match_number'), 'data' => 'landlord_match_number']),
            'contract_business_sign_back' => new Column(['title' => '業務姓名', 'data' => 'contract_business_sign_back']),
            'landlord_contract_date' => new Column(['title' => __('models/contracts.fields.landlord_contract_date'), 'data' => 'landlord_contract_date']),
            'landlord_contract_expiry_date' => new Column(['title' => __('models/contracts.fields.landlord_contract_expiry_date'), 'data' => 'landlord_contract_expiry_date']),
            'landlord_item_number' => new Column(['title' => __('models/contracts.fields.landlord_item_number'), 'data' => 'landlord_item_number']),
            // 'buildAddress' => new Column(['title' => '物件地址', 'data' => 'building_management.address']),
            'buildAddress' => new Column(['title' => '物件地址', 'data' => 'building_management.located_build_address']),
            'landlord_types_of' => new Column(['title' => __('models/contracts.fields.landlord_types_of'), 'data' => 'landlord_types_of']),
            'collection_agent' => new Column(['title' => '收款人', 'data' => 'building_management.collection_agent']),
            'agent_ID_num' => new Column(['title' => '收款人ID', 'data' => 'building_management.agent_ID_num']),
            'agent_bank_name' => new Column(['title' => '銀行名稱', 'data' => 'building_management.agent_bank_name']),
            'agent_branch_name' => new Column(['title' => '銀行代號', 'data' => 'building_management.agent_branch_name']),
            'landlord_bank_account' => new Column(['title' => '銀行帳號', 'data' => 'building_management.landlord_bank_account']),
            'actual_contract_tenant_pays_rent' => new Column(['title' => '房客自付額', 'data' => 'actual_contract_tenant_pays_rent']),
            'actual_contract_rent_subsidy' => new Column(['title' => '租金補助', 'data' => 'actual_contract_rent_subsidy']),
            'landlord_existing_tenant' => new Column(['title' => '現有房客', 'data' => 'building_management.landlord_existing_tenant']),
            'a' => new Column(['title' => '管理費', 'data' => '_']),
            'b' => new Column(['title' => '停車費', 'data' => '_']),
            'c' => new Column(['title' => '修繕費', 'data' => '_']),
            'e' => new Column(['title' => '手續費', 'data' => '_']),
            'f' => new Column(['title' => '其他', 'data' => '_']),
            'g' => new Column(['title' => '應匯租金', 'data' => '_']),
            'h' => new Column(['title' => '備註', 'data' => '_']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contracts_datatable_' . time();
    }
}
