<?php

namespace App\DataTables;

use App\Models\contract_management;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class contract_managementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'contract_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\contract_management $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(contract_management $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'case_status' => new Column(['title' => __('models/contractManagements.fields.case_status'), 'data' => 'case_status']),
            'signing_date' => new Column(['title' => __('models/contractManagements.fields.signing_date'), 'data' => 'signing_date']),
            'other_landlord_desc' => new Column(['title' => __('models/contractManagements.fields.other_landlord_desc'), 'data' => 'other_landlord_desc']),
            'lease_start_date' => new Column(['title' => __('models/contractManagements.fields.lease_start_date'), 'data' => 'lease_start_date']),
            'lease_end_date' => new Column(['title' => __('models/contractManagements.fields.lease_end_date'), 'data' => 'lease_end_date']),
            'rent' => new Column(['title' => __('models/contractManagements.fields.rent'), 'data' => 'rent']),
            'rent_pay_type' => new Column(['title' => __('models/contractManagements.fields.rent_pay_type'), 'data' => 'rent_pay_type']),
            'bank_name' => new Column(['title' => __('models/contractManagements.fields.bank_name'), 'data' => 'bank_name']),
            'bank_branch_code' => new Column(['title' => __('models/contractManagements.fields.bank_branch_code'), 'data' => 'bank_branch_code']),
            'bank_owner_name' => new Column(['title' => __('models/contractManagements.fields.bank_owner_name'), 'data' => 'bank_owner_name']),
            'bank_account' => new Column(['title' => __('models/contractManagements.fields.bank_account'), 'data' => 'bank_account']),
            'deposit_amount' => new Column(['title' => __('models/contractManagements.fields.deposit_amount'), 'data' => 'deposit_amount']),
            'contract_note' => new Column(['title' => __('models/contractManagements.fields.contract_note'), 'data' => 'contract_note']),
            'apply_man_id' => new Column(['title' => __('models/contractManagements.fields.apply_man_id'), 'data' => 'apply_man_id']),
            'remark' => new Column(['title' => __('models/contractManagements.fields.remark'), 'data' => 'remark']),
            'file_path' => new Column(['title' => __('models/contractManagements.fields.file_path'), 'data' => 'file_path'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contract_managements_datatable_' . time();
    }
}
