<?php

namespace App\DataTables;

use App\Models\contracts_new;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use DB;

class contracts_newDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'contracts_news.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\contracts_new $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(contracts_new $model)
    {
        return $model->newQuery()
            ->leftJoin('building_management_new', 'contracts_new.building_case_id', '=', 'building_management_new.case_no')
            ->leftJoin('tenant_management_new', 'contracts_new.tenant_case_id', '=', 'tenant_management_new.case_no')
            ->select('contracts_new.*', 'building_management_new.name as landlord_name', 'tenant_management_new.name as tenant_name');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $lease_date = '`${full.lease_start_date} ~ ${full.lease_end_date}`';
        return [
            'match_id' => new Column(['title' => __('models/contractsNews.fields.match_id'), 'data' => 'match_id']),
            'contract_type' => new Column(['title' => __('models/contractsNews.fields.contract_type'), 'data' => 'contract_type']),
            'part_a' => new Column(['title' => __('models/contractsNews.fields.part_a'), 'data' => 'landlord_name']),
            'part_b' => new Column(['title' => __('models/contractsNews.fields.part_b'), 'data' => 'tenant_name']),
            // 'service_unit' => new Column(['title' => __('models/contractsNews.fields.service_unit'), 'data' => 'service_unit', 'render' => '易居管理顧問股份有限公司']),
            'contract_date' => new Column(['title' => __('models/contractsNews.fields.contract_date'), 'data' => 'test', 'render' => '125555']),
            'lease_date' => new Column(['title' => __('models/contractsNews.fields.lease_date'), 'data' => 'test', 'render' => $lease_date]),
            'rent' => new Column(['title' => __('models/contractsNews.fields.rent'), 'data' => 'test', 'render' => '125555']),
            'contract_state' => new Column(['title' => __('models/contractsNews.fields.contract_state'), 'data' => 'test', 'render' => '125555']),
            'form_state' => new Column(['title' => __('models/contractsNews.fields.form_state'), 'data' => 'test', 'render' => '125555']),
            // 'building_case_id' => new Column(['title' => __('models/contractsNews.fields.building_case_id'), 'data' => 'building_case_id']),
            // 'tenant_case_id' => new Column(['title' => __('models/contractsNews.fields.tenant_case_id'), 'data' => 'tenant_case_id']),
            // 'lease_start_date' => new Column(['title' => __('models/contractsNews.fields.lease_start_date'), 'data' => 'lease_start_date']),
            // 'lease_end_date' => new Column(['title' => __('models/contractsNews.fields.lease_end_date'), 'data' => 'lease_end_date']),
            // 'charter_case_id' => new Column(['title' => __('models/contractsNews.fields.charter_case_id'), 'data' => 'charter_case_id']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contracts_news_datatable_' . time();
    }
}
