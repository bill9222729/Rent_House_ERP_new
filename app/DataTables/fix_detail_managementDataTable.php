<?php

namespace App\DataTables;

use App\Models\fix_detail_management;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class fix_detail_managementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'fix_detail_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\fix_detail_management $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(fix_detail_management $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'report_date' => new Column(['title' => __('models/fixDetailManagements.fields.report_date'), 'data' => 'report_date']),
            'fix_id' => new Column(['title' => __('models/fixDetailManagements.fields.fix_id'), 'data' => 'fix_id']),
            'repair_item' => new Column(['title' => __('models/fixDetailManagements.fields.repair_item'), 'data' => 'repair_item']),
            'repair_manufacturers' => new Column(['title' => __('models/fixDetailManagements.fields.repair_manufacturers'), 'data' => 'repair_manufacturers']),
            'repair_records' => new Column(['title' => __('models/fixDetailManagements.fields.repair_records'), 'data' => 'repair_records']),
            'meeting_date' => new Column(['title' => __('models/fixDetailManagements.fields.meeting_date'), 'data' => 'meeting_date']),
            'meeting_master' => new Column(['title' => __('models/fixDetailManagements.fields.meeting_master'), 'data' => 'meeting_master']),
            'fix_date' => new Column(['title' => __('models/fixDetailManagements.fields.fix_date'), 'data' => 'fix_date']),
            'completion_date' => new Column(['title' => __('models/fixDetailManagements.fields.completion_date'), 'data' => 'completion_date']),
            'please_amount' => new Column(['title' => __('models/fixDetailManagements.fields.please_amount'), 'data' => 'please_amount']),
            'reply_amount' => new Column(['title' => __('models/fixDetailManagements.fields.reply_amount'), 'data' => 'reply_amount']),
            'monthly_application_month' => new Column(['title' => __('models/fixDetailManagements.fields.monthly_application_month'), 'data' => 'monthly_application_month']),
            'balance' => new Column(['title' => __('models/fixDetailManagements.fields.balance'), 'data' => 'balance']),
            'note' => new Column(['title' => __('models/fixDetailManagements.fields.note'), 'data' => 'note']),
            'no_reference' => new Column(['title' => __('models/fixDetailManagements.fields.no_reference'), 'data' => 'no_reference'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'fix_detail_managements_datatable_' . time();
    }
}
