<?php

namespace App\DataTables;

use App\Models\insurance_management;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class insurance_managementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'insurance_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\insurance_management $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(insurance_management $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'sales_id' => new Column(['title' => __('models/insuranceManagements.fields.sales_id'), 'data' => 'sales_id']),
            'match_id' => new Column(['title' => __('models/insuranceManagements.fields.match_id'), 'data' => 'match_id']),
            'building_id' => new Column(['title' => __('models/insuranceManagements.fields.building_id'), 'data' => 'building_id']),
            'insurance_premium' => new Column(['title' => __('models/insuranceManagements.fields.insurance_premium'), 'data' => 'insurance_premium']),
            'insurer' => new Column(['title' => __('models/insuranceManagements.fields.insurer'), 'data' => 'insurer']),
            'insurance_start' => new Column(['title' => __('models/insuranceManagements.fields.insurance_start'), 'data' => 'insurance_start']),
            'insurance_end' => new Column(['title' => __('models/insuranceManagements.fields.insurance_end'), 'data' => 'insurance_end']),
            'monthly_application_month' => new Column(['title' => __('models/insuranceManagements.fields.monthly_application_month'), 'data' => 'monthly_application_month'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'insurance_managements_datatable_' . time();
    }
}
