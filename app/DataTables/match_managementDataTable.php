<?php

namespace App\DataTables;

use App\Models\match_management;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class match_managementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'match_managements.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\match_management $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(match_management $model)
    {
        return $model->newQuery()
            ->leftJoin('building_management_new', 'match_management.building_case_id', '=', 'building_management_new.case_no')
            ->leftJoin('tenant_management_new', 'match_management.tenant_case_id', '=', 'tenant_management_new.case_no')
            ->leftJoin('contract_management', 'match_management.match_id', '=', 'contract_management.match_id')
            ->select(
                'match_management.*',
                'building_management_new.name as landlord_name',
                'contract_management.rent',
                'contract_management.case_status',
                'contract_management.signing_date',
                \DB::raw('CASE WHEN (ISNULL(tenant_management_new.name) AND match_management.contract_type = "包租約") THEN "易居管理顧問股份有限公司" ELSE tenant_management_new.name END AS tenant_name')
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $lease_date = '`${full.lease_start_date} ~ ${full.lease_end_date}`';
        return [
            'match_id' => new Column(['title' => __('models/matchManagements.fields.match_id'), 'data' => 'match_id']),
            'contract_type' => new Column(['title' => __('models/matchManagements.fields.contract_type'), 'data' => 'contract_type']),
            'part_a' => new Column(['title' => __('models/matchManagements.fields.part_a'), 'data' => 'landlord_name']),
            'part_b' => new Column(['title' => __('models/matchManagements.fields.part_b'), 'data' => 'tenant_name']),
            'service_unit' => new Column(['title' => __('models/matchManagements.fields.service_unit'), 'data' => 'service_unit', 'render' => "'易居管理顧問股份有限公司'"]),
            'contract_date' => new Column(['title' => __('models/matchManagements.fields.contract_date'), 'data' => 'signing_date']),
            'lease_date' => new Column(['title' => __('models/matchManagements.fields.lease_date'), 'data' => 'test', 'render' => $lease_date]),
            'rent' => new Column(['title' => __('models/matchManagements.fields.rent'), 'data' => 'rent']),
            'contract_state' => new Column(['title' => __('models/matchManagements.fields.contract_state'), 'data' => 'case_status']),
            'form_state' => new Column(['title' => __('models/matchManagements.fields.form_state'), 'data' => 'form_state']),
            // 'building_case_id' => new Column(['title' => __('models/matchManagements.fields.building_case_id'), 'data' => 'building_case_id']),
            // 'tenant_case_id' => new Column(['title' => __('models/matchManagements.fields.tenant_case_id'), 'data' => 'tenant_case_id']),
            // 'lease_start_date' => new Column(['title' => __('models/matchManagements.fields.lease_start_date'), 'data' => 'lease_start_date']),
            // 'lease_end_date' => new Column(['title' => __('models/matchManagements.fields.lease_end_date'), 'data' => 'lease_end_date']),
            // 'charter_case_id' => new Column(['title' => __('models/matchManagements.fields.charter_case_id'), 'data' => 'charter_case_id']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'match_managements_datatable_' . time();
    }
}
