<?php

namespace App\DataTables;

use App\Models\tenant_building_info;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class tenant_building_infoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'tenant_building_infos.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\tenant_building_info $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(tenant_building_info $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'tenant_id' => new Column(['title' => __('models/tenantBuildingInfos.fields.tenant_id'), 'data' => 'tenant_id']),
            'name' => new Column(['title' => __('models/tenantBuildingInfos.fields.name'), 'data' => 'name']),
            'located_city' => new Column(['title' => __('models/tenantBuildingInfos.fields.located_city'), 'data' => 'located_city']),
            'located_Twn_spcode_area' => new Column(['title' => __('models/tenantBuildingInfos.fields.located_Twn_spcode_area'), 'data' => 'located_Twn_spcode_area']),
            'located_lot_area' => new Column(['title' => __('models/tenantBuildingInfos.fields.located_lot_area'), 'data' => 'located_lot_area']),
            'located_landno' => new Column(['title' => __('models/tenantBuildingInfos.fields.located_landno'), 'data' => 'located_landno']),
            'building_no' => new Column(['title' => __('models/tenantBuildingInfos.fields.building_no'), 'data' => 'building_no']),
            'building_total_square_meter_percent' => new Column(['title' => __('models/tenantBuildingInfos.fields.building_total_square_meter_percent'), 'data' => 'building_total_square_meter_percent']),
            'building_total_square_meter' => new Column(['title' => __('models/tenantBuildingInfos.fields.building_total_square_meter'), 'data' => 'building_total_square_meter']),
            'addr_cntcode' => new Column(['title' => __('models/tenantBuildingInfos.fields.addr_cntcode'), 'data' => 'addr_cntcode']),
            'addr_twnspcode' => new Column(['title' => __('models/tenantBuildingInfos.fields.addr_twnspcode'), 'data' => 'addr_twnspcode']),
            'addr_street_area' => new Column(['title' => __('models/tenantBuildingInfos.fields.addr_street_area'), 'data' => 'addr_street_area']),
            'addr_lane' => new Column(['title' => __('models/tenantBuildingInfos.fields.addr_lane'), 'data' => 'addr_lane']),
            'addr_alley' => new Column(['title' => __('models/tenantBuildingInfos.fields.addr_alley'), 'data' => 'addr_alley']),
            'addr_no' => new Column(['title' => __('models/tenantBuildingInfos.fields.addr_no'), 'data' => 'addr_no']),
            'is_household' => new Column(['title' => __('models/tenantBuildingInfos.fields.is_household'), 'data' => 'is_household']),
            'filers' => new Column(['title' => __('models/tenantBuildingInfos.fields.filers'), 'data' => 'filers']),
            'last_modified_person' => new Column(['title' => __('models/tenantBuildingInfos.fields.last_modified_person'), 'data' => 'last_modified_person'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tenant_building_infos_datatable_' . time();
    }
}
