<?php

namespace App\DataTables;

use App\Models\tenant_family_info;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class tenant_family_infoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'tenant_family_infos.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\tenant_family_info $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(tenant_family_info $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'tenant_id' => new Column(['title' => __('models/tenantFamilyInfos.fields.tenant_id'), 'data' => 'tenant_id']),
            'name' => new Column(['title' => __('models/tenantFamilyInfos.fields.name'), 'data' => 'name']),
            'is_foreigner' => new Column(['title' => __('models/tenantFamilyInfos.fields.is_foreigner'), 'data' => 'is_foreigner']),
            'id_no' => new Column(['title' => __('models/tenantFamilyInfos.fields.id_no'), 'data' => 'id_no']),
            'appellation' => new Column(['title' => __('models/tenantFamilyInfos.fields.appellation'), 'data' => 'appellation']),
            'gender' => new Column(['title' => __('models/tenantFamilyInfos.fields.gender'), 'data' => 'gender']),
            'condition_low_income_households' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_low_income_households'), 'data' => 'condition_low_income_households']),
            'condition_low_and_middle_income_households' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_low_and_middle_income_households'), 'data' => 'condition_low_and_middle_income_households']),
            'condition_special_circumstances_families' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_special_circumstances_families'), 'data' => 'condition_special_circumstances_families']),
            'condition_over_sixty_five' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_over_sixty_five'), 'data' => 'condition_over_sixty_five']),
            'condition_domestic_violence' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_domestic_violence'), 'data' => 'condition_domestic_violence']),
            'condition_disabled' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_disabled'), 'data' => 'condition_disabled']),
            'condition_disabled_type' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_disabled_type'), 'data' => 'condition_disabled_type']),
            'condition_disabled_level' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_disabled_level'), 'data' => 'condition_disabled_level']),
            'condition_aids' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_aids'), 'data' => 'condition_aids']),
            'condition_aboriginal' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_aboriginal'), 'data' => 'condition_aboriginal']),
            'condition_disaster_victims' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_disaster_victims'), 'data' => 'condition_disaster_victims']),
            'condition_vagabond' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_vagabond'), 'data' => 'condition_vagabond']),
            'condition_Applicant_naturalization' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_Applicant_naturalization'), 'data' => 'condition_Applicant_naturalization']),
            'condition_Applicant_not_naturalization' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_Applicant_not_naturalization'), 'data' => 'condition_Applicant_not_naturalization']),
            'condition_Applicant_not_naturalization_and_study' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_Applicant_not_naturalization_and_study'), 'data' => 'condition_Applicant_not_naturalization_and_study']),
            'condition_police_officer' => new Column(['title' => __('models/tenantFamilyInfos.fields.condition_police_officer'), 'data' => 'condition_police_officer']),
            'filers' => new Column(['title' => __('models/tenantFamilyInfos.fields.filers'), 'data' => 'filers']),
            'last_modified_person' => new Column(['title' => __('models/tenantFamilyInfos.fields.last_modified_person'), 'data' => 'last_modified_person'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tenant_family_infos_datatable_' . time();
    }
}
