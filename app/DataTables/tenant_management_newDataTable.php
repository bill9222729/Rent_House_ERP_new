<?php

namespace App\DataTables;

use App\Models\tenant_management_new;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class tenant_management_newDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'tenant_management_news.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\tenant_management_new $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(tenant_management_new $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'apply_date' => new Column(['title' => __('models/tenantManagementNews.fields.apply_date'), 'data' => 'apply_date']),
            'sales_id' => new Column(['title' => __('models/tenantManagementNews.fields.sales_id'), 'data' => 'sales_id']),
            'government_no' => new Column(['title' => __('models/tenantManagementNews.fields.government_no'), 'data' => 'government_no']),
            'case_no' => new Column(['title' => __('models/tenantManagementNews.fields.case_no'), 'data' => 'case_no']),
            'case_type_chartering' => new Column(['title' => __('models/tenantManagementNews.fields.case_type_chartering'), 'data' => 'case_type_chartering']),
            'case_type_escrow' => new Column(['title' => __('models/tenantManagementNews.fields.case_type_escrow'), 'data' => 'case_type_escrow']),
            'case_status' => new Column(['title' => __('models/tenantManagementNews.fields.case_status'), 'data' => 'case_status']),
            'name' => new Column(['title' => __('models/tenantManagementNews.fields.name'), 'data' => 'name']),
            'gender' => new Column(['title' => __('models/tenantManagementNews.fields.gender'), 'data' => 'gender']),
            'birthday' => new Column(['title' => __('models/tenantManagementNews.fields.birthday'), 'data' => 'birthday']),
            'id_no' => new Column(['title' => __('models/tenantManagementNews.fields.id_no'), 'data' => 'id_no']),
            'house_no' => new Column(['title' => __('models/tenantManagementNews.fields.house_no'), 'data' => 'house_no']),
            'tel_day' => new Column(['title' => __('models/tenantManagementNews.fields.tel_day'), 'data' => 'tel_day']),
            'tel_night' => new Column(['title' => __('models/tenantManagementNews.fields.tel_night'), 'data' => 'tel_night']),
            'cellphone' => new Column(['title' => __('models/tenantManagementNews.fields.cellphone'), 'data' => 'cellphone']),
            'email' => new Column(['title' => __('models/tenantManagementNews.fields.email'), 'data' => 'email']),
            'membtype' => new Column(['title' => __('models/tenantManagementNews.fields.membtype'), 'data' => 'membtype']),
            'weak_item' => new Column(['title' => __('models/tenantManagementNews.fields.weak_item'), 'data' => 'weak_item']),
            'residence_city' => new Column(['title' => __('models/tenantManagementNews.fields.residence_city'), 'data' => 'residence_city']),
            'residence_city_area' => new Column(['title' => __('models/tenantManagementNews.fields.residence_city_area'), 'data' => 'residence_city_area']),
            'residence_addr' => new Column(['title' => __('models/tenantManagementNews.fields.residence_addr'), 'data' => 'residence_addr']),
            'mailing_city' => new Column(['title' => __('models/tenantManagementNews.fields.mailing_city'), 'data' => 'mailing_city']),
            'mailing_city_area' => new Column(['title' => __('models/tenantManagementNews.fields.mailing_city_area'), 'data' => 'mailing_city_area']),
            'mailing_addr' => new Column(['title' => __('models/tenantManagementNews.fields.mailing_addr'), 'data' => 'mailing_addr']),
            'is_have_other_subsidy' => new Column(['title' => __('models/tenantManagementNews.fields.is_have_other_subsidy'), 'data' => 'is_have_other_subsidy']),
            'isOtherSubsidy01' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy01'), 'data' => 'isOtherSubsidy01']),
            'isOtherSubsidy01Info' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy01Info'), 'data' => 'isOtherSubsidy01Info']),
            'isOtherSubsidy02' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy02'), 'data' => 'isOtherSubsidy02']),
            'isOtherSubsidy02Info' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy02Info'), 'data' => 'isOtherSubsidy02Info']),
            'isOtherSubsidy03' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy03'), 'data' => 'isOtherSubsidy03']),
            'isOtherSubsidy03Info' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy03Info'), 'data' => 'isOtherSubsidy03Info']),
            'isOtherSubsidy04' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy04'), 'data' => 'isOtherSubsidy04']),
            'isOtherSubsidy04Info' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy04Info'), 'data' => 'isOtherSubsidy04Info']),
            'isOtherSubsidy05' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy05'), 'data' => 'isOtherSubsidy05']),
            'isOtherSubsidy05Info' => new Column(['title' => __('models/tenantManagementNews.fields.isOtherSubsidy05Info'), 'data' => 'isOtherSubsidy05Info']),
            'legal_agent_num' => new Column(['title' => __('models/tenantManagementNews.fields.legal_agent_num'), 'data' => 'legal_agent_num']),
            'single_proxy_reason' => new Column(['title' => __('models/tenantManagementNews.fields.single_proxy_reason'), 'data' => 'single_proxy_reason']),
            'first_legal_agent_name' => new Column(['title' => __('models/tenantManagementNews.fields.first_legal_agent_name'), 'data' => 'first_legal_agent_name']),
            'first_legal_agent_tel' => new Column(['title' => __('models/tenantManagementNews.fields.first_legal_agent_tel'), 'data' => 'first_legal_agent_tel']),
            'first_legal_agent_phone' => new Column(['title' => __('models/tenantManagementNews.fields.first_legal_agent_phone'), 'data' => 'first_legal_agent_phone']),
            'first_legal_agent_addr' => new Column(['title' => __('models/tenantManagementNews.fields.first_legal_agent_addr'), 'data' => 'first_legal_agent_addr']),
            'second_legal_agent_name' => new Column(['title' => __('models/tenantManagementNews.fields.second_legal_agent_name'), 'data' => 'second_legal_agent_name']),
            'second_legal_agent_tel' => new Column(['title' => __('models/tenantManagementNews.fields.second_legal_agent_tel'), 'data' => 'second_legal_agent_tel']),
            'second_legal_agent_phone' => new Column(['title' => __('models/tenantManagementNews.fields.second_legal_agent_phone'), 'data' => 'second_legal_agent_phone']),
            'second_legal_agent_addr' => new Column(['title' => __('models/tenantManagementNews.fields.second_legal_agent_addr'), 'data' => 'second_legal_agent_addr']),
            'bank_name' => new Column(['title' => __('models/tenantManagementNews.fields.bank_name'), 'data' => 'bank_name']),
            'bank_branch_code' => new Column(['title' => __('models/tenantManagementNews.fields.bank_branch_code'), 'data' => 'bank_branch_code']),
            'bank_owner_name' => new Column(['title' => __('models/tenantManagementNews.fields.bank_owner_name'), 'data' => 'bank_owner_name']),
            'bank_account' => new Column(['title' => __('models/tenantManagementNews.fields.bank_account'), 'data' => 'bank_account']),
            'building_pattern_01' => new Column(['title' => __('models/tenantManagementNews.fields.building_pattern_01'), 'data' => 'building_pattern_01']),
            'building_pattern_02' => new Column(['title' => __('models/tenantManagementNews.fields.building_pattern_02'), 'data' => 'building_pattern_02']),
            'building_pattern_03' => new Column(['title' => __('models/tenantManagementNews.fields.building_pattern_03'), 'data' => 'building_pattern_03']),
            'building_pattern_04' => new Column(['title' => __('models/tenantManagementNews.fields.building_pattern_04'), 'data' => 'building_pattern_04']),
            'building_pattern_05' => new Column(['title' => __('models/tenantManagementNews.fields.building_pattern_05'), 'data' => 'building_pattern_05']),
            'building_type_01' => new Column(['title' => __('models/tenantManagementNews.fields.building_type_01'), 'data' => 'building_type_01']),
            'building_type_02' => new Column(['title' => __('models/tenantManagementNews.fields.building_type_02'), 'data' => 'building_type_02']),
            'building_type_03' => new Column(['title' => __('models/tenantManagementNews.fields.building_type_03'), 'data' => 'building_type_03']),
            'building_type_04' => new Column(['title' => __('models/tenantManagementNews.fields.building_type_04'), 'data' => 'building_type_04']),
            'building_use_area_01' => new Column(['title' => __('models/tenantManagementNews.fields.building_use_area_01'), 'data' => 'building_use_area_01']),
            'building_use_area_02' => new Column(['title' => __('models/tenantManagementNews.fields.building_use_area_02'), 'data' => 'building_use_area_02']),
            'building_use_area_03' => new Column(['title' => __('models/tenantManagementNews.fields.building_use_area_03'), 'data' => 'building_use_area_03']),
            'building_use_area_04' => new Column(['title' => __('models/tenantManagementNews.fields.building_use_area_04'), 'data' => 'building_use_area_04']),
            'building_use_area_05' => new Column(['title' => __('models/tenantManagementNews.fields.building_use_area_05'), 'data' => 'building_use_area_05']),
            'wish_floor_01' => new Column(['title' => __('models/tenantManagementNews.fields.wish_floor_01'), 'data' => 'wish_floor_01']),
            'wish_floor_02' => new Column(['title' => __('models/tenantManagementNews.fields.wish_floor_02'), 'data' => 'wish_floor_02']),
            'wish_floor_03' => new Column(['title' => __('models/tenantManagementNews.fields.wish_floor_03'), 'data' => 'wish_floor_03']),
            'wish_floor_04' => new Column(['title' => __('models/tenantManagementNews.fields.wish_floor_04'), 'data' => 'wish_floor_04']),
            'wish_floor_exact' => new Column(['title' => __('models/tenantManagementNews.fields.wish_floor_exact'), 'data' => 'wish_floor_exact']),
            'wish_room_num' => new Column(['title' => __('models/tenantManagementNews.fields.wish_room_num'), 'data' => 'wish_room_num']),
            'wish_living_rooms_num' => new Column(['title' => __('models/tenantManagementNews.fields.wish_living_rooms_num'), 'data' => 'wish_living_rooms_num']),
            'wish_bathrooms_num' => new Column(['title' => __('models/tenantManagementNews.fields.wish_bathrooms_num'), 'data' => 'wish_bathrooms_num']),
            'req_access_control' => new Column(['title' => __('models/tenantManagementNews.fields.req_access_control'), 'data' => 'req_access_control']),
            'rent_min' => new Column(['title' => __('models/tenantManagementNews.fields.rent_min'), 'data' => 'rent_min']),
            'building_offer_other' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_other'), 'data' => 'building_offer_other']),
            'building_offer_sofa' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_sofa'), 'data' => 'building_offer_sofa']),
            'building_offer_chair' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_chair'), 'data' => 'building_offer_chair']),
            'building_offer_desk' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_desk'), 'data' => 'building_offer_desk']),
            'building_offer_wardrobe' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_wardrobe'), 'data' => 'building_offer_wardrobe']),
            'building_offer_bed' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_bed'), 'data' => 'building_offer_bed']),
            'building_offer_natural_gas' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_natural_gas'), 'data' => 'building_offer_natural_gas']),
            'building_offer_washing_machine' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_washing_machine'), 'data' => 'building_offer_washing_machine']),
            'building_offer_internet' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_internet'), 'data' => 'building_offer_internet']),
            'building_offer_geyser' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_geyser'), 'data' => 'building_offer_geyser']),
            'building_offer_air_conditioner' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_air_conditioner'), 'data' => 'building_offer_air_conditioner']),
            'building_offer_cable' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_cable'), 'data' => 'building_offer_cable']),
            'building_offer_refrigerator' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_refrigerator'), 'data' => 'building_offer_refrigerator']),
            'building_offer_tv' => new Column(['title' => __('models/tenantManagementNews.fields.building_offer_tv'), 'data' => 'building_offer_tv']),
            'is_live_together' => new Column(['title' => __('models/tenantManagementNews.fields.is_live_together'), 'data' => 'is_live_together']),
            'is_live_together_desc' => new Column(['title' => __('models/tenantManagementNews.fields.is_live_together_desc'), 'data' => 'is_live_together_desc']),
            'rent_max' => new Column(['title' => __('models/tenantManagementNews.fields.rent_max'), 'data' => 'rent_max'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tenant_management_news_datatable_' . time();
    }
}
