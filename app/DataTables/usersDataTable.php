<?php

namespace App\DataTables;

use App\Models\users;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class usersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'users.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\users $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(users $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => new Column(['title' => __('models/users.fields.name'), 'data' => 'name']),
            'email' => new Column(['title' => __('models/users.fields.email'), 'data' => 'email']),
            'passport_english_name' => new Column(['title' => __('models/users.fields.passport_english_name'), 'data' => 'passport_english_name']),
            'english_name' => new Column(['title' => __('models/users.fields.english_name'), 'data' => 'english_name']),
            'birthday' => new Column(['title' => __('models/users.fields.birthday'), 'data' => 'birthday']),
            'id_no' => new Column(['title' => __('models/users.fields.id_no'), 'data' => 'id_no']),
            'residence_city' => new Column(['title' => __('models/users.fields.residence_city'), 'data' => 'residence_city']),
            'residence_city_area' => new Column(['title' => __('models/users.fields.residence_city_area'), 'data' => 'residence_city_area']),
            'residenceresidence_addr_addr' => new Column(['title' => __('models/users.fields.residenceresidence_addr_addr'), 'data' => 'residenceresidence_addr_addr']),
            'mailing_city' => new Column(['title' => __('models/users.fields.mailing_city'), 'data' => 'mailing_city']),
            'mailing_city_area' => new Column(['title' => __('models/users.fields.mailing_city_area'), 'data' => 'mailing_city_area']),
            'mailing_addr' => new Column(['title' => __('models/users.fields.mailing_addr'), 'data' => 'mailing_addr']),
            'tel' => new Column(['title' => __('models/users.fields.tel'), 'data' => 'tel']),
            'cellphone' => new Column(['title' => __('models/users.fields.cellphone'), 'data' => 'cellphone']),
            'company_cellphone' => new Column(['title' => __('models/users.fields.company_cellphone'), 'data' => 'company_cellphone']),
            'bank_name' => new Column(['title' => __('models/users.fields.bank_name'), 'data' => 'bank_name']),
            'bank_branch_code' => new Column(['title' => __('models/users.fields.bank_branch_code'), 'data' => 'bank_branch_code']),
            'bank_owner_name' => new Column(['title' => __('models/users.fields.bank_owner_name'), 'data' => 'bank_owner_name']),
            'bank_account' => new Column(['title' => __('models/users.fields.bank_account'), 'data' => 'bank_account']),
            'email_verified_at' => new Column(['title' => __('models/users.fields.email_verified_at'), 'data' => 'email_verified_at']),
            'password' => new Column(['title' => __('models/users.fields.password'), 'data' => 'password']),
            'role' => new Column(['title' => __('models/users.fields.role'), 'data' => 'role']),
            'remember_token' => new Column(['title' => __('models/users.fields.remember_token'), 'data' => 'remember_token']),
            'stoken' => new Column(['title' => __('models/users.fields.stoken'), 'data' => 'stoken'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'users_datatable_' . time();
    }
}
