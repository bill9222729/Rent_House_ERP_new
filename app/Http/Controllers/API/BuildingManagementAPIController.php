<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBuildingManagementAPIRequest;
use App\Http\Requests\API\UpdateBuildingManagementAPIRequest;
use App\Models\BuildingManagement;
use App\Repositories\BuildingManagementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BuildingManagementResource;
use Response;

/**
 * Class BuildingManagementController
 * @package App\Http\Controllers\API
 */

class BuildingManagementAPIController extends AppBaseController
{
    /** @var  BuildingManagementRepository */
    private $buildingManagementRepository;

    public function __construct(BuildingManagementRepository $buildingManagementRepo)
    {
        $this->buildingManagementRepository = $buildingManagementRepo;
    }

    /**
     * Display a listing of the BuildingManagement.
     * GET|HEAD /buildingManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $buildingManagements = $this->buildingManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            BuildingManagementResource::collection($buildingManagements),
            __('messages.retrieved', ['model' => __('models/buildingManagements.plural')])
        );
    }

    /**
     * Store a newly created BuildingManagement in storage.
     * POST /buildingManagements
     *
     * @param CreateBuildingManagementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBuildingManagementAPIRequest $request)
    {
        $input = $request->all();

        $buildingManagement = $this->buildingManagementRepository->create($input);

        return $this->sendResponse(
            new BuildingManagementResource($buildingManagement),
            __('messages.saved', ['model' => __('models/buildingManagements.singular')])
        );
    }

    /**
     * Display the specified BuildingManagement.
     * GET|HEAD /buildingManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BuildingManagement $buildingManagement */
        $buildingManagement = $this->buildingManagementRepository->find($id);

        if (empty($buildingManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/buildingManagements.singular')])
            );
        }

        return $this->sendResponse(
            new BuildingManagementResource($buildingManagement),
            __('messages.retrieved', ['model' => __('models/buildingManagements.singular')])
        );
    }

    /**
     * Update the specified BuildingManagement in storage.
     * PUT/PATCH /buildingManagements/{id}
     *
     * @param int $id
     * @param UpdateBuildingManagementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $input = $request->all();
       
        /** @var BuildingManagement $buildingManagement */
        $buildingManagement = $this->buildingManagementRepository->find($id);

        if (empty($buildingManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/buildingManagements.singular')])
            );
        }

        $buildingManagement = $this->buildingManagementRepository->update($input, $id);
        
      

        return $this->sendResponse(
            new BuildingManagementResource($buildingManagement),
            __('messages.updated', ['model' => __('models/buildingManagements.singular')])
        );
    }

    /**
     * Remove the specified BuildingManagement from storage.
     * DELETE /buildingManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BuildingManagement $buildingManagement */
        $buildingManagement = $this->buildingManagementRepository->find($id);

        if (empty($buildingManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/buildingManagements.singular')])
            );
        }

        $buildingManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/buildingManagements.singular')])
        );
    }
    
}
