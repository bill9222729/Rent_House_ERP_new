<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFixHistoryAPIRequest;
use App\Http\Requests\API\UpdateFixHistoryAPIRequest;
use App\Models\FixHistory;
use App\Repositories\FixHistoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\FixHistoryResource;
use Response;

/**
 * Class FixHistoryController
 * @package App\Http\Controllers\API
 */

class FixHistoryAPIController extends AppBaseController
{
    /** @var  FixHistoryRepository */
    private $fixHistoryRepository;

    public function __construct(FixHistoryRepository $fixHistoryRepo)
    {
        $this->fixHistoryRepository = $fixHistoryRepo;
    }

    /**
     * Display a listing of the FixHistory.
     * GET|HEAD /fixHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $fixHistories = $this->fixHistoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            FixHistoryResource::collection($fixHistories),
            __('messages.retrieved', ['model' => __('models/fixHistories.plural')])
        );
    }

    /**
     * Store a newly created FixHistory in storage.
     * POST /fixHistories
     *
     * @param CreateFixHistoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFixHistoryAPIRequest $request)
    {
        $input = $request->all();
        $input['pay_date'] = self::Format_TW_Date($input['pay_date']);
        $input['call_fix_date'] = self::Format_TW_Date($input['call_fix_date']);
        $input['check_date'] = self::Format_TW_Date($input['check_date']);
        $input['fix_date'] = self::Format_TW_Date($input['fix_date']);
        $input['fix_done'] = self::Format_TW_Date($input['fix_done']);
        $input['owner_start_date'] = self::Format_TW_Date($input['owner_start_date']);
        $input['owner_end_date'] = self::Format_TW_Date($input['owner_end_date']);
        $input['change_start_date'] = self::Format_TW_Date($input['change_start_date']);
        $input['change_end_date'] = self::Format_TW_Date($input['change_end_date']);

        $fixHistory = $this->fixHistoryRepository->create($input);

        return $this->sendResponse(
            new FixHistoryResource($fixHistory),
            __('messages.saved', ['model' => __('models/fixHistories.singular')])
        );
    }

    /**
     * Display the specified FixHistory.
     * GET|HEAD /fixHistories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FixHistory $fixHistory */
        $fixHistory = $this->fixHistoryRepository->find($id);

        if (empty($fixHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixHistories.singular')])
            );
        }

        return $this->sendResponse(
            new FixHistoryResource($fixHistory),
            __('messages.retrieved', ['model' => __('models/fixHistories.singular')])
        );
    }

    /**
     * Update the specified FixHistory in storage.
     * PUT/PATCH /fixHistories/{id}
     *
     * @param int $id
     * @param UpdateFixHistoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFixHistoryAPIRequest $request)
    {
        /** @var FixHistory $fixHistory */
        $fixHistory = $this->fixHistoryRepository->find($id);

        if (empty($fixHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixHistories.singular')])
            );
        }
        $input = $request->all();
        $input['pay_date'] = self::Format_TW_Date($input['pay_date']);
        $input['call_fix_date'] = self::Format_TW_Date($input['call_fix_date']);
        $input['check_date'] = self::Format_TW_Date($input['check_date']);
        $input['fix_date'] = self::Format_TW_Date($input['fix_date']);
        $input['fix_done'] = self::Format_TW_Date($input['fix_done']);
        $input['owner_start_date'] = self::Format_TW_Date($input['owner_start_date']);
        $input['owner_end_date'] = self::Format_TW_Date($input['owner_end_date']);
        $input['change_start_date'] = self::Format_TW_Date($input['change_start_date']);
        $input['change_end_date'] = self::Format_TW_Date($input['change_end_date']);

        $fixHistory = $this->fixHistoryRepository->update($input, $id);

        return $this->sendResponse(
            new FixHistoryResource($fixHistory),
            __('messages.updated', ['model' => __('models/fixHistories.singular')])
        );
    }

    /**
     * Remove the specified FixHistory from storage.
     * DELETE /fixHistories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var FixHistory $fixHistory */
        $fixHistory = $this->fixHistoryRepository->find($id);

        if (empty($fixHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixHistories.singular')])
            );
        }

        $fixHistory->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/fixHistories.singular')])
        );
    }

    private static function Format_TW_Date($date_string){
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list,"/")) {
            $date_list = explode("/",$date_list);
        }else if(strpos($date_list,".")){
            $date_list = explode(".",$date_list);
        }else if(strpos($date_list,"-")){
            $date_list = explode("-",$date_list);
        }else{
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        if (count($date_list) == 2) {
            $date = $date_Y . "-" . $date_list[1] . "-01";
        }else{
            $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        }
        return $date;
    }
}
