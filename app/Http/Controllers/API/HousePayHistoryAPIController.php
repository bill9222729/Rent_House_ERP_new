<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHousePayHistoryAPIRequest;
use App\Http\Requests\API\UpdateHousePayHistoryAPIRequest;
use App\Models\HousePayHistory;
use App\Repositories\HousePayHistoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\HousePayHistoryResource;
use Response;

/**
 * Class HousePayHistoryController
 * @package App\Http\Controllers\API
 */

class HousePayHistoryAPIController extends AppBaseController
{
    /** @var  HousePayHistoryRepository */
    private $housePayHistoryRepository;

    public function __construct(HousePayHistoryRepository $housePayHistoryRepo)
    {
        $this->housePayHistoryRepository = $housePayHistoryRepo;
    }

    /**
     * Display a listing of the HousePayHistory.
     * GET|HEAD /housePayHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $housePayHistories = $this->housePayHistoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            HousePayHistoryResource::collection($housePayHistories),
            __('messages.retrieved', ['model' => __('models/housePayHistories.plural')])
        );
    }

    /**
     * Store a newly created HousePayHistory in storage.
     * POST /housePayHistories
     *
     * @param CreateHousePayHistoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHousePayHistoryAPIRequest $request)
    {
        $input = $request->all();

        $housePayHistory = $this->housePayHistoryRepository->create($input);

        return $this->sendResponse(
            new HousePayHistoryResource($housePayHistory),
            __('messages.saved', ['model' => __('models/housePayHistories.singular')])
        );
    }

    /**
     * Display the specified HousePayHistory.
     * GET|HEAD /housePayHistories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HousePayHistory $housePayHistory */
        $housePayHistory = $this->housePayHistoryRepository->find($id);

        if (empty($housePayHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/housePayHistories.singular')])
            );
        }

        return $this->sendResponse(
            new HousePayHistoryResource($housePayHistory),
            __('messages.retrieved', ['model' => __('models/housePayHistories.singular')])
        );
    }

    /**
     * Update the specified HousePayHistory in storage.
     * PUT/PATCH /housePayHistories/{id}
     *
     * @param int $id
     * @param UpdateHousePayHistoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHousePayHistoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var HousePayHistory $housePayHistory */
        $housePayHistory = $this->housePayHistoryRepository->find($id);

        if (empty($housePayHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/housePayHistories.singular')])
            );
        }

        $housePayHistory = $this->housePayHistoryRepository->update($input, $id);

        return $this->sendResponse(
            new HousePayHistoryResource($housePayHistory),
            __('messages.updated', ['model' => __('models/housePayHistories.singular')])
        );
    }

    /**
     * Remove the specified HousePayHistory from storage.
     * DELETE /housePayHistories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HousePayHistory $housePayHistory */
        $housePayHistory = $this->housePayHistoryRepository->find($id);

        if (empty($housePayHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/housePayHistories.singular')])
            );
        }

        $housePayHistory->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/housePayHistories.singular')])
        );
    }
}
