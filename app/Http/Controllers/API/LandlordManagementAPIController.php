<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLandlordManagementAPIRequest;
use App\Http\Requests\API\UpdateLandlordManagementAPIRequest;
use App\Models\LandlordManagement;
use App\Repositories\LandlordManagementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\LandlordManagementResource;
use Response;

/**
 * Class LandlordManagementController
 * @package App\Http\Controllers\API
 */

class LandlordManagementAPIController extends AppBaseController
{
    /** @var  LandlordManagementRepository */
    private $landlordManagementRepository;

    public function __construct(LandlordManagementRepository $landlordManagementRepo)
    {
        $this->landlordManagementRepository = $landlordManagementRepo;
    }

    /**
     * Display a listing of the LandlordManagement.
     * GET|HEAD /landlordManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $landlordManagements = $this->landlordManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            LandlordManagementResource::collection($landlordManagements),
            __('messages.retrieved', ['model' => __('models/landlordManagements.plural')])
        );
    }

    /**
     * Store a newly created LandlordManagement in storage.
     * POST /landlordManagements
     *
     * @param CreateLandlordManagementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLandlordManagementAPIRequest $request)
    {
        $input = $request->all();

        $landlordManagement = $this->landlordManagementRepository->create($input);

        return $this->sendResponse(
            new LandlordManagementResource($landlordManagement),
            __('messages.saved', ['model' => __('models/landlordManagements.singular')])
        );
    }

    /**
     * Display the specified LandlordManagement.
     * GET|HEAD /landlordManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LandlordManagement $landlordManagement */
        $landlordManagement = $this->landlordManagementRepository->find($id);

        if (empty($landlordManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/landlordManagements.singular')])
            );
        }

        return $this->sendResponse(
            new LandlordManagementResource($landlordManagement),
            __('messages.retrieved', ['model' => __('models/landlordManagements.singular')])
        );
    }

    /**
     * Update the specified LandlordManagement in storage.
     * PUT/PATCH /landlordManagements/{id}
     *
     * @param int $id
     * @param UpdateLandlordManagementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLandlordManagementAPIRequest $request)
    {
        $input = $request->all();

        /** @var LandlordManagement $landlordManagement */
        $landlordManagement = $this->landlordManagementRepository->find($id);

        if (empty($landlordManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/landlordManagements.singular')])
            );
        }

        $landlordManagement = $this->landlordManagementRepository->update($input, $id);

        return $this->sendResponse(
            new LandlordManagementResource($landlordManagement),
            __('messages.updated', ['model' => __('models/landlordManagements.singular')])
        );
    }

    /**
     * Remove the specified LandlordManagement from storage.
     * DELETE /landlordManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LandlordManagement $landlordManagement */
        $landlordManagement = $this->landlordManagementRepository->find($id);

        if (empty($landlordManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/landlordManagements.singular')])
            );
        }

        $landlordManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/landlordManagements.singular')])
        );
    }
}
