<?php
namespace App\Http\Controllers\Api;

use App\Models\TenantManagement;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LINE\LINEBot;
use LINE\LINEBot\Constant\HTTPHeader;
use LINE\LINEBot\SignatureValidator;
use LINE\LINEBot\HTTPClient\CurlHTTPClient;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use Exception;
use Illuminate\Support\Facades\Log;

class LineWebhookController extends Controller
{

    protected $lineAccessToken;
    protected $lineChannelSecret;

    public function __construct()
    {
        // $this->lineAccessToken = env("LINE_ACCESS_TOKEN");
        // $this->lineChannelSecret = env("LINE_CHANNEL_SECRET");
        // $this->app_url = env("APP_URL");

        // Laravel版本因素，目前.env讀取有問題，所以繞道config讀取

        $this->lineAccessToken = config("app.LINE_ACCESS_TOKEN");
        $this->lineChannelSecret = config("app.LINE_CHANNEL_SECRET");
        $this->app_url = config("app.url");
        // dd($this->lineAccessToken);
    }

    public function webhook (Request $request)
    {

        $signature = $request->headers->get(HTTPHeader::LINE_SIGNATURE);
        if (!SignatureValidator::validateSignature($request->getContent(), $this->lineChannelSecret, $signature)) {

            return;
        }

        $httpClient = new CurlHTTPClient ($this->lineAccessToken);
        $lineBot = new LINEBot($httpClient, ['channelSecret' => $this->lineChannelSecret]);

        try {

            $events = $lineBot->parseEventRequest($request->getContent(), $signature);
            $type = json_decode($request->getContent())->events[0]->type;
            if ($type == 'message') {
                foreach ($events as $event) {
                    $replyToken = $event->getReplyToken();
                    $uid = $event->getUserId();
                    $text = $event->getText();// 得到使用者輸入

                    $tenant = TenantManagement::where('line_uid', $uid)->first();
                    if ($tenant) {
                        if ($text == '索取門房密碼')
                        {
                            $lineBot->replyText($replyToken, $this->create_house_password());// 回復使用者輸入
                        }else {
                            $lineBot->replyText($replyToken, "您好 $tenant->lessee_name");// 回復使用者輸入
                        }
                    } else {
                        if (is_numeric($text)) {
                            $tenant = TenantManagement::where('ref_code', $text)->first();
                            if ($tenant) {
                                $lineBot->replyText($replyToken, "您好 $tenant->lessee_name");// 回復使用者輸入
                                $tenant->line_uid = $uid;
                                $tenant->save();
                            }else {
                                $lineBot->replyText($replyToken, "代碼錯誤");
                            }
                        }else {
                            $lineBot->replyText($replyToken, "請輸入您的租客代碼");
                        }
                    }

                    //$textMessage = new TextMessageBuilder("你好");
                    //$lineBot->replyMessage($replyToken, $textMessage);
                }
            }else if($type == 'join') {
//                $lineBot->replyText($replyToken, $this->message_event_join());
            }
        } catch (Exception $e) {

            return;
        }

        return;
    }

    public function createRichMenu (Request $request)
    {

        $httpClient = new CurlHTTPClient ($this->lineAccessToken);
        $lineBot = new LINEBot($httpClient, ['channelSecret' => $this->lineChannelSecret]);
        $imagePath = $this->app_url . '/img/echouse-LOGO.png';
        $contentType = 'image/png';

        $size = new \LINE\LINEBot\RichMenuBuilder\RichMenuSizeBuilder(900,1958);
        // $password_boundsBuilder = new \LINE\LINEBot\RichMenuBuilder\RichMenuAreaBoundsBuilder(1250, 1,1250,1683);
        // $house_boundsBuilder = new \LINE\LINEBot\RichMenuBuilder\RichMenuAreaBoundsBuilder(1, 1,1958,900);
        $msg_boundsBuilder = new \LINE\LINEBot\RichMenuBuilder\RichMenuAreaBoundsBuilder(1, 1,1958,900);
        // 參考：
        // https://line.github.io/line-bot-sdk-php/class-LINE.LINEBot.RichMenuBuilder.RichMenuAreaBoundsBuilder.html

        // $password_actionBuilder = new \LINE\LINEBot\TemplateActionBuilder\MessageTemplateActionBuilder('密碼', "索取門房密碼");
        // $house_actionBuilder = new \LINE\LINEBot\TemplateActionBuilder\UriTemplateActionBuilder('訂房', "https://www.bbnet.com.tw/winninn_tn2/reserve/");
        $msg_actionBuilder = new \LINE\LINEBot\TemplateActionBuilder\MessageTemplateActionBuilder('text', "請輸入您的租客代碼");
        // 參考：
        // https://line.github.io/line-bot-sdk-php/interface-LINE.LINEBot.TemplateActionBuilder.html

        // $password_area = new  \LINE\LINEBot\RichMenuBuilder\RichMenuAreaBuilder($password_boundsBuilder, $password_actionBuilder);
        // $house_area = new  \LINE\LINEBot\RichMenuBuilder\RichMenuAreaBuilder($house_boundsBuilder, $house_actionBuilder);
        $msg_area = new  \LINE\LINEBot\RichMenuBuilder\RichMenuAreaBuilder($msg_boundsBuilder, $msg_actionBuilder);
//        dd($area);
        // $richMenuBuilder = new \LINE\LINEBot\RichMenuBuilder($size, false, "Nice richmenu", "chatBarText", [$house_area, $password_area]);
        $richMenuBuilder = new \LINE\LINEBot\RichMenuBuilder($size, false, "Nice richmenu", "易居管理顧問股份有限公司", [$msg_area]);
//        dd($richMenuBuilder);
        $response = $lineBot->createRichMenu($richMenuBuilder);
    //    dd($response);
        $richmenu = json_decode($response->getRawBody(), true)["richMenuId"];
//        dd($richmenu);
        // 上面這些有得沒得做完之後會拿到 richmenuID 再用這個 id 去做下面上傳圖片跟 設定全部user 默認的 菜單
        // 參考 https://developers.line.biz/zh-hant/docs/messaging-api/using-rich-menus/#creating-a-rich-menu-using-the-messaging-api
        $upload_image_url = "https://api-data.line.me/v2/bot/richmenu/{$richmenu}/content";
        // dd($upload_image_url);
        $clinet = new Client();
        // dd($upload_image_url);
        $res = $clinet->post($upload_image_url,
            ['headers' =>
                [
                    'Authorization' => "Bearer {$this->lineAccessToken}",
                    'Content-Type' => $contentType
                ],
                'body' =>  file_get_contents($imagePath)

        ]);

        $link_menu_to_all_user_url = "https://api.line.me/v2/bot/user/all/richmenu/{$richmenu}";
        $res = $clinet->post($link_menu_to_all_user_url,
            ['headers' =>
                [
                    'Authorization' => "Bearer {$this->lineAccessToken}",
                    'Content-Type' => 'application/json'
                ]
            ]);
        return response()->json($res->getBody());
    }
    public function createRichMenuForUser(Request $request, $line_user_id)
    {

        //這部分製作特製的菜單都跟上面那個function一樣只要在最後 link user 菜單時要指定哪個user
        // 參考https://developers.line.biz/en/reference/messaging-api/#link-rich-menu-to-user 裡面也有 link 多個 uer的飯ㄈㄧˋ
        $clinet = new Client();
        $link_menu_to_user_url = "https://api.line.me/v2/bot/user/{$line_user_id}}/richmenu/{$richmenu}";
        $res = $clinet->post($link_menu_to_user_url,
            ['headers' =>
                [
                    'Authorization' => "Bearer {$this->lineAccessToken}",
                    'Content-Type' => 'application/json'
                ]
            ]);

    }
    public function message_event_join (){
        return '歡迎加入';
    }

    private function create_house_password()
    {
        return rand(1000000, 9999999);
    }

    public function sendMessage(Request $request)
    {
        $line_uid = $request->get("line_uid");
        $httpClient = new CurlHTTPClient ($this->lineAccessToken);
        $lineBot = new LINEBot($httpClient, ['channelSecret' => $this->lineChannelSecret]);

        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($request->get('message'));
        if ($line_uid) {
            $lineBot->pushMessage($line_uid, $textMessageBuilder);
            return response()->json('ok');

        }
        $tenants = TenantManagement::where('line_uid', '!=', null)->get();
        foreach ($tenants as $tenant) {
            try {
                $lineBot->pushMessage($tenant->line_uid, $textMessageBuilder);
            } catch (Exception $e) {
                continue;
            }
        }
        return response()->json('ok');

    }

}
