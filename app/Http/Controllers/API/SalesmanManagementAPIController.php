<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSalesmanManagementAPIRequest;
use App\Http\Requests\API\UpdateSalesmanManagementAPIRequest;
use App\Models\SalesmanManagement;
use App\Repositories\SalesmanManagementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\SalesmanManagementResource;
use Response;

/**
 * Class SalesmanManagementController
 * @package App\Http\Controllers\API
 */

class SalesmanManagementAPIController extends AppBaseController
{
    /** @var  SalesmanManagementRepository */
    private $salesmanManagementRepository;

    public function __construct(SalesmanManagementRepository $salesmanManagementRepo)
    {
        $this->salesmanManagementRepository = $salesmanManagementRepo;
    }

    /**
     * Display a listing of the SalesmanManagement.
     * GET|HEAD /salesmanManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $salesmanManagements = $this->salesmanManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            SalesmanManagementResource::collection($salesmanManagements),
            __('messages.retrieved', ['model' => __('models/salesmanManagements.plural')])
        );
    }

    /**
     * Store a newly created SalesmanManagement in storage.
     * POST /salesmanManagements
     *
     * @param CreateSalesmanManagementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSalesmanManagementAPIRequest $request)
    {
        $input = $request->all();

        $salesmanManagement = $this->salesmanManagementRepository->create($input);

        return $this->sendResponse(
            new SalesmanManagementResource($salesmanManagement),
            __('messages.saved', ['model' => __('models/salesmanManagements.singular')])
        );
    }

    /**
     * Display the specified SalesmanManagement.
     * GET|HEAD /salesmanManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SalesmanManagement $salesmanManagement */
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/salesmanManagements.singular')])
            );
        }

        return $this->sendResponse(
            new SalesmanManagementResource($salesmanManagement),
            __('messages.retrieved', ['model' => __('models/salesmanManagements.singular')])
        );
    }

    /**
     * Update the specified SalesmanManagement in storage.
     * PUT/PATCH /salesmanManagements/{id}
     *
     * @param int $id
     * @param UpdateSalesmanManagementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSalesmanManagementAPIRequest $request)
    {
        $input = $request->all();

        /** @var SalesmanManagement $salesmanManagement */
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/salesmanManagements.singular')])
            );
        }

        $salesmanManagement = $this->salesmanManagementRepository->update($input, $id);

        return $this->sendResponse(
            new SalesmanManagementResource($salesmanManagement),
            __('messages.updated', ['model' => __('models/salesmanManagements.singular')])
        );
    }

    /**
     * Remove the specified SalesmanManagement from storage.
     * DELETE /salesmanManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SalesmanManagement $salesmanManagement */
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/salesmanManagements.singular')])
            );
        }

        $salesmanManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/salesmanManagements.singular')])
        );
    }
}
