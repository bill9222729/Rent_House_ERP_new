<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTenantManagementAPIRequest;
use App\Http\Requests\API\UpdateTenantManagementAPIRequest;
use App\Models\TenantManagement;
use App\Repositories\TenantManagementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TenantManagementResource;
use Response;

/**
 * Class TenantManagementController
 * @package App\Http\Controllers\API
 */

class TenantManagementAPIController extends AppBaseController
{
    /** @var  TenantManagementRepository */
    private $tenantManagementRepository;

    public function __construct(TenantManagementRepository $tenantManagementRepo)
    {
        $this->tenantManagementRepository = $tenantManagementRepo;
    }

    /**
     * Display a listing of the TenantManagement.
     * GET|HEAD /tenantManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tenantManagements = $this->tenantManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            TenantManagementResource::collection($tenantManagements),
            __('messages.retrieved', ['model' => __('models/tenantManagements.plural')])
        );
    }

    /**
     * Store a newly created TenantManagement in storage.
     * POST /tenantManagements
     *
     * @param CreateTenantManagementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTenantManagementAPIRequest $request)
    {
        $input = $request->all();

        $tenantManagement = $this->tenantManagementRepository->create($input);

        return $this->sendResponse(
            new TenantManagementResource($tenantManagement),
            __('messages.saved', ['model' => __('models/tenantManagements.singular')])
        );
    }

    /**
     * Display the specified TenantManagement.
     * GET|HEAD /tenantManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TenantManagement $tenantManagement */
        $tenantManagement = $this->tenantManagementRepository->find($id);

        if (empty($tenantManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantManagements.singular')])
            );
        }

        return $this->sendResponse(
            new TenantManagementResource($tenantManagement),
            __('messages.retrieved', ['model' => __('models/tenantManagements.singular')])
        );
    }

    /**
     * Update the specified TenantManagement in storage.
     * PUT/PATCH /tenantManagements/{id}
     *
     * @param int $id
     * @param UpdateTenantManagementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var TenantManagement $tenantManagement */
        // $tenantManagement = $this->tenantManagementRepository->find($id);

        // if (empty($tenantManagement)) {
        //     return $this->sendError(
        //         __('messages.not_found', ['model' => __('models/tenantManagements.singular')])
        //     );
        // }

        $tenantManagement = $this->tenantManagementRepository->update($input, $id);

        return $this->sendResponse(
            new TenantManagementResource($tenantManagement),
            __('messages.updated', ['model' => __('models/tenantManagements.singular')])
        );
    }

    /**
     * Remove the specified TenantManagement from storage.
     * DELETE /tenantManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TenantManagement $tenantManagement */
        $tenantManagement = $this->tenantManagementRepository->find($id);

        if (empty($tenantManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantManagements.singular')])
            );
        }

        $tenantManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/tenantManagements.singular')])
        );
    }
}
