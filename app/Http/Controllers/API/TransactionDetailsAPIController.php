<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTransactionDetailsAPIRequest;
use App\Http\Requests\API\UpdateTransactionDetailsAPIRequest;
use App\Models\TransactionDetails;
use App\Repositories\TransactionDetailsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TransactionDetailsResource;
use Response;

/**
 * Class TransactionDetailsController
 * @package App\Http\Controllers\API
 */

class TransactionDetailsAPIController extends AppBaseController
{
    /** @var  TransactionDetailsRepository */
    private $transactionDetailsRepository;

    public function __construct(TransactionDetailsRepository $transactionDetailsRepo)
    {
        $this->transactionDetailsRepository = $transactionDetailsRepo;
    }

    /**
     * Display a listing of the TransactionDetails.
     * GET|HEAD /transactionDetails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $transactionDetails = $this->transactionDetailsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            TransactionDetailsResource::collection($transactionDetails),
            __('messages.retrieved', ['model' => __('models/transactionDetails.plural')])
        );
    }

    /**
     * Store a newly created TransactionDetails in storage.
     * POST /transactionDetails
     *
     * @param CreateTransactionDetailsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionDetailsAPIRequest $request)
    {
        $input = $request->all();

        $transactionDetails = $this->transactionDetailsRepository->create($input);

        return $this->sendResponse(
            new TransactionDetailsResource($transactionDetails),
            __('messages.saved', ['model' => __('models/transactionDetails.singular')])
        );
    }

    /**
     * Display the specified TransactionDetails.
     * GET|HEAD /transactionDetails/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TransactionDetails $transactionDetails */
        $transactionDetails = $this->transactionDetailsRepository->find($id);

        if (empty($transactionDetails)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/transactionDetails.singular')])
            );
        }

        return $this->sendResponse(
            new TransactionDetailsResource($transactionDetails),
            __('messages.retrieved', ['model' => __('models/transactionDetails.singular')])
        );
    }

    /**
     * Update the specified TransactionDetails in storage.
     * PUT/PATCH /transactionDetails/{id}
     *
     * @param int $id
     * @param UpdateTransactionDetailsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionDetailsAPIRequest $request)
    {
        $input = $request->all();

        /** @var TransactionDetails $transactionDetails */
        $transactionDetails = $this->transactionDetailsRepository->find($id);

        if (empty($transactionDetails)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/transactionDetails.singular')])
            );
        }

        $transactionDetails = $this->transactionDetailsRepository->update($input, $id);

        return $this->sendResponse(
            new TransactionDetailsResource($transactionDetails),
            __('messages.updated', ['model' => __('models/transactionDetails.singular')])
        );
    }

    /**
     * Remove the specified TransactionDetails from storage.
     * DELETE /transactionDetails/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TransactionDetails $transactionDetails */
        $transactionDetails = $this->transactionDetailsRepository->find($id);

        if (empty($transactionDetails)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/transactionDetails.singular')])
            );
        }

        $transactionDetails->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/transactionDetails.singular')])
        );
    }
}
