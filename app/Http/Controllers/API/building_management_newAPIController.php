<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createbuilding_management_newAPIRequest;
use App\Http\Requests\API\Updatebuilding_management_newAPIRequest;
use App\Models\building_management_new;
use App\Repositories\building_management_newRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\building_management_newResource;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Response;
use DB;

/**
 * Class building_management_newController
 * @package App\Http\Controllers\API
 */

class building_management_newAPIController extends AppBaseController
{
    /** @var  building_management_newRepository */
    private $buildingManagementNewRepository;

    public function __construct(building_management_newRepository $buildingManagementNewRepo)
    {
        $this->buildingManagementNewRepository = $buildingManagementNewRepo;
    }

    /**
     * Display a listing of the building_management_new.
     * GET|HEAD /buildingManagementNews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $buildingManagementNews = $this->buildingManagementNewRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $input = $request->all();

        $data = DB::table("building_management_new")
            ->leftJoin("users as sales_user", function ($join) {
                $join->on("building_management_new.sales_id", "=", "sales_user.id");
            })
            ->leftJoin("users as creator_user", function ($join) {
                $join->on("building_management_new.creator", "=", "creator_user.id");
            })
            ->select(
                "building_management_new.*",
                "sales_user.name as sales_name",
                "creator_user.name as creator_name",
                DB::raw("concat (building_management_new.entrusted_lease_start_date, '~', building_management_new.entrusted_lease_end_date) as entrusted_lease_date"),
                DB::raw("concat (building_management_new.entrusted_management_start_date, '~', building_management_new.entrusted_management_end_date) as entrusted_management_date"),
                DB::raw("(SELECT name FROM city_area WHERE value=building_management_new.building_located_city_area) AS building_located_city_area_name"),
                DB::raw("(SELECT name FROM city_area WHERE value=building_management_new.building_address_city_area) AS building_address_city_area_name")
            )->whereNull("building_management_new.deleted_at");

        if(count($input) > 0) {
            foreach ($input as $key => $value) {
                if ($key == "_") {
                    continue;
                }
                $data = $data->where("building_management_new.".$key, "=", $value);
            }
        }

        $data = $data->get();

        return $this->sendResponse(
            $data,
            __('messages.retrieved', ['model' => __('models/buildingManagementNews.plural')])
        );

        // return $this->sendResponse(
        //     building_management_newResource::collection($buildingManagementNews),
        //     __('messages.retrieved', ['model' => __('models/buildingManagementNews.plural')])
        // );
    }

    /**
     * Store a newly created building_management_new in storage.
     * POST /buildingManagementNews
     *
     * @param Createbuilding_management_newAPIRequest $request
     *
     * @return Response
     */
    public function store(Createbuilding_management_newAPIRequest $request)
    {
        $input = $request->all();

        $buildingManagementNew = $this->buildingManagementNewRepository->create($input);

        return $this->sendResponse(
            new building_management_newResource($buildingManagementNew),
            __('messages.saved', ['model' => __('models/buildingManagementNews.singular')])
        );
    }

    /**
     * Display the specified building_management_new.
     * GET|HEAD /buildingManagementNews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var building_management_new $buildingManagementNew */
        $buildingManagementNew = $this->buildingManagementNewRepository->find($id);

        if (empty($buildingManagementNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/buildingManagementNews.singular')])
            );
        }

        return $this->sendResponse(
            new building_management_newResource($buildingManagementNew),
            __('messages.retrieved', ['model' => __('models/buildingManagementNews.singular')])
        );
    }

    /**
     * Update the specified building_management_new in storage.
     * PUT/PATCH /buildingManagementNews/{id}
     *
     * @param int $id
     * @param Updatebuilding_management_newAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatebuilding_management_newAPIRequest $request)
    {
        $input = $request->all();

        /** @var building_management_new $buildingManagementNew */
        $buildingManagementNew = $this->buildingManagementNewRepository->find($id);

        if (empty($buildingManagementNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/buildingManagementNews.singular')])
            );
        }

        $buildingManagementNew = $this->buildingManagementNewRepository->update($input, $id);

        return $this->sendResponse(
            new building_management_newResource($buildingManagementNew),
            __('messages.updated', ['model' => __('models/buildingManagementNews.singular')])
        );
    }

    /**
     * Remove the specified building_management_new from storage.
     * DELETE /buildingManagementNews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var building_management_new $buildingManagementNew */
        $buildingManagementNew = $this->buildingManagementNewRepository->find($id);

        if (empty($buildingManagementNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/buildingManagementNews.singular')])
            );
        }

        $buildingManagementNew->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/buildingManagementNews.singular')])
        );
    }

    // 上傳檔案(目前無用，已移至app\Http\Controllers\API\file_managmentAPIController.php)
    public function upload(Request $request)
    {
        if ($request->isMethod('POST')) {
            // var_dump($_FILES);
            $file = $request->file('source');
            dd($request);
            //檔案是否上傳成功
            if ($file->isValid()) {
                //獲取原檔名
                $originalName = $file->getClientOriginalName();
                //獲取檔案拓展名
                $ext = $file->getClientOriginalExtension();
                $type = $file->getClientMimeType();
                //獲取檔案臨時絕對路徑
                $realPath = $file->getRealPath();
                //自定義檔案儲存的名稱
                $fileName = date('Y-m-d-H-i-s') . '-' . uniqid() . '.' . $ext;

                $bool =  \Storage::disk('buildings_file')->put($fileName, file_get_contents($realPath));

                // var_dump($bool);

                return $bool;
            }
            exit;
        }
        return "400";
    }
}
