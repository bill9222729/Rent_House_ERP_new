<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createchange_historyAPIRequest;
use App\Http\Requests\API\Updatechange_historyAPIRequest;
use App\Models\change_history;
use App\Repositories\change_historyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\change_historyResource;
use Response;
use DB;

/**
 * Class change_historyController
 * @package App\Http\Controllers\API
 */

class change_historyAPIController extends AppBaseController
{
    /** @var  change_historyRepository */
    private $changeHistoryRepository;

    public function __construct(change_historyRepository $changeHistoryRepo)
    {
        $this->changeHistoryRepository = $changeHistoryRepo;
    }

    /**
     * Display a listing of the change_history.
     * GET|HEAD /changeHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $changeHistories = $this->changeHistoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $data = DB::table("change_history")
            ->leftJoin("users", function ($join) {
                $join->on("change_history.edit_member_id", "=", "users.id");
            })
            ->select("change_history.*", "users.name as edit_member_name")
            ->where("building_id", "=", $request->get("building_id"))
            ->get();

        return $this->sendResponse(
            $data,
            __('messages.retrieved', ['model' => __('models/changeHistories.plural')])
        );

        // return $this->sendResponse(
        //     change_historyResource::collection($changeHistories),
        //     __('messages.retrieved', ['model' => __('models/changeHistories.plural')])
        // );
    }

    /**
     * Store a newly created change_history in storage.
     * POST /changeHistories
     *
     * @param Createchange_historyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createchange_historyAPIRequest $request)
    {
        $input = $request->all();

        $changeHistory = $this->changeHistoryRepository->create($input);

        return $this->sendResponse(
            new change_historyResource($changeHistory),
            __('messages.saved', ['model' => __('models/changeHistories.singular')])
        );
    }

    /**
     * Display the specified change_history.
     * GET|HEAD /changeHistories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var change_history $changeHistory */
        $changeHistory = $this->changeHistoryRepository->find($id);

        if (empty($changeHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/changeHistories.singular')])
            );
        }

        return $this->sendResponse(
            new change_historyResource($changeHistory),
            __('messages.retrieved', ['model' => __('models/changeHistories.singular')])
        );
    }

    /**
     * Update the specified change_history in storage.
     * PUT/PATCH /changeHistories/{id}
     *
     * @param int $id
     * @param Updatechange_historyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatechange_historyAPIRequest $request)
    {
        $input = $request->all();

        /** @var change_history $changeHistory */
        $changeHistory = $this->changeHistoryRepository->find($id);

        if (empty($changeHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/changeHistories.singular')])
            );
        }

        $changeHistory = $this->changeHistoryRepository->update($input, $id);

        return $this->sendResponse(
            new change_historyResource($changeHistory),
            __('messages.updated', ['model' => __('models/changeHistories.singular')])
        );
    }

    /**
     * Remove the specified change_history from storage.
     * DELETE /changeHistories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var change_history $changeHistory */
        $changeHistory = $this->changeHistoryRepository->find($id);

        if (empty($changeHistory)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/changeHistories.singular')])
            );
        }

        $changeHistory->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/changeHistories.singular')])
        );
    }
}
