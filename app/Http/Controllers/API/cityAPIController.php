<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatecityAPIRequest;
use App\Http\Requests\API\UpdatecityAPIRequest;
use App\Models\city;
use App\Repositories\cityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\cityResource;
use Response;

/**
 * Class cityController
 * @package App\Http\Controllers\API
 */

class cityAPIController extends AppBaseController
{
    /** @var  cityRepository */
    private $cityRepository;

    public function __construct(cityRepository $cityRepo)
    {
        $this->cityRepository = $cityRepo;
    }

    /**
     * Display a listing of the city.
     * GET|HEAD /cities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $cities = $this->cityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            cityResource::collection($cities),
            __('messages.retrieved', ['model' => __('models/cities.plural')])
        );
    }

    /**
     * Store a newly created city in storage.
     * POST /cities
     *
     * @param CreatecityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatecityAPIRequest $request)
    {
        $input = $request->all();

        $city = $this->cityRepository->create($input);

        return $this->sendResponse(
            new cityResource($city),
            __('messages.saved', ['model' => __('models/cities.singular')])
        );
    }

    /**
     * Display the specified city.
     * GET|HEAD /cities/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var city $city */
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/cities.singular')])
            );
        }

        return $this->sendResponse(
            new cityResource($city),
            __('messages.retrieved', ['model' => __('models/cities.singular')])
        );
    }

    /**
     * Update the specified city in storage.
     * PUT/PATCH /cities/{id}
     *
     * @param int $id
     * @param UpdatecityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecityAPIRequest $request)
    {
        $input = $request->all();

        /** @var city $city */
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/cities.singular')])
            );
        }

        $city = $this->cityRepository->update($input, $id);

        return $this->sendResponse(
            new cityResource($city),
            __('messages.updated', ['model' => __('models/cities.singular')])
        );
    }

    /**
     * Remove the specified city from storage.
     * DELETE /cities/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var city $city */
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/cities.singular')])
            );
        }

        $city->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/cities.singular')])
        );
    }
}
