<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createcity_areaAPIRequest;
use App\Http\Requests\API\Updatecity_areaAPIRequest;
use App\Models\city_area;
use App\Repositories\city_areaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\city_areaResource;
use Response;

/**
 * Class city_areaController
 * @package App\Http\Controllers\API
 */

class city_areaAPIController extends AppBaseController
{
    /** @var  city_areaRepository */
    private $cityAreaRepository;

    public function __construct(city_areaRepository $cityAreaRepo)
    {
        $this->cityAreaRepository = $cityAreaRepo;
    }

    /**
     * Display a listing of the city_area.
     * GET|HEAD /cityAreas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $cityAreas = $this->cityAreaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            city_areaResource::collection($cityAreas),
            __('messages.retrieved', ['model' => __('models/cityAreas.plural')])
        );
    }

    /**
     * Store a newly created city_area in storage.
     * POST /cityAreas
     *
     * @param Createcity_areaAPIRequest $request
     *
     * @return Response
     */
    public function store(Createcity_areaAPIRequest $request)
    {
        $input = $request->all();

        $cityArea = $this->cityAreaRepository->create($input);

        return $this->sendResponse(
            new city_areaResource($cityArea),
            __('messages.saved', ['model' => __('models/cityAreas.singular')])
        );
    }

    /**
     * Display the specified city_area.
     * GET|HEAD /cityAreas/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var city_area $cityArea */
        $cityArea = $this->cityAreaRepository->find($id);

        if (empty($cityArea)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/cityAreas.singular')])
            );
        }

        return $this->sendResponse(
            new city_areaResource($cityArea),
            __('messages.retrieved', ['model' => __('models/cityAreas.singular')])
        );
    }

    /**
     * Update the specified city_area in storage.
     * PUT/PATCH /cityAreas/{id}
     *
     * @param int $id
     * @param Updatecity_areaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecity_areaAPIRequest $request)
    {
        $input = $request->all();

        /** @var city_area $cityArea */
        $cityArea = $this->cityAreaRepository->find($id);

        if (empty($cityArea)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/cityAreas.singular')])
            );
        }

        $cityArea = $this->cityAreaRepository->update($input, $id);

        return $this->sendResponse(
            new city_areaResource($cityArea),
            __('messages.updated', ['model' => __('models/cityAreas.singular')])
        );
    }

    /**
     * Remove the specified city_area from storage.
     * DELETE /cityAreas/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var city_area $cityArea */
        $cityArea = $this->cityAreaRepository->find($id);

        if (empty($cityArea)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/cityAreas.singular')])
            );
        }

        $cityArea->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/cityAreas.singular')])
        );
    }
}
