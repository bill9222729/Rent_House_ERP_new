<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createcontract_managementAPIRequest;
use App\Http\Requests\API\Updatecontract_managementAPIRequest;
use App\Models\contract_management;
use App\Repositories\contract_managementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\contract_managementResource;
use Response;

/**
 * Class contract_managementController
 * @package App\Http\Controllers\API
 */

class contract_managementAPIController extends AppBaseController
{
    /** @var  contract_managementRepository */
    private $contractManagementRepository;

    public function __construct(contract_managementRepository $contractManagementRepo)
    {
        $this->contractManagementRepository = $contractManagementRepo;
    }

    /**
     * Display a listing of the contract_management.
     * GET|HEAD /contractManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $contractManagements = $this->contractManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            contract_managementResource::collection($contractManagements),
            __('messages.retrieved', ['model' => __('models/contractManagements.plural')])
        );
    }

    /**
     * Store a newly created contract_management in storage.
     * POST /contractManagements
     *
     * @param Createcontract_managementAPIRequest $request
     *
     * @return Response
     */
    public function store(Createcontract_managementAPIRequest $request)
    {
        $input = $request->all();

        $contractManagement = $this->contractManagementRepository->create($input);

        return $this->sendResponse(
            new contract_managementResource($contractManagement),
            __('messages.saved', ['model' => __('models/contractManagements.singular')])
        );
    }

    /**
     * Display the specified contract_management.
     * GET|HEAD /contractManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var contract_management $contractManagement */
        $contractManagement = $this->contractManagementRepository->find($id);

        if (empty($contractManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/contractManagements.singular')])
            );
        }

        return $this->sendResponse(
            new contract_managementResource($contractManagement),
            __('messages.retrieved', ['model' => __('models/contractManagements.singular')])
        );
    }

    /**
     * Update the specified contract_management in storage.
     * PUT/PATCH /contractManagements/{id}
     *
     * @param int $id
     * @param Updatecontract_managementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecontract_managementAPIRequest $request)
    {
        $input = $request->all();

        /** @var contract_management $contractManagement */
        $contractManagement = $this->contractManagementRepository->find($id);

        if (empty($contractManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/contractManagements.singular')])
            );
        }

        $contractManagement = $this->contractManagementRepository->update($input, $id);

        return $this->sendResponse(
            new contract_managementResource($contractManagement),
            __('messages.updated', ['model' => __('models/contractManagements.singular')])
        );
    }

    /**
     * Remove the specified contract_management from storage.
     * DELETE /contractManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var contract_management $contractManagement */
        $contractManagement = $this->contractManagementRepository->find($id);

        if (empty($contractManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/contractManagements.singular')])
            );
        }

        $contractManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/contractManagements.singular')])
        );
    }
}
