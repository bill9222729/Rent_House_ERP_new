<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createcontracts_newAPIRequest;
use App\Http\Requests\API\Updatecontracts_newAPIRequest;
use App\Models\contracts_new;
use App\Repositories\contracts_newRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\contracts_newResource;
use Response;

/**
 * Class contracts_newController
 * @package App\Http\Controllers\API
 */

class contracts_newAPIController extends AppBaseController
{
    /** @var  contracts_newRepository */
    private $contractsNewRepository;

    public function __construct(contracts_newRepository $contractsNewRepo)
    {
        $this->contractsNewRepository = $contractsNewRepo;
    }

    /**
     * Display a listing of the contracts_new.
     * GET|HEAD /contractsNews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $contractsNews = $this->contractsNewRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            contracts_newResource::collection($contractsNews),
            __('messages.retrieved', ['model' => __('models/contractsNews.plural')])
        );
    }

    /**
     * Store a newly created contracts_new in storage.
     * POST /contractsNews
     *
     * @param Createcontracts_newAPIRequest $request
     *
     * @return Response
     */
    public function store(Createcontracts_newAPIRequest $request)
    {
        $input = $request->all();

        $contractsNew = $this->contractsNewRepository->create($input);

        return $this->sendResponse(
            new contracts_newResource($contractsNew),
            __('messages.saved', ['model' => __('models/contractsNews.singular')])
        );
    }

    /**
     * Display the specified contracts_new.
     * GET|HEAD /contractsNews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var contracts_new $contractsNew */
        $contractsNew = $this->contractsNewRepository->find($id);

        if (empty($contractsNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/contractsNews.singular')])
            );
        }

        return $this->sendResponse(
            new contracts_newResource($contractsNew),
            __('messages.retrieved', ['model' => __('models/contractsNews.singular')])
        );
    }

    /**
     * Update the specified contracts_new in storage.
     * PUT/PATCH /contractsNews/{id}
     *
     * @param int $id
     * @param Updatecontracts_newAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecontracts_newAPIRequest $request)
    {
        $input = $request->all();

        /** @var contracts_new $contractsNew */
        $contractsNew = $this->contractsNewRepository->find($id);

        if (empty($contractsNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/contractsNews.singular')])
            );
        }

        $contractsNew = $this->contractsNewRepository->update($input, $id);

        return $this->sendResponse(
            new contracts_newResource($contractsNew),
            __('messages.updated', ['model' => __('models/contractsNews.singular')])
        );
    }

    /**
     * Remove the specified contracts_new from storage.
     * DELETE /contractsNews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var contracts_new $contractsNew */
        $contractsNew = $this->contractsNewRepository->find($id);

        if (empty($contractsNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/contractsNews.singular')])
            );
        }

        $contractsNew->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/contractsNews.singular')])
        );
    }
}
