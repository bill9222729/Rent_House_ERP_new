<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createfile_managmentAPIRequest;
use App\Http\Requests\API\Updatefile_managmentAPIRequest;
use App\Models\file_managment;
use App\Repositories\file_managmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\file_managmentResource;
use Response;

/**
 * Class file_managmentController
 * @package App\Http\Controllers\API
 */

class file_managmentAPIController extends AppBaseController
{
    /** @var  file_managmentRepository */
    private $fileManagmentRepository;

    public function __construct(file_managmentRepository $fileManagmentRepo)
    {
        $this->fileManagmentRepository = $fileManagmentRepo;
    }

    /**
     * Display a listing of the file_managment.
     * GET|HEAD /fileManagments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $fileManagments = $this->fileManagmentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            file_managmentResource::collection($fileManagments),
            __('messages.retrieved', ['model' => __('models/fileManagments.plural')])
        );
    }

    /**
     * Store a newly created file_managment in storage.
     * POST /fileManagments
     *
     * @param Createfile_managmentAPIRequest $request
     *
     * @return Response
     */
    public function store(Createfile_managmentAPIRequest $request)
    {
        $input = $request->all();
        $file = $request->file('source');
        // 上傳檔案
        if ($file->isValid()) {
            //獲取原檔名
            $originalName = $file->getClientOriginalName();
            //獲取檔案大小
            $fileSize = $file->getSize();
            //獲取檔案拓展名
            $ext = $file->getClientOriginalExtension();
            $type = $file->getClientMimeType();
            //獲取檔案臨時絕對路徑
            $realPath = $file->getRealPath();
            //自定義檔案儲存的名稱
            $fileName = date('Y-m-d-H-i-s') . '-' . uniqid() . '.' . $ext;

            $bool =  \Storage::disk('buildings_file')->put($fileName, file_get_contents($realPath));

            // var_dump($bool);

            // return $bool;
        }

        if ($bool) {
            // $input['file_source'] = "building_management";
            $input['file_size'] = $fileSize;
            $input['file_origin_name'] = $originalName;
            $input['file_server_name'] = $fileName;
            $fileManagment = $this->fileManagmentRepository->create($input);

            return $this->sendResponse(
                new file_managmentResource($fileManagment),
                __('messages.saved', ['model' => __('models/fileManagments.singular')])
            );
        }
    }

    /**
     * Display the specified file_managment.
     * GET|HEAD /fileManagments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var file_managment $fileManagment */
        $fileManagment = $this->fileManagmentRepository->find($id);

        if (empty($fileManagment)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fileManagments.singular')])
            );
        }

        return $this->sendResponse(
            new file_managmentResource($fileManagment),
            __('messages.retrieved', ['model' => __('models/fileManagments.singular')])
        );
    }

    /**
     * Update the specified file_managment in storage.
     * PUT/PATCH /fileManagments/{id}
     *
     * @param int $id
     * @param Updatefile_managmentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatefile_managmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var file_managment $fileManagment */
        $fileManagment = $this->fileManagmentRepository->find($id);

        if (empty($fileManagment)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fileManagments.singular')])
            );
        }

        $fileManagment = $this->fileManagmentRepository->update($input, $id);

        return $this->sendResponse(
            new file_managmentResource($fileManagment),
            __('messages.updated', ['model' => __('models/fileManagments.singular')])
        );
    }

    /**
     * Remove the specified file_managment from storage.
     * DELETE /fileManagments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var file_managment $fileManagment */
        $fileManagment = $this->fileManagmentRepository->find($id);

        if (empty($fileManagment)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fileManagments.singular')])
            );
        }

        $fileManagment->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/fileManagments.singular')])
        );
    }
}
