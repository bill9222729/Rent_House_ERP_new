<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createfix_detail_managementAPIRequest;
use App\Http\Requests\API\Updatefix_detail_managementAPIRequest;
use App\Models\fix_detail_management;
use App\Repositories\fix_detail_managementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\fix_detail_managementResource;
use Response;

/**
 * Class fix_detail_managementController
 * @package App\Http\Controllers\API
 */

class fix_detail_managementAPIController extends AppBaseController
{
    /** @var  fix_detail_managementRepository */
    private $fixDetailManagementRepository;

    public function __construct(fix_detail_managementRepository $fixDetailManagementRepo)
    {
        $this->fixDetailManagementRepository = $fixDetailManagementRepo;
    }

    /**
     * Display a listing of the fix_detail_management.
     * GET|HEAD /fixDetailManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $fixDetailManagements = $this->fixDetailManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            fix_detail_managementResource::collection($fixDetailManagements),
            __('messages.retrieved', ['model' => __('models/fixDetailManagements.plural')])
        );
    }

    /**
     * Store a newly created fix_detail_management in storage.
     * POST /fixDetailManagements
     *
     * @param Createfix_detail_managementAPIRequest $request
     *
     * @return Response
     */
    public function store(Createfix_detail_managementAPIRequest $request)
    {
        $input = $request->all();

        $fixDetailManagement = $this->fixDetailManagementRepository->create($input);

        return $this->sendResponse(
            new fix_detail_managementResource($fixDetailManagement),
            __('messages.saved', ['model' => __('models/fixDetailManagements.singular')])
        );
    }

    /**
     * Display the specified fix_detail_management.
     * GET|HEAD /fixDetailManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var fix_detail_management $fixDetailManagement */
        $fixDetailManagement = $this->fixDetailManagementRepository->find($id);

        if (empty($fixDetailManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixDetailManagements.singular')])
            );
        }

        return $this->sendResponse(
            new fix_detail_managementResource($fixDetailManagement),
            __('messages.retrieved', ['model' => __('models/fixDetailManagements.singular')])
        );
    }

    /**
     * Update the specified fix_detail_management in storage.
     * PUT/PATCH /fixDetailManagements/{id}
     *
     * @param int $id
     * @param Updatefix_detail_managementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatefix_detail_managementAPIRequest $request)
    {
        $input = $request->all();

        /** @var fix_detail_management $fixDetailManagement */
        $fixDetailManagement = $this->fixDetailManagementRepository->find($id);

        if (empty($fixDetailManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixDetailManagements.singular')])
            );
        }

        $fixDetailManagement = $this->fixDetailManagementRepository->update($input, $id);

        return $this->sendResponse(
            new fix_detail_managementResource($fixDetailManagement),
            __('messages.updated', ['model' => __('models/fixDetailManagements.singular')])
        );
    }

    /**
     * Remove the specified fix_detail_management from storage.
     * DELETE /fixDetailManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var fix_detail_management $fixDetailManagement */
        $fixDetailManagement = $this->fixDetailManagementRepository->find($id);

        if (empty($fixDetailManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixDetailManagements.singular')])
            );
        }

        $fixDetailManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/fixDetailManagements.singular')])
        );
    }
}
