<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createfix_managementAPIRequest;
use App\Http\Requests\API\Updatefix_managementAPIRequest;
use App\Models\fix_management;
use App\Repositories\fix_managementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\fix_managementResource;
use Response;
use DB;

/**
 * Class fix_managementController
 * @package App\Http\Controllers\API
 */

class fix_managementAPIController extends AppBaseController
{
    /** @var  fix_managementRepository */
    private $fixManagementRepository;

    public function __construct(fix_managementRepository $fixManagementRepo)
    {
        $this->fixManagementRepository = $fixManagementRepo;
    }

    /**
     * Display a listing of the fix_management.
     * GET|HEAD /fixManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $fixManagements = $this->fixManagementRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );

        $input = $request->all();

        $data = DB::table("fix_management")
            ->leftJoin("fix_detail_management", function ($join) {
                $join->on("fix_management.id", "=", "fix_detail_management.fix_id")
                    ->where(DB::raw("(SELECT max(`fix_detail_management`.`created_at`) FROM `fix_detail_management`)"), "=", "`fix_detail_management`.`created_at`");
            })
            ->leftJoin("building_management_new", function ($join) {
                $join->on("fix_management.building_case_no", "=", "building_management_new.case_no");
            })
            ->leftJoin("city_area", function ($join) {
                $join->on("city_area.value", "=", "building_management_new.building_address_city_area");
            })
            ->leftJoin("salesman_management", function ($join) {
                $join->on("salesman_management.id", "=", "building_management_new.sales_id");
            })
            ->leftJoin("match_management", function ($join) {
                $join->on("match_management.match_id", "=", "fix_management.match_id");
            })
            ->leftJoin("tenant_management_new", function ($join) {
                $join->on("tenant_management_new.government_no", "=", "match_management.tenant_case_id");
            })
            ->select(
                "fix_management.*",
                "fix_detail_management.*",
                "building_management_new.name",
                "building_management_new.building_address_city",
                "building_management_new.building_address_city_area",
                "building_management_new.building_address_street",
                "building_management_new.building_address_ln",
                "building_management_new.building_address_aly",
                "building_management_new.building_address_num",
                "building_management_new.building_address_num_hyphen",
                "building_management_new.building_address_floor",
                "building_management_new.building_address_floor_sub",
                "building_management_new.building_address_room_num",
                "city_area.name as building_address_city_area_name",
                "salesman_management.name as sales_name",
                "building_management_new.name as landlord_name",
                "match_management.charter_case_id",
                "tenant_management_new.name as tenant_name"
            )
            ->whereNull("fix_management.deleted_at");

        if (count($input) > 0) {
            foreach ($input as $key => $value) {
                if ($key == "_") {
                    continue;
                }
                $data = $data->where("fix_management." . $key, "=", $value);
            }
        }

        $data = $data->get();

        return $this->sendResponse(
            $data,
            __('messages.retrieved', ['model' => __('models/fixManagements.plural')])
        );

        // return $this->sendResponse(
        //     fix_managementResource::collection($fixManagements),
        //     __('messages.retrieved', ['model' => __('models/fixManagements.plural')])
        // );
    }

    /**
     * Store a newly created fix_management in storage.
     * POST /fixManagements
     *
     * @param Createfix_managementAPIRequest $request
     *
     * @return Response
     */
    public function store(Createfix_managementAPIRequest $request)
    {
        $input = $request->all();

        $fixManagement = $this->fixManagementRepository->create($input);

        return $this->sendResponse(
            new fix_managementResource($fixManagement),
            __('messages.saved', ['model' => __('models/fixManagements.singular')])
        );
    }

    /**
     * Display the specified fix_management.
     * GET|HEAD /fixManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var fix_management $fixManagement */
        $fixManagement = $this->fixManagementRepository->find($id);

        if (empty($fixManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixManagements.singular')])
            );
        }

        return $this->sendResponse(
            new fix_managementResource($fixManagement),
            __('messages.retrieved', ['model' => __('models/fixManagements.singular')])
        );
    }

    /**
     * Update the specified fix_management in storage.
     * PUT/PATCH /fixManagements/{id}
     *
     * @param int $id
     * @param Updatefix_managementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatefix_managementAPIRequest $request)
    {
        $input = $request->all();

        /** @var fix_management $fixManagement */
        $fixManagement = $this->fixManagementRepository->find($id);

        if (empty($fixManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixManagements.singular')])
            );
        }

        $fixManagement = $this->fixManagementRepository->update($input, $id);

        return $this->sendResponse(
            new fix_managementResource($fixManagement),
            __('messages.updated', ['model' => __('models/fixManagements.singular')])
        );
    }

    /**
     * Remove the specified fix_management from storage.
     * DELETE /fixManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var fix_management $fixManagement */
        $fixManagement = $this->fixManagementRepository->find($id);

        if (empty($fixManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/fixManagements.singular')])
            );
        }

        $fixManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/fixManagements.singular')])
        );
    }
}
