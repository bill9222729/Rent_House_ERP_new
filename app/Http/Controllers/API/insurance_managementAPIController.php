<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createinsurance_managementAPIRequest;
use App\Http\Requests\API\Updateinsurance_managementAPIRequest;
use App\Models\insurance_management;
use App\Repositories\insurance_managementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\insurance_managementResource;
use Response;
use DB;

/**
 * Class insurance_managementController
 * @package App\Http\Controllers\API
 */

class insurance_managementAPIController extends AppBaseController
{
    /** @var  insurance_managementRepository */
    private $insuranceManagementRepository;

    public function __construct(insurance_managementRepository $insuranceManagementRepo)
    {
        $this->insuranceManagementRepository = $insuranceManagementRepo;
    }

    /**
     * Display a listing of the insurance_management.
     * GET|HEAD /insuranceManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $insuranceManagements = $this->insuranceManagementRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );

        $input = $request->all();

        $data = DB::table("insurance_management")
            ->leftJoin("building_management_new", function ($join) {
                $join->on("insurance_management.building_id", "=", "building_management_new.case_no")
                ->whereNull("building_management_new.deleted_at");
            })
            ->leftJoin("city_area", function ($join) {
                $join->on("building_management_new.building_address_city_area", "=", "city_area.value")
                ->whereNull("city_area.deleted_at");
            })
            ->leftJoin("salesman_management", function ($join) {
                $join->on("building_management_new.sales_id", "=", "salesman_management.id");
            })
            ->select(
                "insurance_management.*",
                "building_management_new.id AS building_id",
                "building_management_new.name",
                "building_management_new.building_address_city",
                "building_management_new.building_address_city_area",
                "building_management_new.building_address_street",
                "building_management_new.building_address_ln",
                "building_management_new.building_address_aly",
                "building_management_new.building_address_num",
                "building_management_new.building_address_num_hyphen",
                "building_management_new.building_address_floor",
                "building_management_new.building_address_floor_sub",
                "building_management_new.building_address_room_num",
                "city_area.name AS city_area_name",
                "salesman_management.name AS salesman_name"
                )
            ->whereNull("insurance_management.deleted_at");

        if (count($input) > 0) {
            foreach ($input as $key => $value) {
                if ($key == "_") {
                    continue;
                }
                $data = $data->where("insurance_management." . $key, "=", $value);
            }
        }

        $data = $data->get();

        // return $this->sendResponse(
        //     insurance_managementResource::collection($insuranceManagements),
        //     __('messages.retrieved', ['model' => __('models/insuranceManagements.plural')])
        // );
        return $this->sendResponse(
            $data,
            __('messages.retrieved', ['model' => __('models/insuranceManagements.plural')])
        );
    }

    /**
     * Store a newly created insurance_management in storage.
     * POST /insuranceManagements
     *
     * @param Createinsurance_managementAPIRequest $request
     *
     * @return Response
     */
    public function store(Createinsurance_managementAPIRequest $request)
    {
        $input = $request->all();

        $insuranceManagement = $this->insuranceManagementRepository->create($input);

        return $this->sendResponse(
            new insurance_managementResource($insuranceManagement),
            __('messages.saved', ['model' => __('models/insuranceManagements.singular')])
        );
    }

    /**
     * Display the specified insurance_management.
     * GET|HEAD /insuranceManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var insurance_management $insuranceManagement */
        $insuranceManagement = $this->insuranceManagementRepository->find($id);

        if (empty($insuranceManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/insuranceManagements.singular')])
            );
        }

        return $this->sendResponse(
            new insurance_managementResource($insuranceManagement),
            __('messages.retrieved', ['model' => __('models/insuranceManagements.singular')])
        );
    }

    /**
     * Update the specified insurance_management in storage.
     * PUT/PATCH /insuranceManagements/{id}
     *
     * @param int $id
     * @param Updateinsurance_managementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateinsurance_managementAPIRequest $request)
    {
        $input = $request->all();

        /** @var insurance_management $insuranceManagement */
        $insuranceManagement = $this->insuranceManagementRepository->find($id);

        if (empty($insuranceManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/insuranceManagements.singular')])
            );
        }

        $insuranceManagement = $this->insuranceManagementRepository->update($input, $id);

        return $this->sendResponse(
            new insurance_managementResource($insuranceManagement),
            __('messages.updated', ['model' => __('models/insuranceManagements.singular')])
        );
    }

    /**
     * Remove the specified insurance_management from storage.
     * DELETE /insuranceManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var insurance_management $insuranceManagement */
        $insuranceManagement = $this->insuranceManagementRepository->find($id);

        if (empty($insuranceManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/insuranceManagements.singular')])
            );
        }

        $insuranceManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/insuranceManagements.singular')])
        );
    }
}
