<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createlocated_lotAPIRequest;
use App\Http\Requests\API\Updatelocated_lotAPIRequest;
use App\Models\located_lot;
use App\Repositories\located_lotRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\located_lotResource;
use Response;

/**
 * Class located_lotController
 * @package App\Http\Controllers\API
 */

class located_lotAPIController extends AppBaseController
{
    /** @var  located_lotRepository */
    private $locatedLotRepository;

    public function __construct(located_lotRepository $locatedLotRepo)
    {
        $this->locatedLotRepository = $locatedLotRepo;
    }

    /**
     * Display a listing of the located_lot.
     * GET|HEAD /locatedLots
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $locatedLots = $this->locatedLotRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            located_lotResource::collection($locatedLots),
            __('messages.retrieved', ['model' => __('models/locatedLots.plural')])
        );
    }

    /**
     * Store a newly created located_lot in storage.
     * POST /locatedLots
     *
     * @param Createlocated_lotAPIRequest $request
     *
     * @return Response
     */
    public function store(Createlocated_lotAPIRequest $request)
    {
        $input = $request->all();

        $locatedLot = $this->locatedLotRepository->create($input);

        return $this->sendResponse(
            new located_lotResource($locatedLot),
            __('messages.saved', ['model' => __('models/locatedLots.singular')])
        );
    }

    /**
     * Display the specified located_lot.
     * GET|HEAD /locatedLots/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var located_lot $locatedLot */
        $locatedLot = $this->locatedLotRepository->find($id);

        if (empty($locatedLot)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locatedLots.singular')])
            );
        }

        return $this->sendResponse(
            new located_lotResource($locatedLot),
            __('messages.retrieved', ['model' => __('models/locatedLots.singular')])
        );
    }

    /**
     * Update the specified located_lot in storage.
     * PUT/PATCH /locatedLots/{id}
     *
     * @param int $id
     * @param Updatelocated_lotAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatelocated_lotAPIRequest $request)
    {
        $input = $request->all();

        /** @var located_lot $locatedLot */
        $locatedLot = $this->locatedLotRepository->find($id);

        if (empty($locatedLot)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locatedLots.singular')])
            );
        }

        $locatedLot = $this->locatedLotRepository->update($input, $id);

        return $this->sendResponse(
            new located_lotResource($locatedLot),
            __('messages.updated', ['model' => __('models/locatedLots.singular')])
        );
    }

    /**
     * Remove the specified located_lot from storage.
     * DELETE /locatedLots/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var located_lot $locatedLot */
        $locatedLot = $this->locatedLotRepository->find($id);

        if (empty($locatedLot)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/locatedLots.singular')])
            );
        }

        $locatedLot->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/locatedLots.singular')])
        );
    }
}
