<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Creatematch_managementAPIRequest;
use App\Http\Requests\API\Updatematch_managementAPIRequest;
use App\Models\match_management;
use App\Repositories\match_managementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\match_managementResource;
use Response;
use DB;

/**
 * Class match_managementController
 * @package App\Http\Controllers\API
 */

class match_managementAPIController extends AppBaseController
{
    /** @var  match_managementRepository */
    private $matchManagementRepository;

    public function __construct(match_managementRepository $matchManagementRepo)
    {
        $this->matchManagementRepository = $matchManagementRepo;
    }

    /**
     * Display a listing of the match_management.
     * GET|HEAD /matchManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $matchManagements = $this->matchManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $input = $request->all();
        // $data = DB::table("match_management")
        //     ->leftJoin("contract_management", function ($join) {
        //         $join->on("contract_management.match_id", "match_management.match_id", "=")
        //             ->on("contract_management.id", DB::raw("(SELECT MAX(contract_management.id) FROM contract_management WHERE contract_management.match_id = match_management.match_id)"))
        //             ->whereNull("contract_management.deleted_at");
        //     })
        //     ->leftJoin("building_management_new", function ($join) {
        //         $join->on("match_management.building_case_id", "=", "building_management_new.government_no")
        //             ->whereNull("building_management_new.deleted_at");
        //     })
        //     ->leftJoin("tenant_management_new", function ($join) {
        //         $join->on("match_management.tenant_case_id", "=", "tenant_management_new.government_no")
        //             ->whereNull("tenant_management_new.deleted_at");
        //     })
        //     ->select(
        //         "match_management.*",
        //         "contract_management.rent",
        //         "contract_management.bank_owner_name",
        //         "contract_management.signing_date",
        //         "contract_management.case_status",
        //         "contract_management.building_case_no",
        //         "contract_management.tenant_case_no",
        //         DB::raw("concat (contract_management.lease_start_date, '~', contract_management.lease_end_date) as lease_date"),
        //         "building_management_new.name as part_a",
        //         "tenant_management_new.name as part_b"
        //     )
        //     ->whereNull("match_management.deleted_at")
        //     ->whereNull("building_management_new.deleted_at")
        //     ->whereNull("tenant_management_new.deleted_at")
        //     ->whereNull("contract_management.deleted_at");

        $data = DB::table("match_management")
        ->leftJoin("contract_management", function ($join) {
            $join->on("contract_management.match_id", "match_management.id", "=")
                ->whereNull("contract_management.deleted_at");
        })
        ->leftJoin("building_management_new", function ($join) {
            $join->on("match_management.building_case_id", "=", "building_management_new.id");
        })
        ->leftJoin("tenant_management_new", function ($join) {
            $join->on("match_management.tenant_case_id", "=", "tenant_management_new.id");
        })
        ->select(
            "match_management.*",
            "contract_management.rent",
            "contract_management.bank_owner_name",
            "contract_management.signing_date",
            "contract_management.case_status",
            "contract_management.building_case_no",
            "contract_management.tenant_case_no",
            DB::raw("concat (contract_management.lease_start_date, '~', contract_management.lease_end_date) as lease_date"),
            "building_management_new.name as part_a",
            "building_management_new.government_no as building_government_no",
            "tenant_management_new.name as part_b",
            "tenant_management_new.government_no as tenant_government_no",
            DB::Raw('IFNULL( `building_management_new`.`sales_id` , `tenant_management_new`.`sales_id` ) AS sales_id')
        )
        ->whereNull("match_management.deleted_at")
        ->whereNull("contract_management.deleted_at");

        if (count($input) > 0) {
            foreach ($input as $key => $value) {
                if ($key == "_") {
                    continue;
                }
                $data = $data->where("match_management." . $key, "=", $value);
            }
        }

        $data = $data->get();

        // return $this->sendResponse(
        //     match_managementResource::collection($matchManagements),
        //     __('messages.retrieved', ['model' => __('models/matchManagements.plural')])
        // );
        return $this->sendResponse(
            $data,
            __('messages.retrieved', ['model' => __('models/matchManagements.plural')])
        );
    }

    /**
     * Store a newly created match_management in storage.
     * POST /matchManagements
     *
     * @param Creatematch_managementAPIRequest $request
     *
     * @return Response
     */
    public function store(Creatematch_managementAPIRequest $request)
    {
        $input = $request->all();

        $matchManagement = $this->matchManagementRepository->create($input);

        return $this->sendResponse(
            new match_managementResource($matchManagement),
            __('messages.saved', ['model' => __('models/matchManagements.singular')])
        );
    }

    /**
     * Display the specified match_management.
     * GET|HEAD /matchManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var match_management $matchManagement */
        $matchManagement = $this->matchManagementRepository->find($id);

        if (empty($matchManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/matchManagements.singular')])
            );
        }

        return $this->sendResponse(
            new match_managementResource($matchManagement),
            __('messages.retrieved', ['model' => __('models/matchManagements.singular')])
        );
    }

    /**
     * Update the specified match_management in storage.
     * PUT/PATCH /matchManagements/{id}
     *
     * @param int $id
     * @param Updatematch_managementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatematch_managementAPIRequest $request)
    {
        $input = $request->all();

        /** @var match_management $matchManagement */
        $matchManagement = $this->matchManagementRepository->find($id);

        if (empty($matchManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/matchManagements.singular')])
            );
        }

        $matchManagement = $this->matchManagementRepository->update($input, $id);

        return $this->sendResponse(
            new match_managementResource($matchManagement),
            __('messages.updated', ['model' => __('models/matchManagements.singular')])
        );
    }

    /**
     * Remove the specified match_management from storage.
     * DELETE /matchManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var match_management $matchManagement */
        $matchManagement = $this->matchManagementRepository->find($id);

        if (empty($matchManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/matchManagements.singular')])
            );
        }

        $matchManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/matchManagements.singular')])
        );
    }
}
