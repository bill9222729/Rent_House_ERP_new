<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createsalesman_managementAPIRequest;
use App\Http\Requests\API\Updatesalesman_managementAPIRequest;
// use App\Models\salesman_management;
use App\Repositories\salesman_managementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\salesman_managementResource;
use Response;

/**
 * Class salesman_managementController
 * @package App\Http\Controllers\API
 */

class salesman_managementAPIController extends AppBaseController
{
    /** @var  salesman_managementRepository */
    private $salesmanManagementRepository;

    public function __construct(salesman_managementRepository $salesmanManagementRepo)
    {
        $this->salesmanManagementRepository = $salesmanManagementRepo;
    }

    /**
     * Display a listing of the salesman_management.
     * GET|HEAD /salesmanManagements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $salesmanManagements = $this->salesmanManagementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        return $this->sendResponse(
            salesman_managementResource::collection($salesmanManagements),
            __('messages.retrieved', ['model' => __('models/salesmanManagements.plural')])
        );
    }

    /**
     * Store a newly created salesman_management in storage.
     * POST /salesmanManagements
     *
     * @param Createsalesman_managementAPIRequest $request
     *
     * @return Response
     */
    public function store(Createsalesman_managementAPIRequest $request)
    {
        $input = $request->all();

        $salesmanManagement = $this->salesmanManagementRepository->create($input);

        return $this->sendResponse(
            new salesman_managementResource($salesmanManagement),
            __('messages.saved', ['model' => __('models/salesmanManagements.singular')])
        );
    }

    /**
     * Display the specified salesman_management.
     * GET|HEAD /salesmanManagements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var salesman_management $salesmanManagement */
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/salesmanManagements.singular')])
            );
        }

        return $this->sendResponse(
            new salesman_managementResource($salesmanManagement),
            __('messages.retrieved', ['model' => __('models/salesmanManagements.singular')])
        );
    }

    /**
     * Update the specified salesman_management in storage.
     * PUT/PATCH /salesmanManagements/{id}
     *
     * @param int $id
     * @param Updatesalesman_managementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatesalesman_managementAPIRequest $request)
    {
        $input = $request->all();

        /** @var salesman_management $salesmanManagement */
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/salesmanManagements.singular')])
            );
        }

        $salesmanManagement = $this->salesmanManagementRepository->update($input, $id);

        return $this->sendResponse(
            new salesman_managementResource($salesmanManagement),
            __('messages.updated', ['model' => __('models/salesmanManagements.singular')])
        );
    }

    /**
     * Remove the specified salesman_management from storage.
     * DELETE /salesmanManagements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var salesman_management $salesmanManagement */
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/salesmanManagements.singular')])
            );
        }

        $salesmanManagement->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/salesmanManagements.singular')])
        );
    }
}
