<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatestreetAPIRequest;
use App\Http\Requests\API\UpdatestreetAPIRequest;
use App\Models\street;
use App\Repositories\streetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\streetResource;
use Response;

/**
 * Class streetController
 * @package App\Http\Controllers\API
 */

class streetAPIController extends AppBaseController
{
    /** @var  streetRepository */
    private $streetRepository;

    public function __construct(streetRepository $streetRepo)
    {
        $this->streetRepository = $streetRepo;
    }

    /**
     * Display a listing of the street.
     * GET|HEAD /streets
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $streets = $this->streetRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            streetResource::collection($streets),
            __('messages.retrieved', ['model' => __('models/streets.plural')])
        );
    }

    /**
     * Store a newly created street in storage.
     * POST /streets
     *
     * @param CreatestreetAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatestreetAPIRequest $request)
    {
        $input = $request->all();

        $street = $this->streetRepository->create($input);

        return $this->sendResponse(
            new streetResource($street),
            __('messages.saved', ['model' => __('models/streets.singular')])
        );
    }

    /**
     * Display the specified street.
     * GET|HEAD /streets/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var street $street */
        $street = $this->streetRepository->find($id);

        if (empty($street)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/streets.singular')])
            );
        }

        return $this->sendResponse(
            new streetResource($street),
            __('messages.retrieved', ['model' => __('models/streets.singular')])
        );
    }

    /**
     * Update the specified street in storage.
     * PUT/PATCH /streets/{id}
     *
     * @param int $id
     * @param UpdatestreetAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatestreetAPIRequest $request)
    {
        $input = $request->all();

        /** @var street $street */
        $street = $this->streetRepository->find($id);

        if (empty($street)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/streets.singular')])
            );
        }

        $street = $this->streetRepository->update($input, $id);

        return $this->sendResponse(
            new streetResource($street),
            __('messages.updated', ['model' => __('models/streets.singular')])
        );
    }

    /**
     * Remove the specified street from storage.
     * DELETE /streets/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var street $street */
        $street = $this->streetRepository->find($id);

        if (empty($street)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/streets.singular')])
            );
        }

        $street->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/streets.singular')])
        );
    }
}
