<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createtenant_building_infoAPIRequest;
use App\Http\Requests\API\Updatetenant_building_infoAPIRequest;
use App\Models\tenant_building_info;
use App\Repositories\tenant_building_infoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\tenant_building_infoResource;
use Response;

/**
 * Class tenant_building_infoController
 * @package App\Http\Controllers\API
 */

class tenant_building_infoAPIController extends AppBaseController
{
    /** @var  tenant_building_infoRepository */
    private $tenantBuildingInfoRepository;

    public function __construct(tenant_building_infoRepository $tenantBuildingInfoRepo)
    {
        $this->tenantBuildingInfoRepository = $tenantBuildingInfoRepo;
    }

    /**
     * Display a listing of the tenant_building_info.
     * GET|HEAD /tenantBuildingInfos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tenantBuildingInfos = $this->tenantBuildingInfoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            tenant_building_infoResource::collection($tenantBuildingInfos),
            __('messages.retrieved', ['model' => __('models/tenantBuildingInfos.plural')])
        );
    }

    /**
     * Store a newly created tenant_building_info in storage.
     * POST /tenantBuildingInfos
     *
     * @param Createtenant_building_infoAPIRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_building_infoAPIRequest $request)
    {
        $input = $request->all();

        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->create($input);

        return $this->sendResponse(
            new tenant_building_infoResource($tenantBuildingInfo),
            __('messages.saved', ['model' => __('models/tenantBuildingInfos.singular')])
        );
    }

    /**
     * Display the specified tenant_building_info.
     * GET|HEAD /tenantBuildingInfos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var tenant_building_info $tenantBuildingInfo */
        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->find($id);

        if (empty($tenantBuildingInfo)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantBuildingInfos.singular')])
            );
        }

        return $this->sendResponse(
            new tenant_building_infoResource($tenantBuildingInfo),
            __('messages.retrieved', ['model' => __('models/tenantBuildingInfos.singular')])
        );
    }

    /**
     * Update the specified tenant_building_info in storage.
     * PUT/PATCH /tenantBuildingInfos/{id}
     *
     * @param int $id
     * @param Updatetenant_building_infoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_building_infoAPIRequest $request)
    {
        $input = $request->all();

        /** @var tenant_building_info $tenantBuildingInfo */
        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->find($id);

        if (empty($tenantBuildingInfo)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantBuildingInfos.singular')])
            );
        }

        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->update($input, $id);

        return $this->sendResponse(
            new tenant_building_infoResource($tenantBuildingInfo),
            __('messages.updated', ['model' => __('models/tenantBuildingInfos.singular')])
        );
    }

    /**
     * Remove the specified tenant_building_info from storage.
     * DELETE /tenantBuildingInfos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var tenant_building_info $tenantBuildingInfo */
        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->find($id);

        if (empty($tenantBuildingInfo)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantBuildingInfos.singular')])
            );
        }

        $tenantBuildingInfo->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/tenantBuildingInfos.singular')])
        );
    }
}
