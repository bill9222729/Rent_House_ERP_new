<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createtenant_family_infoAPIRequest;
use App\Http\Requests\API\Updatetenant_family_infoAPIRequest;
use App\Models\tenant_family_info;
use App\Repositories\tenant_family_infoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\tenant_family_infoResource;
use Response;

/**
 * Class tenant_family_infoController
 * @package App\Http\Controllers\API
 */

class tenant_family_infoAPIController extends AppBaseController
{
    /** @var  tenant_family_infoRepository */
    private $tenantFamilyInfoRepository;

    public function __construct(tenant_family_infoRepository $tenantFamilyInfoRepo)
    {
        $this->tenantFamilyInfoRepository = $tenantFamilyInfoRepo;
    }

    /**
     * Display a listing of the tenant_family_info.
     * GET|HEAD /tenantFamilyInfos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tenantFamilyInfos = $this->tenantFamilyInfoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            tenant_family_infoResource::collection($tenantFamilyInfos),
            __('messages.retrieved', ['model' => __('models/tenantFamilyInfos.plural')])
        );
    }

    /**
     * Store a newly created tenant_family_info in storage.
     * POST /tenantFamilyInfos
     *
     * @param Createtenant_family_infoAPIRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_family_infoAPIRequest $request)
    {
        $input = $request->all();

        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->create($input);

        return $this->sendResponse(
            new tenant_family_infoResource($tenantFamilyInfo),
            __('messages.saved', ['model' => __('models/tenantFamilyInfos.singular')])
        );
    }

    /**
     * Display the specified tenant_family_info.
     * GET|HEAD /tenantFamilyInfos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var tenant_family_info $tenantFamilyInfo */
        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->find($id);

        if (empty($tenantFamilyInfo)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantFamilyInfos.singular')])
            );
        }

        return $this->sendResponse(
            new tenant_family_infoResource($tenantFamilyInfo),
            __('messages.retrieved', ['model' => __('models/tenantFamilyInfos.singular')])
        );
    }

    /**
     * Update the specified tenant_family_info in storage.
     * PUT/PATCH /tenantFamilyInfos/{id}
     *
     * @param int $id
     * @param Updatetenant_family_infoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_family_infoAPIRequest $request)
    {
        $input = $request->all();

        /** @var tenant_family_info $tenantFamilyInfo */
        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->find($id);

        if (empty($tenantFamilyInfo)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantFamilyInfos.singular')])
            );
        }

        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->update($input, $id);

        return $this->sendResponse(
            new tenant_family_infoResource($tenantFamilyInfo),
            __('messages.updated', ['model' => __('models/tenantFamilyInfos.singular')])
        );
    }

    /**
     * Remove the specified tenant_family_info from storage.
     * DELETE /tenantFamilyInfos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var tenant_family_info $tenantFamilyInfo */
        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->find($id);

        if (empty($tenantFamilyInfo)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantFamilyInfos.singular')])
            );
        }

        $tenantFamilyInfo->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/tenantFamilyInfos.singular')])
        );
    }
}
