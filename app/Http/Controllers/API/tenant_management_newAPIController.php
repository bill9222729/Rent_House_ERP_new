<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createtenant_management_newAPIRequest;
use App\Http\Requests\API\Updatetenant_management_newAPIRequest;
use App\Models\tenant_management_new;
use App\Repositories\tenant_management_newRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\tenant_management_newResource;
use Response;

/**
 * Class tenant_management_newController
 * @package App\Http\Controllers\API
 */

class tenant_management_newAPIController extends AppBaseController
{
    /** @var  tenant_management_newRepository */
    private $tenantManagementNewRepository;

    public function __construct(tenant_management_newRepository $tenantManagementNewRepo)
    {
        $this->tenantManagementNewRepository = $tenantManagementNewRepo;
    }

    /**
     * Display a listing of the tenant_management_new.
     * GET|HEAD /tenantManagementNews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tenantManagementNews = $this->tenantManagementNewRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            tenant_management_newResource::collection($tenantManagementNews),
            __('messages.retrieved', ['model' => __('models/tenantManagementNews.plural')])
        );
    }

    /**
     * Store a newly created tenant_management_new in storage.
     * POST /tenantManagementNews
     *
     * @param Createtenant_management_newAPIRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_management_newAPIRequest $request)
    {
        $input = $request->all();

        $tenantManagementNew = $this->tenantManagementNewRepository->create($input);

        return $this->sendResponse(
            new tenant_management_newResource($tenantManagementNew),
            __('messages.saved', ['model' => __('models/tenantManagementNews.singular')])
        );
    }

    /**
     * Display the specified tenant_management_new.
     * GET|HEAD /tenantManagementNews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var tenant_management_new $tenantManagementNew */
        $tenantManagementNew = $this->tenantManagementNewRepository->find($id);

        if (empty($tenantManagementNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantManagementNews.singular')])
            );
        }

        return $this->sendResponse(
            new tenant_management_newResource($tenantManagementNew),
            __('messages.retrieved', ['model' => __('models/tenantManagementNews.singular')])
        );
    }

    /**
     * Update the specified tenant_management_new in storage.
     * PUT/PATCH /tenantManagementNews/{id}
     *
     * @param int $id
     * @param Updatetenant_management_newAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_management_newAPIRequest $request)
    {
        $input = $request->all();

        /** @var tenant_management_new $tenantManagementNew */
        $tenantManagementNew = $this->tenantManagementNewRepository->find($id);

        if (empty($tenantManagementNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantManagementNews.singular')])
            );
        }

        $tenantManagementNew = $this->tenantManagementNewRepository->update($input, $id);

        return $this->sendResponse(
            new tenant_management_newResource($tenantManagementNew),
            __('messages.updated', ['model' => __('models/tenantManagementNews.singular')])
        );
    }

    /**
     * Remove the specified tenant_management_new from storage.
     * DELETE /tenantManagementNews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var tenant_management_new $tenantManagementNew */
        $tenantManagementNew = $this->tenantManagementNewRepository->find($id);

        if (empty($tenantManagementNew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantManagementNews.singular')])
            );
        }

        $tenantManagementNew->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/tenantManagementNews.singular')])
        );
    }
}
