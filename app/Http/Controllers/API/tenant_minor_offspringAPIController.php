<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createtenant_minor_offspringAPIRequest;
use App\Http\Requests\API\Updatetenant_minor_offspringAPIRequest;
use App\Models\tenant_minor_offspring;
use App\Repositories\tenant_minor_offspringRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\tenant_minor_offspringResource;
use Response;

/**
 * Class tenant_minor_offspringController
 * @package App\Http\Controllers\API
 */

class tenant_minor_offspringAPIController extends AppBaseController
{
    /** @var  tenant_minor_offspringRepository */
    private $tenantMinorOffspringRepository;

    public function __construct(tenant_minor_offspringRepository $tenantMinorOffspringRepo)
    {
        $this->tenantMinorOffspringRepository = $tenantMinorOffspringRepo;
    }

    /**
     * Display a listing of the tenant_minor_offspring.
     * GET|HEAD /tenantMinorOffsprings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tenantMinorOffsprings = $this->tenantMinorOffspringRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            tenant_minor_offspringResource::collection($tenantMinorOffsprings),
            __('messages.retrieved', ['model' => __('models/tenantMinorOffsprings.plural')])
        );
    }

    /**
     * Store a newly created tenant_minor_offspring in storage.
     * POST /tenantMinorOffsprings
     *
     * @param Createtenant_minor_offspringAPIRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_minor_offspringAPIRequest $request)
    {
        $input = $request->all();

        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->create($input);

        return $this->sendResponse(
            new tenant_minor_offspringResource($tenantMinorOffspring),
            __('messages.saved', ['model' => __('models/tenantMinorOffsprings.singular')])
        );
    }

    /**
     * Display the specified tenant_minor_offspring.
     * GET|HEAD /tenantMinorOffsprings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var tenant_minor_offspring $tenantMinorOffspring */
        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->find($id);

        if (empty($tenantMinorOffspring)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantMinorOffsprings.singular')])
            );
        }

        return $this->sendResponse(
            new tenant_minor_offspringResource($tenantMinorOffspring),
            __('messages.retrieved', ['model' => __('models/tenantMinorOffsprings.singular')])
        );
    }

    /**
     * Update the specified tenant_minor_offspring in storage.
     * PUT/PATCH /tenantMinorOffsprings/{id}
     *
     * @param int $id
     * @param Updatetenant_minor_offspringAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_minor_offspringAPIRequest $request)
    {
        $input = $request->all();

        /** @var tenant_minor_offspring $tenantMinorOffspring */
        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->find($id);

        if (empty($tenantMinorOffspring)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantMinorOffsprings.singular')])
            );
        }

        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->update($input, $id);

        return $this->sendResponse(
            new tenant_minor_offspringResource($tenantMinorOffspring),
            __('messages.updated', ['model' => __('models/tenantMinorOffsprings.singular')])
        );
    }

    /**
     * Remove the specified tenant_minor_offspring from storage.
     * DELETE /tenantMinorOffsprings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var tenant_minor_offspring $tenantMinorOffspring */
        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->find($id);

        if (empty($tenantMinorOffspring)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/tenantMinorOffsprings.singular')])
            );
        }

        $tenantMinorOffspring->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/tenantMinorOffsprings.singular')])
        );
    }
}
