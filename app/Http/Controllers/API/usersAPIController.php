<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateusersAPIRequest;
use App\Http\Requests\API\UpdateusersAPIRequest;
use App\Models\users;
use App\Repositories\usersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\usersResource;
use Response;

/**
 * Class usersController
 * @package App\Http\Controllers\API
 */

class usersAPIController extends AppBaseController
{
    /** @var  usersRepository */
    private $usersRepository;

    public function __construct(usersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * Display a listing of the users.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->usersRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            usersResource::collection($users),
            __('messages.retrieved', ['model' => __('models/users.plural')])
        );
    }

    /**
     * Store a newly created users in storage.
     * POST /users
     *
     * @param CreateusersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateusersAPIRequest $request)
    {
        $input = $request->all();

        $users = $this->usersRepository->create($input);

        return $this->sendResponse(
            new usersResource($users),
            __('messages.saved', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Display the specified users.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var users $users */
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        return $this->sendResponse(
            new usersResource($users),
            __('messages.retrieved', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Update the specified users in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateusersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateusersAPIRequest $request)
    {
        $input = $request->all();

        /** @var users $users */
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        $users = $this->usersRepository->update($input, $id);

        return $this->sendResponse(
            new usersResource($users),
            __('messages.updated', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Remove the specified users from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var users $users */
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        $users->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/users.singular')])
        );
    }
}
