<?php

namespace App\Http\Controllers;

use App\DataTables\BankDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBankRequest;
use App\Http\Requests\UpdateBankRequest;
use App\Models\Bank;
use App\Repositories\BankRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Facades\Excel;
use Response;
class BankExport implements FromArray
{
    protected $ary;
    function __construct($array) {
        $this->ary = $array;
    }
    public function array(): array
    {
        return $this->ary;
    }
}


class BankController extends AppBaseController
{
    /** @var  BankRepository */
    private $bankRepository;

    public function __construct(BankRepository $bankRepo)
    {
        $this->bankRepository = $bankRepo;
    }

    /**
     * Display a listing of the Bank.
     *
     * @param BankDataTable $bankDataTable
     * @return Response
     */
    public function index(BankDataTable $bankDataTable)
    {
        return $bankDataTable->render('banks.index');
    }

    /**
     * Show the form for creating a new Bank.
     *
     * @return Response
     */
    public function create()
    {
        return view('banks.create');
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param CreateBankRequest $request
     *
     * @return Response
     */
    public function store(CreateBankRequest $request)
    {
        $input = $request->all();

        $bank = $this->bankRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/banks.singular')]));

        return redirect(route('banks.index'));
    }

    /**
     * Display the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error(__('messages.not_found', ['model' => __('models/banks.singular')]));

            return redirect(route('banks.index'));
        }

        return view('banks.show')->with('bank', $bank);
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error(__('messages.not_found', ['model' => __('models/banks.singular')]));

            return redirect(route('banks.index'));
        }

        return view('banks.edit')->with('bank', $bank);
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param  int              $id
     * @param UpdateBankRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBankRequest $request)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error(__('messages.not_found', ['model' => __('models/banks.singular')]));

            return redirect(route('banks.index'));
        }

        $bank = $this->bankRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/banks.singular')]));

        return redirect(route('banks.index'));
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error(__('messages.not_found', ['model' => __('models/banks.singular')]));

            return redirect(route('banks.index'));
        }

        $this->bankRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/banks.singular')]));

        return redirect(route('banks.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('Bank'), $file);

    }

    public static function processExcel($file)
    {
        $results = []; // a arrany of { line: number, case_num: string, result: "inserted" | "skipped" }
        // dd($request->file("user_csv"));
        $path = $file->getRealPath();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load($path);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();
        foreach ($rows as $key => $row) {
            if ($key == 0) {
                continue;
            }
            if (!$row[0]) {
                continue;
            }
            try {
                $bank = new Bank();
                $bank->bank_code = $row[0];
                $bank->bank_account = $row[1];
                $bank->tax = $row[2];
                $bank->price = $row[3];
                $bank->user_number = $row[4];
                $bank->company_stack_code = $row[5] ?: null;
                $bank->Creator = $row[6] ?: null;
                $bank->search = $row[7] ?: null;
                $bank->bank_noted = $row[8] ?: null;
                $bank->save();
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $row[1],
                    'result' => '新增成功',
                ];
            }catch (\Exception $e) {
                continue;
            }

        }
        return $results;
    }
    public function generateData() {
        $txs = \App\Models\TransactionDetails::where('t_date', '>', \Carbon\Carbon::now()->subMonth())->get()->map(function ($tx) {
            return substr($tx->remark_1, -5, 4);
        })->toArray();
        if (request()->input('payday')) {
            $banks = Bank::whereHas('contract', function ($q) {
                $q->where('contracts.to_the_landlord_rent_day', request()->input('payday'));
            })->get();
        } else {
            $banks = Bank::all();
        }

        $data = [
            ['收受行代號', '收受者帳號', '收受者統編', '金額', '物件編號', '公司股市代號', '發動者專用區', '查詢專用區', '存摺摘要', '付款狀態']
        ];

        foreach ($banks as $bank) {
            $paied = in_array(substr(strval($bank->user_number), -4), $txs);
            $data[] = [
                strval($bank->bank_code),
                strval($bank->bank_account),
                strval($bank->tax),
                strval($bank->price),
                strval($bank->user_number),
                strval($bank->company_stack_code),
                strval($bank->Creator),
                strval($bank->search),
                strval($bank->bank_noted),
                $paied ? '已付款' : '未付款',
            ];
        }
        $export = new BankExport($data);
        return Excel::download($export, 'output.xlsx');
    }

}
