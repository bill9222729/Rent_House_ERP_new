<?php

namespace App\Http\Controllers;

use App\DataTables\BuildingManagementDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBuildingManagementRequest;
use App\Http\Requests\UpdateBuildingManagementRequest;
use App\Models\BuildingManagement;
use App\Repositories\BuildingManagementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Http\Request;
use Response;
use App\Models\TransactionDetails;

class BillExport implements FromArray
{
    protected $ary;
    function __construct($array)
    {
        $this->ary = $array;
    }
    public function array(): array
    {
        return $this->ary;
    }
}

class bExport implements FromArray, WithEvents
{
    protected $ary;
    function __construct($array)
    {
        $this->ary = $array;
    }
    public function array(): array
    {
        return $this->ary;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function ($event) {
                //設定區域單元格垂直居中
                $event->sheet->getDelegate()->getStyle('A1:Z1265')->getAlignment()->setVertical('center');
                //設定區域單元格字型、顏色、背景等，其他設定請檢視 applyFromArray 方法，提供了註釋
                $event->sheet->getDelegate()->getStyle('A1:Z1')->applyFromArray([
                    'font' => [
                        'name' => 'Arial',
                        'bold' => true,
                        'italic' => false,
                        'strikethrough' => false,
                        'color' => [
                            'rgb' => 'FFFFFF'
                        ]
                    ],
                    'fill' => [
                        'fillType' => 'linear', //線性填充，類似漸變
                        'rotation' => 45, //漸變角度
                        'startColor' => [
                            'rgb' => '54AE54' //初始顏色
                        ],
                        //結束顏色，如果需要單一背景色，請和初始顏色保持一致
                        'endColor' => [
                            'argb' => '54AE54'
                        ]
                    ]
                ]);
            }
        ];
    }
}


class BuildingManagementController extends AppBaseController
{
    /** @var  BuildingManagementRepository */
    private $buildingManagementRepository;

    public function __construct(BuildingManagementRepository $buildingManagementRepo)
    {
        $this->buildingManagementRepository = $buildingManagementRepo;
    }

    /**
     * Display a listing of the BuildingManagement.
     *
     * @param BuildingManagementDataTable $buildingManagementDataTable
     * @return Response
     */
    public function index(BuildingManagementDataTable $buildingManagementDataTable)
    {
        return $buildingManagementDataTable->render('building_managements.index');
    }

    /**
     * Show the form for creating a new BuildingManagement.
     *
     * @return Response
     */
    public function create()
    {
        return view('building_managements.create');
    }

    /**
     * Store a newly created BuildingManagement in storage.
     *
     * @param CreateBuildingManagementRequest $request
     *
     * @return Response
     */
    public function store(CreateBuildingManagementRequest $request)
    {
        $input = $request->all();
        if (isset($input['same_address'])) {
            if ($input['same_address'] == 'on') {
                $input['residence_address_city'] = $input['address_city'];
                $input['residence_address_cityarea'] = $input['address_cityarea'];
                $input['residence_address_street'] = $input['address_street'];
                $input['residence_address_ln'] = $input['address_ln'];
                $input['residence_address_aly'] = $input['address_aly'];
                $input['residence_address_num'] = $input['address_num'];
                $input['residence_address_num_hyphen'] = $input['address_num_hyphen'];
            }
        }
        if (isset($input['pay_by_tenant'])) {
            if ($input['pay_by_tenant'] == 'on') {
                $input['manage_fee_month'] = 0;
            }
        }
        $input['Lessor_birthday'] = self::Format_TW_Date($input['Lessor_birthday']);
        $input['apply_date'] = self::Format_TW_Date($input['apply_date']);
        $input['h_info_completion_date'] = self::Format_TW_Date($input['h_info_completion_date']);
        $input['remark_1'] = self::Format_TW_Date($input['remark_1']);
        $input['files'] = $this->createFilesJson($request);

        $buildingManagement = $this->buildingManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/buildingManagements.singular')]));

        return redirect(route('buildingManagements.index'));
    }

    /**
     * Display the specified BuildingManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $buildingManagement = $this->buildingManagementRepository->find($id);

        if (empty($buildingManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagements.singular')]));

            return redirect(route('buildingManagements.index'));
        }

        return view('building_managements.edit')->with(['buildingManagement' => $buildingManagement, 'show' => true]);
    }

    /**
     * Show the form for editing the specified BuildingManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $buildingManagement = $this->buildingManagementRepository->find($id);

        if (empty($buildingManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagements.singular')]));

            return redirect(route('buildingManagements.index'));
        }

        return view('building_managements.edit')->with('buildingManagement', $buildingManagement);
    }

    public function update(Request $request, $id)
    {
        $buildingManagement = $this->buildingManagementRepository->find($id);

        if (empty($buildingManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagements.singular')]));

            return redirect(route('buildingManagements.index'));
        }
        $input = $request->all();
        if (isset($input['pay_by_tenant'])) {
            if ($input['pay_by_tenant'] == 'on') {
                $input['manage_fee_month'] = 0;
            }
        }

        //        // 處理檔案json
        //        if ($buildingManagement['file_json'] != null && $input['file_json'] != null) {
        //            $database_file_json = json_decode($buildingManagement['file_json']);
        //            $new__file_json = json_decode($input['file_json']);
        //            foreach ($new__file_json as $key => $arr) {
        //                foreach ($arr as $index => $value) {
        //                    array_push($database_file_json->$key, $value);
        //                }
        //            }
        //            $input['file_json'] = json_encode($database_file_json);
        //        }

        $input['Lessor_birthday'] = isset($input['Lessor_birthday']) ? self::Format_TW_Date($input['Lessor_birthday']) : null;
        $input['apply_date'] = self::Format_TW_Date($input['apply_date']);
        $input['h_info_completion_date'] = self::Format_TW_Date($input['h_info_completion_date']);
        $input['remark_1'] = self::Format_TW_Date($input['remark_1']);
        $input['files'] = $this->createFilesJson($request, $buildingManagement);
        $buildingManagement = $this->buildingManagementRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/buildingManagements.singular')]));

        return redirect(route('buildingManagements.index'));
    }

    /**
     * Remove the specified BuildingManagement from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $buildingManagement = $this->buildingManagementRepository->find($id);

        if (empty($buildingManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagements.singular')]));

            return redirect(route('buildingManagements.index'));
        }

        $this->buildingManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/buildingManagements.singular')]));

        return redirect(route('buildingManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('BuildingManagement'), $file);
    }

    public function import_build(Request $request)
    {
        return self::processExcel($request->file('import_file'));
    }

    public static function processExcel($file)
    {
        $results = [];

        $path = $file->getRealPath();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setLoadSheetsOnly(["包租"]);
        $spreadsheet = $reader->load($path);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();
        foreach ($rows as $key => $row) {
            if ($key === 0 || $key === 1) {
                continue;
            }
            try {
                $find_by_num = BuildingManagement::where('num', $row[10])->first();
                if ($find_by_num) {
                    $results[] = [
                        'line' => $key + 1,
                        'case_num' => $row[10],
                        'result' => '重複輸入',
                    ];
                    continue;
                }
                $build = new BuildingManagement();
                $build->p_code = $row[3];
                $build->sales = $row[8];
                $build->house_id = $row[9];
                $build->num = $row[10];
                $build->v_account = $row[11];
                $build->apply_date = $row[12];
                $build->empty_house = $row[17];
                $build->source = $row[18];
                $build->fire_alert = $row[19] === 'V' ? '有' : '無';
                $build->fire_alert_in_house = $row[20] === 'V' ? '有' : '無';
                $build->equipment_water_heater = intval($row[21]);
                $build->notary = $row[23] === 'V' ? '是' : '否';
                $build->n_name = $row[24];
                $build->address_cityarea = $row[25];
                $build->h_info_warrant_ping_num = $row[27];
                $build->h_info_actual_ping_num = $row[28];
                $build->h_info_usage = $row[29];
                $build->landlord_expect_rent = $row[32];
                $build->manage_fee_month = $row[34];
                $build->car_position = $row[35];
                $build->Lessor_name = $row[36];
                $build->Lessor_ID_num = $row[37];
                $build->remark_1 = $row[42];
                $build->save();

                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $row[10],
                    'result' => '新增成功',
                ];
            } catch (\Exception $exception) {
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $row[10],
                    'result' => $exception->getMessage(),
                ];
                continue;
            }
        }

        $reader->setLoadSheetsOnly(["代管"]);
        $spreadsheet = $reader->load($path);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();

        foreach ($rows as $key => $row) {
            if ($key === 0 || $key === 1) {
                continue;
            }
            try {
                $find_by_num = BuildingManagement::where('num', $row[10])->first();
                if ($find_by_num) {
                    $results[] = [
                        'line' => $key + 1,
                        'case_num' => $row[10],
                        'result' => '重複輸入',
                    ];
                    continue;
                }
                $build = new BuildingManagement();
                $build->p_code = $row[3];
                $build->sales = $row[8];
                $build->house_id = $row[9];
                $build->num = $row[10];
                $build->v_account = $row[11];
                $build->apply_date = $row[12];
                $build->empty_house = $row[17];
                $build->source = $row[18];
                $build->fire_alert = $row[19] === 'V' ? '有' : '無';
                $build->fire_alert_in_house = $row[20] === 'V' ? '有' : '無';
                $build->equipment_water_heater = intval($row[21]);
                $build->notary = $row[23] === 'V' ? '是' : '否';
                $build->n_name = $row[24];
                $build->address_cityarea = $row[25];
                $build->h_info_warrant_ping_num = $row[27];
                $build->h_info_actual_ping_num = $row[28];
                $build->h_info_usage = $row[29];
                $build->landlord_expect_rent = $row[33];
                $build->manage_fee_month = $row[34];
                $build->car_position = $row[35];
                $build->Lessor_name = $row[36];
                $build->Lessor_ID_num = $row[37];
                $build->email = $row[42];
                $build->remark_1 = $row[43];
                $build->save();

                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $row[10],
                    'result' => '新增成功',
                ];
            } catch (\Exception $exception) {
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $row[10],
                    'result' => $exception->getMessage(),
                ];
                continue;
            }
        }
        return $results;
    }

    public function generateBill()
    {
        $transaction = TransactionDetails::all();
        $arr = array();
        // 列出所有交易明細裡的物件編號4碼
        foreach ($transaction as $item) {
            $build_id = substr($item->remark_1, 11, -1); // 取12-15字元
            $arr[] = $build_id;
        }
        $data = [
            ['收受行代號', '收受者帳號', '收受者統編', '金額', '用戶號碼', '公司股市代號', '發動者專用區', '查詢專用區', '存摺摘要']
        ];
        $buildings = BuildingManagement::all();
        foreach ($buildings as $building) {
            $build_num = substr($building->num, -4);
            //【物件編號】取末4碼 接著與 交易明細裡的物件編號4碼 去比較
            if ($building->re_type == '代管' && !in_array($build_num, $arr)) {
                continue;
            }
            $data[] = [
                strval($building->agent_branch_name),
                strval($building->landlord_bank_account),
                strval($building->Lessor_ID_num),
                strval($building->landlord_expect_rent),
            ];
        }
        $export = new BillExport($data);
        return Excel::download($export, 'output.xlsx');
    }

    private static function Format_TW_Date($date_string)
    {
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list, "/")) {
            $date_list = explode("/", $date_list);
        } else if (strpos($date_list, ".")) {
            $date_list = explode(".", $date_list);
        } else if (strpos($date_list, "-")) {
            $date_list = explode("-", $date_list);
        } else {
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        return $date;
    }

    private static function fixPattern($str)
    {
        if ($str == "套房" || $str == "套") return "1";
        if ($str == "雅房" || $str == "雅") return "2";
        if ($str == "1房") return "3";
        if ($str == "2房") return "4";
        if ($str == "3房") return "5";
        if ($str == "4房" || $str == "4房以上") return "6";
        return $str;
    }

    private static function fixType($str)
    {
        if ($str == "公寓") return "1";
        if ($str == "電梯大樓") return "2";
        if ($str == "透天") return "3";
        if ($str == "平房") return "4";
        return $str;
    }

    private static function Yes_Or_No($string)
    {
        switch ($string) {
            case "x":
            case "否":
            case "0":
                return 0;
            case "o":
            case "是":
            case "1":
                return 1;
        }
        return 0;
    }

    private function createFilesJson(Request $request, $building = null)
    {
        $files = [
            'lessor_application' => [],
            'com_rent' => [],
            'com_manage' => [],
            'identity' => [],
            'building' => [],
            'house_tax' => [],
            'land_tax' => [],
            'water_fee' => [],
            'ele_fee' => [],
            'bank_account' => [],
            'house_checklist' => [],
            'furniture_checklist' => [],
            'picture' => [],
            'other' => [],
        ];

        if ($building) {
            $files = $building->files ? json_decode($building->files, true) : $files;
        }

        foreach ($files as $key => $value) {
            if ($request->hasFile($key)) {
                $files[$key][] = $request->file($key)->store('build', 'public');
            }
        }

        return json_encode($files);
    }

    public function generateT(Request $request, $tId)
    {
        $build = BuildingManagement::where('id', $tId)->first();
        $arr = array();
        $data = [
            [
                '申請日期' . "\r\n" . '(yyy/mm/dd)',
                '本人欲提供住宅' . "\r\n" . '出租予租屋服務事業再轉租(包租)' . "\r\n" . '0：否   1：是',
                '本人欲提供住宅' . "\r\n" . '經由租屋服務事業協助出租(代管)' . "\r\n" . '0：否   1：是',
                '申請單類型' . "\r\n" . '1:自然人' . "\r\n" . '2:法人',
                '地址' . "\r\n" . '縣市',
                '地址' . "\r\n" . '鄉鎮區',
                '地址' . "\r\n" . '街道',
                '地址' . "\r\n" . '巷',
                '地址' . "\r\n" . '弄',
                '地址' . "\r\n" . '號',
                '地址' . "\r\n" . '之Ｏ',
                '建物坐' . "\r\n" . '落' . "\r\n" . "縣市",
                '建物坐落' . "\r\n" . '鄉鎮區',
                '建物坐落' . "\r\n" . '段號',
                '建物坐落' . "\r\n" . '地段小段' . "\r\n" . "(若有填" . "\r\n" . "寫段號，" . "\r\n" . "以段號為" . "\r\n" . "主)",
                '建物坐落' . "\r\n" . '地號',
                '建號',
                '格局' . "\r\n" . "1: 套房" . "\r\n" . "2: 雅房" . "\r\n" . "3: 1房" . "\r\n" . "4: 2房" . "\r\n" . "5: 3房" . "\r\n" . "6: 4房以上" . "\r\n" . "99: 其他",
                '格局-其他備註',
                '房屋類型' . "\r\n" . "1: 公寓" . "\r\n" . "2: 電梯大樓" . "\r\n" . "3: 透天厝" . "\r\n" . "4: 平房",
                '屋齡',
                '建築物完成日' . "\r\n" . "期" . "\r\n" . "(yyy/mm/dd)",
                '樓層' . "\r\n" . "第Ｏ層" . "\r\n" . "(透天請填寫1" . "\r\n" . ")",
                '樓層' . "\r\n" . "之Ｏ",
                '樓層' . "\r\n" . "室/房號",
                '樓層' . "\r\n" . "共Ｏ層",
                '隔間' . "\r\n" . "Ｏ房",
                '隔間' . "\r\n" . "Ｏ廳",
                '隔間' . "\r\n" . "Ｏ衛",
                '隔間材質',
                '權狀坪數' . "\r\n" . "(平方公" . "\r\n" . "尺)",
                '實際使用' . "\r\n" . "坪數(平" . "\r\n" . "方公尺)",
                '建物主要' . "\r\n" . "用途",
                '建材',
                '房東期待租金',
                '押金Ｏ個月',
                '押金ＯＯＯ元',
                '是否可' . "\r\n" . "議價" . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '管理費' . "\r\n" . "(AM、AN請依" . "\r\n" . "照計價方式則" . "\r\n" . "一填寫)" . "\r\n" . '0：不包含' . "\r\n" . '1：包含',
                '管理費-' . "\r\n" . "每月Ｏ元",
                '管理費-' . "\r\n" . "每坪Ｏ元",
                '是否包含' . "\r\n" . "電費" . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '是否可炊' . "\r\n" . "煮" . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '是否包含水' . "\r\n" . "費" . "\r\n" . '0：否 1：是',
                '是否包含車位' . "\r\n" . '0：否 1：是',
                '是否有無障礙設施' . "\r\n" . '0：否 1：是',
                '是否包含瓦斯/' . "\r\n" . "天然瓦斯"  . "\r\n" . '0：否 1：是',
                '是否包' . "\r\n" . "含第四"  . "\r\n" . '臺' . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '是否包含網路' . "\r\n" . '0：否 1：是',
                '是否包含清潔' . "\r\n" . "費" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "電視" . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '提供設備' . "\r\n" . "冰箱" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "有線電視(第四臺)" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "冷氣" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "熱水器" . "\r\n" . '0：否 1：是',
                '提供設' . "\r\n" . "備" . "\r\n" .  "網際網" . "\r\n" .  '路' . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '提供設備' . "\r\n" . "洗衣機" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "天然瓦斯" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "床" . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '提供設備' . "\r\n" . "衣櫃" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "桌" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "椅" . "\r\n" . '0：否 1：是',
                '提供設備' . "\r\n" . "沙發" . "\r\n" . '0：否 1：是',
                '提供設' . "\r\n" . "備" . "\r\n" .  "其他" . "\r\n" . '0：否' . "\r\n" .  '1：是',
                '提供設備' . "\r\n" . "其他(請描述)",
                '門禁管理' . "\r\n" . '0：無 1：有' . "\r\n" .  '(若有，請續' . "\r\n" . "填BL~BO)",
                '門禁管理' . "\r\n" . "管理員" . "\r\n" . '0：無' . "\r\n" .  '1：有',
                '門禁管理' . "\r\n" . "刷卡門禁" . "\r\n" . '0：無' . "\r\n" .  '1：有',
                '門禁管理' . "\r\n" . "其他" . "\r\n" . '0：無' . "\r\n" .  '1：有',
                '門禁管理' . "\r\n" . "其他(請描述" . "\r\n" . ')',
            ]
        ];
        $data[] = [
            strval($this->tranToTwYear($build->apply_date)),
            strval($build->sublet),
            strval($build->rent),
            strval($build->request_form_type),
            strval($build->address_city),
            strval($build->address_cityarea),
            strval($build->address_street),
            strval($build->address_ln),
            strval($build->address_aly),
            strval($build->address_num),
            strval($build->address_num_hyphen),
            strval($build->located_city),
            strval($build->located_cityarea),
            strval($build->located_segment_num),
            strval($build->located_small_lot),
            strval($build->located_land_num),
            strval($build->located_build_num),
            strval($build->pattern),
            strval($build->pattern_remark),
            strval($build->h_info_type),
            strval($build->h_info_age),
            strval($this->tranToTwYear($build->h_info_completion_date)),
            strval($build->h_info_fl),
            strval($build->h_info_hyphen),
            strval($build->h_info_suite),
            strval($build->h_info_total_fl),
            strval($build->h_info_pattern_room),
            strval($build->h_info_pattern_hall),
            strval($build->h_info_pattern_bath),
            strval($build->h_info_pattern_material),
            strval($build->h_info_warrant_ping_num),
            strval($build->h_info_actual_ping_num),
            strval($build->h_info_usage),
            strval($build->h_info_material),
            strval($build->landlord_expect_rent),
            strval($build->landlord_expect_deposit_month),
            strval($build->landlord_expect_deposit),
            strval($build->landlord_expect_bargain),
            strval($build->manage_fee),
            strval($build->manage_fee_month),
            strval($build->manage_fee_ping),
            strval($build->contain_fee_ele),
            strval($build->can_Cook),
            strval($build->contain_fee_water),
            strval($build->parking),
            strval($build->Barrier_free_facility),
            strval($build->contain_fee_gas),
            strval($build->contain_fee_pay_tv),
            strval($build->contain_fee_net),
            strval($build->contain_fee_clean),
            strval($build->equipment_tv),
            strval($build->equipment_refrigerator),
            strval($build->equipment_pay_tv),
            strval($build->equipment_air_conditioner),
            strval($build->equipment_water_heater),
            strval($build->equipment_net),
            strval($build->equipment_washer),
            strval($build->equipment_gas),
            strval($build->equipment_bed),
            strval($build->equipment_wardrobe),
            strval($build->equipment_table),
            strval($build->equipment_chair),
            strval($build->equipment_sofa),
            strval($build->equipment_other),
            strval($build->equipment_other_detail),
            strval($build->curfew),
            strval($build->curfew_management),
            strval($build->curfew_card),
            strval($build->curfew_other),
            strval($build->curfew_other_detail),
        ];

        // $data[] = [""];

        // $data[] = [
        //     '地址' . "\r\n" . '巷',
        //     '地址' . "\r\n" . '弄',
        //     '地址' . "\r\n" . '號',
        //     '地址' . "\r\n" . '之Ｏ',
        //     '建物坐' . "\r\n" . '落' . "\r\n" . "縣市",
        //     '建物坐落' . "\r\n" . '鄉鎮區',
        //     '建物坐落' . "\r\n" . '段號',
        //     '建物坐落' . "\r\n" . '地段小段' . "\r\n" . "(若有填" . "\r\n" . "寫段號，" . "\r\n" . "以段號為" . "\r\n" . "主)",
        //     '建物坐落' . "\r\n" . '地號',
        // ];

        // $data[] = [
        //     strval($build->address_ln),
        //     strval($build->address_aly),
        //     strval($build->address_num),
        //     strval($build->address_num_hyphen),
        //     strval($build->located_city),
        //     strval($build->located_cityarea),
        //     strval($build->located_segment_num),
        //     strval($build->located_small_lot),
        //     strval($build->located_land_num),
        // ];

        // $data[] = [""];

        // $data[] = [
        //     '建號',
        //     '格局' . "\r\n" . "1: 套房" . "\r\n" . "2: 雅房" . "\r\n" . "3: 1房" . "\r\n" . "4: 2房" . "\r\n" . "5: 3房" . "\r\n" . "6: 4房以上" . "\r\n" . "99: 其他",
        //     '格局-其他備註',
        //     '房屋類型' . "\r\n" . "1: 公寓" . "\r\n" . "2: 電梯大樓" . "\r\n" . "3: 透天厝" . "\r\n" . "4: 平房",
        //     '屋齡',
        //     '建築物完成日' . "\r\n" . "期" . "\r\n" . "(yyy/mm/dd)",
        //     '樓層' . "\r\n" . "第Ｏ層" . "\r\n" . "(透天請填寫1" . "\r\n" . ")",
        // ];

        // $data[] = [
        //     strval($build->located_build_num),
        //     strval($build->pattern),
        //     strval($build->pattern_remark),
        //     strval($build->h_info_type),
        //     strval($build->h_info_age),
        //     strval($this->tranToTwYear($build->h_info_completion_date)),
        //     strval($build->h_info_fl),
        // ];

        // $data[] = [""];

        // $data[] = [
        //     '樓層' . "\r\n" . "之Ｏ",
        //     '樓層' . "\r\n" . "室/房號",
        //     '樓層' . "\r\n" . "共Ｏ層",
        //     '隔間' . "\r\n" . "Ｏ房",
        //     '隔間' . "\r\n" . "Ｏ廳",
        //     '隔間' . "\r\n" . "Ｏ衛",
        //     '隔間材質',
        //     '權狀坪數' . "\r\n" . "(平方公" . "\r\n" . "尺)",
        //     '實際使用' . "\r\n" . "坪數(平" . "\r\n" . "方公尺)",
        //     '建物主要' . "\r\n" . "用途",
        // ];

        // $data[] = [
        //     strval($build->h_info_hyphen),
        //     strval($build->h_info_suite),
        //     strval($build->h_info_total_fl),
        //     strval($build->h_info_pattern_room),
        //     strval($build->h_info_pattern_hall),
        //     strval($build->h_info_pattern_bath),
        //     strval($build->h_info_pattern_material),
        //     strval($build->h_info_warrant_ping_num),
        //     strval($build->h_info_actual_ping_num),
        //     strval($build->h_info_usage),
        // ];

        // $data[] = [""];

        // $data[] = [
        //     '建材',
        //     '房東期待租金',
        //     '押金Ｏ個月',
        //     '押金ＯＯＯ元',
        //     '是否可' . "\r\n" . "議價" . "\r\n" . '0：否' . "\r\n" .  '1：是',
        //     '管理費' . "\r\n" . "(AM、AN請依" . "\r\n" . "照計價方式則" . "\r\n" . "一填寫)" . "\r\n" . '0：不包含' . "\r\n" . '1：包含',
        //     '管理費-' . "\r\n" . "每月Ｏ元",
        //     '是否包含' . "\r\n" . "電費" . "\r\n" . '0：否' . "\r\n" .  '1：是',
        //     '是否可炊' . "\r\n" . "煮" . "\r\n" . '0：否' . "\r\n" .  '1：是',
        // ];

        // $data[] = [
        //     strval($build->h_info_material),
        //     strval($build->landlord_expect_rent),
        //     strval($build->landlord_expect_deposit_month),
        //     strval($build->landlord_expect_deposit),
        //     strval($build->landlord_expect_bargain),
        //     strval($build->manage_fee),
        //     strval($build->manage_fee_month),
        //     strval($build->contain_fee_ele),
        //     strval($build->can_Cook),
        // ];

        // $data[] = [""];

        // $data[] = [
        //     '是否包含水' . "\r\n" . "費" . "\r\n" . '0：否 1：是',
        //     '是否包含車位' . "\r\n" . '0：否 1：是',
        //     '是否有無障礙設施' . "\r\n" . '0：否 1：是',
        //     '是否包含瓦斯/'. "\r\n" . "天然瓦斯"  . "\r\n" . '0：否 1：是',
        //     '是否包'. "\r\n" . "含第四"  . "\r\n" . '臺' . "\r\n" . '0：否' . "\r\n" .  '1：是',
        //     '是否包含網路' . "\r\n" . '0：否 1：是',
        //     '是否包含清潔' . "\r\n" . "費" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "電視" . "\r\n" . '0：否' . "\r\n" .  '1：是',
        // ];

        // $data[] = [
        //     strval($build->contain_fee_water),
        //     strval($build->parking),
        //     strval($build->Barrier_free_facility),
        //     strval($build->contain_fee_gas),
        //     strval($build->contain_fee_pay_tv),
        //     strval($build->contain_fee_net),
        //     strval($build->contain_fee_clean),
        //     strval($build->equipment_tv),
        // ];

        // $data[] = [""];

        // $data[] = [
        //     '提供設備' . "\r\n" . "冰箱" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "有線電視(第四臺)" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "冷氣" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "熱水器" . "\r\n" . '0：否 1：是',
        //     '提供設' . "\r\n" . "備" . "\r\n" .  "網際網" . "\r\n" .  '路' . "\r\n" . '0：否' . "\r\n" .  '1：是',
        //     '提供設備' . "\r\n" . "洗衣機" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "天然瓦斯" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "床" . "\r\n" . '0：否' . "\r\n" .  '1：是',
        // ];

        // $data[] = [
        //     strval($build->equipment_refrigerator),
        //     strval($build->equipment_pay_tv),
        //     strval($build->equipment_air_conditioner),
        //     strval($build->equipment_water_heater),
        //     strval($build->equipment_net),
        //     strval($build->equipment_washer),
        //     strval($build->equipment_gas),
        //     strval($build->equipment_bed),
        // ];

        // $data[] = [""];

        // $data[] = [
        //     '提供設備' . "\r\n" . "衣櫃" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "桌" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "椅" . "\r\n" . '0：否 1：是',
        //     '提供設備' . "\r\n" . "沙發" . "\r\n" . '0：否 1：是',
        //     '提供設' . "\r\n" . "備" . "\r\n" .  "其他" . "\r\n" . '0：否' . "\r\n" .  '1：是',
        //     '提供設備' . "\r\n" . "其他(請描述)",
        //     '門禁管理' . "\r\n" . '0：無 1：有' . "\r\n" .  '(若有，請續' . "\r\n" . "填BL~BO)",
        //     '門禁管理' . "\r\n" . "管理員" . "\r\n" . '0：無' . "\r\n" .  '1：有',
        //     '門禁管理' . "\r\n" . "刷卡門禁" . "\r\n" . '0：無' . "\r\n" .  '1：有',
        //     '門禁管理' . "\r\n" . "其他" . "\r\n" . '0：無' . "\r\n" .  '1：有',
        //     '門禁管理' . "\r\n" . "其他(請描述" . "\r\n" . ')',
        //    ];

        // $data[] = [
        //     strval($build->equipment_wardrobe),
        //     strval($build->equipment_table),
        //     strval($build->equipment_chair),
        //     strval($build->equipment_sofa),
        //     strval($build->equipment_other),
        //     strval($build->equipment_other_detail),
        //     strval($build->curfew),
        //     strval($build->curfew_management),
        //     strval($build->curfew_card),
        //     strval($build->curfew_other),
        //     strval($build->curfew_other_detail),
        // ];

        // dd($data);
        $export = new bExport($data);
        return Excel::download($export, 'output.xls');
    }


    private function tranToTwYear($date)
    {
        if (!$date) {
            return null;
        }
        $dateArr = explode(" ", $date);
        $dateArr = explode("-", $dateArr[0]);
        $date_Y = (int)$dateArr[0];
        $date_Y = $date_Y - 1911;
        $date_Y = strval($date_Y);
        $TWdate = $date_Y . "/" . $dateArr[1] . "/" . $dateArr[2];
        return $TWdate;
    }
}
