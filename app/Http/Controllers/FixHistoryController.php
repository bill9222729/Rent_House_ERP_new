<?php

namespace App\Http\Controllers;

use App\DataTables\FixHistoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFixHistoryRequest;
use App\Http\Requests\UpdateFixHistoryRequest;
use App\Repositories\FixHistoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FixHistoryController extends AppBaseController
{
    /** @var  FixHistoryRepository */
    private $fixHistoryRepository;

    public function __construct(FixHistoryRepository $fixHistoryRepo)
    {
        $this->fixHistoryRepository = $fixHistoryRepo;
    }

    /**
     * Display a listing of the FixHistory.
     *
     * @param FixHistoryDataTable $fixHistoryDataTable
     * @return Response
     */
    public function index(FixHistoryDataTable $fixHistoryDataTable)
    {
        return $fixHistoryDataTable->render('fix_histories.index');
    }

    /**
     * Show the form for creating a new FixHistory.
     *
     * @return Response
     */
    public function create()
    {
        return view('fix_histories.create');
    }

    /**
     * Store a newly created FixHistory in storage.
     *
     * @param CreateFixHistoryRequest $request
     *
     * @return Response
     */
    public function store(CreateFixHistoryRequest $request)
    {
        $input = $request->all();
        $input['pay_date'] = self::Format_TW_Date($input['pay_date']);
        $input['call_fix_date'] = self::Format_TW_Date($input['call_fix_date']);
        $input['check_date'] = self::Format_TW_Date($input['check_date']);
        $input['fix_date'] = self::Format_TW_Date($input['fix_date']);
        $input['fix_done'] = self::Format_TW_Date($input['fix_done']);
        $input['owner_start_date'] = self::Format_TW_Date($input['owner_start_date']);
        $input['owner_end_date'] = self::Format_TW_Date($input['owner_end_date']);
        $input['change_start_date'] = self::Format_TW_Date($input['change_start_date']);
        $input['change_end_date'] = self::Format_TW_Date($input['change_end_date']);

        $fixHistory = $this->fixHistoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/fixHistories.singular')]));

        return redirect(route('fixHistories.index'));
    }

    /**
     * Display the specified FixHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fixHistory = $this->fixHistoryRepository->find($id);

        if (empty($fixHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixHistories.singular')]));

            return redirect(route('fixHistories.index'));
        }

        return view('fix_histories.show')->with('fixHistory', $fixHistory);
    }

    /**
     * Show the form for editing the specified FixHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $fixHistory = $this->fixHistoryRepository->find($id);

        if (empty($fixHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixHistories.singular')]));

            return redirect(route('fixHistories.index'));
        }

        return view('fix_histories.edit')->with('fixHistory', $fixHistory);
    }

    /**
     * Update the specified FixHistory in storage.
     *
     * @param  int              $id
     * @param UpdateFixHistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFixHistoryRequest $request)
    {
        $fixHistory = $this->fixHistoryRepository->find($id);

        if (empty($fixHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixHistories.singular')]));

            return redirect(route('fixHistories.index'));
        }
        $input = $request->all();
        $input['pay_date'] = self::Format_TW_Date($input['pay_date']);
        $input['call_fix_date'] = self::Format_TW_Date($input['call_fix_date']);
        $input['check_date'] = self::Format_TW_Date($input['check_date']);
        $input['fix_date'] = self::Format_TW_Date($input['fix_date']);
        $input['fix_done'] = self::Format_TW_Date($input['fix_done']);
        $input['owner_start_date'] = self::Format_TW_Date($input['owner_start_date']);
        $input['owner_end_date'] = self::Format_TW_Date($input['owner_end_date']);
        $input['change_start_date'] = self::Format_TW_Date($input['change_start_date']);
        $input['change_end_date'] = self::Format_TW_Date($input['change_end_date']);

        $fixHistory = $this->fixHistoryRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/fixHistories.singular')]));

        return redirect(route('fixHistories.index'));
    }

    /**
     * Remove the specified FixHistory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fixHistory = $this->fixHistoryRepository->find($id);

        if (empty($fixHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixHistories.singular')]));

            return redirect(route('fixHistories.index'));
        }

        $this->fixHistoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/fixHistories.singular')]));

        return redirect(route('fixHistories.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('FixHistory'), $file);

    }

    private static function Format_TW_Date($date_string){
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list,"/")) {
            $date_list = explode("/",$date_list);
        }else if(strpos($date_list,".")){
            $date_list = explode(".",$date_list);
        }else if(strpos($date_list,"-")){
            $date_list = explode("-",$date_list);
        }else{
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        if (count($date_list) == 2) {
            $date = $date_Y . "-" . $date_list[1] . "-01";
        }else{
            $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        }
        return $date;
    }

}
