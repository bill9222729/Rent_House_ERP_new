<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\contract;
use App\Models\TransactionDetails;
//use Google_Client;
//use Google_Service_Drive;
//use Google_Service_Oauth2;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromArray;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class RewardImport implements WithMultipleSheets
{
    public function __construct(array $contracts)
    {
        $this->contracts = $contracts;
    }

    public function sheets(): array
    {
        return [
            new RewardExport($this->contracts),
            new SalesCaseExport($this->contracts)
        ];
    }
}


class SalesCaseExport implements FromArray
{
    public function __construct(array $contracts)
    {
        $this->contracts = $contracts;
    }

    public function array(): array
    {
        $ary = [
            [
                '業務獎金計算表'
            ],
            [
                '業務',
                \Carbon\Carbon::now()->month . '月發放案件數',
                \Carbon\Carbon::now()->month . '月代管費',
                \Carbon\Carbon::now()->month . '月媒合費',
                '合計'
            ],
        ];

        $totalCase = 0;
        $totalLandlordPrice = 0;
        $totalMatchPrice = 0;
        $sales = [];

        foreach ($this->contracts as $contract) {
            $totalCase += 1;
            // $r = ($contract['landlord_types_of'] == '代管' ? 1 : 1.5) * ($contract['landlord_existing_tenant'] == '是' ? 0.5 : 1);
            // $matchPrice = intval($contract['actual_contract_rent_to_landlord']) * $r;
            $matchPrice = intval($contract['actual_contract_rent_to_landlord']) * floatval($contract['business_management']['match_fee_set']);
            // $landlordPrice = intval($contract['actual_contract_rent_to_landlord']) * ($contract['tenant_management']['qualifications'] == '1' ? 0.2 : 0.1);
            $landlordPrice = intval($contract['actual_contract_rent_to_landlord']) * floatval($contract['business_management']['management_fee_set']);
            $salesName = $contract['contract_business_sign_back'];
            $totalLandlordPrice += $landlordPrice;
            $totalMatchPrice += $matchPrice;
            if (data_get($sales, $salesName)) {
                $sales[$salesName]['case'] += 1;
                $sales[$salesName]['landlordPrice'] += $matchPrice;
                $sales[$salesName]['matchPrice'] += $landlordPrice;
            } else {
                $sales[$salesName] = [
                    'case' => 1,
                    'landlordPrice' => $landlordPrice,
                    'matchPrice' => $matchPrice
                ];
            }
        }

        foreach ($sales as $name => $value) {
            $ary[] = [
                $name,
                strval($value['case']),
                strval($value['landlordPrice']),
                strval($value['matchPrice']),
                strval($value['landlordPrice'] + $value['matchPrice'])
            ];
        }

        $ary[] = [
            '合計',
            strval($totalCase),
            strval($totalLandlordPrice),
            strval($totalMatchPrice),
            strval($totalMatchPrice + $totalLandlordPrice)
        ];

        return $ary;
    }

}


class RewardExport implements FromArray
{
    public function __construct(array $contracts)
    {
        $this->contracts = $contracts;
    }

    public function array(): array
    {
        $ary = [
            [
                \Carbon\Carbon::now()->year . '年' . \Carbon\Carbon::now()->month . '月份獎金明細'
            ],
            [
                '件數',
                '媒合編號',
                '業務',
                '簽約日',
                '物件代碼',
                '物件地址',
                '種類',
                '所有權人',
                '類別',
                '承租人',
                '契約起日',
                '~',
                '契約訖日',
                '成交租金',
                '現有房客',
                '包管代管%數',
                '代管費',
                '代管業務獎金5%',
                '代管公司',
                '開發媒合%',
                '實際媒合%',
                '媒合費',
                '業務獎金40%',
                '公司',
                '備註',
            ],
        ];
        $count = 1;
        // dd($this->contracts);
        foreach ($this->contracts as $contract) {
            if ($contract['tenant_management']['qualifications'] == "1") {
                $qualifications = "一般戶";
            } elseif ($contract['tenant_management']['qualifications'] == "2") {
                $qualifications = "第一類";
            } else {
                $qualifications = "第二類";
            }
            $ary[] = [
                $count,
                $contract['landlord_match_number'],
                $contract['contract_business_sign_back'],
                $contract['remark_1'],
                $contract['landlord_item_number'],
                $contract['building_management']['located_build_address'],
                $contract['landlord_types_of'],
                $contract['building_management']['Lessor_name'],
                $qualifications,
                $contract['tenant_management']['lessee_name'],
                $contract['landlord_contract_date'],
                '~',
                $contract['landlord_contract_expiry_date'],
                $contract['actual_contract_rent_to_landlord'],
                $contract['landlord_existing_tenant'],
                $contract['tenant_management']['qualifications'] == '1' ? '20%' : '10%',
                strval($n = intval($contract['actual_contract_rent_to_landlord']) * ($contract['tenant_management']['qualifications'] == '1' ? 0.2 : 0.1)),
                strval($o = intval($contract['actual_contract_rent_to_landlord']) * floatval($contract['business_management']['management_fee_set'])),
                strval($n - $o),
                $contract['landlord_types_of'] == '代管' ? '100%' : '150%',
                strval(($r = ($contract['landlord_types_of'] == '代管' ? 1 : 1.5) * ($contract['landlord_existing_tenant'] == '是' ? 0.5 : 1)) * 100) . '%',
                strval($s = intval($contract['actual_contract_rent_to_landlord']) * $r),
                strval($t = intval($contract['actual_contract_rent_to_landlord']) * floatval($contract['business_management']['match_fee_set'])),
                $s - $t,
                '',
            ];
        }
        $count++;
        // dd( $ary);
        return $ary;
    }
}


class HomeController extends Controller
{
    public $gClient;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
//        $google_redirect_url = route('glogin');
//        $this->gClient = new Google_Client();
//        $this->gClient->setApplicationName('test');
//        $this->gClient->setClientId(env('GOOGLE_CLIENT_ID'));
//        $this->gClient->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
//        $this->gClient->setRedirectUri($google_redirect_url);
//        $this->gClient->setDeveloperKey(env('GOOGLE_API_KEY'));
//        $this->gClient->setScopes(array(
//            'https://www.googleapis.com/auth/drive.file',
//            'https://www.googleapis.com/auth/drive'
//        ));
//        $this->gClient->setAccessType("offline");
//        $this->gClient->setApprovalPrompt("force");
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index()
    {

        $contractsCreate30Days = contract::with('tenantManagement')->whereDate('landlord_contract_date', '>', Carbon::now()->subDays(30))->get();
        $contractsEnd30Days = contract::with('tenantManagement')->whereDate('landlord_contract_expiry_date', '>=', Carbon::now())->whereDate('landlord_contract_expiry_date', '<=', Carbon::now()->addDays(60))->get();
        $transaction = TransactionDetails::all();
        $transactionArr = array();
        $payedContracts = array();
        $notPayContracts = array();
        // 列出所有交易明細裡的物件編號4碼
        foreach ($transaction as $item) {
            $build_id = substr($item->remark_1, -5, 4);// 取12-15字元
            $transactionArr[$build_id] = $item->t_date;
        }

        foreach ($contractsCreate30Days as $contract) {
            $landlord_item_number = substr($contract->landlord_item_number, -4);
            if (data_get($transactionArr, $landlord_item_number)) {
                $payedContracts[] = [
                    "payed_at" => $transactionArr[$landlord_item_number],
                    "price" => $contract->actual_contract_rent_to_landlord,
                    "item_no" => $contract->landlord_item_number,
                    "t_name" => $contract->tenantManagement ? $contract->tenantManagement->lessee_name : null,
                    "t_phone" => $contract->tenantManagement ? $contract->tenantManagement->lessee_cellphone : null,
                    "sales" => $contract->contract_business_sign_back
                ];
            } else {

                $notPayContracts[] = [
                    "price" => $contract->actual_contract_rent_to_landlord,
                    "item_no" => $contract->landlord_item_number,
                    "t_name" => $contract->tenantManagement ? $contract->tenantManagement->lessee_name : null,
                    "t_phone" => $contract->tenantManagement ? $contract->tenantManagement->lessee_cellphone : null,
                    "sales" => $contract->contract_business_sign_back
                ];
            }
        }


        return view('home')->with([
            "contractsEnd30Days" => $contractsEnd30Days,
            "payedContracts" => $payedContracts,
            "notPayContracts" => $notPayContracts
        ]);
    }

    public function reward()
    {
        $txs = \App\Models\TransactionDetails::where('t_date', '>', \Carbon\Carbon::now()->subMonth())->where('income', '>', 0)->get()->map(function ($tx) {
            return substr($tx->remark_1, -5, 4);
        })->toArray();
        // dd($txs);
        $cc = contract::where('landlord_contract_expiry_date', '>', \Carbon\Carbon::now())->with(['tenantManagement', 'buildingManagement', 'businessManagement'])->get();
        $contracts = contract::where('landlord_contract_expiry_date', '>',
            \Carbon\Carbon::now())->with(['tenantManagement', 'buildingManagement', 'businessManagement'])
            ->get()
            ->filter(function ($contract) use ($txs) {
                return $contract->buildingManagement && in_array(substr($contract->buildingManagement->num, -4, 4), $txs);
            });
        // dd($cc);
        return Excel::download(new RewardImport($contracts->toArray()), \Carbon\Carbon::now()->year . '-' . \Carbon\Carbon::now()->month . '月' . '獎金計算.xlsx');
    }


    public function googleLogin(Request $request)  {

        $google_oauthV2 = new Google_Service_Oauth2($this->gClient);
        if ($request->get('code')){
            $this->gClient->authenticate($request->get('code'));
            $request->session()->put('token', $this->gClient->getAccessToken());
        }
        if ($request->session()->get('token'))
        {
            $this->gClient->setAccessToken($request->session()->get('token'));
        }
        if ($this->gClient->getAccessToken())
        {
            $user->access_token=dd(json_encode($request->session()->get('token')));
        } else
        {
            //For Guest user, get google login url
            $authUrl = $this->gClient->createAuthUrl();
            return redirect()->to($authUrl);
        }
    }

    public function uploadFileUsingAccessToken(){
        $service = new Google_Service_Drive($this->gClient);
        $user=User::find(1);
        $this->gClient->setAccessToken(json_decode($user->access_token,true));
        if ($this->gClient->isAccessTokenExpired()) {

            // save refresh token to some variable
            $refreshTokenSaved = $this->gClient->getRefreshToken();
            // update access token
            $this->gClient->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
            // // pass access token to some variable
            $updatedAccessToken = $this->gClient->getAccessToken();
            // // append refresh token
            $updatedAccessToken['refresh_token'] = $refreshTokenSaved;
            //Set the new acces token
            $this->gClient->setAccessToken($updatedAccessToken);

            $user->access_token=$updatedAccessToken;
            $user->save();
        }

        $fileMetadata = new \Google_Service_Drive_DriveFile(array(
            'name' => 'ExpertPHP',
            'mimeType' => 'application/vnd.google-apps.folder'));
        $folder = $service->files->create($fileMetadata, array(
            'fields' => 'id'));
        printf("Folder ID: %s\n", $folder->id);


        $file = new \Google_Service_Drive_DriveFile(array(
            'name' => 'cdrfile.jpg',
            'parents' => array($folder->id)
        ));
        $result = $service->files->create($file, array(
            'data' => file_get_contents(public_path('images/myimage.jpg')),
            'mimeType' => 'application/octet-stream',
            'uploadType' => 'media'
        ));
        // get url of uploaded file
        $url='https://drive.google.com/open?id='.$result->id;
        dd($result);

    }
}
