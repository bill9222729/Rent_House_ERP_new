<?php

namespace App\Http\Controllers;

use App\DataTables\HousePayHistoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHousePayHistoryRequest;
use App\Http\Requests\UpdateHousePayHistoryRequest;
use App\Repositories\HousePayHistoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class HousePayHistoryController extends AppBaseController
{
    /** @var  HousePayHistoryRepository */
    private $housePayHistoryRepository;

    public function __construct(HousePayHistoryRepository $housePayHistoryRepo)
    {
        $this->housePayHistoryRepository = $housePayHistoryRepo;
    }

    /**
     * Display a listing of the HousePayHistory.
     *
     * @param HousePayHistoryDataTable $housePayHistoryDataTable
     * @return Response
     */
    public function index(HousePayHistoryDataTable $housePayHistoryDataTable)
    {
        return $housePayHistoryDataTable->render('house_pay_histories.index');
    }

    /**
     * Show the form for creating a new HousePayHistory.
     *
     * @return Response
     */
    public function create()
    {
        return view('house_pay_histories.create');
    }

    /**
     * Store a newly created HousePayHistory in storage.
     *
     * @param CreateHousePayHistoryRequest $request
     *
     * @return Response
     */
    public function store(CreateHousePayHistoryRequest $request)
    {
        $input = $request->all();

        $housePayHistory = $this->housePayHistoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/housePayHistories.singular')]));

        return redirect(route('housePayHistories.index'));
    }

    /**
     * Display the specified HousePayHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $housePayHistory = $this->housePayHistoryRepository->find($id);

        if (empty($housePayHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/housePayHistories.singular')]));

            return redirect(route('housePayHistories.index'));
        }

        return view('house_pay_histories.show')->with('housePayHistory', $housePayHistory);
    }

    /**
     * Show the form for editing the specified HousePayHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $housePayHistory = $this->housePayHistoryRepository->find($id);

        if (empty($housePayHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/housePayHistories.singular')]));

            return redirect(route('housePayHistories.index'));
        }

        return view('house_pay_histories.edit')->with('housePayHistory', $housePayHistory);
    }

    /**
     * Update the specified HousePayHistory in storage.
     *
     * @param  int              $id
     * @param UpdateHousePayHistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHousePayHistoryRequest $request)
    {
        $housePayHistory = $this->housePayHistoryRepository->find($id);

        if (empty($housePayHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/housePayHistories.singular')]));

            return redirect(route('housePayHistories.index'));
        }

        $housePayHistory = $this->housePayHistoryRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/housePayHistories.singular')]));

        return redirect(route('housePayHistories.index'));
    }

    /**
     * Remove the specified HousePayHistory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $housePayHistory = $this->housePayHistoryRepository->find($id);

        if (empty($housePayHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/housePayHistories.singular')]));

            return redirect(route('housePayHistories.index'));
        }

        $this->housePayHistoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/housePayHistories.singular')]));

        return redirect(route('housePayHistories.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('HousePayHistory'), $file);

    }

}
