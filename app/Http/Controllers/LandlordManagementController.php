<?php

namespace App\Http\Controllers;

use App\DataTables\LandlordManagementDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLandlordManagementRequest;
use App\Http\Requests\UpdateLandlordManagementRequest;
use App\Repositories\LandlordManagementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\LandlordManagement;

class LandlordManagementImport implements WithCalculatedFormulas
{
    public function model(array $row)
    {
        return $row;
    }
}

class LandlordManagementController extends AppBaseController
{
    /** @var  LandlordManagementRepository */
    private $landlordManagementRepository;

    public function __construct(LandlordManagementRepository $landlordManagementRepo)
    {
        $this->landlordManagementRepository = $landlordManagementRepo;
    }

    /**
     * Display a listing of the LandlordManagement.
     *
     * @param LandlordManagementDataTable $landlordManagementDataTable
     * @return Response
     */
    public function index(LandlordManagementDataTable $landlordManagementDataTable)
    {
        return $landlordManagementDataTable->render('landlord_managements.index');
    }

    /**
     * Show the form for creating a new LandlordManagement.
     *
     * @return Response
     */
    public function create()
    {
        return view('landlord_managements.create');
    }

    /**
     * Store a newly created LandlordManagement in storage.
     *
     * @param CreateLandlordManagementRequest $request
     *
     * @return Response
     */
    public function store(CreateLandlordManagementRequest $request)
    {
        $input = $request->all();
        $input['birthday'] = self::Format_TW_Date($input['birthday']);

        $landlordManagement = $this->landlordManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/landlordManagements.singular')]));

        return redirect(route('landlordManagements.index'));
    }

    /**
     * Display the specified LandlordManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $landlordManagement = $this->landlordManagementRepository->find($id);

        if (empty($landlordManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/landlordManagements.singular')]));

            return redirect(route('landlordManagements.index'));
        }

        return view('landlord_managements.show')->with('landlordManagement', $landlordManagement);
    }

    /**
     * Show the form for editing the specified LandlordManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $landlordManagement = $this->landlordManagementRepository->find($id);

        if (empty($landlordManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/landlordManagements.singular')]));

            return redirect(route('landlordManagements.index'));
        }

        return view('landlord_managements.edit')->with('landlordManagement', $landlordManagement);
    }

    /**
     * Update the specified LandlordManagement in storage.
     *
     * @param  int              $id
     * @param UpdateLandlordManagementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLandlordManagementRequest $request)
    {
        $landlordManagement = $this->landlordManagementRepository->find($id);

        if (empty($landlordManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/landlordManagements.singular')]));

            return redirect(route('landlordManagements.index'));
        }

        $input = $request->all();
        $input['birthday'] = self::Format_TW_Date($input['birthday']);
        $landlordManagement = $this->landlordManagementRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/landlordManagements.singular')]));

        return redirect(route('landlordManagements.index'));
    }

    /**
     * Remove the specified LandlordManagement from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $landlordManagement = $this->landlordManagementRepository->find($id);

        if (empty($landlordManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/landlordManagements.singular')]));

            return redirect(route('landlordManagements.index'));
        }

        $this->landlordManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/landlordManagements.singular')]));

        return redirect(route('landlordManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('LandlordManagement'), $file);

    }

    public function UploadExcel(Request $request)
    {
        $file = $request->file('upload_file');

        if (isset($file)) {
            // $file_read = '';
            self::processExcel($file);
            return redirect(route('landlordManagements.index'));
        }else{
            return redirect(route('landlordManagements.index'));
        }
    }

    public static function processExcel($file) {
        $match_value_column_num = 0;
        $option_column_num = 0;
        $id_column_num = 0;
        $reader = Excel::toCollection(new LandlordManagementImport, $file);

        $excel_data = $reader[0];
        if ($excel_data[0][1] !== '物件編號') return false;
        unset($excel_data[0]);
        foreach ($excel_data as $key => $value) {
            $LandlordManagement = new LandlordManagement;
            $LandlordManagement->name = $value[6];
            $LandlordManagement->id_num = $value[9];
            $LandlordManagement->address = $value[11] . $value[12] . $value[13] . ($value[14] ? $value[14] . '巷' : '') . ($value[15] ? $value[15] . '弄' : '') . ($value[16] ? $value[16] . '號' : '');
            $LandlordManagement->phone = $value[10];
            $LandlordManagement->birthday = $value[8];
            $LandlordManagement->bank_acc = '(' . strval($value[27]) . ') ' . $value[29];
            $LandlordManagement->save();
        }
        return true;
    }

    private static function Format_TW_Date($date_string){
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list,"/")) {
            $date_list = explode("/",$date_list);
        }else if(strpos($date_list,".")){
            $date_list = explode(".",$date_list);
        }else if(strpos($date_list,"-")){
            $date_list = explode("-",$date_list);
        }else{
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        return $date;
    }

}
