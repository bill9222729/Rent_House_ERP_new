<?php

namespace App\Http\Controllers;

use App\DataTables\LogHistoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLogHistoryRequest;
use App\Http\Requests\UpdateLogHistoryRequest;
use App\Repositories\LogHistoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class LogHistoryController extends AppBaseController
{
    /** @var  LogHistoryRepository */
    private $logHistoryRepository;

    public function __construct(LogHistoryRepository $logHistoryRepo)
    {
        $this->logHistoryRepository = $logHistoryRepo;
    }

    /**
     * Display a listing of the LogHistory.
     *
     * @param LogHistoryDataTable $logHistoryDataTable
     * @return Response
     */
    public function index(LogHistoryDataTable $logHistoryDataTable)
    {
        return $logHistoryDataTable->render('log_histories.index');
    }

    /**
     * Show the form for creating a new LogHistory.
     *
     * @return Response
     */
    public function create()
    {
        return view('log_histories.create');
    }

    /**
     * Store a newly created LogHistory in storage.
     *
     * @param CreateLogHistoryRequest $request
     *
     * @return Response
     */
    public function store(CreateLogHistoryRequest $request)
    {
        $input = $request->all();

        $logHistory = $this->logHistoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/logHistories.singular')]));

        return redirect(route('logHistories.index'));
    }

    /**
     * Display the specified LogHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $logHistory = $this->logHistoryRepository->find($id);

        if (empty($logHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logHistories.singular')]));

            return redirect(route('logHistories.index'));
        }

        return view('log_histories.show')->with('logHistory', $logHistory);
    }

    /**
     * Show the form for editing the specified LogHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $logHistory = $this->logHistoryRepository->find($id);

        if (empty($logHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logHistories.singular')]));

            return redirect(route('logHistories.index'));
        }

        return view('log_histories.edit')->with('logHistory', $logHistory);
    }

    /**
     * Update the specified LogHistory in storage.
     *
     * @param  int              $id
     * @param UpdateLogHistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLogHistoryRequest $request)
    {
        $logHistory = $this->logHistoryRepository->find($id);

        if (empty($logHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logHistories.singular')]));

            return redirect(route('logHistories.index'));
        }

        $logHistory = $this->logHistoryRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/logHistories.singular')]));

        return redirect(route('logHistories.index'));
    }

    /**
     * Remove the specified LogHistory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $logHistory = $this->logHistoryRepository->find($id);

        if (empty($logHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logHistories.singular')]));

            return redirect(route('logHistories.index'));
        }

        $this->logHistoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/logHistories.singular')]));

        return redirect(route('logHistories.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('LogHistory'), $file);

    }

}
