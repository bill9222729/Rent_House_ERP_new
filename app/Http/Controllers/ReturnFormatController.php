<?php

namespace App\Http\Controllers;

use App\DataTables\ReturnFormatDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateReturnFormatRequest;
use App\Http\Requests\UpdateReturnFormatRequest;
use App\Models\BuildingManagement;
use App\Models\contract;
use App\Models\ReturnFormat;
use App\Models\TransactionDetails;
use App\Repositories\ReturnFormatRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class ReturnFormatExport implements FromArray
{
    protected $ary;
    function __construct($array) {
        $this->ary = $array;
    }
    public function array(): array
    {
        return $this->ary;
    }
}

class ReturnFormatController extends AppBaseController
{
    /** @var  ReturnFormatRepository */
    private $returnFormatRepository;

    public function __construct(ReturnFormatRepository $returnFormatRepo)
    {
        $this->returnFormatRepository = $returnFormatRepo;
    }

    /**
     * Display a listing of the ReturnFormat.
     *
     * @param ReturnFormatDataTable $returnFormatDataTable
     * @return Response
     */
    public function index(ReturnFormatDataTable $returnFormatDataTable)
    {
        return $returnFormatDataTable->render('return_formats.index');
    }

    /**
     * Show the form for creating a new ReturnFormat.
     *
     * @return Response
     */
    public function create()
    {
        return view('return_formats.create');
    }

    /**
     * Store a newly created ReturnFormat in storage.
     *
     * @param CreateReturnFormatRequest $request
     *
     * @return Response
     */
    public function store(CreateReturnFormatRequest $request)
    {
        $input = $request->all();

        $returnFormat = $this->returnFormatRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/returnFormats.singular')]));

        return redirect(route('returnFormats.index'));
    }

    /**
     * Display the specified ReturnFormat.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $returnFormat = $this->returnFormatRepository->find($id);

        if (empty($returnFormat)) {
            Flash::error(__('messages.not_found', ['model' => __('models/returnFormats.singular')]));

            return redirect(route('returnFormats.index'));
        }

        return view('return_formats.show')->with('returnFormat', $returnFormat);
    }

    /**
     * Show the form for editing the specified ReturnFormat.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $returnFormat = $this->returnFormatRepository->find($id);

        if (empty($returnFormat)) {
            Flash::error(__('messages.not_found', ['model' => __('models/returnFormats.singular')]));

            return redirect(route('returnFormats.index'));
        }

        return view('return_formats.edit')->with('returnFormat', $returnFormat);
    }

    /**
     * Update the specified ReturnFormat in storage.
     *
     * @param  int              $id
     * @param UpdateReturnFormatRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReturnFormatRequest $request)
    {
        $returnFormat = $this->returnFormatRepository->find($id);

        if (empty($returnFormat)) {
            Flash::error(__('messages.not_found', ['model' => __('models/returnFormats.singular')]));

            return redirect(route('returnFormats.index'));
        }

        $returnFormat = $this->returnFormatRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/returnFormats.singular')]));

        return redirect(route('returnFormats.index'));
    }

    /**
     * Remove the specified ReturnFormat from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $returnFormat = $this->returnFormatRepository->find($id);

        if (empty($returnFormat)) {
            Flash::error(__('messages.not_found', ['model' => __('models/returnFormats.singular')]));

            return redirect(route('returnFormats.index'));
        }

        $this->returnFormatRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/returnFormats.singular')]));

        return redirect(route('returnFormats.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('ReturnFormat'), $file);

    }

    public static function processExcel($file)
    {
        $results = []; // a arrany of { line: number, case_num: string, result: "inserted" | "skipped" }
        // dd($request->file("user_csv"));
        // $path = $file->getRealPath();
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        // $spreadsheet = $reader->load($path);
        // $worksheet = $spreadsheet->getActiveSheet();
        // $rows = $worksheet->toArray();

        $reader = Excel::toCollection(new ContractImport, $file);
        $rows = $reader[0];

        foreach ($rows as $key => $row) {
            if ($key == 0) {
                continue;
            }
            if (!$row[0]) {
                continue;
            }
            try {
                $contract = contract::where('landlord_item_number', $row[4])->first();
                $return_format = new ReturnFormat();
                $return_format->t_id = $row[0];
                $return_format->t_code = $contract ? $contract->landlord_match_number : $row[1];
                $return_format->tax = $row[2];
                $return_format->bank_account = $row[3];
                $return_format->user_name = $row[4] ? : null;
                $return_format->t_price = self::IntFormat($row[5]);
                $return_format->t_status = $row[6];
                $return_format->name = $row[7] ? : null;
                $return_format->item_address = $row[8] ? : '';
                $return_format->item_month_price = $row[9] ? : 0;
                $return_format->item_price_support = $row[10] ? : 0;
                $return_format->watch_price = $row[11] ? : 0;
                $return_format->parking_price = $row[12] ? : 0;
                $return_format->fix_price = $row[13] ? : 0;
                $return_format->t_cost = $row[14] ? : 0;
                $return_format->other_price = $row[15] ? : 0;
                $return_format->total_price = $row[16] ? : 0;
                $return_format->note = $row[17] ? : null;

                if ($row[4]) {
                    $building_management = BuildingManagement::where('num', $row[4])->first();
                    if (!empty($building_management)) {
                        $return_format->name = $building_management->Lessor_name;
                        $return_format->item_address = $building_management->located_build_address;
                        $return_format->watch_price = $building_management->manage_fee_month;

                        $contract = contract::where('landlord_item_number', $building_management->num)->first();
                        if (!empty($contract)) {
                            $return_format->item_month_price = $contract->actual_contract_tenant_pays_rent;
                            $return_format->item_price_support = $contract->actual_contract_rent_subsidy;
                        }
                    }
                }
                $return_format->save();
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $row[1],
                    'result' => '新增成功',
                ];
            }catch (\Exception $e) {
                continue;
            }

        }
        return $results;
    }

    private static function IntFormat($strind){
        if (!$strind) {
            return null;
        }

        if (strpos($strind, ",")) {
            $strind = str_replace(',', '', $strind);
        }

        return $strind;
    }

    public function generateData() {
        // $returnFormats = ReturnFormat::with('building_management')->get();
        $returnFormats = ReturnFormat::all();

        $data = [
            ['交易序號', '媒合編號', '收受者統一編號', '收受者帳號', '物件編號', '交易金額', '交易結果', '姓名', '物件地址', '自付額', '租金補助', '管理費', '停車費', '修繕費', '扣手續費', '其他', '小計', '備註']
        ];

        foreach ($returnFormats as $returnFormat) {
            // $address = $returnFormat->item_address;
            // $name = $returnFormat->name;
            // $item_month_price = $returnFormat->item_month_price;
            // if (is_null($name)) {
            //     if ($returnFormat->building_management) {
            //         $building_management = $returnFormat->building_management;
            //         $address = "$building_management->address_city$building_management->address_cityarea$building_management->address_street$building_management->address_ln$building_management->address_aly$building_management->address_num$building_management->address_num_hyphen";
            //         $name = $building_management->Lessor_name;
            //         $item_month_price = $building_management->landlord_expect_rent;
            //     }
            // }
            $data[] = [
                strval($returnFormat->t_id),
                strval($returnFormat->t_code),
                strval($returnFormat->tax),
                strval($returnFormat->bank_account),
                strval($returnFormat->user_name),
                strval($returnFormat->t_price),
                strval($returnFormat->t_status),

                // strval($name),
                strval($returnFormat->name),
                // strval($address),
                strval($returnFormat->item_address),
                // strval($item_month_price),
                strval($returnFormat->item_month_price),

                strval($returnFormat->item_price_support),
                strval($returnFormat->watch_price),
                strval($returnFormat->parking_price),
                strval($returnFormat->fix_price),
                strval($returnFormat->t_cost),
                strval($returnFormat->other_price),
                strval($returnFormat->total_price),
                strval($returnFormat->note),
            ];
        }
        $export = new ReturnFormatExport($data);
        return Excel::download($export, 'output.xlsx');
    }
}
