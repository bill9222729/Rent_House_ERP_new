<?php

namespace App\Http\Controllers;

use App\DataTables\SalesmanManagementDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSalesmanManagementRequest;
use App\Http\Requests\UpdateSalesmanManagementRequest;
use App\Repositories\SalesmanManagementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SalesmanManagementController extends AppBaseController
{
    /** @var  SalesmanManagementRepository */
    private $salesmanManagementRepository;

    public function __construct(SalesmanManagementRepository $salesmanManagementRepo)
    {
        $this->salesmanManagementRepository = $salesmanManagementRepo;
    }

    /**
     * Display a listing of the SalesmanManagement.
     *
     * @param SalesmanManagementDataTable $salesmanManagementDataTable
     * @return Response
     */
    public function index(SalesmanManagementDataTable $salesmanManagementDataTable)
    {
        return $salesmanManagementDataTable->render('salesman_managements.index');
    }

    /**
     * Show the form for creating a new SalesmanManagement.
     *
     * @return Response
     */
    public function create()
    {
        return view('salesman_managements.create');
    }

    /**
     * Store a newly created SalesmanManagement in storage.
     *
     * @param CreateSalesmanManagementRequest $request
     *
     * @return Response
     */
    public function store(CreateSalesmanManagementRequest $request)
    {
        $input = $request->all();
        $input['birthday'] = self::Format_TW_Date($input['birthday']);

        $salesmanManagement = $this->salesmanManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/salesmanManagements.singular')]));

        return redirect(route('salesmanManagements.index'));
    }

    /**
     * Display the specified SalesmanManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        return view('salesman_managements.show')->with('salesmanManagement', $salesmanManagement);
    }

    /**
     * Show the form for editing the specified SalesmanManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        return view('salesman_managements.edit')->with('salesmanManagement', $salesmanManagement);
    }

    /**
     * Update the specified SalesmanManagement in storage.
     *
     * @param  int              $id
     * @param UpdateSalesmanManagementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSalesmanManagementRequest $request)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        $input = $request->all();
        $input['birthday'] = self::Format_TW_Date($input['birthday']);
        $salesmanManagement = $this->salesmanManagementRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/salesmanManagements.singular')]));

        return redirect(route('salesmanManagements.index'));
    }

    /**
     * Remove the specified SalesmanManagement from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        $this->salesmanManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/salesmanManagements.singular')]));

        return redirect(route('salesmanManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('SalesmanManagement'), $file);

    }

    private static function Format_TW_Date($date_string){
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list,"/")) {
            $date_list = explode("/",$date_list);
        }else if(strpos($date_list,".")){
            $date_list = explode(".",$date_list);
        }else if(strpos($date_list,"-")){
            $date_list = explode("-",$date_list);
        }else{
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        return $date;
    }

}
