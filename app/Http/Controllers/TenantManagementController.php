<?php

namespace App\Http\Controllers;

use App\DataTables\TenantManagementDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTenantManagementRequest;
use App\Http\Requests\UpdateTenantManagementRequest;
use App\Repositories\TenantManagementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\TenantManagement;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;


class TenantManagementImport implements WithCalculatedFormulas
{
    public function model(array $row)
    {
        return $row;
    }
}

class tExport implements FromArray, WithEvents
{
    protected $ary;
    function __construct($array)
    {
        $this->ary = $array;
    }
    public function array(): array
    {
        return $this->ary;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function ($event) {
                //設定區域單元格垂直居中
                $event->sheet->getDelegate()->getStyle('A1:Z1265')->getAlignment()->setVertical('center');
                //設定區域單元格字型、顏色、背景等，其他設定請檢視 applyFromArray 方法，提供了註釋
                $event->sheet->getDelegate()->getStyle('A1:Z1')->applyFromArray([
                    'font' => [
                        'name' => 'Arial',
                        'bold' => true,
                        'italic' => false,
                        'strikethrough' => false,
                        'color' => [
                            'rgb' => 'FFFFFF'
                        ]
                    ],
                    'fill' => [
                        'fillType' => 'linear', //線性填充，類似漸變
                        'rotation' => 45, //漸變角度
                        'startColor' => [
                            'rgb' => '54AE54' //初始顏色
                        ],
                        //結束顏色，如果需要單一背景色，請和初始顏色保持一致
                        'endColor' => [
                            'argb' => '54AE54'
                        ]
                    ]
                ]);
            }
        ];
    }
}

class TenantManagementController extends AppBaseController
{
    /** @var  TenantManagementRepository */
    private $tenantManagementRepository;

    public function __construct(TenantManagementRepository $tenantManagementRepo)
    {
        $this->tenantManagementRepository = $tenantManagementRepo;
    }

    /**
     * Display a listing of the TenantManagement.
     *
     * @param TenantManagementDataTable $tenantManagementDataTable
     * @return Response
     */
    public function index(TenantManagementDataTable $tenantManagementDataTable)
    {
        return $tenantManagementDataTable->render('tenant_managements.index');
    }

    /**
     * Show the form for creating a new TenantManagement.
     *
     * @return Response
     */
    public function create()
    {
        return view('tenant_managements.create');
    }

    /**
     * Store a newly created TenantManagement in storage.
     *
     * @param CreateTenantManagementRequest $request
     *
     * @return Response
     */
    public function store(CreateTenantManagementRequest $request)
    {
        $input = $request->all();
        if (isset($input['same_address'])) {
            if ($input['same_address'] == 'on') {
                $input['contact_city'] = $input['hr_city'];
                $input['contact_cityarea'] = $input['hr_cityarea'];
                $input['contact_address'] = $input['hr_address'];
            }
        }
        $input['apply_date'] = self::Format_TW_Date($input['apply_date']);
        $input['lessee_birthday'] = self::Format_TW_Date($input['lessee_birthday']);
        $input['files'] = $this->createFilesJson($request);
        $tenantManagement = $this->tenantManagementRepository->create($input);

        if ($request->hasFile("user_file")) {
            $tenantManagement->user_file = $request->file("user_file")->store("tenant" . $tenantManagement->id, 'public');
            $tenantManagement->save();
        }

        Flash::success(__('messages.saved', ['model' => __('models/tenantManagements.singular')]));

        return redirect(route('tenantManagements.index'));
    }

    /**
     * Display the specified TenantManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tenantManagement = $this->tenantManagementRepository->find($id);

        if (empty($tenantManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagements.singular')]));

            return redirect(route('tenantManagements.index'));
        }

        return view('tenant_managements.edit')->with(['tenantManagement' => $tenantManagement, 'show' => true]);
    }

    /**
     * Show the form for editing the specified TenantManagement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tenantManagement = $this->tenantManagementRepository->find($id);

        if (empty($tenantManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagements.singular')]));

            return redirect(route('tenantManagements.index'));
        }

        return view('tenant_managements.edit')->with('tenantManagement', $tenantManagement);
    }

    /**
     * Update the specified TenantManagement in storage.
     *
     * @param  int              $id
     * @param UpdateTenantManagementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTenantManagementRequest $request)
    {
        $tenantManagement = $this->tenantManagementRepository->find($id);

        if (empty($tenantManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagements.singular')]));

            return redirect(route('tenantManagements.index'));
        }
        $input = $request->all();

        // 處理檔案json
        if ($tenantManagement['file_json'] != null && $input['file_json'] != null) {
            $database_file_json = json_decode($tenantManagement['file_json']);
            $new__file_json = json_decode($input['file_json']);
            foreach ($new__file_json as $key => $arr) {
                foreach ($arr as $index => $value) {
                    array_push($database_file_json->$key, $value);
                }
            }
            $input['file_json'] = json_encode($database_file_json);
        }



        $input['apply_date'] = self::Format_TW_Date($input['apply_date']);
        $input['lessee_birthday'] = self::Format_TW_Date($input['lessee_birthday']);
        $input['files'] = $this->createFilesJson($request, $tenantManagement);
        if ($request->hasFile("user_file")) {
            //            dd($request->file("user_file")->store("tenant" . $tenantManagement->id . "/user_file" . $request->file("user_file")->getClientOriginalExtension(), 'public'));
            $input["user_file"] = $request->file("user_file")->store("tenant" . $tenantManagement->id, 'public');
        }
        $tenantManagement = $this->tenantManagementRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/tenantManagements.singular')]));

        return redirect(route('tenantManagements.index'));
    }

    /**
     * Remove the specified TenantManagement from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tenantManagement = $this->tenantManagementRepository->find($id);

        if (empty($tenantManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagements.singular')]));

            return redirect(route('tenantManagements.index'));
        }

        $this->tenantManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tenantManagements.singular')]));

        return redirect(route('tenantManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('TenantManagement'), $file);
    }
    public function UploadExcel(Request $request)
    {
        $file = $request->file('upload_file');

        if (isset($file)) {
            // $file_read = '';
            self::processExcel($file);
            return redirect(route('tenantManagements.index'));
        } else {
            return redirect(route('tenantManagements.index'));
        }
    }
    private static function Format_TW_Date($date_string)
    {
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list, "/")) {
            $date_list = explode("/", $date_list);
        } else if (strpos($date_list, ".")) {
            $date_list = explode(".", $date_list);
        } else if (strpos($date_list, "-")) {
            $date_list = explode("-", $date_list);
        } else {
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        return $date;
    }

    public static function processExcel($file)
    {
        $results = []; // a arrany of { line: number, case_num: string, result: "inserted" | "skipped" }
        $match_value_column_num = 0;
        $option_column_num = 0;
        $id_column_num = 0;
        $reader = Excel::toCollection(new TenantManagementImport, $file);

        $excel_data = $reader[2];
        unset($excel_data[0]);
        foreach ($excel_data as $key => $value) {
            $find_by_case_num = TenantManagement::where('tenant_ministry_of_the_interior_number', $value[6])->first();
            if ($find_by_case_num) {
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $value[4],
                    'result' => '重複輸入',
                ];
                continue;
            }
            try {
                $tenantManagment = new TenantManagement;
                $tenantManagment->pay_date = $value[1];
                $tenantManagment->sales = $value[7];
                $tenantManagment->tenant_ministry_of_the_interior_number = $value[8];
                $tenantManagment->tenant_case_number = $value[9];
                $tenantManagment->apply_date = $value[10];
                $tenantManagment->receive_subsidy = $value[14] === '通過' ? 1 : 0;
                $tenantManagment->status = $value[15];
                $tenantManagment->weak_item = $value[16];
                $tenantManagment->lessee_name = $value[17];
                $tenantManagment->lessee_id_num = $value[18];
                $tenantManagment->lessee_cellphone = $value[19];
                $tenantManagment->lessee_hc_num = $value[20];
                $tenantManagment->res = $value[22];
                $tenantManagment->tp = $value[23];
                $tenantManagment->remark_1 = $value[26];

                $tenantManagment->save();
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $value[6],
                    'result' => '新增成功',
                ];
            } catch (\Exception $exception) {
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $value[4],
                    'result' => $exception->getMessage(),
                ];
                continue;
            }
        }
        return $results;
    }

    private static function fixQualification($str)
    {
        if ($str == "一般戶") return "1";
        if ($str == "第一類弱勢戶") return "2";
        if ($str == "第二類弱勢戶") return "3";
        return $str;
    }

    private static function fixYesNo($str)
    {
        if ($str == "是") return "1";
        if ($str == "否") return "0";
        return $str;
    }

    private static function fixAgentNum($str)
    {
        if ($str == "一位") return "1";
        if ($str == "兩位") return "2";
        return $str;
    }

    private function createFilesJson(Request $request, $tenant = null)
    {
        $files = [
            'lessor_application' => [],
            'address_info' => [],
            'identity' => [],
            'weak_identity' => [],
            't_tax' => [],
            'p_tax' => [],
            'give_up' => [],
            'bank_account' => [],
            'other' => [],
        ];

        if ($tenant) {
            $files = $tenant->files ? json_decode($tenant->files, true) : $files;
        }

        foreach ($files as $key => $value) {
            if ($request->hasFile($key)) {
                $files[$key][] = $request->file($key)->store('tenant', 'public');
            }
        }

        return json_encode($files);
    }

    public function generateT(Request $request, $tId)
    {
        $tenant = TenantManagement::where('id', $tId)->first();
        $arr = array();
        $data = [
            [
                '申請日期' . "\r\n" . '(yyy/mm/dd)',
                '本人欲承租' . "\r\n" . '租屋服務事業轉租之住宅(包租)' . "\r\n" . '0：否   1：是',
                '本人欲承租' . "\r\n" . '經由租屋服務事業協助出租(代管)' . "\r\n" . '0：否   1：是',
                '承租人姓名',
                '性別' . "\r\n" . '0：女  1：男',
                '出生年月日' . "\r\n" . '(yyy/mm/dd)',
                '國民身分證統一編號',
                '戶口名簿戶號',
                '電話(日)',
                '電話(夜)',
                '手機',
                '電子信箱',
                '承租人及其家庭成員資格' . "\r\n" . '1：一般戶' . "\r\n" . '2：第一類弱勢戶' . "\r\n" . '3：第二類弱勢戶',
                '戶籍地址縣市',
                '戶籍地址' . "\r\n" . '鄉鎮區',
                '戶籍地址' . "\r\n" . '地址',
                '通訊地址' . "\r\n" . '縣市',
                '通訊地址' . "\r\n" . '鄉鎮區',
                '通訊地址' . "\r\n" . '地址',
                '承租人及家庭成員其是否領有政府最近年度核發之租金補貼核定函或承租右列住宅(V~Z列)' . "\r\n" . '0：否  1：是',
                'OOO年度租金補貼',
                'OOO年度低收入戶及中低收入戶租金補貼',
                'OOO年度身心障礙者房屋租金補貼',
                'OOO年度依其他法令相關租金補貼規定之租金補貼',
                'OO縣市承租政府興建之國民住宅或社會住宅',
                '法定代理人人數' . "\r\n" . '(未滿二十歲，且為單獨監護者之申請人，法定代理人數請選擇一位)' . "\r\n" . '1：一位' . "\r\n" . '2：兩位',
                '第一代理人' . "\r\n" . '姓名',
                '第一代理人聯絡方式' . "\r\n" . '電話',
                '第一代理人聯絡方式' . "\r\n" . '手機',
                '第一代理人聯絡方式' . "\r\n" . '地址',
                '第二代理人' . "\r\n" . '姓名',
                '第二代理人聯絡方式' . "\r\n" . '電話',
                '第二代理人聯絡方式' . "\r\n" . '手機',
                '第二代理人聯絡方式' . "\r\n" . '地址'
            ]
        ];
        $data[] = [
            strval($this->tranToTwYear($tenant->apply_date)),
            strval($tenant->sublet),
            strval($tenant->rent),
            strval($tenant->lessee_name),
            strval($tenant->lessee_gender),
            strval($this->tranToTwYear($tenant->lessee_birthday)),
            strval($tenant->lessee_id_num),
            strval($tenant->lessee_hc_num),
            strval($tenant->lessee_telephone_d),
            strval($tenant->lessee_telephone_n),
            strval($tenant->lessee_cellphone),
            strval($tenant->lessee_email),
            strval($tenant->qualifications),
            strval($tenant->hr_city),
            strval($tenant->hr_cityarea),
            strval($tenant->hr_address),
            strval($tenant->contact_city),
            strval($tenant->contact_cityarea),
            strval($tenant->contact_address),
            strval($tenant->receive_subsidy),
            strval($tenant->subsidy_rent),
            strval($tenant->subsidy_low_income),
            strval($tenant->subsidy_disability),
            strval($tenant->subsidy_decree),
            strval($tenant->lease_nr_sr_city),
            strval($tenant->legal_agent_num),
            strval($tenant->first_agent_name),
            strval($tenant->first_agent_telephone),
            strval($tenant->first_agent_cellphone),
            strval($tenant->first_agent_address),
            strval($tenant->second_agent_name),
            strval($tenant->second_agent_telephone),
            strval($tenant->second_agent_cellphone),
            strval($tenant->second_agent_address)
        ];

        // dd($data);
        $export = new tExport($data);
        return Excel::download($export, 'output.xls');
    }

    private function tranToTwYear($date)
    {
        if (!$date) {
            return null;
        }
        $dateArr = explode(" ", $date);
        $dateArr = explode("-", $dateArr[0]);
        $date_Y = (int)$dateArr[0];
        $date_Y = $date_Y - 1911;
        $date_Y = strval($date_Y);
        $TWdate = $date_Y . "/" . $dateArr[1] . "/" . $dateArr[2];
        return $TWdate;
    }
}
