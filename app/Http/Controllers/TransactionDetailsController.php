<?php

namespace App\Http\Controllers;

use App\DataTables\TransactionDetailsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTransactionDetailsRequest;
use App\Http\Requests\UpdateTransactionDetailsRequest;
use App\Models\TransactionDetails;
use App\Repositories\TransactionDetailsRepository;
use Carbon\Carbon;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class TransactionDetailsController extends AppBaseController
{
    /** @var  TransactionDetailsRepository */
    private $transactionDetailsRepository;

    public function __construct(TransactionDetailsRepository $transactionDetailsRepo)
    {
        $this->transactionDetailsRepository = $transactionDetailsRepo;
    }

    /**
     * Display a listing of the TransactionDetails.
     *
     * @param TransactionDetailsDataTable $transactionDetailsDataTable
     * @return Response
     */
    public function index(TransactionDetailsDataTable $transactionDetailsDataTable)
    {
        return $transactionDetailsDataTable->render('transaction_details.index');
    }

    /**
     * Show the form for creating a new TransactionDetails.
     *
     * @return Response
     */
    public function create()
    {
        return view('transaction_details.create');
    }

    /**
     * Store a newly created TransactionDetails in storage.
     *
     * @param CreateTransactionDetailsRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionDetailsRequest $request)
    {
        $input = $request->all();
        $input['t_date'] = self::Format_TW_Date($input['t_date']);
        $input['t_time'] = self::Format_TW_Date($input['t_time']);
        $input['a_date'] = self::Format_TW_Date($input['a_date']);

        $transactionDetails = $this->transactionDetailsRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/transactionDetails.singular')]));

        return redirect(route('transactionDetails.index'));
    }

    /**
     * Display the specified TransactionDetails.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transactionDetails = $this->transactionDetailsRepository->find($id);

        if (empty($transactionDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/transactionDetails.singular')]));

            return redirect(route('transactionDetails.index'));
        }

        return view('transaction_details.show')->with('transactionDetails', $transactionDetails);
    }

    /**
     * Show the form for editing the specified TransactionDetails.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transactionDetails = $this->transactionDetailsRepository->find($id);

        if (empty($transactionDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/transactionDetails.singular')]));

            return redirect(route('transactionDetails.index'));
        }

        return view('transaction_details.edit')->with('transactionDetails', $transactionDetails);
    }

    /**
     * Update the specified TransactionDetails in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionDetailsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionDetailsRequest $request)
    {
        $transactionDetails = $this->transactionDetailsRepository->find($id);

        if (empty($transactionDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/transactionDetails.singular')]));

            return redirect(route('transactionDetails.index'));
        }

        $input = $request->all();
        $input['t_date'] = self::Format_TW_Date($input['t_date']);
        $input['t_time'] = self::Format_TW_Date($input['t_time']);
        $input['a_date'] = self::Format_TW_Date($input['a_date']);
        $transactionDetails = $this->transactionDetailsRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/transactionDetails.singular')]));

        return redirect(route('transactionDetails.index'));
    }

    /**
     * Remove the specified TransactionDetails from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transactionDetails = $this->transactionDetailsRepository->find($id);

        if (empty($transactionDetails)) {
            Flash::error(__('messages.not_found', ['model' => __('models/transactionDetails.singular')]));

            return redirect(route('transactionDetails.index'));
        }

        $this->transactionDetailsRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/transactionDetails.singular')]));

        return redirect(route('transactionDetails.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('TransactionDetails'), $file);

    }

    public static function processExcel($file)
    {
        $results = []; // a arrany of { line: number, case_num: string, result: "inserted" | "skipped" }
        // dd($request->file("user_csv"));
        $path = $file->getRealPath();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($path);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();
        foreach ($rows as $key => $row) {
            if ($key == 0) {
                continue;
            }
            if (!$row[0]) {
                continue;
            }
            try {
                $transaction = new TransactionDetails();
                $transaction->account = str_replace("'", "", $row[0]);
                $transaction->t_date = date("Y-m-d", strtotime(str_replace("'", "", $row[1])));
                $transaction->t_time = date("Y-m-d h:i:s", strtotime(str_replace("'", "", $row[1]) . ' ' . str_replace("'", "", $row[2])));
                $transaction->a_date = date("Y-m-d", strtotime(str_replace("'", "", $row[3])));
                $transaction->detail = str_replace("'", '', $row[4]);
                $transaction->expense = $row[5];
                $transaction->income = $row[6];
                $transaction->balance = $row[7];
                $transaction->ticket_num = $row[9];
                $notes = str_replace("'", "", $row[8]);
                $len = mb_strlen($notes, 'UTF-8');
                $notesResult = [];
                for ($i = 0; $i < $len; $i++) {
                    $notesResult[] = mb_substr($notes, $i, 1, 'UTF-8');
                }
                $counter = 1;
                $foundSpace = false;
                foreach($notesResult as $note) {
                    $colName = "remark_$counter";
                    if ($counter < 3) {
                        if (is_numeric($note) && $foundSpace == false) {
                            $transaction->$colName .= $note;
                        }else if (is_numeric($note) && $foundSpace == true){
                            $transaction->$colName .= $note;
                            $foundSpace = false;
                        }else {
                            if ($foundSpace == false) {
                                $foundSpace = true;
                                $counter++;
                            }
                        }
                    }else {
                        $transaction->$colName .= $note;
                    }
                }
                $transaction->save();
                $results[] = [
                    'line' => $key + 1,
                    'case_num' => $row[1],
                    'result' => '新增成功',
                ];
            }catch (\Exception $e) {
                continue;
            }

        }
        return $results;
    }
    private static function Format_TW_Date($date_string){
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        $date_list = $date_string;

        if (strpos($date_list,"-")) {
            $date_list = explode("-",$date_list);
        }else if (strpos($date_list,"/")) {
            $date_list = explode("/",$date_list);
        }else if(strpos($date_list,".")){
            $date_list = explode(".",$date_list);
        }else{
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];

        return $date;
    }

}
