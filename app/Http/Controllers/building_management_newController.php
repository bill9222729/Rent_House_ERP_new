<?php

namespace App\Http\Controllers;

use App\DataTables\building_management_newDataTable;
use App\Http\Requests;
use App\Http\Requests\Createbuilding_management_newRequest;
use App\Http\Requests\Updatebuilding_management_newRequest;
use App\Repositories\building_management_newRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class building_management_newController extends AppBaseController
{
    /** @var  building_management_newRepository */
    private $buildingManagementNewRepository;

    public function __construct(building_management_newRepository $buildingManagementNewRepo)
    {
        $this->buildingManagementNewRepository = $buildingManagementNewRepo;
    }

    /**
     * Display a listing of the building_management_new.
     *
     * @param building_management_newDataTable $buildingManagementNewDataTable
     * @return Response
     */
    public function index(building_management_newDataTable $buildingManagementNewDataTable)
    {
        return $buildingManagementNewDataTable->render('building_management_news.index');
    }

    /**
     * Show the form for creating a new building_management_new.
     *
     * @return Response
     */
    public function create()
    {
        return view('building_management_news.create');
    }

    /**
     * Store a newly created building_management_new in storage.
     *
     * @param Createbuilding_management_newRequest $request
     *
     * @return Response
     */
    public function store(Createbuilding_management_newRequest $request)
    {
        $input = $request->all();

        $buildingManagementNew = $this->buildingManagementNewRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/buildingManagementNews.singular')]));

        return redirect(route('buildingManagementNews.index'));
    }

    /**
     * Display the specified building_management_new.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $buildingManagementNew = $this->buildingManagementNewRepository->find($id);

        if (empty($buildingManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagementNews.singular')]));

            return redirect(route('buildingManagementNews.index'));
        }

        return view('building_management_news.show')->with('buildingManagementNew', $buildingManagementNew);
    }

    /**
     * Show the form for editing the specified building_management_new.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $buildingManagementNew = $this->buildingManagementNewRepository->find($id);

        if (empty($buildingManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagementNews.singular')]));

            return redirect(route('buildingManagementNews.index'));
        }

        return view('building_management_news.edit')->with('buildingManagementNew', $buildingManagementNew);
    }

    /**
     * Update the specified building_management_new in storage.
     *
     * @param  int              $id
     * @param Updatebuilding_management_newRequest $request
     *
     * @return Response
     */
    public function update($id, Updatebuilding_management_newRequest $request)
    {
        $buildingManagementNew = $this->buildingManagementNewRepository->find($id);

        if (empty($buildingManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagementNews.singular')]));

            return redirect(route('buildingManagementNews.index'));
        }

        $buildingManagementNew = $this->buildingManagementNewRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/buildingManagementNews.singular')]));

        return redirect(route('buildingManagementNews.index'));
    }

    /**
     * Remove the specified building_management_new from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $buildingManagementNew = $this->buildingManagementNewRepository->find($id);

        if (empty($buildingManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/buildingManagementNews.singular')]));

            return redirect(route('buildingManagementNews.index'));
        }

        $this->buildingManagementNewRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/buildingManagementNews.singular')]));

        return redirect(route('buildingManagementNews.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('building_management_new'), $file);

    }

}
