<?php

namespace App\Http\Controllers;

use App\DataTables\business_managementDataTable;
use App\Http\Requests;
use App\Http\Requests\Createbusiness_managementRequest;
use App\Http\Requests\Updatebusiness_managementRequest;
use App\Repositories\business_managementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Log;
use App\LoggerData;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\business_management;
use Carbon\Carbon;


class business_managementImport implements WithCalculatedFormulas
{
    public function model(array $row)
    {
        return $row;
    }
}

class business_managementController extends AppBaseController
{
    /** @var  business_managementRepository */
    private $businessManagementRepository;

    public function __construct(business_managementRepository $businessManagementRepo)
    {
        $this->businessManagementRepository = $businessManagementRepo;
    }

    /**
     * Display a listing of the business_management.
     *
     * @param business_managementDataTable $businessManagementDataTable
     * @return Response
     */
    public function index(business_managementDataTable $businessManagementDataTable)
    {
        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "index";
        $LoggerData->save();
        return $businessManagementDataTable->render('business_managements.index');
    }

    /**
     * Show the form for creating a new business_management.
     *
     * @return Response
     */
    public function create()
    {
        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "create";
        $LoggerData->save();
        return view('business_managements.create');
    }

    /**
     * Store a newly created business_management in storage.
     *
     * @param Createbusiness_managementRequest $request
     *
     * @return Response
     */
    public function store(Createbusiness_managementRequest $request)
    {
        $input = $request->all();
        $input['birthday'] = self::Format_TW_Date($input['birthday']);

        $businessManagement = $this->businessManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/businessManagements.singular')]));

        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "store";
        $LoggerData->save();
        return redirect(route('businessManagements.index'));
    }

    /**
     * Display the specified business_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $businessManagement = $this->businessManagementRepository->find($id);

        if (empty($businessManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/businessManagements.singular')]));

            return redirect(route('businessManagements.index'));
        }

        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "show";
        $LoggerData->save();
        return view('business_managements.show')->with('businessManagement', $businessManagement);
    }

    /**
     * Show the form for editing the specified business_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $businessManagement = $this->businessManagementRepository->find($id);

        if (empty($businessManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/businessManagements.singular')]));

            return redirect(route('businessManagements.index'));
        }

        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "edit";
        $LoggerData->save();
        return view('business_managements.edit')->with('businessManagement', $businessManagement);
    }

    /**
     * Update the specified business_management in storage.
     *
     * @param  int              $id
     * @param Updatebusiness_managementRequest $request
     *
     * @return Response
     */
    public function update($id, Updatebusiness_managementRequest $request)
    {
        $businessManagement = $this->businessManagementRepository->find($id);

        if (empty($businessManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/businessManagements.singular')]));

            return redirect(route('businessManagements.index'));
        }

        $input = $request->all();
        $input['birthday'] = self::Format_TW_Date($input['birthday']);
        $businessManagement = $this->businessManagementRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/businessManagements.singular')]));

        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "update";
        $LoggerData->save();
        return redirect(route('businessManagements.index'));
    }

    /**
     * Remove the specified business_management from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $businessManagement = $this->businessManagementRepository->find($id);

        if (empty($businessManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/businessManagements.singular')]));

            return redirect(route('businessManagements.index'));
        }

        $this->businessManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/businessManagements.singular')]));

        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "destroy";
        $LoggerData->save();
        return redirect(route('businessManagements.index'));
    }

    public function file_upload($file)
    {
        Log::info('123');
        // Log::info("[".__METHOD__."][".__LINE__."]"."[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]");
        $LoggerData = new LoggerData;
        $LoggerData->locate = __METHOD__;
        $LoggerData->line = __LINE__;
        $LoggerData->message = "[user_id: ".auth()->user()->id."]"."[Time: ".date("Y_m_d_H_i_s")."]";
        $LoggerData->action = "file_upload";
        $LoggerData->save();
        return Storage::put(storage_path('business_management'), $file);

    }

    public function import_build(Request $request)
    {
        return self::processExcel($request->file('import_file'));
    }


    public static function processExcel($file) {
        Log::info('test');
        $results = []; // a arrany of { line: number, case_num: string, result: "inserted" | "skipped" }
        $match_value_column_num = 0;
        $option_column_num = 0;
        $id_column_num = 0;
        $reader = Excel::toCollection(new business_managementImport, $file);

        $excel_data = $reader[0];
        unset($excel_data[0]);
        foreach ($excel_data as $key => $value) {

            $business_management = new business_management;
            $business_management->name = ($value[0] == null)?"空":$value[0];
            $business_management->address = ($value[1] == null)?"空":$value[1];
            $business_management->phone = ($value[2] == null)?"空":$value[2];
            $business_management->id_number = ($value[3] == null)?"空":$value[3];
            $business_management->birthday = ($value[4] == null)?DATE(Carbon::now()):self::Format_TW_Date($value[4]);
            $business_management->bank_account = ($value[5] == null)?"空":$value[5];
            $business_management->management_fee_set = ($value[6] == null)?"未設定":$value[6];
            $business_management->match_fee_set = ($value[7] == null)?"未設定":$value[7];
            $business_management->escrow_quota = ($value[8] == null)?0:$value[8];
            $business_management->box_quota = ($value[9] == null)?0:$value[9];
            $business_management->save();

            $results[] = [
                'line' => $key + 1,
                'case_num' => $value[1],
                'result' => '新增成功',
            ];
            continue;
        }
        return $results;
    }

    private static function Format_TW_Date($date_string){
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list,"/")) {
            $date_list = explode("/",$date_list);
        }else if(strpos($date_list,".")){
            $date_list = explode(".",$date_list);
        }else if(strpos($date_list,"-")){
            $date_list = explode("-",$date_list);
        }else{
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        return $date;
    }

}
