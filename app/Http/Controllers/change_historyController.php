<?php

namespace App\Http\Controllers;

use App\DataTables\change_historyDataTable;
use App\Http\Requests;
use App\Http\Requests\Createchange_historyRequest;
use App\Http\Requests\Updatechange_historyRequest;
use App\Repositories\change_historyRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class change_historyController extends AppBaseController
{
    /** @var  change_historyRepository */
    private $changeHistoryRepository;

    public function __construct(change_historyRepository $changeHistoryRepo)
    {
        $this->changeHistoryRepository = $changeHistoryRepo;
    }

    /**
     * Display a listing of the change_history.
     *
     * @param change_historyDataTable $changeHistoryDataTable
     * @return Response
     */
    public function index(change_historyDataTable $changeHistoryDataTable)
    {
        return $changeHistoryDataTable->render('change_histories.index');
    }

    /**
     * Show the form for creating a new change_history.
     *
     * @return Response
     */
    public function create()
    {
        return view('change_histories.create');
    }

    /**
     * Store a newly created change_history in storage.
     *
     * @param Createchange_historyRequest $request
     *
     * @return Response
     */
    public function store(Createchange_historyRequest $request)
    {
        $input = $request->all();

        $changeHistory = $this->changeHistoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/changeHistories.singular')]));

        return redirect(route('changeHistories.index'));
    }

    /**
     * Display the specified change_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $changeHistory = $this->changeHistoryRepository->find($id);

        if (empty($changeHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/changeHistories.singular')]));

            return redirect(route('changeHistories.index'));
        }

        return view('change_histories.show')->with('changeHistory', $changeHistory);
    }

    /**
     * Show the form for editing the specified change_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $changeHistory = $this->changeHistoryRepository->find($id);

        if (empty($changeHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/changeHistories.singular')]));

            return redirect(route('changeHistories.index'));
        }

        return view('change_histories.edit')->with('changeHistory', $changeHistory);
    }

    /**
     * Update the specified change_history in storage.
     *
     * @param  int              $id
     * @param Updatechange_historyRequest $request
     *
     * @return Response
     */
    public function update($id, Updatechange_historyRequest $request)
    {
        $changeHistory = $this->changeHistoryRepository->find($id);

        if (empty($changeHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/changeHistories.singular')]));

            return redirect(route('changeHistories.index'));
        }

        $changeHistory = $this->changeHistoryRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/changeHistories.singular')]));

        return redirect(route('changeHistories.index'));
    }

    /**
     * Remove the specified change_history from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $changeHistory = $this->changeHistoryRepository->find($id);

        if (empty($changeHistory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/changeHistories.singular')]));

            return redirect(route('changeHistories.index'));
        }

        $this->changeHistoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/changeHistories.singular')]));

        return redirect(route('changeHistories.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('change_history'), $file);

    }

}
