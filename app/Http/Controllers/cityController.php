<?php

namespace App\Http\Controllers;

use App\DataTables\cityDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatecityRequest;
use App\Http\Requests\UpdatecityRequest;
use App\Repositories\cityRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class cityController extends AppBaseController
{
    /** @var  cityRepository */
    private $cityRepository;

    public function __construct(cityRepository $cityRepo)
    {
        $this->cityRepository = $cityRepo;
    }

    /**
     * Display a listing of the city.
     *
     * @param cityDataTable $cityDataTable
     * @return Response
     */
    public function index(cityDataTable $cityDataTable)
    {
        return $cityDataTable->render('cities.index');
    }

    /**
     * Show the form for creating a new city.
     *
     * @return Response
     */
    public function create()
    {
        return view('cities.create');
    }

    /**
     * Store a newly created city in storage.
     *
     * @param CreatecityRequest $request
     *
     * @return Response
     */
    public function store(CreatecityRequest $request)
    {
        $input = $request->all();

        $city = $this->cityRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/cities.singular')]));

        return redirect(route('cities.index'));
    }

    /**
     * Display the specified city.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cities.singular')]));

            return redirect(route('cities.index'));
        }

        return view('cities.show')->with('city', $city);
    }

    /**
     * Show the form for editing the specified city.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cities.singular')]));

            return redirect(route('cities.index'));
        }

        return view('cities.edit')->with('city', $city);
    }

    /**
     * Update the specified city in storage.
     *
     * @param  int              $id
     * @param UpdatecityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecityRequest $request)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cities.singular')]));

            return redirect(route('cities.index'));
        }

        $city = $this->cityRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/cities.singular')]));

        return redirect(route('cities.index'));
    }

    /**
     * Remove the specified city from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cities.singular')]));

            return redirect(route('cities.index'));
        }

        $this->cityRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/cities.singular')]));

        return redirect(route('cities.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('city'), $file);

    }

}
