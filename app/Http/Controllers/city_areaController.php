<?php

namespace App\Http\Controllers;

use App\DataTables\city_areaDataTable;
use App\Http\Requests;
use App\Http\Requests\Createcity_areaRequest;
use App\Http\Requests\Updatecity_areaRequest;
use App\Repositories\city_areaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class city_areaController extends AppBaseController
{
    /** @var  city_areaRepository */
    private $cityAreaRepository;

    public function __construct(city_areaRepository $cityAreaRepo)
    {
        $this->cityAreaRepository = $cityAreaRepo;
    }

    /**
     * Display a listing of the city_area.
     *
     * @param city_areaDataTable $cityAreaDataTable
     * @return Response
     */
    public function index(city_areaDataTable $cityAreaDataTable)
    {
        return $cityAreaDataTable->render('city_areas.index');
    }

    /**
     * Show the form for creating a new city_area.
     *
     * @return Response
     */
    public function create()
    {
        return view('city_areas.create');
    }

    /**
     * Store a newly created city_area in storage.
     *
     * @param Createcity_areaRequest $request
     *
     * @return Response
     */
    public function store(Createcity_areaRequest $request)
    {
        $input = $request->all();

        $cityArea = $this->cityAreaRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/cityAreas.singular')]));

        return redirect(route('cityAreas.index'));
    }

    /**
     * Display the specified city_area.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cityArea = $this->cityAreaRepository->find($id);

        if (empty($cityArea)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cityAreas.singular')]));

            return redirect(route('cityAreas.index'));
        }

        return view('city_areas.show')->with('cityArea', $cityArea);
    }

    /**
     * Show the form for editing the specified city_area.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cityArea = $this->cityAreaRepository->find($id);

        if (empty($cityArea)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cityAreas.singular')]));

            return redirect(route('cityAreas.index'));
        }

        return view('city_areas.edit')->with('cityArea', $cityArea);
    }

    /**
     * Update the specified city_area in storage.
     *
     * @param  int              $id
     * @param Updatecity_areaRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecity_areaRequest $request)
    {
        $cityArea = $this->cityAreaRepository->find($id);

        if (empty($cityArea)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cityAreas.singular')]));

            return redirect(route('cityAreas.index'));
        }

        $cityArea = $this->cityAreaRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/cityAreas.singular')]));

        return redirect(route('cityAreas.index'));
    }

    /**
     * Remove the specified city_area from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cityArea = $this->cityAreaRepository->find($id);

        if (empty($cityArea)) {
            Flash::error(__('messages.not_found', ['model' => __('models/cityAreas.singular')]));

            return redirect(route('cityAreas.index'));
        }

        $this->cityAreaRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/cityAreas.singular')]));

        return redirect(route('cityAreas.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('city_area'), $file);

    }

}
