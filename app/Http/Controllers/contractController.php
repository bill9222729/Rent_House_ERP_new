<?php

namespace App\Http\Controllers;

use App\DataTables\contractDataTable;
use App\DataTables\contractMixedDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatecontractRequest;
use App\Http\Requests\UpdatecontractRequest;
use App\Repositories\contractRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\FromArray;
use Response;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\contract;
use App\Models\BuildingManagement;
use App\Models\TenantManagement;

class ContractImport implements WithCalculatedFormulas
{
    public function model(array $row)
    {
        return $row;
    }
}

class ContractMixedImport implements FromArray
{
    protected $ary;

    function __construct($array)
    {
        $this->ary = $array;
    }

    public function array(): array
    {
        return $this->ary;
    }
}

class contractController extends AppBaseController
{
    /** @var  contractRepository */
    private $contractRepository;

    public function __construct(contractRepository $contractRepo)
    {
        $this->contractRepository = $contractRepo;
    }

    /**
     * Display a listing of the contract.
     *
     * @param contractDataTable $contractDataTable
     * @return Response
     */
    public function index(contractDataTable $contractDataTable)
    {
        return $contractDataTable->render('contracts.index');
    }

    /**
     * Show the form for creating a new contract.
     *
     * @return Response
     */
    public function create()
    {
        return view('contracts.create');
    }

    /**
     * Store a newly created contract in storage.
     *
     * @param CreatecontractRequest $request
     *
     * @return Response
     */
    public function store(CreatecontractRequest $request)
    {
        $input = $request->all();

        $input['contract_match_date'] = $this->Format_TW_Date($input['contract_match_date']);
        $input['landlord_contract_date'] = $this->Format_TW_Date($input['landlord_contract_date']);
        $input['landlord_contract_expiry_date'] = $this->Format_TW_Date($input['landlord_contract_expiry_date']);
        $contract = $this->contractRepository->create($input);

        $contract->file_upload = $this->createFilesJson($request);
        $contract->save();

        $this->storeContractFiles($request, $contract);

        Flash::success(__('messages.saved', ['model' => __('models/contracts.singular')]));

        return redirect(route('contracts.index'));
    }

    /**
     * Display the specified contract.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }

        return view('contracts.edit')->with(['contract' => $contract, 'show' => true]);
    }

    /**
     * Show the form for editing the specified contract.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }

        return view('contracts.edit')->with('contract', $contract);
    }

    /**
     * Update the specified contract in storage.
     *
     * @param int $id
     * @param UpdatecontractRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecontractRequest $request)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }
        $input = $request->all();

        $input['contract_match_date'] = $this->Format_TW_Date($input['contract_match_date']);
        $input['landlord_contract_date'] = $this->Format_TW_Date($input['landlord_contract_date']);
        $input['landlord_contract_expiry_date'] = $this->Format_TW_Date($input['landlord_contract_expiry_date']);

        if ($request->has('file_upload')) {
            unset($input['file_upload']);
        }

        $contract = $this->contractRepository->update($input, $id);
        $contract->file_upload = $this->createFilesJson($request, $contract);
        $contract->save();
        $this->storeContractFiles($request, $contract, 'edit');

        Flash::success(__('messages.updated', ['model' => __('models/contracts.singular')]));

        return redirect(route('contracts.index'));
    }

    /**
     * Remove the specified contract from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }

        $this->contractRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/contracts.singular')]));

        return redirect(route('contracts.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('contract'), $file);
    }

    public function UploadExcel(Request $request)
    {
        $file = $request->file('upload_file');

        if (isset($file)) {
            // $file_read = '';
            self::processExcel($file);
            return redirect(route('contracts.index'));
        } else {
            return redirect(route('contracts.index'));
        }
    }

    private static function Format_TW_Date($date_string)
    {
        // print_r($date_string);
        if (!$date_string) {
            return null;
        }
        if ($date_string == "房東自修") {
            return "房東自修";
        }
        $date_list = $date_string;

        if (strpos($date_list, "/")) {
            $date_list = explode("/", $date_list);
        } else if (strpos($date_list, ".")) {
            $date_list = explode(".", $date_list);
        } else if (strpos($date_list, "-")) {
            $date_list = explode("-", $date_list);
        } else {
            return "日期格式錯誤";
        }

        $date_Y = (int)$date_list[0];
        if ($date_Y <= 1911) {
            $date_Y = $date_Y + 1911;
        }
        $date_Y = strval($date_Y);
        $date = $date_Y . "-" . $date_list[1] . "-" . $date_list[2];
        return $date;
    }

    public static function processExcel($file)
    {
        $results = []; // a arrany of { line: number, case_num: string, result: "inserted" | "skipped" }
        $reader = Excel::toCollection(new ContractImport, $file);

        $excel_data = $reader[0];
        // print_r($excel_data[0][2]);
        // exit();
        $importType = $excel_data[0][0];

        unset($excel_data[0]);
        foreach ($excel_data as $key => $value) {
            if (!$value[0]) {
                continue;
            }
            //            dd($value);
            $find_match = contract::where('landlord_item_number', $value[7])->first();
            if ($find_match) {
                $results[] = [
                    'line' => $key + 1,
                    'match_num' => $value[7],
                    'result' => '重複輸入',
                    'reason' => 'already inserted', // debug
                ];
                continue;
            }
            $find_building = BuildingManagement::where('num', $value[7])->first();
            if ($importType == '包租') {
                $find_tenant = TenantManagement::where('tenant_case_number', $value[34])->first();
            } else {
                $find_tenant = true;
            }

            if (!$find_building || !$find_tenant) {
                $results[] = [
                    'line' => $key + 1,
                    'match_num' => $value[7],
                    'result' => '房東或房客資料缺失',
                    'reason' => (!$find_building ? 'No building ' : '') . (!$find_tenant ? 'No tenant' : ''), // debug
                ];
                continue;
            }
            try {
                $contract = new contract;
                $contract->landlord_existing_tenant = $value[3];
                $contract->landlord_item_number = $value[7];
                $contract->landlord_ministry_of_the_interior_number = $value[8];
                $contract->actual_contract_rent_to_landlord = $value[15];
                $contract->actual_contract_tenant_pays_rent = $value[16];
                $contract->actual_contract_rent_subsidy = $value[17];
                $contract->actual_contract_deposit = $value[18];
                if ($importType == '包租約編號') {
                    $contract->b_id = $value[0];
                    $contract->tb_id = $value[1];
                    $contract->landlord_contract_date = strval($value[2]);
                    $contract->develop_price = is_numeric($value[21]) ? $value[21] : 0;
                    $contract->bmanagement_price = is_numeric($value[22]) ? $value[22] : 0;
                    $contract->landlord_contract_date = $value[27];
                    $contract->landlord_contract_expiry_date = $value[29];
                    $contract->notary = $value[29];
                    $contract->landlord_item_number = $value[32];
                    $contract->landlord_ministry_of_the_interior_number = $value[33];
                    $contract->remark_1 = $value[43];
                    $contract->to_the_landlord_rent_day = $value[45];
                    $contract->save();
                } else {
                    $contract = new contract;
                    $contract->landlord_match_number = $value[0];
                    $contract->landlord_contract_date = $value[1];
                    $contract->search_price = is_numeric($value[21]) ? $value[21] : 0;
                    $contract->management_price = is_numeric($value[22]) ? $value[22] : 0;
                    $contract->landlord_contract_date = $value[37];
                    $contract->landlord_contract_expiry_date = $value[39];
                    $contract->notary = $value[2];
                    $contract->remark_1 = $value[40];
                    $contract->to_the_landlord_rent_day = $value[46];
                    $contract->save();
                }
            } catch (\Exception $exception) {
                $results[] = [
                    'line' => $key + 1,
                    'match_num' => $value[7],
                    'result' => $exception->getMessage(),
                    'reason' => $exception->getMessage()
                ];
                continue;
            }
        }

        return $results;
    }

    private static function fixYearMonth($str)
    {
        if (strpos($str, "/")) {
            $ary = explode("/", $str);
        } else if (strpos($str, ".")) {
            $ary = explode(".", $str);
        } else if (strpos($str, "-")) {
            $ary = explode("-", $str);
        } else return $str;
        $year = (int)$ary[0];
        if ($year < 1911) return ($year + 1911) . '-' . $ary[1];
        return $ary[0] . '-' . $ary[1];
    }

    private function storeContractFiles(Request $request, $contract, $method = 'create')
    {
        if ($request->hasFile("file_upload")) {
            $files = $request->file('file_upload');
            $fileStorePaths = '';
            foreach ($files as $file) {
                $fileStorePaths .= $file->store('contract' . $contract->id, 'public') . ',';
            }
            $contract->file_upload = $method == 'create' ? $fileStorePaths : $contract->file_upload . $fileStorePaths;
            $contract->save();
        }
    }

    public function mixedTable(contractMixedDataTable $contractDataTable)
    {
        return $contractDataTable->render('mixedTable');
    }

    public function generateMixedData()
    {
        $contracts = contract::with('buildingManagement')->get();
        // dd($contracts);

        $data = [
            ['房東_媒合編號', '業務姓名', '房東_契約起日', '房東_契約訖日', '房東_物件編號', '物件地址', '房東_類型', '收款人', '收款人ID', '銀行名稱', '銀行代號', '銀行帳號', '房客自付額', '租金補助', '管理費', '停車費', '修繕費', '手續費', '其他', '應匯租金', '備註']
        ];

        foreach ($contracts as $contract) {
            #避免沒關聯的空值
            $build_error_msg =  "尚未有對應的物件";
            $located_build_address = $build_error_msg;
            $collection_agent = $build_error_msg;
            $build_mag_work_col = ['located_build_address', 'collection_agent', 'agent_ID_num', 'agent_bank_name', 'agent_branch_name', 'landlord_bank_account', 'actual_contract_tenant_pays_rent', 'actual_contract_rent_subsidy'];

            $build_info = [];
            foreach ($build_mag_work_col as $key => $value) {
                if ($contract->buildingManagement != null) {
                    $build_info[$value] = $contract->buildingManagement->{$value};
                } else {
                    $build_info[$value] = $build_error_msg;
                }
            }
            $contract->buildingManagement = json_decode(json_encode($build_info));
            $data[] = [
                strval($contract->landlord_match_number),
                strval($contract->contract_business_sign_back),
                strval($contract->landlord_contract_date),
                strval($contract->landlord_contract_expiry_date),
                strval($contract->landlord_item_number),
                strval($contract->buildingManagement->located_build_address),
                strval($contract->landlord_types_of),
                strval($contract->buildingManagement->collection_agent),
                strval($contract->buildingManagement->agent_ID_num),
                strval($contract->buildingManagement->agent_bank_name),
                strval($contract->buildingManagement->agent_branch_name),
                strval($contract->buildingManagement->landlord_bank_account),
                strval($contract->buildingManagement->actual_contract_tenant_pays_rent),
                strval($contract->buildingManagement->actual_contract_rent_subsidy)
            ];
        }
        $export = new ContractMixedImport($data);
        return Excel::download($export, 'output.xlsx');
    }

    private function createFilesJson(Request $request, $contract = null)
    {
        $files = [
            'contract_book' => [],
            'b_contract_book' => [],
            't_contract_book' => [],
            'n_invoke' => [],
            'i_book' => [],
            's_book' => [],
            'stop_book' => [],
            'g_book' => [],
            'o_book' => []
        ];

        if ($contract) {
            $files = $contract->file_upload ? json_decode($contract->file_upload, true) : $files;
        }

        foreach ($files as $key => $value) {
            if ($request->hasFile($key)) {
                $files[$key][] = $request->file($key)->store('contract', 'public');
            }
        }

        return json_encode($files);
    }
}
