<?php

namespace App\Http\Controllers;

use App\DataTables\contract_managementDataTable;
use App\Http\Requests;
use App\Http\Requests\Createcontract_managementRequest;
use App\Http\Requests\Updatecontract_managementRequest;
use App\Repositories\contract_managementRepository;
use App\Models\match_management;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class contract_managementController extends AppBaseController
{
    /** @var  contract_managementRepository */
    private $contractManagementRepository;

    public function __construct(contract_managementRepository $contractManagementRepo)
    {
        $this->contractManagementRepository = $contractManagementRepo;
    }

    /**
     * Display a listing of the contract_management.
     *
     * @param contract_managementDataTable $contractManagementDataTable
     * @return Response
     */
    public function index(contract_managementDataTable $contractManagementDataTable)
    {
        return $contractManagementDataTable->render('contract_managements.index');
    }

    /**
     * Show the form for creating a new contract_management.
     *
     * @return Response
     */
    public function create($match_id)
    {
        $matchManagement = match_management::where("id", $match_id)->first();
        return view('contract_managements.create')->with('matchManagement', $matchManagement);
    }

    /**
     * 自己新增的代管約新增資料頁面
     *
     * @return Response
     */
    public function escrow_create($match_id)
    {
        $matchManagement = match_management::where("match_id", $match_id)->first();
        return view('contract_managements.escrow_create')->with('matchManagement', $matchManagement);
    }


    /**
     * 自己新增的包租約新增資料頁面
     *
     * @return Response
     */
    public function charter_create($match_id)
    {
        $matchManagement = match_management::where("match_id", $match_id)->first();
        return view('contract_managements.charter_create')->with('matchManagement', $matchManagement);
    }

    /**
     * Store a newly created contract_management in storage.
     *
     * @param Createcontract_managementRequest $request
     *
     * @return Response
     */
    public function store(Createcontract_managementRequest $request)
    {
        $input = $request->all();

        $contractManagement = $this->contractManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/contractManagements.singular')]));

        return redirect(route('contractManagements.index'));
    }

    /**
     * Display the specified contract_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contractManagement = $this->contractManagementRepository->find($id);

        if (empty($contractManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractManagements.singular')]));

            return redirect(route('contractManagements.index'));
        }

        return view('contract_managements.show')->with('contractManagement', $contractManagement);
    }

    /**
     * Show the form for editing the specified contract_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contractManagement = $this->contractManagementRepository->find($id);
        $matchManagement = match_management::where("id", "=", $contractManagement["match_id"])->firstOrFail();
        if (empty($contractManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractManagements.singular')]));

            return redirect(route('contractManagements.index'));
        }

        return view('contract_managements.edit')->with('contractManagement', $contractManagement)->with('matchManagement', $matchManagement);
    }

    /**
     * Update the specified contract_management in storage.
     *
     * @param  int              $id
     * @param Updatecontract_managementRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecontract_managementRequest $request)
    {
        $contractManagement = $this->contractManagementRepository->find($id);

        if (empty($contractManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractManagements.singular')]));

            return redirect(route('contractManagements.index'));
        }

        $contractManagement = $this->contractManagementRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/contractManagements.singular')]));

        return redirect(route('contractManagements.index'));
    }

    /**
     * Remove the specified contract_management from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contractManagement = $this->contractManagementRepository->find($id);

        if (empty($contractManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractManagements.singular')]));

            return redirect(route('contractManagements.index'));
        }

        $this->contractManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/contractManagements.singular')]));

        return redirect(route('contractManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('contract_management'), $file);
    }
}
