<?php

namespace App\Http\Controllers;

use App\DataTables\contracts_newDataTable;
use App\Http\Requests;
use App\Http\Requests\Createcontracts_newRequest;
use App\Http\Requests\Updatecontracts_newRequest;
use App\Repositories\contracts_newRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class contracts_newController extends AppBaseController
{
    /** @var  contracts_newRepository */
    private $contractsNewRepository;

    public function __construct(contracts_newRepository $contractsNewRepo)
    {
        $this->contractsNewRepository = $contractsNewRepo;
    }

    /**
     * Display a listing of the contracts_new.
     *
     * @param contracts_newDataTable $contractsNewDataTable
     * @return Response
     */
    public function index(contracts_newDataTable $contractsNewDataTable)
    {
        return $contractsNewDataTable->render('contracts_news.index');
    }

    /**
     * Show the form for creating a new contracts_new.
     *
     * @return Response
     */
    public function create()
    {
        return view('contracts_news.create');
    }

    /**
     * Store a newly created contracts_new in storage.
     *
     * @param Createcontracts_newRequest $request
     *
     * @return Response
     */
    public function store(Createcontracts_newRequest $request)
    {
        $input = $request->all();

        $contractsNew = $this->contractsNewRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/contractsNews.singular')]));

        return redirect(route('contractsNews.index'));
    }

    /**
     * Display the specified contracts_new.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contractsNew = $this->contractsNewRepository->find($id);

        if (empty($contractsNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractsNews.singular')]));

            return redirect(route('contractsNews.index'));
        }

        return view('contracts_news.show')->with('contractsNew', $contractsNew);
    }

    /**
     * Show the form for editing the specified contracts_new.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contractsNew = $this->contractsNewRepository->find($id);

        if (empty($contractsNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractsNews.singular')]));

            return redirect(route('contractsNews.index'));
        }

        return view('contracts_news.edit')->with('contractsNew', $contractsNew);
    }

    /**
     * Update the specified contracts_new in storage.
     *
     * @param  int              $id
     * @param Updatecontracts_newRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecontracts_newRequest $request)
    {
        $contractsNew = $this->contractsNewRepository->find($id);

        if (empty($contractsNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractsNews.singular')]));

            return redirect(route('contractsNews.index'));
        }

        $contractsNew = $this->contractsNewRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/contractsNews.singular')]));

        return redirect(route('contractsNews.index'));
    }

    /**
     * Remove the specified contracts_new from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contractsNew = $this->contractsNewRepository->find($id);

        if (empty($contractsNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contractsNews.singular')]));

            return redirect(route('contractsNews.index'));
        }

        $this->contractsNewRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/contractsNews.singular')]));

        return redirect(route('contractsNews.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('contracts_new'), $file);
    }
}
