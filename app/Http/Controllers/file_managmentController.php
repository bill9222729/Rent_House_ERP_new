<?php

namespace App\Http\Controllers;

use App\DataTables\file_managmentDataTable;
use App\Http\Requests;
use App\Http\Requests\Createfile_managmentRequest;
use App\Http\Requests\Updatefile_managmentRequest;
use App\Repositories\file_managmentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class file_managmentController extends AppBaseController
{
    /** @var  file_managmentRepository */
    private $fileManagmentRepository;

    public function __construct(file_managmentRepository $fileManagmentRepo)
    {
        $this->fileManagmentRepository = $fileManagmentRepo;
    }

    /**
     * Display a listing of the file_managment.
     *
     * @param file_managmentDataTable $fileManagmentDataTable
     * @return Response
     */
    public function index(file_managmentDataTable $fileManagmentDataTable)
    {
        return $fileManagmentDataTable->render('file_managments.index');
    }

    /**
     * Show the form for creating a new file_managment.
     *
     * @return Response
     */
    public function create()
    {
        return view('file_managments.create');
    }

    /**
     * Store a newly created file_managment in storage.
     *
     * @param Createfile_managmentRequest $request
     *
     * @return Response
     */
    public function store(Createfile_managmentRequest $request)
    {
        $input = $request->all();

        $fileManagment = $this->fileManagmentRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/fileManagments.singular')]));

        return redirect(route('fileManagments.index'));
    }

    /**
     * Display the specified file_managment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fileManagment = $this->fileManagmentRepository->find($id);

        if (empty($fileManagment)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fileManagments.singular')]));

            return redirect(route('fileManagments.index'));
        }

        return view('file_managments.show')->with('fileManagment', $fileManagment);
    }

    /**
     * Show the form for editing the specified file_managment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $fileManagment = $this->fileManagmentRepository->find($id);

        if (empty($fileManagment)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fileManagments.singular')]));

            return redirect(route('fileManagments.index'));
        }

        return view('file_managments.edit')->with('fileManagment', $fileManagment);
    }

    /**
     * Update the specified file_managment in storage.
     *
     * @param  int              $id
     * @param Updatefile_managmentRequest $request
     *
     * @return Response
     */
    public function update($id, Updatefile_managmentRequest $request)
    {
        $fileManagment = $this->fileManagmentRepository->find($id);

        if (empty($fileManagment)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fileManagments.singular')]));

            return redirect(route('fileManagments.index'));
        }

        $fileManagment = $this->fileManagmentRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/fileManagments.singular')]));

        return redirect(route('fileManagments.index'));
    }

    /**
     * Remove the specified file_managment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fileManagment = $this->fileManagmentRepository->find($id);

        if (empty($fileManagment)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fileManagments.singular')]));

            return redirect(route('fileManagments.index'));
        }

        $this->fileManagmentRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/fileManagments.singular')]));

        return redirect(route('fileManagments.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('file_managment'), $file);

    }

}
