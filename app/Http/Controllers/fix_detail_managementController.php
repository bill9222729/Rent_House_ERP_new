<?php

namespace App\Http\Controllers;

use App\DataTables\fix_detail_managementDataTable;
use App\Http\Requests;
use App\Http\Requests\Createfix_detail_managementRequest;
use App\Http\Requests\Updatefix_detail_managementRequest;
use App\Repositories\fix_detail_managementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class fix_detail_managementController extends AppBaseController
{
    /** @var  fix_detail_managementRepository */
    private $fixDetailManagementRepository;

    public function __construct(fix_detail_managementRepository $fixDetailManagementRepo)
    {
        $this->fixDetailManagementRepository = $fixDetailManagementRepo;
    }

    /**
     * Display a listing of the fix_detail_management.
     *
     * @param fix_detail_managementDataTable $fixDetailManagementDataTable
     * @return Response
     */
    public function index(fix_detail_managementDataTable $fixDetailManagementDataTable)
    {
        return $fixDetailManagementDataTable->render('fix_detail_managements.index');
    }

    /**
     * Show the form for creating a new fix_detail_management.
     *
     * @return Response
     */
    public function create()
    {
        return view('fix_detail_managements.create');
    }

    /**
     * Store a newly created fix_detail_management in storage.
     *
     * @param Createfix_detail_managementRequest $request
     *
     * @return Response
     */
    public function store(Createfix_detail_managementRequest $request)
    {
        $input = $request->all();

        $fixDetailManagement = $this->fixDetailManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/fixDetailManagements.singular')]));

        return redirect(route('fixDetailManagements.index'));
    }

    /**
     * Display the specified fix_detail_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fixDetailManagement = $this->fixDetailManagementRepository->find($id);

        if (empty($fixDetailManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixDetailManagements.singular')]));

            return redirect(route('fixDetailManagements.index'));
        }

        return view('fix_detail_managements.show')->with('fixDetailManagement', $fixDetailManagement);
    }

    /**
     * Show the form for editing the specified fix_detail_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $fixDetailManagement = $this->fixDetailManagementRepository->find($id);

        if (empty($fixDetailManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixDetailManagements.singular')]));

            return redirect(route('fixDetailManagements.index'));
        }

        return view('fix_detail_managements.edit')->with('fixDetailManagement', $fixDetailManagement);
    }

    /**
     * Update the specified fix_detail_management in storage.
     *
     * @param  int              $id
     * @param Updatefix_detail_managementRequest $request
     *
     * @return Response
     */
    public function update($id, Updatefix_detail_managementRequest $request)
    {
        $fixDetailManagement = $this->fixDetailManagementRepository->find($id);

        if (empty($fixDetailManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixDetailManagements.singular')]));

            return redirect(route('fixDetailManagements.index'));
        }

        $fixDetailManagement = $this->fixDetailManagementRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/fixDetailManagements.singular')]));

        return redirect(route('fixDetailManagements.index'));
    }

    /**
     * Remove the specified fix_detail_management from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fixDetailManagement = $this->fixDetailManagementRepository->find($id);

        if (empty($fixDetailManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixDetailManagements.singular')]));

            return redirect(route('fixDetailManagements.index'));
        }

        $this->fixDetailManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/fixDetailManagements.singular')]));

        return redirect(route('fixDetailManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('fix_detail_management'), $file);

    }

}
