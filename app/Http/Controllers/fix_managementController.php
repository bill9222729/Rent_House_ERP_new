<?php

namespace App\Http\Controllers;

use App\DataTables\fix_managementDataTable;
use App\Http\Requests;
use App\Http\Requests\Createfix_managementRequest;
use App\Http\Requests\Updatefix_managementRequest;
use App\Repositories\fix_managementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class fix_managementController extends AppBaseController
{
    /** @var  fix_managementRepository */
    private $fixManagementRepository;

    public function __construct(fix_managementRepository $fixManagementRepo)
    {
        $this->fixManagementRepository = $fixManagementRepo;
    }

    /**
     * Display a listing of the fix_management.
     *
     * @param fix_managementDataTable $fixManagementDataTable
     * @return Response
     */
    public function index(fix_managementDataTable $fixManagementDataTable)
    {
        return $fixManagementDataTable->render('fix_managements.index');
    }

    /**
     * Show the form for creating a new fix_management.
     *
     * @return Response
     */
    public function create()
    {
        return view('fix_managements.create');
    }

    /**
     * Store a newly created fix_management in storage.
     *
     * @param Createfix_managementRequest $request
     *
     * @return Response
     */
    public function store(Createfix_managementRequest $request)
    {
        $input = $request->all();

        $fixManagement = $this->fixManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/fixManagements.singular')]));

        return redirect(route('fixManagements.index'));
    }

    /**
     * Display the specified fix_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fixManagement = $this->fixManagementRepository->find($id);

        if (empty($fixManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixManagements.singular')]));

            return redirect(route('fixManagements.index'));
        }

        return view('fix_managements.show')->with('fixManagement', $fixManagement);
    }

    /**
     * Show the form for editing the specified fix_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $fixManagement = $this->fixManagementRepository->find($id);

        if (empty($fixManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixManagements.singular')]));

            return redirect(route('fixManagements.index'));
        }

        return view('fix_managements.edit')->with('fixManagement', $fixManagement);
    }

    /**
     * Update the specified fix_management in storage.
     *
     * @param  int              $id
     * @param Updatefix_managementRequest $request
     *
     * @return Response
     */
    public function update($id, Updatefix_managementRequest $request)
    {
        $fixManagement = $this->fixManagementRepository->find($id);

        if (empty($fixManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixManagements.singular')]));

            return redirect(route('fixManagements.index'));
        }

        $fixManagement = $this->fixManagementRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/fixManagements.singular')]));

        return redirect(route('fixManagements.index'));
    }

    /**
     * Remove the specified fix_management from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fixManagement = $this->fixManagementRepository->find($id);

        if (empty($fixManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/fixManagements.singular')]));

            return redirect(route('fixManagements.index'));
        }

        $this->fixManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/fixManagements.singular')]));

        return redirect(route('fixManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('fix_management'), $file);

    }

}
