<?php

namespace App\Http\Controllers;

use App\DataTables\insurance_managementDataTable;
use App\Http\Requests;
use App\Http\Requests\Createinsurance_managementRequest;
use App\Http\Requests\Updateinsurance_managementRequest;
use App\Repositories\insurance_managementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class insurance_managementController extends AppBaseController
{
    /** @var  insurance_managementRepository */
    private $insuranceManagementRepository;

    public function __construct(insurance_managementRepository $insuranceManagementRepo)
    {
        $this->insuranceManagementRepository = $insuranceManagementRepo;
    }

    /**
     * Display a listing of the insurance_management.
     *
     * @param insurance_managementDataTable $insuranceManagementDataTable
     * @return Response
     */
    public function index(insurance_managementDataTable $insuranceManagementDataTable)
    {
        return $insuranceManagementDataTable->render('insurance_managements.index');
    }

    /**
     * Show the form for creating a new insurance_management.
     *
     * @return Response
     */
    public function create()
    {
        return view('insurance_managements.create');
    }

    /**
     * Store a newly created insurance_management in storage.
     *
     * @param Createinsurance_managementRequest $request
     *
     * @return Response
     */
    public function store(Createinsurance_managementRequest $request)
    {
        $input = $request->all();

        $insuranceManagement = $this->insuranceManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/insuranceManagements.singular')]));

        return redirect(route('insuranceManagements.index'));
    }

    /**
     * Display the specified insurance_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $insuranceManagement = $this->insuranceManagementRepository->find($id);

        if (empty($insuranceManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/insuranceManagements.singular')]));

            return redirect(route('insuranceManagements.index'));
        }

        return view('insurance_managements.show')->with('insuranceManagement', $insuranceManagement);
    }

    /**
     * Show the form for editing the specified insurance_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $insuranceManagement = $this->insuranceManagementRepository->find($id);

        if (empty($insuranceManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/insuranceManagements.singular')]));

            return redirect(route('insuranceManagements.index'));
        }

        return view('insurance_managements.edit')->with('insuranceManagement', $insuranceManagement);
    }

    /**
     * Update the specified insurance_management in storage.
     *
     * @param  int              $id
     * @param Updateinsurance_managementRequest $request
     *
     * @return Response
     */
    public function update($id, Updateinsurance_managementRequest $request)
    {
        $insuranceManagement = $this->insuranceManagementRepository->find($id);

        if (empty($insuranceManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/insuranceManagements.singular')]));

            return redirect(route('insuranceManagements.index'));
        }

        $insuranceManagement = $this->insuranceManagementRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/insuranceManagements.singular')]));

        return redirect(route('insuranceManagements.index'));
    }

    /**
     * Remove the specified insurance_management from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $insuranceManagement = $this->insuranceManagementRepository->find($id);

        if (empty($insuranceManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/insuranceManagements.singular')]));

            return redirect(route('insuranceManagements.index'));
        }

        $this->insuranceManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/insuranceManagements.singular')]));

        return redirect(route('insuranceManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('insurance_management'), $file);

    }

}
