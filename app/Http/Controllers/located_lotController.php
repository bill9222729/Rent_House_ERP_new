<?php

namespace App\Http\Controllers;

use App\DataTables\located_lotDataTable;
use App\Http\Requests;
use App\Http\Requests\Createlocated_lotRequest;
use App\Http\Requests\Updatelocated_lotRequest;
use App\Repositories\located_lotRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class located_lotController extends AppBaseController
{
    /** @var  located_lotRepository */
    private $locatedLotRepository;

    public function __construct(located_lotRepository $locatedLotRepo)
    {
        $this->locatedLotRepository = $locatedLotRepo;
    }

    /**
     * Display a listing of the located_lot.
     *
     * @param located_lotDataTable $locatedLotDataTable
     * @return Response
     */
    public function index(located_lotDataTable $locatedLotDataTable)
    {
        return $locatedLotDataTable->render('located_lots.index');
    }

    /**
     * Show the form for creating a new located_lot.
     *
     * @return Response
     */
    public function create()
    {
        return view('located_lots.create');
    }

    /**
     * Store a newly created located_lot in storage.
     *
     * @param Createlocated_lotRequest $request
     *
     * @return Response
     */
    public function store(Createlocated_lotRequest $request)
    {
        $input = $request->all();

        $locatedLot = $this->locatedLotRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/locatedLots.singular')]));

        return redirect(route('locatedLots.index'));
    }

    /**
     * Display the specified located_lot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $locatedLot = $this->locatedLotRepository->find($id);

        if (empty($locatedLot)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locatedLots.singular')]));

            return redirect(route('locatedLots.index'));
        }

        return view('located_lots.show')->with('locatedLot', $locatedLot);
    }

    /**
     * Show the form for editing the specified located_lot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $locatedLot = $this->locatedLotRepository->find($id);

        if (empty($locatedLot)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locatedLots.singular')]));

            return redirect(route('locatedLots.index'));
        }

        return view('located_lots.edit')->with('locatedLot', $locatedLot);
    }

    /**
     * Update the specified located_lot in storage.
     *
     * @param  int              $id
     * @param Updatelocated_lotRequest $request
     *
     * @return Response
     */
    public function update($id, Updatelocated_lotRequest $request)
    {
        $locatedLot = $this->locatedLotRepository->find($id);

        if (empty($locatedLot)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locatedLots.singular')]));

            return redirect(route('locatedLots.index'));
        }

        $locatedLot = $this->locatedLotRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/locatedLots.singular')]));

        return redirect(route('locatedLots.index'));
    }

    /**
     * Remove the specified located_lot from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $locatedLot = $this->locatedLotRepository->find($id);

        if (empty($locatedLot)) {
            Flash::error(__('messages.not_found', ['model' => __('models/locatedLots.singular')]));

            return redirect(route('locatedLots.index'));
        }

        $this->locatedLotRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/locatedLots.singular')]));

        return redirect(route('locatedLots.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('located_lot'), $file);

    }

}
