<?php

namespace App\Http\Controllers;

use App\DataTables\match_managementDataTable;
use App\Http\Requests;
use App\Http\Requests\Creatematch_managementRequest;
use App\Http\Requests\Updatematch_managementRequest;
use App\Repositories\match_managementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class match_managementController extends AppBaseController
{
    /** @var  match_managementRepository */
    private $matchManagementRepository;

    public function __construct(match_managementRepository $matchManagementRepo)
    {
        $this->matchManagementRepository = $matchManagementRepo;
    }

    /**
     * Display a listing of the match_management.
     *
     * @param match_managementDataTable $matchManagementDataTable
     * @return Response
     */
    public function index(match_managementDataTable $matchManagementDataTable)
    {
        return $matchManagementDataTable->render('match_managements.index');
    }

    /**
     * Show the form for creating a new match_management.
     *
     * @return Response
     */
    public function create()
    {
        return view('match_managements.create');
    }

    /**
     * Store a newly created match_management in storage.
     *
     * @param Creatematch_managementRequest $request
     *
     * @return Response
     */
    public function store(Creatematch_managementRequest $request)
    {
        $input = $request->all();

        $matchManagement = $this->matchManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/matchManagements.singular')]));

        return redirect(route('matchManagements.index'));
    }

    /**
     * Display the specified match_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $matchManagement = $this->matchManagementRepository->find($id);

        if (empty($matchManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/matchManagements.singular')]));

            return redirect(route('matchManagements.index'));
        }

        return view('match_managements.show')->with('matchManagement', $matchManagement);
    }

    /**
     * Show the form for editing the specified match_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $matchManagement = $this->matchManagementRepository->find($id);

        if (empty($matchManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/matchManagements.singular')]));

            return redirect(route('matchManagements.index'));
        }

        return view('match_managements.edit')->with('matchManagement', $matchManagement);
    }

    /**
     * Update the specified match_management in storage.
     *
     * @param  int              $id
     * @param Updatematch_managementRequest $request
     *
     * @return Response
     */
    public function update($id, Updatematch_managementRequest $request)
    {
        $matchManagement = $this->matchManagementRepository->find($id);

        if (empty($matchManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/matchManagements.singular')]));

            return redirect(route('matchManagements.index'));
        }

        $matchManagement = $this->matchManagementRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/matchManagements.singular')]));

        return redirect(route('matchManagements.index'));
    }

    /**
     * Remove the specified match_management from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $matchManagement = $this->matchManagementRepository->find($id);

        if (empty($matchManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/matchManagements.singular')]));

            return redirect(route('matchManagements.index'));
        }

        $this->matchManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/matchManagements.singular')]));

        return redirect(route('matchManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('match_management'), $file);

    }

}
