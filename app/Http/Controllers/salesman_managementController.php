<?php

namespace App\Http\Controllers;

use App\DataTables\salesman_managementDataTable;
use App\Http\Requests;
use App\Http\Requests\Createsalesman_managementRequest;
use App\Http\Requests\Updatesalesman_managementRequest;
use App\Repositories\salesman_managementRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class salesman_managementController extends AppBaseController
{
    /** @var  salesman_managementRepository */
    private $salesmanManagementRepository;

    public function __construct(salesman_managementRepository $salesmanManagementRepo)
    {
        $this->salesmanManagementRepository = $salesmanManagementRepo;
    }

    /**
     * Display a listing of the salesman_management.
     *
     * @param salesman_managementDataTable $salesmanManagementDataTable
     * @return Response
     */
    public function index(salesman_managementDataTable $salesmanManagementDataTable)
    {
        return $salesmanManagementDataTable->render('salesman_managements.index');
    }

    /**
     * Show the form for creating a new salesman_management.
     *
     * @return Response
     */
    public function create()
    {
        return view('salesman_managements.create');
    }

    /**
     * Store a newly created salesman_management in storage.
     *
     * @param Createsalesman_managementRequest $request
     *
     * @return Response
     */
    public function store(Createsalesman_managementRequest $request)
    {
        $input = $request->all();

        $salesmanManagement = $this->salesmanManagementRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/salesmanManagements.singular')]));

        return redirect(route('salesmanManagements.index'));
    }

    /**
     * Display the specified salesman_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        return view('salesman_managements.show')->with('salesmanManagement', $salesmanManagement);
    }

    /**
     * Show the form for editing the specified salesman_management.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        return view('salesman_managements.edit')->with('salesmanManagement', $salesmanManagement);
    }

    /**
     * Update the specified salesman_management in storage.
     *
     * @param  int              $id
     * @param Updatesalesman_managementRequest $request
     *
     * @return Response
     */
    public function update($id, Updatesalesman_managementRequest $request)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        $salesmanManagement = $this->salesmanManagementRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/salesmanManagements.singular')]));

        return redirect(route('salesmanManagements.index'));
    }

    /**
     * Remove the specified salesman_management from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $salesmanManagement = $this->salesmanManagementRepository->find($id);

        if (empty($salesmanManagement)) {
            Flash::error(__('messages.not_found', ['model' => __('models/salesmanManagements.singular')]));

            return redirect(route('salesmanManagements.index'));
        }

        $this->salesmanManagementRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/salesmanManagements.singular')]));

        return redirect(route('salesmanManagements.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('salesman_management'), $file);

    }

}
