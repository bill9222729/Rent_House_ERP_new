<?php

namespace App\Http\Controllers;

use App\DataTables\streetDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatestreetRequest;
use App\Http\Requests\UpdatestreetRequest;
use App\Repositories\streetRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class streetController extends AppBaseController
{
    /** @var  streetRepository */
    private $streetRepository;

    public function __construct(streetRepository $streetRepo)
    {
        $this->streetRepository = $streetRepo;
    }

    /**
     * Display a listing of the street.
     *
     * @param streetDataTable $streetDataTable
     * @return Response
     */
    public function index(streetDataTable $streetDataTable)
    {
        return $streetDataTable->render('streets.index');
    }

    /**
     * Show the form for creating a new street.
     *
     * @return Response
     */
    public function create()
    {
        return view('streets.create');
    }

    /**
     * Store a newly created street in storage.
     *
     * @param CreatestreetRequest $request
     *
     * @return Response
     */
    public function store(CreatestreetRequest $request)
    {
        $input = $request->all();

        $street = $this->streetRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/streets.singular')]));

        return redirect(route('streets.index'));
    }

    /**
     * Display the specified street.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $street = $this->streetRepository->find($id);

        if (empty($street)) {
            Flash::error(__('messages.not_found', ['model' => __('models/streets.singular')]));

            return redirect(route('streets.index'));
        }

        return view('streets.show')->with('street', $street);
    }

    /**
     * Show the form for editing the specified street.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $street = $this->streetRepository->find($id);

        if (empty($street)) {
            Flash::error(__('messages.not_found', ['model' => __('models/streets.singular')]));

            return redirect(route('streets.index'));
        }

        return view('streets.edit')->with('street', $street);
    }

    /**
     * Update the specified street in storage.
     *
     * @param  int              $id
     * @param UpdatestreetRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatestreetRequest $request)
    {
        $street = $this->streetRepository->find($id);

        if (empty($street)) {
            Flash::error(__('messages.not_found', ['model' => __('models/streets.singular')]));

            return redirect(route('streets.index'));
        }

        $street = $this->streetRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/streets.singular')]));

        return redirect(route('streets.index'));
    }

    /**
     * Remove the specified street from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $street = $this->streetRepository->find($id);

        if (empty($street)) {
            Flash::error(__('messages.not_found', ['model' => __('models/streets.singular')]));

            return redirect(route('streets.index'));
        }

        $this->streetRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/streets.singular')]));

        return redirect(route('streets.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('street'), $file);

    }

}
