<?php

namespace App\Http\Controllers;

use App\DataTables\tenant_building_infoDataTable;
use App\Http\Requests;
use App\Http\Requests\Createtenant_building_infoRequest;
use App\Http\Requests\Updatetenant_building_infoRequest;
use App\Repositories\tenant_building_infoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class tenant_building_infoController extends AppBaseController
{
    /** @var  tenant_building_infoRepository */
    private $tenantBuildingInfoRepository;

    public function __construct(tenant_building_infoRepository $tenantBuildingInfoRepo)
    {
        $this->tenantBuildingInfoRepository = $tenantBuildingInfoRepo;
    }

    /**
     * Display a listing of the tenant_building_info.
     *
     * @param tenant_building_infoDataTable $tenantBuildingInfoDataTable
     * @return Response
     */
    public function index(tenant_building_infoDataTable $tenantBuildingInfoDataTable)
    {
        return $tenantBuildingInfoDataTable->render('tenant_building_infos.index');
    }

    /**
     * Show the form for creating a new tenant_building_info.
     *
     * @return Response
     */
    public function create()
    {
        return view('tenant_building_infos.create');
    }

    /**
     * Store a newly created tenant_building_info in storage.
     *
     * @param Createtenant_building_infoRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_building_infoRequest $request)
    {
        $input = $request->all();

        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tenantBuildingInfos.singular')]));

        return redirect(route('tenantBuildingInfos.index'));
    }

    /**
     * Display the specified tenant_building_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->find($id);

        if (empty($tenantBuildingInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantBuildingInfos.singular')]));

            return redirect(route('tenantBuildingInfos.index'));
        }

        return view('tenant_building_infos.show')->with('tenantBuildingInfo', $tenantBuildingInfo);
    }

    /**
     * Show the form for editing the specified tenant_building_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->find($id);

        if (empty($tenantBuildingInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantBuildingInfos.singular')]));

            return redirect(route('tenantBuildingInfos.index'));
        }

        return view('tenant_building_infos.edit')->with('tenantBuildingInfo', $tenantBuildingInfo);
    }

    /**
     * Update the specified tenant_building_info in storage.
     *
     * @param  int              $id
     * @param Updatetenant_building_infoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_building_infoRequest $request)
    {
        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->find($id);

        if (empty($tenantBuildingInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantBuildingInfos.singular')]));

            return redirect(route('tenantBuildingInfos.index'));
        }

        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tenantBuildingInfos.singular')]));

        return redirect(route('tenantBuildingInfos.index'));
    }

    /**
     * Remove the specified tenant_building_info from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tenantBuildingInfo = $this->tenantBuildingInfoRepository->find($id);

        if (empty($tenantBuildingInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantBuildingInfos.singular')]));

            return redirect(route('tenantBuildingInfos.index'));
        }

        $this->tenantBuildingInfoRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tenantBuildingInfos.singular')]));

        return redirect(route('tenantBuildingInfos.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('tenant_building_info'), $file);

    }

}
