<?php

namespace App\Http\Controllers;

use App\DataTables\tenant_family_infoDataTable;
use App\Http\Requests;
use App\Http\Requests\Createtenant_family_infoRequest;
use App\Http\Requests\Updatetenant_family_infoRequest;
use App\Repositories\tenant_family_infoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class tenant_family_infoController extends AppBaseController
{
    /** @var  tenant_family_infoRepository */
    private $tenantFamilyInfoRepository;

    public function __construct(tenant_family_infoRepository $tenantFamilyInfoRepo)
    {
        $this->tenantFamilyInfoRepository = $tenantFamilyInfoRepo;
    }

    /**
     * Display a listing of the tenant_family_info.
     *
     * @param tenant_family_infoDataTable $tenantFamilyInfoDataTable
     * @return Response
     */
    public function index(tenant_family_infoDataTable $tenantFamilyInfoDataTable)
    {
        return $tenantFamilyInfoDataTable->render('tenant_family_infos.index');
    }

    /**
     * Show the form for creating a new tenant_family_info.
     *
     * @return Response
     */
    public function create()
    {
        return view('tenant_family_infos.create');
    }

    /**
     * Store a newly created tenant_family_info in storage.
     *
     * @param Createtenant_family_infoRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_family_infoRequest $request)
    {
        $input = $request->all();

        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tenantFamilyInfos.singular')]));

        return redirect(route('tenantFamilyInfos.index'));
    }

    /**
     * Display the specified tenant_family_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->find($id);

        if (empty($tenantFamilyInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantFamilyInfos.singular')]));

            return redirect(route('tenantFamilyInfos.index'));
        }

        return view('tenant_family_infos.show')->with('tenantFamilyInfo', $tenantFamilyInfo);
    }

    /**
     * Show the form for editing the specified tenant_family_info.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->find($id);

        if (empty($tenantFamilyInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantFamilyInfos.singular')]));

            return redirect(route('tenantFamilyInfos.index'));
        }

        return view('tenant_family_infos.edit')->with('tenantFamilyInfo', $tenantFamilyInfo);
    }

    /**
     * Update the specified tenant_family_info in storage.
     *
     * @param  int              $id
     * @param Updatetenant_family_infoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_family_infoRequest $request)
    {
        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->find($id);

        if (empty($tenantFamilyInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantFamilyInfos.singular')]));

            return redirect(route('tenantFamilyInfos.index'));
        }

        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tenantFamilyInfos.singular')]));

        return redirect(route('tenantFamilyInfos.index'));
    }

    /**
     * Remove the specified tenant_family_info from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tenantFamilyInfo = $this->tenantFamilyInfoRepository->find($id);

        if (empty($tenantFamilyInfo)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantFamilyInfos.singular')]));

            return redirect(route('tenantFamilyInfos.index'));
        }

        $this->tenantFamilyInfoRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tenantFamilyInfos.singular')]));

        return redirect(route('tenantFamilyInfos.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('tenant_family_info'), $file);

    }

}
