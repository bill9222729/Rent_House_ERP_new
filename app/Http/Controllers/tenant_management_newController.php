<?php

namespace App\Http\Controllers;

use App\DataTables\tenant_management_newDataTable;
use App\Http\Requests;
use App\Http\Requests\Createtenant_management_newRequest;
use App\Http\Requests\Updatetenant_management_newRequest;
use App\Repositories\tenant_management_newRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class tenant_management_newController extends AppBaseController
{
    /** @var  tenant_management_newRepository */
    private $tenantManagementNewRepository;

    public function __construct(tenant_management_newRepository $tenantManagementNewRepo)
    {
        $this->tenantManagementNewRepository = $tenantManagementNewRepo;
    }

    /**
     * Display a listing of the tenant_management_new.
     *
     * @param tenant_management_newDataTable $tenantManagementNewDataTable
     * @return Response
     */
    public function index(tenant_management_newDataTable $tenantManagementNewDataTable)
    {
        return $tenantManagementNewDataTable->render('tenant_management_news.index');
    }

    /**
     * Show the form for creating a new tenant_management_new.
     *
     * @return Response
     */
    public function create()
    {
        return view('tenant_management_news.create');
    }

    /**
     * Store a newly created tenant_management_new in storage.
     *
     * @param Createtenant_management_newRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_management_newRequest $request)
    {
        $input = $request->all();

        $tenantManagementNew = $this->tenantManagementNewRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tenantManagementNews.singular')]));

        return redirect(route('tenantManagementNews.index'));
    }

    /**
     * Display the specified tenant_management_new.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tenantManagementNew = $this->tenantManagementNewRepository->find($id);

        if (empty($tenantManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagementNews.singular')]));

            return redirect(route('tenantManagementNews.index'));
        }

        return view('tenant_management_news.show')->with('tenantManagementNew', $tenantManagementNew);
    }

    /**
     * Show the form for editing the specified tenant_management_new.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tenantManagementNew = $this->tenantManagementNewRepository->find($id);

        if (empty($tenantManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagementNews.singular')]));

            return redirect(route('tenantManagementNews.index'));
        }

        return view('tenant_management_news.edit')->with('tenantManagementNew', $tenantManagementNew);
    }

    /**
     * Update the specified tenant_management_new in storage.
     *
     * @param  int              $id
     * @param Updatetenant_management_newRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_management_newRequest $request)
    {
        $tenantManagementNew = $this->tenantManagementNewRepository->find($id);

        if (empty($tenantManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagementNews.singular')]));

            return redirect(route('tenantManagementNews.index'));
        }

        $tenantManagementNew = $this->tenantManagementNewRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tenantManagementNews.singular')]));

        return redirect(route('tenantManagementNews.index'));
    }

    /**
     * Remove the specified tenant_management_new from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tenantManagementNew = $this->tenantManagementNewRepository->find($id);

        if (empty($tenantManagementNew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantManagementNews.singular')]));

            return redirect(route('tenantManagementNews.index'));
        }

        $this->tenantManagementNewRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tenantManagementNews.singular')]));

        return redirect(route('tenantManagementNews.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('tenant_management_new'), $file);

    }

}
