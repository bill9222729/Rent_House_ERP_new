<?php

namespace App\Http\Controllers;

use App\DataTables\tenant_minor_offspringDataTable;
use App\Http\Requests;
use App\Http\Requests\Createtenant_minor_offspringRequest;
use App\Http\Requests\Updatetenant_minor_offspringRequest;
use App\Repositories\tenant_minor_offspringRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class tenant_minor_offspringController extends AppBaseController
{
    /** @var  tenant_minor_offspringRepository */
    private $tenantMinorOffspringRepository;

    public function __construct(tenant_minor_offspringRepository $tenantMinorOffspringRepo)
    {
        $this->tenantMinorOffspringRepository = $tenantMinorOffspringRepo;
    }

    /**
     * Display a listing of the tenant_minor_offspring.
     *
     * @param tenant_minor_offspringDataTable $tenantMinorOffspringDataTable
     * @return Response
     */
    public function index(tenant_minor_offspringDataTable $tenantMinorOffspringDataTable)
    {
        return $tenantMinorOffspringDataTable->render('tenant_minor_offsprings.index');
    }

    /**
     * Show the form for creating a new tenant_minor_offspring.
     *
     * @return Response
     */
    public function create()
    {
        return view('tenant_minor_offsprings.create');
    }

    /**
     * Store a newly created tenant_minor_offspring in storage.
     *
     * @param Createtenant_minor_offspringRequest $request
     *
     * @return Response
     */
    public function store(Createtenant_minor_offspringRequest $request)
    {
        $input = $request->all();

        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tenantMinorOffsprings.singular')]));

        return redirect(route('tenantMinorOffsprings.index'));
    }

    /**
     * Display the specified tenant_minor_offspring.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->find($id);

        if (empty($tenantMinorOffspring)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantMinorOffsprings.singular')]));

            return redirect(route('tenantMinorOffsprings.index'));
        }

        return view('tenant_minor_offsprings.show')->with('tenantMinorOffspring', $tenantMinorOffspring);
    }

    /**
     * Show the form for editing the specified tenant_minor_offspring.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->find($id);

        if (empty($tenantMinorOffspring)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantMinorOffsprings.singular')]));

            return redirect(route('tenantMinorOffsprings.index'));
        }

        return view('tenant_minor_offsprings.edit')->with('tenantMinorOffspring', $tenantMinorOffspring);
    }

    /**
     * Update the specified tenant_minor_offspring in storage.
     *
     * @param  int              $id
     * @param Updatetenant_minor_offspringRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetenant_minor_offspringRequest $request)
    {
        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->find($id);

        if (empty($tenantMinorOffspring)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantMinorOffsprings.singular')]));

            return redirect(route('tenantMinorOffsprings.index'));
        }

        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tenantMinorOffsprings.singular')]));

        return redirect(route('tenantMinorOffsprings.index'));
    }

    /**
     * Remove the specified tenant_minor_offspring from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tenantMinorOffspring = $this->tenantMinorOffspringRepository->find($id);

        if (empty($tenantMinorOffspring)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tenantMinorOffsprings.singular')]));

            return redirect(route('tenantMinorOffsprings.index'));
        }

        $this->tenantMinorOffspringRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tenantMinorOffsprings.singular')]));

        return redirect(route('tenantMinorOffsprings.index'));
    }

    public function file_upload($file)
    {
        return Storage::put(storage_path('tenant_minor_offspring'), $file);

    }

}
