<?php

namespace App\Http\Middleware;

use App\Models\LogHistory;
use Closure;
use Illuminate\Support\Facades\Auth;

class LoggingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->method() == 'POST' || $request->method() == 'PATCH' || $request->method() == 'DELETE') {
            $log = new LogHistory();
            $log->url = $request->path();
            // $log->url = $request->fullUrl();
            $log->request_body = json_encode($request->getContent());
            $log->user = auth()->user()->name;
            $log->user_id = Auth::user()->id;
            $function_name = explode('@', \Route::currentRouteAction())[1];
            $action_zh_TW = '';
            switch ($function_name) {
                case 'store':
                    $action_zh_TW = '新增';
                break;
                case 'update':
                    $action_zh_TW = '修改';
                break;
                case 'destroy':
                    $action_zh_TW = '刪除';
                break;
            }

            $log->method = $action_zh_TW;
            // $log->method = $request->method();
            $log->save();
        }

        return $next($request);
    }
}
