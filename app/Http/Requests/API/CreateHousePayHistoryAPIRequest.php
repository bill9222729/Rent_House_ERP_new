<?php

namespace App\Http\Requests\API;

use App\Models\HousePayHistory;
use InfyOm\Generator\Request\APIRequest;

class CreateHousePayHistoryAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return HousePayHistory::$rules;
    }
}
