<?php

namespace App\Http\Requests\API;

use App\Models\FixHistory;
use InfyOm\Generator\Request\APIRequest;

class UpdateFixHistoryAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = FixHistory::$rules;
        
        return $rules;
    }
}
