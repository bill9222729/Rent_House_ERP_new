<?php

namespace App\Http\Requests\API;

use App\Models\located_lot;
use InfyOm\Generator\Request\APIRequest;

class Updatelocated_lotAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = located_lot::$rules;
        
        return $rules;
    }
}
