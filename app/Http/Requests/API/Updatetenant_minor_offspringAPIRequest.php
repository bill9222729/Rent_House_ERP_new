<?php

namespace App\Http\Requests\API;

use App\Models\tenant_minor_offspring;
use InfyOm\Generator\Request\APIRequest;

class Updatetenant_minor_offspringAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = tenant_minor_offspring::$rules;
        
        return $rules;
    }
}
