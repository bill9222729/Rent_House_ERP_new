<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BuildingManagementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'apply_date' => $this->apply_date,
            'charter' => $this->charter,
            'escrow' => $this->escrow,
            'request_form_type' => $this->request_form_type,
            'address_city' => $this->address_city,
            'address_cityarea' => $this->address_cityarea,
            'address_street' => $this->address_street,
            'address_ln' => $this->address_ln,
            'address_aly' => $this->address_aly,
            'address_num' => $this->address_num,
            'address_num_hyphen' => $this->address_num_hyphen,
            'located_city' => $this->located_city,
            'located_cityarea' => $this->located_cityarea,
            'located_build_address' => $this->located_build_address,
            'located_segment_num' => $this->located_segment_num,
            'located_small_lot' => $this->located_small_lot,
            'located_land_num' => $this->located_land_num,
            'located_build_num' => $this->located_build_num,
            'pattern' => $this->pattern,
            'pattern_remark' => $this->pattern_remark,
            'h_info_type' => $this->h_info_type,
            'h_info_age' => $this->h_info_age,
            'h_info_completion_date' => $this->h_info_completion_date,
            'h_info_fl' => $this->h_info_fl,
            'h_info_hyphen' => $this->h_info_hyphen,
            'h_info_suite' => $this->h_info_suite,
            'h_info_total_fl' => $this->h_info_total_fl,
            'h_info_pattern_room' => $this->h_info_pattern_room,
            'h_info_pattern_hall' => $this->h_info_pattern_hall,
            'h_info_pattern_bath' => $this->h_info_pattern_bath,
            'h_info_pattern_material' => $this->h_info_pattern_material,
            'h_info_warrant_ping_num' => $this->h_info_warrant_ping_num,
            'h_info_actual_ping_num' => $this->h_info_actual_ping_num,
            'h_info_usage' => $this->h_info_usage,
            'h_info_material' => $this->h_info_material,
            'landlord_expect_rent' => $this->landlord_expect_rent,
            'landlord_expect_deposit_month' => $this->landlord_expect_deposit_month,
            'landlord_expect_deposit' => $this->landlord_expect_deposit,
            'landlord_expect_bargain' => $this->landlord_expect_bargain,
            'manage_fee' => $this->manage_fee,
            'manage_fee_month' => $this->manage_fee_month,
            'manage_fee_ping' => $this->manage_fee_ping,
            'contain_fee_ele' => $this->contain_fee_ele,
            'contain_fee_water' => $this->contain_fee_water,
            'contain_fee_gas' => $this->contain_fee_gas,
            'contain_fee_clean' => $this->contain_fee_clean,
            'contain_fee_pay_tv' => $this->contain_fee_pay_tv,
            'contain_fee_net' => $this->contain_fee_net,
            'equipment_tv' => $this->equipment_tv,
            'equipment_refrigerator' => $this->equipment_refrigerator,
            'equipment_pay_tv' => $this->equipment_pay_tv,
            'equipment_air_conditioner' => $this->equipment_air_conditioner,
            'equipment_water_heater' => $this->equipment_water_heater,
            'equipment_net' => $this->equipment_net,
            'equipment_washer' => $this->equipment_washer,
            'equipment_gas' => $this->equipment_gas,
            'equipment_bed' => $this->equipment_bed,
            'equipment_wardrobe' => $this->equipment_wardrobe,
            'equipment_table' => $this->equipment_table,
            'equipment_chair' => $this->equipment_chair,
            'equipment_sofa' => $this->equipment_sofa,
            'equipment_other' => $this->equipment_other,
            'equipment_other_detail' => $this->equipment_other_detail,
            'can_Cook' => $this->can_Cook,
            'parking' => $this->parking,
            'Barrier_free_facility' => $this->Barrier_free_facility,
            'curfew' => $this->curfew,
            'curfew_management' => $this->curfew_management,
            'curfew_card' => $this->curfew_card,
            'curfew_other' => $this->curfew_other,
            'curfew_other_detail' => $this->curfew_other_detail,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'house_id' => $this->house_id,
            'landlord_id' => $this->landlord_id,
            'park_mang_fee' => $this->park_mang_fee,
            'rent' => $this->rent,
            'sublet' => $this->sublet,
        ];
    }
}
