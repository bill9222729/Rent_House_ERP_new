<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FixHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sales' => $this->sales,
            'pay_date' => $this->pay_date,
            'item_code' => $this->item_code,
            'item_address' => $this->item_address,
            'pay_year' => $this->pay_year,
            'fix_vendor' => $this->fix_vendor,
            'call_fix_date' => $this->call_fix_date,
            'item_owner' => $this->item_owner,
            'owner_phone' => $this->owner_phone,
            'client_name' => $this->client_name,
            'client_phone' => $this->client_phone,
            'fix_item' => $this->fix_item,
            'fix_source' => $this->fix_source,
            'check_date' => $this->check_date,
            'fix_date' => $this->fix_date,
            'fix_done' => $this->fix_done,
            'fix_record' => $this->fix_record,
            'fix_price' => $this->fix_price,
            'receipt' => $this->receipt,
            'final_price' => $this->final_price,
            'balance' => $this->balance,
            'owner_start_date' => $this->owner_start_date,
            'owner_end_date' => $this->owner_end_date,
            'change_start_date' => $this->change_start_date,
            'change_end_date' => $this->change_end_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at
        ];
    }
}
