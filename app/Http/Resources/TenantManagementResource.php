<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TenantManagementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'apply_date' => $this->apply_date,
            'charter' => $this->charter,
            'escrow' => $this->escrow,
            'lessee_name' => $this->lessee_name,
            'lessee_gender' => $this->lessee_gender,
            'lessee_birthday' => $this->lessee_birthday,
            'lessee_id_num' => $this->lessee_id_num,
            'lessee_hc_num' => $this->lessee_hc_num,
            'lessee_telephone_d' => $this->lessee_telephone_d,
            'lessee_telephone_n' => $this->lessee_telephone_n,
            'lessee_cellphone' => $this->lessee_cellphone,
            'lessee_email' => $this->lessee_email,
            'qualifications' => $this->qualifications,
            'hr_city' => $this->hr_city,
            'hr_cityarea' => $this->hr_cityarea,
            'hr_address' => $this->hr_address,
            'contact_city' => $this->contact_city,
            'contact_cityarea' => $this->contact_cityarea,
            'contact_address' => $this->contact_address,
            'receive_subsidy' => $this->receive_subsidy,
            'subsidy_rent' => $this->subsidy_rent,
            'subsidy_low_income' => $this->subsidy_low_income,
            'subsidy_disability' => $this->subsidy_disability,
            'subsidy_decree' => $this->subsidy_decree,
            'lease_nr_sr_city' => $this->lease_nr_sr_city,
            'legal_agent_num' => $this->legal_agent_num,
            'first_agent_name' => $this->first_agent_name,
            'first_agent_telephone' => $this->first_agent_telephone,
            'first_agent_cellphone' => $this->first_agent_cellphone,
            'first_agent_address' => $this->first_agent_address,
            'second_agent_name' => $this->second_agent_name,
            'second_agent_telephone' => $this->second_agent_telephone,
            'second_agent_cellphone' => $this->second_agent_cellphone,
            'second_agent_address' => $this->second_agent_address,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at
        ];
    }
}
