<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'account' => $this->account,
            't_date' => $this->t_date,
            't_time' => $this->t_time,
            'a_date' => $this->a_date,
            'detail' => $this->detail,
            'expense' => $this->expense,
            'income' => $this->income,
            'balance' => $this->balance,
            'remark_1' => $this->remark_1,
            'remark_2' => $this->remark_2,
            'remark_3' => $this->remark_3,
            'ticket_num' => $this->ticket_num,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at
        ];
    }
}
