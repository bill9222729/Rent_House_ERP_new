<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class change_historyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'building_id' => $this->building_id,
            'apply_date' => $this->apply_date,
            'change_date' => $this->change_date,
            'lessor_type' => $this->lessor_type,
            'name' => $this->name,
            'phone' => $this->phone,
            'reason' => $this->reason,
            'change_note' => $this->change_note,
            'edit_member_id' => $this->edit_member_id,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
