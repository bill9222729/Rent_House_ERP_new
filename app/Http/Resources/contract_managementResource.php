<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class contract_managementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'case_status' => $this->case_status,
            'signing_date' => $this->signing_date->format('Y-m-d'),
            'other_landlord_desc' => $this->other_landlord_desc,
            'lease_start_date' => $this->lease_start_date->format('Y-m-d'),
            'lease_end_date' => $this->lease_end_date->format('Y-m-d'),
            'lease_start_end_date' => $this->lease_start_date->format('Y-m-d') . '~' . $this->lease_end_date->format('Y-m-d'),
            'rent' => $this->rent,
            'rent_pay_type' => $this->rent_pay_type,
            'bank_name' => $this->bank_name,
            'bank_branch_code' => $this->bank_branch_code,
            'bank_owner_name' => $this->bank_owner_name,
            'bank_account' => $this->bank_account,
            'deposit_amount' => $this->deposit_amount,
            'contract_note' => $this->contract_note,
            'apply_man_id' => $this->apply_man_id,
            'remark' => $this->remark,
            'file_path' => $this->file_path,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'match_id' => $this->match_id,
            'tenant_case_no' => $this->tenant_case_no,
            'building_case_no' => $this->building_case_no,
            'burden' => $this->burden,
            'payment_date' => $this->payment_date,
            'has_tanants' => $this->has_tanants,
            'notary_fees' => $this->notary_fees,
            'notary_fees_share1' => $this->notary_fees_share1,
            'notary_fees_share2' => $this->notary_fees_share2,
            'matchmaking_fee' => $this->matchmaking_fee,
            'escrow_fee' => $this->escrow_fee,
            'management_fee' => $this->management_fee,
            'parking_fee' => $this->parking_fee,
            'guarantee_fee' => $this->guarantee_fee,
            'development_fee' => $this->development_fee,
            'rent_self_pay' => $this->rent_self_pay,
            'rent_government_pay' => $this->rent_government_pay,
            'created_at' => $this->created_at->format('Y-m-d'),
            'updated_at' => $this->updated_at->format('Y-m-d'),
            'stakeholders' => $this->stakeholders,
            'information_feed' => $this->information_feed,
            'information_feed_date' => $this->information_feed_date,
            'net_price_login' => $this->net_price_login,
            'data_status' => $this->data_status,
        ];
    }
}
