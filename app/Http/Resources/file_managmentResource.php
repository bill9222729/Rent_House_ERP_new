<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class file_managmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file_origin_name' => $this->file_origin_name,
            'file_server_name' => $this->file_server_name,
            'file_size' => $this->file_size,
            'file_source' => $this->file_source,
            'file_source_id' => $this->file_source_id,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
