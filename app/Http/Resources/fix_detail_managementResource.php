<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class fix_detail_managementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'report_date' => $this->report_date,
            'fix_id' => $this->fix_id,
            'repair_item' => $this->repair_item,
            'broken_reason' => $this->broken_reason,
            'repair_manufacturers' => $this->repair_manufacturers,
            'repair_records' => $this->repair_records,
            'meeting_date' => $this->meeting_date,
            'meeting_master' => $this->meeting_master,
            'fix_date' => $this->fix_date,
            'completion_date' => $this->completion_date,
            'please_amount' => $this->please_amount,
            'reply_amount' => $this->reply_amount,
            'monthly_application_month' => $this->monthly_application_month,
            'balance' => $this->balance,
            'note' => $this->note,
            'has_receipt' => $this->has_receipt,
            'no_reference' => $this->no_reference,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'building_id' => $this->building_id,
            'invoice_date' => $this->invoice_date,
        ];
    }
}
