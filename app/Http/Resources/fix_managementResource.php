<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class fix_managementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'match_id' => $this->match_id,
            'building_case_no' => $this->building_case_no,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
