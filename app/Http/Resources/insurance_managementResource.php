<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class insurance_managementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sales_id' => $this->sales_id,
            'match_id' => $this->match_id,
            'building_id' => $this->building_id,
            'insurance_premium' => $this->insurance_premium,
            'insurer' => $this->insurer,
            'insurance_start' => $this->insurance_start,
            'insurance_end' => $this->insurance_end,
            'monthly_application_month' => $this->monthly_application_month,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
