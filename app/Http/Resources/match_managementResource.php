<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class match_managementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'match_id' => $this->match_id,
            'building_case_id' => $this->building_case_id,
            'tenant_case_id' => $this->tenant_case_id,
            'lease_start_date' => $this->lease_start_date,
            'lease_end_date' => $this->lease_end_date,
            'charter_case_id' => $this->charter_case_id,
            'contract_type' => $this->contract_type,
            'contract_stop_date' => $this->contract_stop_date,
            'contract_stop_reason' => $this->contract_stop_reason,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
