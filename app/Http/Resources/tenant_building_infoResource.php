<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class tenant_building_infoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tenant_id' => $this->tenant_id,
            'name' => $this->name,
            'located_city' => $this->located_city,
            'located_Twn_spcode_area' => $this->located_Twn_spcode_area,
            'located_lot_area' => $this->located_lot_area,
            'located_landno' => $this->located_landno,
            'building_no' => $this->building_no,
            'building_total_square_meter_percent' => $this->building_total_square_meter_percent,
            'building_total_square_meter' => $this->building_total_square_meter,
            'addr_cntcode' => $this->addr_cntcode,
            'addr_twnspcode' => $this->addr_twnspcode,
            'addr_street_area' => $this->addr_street_area,
            'addr_lane' => $this->addr_lane,
            'addr_alley' => $this->addr_alley,
            'addr_no' => $this->addr_no,
            'is_household' => $this->is_household,
            'filers' => $this->filers,
            'last_modified_person' => $this->last_modified_person,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
