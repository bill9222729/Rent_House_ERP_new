<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class tenant_family_infoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tenant_id' => $this->tenant_id,
            'name' => $this->name,
            'is_foreigner' => $this->is_foreigner,
            'id_no' => $this->id_no,
            'appellation' => $this->appellation,
            'gender' => $this->gender,
            'condition_low_income_households' => $this->condition_low_income_households,
            'condition_low_and_middle_income_households' => $this->condition_low_and_middle_income_households,
            'condition_special_circumstances_families' => $this->condition_special_circumstances_families,
            'condition_over_sixty_five' => $this->condition_over_sixty_five,
            'condition_domestic_violence' => $this->condition_domestic_violence,
            'condition_disabled' => $this->condition_disabled,
            'condition_disabled_type' => $this->condition_disabled_type,
            'condition_disabled_level' => $this->condition_disabled_level,
            'condition_aids' => $this->condition_aids,
            'condition_aboriginal' => $this->condition_aboriginal,
            'condition_disaster_victims' => $this->condition_disaster_victims,
            'condition_vagabond' => $this->condition_vagabond,
            'condition_Applicant_naturalization' => $this->condition_Applicant_naturalization,
            'condition_Applicant_not_naturalization' => $this->condition_Applicant_not_naturalization,
            'condition_Applicant_not_naturalization_and_study' => $this->condition_Applicant_not_naturalization_and_study,
            'condition_police_officer' => $this->condition_police_officer,
            'filers' => $this->filers,
            'last_modified_person' => $this->last_modified_person,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
