<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class tenant_minor_offspringResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tenant_id' => $this->tenant_id,
            'name' => $this->name,
            'is_foreigner' => $this->is_foreigner,
            'appellation' => $this->appellation,
            'id_no' => $this->id_no,
            'birthday' => $this->birthday,
            'filers' => $this->filers,
            'last_modified_person' => $this->last_modified_person,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
