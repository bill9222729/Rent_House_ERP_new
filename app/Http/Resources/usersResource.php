<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class usersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'passport_english_name' => $this->passport_english_name,
            'english_name' => $this->english_name,
            'birthday' => $this->birthday,
            'id_no' => $this->id_no,
            'residence_city' => $this->residence_city,
            'residence_city_area' => $this->residence_city_area,
            'residenceresidence_addr_addr' => $this->residenceresidence_addr_addr,
            'mailing_city' => $this->mailing_city,
            'mailing_city_area' => $this->mailing_city_area,
            'mailing_addr' => $this->mailing_addr,
            'tel' => $this->tel,
            'cellphone' => $this->cellphone,
            'company_cellphone' => $this->company_cellphone,
            'bank_name' => $this->bank_name,
            'bank_branch_code' => $this->bank_branch_code,
            'bank_owner_name' => $this->bank_owner_name,
            'bank_account' => $this->bank_account,
            'email_verified_at' => $this->email_verified_at,
            'password' => $this->password,
            'role' => $this->role,
            'lock' => $this->lock,
            'is_sales' => $this->is_sales,
            'remember_token' => $this->remember_token,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'stoken' => $this->stoken,
            'deleted_at' => $this->deleted_at,
            'arrival_date' => $this->arrival_date,
        ];
    }
}
