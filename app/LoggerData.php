<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoggerData extends Model
{
    protected $table = 'logger_data';
}
