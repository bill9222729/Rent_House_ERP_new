<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DateTimeInterface;

/**
 * Class Bank
 * @package App\Models
 * @version May 20, 2021, 3:13 pm UTC
 *
 * @property string $bank_code
 * @property string $bank_account
 * @property string $tax
 * @property integer $price
 * @property string $user_number
 * @property string $company_stack_code
 * @property string $Creator
 * @property string $search
 * @property string $bank_noted
 */
class Bank extends Model
{
    use SoftDeletes;

    public $table = 'banks';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'bank_code',
        'bank_account',
        'tax',
        'price',
        'user_number',
        'company_stack_code',
        'Creator',
        'search',
        'bank_noted'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bank_code' => 'string',
        'bank_account' => 'string',
        'tax' => 'string',
        'price' => 'integer',
        'user_number' => 'string',
        'company_stack_code' => 'string',
        'Creator' => 'string',
        'search' => 'string',
        'bank_noted' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

    public function contract() {
        return $this->hasOne('App\Models\contract', 'landlord_item_number', 'user_number');
    }

}
