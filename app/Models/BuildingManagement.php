<?php

namespace App\Models;

use DateTimeInterface;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\contract;

/**
 * Class BuildingManagement
 * @package App\Models
 * @version April 20, 2021, 12:16 pm UTC
 *
 * @property string $file_json
 * @property string $sublent
 * @property string $rent
 * @property string $apply_date
 * @property boolean $charter
 * @property boolean $escrow
 * @property integer $request_form_type
 * @property string $address_city
 * @property string $address_cityarea
 * @property string $address_street
 * @property string $address_ln
 * @property string $address_aly
 * @property string $address_num
 * @property string $address_num_hyphen
 * @property string $located_city
 * @property string $located_cityarea
 * @property string $located_build_address
 * @property string $located_segment_num
 * @property string $located_small_lot
 * @property string $located_land_num
 * @property string $located_build_num
 * @property string $pattern
 * @property string $pattern_remark
 * @property string $h_info_type
 * @property string $h_info_age
 * @property string $h_info_completion_date
 * @property string $h_info_fl
 * @property string $h_info_hyphen
 * @property string $h_info_suite
 * @property string $h_info_total_fl
 * @property string $h_info_pattern_room
 * @property string $h_info_pattern_hall
 * @property string $h_info_pattern_bath
 * @property string $h_info_pattern_material
 * @property string $h_info_warrant_ping_num
 * @property string $h_info_actual_ping_num
 * @property string $h_info_usage
 * @property string $h_info_material
 * @property string $landlord_expect_rent
 * @property string $landlord_expect_deposit_month
 * @property string $landlord_expect_deposit
 * @property boolean $landlord_expect_bargain
 * @property boolean $manage_fee
 * @property string $manage_fee_month
 * @property string $manage_fee_ping
 * @property boolean $contain_fee_ele
 * @property boolean $contain_fee_water
 * @property boolean $contain_fee_gas
 * @property boolean $contain_fee_clean
 * @property boolean $contain_fee_pay_tv
 * @property boolean $contain_fee_net
 * @property boolean $equipment_tv
 * @property boolean $equipment_refrigerator
 * @property boolean $equipment_pay_tv
 * @property boolean $equipment_air_conditioner
 * @property boolean $equipment_water_heater
 * @property boolean $equipment_net
 * @property boolean $equipment_washer
 * @property boolean $equipment_gas
 * @property boolean $equipment_bed
 * @property boolean $equipment_wardrobe
 * @property boolean $equipment_table
 * @property boolean $equipment_chair
 * @property boolean $equipment_sofa
 * @property boolean $equipment_other
 * @property string $equipment_other_detail
 * @property boolean $can_Cook
 * @property boolean $parking
 * @property boolean $Barrier_free_facility
 * @property boolean $curfew
 * @property boolean $curfew_management
 * @property boolean $curfew_card
 * @property boolean $curfew_other
 * @property string $curfew_other_detail
 * @property string $house_id
 * @property string $landlord_id
 */
class BuildingManagement extends Model
{
    use SoftDeletes;

    public $table = 'building_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'file_json',
        'apply_date',
        'sublet',
        'rent',
        'notarization',
        'request_form_type',
        'address_city',
        'address_cityarea',
        'address_street',
        'address_ln',
        'address_aly',
        'address_num',
        'address_num_hyphen',
        'cellphone',
        'located_city',
        'located_cityarea',
        'located_build_address',
        'located_segment_num',
        'located_small_lot',
        'located_land_num',
        'located_build_num',
        'pattern',
        'pattern_remark',
        'h_info_type',
        'h_info_age',
        'h_info_completion_date',
        'h_info_fl',
        'h_info_hyphen',
        'h_info_suite',
        'h_info_total_fl',
        'h_info_pattern_room',
        'h_info_pattern_hall',
        'h_info_pattern_bath',
        'h_info_pattern_material',
        'h_info_warrant_ping_num',
        'h_info_actual_ping_num',
        'h_info_usage',
        'h_info_material',
        'landlord_expect_rent',
        'landlord_expect_deposit_month',
        'landlord_expect_deposit',
        'landlord_expect_bargain',
        'manage_fee',
        'manage_fee_month',
        'manage_fee_ping',
        'contain_fee_ele',
        'contain_fee_water',
        'contain_fee_gas',
        'contain_fee_clean',
        'contain_fee_pay_tv',
        'contain_fee_net',
        'equipment_tv',
        'equipment_refrigerator',
        'equipment_pay_tv',
        'equipment_air_conditioner',
        'equipment_water_heater',
        'equipment_net',
        'equipment_washer',
        'equipment_gas',
        'equipment_bed',
        'equipment_wardrobe',
        'equipment_table',
        'equipment_chair',
        'equipment_sofa',
        'equipment_other',
        'equipment_other_detail',
        'can_Cook',
        'parking',
        'Barrier_free_facility',
        'curfew',
        'curfew_management',
        'curfew_card',
        'curfew_other',
        'curfew_other_detail',
        'house_id',
        'landlord_id',
        'status',
        'num',
        'Lessor_name',
        'Lessor_gender',
        'Lessor_birthday',
        'Lessor_ID_num',
        'Lessor_phone',
        'residence_address_city',
        'residence_address_cityarea',
        'residence_address_street',
        'residence_address_ln',
        'residence_address_aly',
        'residence_address_num',
        'residence_address_num_hyphen',
        'collection_agent',
        'agent_ID_num',
        'agent_bank_name',
        'agent_branch_name',
        'landlord_bank_account',
        'remark_1',
        'remark_2',
        'located_build_address',
        'park_mang_fee',
        'files',
        'collection_agent_address',
        'collection_agent_phone',
        'sales_id',
        'v_account',
        'empty_house',
        'source',
        'fire_alert',
        'fire_alert_in_house',
        'notary',
        'n_name',
        'car_position',
        'p_code',
        'email',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'file_json' => 'string',
        'apply_date' => 'date',
        'sublet' => 'string',
        'rent' => 'string',
        'notarization' => 'string',
        'request_form_type' => 'integer',
        'address_city' => 'string',
        'address_cityarea' => 'string',
        'address_street' => 'string',
        'address_ln' => 'string',
        'address_aly' => 'string',
        'address_num' => 'string',
        'address_num_hyphen' => 'string',
        'cellphone' => 'nullable',
        'located_city' => 'string',
        'located_cityarea' => 'string',
        'located_build_address' => 'string',
        'located_segment_num' => 'string',
        'located_small_lot' => 'string',
        'located_land_num' => 'string',
        'located_build_num' => 'string',
        'pattern' => 'string',
        'pattern_remark' => 'string',
        'h_info_type' => 'string',
        'h_info_age' => 'string',
        'h_info_completion_date' => 'date',
        'h_info_fl' => 'string',
        'h_info_hyphen' => 'string',
        'h_info_suite' => 'string',
        'h_info_total_fl' => 'string',
        'h_info_pattern_room' => 'string',
        'h_info_pattern_hall' => 'string',
        'h_info_pattern_bath' => 'string',
        'h_info_pattern_material' => 'string',
        'h_info_warrant_ping_num' => 'string',
        'h_info_actual_ping_num' => 'string',
        'h_info_usage' => 'string',
        'h_info_material' => 'string',
        'landlord_expect_rent' => 'string',
        'landlord_expect_deposit_month' => 'string',
        'landlord_expect_deposit' => 'string',
        'landlord_expect_bargain' => 'boolean',
        'manage_fee' => 'boolean',
        'manage_fee_month' => 'string',
        'manage_fee_ping' => 'string',
        'contain_fee_ele' => 'boolean',
        'contain_fee_water' => 'boolean',
        'contain_fee_gas' => 'boolean',
        'contain_fee_clean' => 'boolean',
        'contain_fee_pay_tv' => 'boolean',
        'contain_fee_net' => 'boolean',
        'equipment_tv' => 'boolean',
        'equipment_refrigerator' => 'boolean',
        'equipment_pay_tv' => 'boolean',
        'equipment_air_conditioner' => 'boolean',
        'equipment_water_heater' => 'boolean',
        'equipment_net' => 'boolean',
        'equipment_washer' => 'boolean',
        'equipment_gas' => 'boolean',
        'equipment_bed' => 'boolean',
        'equipment_wardrobe' => 'boolean',
        'equipment_table' => 'boolean',
        'equipment_chair' => 'boolean',
        'equipment_sofa' => 'boolean',
        'equipment_other' => 'boolean',
        'equipment_other_detail' => 'string',
        'can_Cook' => 'boolean',
        'parking' => 'boolean',
        'Barrier_free_facility' => 'boolean',
        'curfew' => 'boolean',
        'curfew_management' => 'boolean',
        'curfew_card' => 'boolean',
        'curfew_other' => 'boolean',
        'curfew_other_detail' => 'string',
        'house_id' => 'string',
        'landlord_id' => 'string',
        'sales_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'apply_date' => 'nullable',
        'sublet' => 'nullable',
        'rent' => 'nullable',
        'notarization' => 'nullable',
        'request_form_type' => 'nullable',
        'address_city' => 'nullable|string|max:255',
        'address_cityarea' => 'nullable|string|max:255',
        'address_street' => 'nullable|string|max:255',
        'address_ln' => 'nullable|string|max:255',
        'address_aly' => 'nullable|string|max:255',
        'address_num' => 'nullable|string|max:255',
        'address_num_hyphen' => 'nullable|string|max:255',
        'cellphone' => 'nullable',
        'located_city' => 'nullable|string|max:255',
        'located_cityarea' => 'nullable|string|max:255',
        'located_build_address' => 'nullable|string|max:255',
        'located_segment_num' => 'nullable|string|max:255',
        'located_small_lot' => 'nullable|string|max:255',
        'located_land_num' => 'nullable|string|max:255',
        'located_build_num' => 'nullable|string|max:255',
        'pattern' => 'nullable|string|max:255',
        'pattern_remark' => 'nullable|string',
        'h_info_type' => 'nullable|string|max:255',
        'h_info_age' => 'nullable|string|max:255',
        'h_info_completion_date' => 'nullable',
        'h_info_fl' => 'nullable|string|max:255',
        'h_info_hyphen' => 'nullable|string|max:255',
        'h_info_suite' => 'nullable|string|max:255',
        'h_info_total_fl' => 'nullable|string|max:255',
        'h_info_pattern_room' => 'nullable|string|max:255',
        'h_info_pattern_hall' => 'nullable|string|max:255',
        'h_info_pattern_bath' => 'nullable|string|max:255',
        'h_info_pattern_material' => 'nullable|string|max:255',
        'h_info_warrant_ping_num' => 'nullable|string|max:255',
        'h_info_actual_ping_num' => 'nullable|string|max:255',
        'h_info_usage' => 'nullable|string|max:255',
        'h_info_material' => 'nullable|string|max:255',
        'landlord_expect_rent' => 'nullable|string|max:255',
        'landlord_expect_deposit_month' => 'nullable|string|max:255',
        'landlord_expect_deposit' => 'nullable|string|max:255',
        'landlord_expect_bargain' => 'nullable|boolean',
        'manage_fee' => 'nullable|boolean',
        'manage_fee_month' => 'nullable|string|max:255',
        'manage_fee_ping' => 'nullable|string|max:255',
        'contain_fee_ele' => 'nullable|boolean',
        'contain_fee_water' => 'nullable|boolean',
        'contain_fee_gas' => 'nullable|boolean',
        'contain_fee_clean' => 'nullable|boolean',
        'contain_fee_pay_tv' => 'nullable|boolean',
        'contain_fee_net' => 'nullable|boolean',
        'equipment_tv' => 'nullable|boolean',
        'equipment_refrigerator' => 'nullable|boolean',
        'equipment_pay_tv' => 'nullable|boolean',
        'equipment_air_conditioner' => 'nullable|boolean',
        'equipment_water_heater' => 'nullable|boolean',
        'equipment_net' => 'nullable|boolean',
        'equipment_washer' => 'nullable|boolean',
        'equipment_gas' => 'nullable|boolean',
        'equipment_bed' => 'nullable|boolean',
        'equipment_wardrobe' => 'nullable|boolean',
        'equipment_table' => 'nullable|boolean',
        'equipment_chair' => 'nullable|boolean',
        'equipment_sofa' => 'nullable|boolean',
        'equipment_other' => 'nullable|boolean',
        'equipment_other_detail' => 'nullable|string',
        'can_Cook' => 'nullable|boolean',
        'parking' => 'nullable|boolean',
        'Barrier_free_facility' => 'nullable|boolean',
        'curfew' => 'nullable|boolean',
        'curfew_management' => 'nullable|boolean',
        'curfew_card' => 'nullable|boolean',
        'curfew_other' => 'nullable|boolean',
        'curfew_other_detail' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'house_id' => 'nullable|string|max:255',
        'landlord_id' => 'nullable|string|max:255',
        'status' => 'nullable|string',
        'Lessor_name' => 'nullable|string',
        'Lessor_gender' => 'nullable',
        'Lessor_birthday' => 'nullable',
        'Lessor_ID_num' => 'nullable',
        'Lessor_phone' => 'nullable',
        'collection_agent' => 'nullable',
        'agent_ID_num' => 'nullable',
        'agent_bank_name' => 'nullable',
        'agent_branch_name' => 'nullable',
        'landlord_bank_account' => 'nullable',
        'residence_address_city' => 'nullable',
        'residence_address_cityarea' => 'nullable',
        'residence_address_street' => 'nullable'

    ];





    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d');
    }

    public function buildAddress()
    {
        return $this->located_city . $this->located_cityarea . $this->located_segment_num . $this->located_small_lot . $this->located_land_num . $this->located_build_num;
    }

    public function getAddressAttribute()
    {
        return $this->located_city . $this->located_cityarea . $this->located_segment_num . $this->located_small_lot . $this->located_land_num . $this->located_build_num;
    }

    public function contract()
    {
        return $this->hasOne('App\Models\contract', "landlord_match_number", "num");
    }
}
