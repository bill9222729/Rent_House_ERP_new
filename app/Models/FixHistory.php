<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class FixHistory
 * @package App\Models
 * @version May 20, 2021, 3:43 pm UTC
 *
 * @property string $sales
 * @property string $pay_date
 * @property string $item_code
 * @property string $item_address
 * @property integer $pay_year
 * @property string $call_fix_date
 * @property string $item_owner
 * @property string $owner_phone
 * @property string $client_name
 * @property string $client_phone
 * @property string $fix_item
 * @property string $fix_source
 * @property string $check_date
 * @property string $fix_date
 * @property string $fix_done
 * @property string $fix_record
 * @property integer $fix_price
 * @property integer $receipt
 * @property integer $final_price
 * @property integer $balance
 * @property string $owner_start_date
 * @property string $owner_end_date
 * @property string $change_start_date
 * @property string $change_end_date
 */
class FixHistory extends Model
{
    use SoftDeletes;

    public $table = 'fix_histories';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'sales',
        'pay_date',
        'item_code',
        'item_address',
        'pay_year',
        'call_fix_date',
        'item_owner',
        'owner_phone',
        'client_name',
        'client_phone',
        'fix_item',
        'fix_source',
        'check_date',
        'fix_date',
        'fix_done',
        'fix_record',
        'fix_price',
        'receipt',
        'final_price',
        'balance',
        'owner_start_date',
        'owner_end_date',
        'change_start_date',
        'change_end_date',
        'fix_vendor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sales' => 'string',
        'pay_date' => 'date',
        'item_code' => 'string',
        'item_address' => 'string',
        'pay_year' => 'integer',
        'call_fix_date' => 'string',
        'item_owner' => 'string',
        'owner_phone' => 'string',
        'client_name' => 'string',
        'client_phone' => 'string',
        'fix_item' => 'string',
        'fix_source' => 'string',
        'check_date' => 'string',
        'fix_date' => 'string',
        'fix_done' => 'date',
        'fix_record' => 'string',
        'fix_vendor' => 'string',
        'fix_price' => 'integer',
        'receipt' => 'integer',
        'final_price' => 'integer',
        'balance' => 'integer',
        'owner_start_date' => 'date',
        'owner_end_date' => 'date',
        'change_start_date' => 'date',
        'change_end_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

}
