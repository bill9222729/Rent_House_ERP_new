<?php

namespace App\Models;

use DateTimeInterface;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HousePayHistory
 * @package App\Models
 * @version August 4, 2021, 9:01 am UTC
 *
 * @property integer $contract_id
 * @property integer $month
 * @property integer $money
 * @property string $type
 * @property integer $freq
 */
class HousePayHistory extends Model
{
    use SoftDeletes;

    public $table = 'house_pay_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'contract_id',
        'month',
        'money',
        'type',
        'freq'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contract_id' => 'integer',
        'month' => 'integer',
        'money' => 'integer',
        'type' => 'string',
        'freq' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'contract_id' => 'required|integer',
        'month' => 'required|integer',
        'money' => 'required|integer',
        'type' => 'required|string|max:255',
        'freq' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
