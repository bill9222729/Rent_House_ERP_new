<?php

namespace App\Models;

use DateTimeInterface;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LandlordManagement
 * @package App\Models
 * @version April 20, 2021, 11:41 am UTC
 *
 * @property string $name
 * @property string $id_num
 * @property string $address
 * @property string $phone
 * @property string $birthday
 * @property string $bank_acc
 */
class LandlordManagement extends Model
{
    use SoftDeletes;

    public $table = 'landlord_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'id_num',
        'address',
        'phone',
        'birthday',
        'bank_acc'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'id_num' => 'string',
        'address' => 'string',
        'phone' => 'string',
        'birthday' => 'date',
        'landlord_match_number' => 'string',
        'landlord_existing_tenant' => 'string',
        'landlord_contract_date' => 'string',
        'landlord_contract_expiry_date' => 'string',
        'landlord_item_number' => 'string',
        'landlord_ministry_of_the_interior_number' => 'string',
        'landlord_types_of' => 'string',
        'bank_acc' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'id_num' => 'required|string|max:255',
        'address' => 'required|string|max:255',
        'phone' => 'required|string|max:255',
        'birthday' => 'required',
        'bank_acc' => 'required|string|max:255',
        'landlord_match_number' => 'nullable',
        'landlord_existing_tenant' => 'nullable',
        'landlord_contract_date' => 'nullable',
        'landlord_contract_expiry_date' => 'nullable',
        'landlord_item_number' => 'nullable',
        'landlord_ministry_of_the_interior_number' => 'nullable',
        'landlord_types_of' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];





    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

}
