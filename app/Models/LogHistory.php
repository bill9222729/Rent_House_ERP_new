<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class LogHistory
 * @package App\Models
 * @version June 6, 2021, 2:27 pm UTC
 *
 * @property string $url
 * @property string $request_body
 * @property string $method
 * @property integer $user_id
 */
class LogHistory extends Model
{
    use SoftDeletes;

    public $table = 'log_histories';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'url',
        'request_body',
        'user_id',
        'method',
        'user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'url' => 'string',
        'request_body' => 'string',
        'method' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

}
