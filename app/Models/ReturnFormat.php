<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;
use App\Models\business_management;


/**
 * Class ReturnFormat
 * @package App\Models
 * @version May 20, 2021, 4:00 pm UTC
 *
 * @property string $t_id
 * @property string $t_code
 * @property string $tax
 * @property string $bank_account
 * @property string $user_name
 * @property integer $t_price
 * @property string $t_status
 * @property string $name
 * @property string $item_address
 * @property integer $item_month_price
 * @property integer $item_price_support
 * @property integer $watch_price
 * @property integer $parking_price
 * @property integer $fix_price
 * @property integer $t_cost
 * @property integer $other_price
 * @property integer $total_price
 * @property string $note
 */
class ReturnFormat extends Model
{
    use SoftDeletes;

    public $table = 'return_formats';


    protected $dates = ['deleted_at'];



    public $fillable = [
        't_id',
        't_code',
        'tax',
        'bank_account',
        'user_name',
        't_price',
        't_status',
        'name',
        'item_address',
        'item_month_price',
        'item_price_support',
        'watch_price',
        'parking_price',
        'fix_price',
        't_cost',
        'other_price',
        'total_price',
        'note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        't_id' => 'string',
        't_code' => 'string',
        'tax' => 'string',
        'bank_account' => 'string',
        'user_name' => 'string',
        't_price' => 'integer',
        't_status' => 'string',
        'name' => 'string',
        'item_address' => 'string',
        'item_month_price' => 'integer',
        'item_price_support' => 'integer',
        'watch_price' => 'integer',
        'parking_price' => 'integer',
        'fix_price' => 'integer',
        't_cost' => 'integer',
        'other_price' => 'integer',
        'total_price' => 'integer',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

    public function building_management()
    {
        return $this->belongsTo(business_management::class, 'user_name', 'num');
    }

}
