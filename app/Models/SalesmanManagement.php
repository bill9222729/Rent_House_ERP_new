<?php

namespace App\Models;

use DateTimeInterface;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SalesmanManagement
 * @package App\Models
 * @version April 20, 2021, 11:40 am UTC
 *
 * @property string $name
 * @property string $id_num
 * @property string $address
 * @property string $phone
 * @property string $birthday
 * @property string $bank_acc
 */
class SalesmanManagement extends Model
{
    use SoftDeletes;

    public $table = 'salesman_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'id_num',
        'address',
        'phone',
        'birthday',
        'bank_acc'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'id_num' => 'string',
        'address' => 'string',
        'phone' => 'string',
        'birthday' => 'date',
        'bank_acc' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'id_num' => 'required|string|max:255',
        'address' => 'required|string|max:255',
        'phone' => 'required|string|max:255',
        'birthday' => 'required',
        'bank_acc' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];





    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

}
