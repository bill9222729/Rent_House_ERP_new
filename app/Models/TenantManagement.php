<?php

namespace App\Models;

use DateTimeInterface;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TenantManagement
 * @package App\Models
 * @version April 20, 2021, 11:42 am UTC
 *
 * @property string $file_json
 * @property string $sublent
 * @property string $rent
 * @property string $apply_date
 * @property boolean $charter
 * @property boolean $escrow
 * @property string $lessee_name
 * @property string $lessee_gender
 * @property string $lessee_birthday
 * @property string $lessee_id_num
 * @property string $lessee_hc_num
 * @property string $lessee_telephone_d
 * @property string $lessee_telephone_n
 * @property string $lessee_cellphone
 * @property string $lessee_email
 * @property string $qualifications
 * @property string $hr_city
 * @property string $hr_cityarea
 * @property string $hr_address
 * @property string $contact_city
 * @property string $contact_cityarea
 * @property string $contact_address
 * @property boolean $receive_subsidy
 * @property string $subsidy_rent
 * @property string $subsidy_low_income
 * @property string $subsidy_disability
 * @property string $subsidy_decree
 * @property string $lease_nr_sr_city
 * @property string $legal_agent_num
 * @property string $first_agent_name
 * @property string $first_agent_telephone
 * @property string $first_agent_cellphone
 * @property string $first_agent_address
 * @property string $second_agent_name
 * @property string $second_agent_telephone
 * @property string $second_agent_cellphone
 * @property string $second_agent_address
 */
class TenantManagement extends Model
{
    use SoftDeletes;

    public $table = 'tenant_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $appends = array('qualificationszh');


    public $fillable = [
        'file_json',
        'apply_date',
        'sublet',
        'rent',
        'lessee_name',
        'lessee_gender',
        'lessee_birthday',
        'lessee_id_num',
        'lessee_hc_num',
        'lessee_telephone_d',
        'lessee_telephone_n',
        'lessee_cellphone',
        'lessee_email',
        'qualifications',
        'hr_city',
        'hr_cityarea',
        'hr_address',
        'contact_city',
        'contact_cityarea',
        'contact_address',
        'receive_subsidy',
        'subsidy_rent',
        'subsidy_low_income',
        'subsidy_disability',
        'subsidy_decree',
        'lease_nr_sr_city',
        'legal_agent_num',
        'first_agent_name',
        'first_agent_telephone',
        'first_agent_cellphone',
        'first_agent_address',
        'second_agent_name',
        'second_agent_telephone',
        'second_agent_cellphone',
        'second_agent_address',
        'user_file',
        'tenant_case_number',
        'tenant_ministry_of_the_interior_number',
        'status',
        'remark_1',
        'remark_2',
        'ref_code',
        'bank_name',
        'bank_code',
        'bank_account',
        'files',
        'pay_date',
        'sales',
        'weak_item',
        'res',
        'tp',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'apply_date' => 'date',
        'file_json' => 'string',
        'sublet' => 'string',
        'rent' => 'string',
        'lessee_name' => 'string',
        'lessee_gender' => 'string',
        'lessee_birthday' => 'date',
        'lessee_id_num' => 'string',
        'lessee_hc_num' => 'string',
        'lessee_telephone_d' => 'string',
        'lessee_telephone_n' => 'string',
        'lessee_cellphone' => 'string',
        'lessee_email' => 'string',
        'qualifications' => 'string',
        'hr_city' => 'string',
        'hr_cityarea' => 'string',
        'hr_address' => 'string',
        'contact_city' => 'string',
        'contact_cityarea' => 'string',
        'contact_address' => 'string',
        'receive_subsidy' => 'boolean',
        'subsidy_rent' => 'string',
        'subsidy_low_income' => 'string',
        'subsidy_disability' => 'string',
        'subsidy_decree' => 'string',
        'lease_nr_sr_city' => 'string',
        'legal_agent_num' => 'string',
        'first_agent_name' => 'string',
        'first_agent_telephone' => 'string',
        'first_agent_cellphone' => 'string',
        'first_agent_address' => 'string',
        'second_agent_name' => 'string',
        'second_agent_telephone' => 'string',
        'second_agent_cellphone' => 'string',
        'tenant_case_number' => 'string',
        'tenant_ministry_of_the_interior_number' => 'string',
        'second_agent_address' => 'string',
        'ref_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'apply_date' => 'nullable',
        'sublet' => 'nullable',
        'rent' => 'nullable',
        'lessee_name' => 'nullable|string|max:255',
        'lessee_gender' => 'nullable|string|max:255',
        'lessee_birthday' => 'nullable',
        'lessee_id_num' => 'nullable|string|max:255',
        'lessee_hc_num' => 'nullable|string|max:255',
        'lessee_telephone_d' => 'nullable|string|max:255',
        'lessee_telephone_n' => 'nullable|string|max:255',
        'lessee_cellphone' => 'nullable|string|max:255',
        'lessee_email' => 'nullable|string|max:255',
        'qualifications' => 'nullable|string|max:255',
        'hr_city' => 'nullable|string|max:255',
        'hr_cityarea' => 'nullable|string|max:255',
        'hr_address' => 'nullable|string|max:255',
        'contact_city' => 'nullable|string|max:255',
        'contact_cityarea' => 'nullable|string|max:255',
        'contact_address' => 'nullable|string|max:255',
        'receive_subsidy' => 'nullable|boolean',
        'subsidy_rent' => 'nullable|string|max:255',
        'subsidy_low_income' => 'nullable|string|max:255',
        'subsidy_disability' => 'nullable|string|max:255',
        'subsidy_decree' => 'nullable|string|max:255',
        'lease_nr_sr_city' => 'nullable|string|max:255',
        'legal_agent_num' => 'nullable|string|max:255',
        'first_agent_name' => 'nullable|string|max:255',
        'first_agent_telephone' => 'nullable|string|max:255',
        'first_agent_cellphone' => 'nullable|string|max:255',
        'first_agent_address' => 'nullable|string|max:255',
        'second_agent_name' => 'nullable|string|max:255',
        'second_agent_telephone' => 'nullable|string|max:255',
        'second_agent_cellphone' => 'nullable|string|max:255',
        'second_agent_address' => 'nullable|string|max:255',
        'user_file' => 'nullable',
        'tenant_case_number' => 'nullable',
        'tenant_ministry_of_the_interior_number' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'status' => 'nullable',
    ];

    public function getQualificationszhAttribute()
    {
        $qual = $this->qualifications;
        if ($qual == 1) return '一般戶';
        if ($qual == 2) return '第一類弱勢戶';
        if ($qual == 3) return '第二類弱勢戶';
        return '未知';
    }


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

}
