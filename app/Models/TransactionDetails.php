<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class TransactionDetails
 * @package App\Models
 * @version May 26, 2021, 8:14 am UTC
 *
 * @property string $account
 * @property string $t_date
 * @property string|\Carbon\Carbon $t_time
 * @property string $a_date
 * @property string $detail
 * @property integer $expense
 * @property integer $income
 * @property integer $balance
 * @property string $remark_1
 * @property string $remark_2
 * @property string $remark_3
 * @property string $ticket_num
 */
class TransactionDetails extends Model
{
    use SoftDeletes;

    public $table = 'transaction_details';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'account',
        't_date',
        't_time',
        'a_date',
        'detail',
        'expense',
        'income',
        'balance',
        'remark_1',
        'remark_2',
        'remark_3',
        'ticket_num'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'account' => 'string',
        't_date' => 'date',
        't_time' => 'datetime',
        'a_date' => 'date',
        'detail' => 'string',
        'expense' => 'integer',
        'income' => 'integer',
        'balance' => 'integer',
        'remark_1' => 'string',
        'remark_2' => 'string',
        'remark_3' => 'string',
        'ticket_num' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'account' => 'nullable|string|max:255',
        't_date' => 'nullable',
        't_time' => 'nullable',
        'a_date' => 'nullable',
        'detail' => 'nullable|string|max:255',
        'expense' => 'nullable|integer',
        'income' => 'nullable|integer',
        'balance' => 'nullable|integer',
        'remark_1' => 'nullable|string|max:255',
        'remark_2' => 'nullable|string|max:255',
        'remark_3' => 'nullable|string|max:255',
        'ticket_num' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];




    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

}
