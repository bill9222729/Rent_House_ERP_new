<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class building_management_new
 * @package App\Models
 * @version February 25, 2022, 4:02 am UTC
 *
 * @property string $building_address_city
 * @property string $building_address_city_area
 * @property string $building_address_street
 * @property string $building_address_ln
 * @property string $building_address_aly
 * @property string $building_address_num
 * @property string $building_address_num_hyphen
 * @property string $building_address_floor
 * @property string $building_address_floor_sub
 * @property string $building_address_room_num
 * @property string $building_address_building_floor
 * @property string $building_address_building_desc
 * @property string $building_located_city
 * @property string $building_located_city_area
 * @property string $building_located_lot
 * @property string $building_located_land_num
 * @property string $building_num
 * @property string $building_rent_type
 * @property string $building_pattren
 * @property string $building_type
 * @property string $building_age
 * @property string $building_complete_date
 * @property string $building_rooms
 * @property string $building_livingrooms
 * @property string $building_bathrooms
 * @property string $building_compartment_material
 * @property string $building_total_square_meter
 * @property string $building_use_square_meter
 * @property string $building_materials
 * @property string $building_rent
 * @property string $building_deposit_amount
 * @property string $building_is_including_electricity_fees
 * @property string $building_is_cooking
 * @property string $building_is_including_water_fees
 * @property string $building_is_including_parking_space
 * @property string $building_is_including_accessible_equipment
 * @property string $building_is_including_natural_gas
 * @property string $building_is_including_cable
 * @property string $building_is_including_internet
 * @property string $building_is_including_cleaning_fee
 * @property string $building_offer_tv
 * @property string $building_offer_refrigerator
 * @property string $building_offer_cable
 * @property string $building_offer_air_conditioner
 * @property string $building_offer_geyser
 * @property string $building_offer_internet
 * @property string $building_offer_washing_machine
 * @property string $building_offer_natural_gas
 * @property string $building_offer_bed
 * @property string $building_offer_wardrobe
 * @property string $building_offer_desk
 * @property string $building_offer_chair
 * @property string $building_offer_sofa
 * @property string $building_offer_other
 * @property string $building_use
 * @property string $apply_date
 * @property string $case_no
 * @property integer $case_type_chartering
 * @property integer $case_type_escrow
 * @property string $case_state
 * @property string $name
 * @property integer $gender
 * @property string $birthday
 * @property integer $is_foreigner
 * @property string $id_no
 * @property string $passport_code
 * @property string $house_no
 * @property string $tel_day
 * @property string $tel_night
 * @property string $cellphone
 * @property string $email
 * @property string $residence_city
 * @property string $residence_city_area
 * @property string $residence_addr
 * @property string $mailing_city
 * @property string $mailing_city_area
 * @property string $mailing_addr
 * @property integer $legal_agent_num
 * @property string $single_proxy_reason
 * @property string $first_legal_agent_name
 * @property string $first_legal_agent_tel
 * @property string $first_legal_agent_phone
 * @property string $first_legal_agent_addr
 * @property string $second_legal_agent_name
 * @property string $second_legal_agent_tel
 * @property string $second_legal_agent_phone
 * @property string $second_legal_agent_addr
 * @property string $bank_name
 * @property string $bank_branch_code
 * @property string $bank_owner_name
 * @property string $bank_account
 */
class building_management_new extends Model
{
    use SoftDeletes;

    public $table = 'building_management_new';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'building_address_city',
        'building_address_city_area',
        'building_address_street',
        'building_address_ln',
        'building_address_aly',
        'building_address_num',
        'building_address_num_hyphen',
        'building_address_floor',
        'building_address_floor_sub',
        'building_address_room_num',
        'building_address_building_floor',
        'building_address_building_desc',
        'building_located_city',
        'building_located_city_area',
        'building_located_lot',
        'building_located_land_num',
        'building_num',
        'building_rent_type',
        'building_pattren',
        'building_type',
        'building_age',
        'building_complete_date',
        'building_rooms',
        'building_livingrooms',
        'building_bathrooms',
        'building_compartment_material',
        'building_total_square_meter',
        'building_use_square_meter',
        'building_materials',
        'building_rent',
        'building_deposit_amount',
        'building_is_including_electricity_fees',
        'building_is_cooking',
        'building_is_including_water_fees',
        'building_is_including_parking_space',
        'building_is_including_accessible_equipment',
        'building_is_including_natural_gas',
        'building_is_including_cable',
        'building_is_including_internet',
        'building_is_including_cleaning_fee',
        'building_offer_tv',
        'building_offer_refrigerator',
        'building_offer_cable',
        'building_offer_air_conditioner',
        'building_offer_geyser',
        'building_offer_internet',
        'building_offer_washing_machine',
        'building_offer_natural_gas',
        'building_offer_bed',
        'building_offer_wardrobe',
        'building_offer_desk',
        'building_offer_chair',
        'building_offer_sofa',
        'building_offer_other',
        'building_use',
        'apply_date',
        'case_no',
        'case_type_chartering',
        'case_type_escrow',
        'case_state',
        'name',
        'gender',
        'birthday',
        'is_foreigner',
        'id_no',
        'passport_code',
        'house_no',
        'tel_day',
        'tel_night',
        'cellphone',
        'email',
        'residence_city',
        'residence_city_area',
        'residence_addr',
        'mailing_city',
        'mailing_city_area',
        'mailing_addr',
        'legal_agent_num',
        'single_proxy_reason',
        'first_legal_agent_name',
        'first_legal_agent_tel',
        'first_legal_agent_phone',
        'first_legal_agent_addr',
        'second_legal_agent_name',
        'second_legal_agent_tel',
        'second_legal_agent_phone',
        'second_legal_agent_addr',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'building_deposit_amount_month',
        'can_bargain',
        'has_management_fee',
        'management_fee_month',
        'management_fee_square_meter',
        'building_offer_other_desc',
        'stakeholders',
        'building_source',
        'representative',
        'tax_number',
        'agent_name',
        'agent_tel',
        'agent_cellphone',
        'agent_addr',
        'lessor_type',
        'government_no',
        'sales_id',
        'condition_disabled_type',
        'condition_disabled_level',
        'entrusted_lease_start_date',
        'entrusted_lease_end_date',
        'entrusted_management_start_date',
        'entrusted_management_end_date',
        'virtual_bank_name',
        'virtual_bank_branch_code',
        'virtual_bank_owner_name',
        'virtual_bank_account',
        'note',
        'data_status',
        'creator',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'building_address_city' => 'string',
        'building_address_city_area' => 'string',
        'building_address_street' => 'string',
        'building_address_ln' => 'string',
        'building_address_aly' => 'string',
        'building_address_num' => 'string',
        'building_address_num_hyphen' => 'string',
        'building_address_floor' => 'string',
        'building_address_floor_sub' => 'string',
        'building_address_room_num' => 'string',
        'building_address_building_floor' => 'string',
        'building_address_building_desc' => 'string',
        'building_located_city' => 'string',
        'building_located_city_area' => 'string',
        'building_located_lot' => 'string',
        'building_located_land_num' => 'string',
        'building_num' => 'string',
        'building_rent_type' => 'string',
        'building_pattren' => 'string',
        'building_type' => 'string',
        'building_age' => 'string',
        'building_complete_date' => 'string',
        'building_rooms' => 'string',
        'building_livingrooms' => 'string',
        'building_bathrooms' => 'string',
        'building_compartment_material' => 'string',
        'building_total_square_meter' => 'string',
        'building_use_square_meter' => 'string',
        'building_materials' => 'string',
        'building_rent' => 'string',
        'building_deposit_amount' => 'string',
        'building_is_including_electricity_fees' => 'string',
        'building_is_cooking' => 'string',
        'building_is_including_water_fees' => 'string',
        'building_is_including_parking_space' => 'string',
        'building_is_including_accessible_equipment' => 'string',
        'building_is_including_natural_gas' => 'string',
        'building_is_including_cable' => 'string',
        'building_is_including_internet' => 'string',
        'building_is_including_cleaning_fee' => 'string',
        'building_offer_tv' => 'string',
        'building_offer_refrigerator' => 'string',
        'building_offer_cable' => 'string',
        'building_offer_air_conditioner' => 'string',
        'building_offer_geyser' => 'string',
        'building_offer_internet' => 'string',
        'building_offer_washing_machine' => 'string',
        'building_offer_natural_gas' => 'string',
        'building_offer_bed' => 'string',
        'building_offer_wardrobe' => 'string',
        'building_offer_desk' => 'string',
        'building_offer_chair' => 'string',
        'building_offer_sofa' => 'string',
        'building_offer_other' => 'string',
        'building_use' => 'string',
        'apply_date' => 'date',
        'case_no' => 'string',
        'case_type_chartering' => 'integer',
        'case_type_escrow' => 'integer',
        'case_state' => 'string',
        'name' => 'string',
        'gender' => 'integer',
        'birthday' => 'date',
        'is_foreigner' => 'integer',
        'id_no' => 'string',
        'passport_code' => 'string',
        'house_no' => 'string',
        'tel_day' => 'string',
        'tel_night' => 'string',
        'cellphone' => 'string',
        'email' => 'string',
        'residence_city' => 'string',
        'residence_city_area' => 'string',
        'residence_addr' => 'string',
        'mailing_city' => 'string',
        'mailing_city_area' => 'string',
        'mailing_addr' => 'string',
        'legal_agent_num' => 'integer',
        'single_proxy_reason' => 'string',
        'first_legal_agent_name' => 'string',
        'first_legal_agent_tel' => 'string',
        'first_legal_agent_phone' => 'string',
        'first_legal_agent_addr' => 'string',
        'second_legal_agent_name' => 'string',
        'second_legal_agent_tel' => 'string',
        'second_legal_agent_phone' => 'string',
        'second_legal_agent_addr' => 'string',
        'bank_name' => 'string',
        'bank_branch_code' => 'string',
        'bank_owner_name' => 'string',
        'bank_account' => 'string',
        'building_deposit_amount_month' => 'integer',
        'can_bargain' => 'integer',
        'has_management_fee' => 'integer',
        'management_fee_month' => 'integer',
        'management_fee_square_meter' => 'integer',
        'building_offer_other_desc' => 'string',
        'stakeholders' => 'string',
        'building_source' => 'string',
        'representative' => 'string',
        'tax_number' => 'string',
        'agent_name' => 'string',
        'agent_tel' => 'string',
        'agent_cellphone' => 'string',
        'agent_addr' => 'string',
        'lessor_type' => 'string',
        'government_no' => 'string',
        'sales_id' => 'integer',
        'condition_disabled_type' => 'integer',
        'condition_disabled_level' => 'string',
        'entrusted_lease_start_date' => 'date',
        'entrusted_lease_end_date' => 'date',
        'entrusted_management_start_date' => 'date',
        'entrusted_management_end_date' => 'date',
        'virtual_bank_name' => 'string',
        'virtual_bank_branch_code' => 'integer',
        'virtual_bank_owner_name' => 'string',
        'virtual_bank_account' => 'string',
        'note' => 'string',
        'data_status' => 'string',
        'creator' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'building_address_city' => 'nullable|string|max:255',
        'building_address_city_area' => 'nullable|string|max:255',
        'building_address_street' => 'nullable|string|max:255',
        'building_address_ln' => 'nullable|string|max:255',
        'building_address_aly' => 'nullable|string|max:255',
        'building_address_num' => 'nullable|string|max:255',
        'building_address_num_hyphen' => 'nullable|string|max:255',
        'building_address_floor' => 'nullable|string|max:255',
        'building_address_floor_sub' => 'nullable|string|max:255',
        'building_address_room_num' => 'nullable|string|max:255',
        'building_address_building_floor' => 'nullable|string|max:255',
        'building_address_building_desc' => 'nullable|string|max:255',
        'building_located_city' => 'nullable|string|max:255',
        'building_located_city_area' => 'nullable|string|max:255',
        'building_located_lot' => 'nullable|string|max:255',
        'building_located_land_num' => 'nullable|string|max:255',
        'building_num' => 'nullable|string|max:255',
        'building_rent_type' => 'nullable|string|max:255',
        'building_pattren' => 'nullable|string|max:255',
        'building_type' => 'nullable|string|max:255',
        'building_age' => 'nullable|string|max:255',
        'building_complete_date' => 'nullable|string|max:255',
        'building_rooms' => 'nullable|string|max:255',
        'building_livingrooms' => 'nullable|string|max:255',
        'building_bathrooms' => 'nullable|string|max:255',
        'building_compartment_material' => 'nullable|string|max:255',
        'building_total_square_meter' => 'nullable|string|max:255',
        'building_use_square_meter' => 'nullable|string|max:255',
        'building_materials' => 'nullable|string|max:255',
        'building_rent' => 'nullable|string|max:255',
        'building_deposit_amount' => 'nullable|string|max:255',
        'building_is_including_electricity_fees' => 'nullable|string|max:255',
        'building_is_cooking' => 'nullable|string|max:255',
        'building_is_including_water_fees' => 'nullable|string|max:255',
        'building_is_including_parking_space' => 'nullable|string|max:255',
        'building_is_including_accessible_equipment' => 'nullable|string|max:255',
        'building_is_including_natural_gas' => 'nullable|string|max:255',
        'building_is_including_cable' => 'nullable|string|max:255',
        'building_is_including_internet' => 'nullable|string|max:255',
        'building_is_including_cleaning_fee' => 'nullable|string|max:255',
        'building_offer_tv' => 'nullable|string|max:255',
        'building_offer_refrigerator' => 'nullable|string|max:255',
        'building_offer_cable' => 'nullable|string|max:255',
        'building_offer_air_conditioner' => 'nullable|string|max:255',
        'building_offer_geyser' => 'nullable|string|max:255',
        'building_offer_internet' => 'nullable|string|max:255',
        'building_offer_washing_machine' => 'nullable|string|max:255',
        'building_offer_natural_gas' => 'nullable|string|max:255',
        'building_offer_bed' => 'nullable|string|max:255',
        'building_offer_wardrobe' => 'nullable|string|max:255',
        'building_offer_desk' => 'nullable|string|max:255',
        'building_offer_chair' => 'nullable|string|max:255',
        'building_offer_sofa' => 'nullable|string|max:255',
        'building_offer_other' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'building_use' => 'nullable|string|max:191',
        'apply_date' => 'nullable',
        'case_no' => 'nullable|string',
        'case_type_chartering' => 'nullable|integer',
        'case_type_escrow' => 'nullable|integer',
        'case_state' => 'nullable|string',
        'name' => 'nullable|string',
        'gender' => 'nullable|integer',
        'birthday' => 'nullable',
        'is_foreigner' => 'nullable|integer',
        'id_no' => 'nullable|string',
        'passport_code' => 'nullable|string',
        'house_no' => 'nullable|string',
        'tel_day' => 'nullable|string',
        'tel_night' => 'nullable|string',
        'cellphone' => 'nullable|string',
        'email' => 'nullable|string',
        'residence_city' => 'nullable|string',
        'residence_city_area' => 'nullable|string',
        'residence_addr' => 'nullable|string',
        'mailing_city' => 'nullable|string',
        'mailing_city_area' => 'nullable|string',
        'mailing_addr' => 'nullable|string',
        'legal_agent_num' => 'nullable|integer',
        'single_proxy_reason' => 'nullable|string',
        'first_legal_agent_name' => 'nullable|string',
        'first_legal_agent_tel' => 'nullable|string',
        'first_legal_agent_phone' => 'nullable|string',
        'first_legal_agent_addr' => 'nullable|string',
        'second_legal_agent_name' => 'nullable|string',
        'second_legal_agent_tel' => 'nullable|string',
        'second_legal_agent_phone' => 'nullable|string',
        'second_legal_agent_addr' => 'nullable|string',
        'bank_name' => 'nullable|string',
        'bank_branch_code' => 'nullable|string',
        'bank_owner_name' => 'nullable|string',
        'bank_account' => 'nullable|string',
        'building_deposit_amount_month' => 'nullable|integer',
        'can_bargain' => 'nullable|integer',
        'has_management_fee' => 'nullable|integer',
        'management_fee_month' => 'nullable|integer',
        'management_fee_square_meter' => 'nullable|integer',
        'building_offer_other_desc' => 'nullable|string',
        'stakeholders' => 'nullable|string',
        'building_source' => 'nullable|string',
        'representative' => 'nullable|string',
        'tax_number' => 'nullable|string',
        'agent_name' => 'nullable|string',
        'agent_tel' => 'nullable|string',
        'agent_cellphone' => 'nullable|string',
        'agent_addr' => 'nullable|string',
        'lessor_type' => 'nullable|string',
        'government_no' => 'nullable|string',
        'sales_id' => 'nullable|integer',
        'condition_disabled_type' => 'nullable|integer',
        'condition_disabled_level' => 'nullable|string',
        'entrusted_lease_start_date' => 'nullable|date',
        'entrusted_lease_end_date' => 'nullable|date',
        'entrusted_management_start_date' => 'nullable|date',
        'entrusted_management_end_date' => 'nullable|date',
        'virtual_bank_name' => 'nullable|string',
        'virtual_bank_branch_code' => 'nullable|integer',
        'virtual_bank_owner_name' => 'nullable|string',
        'virtual_bank_account' => 'nullable|string',
        'note' => 'nullable|string',
        'data_status' => 'nullable|string',
        'creator' => 'nullable|integer',
    ];




    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
