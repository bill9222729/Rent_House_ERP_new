<?php

namespace App\Models;


use DateTimeInterface;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class business_management
 * @package App\Models
 * @version April 26, 2021, 1:08 pm UTC
 *
 * @property string $english_name
 * @property string $due_date
 * @property string $r_address
 * @property string $cellphone
 * @property string $labor
 * @property string $f_name
 * @property string $f_title
 * @property string $f_identity
 * @property string $f_birthday
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $id_number
 * @property string $birthday
 * @property string $bank_account
 * @property string $management_fee_set
 * @property integer $escrow_quota
 * @property integer $box_quota
 */
class business_management extends Model
{

    use SoftDeletes;

    public $table = 'business_managements';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'english_name',
        'due_date',
        'r_address',
        'cellphone',
        'labor',
        'f_name',
        'f_title',
        'f_identity',
        'f_birthday',
        'name',
        'address',
        'residence_address',
        'phone',
        'id_number',
        'birthday',
        'bank_account',
        'management_fee_set',
        'match_fee_set',
        'escrow_quota',
        'box_quota',
        'take_office_date',
        'cellphone_1',
        'cellphone_2',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'english_name' => 'string',
        'due_date' => 'string',
        'r_address' => 'string',
        'cellphone' => 'string',
        'labor' => 'string',
        'f_name' => 'string',
        'f_title' => 'string',
        'f_identity' => 'string',
        'f_birthday' => 'string',
        'id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'residence_address' => 'string',
        'phone' => 'string',
        'id_number' => 'string',
        'birthday' => 'date',
        'bank_account' => 'string',
        'management_fee_set' => 'string',
        'escrow_quota' => 'integer',
        'box_quota' => 'integer',
        'take_office_date' => 'date',
        'cellphone_1' => 'string',
        'cellphone_2' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];




    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }
}
