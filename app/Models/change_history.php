<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;


/**
 * Class change_history
 * @package App\Models
 * @version April 8, 2022, 2:04 pm CST
 *
 * @property string $building_id
 * @property string $apply_date
 * @property string $change_date
 * @property string $lessor_type
 * @property string $name
 * @property string $phone
 * @property string $reason
 * @property string $change_note
 * @property integer $edit_member_id
 */
class change_history extends Model
{
    use SoftDeletes;

    public $table = 'change_history';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'building_id',
        'apply_date',
        'change_date',
        'lessor_type',
        'name',
        'phone',
        'reason',
        'change_note',
        'edit_member_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'building_id' => 'string',
        'apply_date' => 'date',
        'change_date' => 'date',
        'lessor_type' => 'string',
        'name' => 'string',
        'phone' => 'string',
        'reason' => 'string',
        'change_note' => 'string',
        'edit_member_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'building_id' => 'nullable|string|max:191',
        'apply_date' => 'nullable',
        'change_date' => 'nullable',
        'lessor_type' => 'nullable|string|max:191',
        'name' => 'nullable|string|max:191',
        'phone' => 'nullable|string|max:191',
        'reason' => 'nullable|string|max:191',
        'change_note' => 'nullable|string|max:191',
        'edit_member_id' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
