<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class contract
 * @package App\Models
 * @version April 29, 2021, 1:27 pm UTC
 *
 * @property string $contract_number_of_periods
 * @property string $contract_category
 * @property string $contract_business_sign_back
 * @property string $contract_match_date
 * @property string $landlord_match_number
 * @property string $landlord_existing_tenant
 * @property string $landlord_contract_date
 * @property string $landlord_contract_expiry_date
 * @property string $landlord_item_number
 * @property string $landlord_ministry_of_the_interior_number
 * @property string $landlord_types_of
 * @property string $tenant_case_number
 * @property string $tenant_ministry_of_the_interior_number
 * @property string $rent_evaluation_form_market_rent
 * @property string $rent_evaluation_form_assess_rent
 * @property string $actual_contract_rent_to_landlord
 * @property string $actual_contract_tenant_pays_rent
 * @property string $actual_contract_rent_subsidy
 * @property string $actual_contract_deposit
 * @property string $to_the_landlord_rent_day
 * @property string $package_escrow_fee_month_minute
 * @property string $package_escrow_fee_amount
 * @property string $package_escrow_fee_number_of_periods
 * @property string $rent_subsidy_month_minute
 * @property string $rent_subsidy_amount
 * @property string $rent_subsidy_number_of_periods
 * @property string $development_matching_fees_month
 * @property string $development_matching_fees_amount
 * @property string $notary_fees_notarization_date
 * @property string $notary_fees_month
 * @property string $notary_fees_amount
 * @property string $repair_cost_month_minute
 * @property string $repair_cost_amount
 * @property string $insurance_month_minute
 * @property string $insurance_amount
 * @property string $insurance_period_three_year
 * @property string $insurance_period_three_start_and_end_date
 * @property string $remark_1
 * @property string $remark_2
 * @property string $file_upload
 * @property string $b_id
 * @property string $tb_id
 * @property string $notary
 * @property int $develop_price
 * @property int $bmanagement_price
 * @property int $search_price
 * @property int $management_price
 */
class contract extends Model
{
    use SoftDeletes;

    public $table = 'contracts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'contract_number_of_periods',
        'contract_category',
        'contract_business_sign_back',
        'contract_match_date',
        'landlord_match_number',
        'landlord_existing_tenant',
        'landlord_contract_date',
        'landlord_contract_expiry_date',
        'landlord_item_number',
        'landlord_ministry_of_the_interior_number',
        'landlord_types_of',
        'tenant_case_number',
        'tenant_ministry_of_the_interior_number',
        'rent_evaluation_form_market_rent',
        'rent_evaluation_form_assess_rent',
        'actual_contract_rent_to_landlord',
        'actual_contract_tenant_pays_rent',
        'actual_contract_rent_subsidy',
        'actual_contract_deposit',
        'to_the_landlord_rent_day',
        'package_escrow_fee_month_minute',
        'package_escrow_fee_amount',
        'package_escrow_fee_number_of_periods',
        'rent_subsidy_month_minute',
        'rent_subsidy_amount',
        'rent_subsidy_number_of_periods',
        'development_matching_fees_month',
        'development_matching_fees_amount',
        'notary_fees_notarization_date',
        'notary_fees_month',
        'notary_fees_amount',
        'repair_cost_month_minute',
        'repair_cost_amount',
        'insurance_month_minute',
        'insurance_amount',
        'insurance_period_three_year',
        'insurance_period_three_start_and_end_date',
        'remark_1',
        'remark_2',
        'file_upload',
        'park_mang_fee',
        'b_id',
        'tb_id',
        'develop_price',
        'bmanagement_price',
        'search_price',
        'management_price',
        'notary',
        'real_code',
        'info_code',
        'real_date',
        'info_date',
        'recive_bank',
        'recive_bank_code',
        'recive_bank_account',
        'recive_id',
        'bank_note',
        'insurance_company'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contract_number_of_periods' => 'string',
        'contract_category' => 'string',
        'contract_business_sign_back' => 'string',
        'contract_match_date' => 'string',
        'landlord_match_number' => 'string',
        'landlord_existing_tenant' => 'string',
        'landlord_contract_date' => 'string',
        'landlord_contract_expiry_date' => 'string',
        'landlord_item_number' => 'string',
        'landlord_ministry_of_the_interior_number' => 'string',
        'landlord_types_of' => 'string',
        'tenant_case_number' => 'string',
        'tenant_ministry_of_the_interior_number' => 'string',
        'rent_evaluation_form_market_rent' => 'string',
        'rent_evaluation_form_assess_rent' => 'string',
        'actual_contract_rent_to_landlord' => 'string',
        'actual_contract_tenant_pays_rent' => 'string',
        'actual_contract_rent_subsidy' => 'string',
        'actual_contract_deposit' => 'string',
        'to_the_landlord_rent_day' => 'string',
        'package_escrow_fee_month_minute' => 'string',
        'package_escrow_fee_amount' => 'string',
        'package_escrow_fee_number_of_periods' => 'string',
        'rent_subsidy_month_minute' => 'string',
        'rent_subsidy_amount' => 'string',
        'rent_subsidy_number_of_periods' => 'string',
        'development_matching_fees_month' => 'string',
        'development_matching_fees_amount' => 'string',
        'notary_fees_notarization_date' => 'string',
        'notary_fees_month' => 'string',
        'notary_fees_amount' => 'string',
        'repair_cost_month_minute' => 'string',
        'repair_cost_amount' => 'string',
        'insurance_month_minute' => 'string',
        'insurance_amount' => 'string',
        'insurance_period_three_year' => 'string',
        'insurance_period_three_start_and_end_date' => 'string',
        'remark_1' => 'string',
        'remark_2' => 'string',
        'file_upload' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'contract_number_of_periods' => 'required',
        // 'contract_category' => 'required',
        // 'contract_business_sign_back' => 'required',
        // 'contract_match_date' => 'required',
        // 'landlord_match_number' => 'required',
        // 'landlord_existing_tenant' => 'required',
        // 'landlord_contract_date' => 'required',
        // 'landlord_contract_expiry_date' => 'required',
        // 'landlord_item_number' => 'required',
        // 'landlord_ministry_of_the_interior_number' => 'required',
        // 'landlord_types_of' => 'required',
        // 'tenant_case_number' => 'required',
        // 'tenant_ministry_of_the_interior_number' => 'required',
        // 'rent_evaluation_form_market_rent' => 'required',
        // 'rent_evaluation_form_assess_rent' => 'required',
        // 'actual_contract_rent_to_landlord' => 'required',
        // 'actual_contract_tenant_pays_rent' => 'required',
        // 'actual_contract_rent_subsidy' => 'required',
        // 'actual_contract_deposit' => 'required',
        // 'to_the_landlord_rent_day' => 'required',
        // 'package_escrow_fee_month_minute' => 'required',
        // 'package_escrow_fee_amount' => 'required',
        // 'package_escrow_fee_number_of_periods' => 'required',
        // 'rent_subsidy_month_minute' => 'required',
        // 'rent_subsidy_amount' => 'required',
        // 'rent_subsidy_number_of_periods' => 'required',
        // 'development_matching_fees_month' => 'required',
        // 'development_matching_fees_amount' => 'required',
        // 'notary_fees_notarization_date' => 'required',
        // 'notary_fees_month' => 'required',
        // 'notary_fees_amount' => 'required',
        // 'repair_cost_month_minute' => 'required',
        // 'repair_cost_amount' => 'required',
        // 'park_mang_fee' => 'required',
        // 'insurance_month_minute' => 'required',
        // 'insurance_amount' => 'required',

        // 'insurance_period_three_year' => 'required',
        // 'insurance_period_three_start_and_end_date' => 'required',

        // 'remark_1' => 'required',
        // 'remark_2' => 'required',
    ];




    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return ((int)$date->format('Y') - 1911) . $date->format('-m-d H:i:s');
    }

    public function tenantManagement()
    {
        return $this->hasOne(TenantManagement::class, 'tenant_case_number', 'tenant_case_number');
    }

    public function buildingManagement()
    {
        return $this->hasOne(BuildingManagement::class, 'num', 'landlord_item_number');
    }

    public function businessManagement()
    {
        return $this->hasOne(business_management::class, 'name', 'contract_business_sign_back');
    }

    public function fix_histories()
    {
        return $this->hasMany(FixHistory::class, 'item_code', 'tenant_case_number');
    }
}
