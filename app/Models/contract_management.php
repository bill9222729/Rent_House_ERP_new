<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class contract_management
 * @package App\Models
 * @version March 2, 2022, 9:15 am UTC
 *
 * @property string $case_status
 * @property string $signing_date
 * @property string $other_landlord_desc
 * @property string $lease_start_date
 * @property string $lease_end_date
 * @property integer $rent
 * @property string $rent_pay_type
 * @property string $bank_name
 * @property string $bank_branch_code
 * @property string $bank_owner_name
 * @property string $bank_account
 * @property integer $deposit_amount
 * @property string $contract_note
 * @property integer $apply_man_id
 * @property string $remark
 * @property string $file_path
 */
class contract_management extends Model
{
    use SoftDeletes;

    public $table = 'contract_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'case_status',
        'signing_date',
        'other_landlord_desc',
        'lease_start_date',
        'lease_end_date',
        'rent',
        'rent_pay_type',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'deposit_amount',
        'contract_note',
        'apply_man_id',
        'remark',
        'file_path',
        'match_id',
        'tenant_case_no',
        'building_case_no',
        'burden',
        'payment_date',
        'has_tanants',
        'notary_fees',
        'notary_fees_share1',
        'notary_fees_share2',
        'matchmaking_fee',
        'escrow_fee',
        'management_fee',
        'parking_fee',
        'rent_self_pay',
        'rent_government_pay',
        'development_fee',
        'guarantee_fee',
        'stakeholders',
        'information_feed',
        'information_feed_date',
        'net_price_login',
        'data_status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'case_status' => 'string',
        'signing_date' => 'date',
        'other_landlord_desc' => 'string',
        'lease_start_date' => 'date',
        'lease_end_date' => 'date',
        'rent' => 'integer',
        'rent_pay_type' => 'string',
        'bank_name' => 'string',
        'bank_branch_code' => 'string',
        'bank_owner_name' => 'string',
        'bank_account' => 'string',
        'deposit_amount' => 'integer',
        'contract_note' => 'string',
        'apply_man_id' => 'integer',
        'remark' => 'string',
        'file_path' => 'string',
        'match_id' => 'string',
        'tenant_case_no' => 'string',
        'building_case_no' => 'string',
        'burden' => 'string',
        'payment_date' => 'string',
        'has_tanants' => 'integer',
        'notary_fees' => 'integer',
        'notary_fees_share1' => 'integer',
        'notary_fees_share2' => 'integer',
        'matchmaking_fee' => 'integer',
        'escrow_fee' => 'integer',
        'management_fee' => 'integer',
        'parking_fee' => 'integer',
        'development_fee' => 'integer',
        'guarantee_fee' => 'integer',
        'rent_self_pay' => 'integer',
        'rent_government_pay' => 'integer',
        'stakeholders' => 'string',
        'information_feed' => 'string',
        'information_feed_date' => 'date',
        'net_price_login' => 'string',
        'data_status' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'case_status' => 'nullable|string|max:191',
        'signing_date' => 'nullable',
        'other_landlord_desc' => 'nullable|string|max:191',
        'lease_start_date' => 'nullable',
        'lease_end_date' => 'nullable',
        'rent' => 'nullable|integer',
        'rent_pay_type' => 'nullable|string|max:191',
        'bank_name' => 'nullable|string|max:191',
        'bank_branch_code' => 'nullable|string|max:191',
        'bank_owner_name' => 'nullable|string|max:191',
        'bank_account' => 'nullable|string|max:191',
        'deposit_amount' => 'nullable|integer',
        'contract_note' => 'nullable|string|max:191',
        'apply_man_id' => 'nullable|integer',
        'remark' => 'nullable|string|max:191',
        'file_path' => 'nullable|string|max:191',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'match_id' => 'nullable|string|max:191',
        'tenant_case_no' => 'nullable|string|max:191',
        'building_case_no' => 'nullable|string|max:191',
        'burden' => 'nullable|string|max:191',
        'payment_date' => 'nullable|string|max:191',
        'has_tanants' => 'nullable|integer',
        'notary_fees' => 'nullable|integer',
        'notary_fees_share1' => 'nullable|integer',
        'notary_fees_share2' => 'nullable|integer',
        'matchmaking_fee' => 'nullable|integer',
        'escrow_fee' => 'nullable|integer',
        'management_fee' => 'nullable|integer',
        'parking_fee' => 'nullable|integer',
        'guarantee_fee' => 'nullable|integer',
        'development_fee' => 'nullable|integer',
        'rent_self_pay' => 'nullable|integer',
        'rent_government_pay' => 'nullable|integer',
        'stakeholders' => 'nullable|string',
        'information_feed' => 'nullable|string',
        'information_feed_date' => 'nullable',
        'net_price_login' => 'nullable|string',
        'data_status' => 'nullable|string',
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d');
    }
}
