<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class contracts_new
 * @package App\Models
 * @version February 24, 2022, 7:30 am UTC
 *
 * @property string $building_case_id
 * @property string $tenant_case_id
 * @property string $lease_start_date
 * @property string $lease_end_date
 * @property string $charter_case_id
 * @property string $contract_type
 * @property string $match_id
 */
class contracts_new extends Model
{
    use SoftDeletes;

    public $table = 'contracts_new';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'building_case_id',
        'tenant_case_id',
        'lease_start_date',
        'lease_end_date',
        'charter_case_id',
        'contract_type',
        'match_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'building_case_id' => 'string',
        'tenant_case_id' => 'string',
        'lease_start_date' => 'date',
        'lease_end_date' => 'date',
        'charter_case_id' => 'string',
        'contract_type' => 'string',
        'match_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'building_case_id' => 'nullable|string|max:191',
        'tenant_case_id' => 'nullable|string|max:191',
        'lease_start_date' => 'nullable',
        'lease_end_date' => 'nullable',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'charter_case_id' => 'nullable|string|max:191',
        'contract_type' => 'nullable|string|max:191',
        'match_id' => 'nullable|string|max:191'
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
