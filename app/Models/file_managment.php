<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class file_managment
 * @package App\Models
 * @version April 6, 2022, 10:37 am CST
 *
 * @property string $file_origin_name
 * @property string $file_server_name
 * @property integer $file_size
 * @property string $file_source
 * @property integer $file_source_id
 */
class file_managment extends Model
{
    use SoftDeletes;

    public $table = 'file_managment';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'file_origin_name',
        'file_server_name',
        'file_size',
        'file_source',
        'file_source_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'file_origin_name' => 'string',
        'file_server_name' => 'string',
        'file_size' => 'integer',
        'file_source' => 'string',
        'file_source_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'file_origin_name' => 'nullable|string|max:191',
        'file_server_name' => 'nullable|string|max:191',
        'file_size' => 'nullable|integer',
        'file_source' => 'nullable|string|max:191',
        'file_source_id' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
