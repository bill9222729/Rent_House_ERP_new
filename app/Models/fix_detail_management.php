<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class fix_detail_management
 * @package App\Models
 * @version May 16, 2022, 9:48 am CST
 *
 * @property string $report_date
 * @property integer $fix_id
 * @property string $repair_item
 * @property string $repair_manufacturers
 * @property string $repair_records
 * @property string $meeting_date
 * @property string $meeting_master
 * @property string $fix_date
 * @property string $completion_date
 * @property integer $please_amount
 * @property integer $reply_amount
 * @property string $monthly_application_month
 * @property integer $balance
 * @property string $note
 * @property integer $no_reference
 */
class fix_detail_management extends Model
{
    use SoftDeletes;

    public $table = 'fix_detail_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'report_date',
        'fix_id',
        'repair_item',
        'repair_manufacturers',
        'repair_records',
        'meeting_date',
        'meeting_master',
        'fix_date',
        'completion_date',
        'please_amount',
        'reply_amount',
        'monthly_application_month',
        'balance',
        'note',
        'no_reference',
        'building_id',
        'has_receipt',
        'broken_reason',
        'invoice_date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'report_date' => 'date',
        'fix_id' => 'integer',
        'repair_item' => 'string',
        'repair_manufacturers' => 'string',
        'repair_records' => 'string',
        'meeting_date' => 'date',
        'meeting_master' => 'string',
        'fix_date' => 'date',
        'completion_date' => 'date',
        'please_amount' => 'integer',
        'reply_amount' => 'integer',
        'monthly_application_month' => 'date',
        'balance' => 'integer',
        'note' => 'string',
        'no_reference' => 'integer',
        'building_id' => 'integer',
        'broken_reason' => 'string',
        'has_receipt' => 'integer',
        'invoice_date' => 'date',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'report_date' => 'nullable',
        'fix_id' => 'nullable|integer',
        'repair_item' => 'nullable|string|max:191',
        'repair_manufacturers' => 'nullable|string|max:191',
        'repair_records' => 'nullable|string|max:191',
        'meeting_date' => 'nullable',
        'meeting_master' => 'nullable|string|max:191',
        'fix_date' => 'nullable',
        'completion_date' => 'nullable',
        'please_amount' => 'nullable|integer',
        'reply_amount' => 'nullable|integer',
        'monthly_application_month' => 'nullable',
        'balance' => 'nullable|integer',
        'note' => 'nullable|string|max:191',
        'no_reference' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'building_id' => 'nullable',
        'broken_reason' => 'nullable|string|max:191',
        'has_receipt' => 'nullable|integer',
        'invoice_date' => 'nullable',
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
