<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class insurance_management
 * @package App\Models
 * @version April 29, 2022, 4:35 pm CST
 *
 * @property integer $sales_id
 * @property string $match_id
 * @property string $building_id
 * @property integer $insurance_premium
 * @property string $insurer
 * @property string $insurance_start
 * @property string $insurance_end
 * @property string $monthly_application_month
 */
class insurance_management extends Model
{
    use SoftDeletes;

    public $table = 'insurance_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'sales_id',
        'match_id',
        'building_id',
        'insurance_premium',
        'insurer',
        'insurance_start',
        'insurance_end',
        'monthly_application_month'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sales_id' => 'integer',
        'match_id' => 'string',
        'building_id' => 'string',
        'insurance_premium' => 'integer',
        'insurer' => 'string',
        'insurance_start' => 'date',
        'insurance_end' => 'date',
        'monthly_application_month' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'sales_id' => 'nullable|integer',
        'match_id' => 'nullable|string|max:191',
        'building_id' => 'nullable|string|max:191',
        'insurance_premium' => 'nullable|integer',
        'insurer' => 'nullable|string|max:191',
        'insurance_start' => 'nullable',
        'insurance_end' => 'nullable',
        'monthly_application_month' => 'nullable',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
