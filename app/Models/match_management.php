<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class match_management
 * @package App\Models
 * @version March 2, 2022, 5:35 am UTC
 *
 * @property string $match_id
 * @property string $building_case_id
 * @property string $tenant_case_id
 * @property string $lease_start_date
 * @property string $lease_end_date
 * @property string $charter_case_id
 * @property string $contract_type
 */
class match_management extends Model
{
    use SoftDeletes;

    public $table = 'match_management';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'match_id',
        'building_case_id',
        'tenant_case_id',
        'lease_start_date',
        'lease_end_date',
        'charter_case_id',
        'contract_type',
        'contract_stop_date',
        'contract_stop_reason',
        'data_status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'match_id' => 'string',
        'building_case_id' => 'string',
        'tenant_case_id' => 'string',
        'lease_start_date' => 'date',
        'lease_end_date' => 'date',
        'charter_case_id' => 'string',
        'contract_type' => 'string',
        'contract_stop_date' => 'date',
        'contract_stop_reason' => 'string',
        'data_status' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'match_id' => 'nullable|string|max:191',
        'building_case_id' => 'nullable|string|max:191',
        'tenant_case_id' => 'nullable|string|max:191',
        'lease_start_date' => 'nullable',
        'lease_end_date' => 'nullable',
        'charter_case_id' => 'nullable|string|max:191',
        'contract_type' => 'nullable|string|max:191',
        'contract_stop_date' => 'nullable',
        'contract_stop_reason' => 'nullable|string|max:191',
        'data_status' => 'nullable|string|max:191',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d');
    }
}
