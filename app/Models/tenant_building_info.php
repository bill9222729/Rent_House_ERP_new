<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class tenant_building_info
 * @package App\Models
 * @version April 13, 2022, 2:12 pm CST
 *
 * @property integer $tenant_id
 * @property string $name
 * @property string $located_city
 * @property string $located_Twn_spcode_area
 * @property string $located_lot_area
 * @property string $located_landno
 * @property string $building_no
 * @property string $building_total_square_meter_percent
 * @property string $building_total_square_meter
 * @property string $addr_cntcode
 * @property string $addr_twnspcode
 * @property string $addr_street_area
 * @property string $addr_lane
 * @property string $addr_alley
 * @property string $addr_no
 * @property integer $is_household
 * @property integer $filers
 * @property integer $last_modified_person
 */
class tenant_building_info extends Model
{
    use SoftDeletes;

    public $table = 'tenant_building_info';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'tenant_id',
        'name',
        'located_city',
        'located_Twn_spcode_area',
        'located_lot_area',
        'located_landno',
        'building_no',
        'building_total_square_meter_percent',
        'building_total_square_meter',
        'addr_cntcode',
        'addr_twnspcode',
        'addr_street_area',
        'addr_lane',
        'addr_alley',
        'addr_no',
        'is_household',
        'filers',
        'last_modified_person'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tenant_id' => 'integer',
        'name' => 'string',
        'located_city' => 'string',
        'located_Twn_spcode_area' => 'string',
        'located_lot_area' => 'string',
        'located_landno' => 'string',
        'building_no' => 'string',
        'building_total_square_meter_percent' => 'string',
        'building_total_square_meter' => 'string',
        'addr_cntcode' => 'string',
        'addr_twnspcode' => 'string',
        'addr_street_area' => 'string',
        'addr_lane' => 'string',
        'addr_alley' => 'string',
        'addr_no' => 'string',
        'is_household' => 'integer',
        'filers' => 'integer',
        'last_modified_person' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tenant_id' => 'nullable|integer',
        'name' => 'nullable|string|max:191',
        'located_city' => 'nullable|string|max:191',
        'located_Twn_spcode_area' => 'nullable|string|max:191',
        'located_lot_area' => 'nullable|string|max:191',
        'located_landno' => 'nullable|string|max:191',
        'building_no' => 'nullable|string|max:191',
        'building_total_square_meter_percent' => 'nullable|string|max:191',
        'building_total_square_meter' => 'nullable|string|max:191',
        'addr_cntcode' => 'nullable|string|max:191',
        'addr_twnspcode' => 'nullable|string|max:191',
        'addr_street_area' => 'nullable|string|max:191',
        'addr_lane' => 'nullable|string|max:191',
        'addr_alley' => 'nullable|string|max:191',
        'addr_no' => 'nullable|string|max:191',
        'is_household' => 'nullable|integer',
        'filers' => 'nullable|integer',
        'last_modified_person' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    

    use DateTimeInterface;

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
