<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class tenant_family_info
 * @package App\Models
 * @version April 13, 2022, 11:46 am CST
 *
 * @property integer $tenant_id
 * @property string $name
 * @property integer $is_foreigner
 * @property string $id_no
 * @property string $appellation
 * @property string $gender
 * @property integer $condition_low_income_households
 * @property integer $condition_low_and_middle_income_households
 * @property integer $condition_special_circumstances_families
 * @property integer $condition_over_sixty_five
 * @property integer $condition_domestic_violence
 * @property integer $condition_disabled
 * @property integer $condition_disabled_type
 * @property string $condition_disabled_level
 * @property integer $condition_aids
 * @property integer $condition_aboriginal
 * @property integer $condition_disaster_victims
 * @property integer $condition_vagabond
 * @property integer $condition_Applicant_naturalization
 * @property integer $condition_Applicant_not_naturalization
 * @property integer $condition_Applicant_not_naturalization_and_study
 * @property integer $condition_police_officer
 * @property integer $filers
 * @property integer $last_modified_person
 */
class tenant_family_info extends Model
{
    use SoftDeletes;

    public $table = 'tenant_family_info';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'tenant_id',
        'name',
        'is_foreigner',
        'id_no',
        'appellation',
        'gender',
        'condition_low_income_households',
        'condition_low_and_middle_income_households',
        'condition_special_circumstances_families',
        'condition_over_sixty_five',
        'condition_domestic_violence',
        'condition_disabled',
        'condition_disabled_type',
        'condition_disabled_level',
        'condition_aids',
        'condition_aboriginal',
        'condition_disaster_victims',
        'condition_vagabond',
        'condition_Applicant_naturalization',
        'condition_Applicant_not_naturalization',
        'condition_Applicant_not_naturalization_and_study',
        'condition_police_officer',
        'filers',
        'last_modified_person'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tenant_id' => 'integer',
        'name' => 'string',
        'is_foreigner' => 'integer',
        'id_no' => 'string',
        'appellation' => 'string',
        'gender' => 'string',
        'condition_low_income_households' => 'integer',
        'condition_low_and_middle_income_households' => 'integer',
        'condition_special_circumstances_families' => 'integer',
        'condition_over_sixty_five' => 'integer',
        'condition_domestic_violence' => 'integer',
        'condition_disabled' => 'integer',
        'condition_disabled_type' => 'integer',
        'condition_disabled_level' => 'string',
        'condition_aids' => 'integer',
        'condition_aboriginal' => 'integer',
        'condition_disaster_victims' => 'integer',
        'condition_vagabond' => 'integer',
        'condition_Applicant_naturalization' => 'integer',
        'condition_Applicant_not_naturalization' => 'integer',
        'condition_Applicant_not_naturalization_and_study' => 'integer',
        'condition_police_officer' => 'integer',
        'filers' => 'integer',
        'last_modified_person' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tenant_id' => 'nullable|integer',
        'name' => 'nullable|string|max:191',
        'is_foreigner' => 'nullable|integer',
        'id_no' => 'nullable|string|max:191',
        'appellation' => 'nullable|string|max:191',
        'gender' => 'nullable|string|max:191',
        'condition_low_income_households' => 'nullable|integer',
        'condition_low_and_middle_income_households' => 'nullable|integer',
        'condition_special_circumstances_families' => 'nullable|integer',
        'condition_over_sixty_five' => 'nullable|integer',
        'condition_domestic_violence' => 'nullable|integer',
        'condition_disabled' => 'nullable|integer',
        'condition_disabled_type' => 'nullable|integer',
        'condition_disabled_level' => 'nullable|string|max:191',
        'condition_aids' => 'nullable|integer',
        'condition_aboriginal' => 'nullable|integer',
        'condition_disaster_victims' => 'nullable|integer',
        'condition_vagabond' => 'nullable|integer',
        'condition_Applicant_naturalization' => 'nullable|integer',
        'condition_Applicant_not_naturalization' => 'nullable|integer',
        'condition_Applicant_not_naturalization_and_study' => 'nullable|integer',
        'condition_police_officer' => 'nullable|integer',
        'filers' => 'nullable|integer',
        'last_modified_person' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
