<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class tenant_management_new
 * @package App\Models
 * @version April 19, 2022, 3:02 pm CST
 *
 * @property string $apply_date
 * @property integer $sales_id
 * @property string $government_no
 * @property string $case_no
 * @property integer $case_type_chartering
 * @property integer $case_type_escrow
 * @property string $case_status
 * @property string $name
 * @property string $gender
 * @property string $birthday
 * @property string $id_no
 * @property string $house_no
 * @property string $tel_day
 * @property string $tel_night
 * @property string $cellphone
 * @property string $email
 * @property string $membtype
 * @property string $weak_item
 * @property string $residence_city
 * @property string $residence_city_area
 * @property string $residence_addr
 * @property string $mailing_city
 * @property string $mailing_city_area
 * @property string $mailing_addr
 * @property integer $is_have_other_subsidy
 * @property integer $isOtherSubsidy01
 * @property integer $isOtherSubsidy01Info
 * @property integer $isOtherSubsidy02
 * @property integer $isOtherSubsidy02Info
 * @property integer $isOtherSubsidy03
 * @property integer $isOtherSubsidy03Info
 * @property integer $isOtherSubsidy04
 * @property integer $isOtherSubsidy04Info
 * @property integer $isOtherSubsidy05
 * @property integer $isOtherSubsidy05Info
 * @property integer $legal_agent_num
 * @property string $single_proxy_reason
 * @property string $first_legal_agent_name
 * @property string $first_legal_agent_tel
 * @property string $first_legal_agent_phone
 * @property string $first_legal_agent_addr
 * @property string $second_legal_agent_name
 * @property string $second_legal_agent_tel
 * @property string $second_legal_agent_phone
 * @property string $second_legal_agent_addr
 * @property string $bank_name
 * @property string $bank_branch_code
 * @property string $bank_owner_name
 * @property string $bank_account
 * @property integer $building_pattern_01
 * @property integer $building_pattern_02
 * @property integer $building_pattern_03
 * @property integer $building_pattern_04
 * @property integer $building_pattern_05
 * @property integer $building_type_01
 * @property integer $building_type_02
 * @property integer $building_type_03
 * @property integer $building_type_04
 * @property integer $building_use_area_01
 * @property integer $building_use_area_02
 * @property integer $building_use_area_03
 * @property integer $building_use_area_04
 * @property integer $building_use_area_05
 * @property integer $wish_floor_01
 * @property integer $wish_floor_02
 * @property integer $wish_floor_03
 * @property integer $wish_floor_04
 * @property integer $wish_floor_exact
 * @property integer $wish_room_num
 * @property integer $wish_living_rooms_num
 * @property integer $wish_bathrooms_num
 * @property integer $req_access_control
 * @property integer $rent_min
 * @property integer $building_offer_other
 * @property integer $building_offer_sofa
 * @property integer $building_offer_chair
 * @property integer $building_offer_desk
 * @property integer $building_offer_wardrobe
 * @property integer $building_offer_bed
 * @property integer $building_offer_natural_gas
 * @property integer $building_offer_washing_machine
 * @property integer $building_offer_internet
 * @property integer $building_offer_geyser
 * @property integer $building_offer_air_conditioner
 * @property integer $building_offer_cable
 * @property integer $building_offer_refrigerator
 * @property integer $building_offer_tv
 * @property integer $is_live_together
 * @property integer $is_live_together_desc
 * @property integer $rent_max
 */
class tenant_management_new extends Model
{
    use SoftDeletes;

    public $table = 'tenant_management_new';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'apply_date',
        'sales_id',
        'government_no',
        'case_no',
        'case_type_chartering',
        'case_type_escrow',
        'case_status',
        'name',
        'gender',
        'birthday',
        'id_no',
        'house_no',
        'tel_day',
        'tel_night',
        'cellphone',
        'email',
        'membtype',
        'weak_item',
        'condition_disabled_level',
        'condition_disabled_type',
        'residence_city',
        'residence_city_area',
        'residence_addr',
        'mailing_city',
        'mailing_city_area',
        'mailing_addr',
        'is_have_other_subsidy',
        'isOtherSubsidy01',
        'isOtherSubsidy01Info',
        'isOtherSubsidy02',
        'isOtherSubsidy02Info',
        'isOtherSubsidy03',
        'isOtherSubsidy03Info',
        'isOtherSubsidy04',
        'isOtherSubsidy04Info',
        'isOtherSubsidy05',
        'isOtherSubsidy05Info',
        'legal_agent_num',
        'single_proxy_reason',
        'first_legal_agent_name',
        'first_legal_agent_tel',
        'first_legal_agent_phone',
        'first_legal_agent_addr',
        'second_legal_agent_name',
        'second_legal_agent_tel',
        'second_legal_agent_phone',
        'second_legal_agent_addr',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'building_pattern_01',
        'building_pattern_02',
        'building_pattern_03',
        'building_pattern_04',
        'building_pattern_05',
        'building_type_01',
        'building_type_02',
        'building_type_03',
        'building_type_04',
        'building_use_area_01',
        'building_use_area_02',
        'building_use_area_03',
        'building_use_area_04',
        'building_use_area_05',
        'wish_floor_01',
        'wish_floor_02',
        'wish_floor_03',
        'wish_floor_04',
        'wish_floor_exact',
        'wish_room_num',
        'wish_living_rooms_num',
        'wish_bathrooms_num',
        'req_access_control',
        'rent_min',
        'building_offer_other',
        'building_offer_sofa',
        'building_offer_chair',
        'building_offer_desk',
        'building_offer_wardrobe',
        'building_offer_bed',
        'building_offer_natural_gas',
        'building_offer_washing_machine',
        'building_offer_internet',
        'building_offer_geyser',
        'building_offer_air_conditioner',
        'building_offer_cable',
        'building_offer_refrigerator',
        'building_offer_tv',
        'is_live_together',
        'is_live_together_desc',
        'rent_max',
        'change_note',
        'tenant_hope_locations',
        'virtual_bank_account',
        'note',
        'data_status',
        'guild_edition',
        'condition_end_date',
        'creator',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'apply_date' => 'date',
        'sales_id' => 'integer',
        'government_no' => 'string',
        'case_no' => 'string',
        'case_type_chartering' => 'integer',
        'case_type_escrow' => 'integer',
        'case_status' => 'string',
        'name' => 'string',
        'gender' => 'string',
        'birthday' => 'date',
        'id_no' => 'string',
        'house_no' => 'string',
        'tel_day' => 'string',
        'tel_night' => 'string',
        'cellphone' => 'string',
        'email' => 'string',
        'membtype' => 'string',
        'weak_item' => 'string',
        'condition_disabled_level' => 'string',
        'condition_disabled_type' => 'integer',
        'residence_city' => 'string',
        'residence_city_area' => 'string',
        'residence_addr' => 'string',
        'mailing_city' => 'string',
        'mailing_city_area' => 'string',
        'mailing_addr' => 'string',
        'is_have_other_subsidy' => 'integer',
        'isOtherSubsidy01' => 'integer',
        'isOtherSubsidy01Info' => 'integer',
        'isOtherSubsidy02' => 'integer',
        'isOtherSubsidy02Info' => 'integer',
        'isOtherSubsidy03' => 'integer',
        'isOtherSubsidy03Info' => 'integer',
        'isOtherSubsidy04' => 'integer',
        'isOtherSubsidy04Info' => 'integer',
        'isOtherSubsidy05' => 'integer',
        'isOtherSubsidy05Info' => 'integer',
        'legal_agent_num' => 'integer',
        'single_proxy_reason' => 'string',
        'first_legal_agent_name' => 'string',
        'first_legal_agent_tel' => 'string',
        'first_legal_agent_phone' => 'string',
        'first_legal_agent_addr' => 'string',
        'second_legal_agent_name' => 'string',
        'second_legal_agent_tel' => 'string',
        'second_legal_agent_phone' => 'string',
        'second_legal_agent_addr' => 'string',
        'bank_name' => 'string',
        'bank_branch_code' => 'string',
        'bank_owner_name' => 'string',
        'bank_account' => 'string',
        'building_pattern_01' => 'integer',
        'building_pattern_02' => 'integer',
        'building_pattern_03' => 'integer',
        'building_pattern_04' => 'integer',
        'building_pattern_05' => 'integer',
        'building_type_01' => 'integer',
        'building_type_02' => 'integer',
        'building_type_03' => 'integer',
        'building_type_04' => 'integer',
        'building_use_area_01' => 'integer',
        'building_use_area_02' => 'integer',
        'building_use_area_03' => 'integer',
        'building_use_area_04' => 'integer',
        'building_use_area_05' => 'integer',
        'wish_floor_01' => 'integer',
        'wish_floor_02' => 'integer',
        'wish_floor_03' => 'integer',
        'wish_floor_04' => 'integer',
        'wish_floor_exact' => 'integer',
        'wish_room_num' => 'integer',
        'wish_living_rooms_num' => 'integer',
        'wish_bathrooms_num' => 'integer',
        'req_access_control' => 'integer',
        'rent_min' => 'integer',
        'building_offer_other' => 'integer',
        'building_offer_sofa' => 'integer',
        'building_offer_chair' => 'integer',
        'building_offer_desk' => 'integer',
        'building_offer_wardrobe' => 'integer',
        'building_offer_bed' => 'integer',
        'building_offer_natural_gas' => 'integer',
        'building_offer_washing_machine' => 'integer',
        'building_offer_internet' => 'integer',
        'building_offer_geyser' => 'integer',
        'building_offer_air_conditioner' => 'integer',
        'building_offer_cable' => 'integer',
        'building_offer_refrigerator' => 'integer',
        'building_offer_tv' => 'integer',
        'is_live_together' => 'integer',
        'is_live_together_desc' => 'string',
        'rent_max' => 'integer',
        'change_note' => 'text',
        'tenant_hope_locations' => 'string',
        'virtual_bank_account' => 'string',
        'note' => 'string',
        'data_status' => 'string',
        'guild_edition' => 'integer',
        'condition_end_date' => 'date',
        'creator' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'apply_date' => 'nullable',
        'sales_id' => 'nullable|integer',
        'government_no' => 'nullable|string|max:191',
        'case_no' => 'nullable|string|max:191',
        'case_type_chartering' => 'nullable|integer',
        'case_type_escrow' => 'nullable|integer',
        'case_status' => 'nullable|string|max:191',
        'name' => 'nullable|string|max:191',
        'gender' => 'nullable|string|max:191',
        'birthday' => 'nullable',
        'id_no' => 'nullable|string|max:191',
        'house_no' => 'nullable|string|max:191',
        'tel_day' => 'nullable|string|max:191',
        'tel_night' => 'nullable|string|max:191',
        'cellphone' => 'nullable|string|max:191',
        'email' => 'nullable|string|max:191',
        'membtype' => 'nullable|string|max:191',
        'weak_item' => 'nullable|string|max:191',
        'condition_disabled_type' => 'nullable|integer',
        'condition_disabled_level' => 'nullable|string|max:191',
        'residence_city' => 'nullable|string|max:191',
        'residence_city_area' => 'nullable|string|max:191',
        'residence_addr' => 'nullable|string|max:191',
        'mailing_city' => 'nullable|string|max:191',
        'mailing_city_area' => 'nullable|string|max:191',
        'mailing_addr' => 'nullable|string|max:191',
        'is_have_other_subsidy' => 'nullable|integer',
        'isOtherSubsidy01' => 'nullable|integer',
        'isOtherSubsidy01Info' => 'nullable|integer',
        'isOtherSubsidy02' => 'nullable|integer',
        'isOtherSubsidy02Info' => 'nullable|integer',
        'isOtherSubsidy03' => 'nullable|integer',
        'isOtherSubsidy03Info' => 'nullable|integer',
        'isOtherSubsidy04' => 'nullable|integer',
        'isOtherSubsidy04Info' => 'nullable|integer',
        'isOtherSubsidy05' => 'nullable|integer',
        'isOtherSubsidy05Info' => 'nullable|integer',
        'legal_agent_num' => 'nullable|integer',
        'single_proxy_reason' => 'nullable|string|max:191',
        'first_legal_agent_name' => 'nullable|string|max:191',
        'first_legal_agent_tel' => 'nullable|string|max:191',
        'first_legal_agent_phone' => 'nullable|string|max:191',
        'first_legal_agent_addr' => 'nullable|string|max:191',
        'second_legal_agent_name' => 'nullable|string|max:191',
        'second_legal_agent_tel' => 'nullable|string|max:191',
        'second_legal_agent_phone' => 'nullable|string|max:191',
        'second_legal_agent_addr' => 'nullable|string|max:191',
        'bank_name' => 'nullable|string|max:191',
        'bank_branch_code' => 'nullable|string|max:191',
        'bank_owner_name' => 'nullable|string|max:191',
        'bank_account' => 'nullable|string|max:191',
        'building_pattern_01' => 'nullable|integer',
        'building_pattern_02' => 'nullable|integer',
        'building_pattern_03' => 'nullable|integer',
        'building_pattern_04' => 'nullable|integer',
        'building_pattern_05' => 'nullable|integer',
        'building_type_01' => 'nullable|integer',
        'building_type_02' => 'nullable|integer',
        'building_type_03' => 'nullable|integer',
        'building_type_04' => 'nullable|integer',
        'building_use_area_01' => 'nullable|integer',
        'building_use_area_02' => 'nullable|integer',
        'building_use_area_03' => 'nullable|integer',
        'building_use_area_04' => 'nullable|integer',
        'building_use_area_05' => 'nullable|integer',
        'wish_floor_01' => 'nullable|integer',
        'wish_floor_02' => 'nullable|integer',
        'wish_floor_03' => 'nullable|integer',
        'wish_floor_04' => 'nullable|integer',
        'wish_floor_exact' => 'nullable|integer',
        'wish_room_num' => 'nullable|integer',
        'wish_living_rooms_num' => 'nullable|integer',
        'wish_bathrooms_num' => 'nullable|integer',
        'req_access_control' => 'nullable|integer',
        'rent_min' => 'nullable|integer',
        'building_offer_other' => 'nullable|integer',
        'building_offer_sofa' => 'nullable|integer',
        'building_offer_chair' => 'nullable|integer',
        'building_offer_desk' => 'nullable|integer',
        'building_offer_wardrobe' => 'nullable|integer',
        'building_offer_bed' => 'nullable|integer',
        'building_offer_natural_gas' => 'nullable|integer',
        'building_offer_washing_machine' => 'nullable|integer',
        'building_offer_internet' => 'nullable|integer',
        'building_offer_geyser' => 'nullable|integer',
        'building_offer_air_conditioner' => 'nullable|integer',
        'building_offer_cable' => 'nullable|integer',
        'building_offer_refrigerator' => 'nullable|integer',
        'building_offer_tv' => 'nullable|integer',
        'is_live_together' => 'nullable|integer',
        'is_live_together_desc' => 'nullable|string|max:191',
        'rent_max' => 'nullable|integer',
        'tenant_hope_locations' => 'nullable|string',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'virtual_bank_account' => 'nullable|string',
        'change_note' => 'nullable|string',
        'note' => 'nullable|string',
        'data_status' => 'nullable|string',
        'guild_edition' => 'nullable|integer',
        'condition_end_date' => 'nullable',
        'creator' => 'nullable|integer',
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
