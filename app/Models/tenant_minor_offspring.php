<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

/**
 * Class tenant_minor_offspring
 * @package App\Models
 * @version April 13, 2022, 11:52 am CST
 *
 * @property integer $tenant_id
 * @property string $name
 * @property integer $is_foreigner
 * @property string $appellation
 * @property string $id_no
 * @property string $birthday
 * @property integer $filers
 * @property integer $last_modified_person
 */
class tenant_minor_offspring extends Model
{
    use SoftDeletes;

    public $table = 'tenant_minor_offspring';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'tenant_id',
        'name',
        'is_foreigner',
        'appellation',
        'id_no',
        'birthday',
        'filers',
        'last_modified_person'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tenant_id' => 'integer',
        'name' => 'string',
        'is_foreigner' => 'integer',
        'appellation' => 'string',
        'id_no' => 'string',
        'birthday' => '',
        'filers' => 'integer',
        'last_modified_person' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tenant_id' => 'nullable|integer',
        'name' => 'nullable|string|max:191',
        'is_foreigner' => 'nullable|integer',
        'appellation' => 'nullable|string|max:191',
        'id_no' => 'nullable|string|max:191',
        'birthday' => 'nullable',
        'filers' => 'nullable|integer',
        'last_modified_person' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d');
    }
}
