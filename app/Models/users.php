<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;


/**
 * Class users
 * @package App\Models
 * @version June 22, 2022, 2:46 pm CST
 *
 * @property string $name
 * @property string $email
 * @property string $passport_english_name
 * @property string $english_name
 * @property string $birthday
 * @property string $id_no
 * @property string $residence_city
 * @property string $residence_city_area
 * @property string $residenceresidence_addr_addr
 * @property string $mailing_city
 * @property string $mailing_city_area
 * @property string $mailing_addr
 * @property string $tel
 * @property string $cellphone
 * @property string $company_cellphone
 * @property string $bank_name
 * @property string $bank_branch_code
 * @property string $bank_owner_name
 * @property string $bank_account
 * @property string|\Carbon\Carbon $email_verified_at
 * @property string $password
 * @property string $role
 * @property string $remember_token
 * @property string $stoken
 */
class users extends Model
{
    use SoftDeletes;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'passport_english_name',
        'english_name',
        'birthday',
        'id_no',
        'residence_city',
        'residence_city_area',
        'residenceresidence_addr_addr',
        'mailing_city',
        'mailing_city_area',
        'mailing_addr',
        'tel',
        'cellphone',
        'company_cellphone',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'email_verified_at',
        'password',
        'role',
        'remember_token',
        'stoken',
        'lock',
        'is_sales',
        'arrival_date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'passport_english_name' => 'string',
        'english_name' => 'string',
        'birthday' => 'date',
        'id_no' => 'string',
        'residence_city' => 'string',
        'residence_city_area' => 'string',
        'residenceresidence_addr_addr' => 'string',
        'mailing_city' => 'string',
        'mailing_city_area' => 'string',
        'mailing_addr' => 'string',
        'tel' => 'string',
        'cellphone' => 'string',
        'company_cellphone' => 'string',
        'bank_name' => 'string',
        'bank_branch_code' => 'string',
        'bank_owner_name' => 'string',
        'bank_account' => 'string',
        'email_verified_at' => 'datetime',
        'password' => 'string',
        'role' => 'string',
        'lock' => 'integer',
        'is_sales' => 'integer',
        'remember_token' => 'string',
        'stoken' => 'string',
        'arrival_date' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:191',
        'email' => 'required|string|max:191',
        'passport_english_name' => 'nullable|string|max:191',
        'english_name' => 'nullable|string|max:191',
        'birthday' => 'nullable',
        'id_no' => 'nullable|string|max:191',
        'residence_city' => 'nullable|string|max:191',
        'residence_city_area' => 'nullable|string|max:191',
        'residenceresidence_addr_addr' => 'nullable|string|max:191',
        'mailing_city' => 'nullable|string|max:191',
        'mailing_city_area' => 'nullable|string|max:191',
        'mailing_addr' => 'nullable|string|max:191',
        'tel' => 'nullable|string|max:191',
        'cellphone' => 'nullable|string|max:191',
        'company_cellphone' => 'nullable|string|max:191',
        'bank_name' => 'nullable|string|max:191',
        'bank_branch_code' => 'nullable|string|max:191',
        'bank_owner_name' => 'nullable|string|max:191',
        'bank_account' => 'nullable|string|max:191',
        'email_verified_at' => 'nullable',
        'password' => 'nullable|string|max:191',
        'role' => 'nullable|string|max:191',
        'lock' => 'required',
        'is_sales' => 'required',
        'remember_token' => 'nullable|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'stoken' => 'nullable|string|max:191',
        'deleted_at' => 'nullable',
        'arrival_date' => 'nullable',
    ];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
