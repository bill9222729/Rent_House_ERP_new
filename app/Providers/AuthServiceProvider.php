<?php

namespace App\Providers;

use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // 系統管理者 Gate 規則
        Gate::define('admin', function ($user) {
            return $user->role == User::ROLE_ADMIN;
        });

        // 一般管理者 Gate 規則
        Gate::define('sales', function ($user) {
            return $user->role == User::ROLE_SALES;
        });

        // 一般使用者 Gate 規則
        Gate::define('staff', function ($user) {
            return $user->role == User::ROLE_STAFF;
        });
    }
}
