<?php

namespace App\Repositories;

use App\Models\BuildingManagement;
use App\Repositories\BaseRepository;

/**
 * Class BuildingManagementRepository
 * @package App\Repositories
 * @version April 20, 2021, 12:16 pm UTC
*/

class BuildingManagementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'file_json',
        'apply_date',
        'sublet',
        'rent',
        'notarization',
        'request_form_type',
        'address_city',
        'address_cityarea',
        'address_street',
        'address_ln',
        'address_aly',
        'address_num',
        'address_num_hyphen',
        'cellphone',
        'located_city',
        'located_cityarea',
        'located_build_address',
        'located_segment_num',
        'located_small_lot',
        'located_land_num',
        'located_build_num',
        'pattern',
        'pattern_remark',
        'h_info_type',
        'h_info_age',
        'h_info_completion_date',
        'h_info_fl',
        'h_info_hyphen',
        'h_info_suite',
        'h_info_total_fl',
        'h_info_pattern_room',
        'h_info_pattern_hall',
        'h_info_pattern_bath',
        'h_info_pattern_material',
        'h_info_warrant_ping_num',
        'h_info_actual_ping_num',
        'h_info_usage',
        'h_info_material',
        'landlord_expect_rent',
        'landlord_expect_deposit_month',
        'landlord_expect_deposit',
        'landlord_expect_bargain',
        'manage_fee',
        'manage_fee_month',
        'manage_fee_ping',
        'contain_fee_ele',
        'contain_fee_water',
        'contain_fee_gas',
        'contain_fee_clean',
        'contain_fee_pay_tv',
        'contain_fee_net',
        'equipment_tv',
        'equipment_refrigerator',
        'equipment_pay_tv',
        'equipment_air_conditioner',
        'equipment_water_heater',
        'equipment_net',
        'equipment_washer',
        'equipment_gas',
        'equipment_bed',
        'equipment_wardrobe',
        'equipment_table',
        'equipment_chair',
        'equipment_sofa',
        'equipment_other',
        'equipment_other_detail',
        'can_Cook',
        'parking',
        'Barrier_free_facility',
        'curfew',
        'curfew_management',
        'curfew_card',
        'curfew_other',
        'curfew_other_detail',
        'house_id',
        'landlord_id',
        'status',
        'num',
        'Lessor_name',
        'Lessor_gender',
        'Lessor_birthday',
        'Lessor_ID_num',
        'Lessor_phone',
        'residence_address_city',
        'residence_address_cityarea',
        'residence_address_street',
        'residence_address_ln',
        'residence_address_aly',
        'residence_address_num',
        'residence_address_num_hyphen',
        'collection_agent',
        'agent_ID_num',
        'agent_bank_name',
        'agent_branch_name',
        'landlord_bank_account',
        'remark_1',
        'remark_2',
        'located_build_address',
        'park_mang_fee',
        'files',
        'collection_agent_address',
        'collection_agent_phone',
        'sales',
        'v_account',
        'empty_house',
        'source',
        'fire_alert',
        'fire_alert_in_house',
        'notary',
        'n_name',
        'car_position',
        'p_code',
        'email',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BuildingManagement::class;
    }
}
