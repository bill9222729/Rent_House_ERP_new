<?php

namespace App\Repositories;

use App\Models\FixHistory;
use App\Repositories\BaseRepository;

/**
 * Class FixHistoryRepository
 * @package App\Repositories
 * @version June 16, 2021, 2:44 pm UTC
*/

class FixHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sales',
        'pay_date',
        'item_code',
        'item_address',
        'pay_year',
        'fix_vendor',
        'call_fix_date',
        'item_owner',
        'owner_phone',
        'client_name',
        'client_phone',
        'fix_item',
        'fix_source',
        'check_date',
        'fix_date',
        'fix_done',
        'fix_record',
        'fix_price',
        'receipt',
        'final_price',
        'balance',
        'owner_start_date',
        'owner_end_date',
        'change_start_date',
        'change_end_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FixHistory::class;
    }
}
