<?php

namespace App\Repositories;

use App\Models\HousePayHistory;
use App\Repositories\BaseRepository;

/**
 * Class HousePayHistoryRepository
 * @package App\Repositories
 * @version August 4, 2021, 9:01 am UTC
*/

class HousePayHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contract_id',
        'month',
        'money',
        'type',
        'freq',
        'year'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HousePayHistory::class;
    }
}
