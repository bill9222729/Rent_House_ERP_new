<?php

namespace App\Repositories;

use App\Models\LandlordManagement;
use App\Repositories\BaseRepository;

/**
 * Class LandlordManagementRepository
 * @package App\Repositories
 * @version April 20, 2021, 11:41 am UTC
*/

class LandlordManagementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'id_num',
        'address',
        'phone',
        'birthday',
        'bank_acc'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LandlordManagement::class;
    }
}
