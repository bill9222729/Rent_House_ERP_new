<?php

namespace App\Repositories;

use App\Models\LogHistory;
use App\Repositories\BaseRepository;

/**
 * Class LogHistoryRepository
 * @package App\Repositories
 * @version June 6, 2021, 2:27 pm UTC
*/

class LogHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'url',
        'request_body',
        'user_id',
        'method'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LogHistory::class;
    }
}
