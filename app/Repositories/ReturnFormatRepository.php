<?php

namespace App\Repositories;

use App\Models\ReturnFormat;
use App\Repositories\BaseRepository;

/**
 * Class ReturnFormatRepository
 * @package App\Repositories
 * @version May 20, 2021, 4:00 pm UTC
*/

class ReturnFormatRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        't_id',
        't_code',
        'tax',
        'bank_account',
        'user_name',
        't_price',
        't_status',
        'name',
        'item_address',
        'item_month_price',
        'item_price_support',
        'watch_price',
        'parking_price',
        'fix_price',
        't_cost',
        'other_price',
        'total_price',
        'note'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReturnFormat::class;
    }
}
