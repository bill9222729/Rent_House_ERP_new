<?php

namespace App\Repositories;

use App\Models\SalesmanManagement;
use App\Repositories\BaseRepository;

/**
 * Class SalesmanManagementRepository
 * @package App\Repositories
 * @version April 20, 2021, 11:40 am UTC
*/

class SalesmanManagementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'id_num',
        'address',
        'phone',
        'birthday',
        'bank_acc'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SalesmanManagement::class;
    }
}
