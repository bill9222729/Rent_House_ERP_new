<?php

namespace App\Repositories;

use App\Models\TenantManagement;
use App\Repositories\BaseRepository;

/**
 * Class TenantManagementRepository
 * @package App\Repositories
 * @version April 20, 2021, 11:42 am UTC
*/

class TenantManagementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'apply_date',
        'charter',
        'escrow',
        'lessee_name',
        'lessee_gender',
        'lessee_birthday',
        'lessee_id_num',
        'lessee_hc_num',
        'lessee_telephone_d',
        'lessee_telephone_n',
        'lessee_cellphone',
        'lessee_email',
        'qualifications',
        'hr_city',
        'hr_cityarea',
        'hr_address',
        'contact_city',
        'contact_cityarea',
        'contact_address',
        'receive_subsidy',
        'subsidy_rent',
        'subsidy_low_income',
        'subsidy_disability',
        'subsidy_decree',
        'lease_nr_sr_city',
        'legal_agent_num',
        'first_agent_name',
        'first_agent_telephone',
        'first_agent_cellphone',
        'first_agent_address',
        'second_agent_name',
        'second_agent_telephone',
        'second_agent_cellphone',
        'second_agent_address'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TenantManagement::class;
    }
}
