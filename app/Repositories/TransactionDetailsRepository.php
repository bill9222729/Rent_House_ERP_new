<?php

namespace App\Repositories;

use App\Models\TransactionDetails;
use App\Repositories\BaseRepository;

/**
 * Class TransactionDetailsRepository
 * @package App\Repositories
 * @version May 26, 2021, 8:14 am UTC
*/

class TransactionDetailsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'account',
        't_date',
        't_time',
        'a_date',
        'detail',
        'expense',
        'income',
        'balance',
        'remark_1',
        'remark_2',
        'remark_3',
        'ticket_num'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TransactionDetails::class;
    }
}
