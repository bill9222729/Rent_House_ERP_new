<?php

namespace App\Repositories;

use App\Models\building_management_new;
use App\Repositories\BaseRepository;

/**
 * Class building_management_newRepository
 * @package App\Repositories
 * @version February 25, 2022, 4:02 am UTC
 */

class building_management_newRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'building_address_city',
        'building_address_city_area',
        'building_address_street',
        'building_address_ln',
        'building_address_aly',
        'building_address_num',
        'building_address_num_hyphen',
        'building_address_floor',
        'building_address_floor_sub',
        'building_address_room_num',
        'building_address_building_floor',
        'building_address_building_desc',
        'building_located_city',
        'building_located_city_area',
        'building_located_lot',
        'building_located_land_num',
        'building_num',
        'building_rent_type',
        'building_pattren',
        'building_type',
        'building_age',
        'building_complete_date',
        'building_rooms',
        'building_livingrooms',
        'building_bathrooms',
        'building_compartment_material',
        'building_total_square_meter',
        'building_use_square_meter',
        'building_materials',
        'building_rent',
        'building_deposit_amount',
        'building_is_including_electricity_fees',
        'building_is_cooking',
        'building_is_including_water_fees',
        'building_is_including_parking_space',
        'building_is_including_accessible_equipment',
        'building_is_including_natural_gas',
        'building_is_including_cable',
        'building_is_including_internet',
        'building_is_including_cleaning_fee',
        'building_offer_tv',
        'building_offer_refrigerator',
        'building_offer_cable',
        'building_offer_air_conditioner',
        'building_offer_geyser',
        'building_offer_internet',
        'building_offer_washing_machine',
        'building_offer_natural_gas',
        'building_offer_bed',
        'building_offer_wardrobe',
        'building_offer_desk',
        'building_offer_chair',
        'building_offer_sofa',
        'building_offer_other',
        'building_use',
        'apply_date',
        'case_no',
        'case_type_chartering',
        'case_type_escrow',
        'case_state',
        'name',
        'gender',
        'birthday',
        'is_foreigner',
        'id_no',
        'passport_code',
        'house_no',
        'tal_day',
        'tal_night',
        'cellphone',
        'email',
        'residence_city',
        'residence_city_area',
        'residence_addr',
        'mailing_city',
        'mailing_city_area',
        'mailing_addr',
        'legal_agent_num',
        'single_proxy_reason',
        'first_legal_agent_name',
        'first_legal_agent_tel',
        'first_legal_agent_phone',
        'first_legal_agent_addr',
        'second_legal_agent_name',
        'second_legal_agent_tel',
        'second_legal_agent_phone',
        'second_legal_agent_addr',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'building_deposit_amount_month',
        'can_bargain',
        'has_management_fee',
        'management_fee_month',
        'management_fee_square_meter',
        'building_offer_other_desc',
        'building_source',
        'representative',
        'tax_number',
        'agent_name',
        'agent_tel',
        'agent_cellphone',
        'agent_addr',
        'lessor_type',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return building_management_new::class;
    }
}
