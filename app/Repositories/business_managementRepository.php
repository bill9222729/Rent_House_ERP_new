<?php

namespace App\Repositories;

use App\Models\business_management;
use App\Repositories\BaseRepository;

/**
 * Class business_managementRepository
 * @package App\Repositories
 * @version April 26, 2021, 1:08 pm UTC
*/

class business_managementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'phone',
        'id_number',
        'birthday',
        'bank_account',
        'management_fee_set',
        'escrow_quota',
        'box_quota'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return business_management::class;
    }
}
