<?php

namespace App\Repositories;

use App\Models\change_history;
use App\Repositories\BaseRepository;

/**
 * Class change_historyRepository
 * @package App\Repositories
 * @version April 8, 2022, 2:04 pm CST
*/

class change_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'building_id',
        'apply_date',
        'change_date',
        'lessor_type',
        'name',
        'phone',
        'reason',
        'change_note',
        'edit_member_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return change_history::class;
    }
}
