<?php

namespace App\Repositories;

use App\Models\city;
use App\Repositories\BaseRepository;

/**
 * Class cityRepository
 * @package App\Repositories
 * @version March 21, 2022, 3:39 am UTC
*/

class cityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return city::class;
    }
}
