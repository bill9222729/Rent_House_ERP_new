<?php

namespace App\Repositories;

use App\Models\city_area;
use App\Repositories\BaseRepository;

/**
 * Class city_areaRepository
 * @package App\Repositories
 * @version March 21, 2022, 4:06 am UTC
*/

class city_areaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city_value',
        'value',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return city_area::class;
    }
}
