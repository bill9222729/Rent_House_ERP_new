<?php

namespace App\Repositories;

use App\Models\contract;
use App\Repositories\BaseRepository;

/**
 * Class contractRepository
 * @package App\Repositories
 * @version April 29, 2021, 1:27 pm UTC
*/

class contractRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contract_number_of_periods',
        'contract_category',
        'contract_business_sign_back',
        'contract_match_date',
        'landlord_match_number',
        'landlord_existing_tenant',
        'landlord_contract_date',
        'landlord_contract_expiry_date',
        'landlord_item_number',
        'landlord_ministry_of_the_interior_number',
        'landlord_types_of',
        'tenant_case_number',
        'tenant_ministry_of_the_interior_number',
        'rent_evaluation_form_market_rent',
        'rent_evaluation_form_assess_rent',
        'actual_contract_rent_to_landlord',
        'actual_contract_tenant_pays_rent',
        'actual_contract_rent_subsidy',
        'actual_contract_deposit',
        'to_the_landlord_rent_day',
        'package_escrow_fee_month_minute',
        'package_escrow_fee_amount',
        'package_escrow_fee_number_of_periods',
        'rent_subsidy_month_minute',
        'rent_subsidy_amount',
        'rent_subsidy_number_of_periods',
        'development_matching_fees_month',
        'development_matching_fees_amount',
        'notary_fees_notarization_date',
        'notary_fees_month',
        'notary_fees_amount',
        'repair_cost_month_minute',
        'repair_cost_amount',
        'insurance_month_minute',
        'insurance_amount',
        'insurance_period_three_year',
        'insurance_period_three_start_and_end_date',
        'remark_1',
        'remark_2',
        'file_upload',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return contract::class;
    }
}
