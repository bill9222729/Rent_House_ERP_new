<?php

namespace App\Repositories;

use App\Models\contract_management;
use App\Repositories\BaseRepository;

/**
 * Class contract_managementRepository
 * @package App\Repositories
 * @version March 2, 2022, 9:15 am UTC
 */

class contract_managementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'case_status',
        'signing_date',
        'other_landlord_desc',
        'lease_start_date',
        'lease_end_date',
        'rent',
        'rent_pay_type',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'deposit_amount',
        'contract_note',
        'apply_man_id',
        'remark',
        'file_path',
        'match_id',
        'lease_start_end_date',
        'building_case_no',
        'tenant_case_no',
        'stakeholders',
        'data_status',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return contract_management::class;
    }
}
