<?php

namespace App\Repositories;

use App\Models\contracts_new;
use App\Repositories\BaseRepository;

/**
 * Class contracts_newRepository
 * @package App\Repositories
 * @version February 24, 2022, 7:30 am UTC
*/

class contracts_newRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'building_case_id',
        'tenant_case_id',
        'lease_start_date',
        'lease_end_date',
        'charter_case_id',
        'contract_type',
        'match_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return contracts_new::class;
    }
}
