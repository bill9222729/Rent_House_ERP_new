<?php

namespace App\Repositories;

use App\Models\file_managment;
use App\Repositories\BaseRepository;

/**
 * Class file_managmentRepository
 * @package App\Repositories
 * @version April 6, 2022, 10:37 am CST
*/

class file_managmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'file_origin_name',
        'file_server_name',
        'file_size',
        'file_source',
        'file_source_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return file_managment::class;
    }
}
