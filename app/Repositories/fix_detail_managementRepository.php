<?php

namespace App\Repositories;

use App\Models\fix_detail_management;
use App\Repositories\BaseRepository;

/**
 * Class fix_detail_managementRepository
 * @package App\Repositories
 * @version May 16, 2022, 9:48 am CST
*/

class fix_detail_managementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'report_date',
        'fix_id',
        'repair_item',
        'repair_manufacturers',
        'repair_records',
        'meeting_date',
        'meeting_master',
        'fix_date',
        'completion_date',
        'please_amount',
        'reply_amount',
        'monthly_application_month',
        'balance',
        'note',
        'no_reference',
        'building_id',
        'has_receipt',
        'broken_reason',
        'invoice_date',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return fix_detail_management::class;
    }
}
