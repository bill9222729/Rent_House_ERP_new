<?php

namespace App\Repositories;

use App\Models\fix_management;
use App\Repositories\BaseRepository;

/**
 * Class fix_managementRepository
 * @package App\Repositories
 * @version May 16, 2022, 9:48 am CST
*/

class fix_managementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'match_id',
        'building_case_no'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return fix_management::class;
    }
}
