<?php

namespace App\Repositories;

use App\Models\insurance_management;
use App\Repositories\BaseRepository;

/**
 * Class insurance_managementRepository
 * @package App\Repositories
 * @version April 29, 2022, 4:35 pm CST
*/

class insurance_managementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sales_id',
        'match_id',
        'building_id',
        'insurance_premium',
        'insurer',
        'insurance_start',
        'insurance_end',
        'monthly_application_month'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return insurance_management::class;
    }
}
