<?php

namespace App\Repositories;

use App\Models\located_lot;
use App\Repositories\BaseRepository;

/**
 * Class located_lotRepository
 * @package App\Repositories
 * @version March 31, 2022, 1:38 am CST
*/

class located_lotRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city_area_value',
        'value',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return located_lot::class;
    }
}
