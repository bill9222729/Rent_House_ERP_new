<?php

namespace App\Repositories;

use App\Models\match_management;
use App\Repositories\BaseRepository;

/**
 * Class match_managementRepository
 * @package App\Repositories
 * @version March 2, 2022, 5:35 am UTC
*/

class match_managementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'match_id',
        'building_case_id',
        'tenant_case_id',
        'lease_start_date',
        'lease_end_date',
        'charter_case_id',
        'contract_type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return match_management::class;
    }
}
