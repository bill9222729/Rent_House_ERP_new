<?php

namespace App\Repositories;

use App\Models\salesman_management;
use App\Repositories\BaseRepository;

/**
 * Class salesman_managementRepository
 * @package App\Repositories
 * @version April 29, 2022, 3:16 pm CST
*/

class salesman_managementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'id_num',
        'address',
        'phone',
        'birthday',
        'bank_acc'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return salesman_management::class;
    }
}
