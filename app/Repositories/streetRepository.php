<?php

namespace App\Repositories;

use App\Models\street;
use App\Repositories\BaseRepository;

/**
 * Class streetRepository
 * @package App\Repositories
 * @version March 31, 2022, 1:36 am CST
*/

class streetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city_area_value',
        'value',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return street::class;
    }
}
