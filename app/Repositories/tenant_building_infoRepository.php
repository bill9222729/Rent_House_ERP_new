<?php

namespace App\Repositories;

use App\Models\tenant_building_info;
use App\Repositories\BaseRepository;

/**
 * Class tenant_building_infoRepository
 * @package App\Repositories
 * @version April 13, 2022, 2:12 pm CST
*/

class tenant_building_infoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tenant_id',
        'name',
        'located_city',
        'located_Twn_spcode_area',
        'located_lot_area',
        'located_landno',
        'building_no',
        'building_total_square_meter_percent',
        'building_total_square_meter',
        'addr_cntcode',
        'addr_twnspcode',
        'addr_street_area',
        'addr_lane',
        'addr_alley',
        'addr_no',
        'is_household',
        'filers',
        'last_modified_person'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tenant_building_info::class;
    }
}
