<?php

namespace App\Repositories;

use App\Models\tenant_family_info;
use App\Repositories\BaseRepository;

/**
 * Class tenant_family_infoRepository
 * @package App\Repositories
 * @version April 13, 2022, 11:46 am CST
 */

class tenant_family_infoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'tenant_id',
        'name',
        'is_foreigner',
        'id_no',
        'appellation',
        'gender',
        'condition_low_income_households',
        'condition_low_and_middle_income_households',
        'condition_special_circumstances_families',
        'condition_over_sixty_five',
        'condition_domestic_violence',
        'condition_disabled',
        'condition_disabled_type',
        'condition_disabled_level',
        'condition_aids',
        'condition_aboriginal',
        'condition_disaster_victims',
        'condition_vagabond',
        'condition_Applicant_naturalization',
        'condition_Applicant_not_naturalization',
        'condition_Applicant_not_naturalization_and_study',
        'condition_police_officer',
        'filers',
        'last_modified_person'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tenant_family_info::class;
    }
}
