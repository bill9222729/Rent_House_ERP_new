<?php

namespace App\Repositories;

use App\Models\tenant_management_new;
use App\Repositories\BaseRepository;

/**
 * Class tenant_management_newRepository
 * @package App\Repositories
 * @version April 19, 2022, 3:02 pm CST
 */

class tenant_management_newRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'apply_date',
        'sales_id',
        'government_no',
        'case_no',
        'case_type_chartering',
        'case_type_escrow',
        'case_status',
        'name',
        'gender',
        'birthday',
        'id_no',
        'house_no',
        'tel_day',
        'tel_night',
        'cellphone',
        'email',
        'membtype',
        'weak_item',
        'residence_city',
        'residence_city_area',
        'residence_addr',
        'mailing_city',
        'mailing_city_area',
        'mailing_addr',
        'is_have_other_subsidy',
        'isOtherSubsidy01',
        'isOtherSubsidy01Info',
        'isOtherSubsidy02',
        'isOtherSubsidy02Info',
        'isOtherSubsidy03',
        'isOtherSubsidy03Info',
        'isOtherSubsidy04',
        'isOtherSubsidy04Info',
        'isOtherSubsidy05',
        'isOtherSubsidy05Info',
        'legal_agent_num',
        'single_proxy_reason',
        'first_legal_agent_name',
        'first_legal_agent_tel',
        'first_legal_agent_phone',
        'first_legal_agent_addr',
        'second_legal_agent_name',
        'second_legal_agent_tel',
        'second_legal_agent_phone',
        'second_legal_agent_addr',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'building_pattern_01',
        'building_pattern_02',
        'building_pattern_03',
        'building_pattern_04',
        'building_pattern_05',
        'building_type_01',
        'building_type_02',
        'building_type_03',
        'building_type_04',
        'building_use_area_01',
        'building_use_area_02',
        'building_use_area_03',
        'building_use_area_04',
        'building_use_area_05',
        'wish_floor_01',
        'wish_floor_02',
        'wish_floor_03',
        'wish_floor_04',
        'wish_floor_exact',
        'wish_room_num',
        'wish_living_rooms_num',
        'wish_bathrooms_num',
        'req_access_control',
        'rent_min',
        'building_offer_other',
        'building_offer_sofa',
        'building_offer_chair',
        'building_offer_desk',
        'building_offer_wardrobe',
        'building_offer_bed',
        'building_offer_natural_gas',
        'building_offer_washing_machine',
        'building_offer_internet',
        'building_offer_geyser',
        'building_offer_air_conditioner',
        'building_offer_cable',
        'building_offer_refrigerator',
        'building_offer_tv',
        'is_live_together',
        'is_live_together_desc',
        'rent_max',
        'tenant_hope_locations',
        'condition_end_date',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tenant_management_new::class;
    }
}
