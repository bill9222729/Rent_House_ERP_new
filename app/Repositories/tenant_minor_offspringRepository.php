<?php

namespace App\Repositories;

use App\Models\tenant_minor_offspring;
use App\Repositories\BaseRepository;

/**
 * Class tenant_minor_offspringRepository
 * @package App\Repositories
 * @version April 13, 2022, 11:52 am CST
 */

class tenant_minor_offspringRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'tenant_id',
        'name',
        'is_foreigner',
        'appellation',
        'id_no',
        'birthday',
        'filers',
        'last_modified_person'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tenant_minor_offspring::class;
    }
}
