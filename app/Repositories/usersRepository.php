<?php

namespace App\Repositories;

use App\Models\users;
use App\Repositories\BaseRepository;

/**
 * Class usersRepository
 * @package App\Repositories
 * @version June 22, 2022, 2:46 pm CST
*/

class usersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'email',
        'passport_english_name',
        'english_name',
        'birthday',
        'id_no',
        'residence_city',
        'residence_city_area',
        'residenceresidence_addr_addr',
        'mailing_city',
        'mailing_city_area',
        'mailing_addr',
        'tel',
        'cellphone',
        'company_cellphone',
        'bank_name',
        'bank_branch_code',
        'bank_owner_name',
        'bank_account',
        'email_verified_at',
        'password',
        'role',
        'remember_token',
        'stoken',
        'is_sales',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return users::class;
    }
}
