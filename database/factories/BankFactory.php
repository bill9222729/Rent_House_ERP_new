<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Bank;
use Faker\Generator as Faker;

$factory->define(Bank::class, function (Faker $faker) {

    return [
        'bank_code' => $faker->word,
        'bank_account' => $faker->word,
        'tax' => $faker->word,
        'price' => $faker->randomDigitNotNull,
        'user_number' => $faker->word,
        'company_stack_code' => $faker->word,
        'Creator' => $faker->word,
        'search' => $faker->word,
        'bank_noted' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
