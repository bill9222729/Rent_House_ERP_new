<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\FixHistory;
use Faker\Generator as Faker;

$factory->define(FixHistory::class, function (Faker $faker) {

    return [
        'sales' => $faker->word,
        'pay_date' => $faker->word,
        'item_code' => $faker->word,
        'item_address' => $faker->word,
        'pay_year' => $faker->randomDigitNotNull,
        'fix_vendor' => $faker->word,
        'call_fix_date' => $faker->word,
        'item_owner' => $faker->word,
        'owner_phone' => $faker->word,
        'client_name' => $faker->word,
        'client_phone' => $faker->word,
        'fix_item' => $faker->word,
        'fix_source' => $faker->word,
        'check_date' => $faker->word,
        'fix_date' => $faker->word,
        'fix_done' => $faker->word,
        'fix_record' => $faker->word,
        'fix_price' => $faker->randomDigitNotNull,
        'receipt' => $faker->randomDigitNotNull,
        'final_price' => $faker->randomDigitNotNull,
        'balance' => $faker->randomDigitNotNull,
        'owner_start_date' => $faker->word,
        'owner_end_date' => $faker->word,
        'change_start_date' => $faker->word,
        'change_end_date' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
