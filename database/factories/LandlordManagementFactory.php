<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LandlordManagement;
use Faker\Generator as Faker;

$factory->define(LandlordManagement::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'id_num' => $faker->word,
        'address' => $faker->word,
        'phone' => $faker->word,
        'birthday' => $faker->word,
        'bank_acc' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
