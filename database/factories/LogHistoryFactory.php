<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LogHistory;
use Faker\Generator as Faker;

$factory->define(LogHistory::class, function (Faker $faker) {

    return [
        'url' => $faker->word,
        'request_body' => $faker->text,
        'user_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
