<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ReturnFormat;
use Faker\Generator as Faker;

$factory->define(ReturnFormat::class, function (Faker $faker) {

    return [
        't_id' => $faker->word,
        't_code' => $faker->word,
        'tax' => $faker->word,
        'bank_account' => $faker->word,
        'user_name' => $faker->word,
        't_price' => $faker->randomDigitNotNull,
        't_status' => $faker->word,
        'name' => $faker->word,
        'item_address' => $faker->word,
        'item_month_price' => $faker->randomDigitNotNull,
        'item_price_support' => $faker->randomDigitNotNull,
        'watch_price' => $faker->randomDigitNotNull,
        'parking_price' => $faker->randomDigitNotNull,
        'fix_price' => $faker->randomDigitNotNull,
        't_cost' => $faker->randomDigitNotNull,
        'other_price' => $faker->randomDigitNotNull,
        'total_price' => $faker->randomDigitNotNull,
        'note' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
