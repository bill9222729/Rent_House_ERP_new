<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SalesmanManagement;
use Faker\Generator as Faker;

$factory->define(SalesmanManagement::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'id_num' => $faker->word,
        'address' => $faker->word,
        'phone' => $faker->word,
        'birthday' => $faker->word,
        'bank_acc' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
