<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TenantManagement;
use Faker\Generator as Faker;

$factory->define(TenantManagement::class, function (Faker $faker) {

    return [
        'apply_date' => $faker->word,
        'charter' => $faker->word,
        'escrow' => $faker->word,
        'lessee_name' => $faker->word,
        'lessee_gender' => $faker->word,
        'lessee_birthday' => $faker->word,
        'lessee_id_num' => $faker->word,
        'lessee_hc_num' => $faker->word,
        'lessee_telephone_d' => $faker->word,
        'lessee_telephone_n' => $faker->word,
        'lessee_cellphone' => $faker->word,
        'lessee_email' => $faker->word,
        'qualifications' => $faker->word,
        'hr_city' => $faker->word,
        'hr_cityarea' => $faker->word,
        'hr_address' => $faker->word,
        'contact_city' => $faker->word,
        'contact_cityarea' => $faker->word,
        'contact_address' => $faker->word,
        'receive_subsidy' => $faker->word,
        'subsidy_rent' => $faker->word,
        'subsidy_low_income' => $faker->word,
        'subsidy_disability' => $faker->word,
        'subsidy_decree' => $faker->word,
        'lease_nr_sr_city' => $faker->word,
        'legal_agent_num' => $faker->word,
        'first_agent_name' => $faker->word,
        'first_agent_telephone' => $faker->word,
        'first_agent_cellphone' => $faker->word,
        'first_agent_address' => $faker->word,
        'second_agent_name' => $faker->word,
        'second_agent_telephone' => $faker->word,
        'second_agent_cellphone' => $faker->word,
        'second_agent_address' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
