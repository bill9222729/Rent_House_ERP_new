<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TransactionDetails;
use Faker\Generator as Faker;

$factory->define(TransactionDetails::class, function (Faker $faker) {

    return [
        'account' => $faker->word,
        't_date' => $faker->word,
        't_time' => $faker->date('Y-m-d H:i:s'),
        'a_date' => $faker->word,
        'detail' => $faker->word,
        'expense' => $faker->randomDigitNotNull,
        'income' => $faker->randomDigitNotNull,
        'balance' => $faker->randomDigitNotNull,
        'remark_1' => $faker->word,
        'remark_2' => $faker->word,
        'remark_3' => $faker->word,
        'ticket_num' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
