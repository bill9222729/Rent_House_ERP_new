<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\business_management;
use Faker\Generator as Faker;

$factory->define(business_management::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'address' => $faker->word,
        'phone' => $faker->word,
        'id_number' => $faker->word,
        'birthday' => $faker->word,
        'bank_account' => $faker->word,
        'management_fee_set' => $faker->word,
        'escrow_quota' => $faker->randomDigitNotNull,
        'box_quota' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
