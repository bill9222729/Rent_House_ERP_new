<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\change_history;
use Faker\Generator as Faker;

$factory->define(change_history::class, function (Faker $faker) {

    return [
        'building_id' => $faker->word,
        'apply_date' => $faker->word,
        'change_date' => $faker->word,
        'lessor_type' => $faker->word,
        'name' => $faker->word,
        'phone' => $faker->word,
        'reason' => $faker->word,
        'change_note' => $faker->word,
        'edit_member_id' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
