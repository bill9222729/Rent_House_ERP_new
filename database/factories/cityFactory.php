<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\city;
use Faker\Generator as Faker;

$factory->define(city::class, function (Faker $faker) {

    return [
        'value' => $faker->word,
        'name' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
