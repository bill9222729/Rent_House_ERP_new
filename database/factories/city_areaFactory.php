<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\city_area;
use Faker\Generator as Faker;

$factory->define(city_area::class, function (Faker $faker) {

    return [
        'city_value' => $faker->word,
        'value' => $faker->word,
        'name' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
