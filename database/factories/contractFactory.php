<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\contract;
use Faker\Generator as Faker;

$factory->define(contract::class, function (Faker $faker) {

    return [
        'contract_number_of_periods' => $faker->word,
        'contract_category' => $faker->word,
        'contract_business_sign_back' => $faker->word,
        'contract_match_date' => $faker->word,
        'landlord_match_number' => $faker->word,
        'landlord_existing_tenant' => $faker->word,
        'landlord_contract_date' => $faker->word,
        'landlord_contract_expiry_date' => $faker->word,
        'landlord_item_number' => $faker->word,
        'landlord_ministry_of_the_interior_number' => $faker->word,
        'landlord_types_of' => $faker->word,
        'tenant_case_number' => $faker->word,
        'tenant_ministry_of_the_interior_number' => $faker->word,
        'rent_evaluation_form_market_rent' => $faker->word,
        'rent_evaluation_form_assess_rent' => $faker->word,
        'actual_contract_rent_to_landlord' => $faker->word,
        'actual_contract_tenant_pays_rent' => $faker->word,
        'actual_contract_rent_subsidy' => $faker->word,
        'actual_contract_deposit' => $faker->word,
        'to_the_landlord_rent_day' => $faker->word,
        'package_escrow_fee_month_minute' => $faker->word,
        'package_escrow_fee_amount' => $faker->word,
        'package_escrow_fee_number_of_periods' => $faker->word,
        'rent_subsidy_month_minute' => $faker->word,
        'rent_subsidy_amount' => $faker->word,
        'rent_subsidy_number_of_periods' => $faker->word,
        'development_matching_fees_month' => $faker->word,
        'development_matching_fees_amount' => $faker->word,
        'notary_fees_notarization_date' => $faker->word,
        'notary_fees_month' => $faker->word,
        'notary_fees_amount' => $faker->word,
        'repair_cost_month_minute' => $faker->word,
        'repair_cost_amount' => $faker->word,
        'insurance_month_minute' => $faker->word,
        'insurance_amount' => $faker->word,
        'insurance_period_three_year' => $faker->word,
        'insurance_period_three_start_and_end_date' => $faker->word,
        'remark_1' => $faker->word,
        'remark_2' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
