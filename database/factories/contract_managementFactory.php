<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\contract_management;
use Faker\Generator as Faker;

$factory->define(contract_management::class, function (Faker $faker) {

    return [
        'case_status' => $faker->word,
        'signing_date' => $faker->word,
        'other_landlord_desc' => $faker->word,
        'lease_start_date' => $faker->word,
        'lease_end_date' => $faker->word,
        'rent' => $faker->randomDigitNotNull,
        'rent_pay_type' => $faker->word,
        'bank_name' => $faker->word,
        'bank_branch_code' => $faker->word,
        'bank_owner_name' => $faker->word,
        'bank_account' => $faker->word,
        'deposit_amount' => $faker->randomDigitNotNull,
        'contract_note' => $faker->word,
        'apply_man_id' => $faker->randomDigitNotNull,
        'remark' => $faker->word,
        'file_path' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
