<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\contracts_new;
use Faker\Generator as Faker;

$factory->define(contracts_new::class, function (Faker $faker) {

    return [
        'building_case_id' => $faker->word,
        'tenant_case_id' => $faker->word,
        'lease_start_date' => $faker->word,
        'lease_end_date' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'charter_case_id' => $faker->word,
        'contract_type' => $faker->word,
        'match_id' => $faker->word
    ];
});
