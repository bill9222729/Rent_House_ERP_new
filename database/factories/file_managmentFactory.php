<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\file_managment;
use Faker\Generator as Faker;

$factory->define(file_managment::class, function (Faker $faker) {

    return [
        'file_origin_name' => $faker->word,
        'file_server_name' => $faker->word,
        'file_size' => $faker->randomDigitNotNull,
        'file_source' => $faker->word,
        'file_source_id' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
