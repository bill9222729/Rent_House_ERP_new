<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\fix_detail_management;
use Faker\Generator as Faker;

$factory->define(fix_detail_management::class, function (Faker $faker) {

    return [
        'report_date' => $faker->word,
        'fix_id' => $faker->randomDigitNotNull,
        'repair_item' => $faker->word,
        'repair_manufacturers' => $faker->word,
        'repair_records' => $faker->word,
        'meeting_date' => $faker->word,
        'meeting_master' => $faker->word,
        'fix_date' => $faker->word,
        'completion_date' => $faker->word,
        'please_amount' => $faker->randomDigitNotNull,
        'reply_amount' => $faker->randomDigitNotNull,
        'monthly_application_month' => $faker->word,
        'balance' => $faker->randomDigitNotNull,
        'note' => $faker->word,
        'no_reference' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
