<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\fix_management;
use Faker\Generator as Faker;

$factory->define(fix_management::class, function (Faker $faker) {

    return [
        'match_id' => $faker->word,
        'building_case_no' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
