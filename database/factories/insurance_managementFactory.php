<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\insurance_management;
use Faker\Generator as Faker;

$factory->define(insurance_management::class, function (Faker $faker) {

    return [
        'sales_id' => $faker->randomDigitNotNull,
        'match_id' => $faker->word,
        'building_id' => $faker->word,
        'insurance_premium' => $faker->randomDigitNotNull,
        'insurer' => $faker->word,
        'insurance_start' => $faker->word,
        'insurance_end' => $faker->word,
        'monthly_application_month' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
