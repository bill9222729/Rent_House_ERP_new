<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\match_management;
use Faker\Generator as Faker;

$factory->define(match_management::class, function (Faker $faker) {

    return [
        'match_id' => $faker->word,
        'building_case_id' => $faker->word,
        'tenant_case_id' => $faker->word,
        'lease_start_date' => $faker->word,
        'lease_end_date' => $faker->word,
        'charter_case_id' => $faker->word,
        'contract_type' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
