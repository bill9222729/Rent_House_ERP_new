<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\salesman_management;
use Faker\Generator as Faker;

$factory->define(salesman_management::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'id_num' => $faker->word,
        'address' => $faker->word,
        'phone' => $faker->word,
        'birthday' => $faker->word,
        'bank_acc' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
