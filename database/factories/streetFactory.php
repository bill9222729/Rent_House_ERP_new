<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\street;
use Faker\Generator as Faker;

$factory->define(street::class, function (Faker $faker) {

    return [
        'city_area_value' => $faker->word,
        'value' => $faker->word,
        'name' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
