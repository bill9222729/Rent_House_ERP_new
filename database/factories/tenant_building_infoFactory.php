<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\tenant_building_info;
use Faker\Generator as Faker;

$factory->define(tenant_building_info::class, function (Faker $faker) {

    return [
        'tenant_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'located_city' => $faker->word,
        'located_Twn_spcode_area' => $faker->word,
        'located_lot_area' => $faker->word,
        'located_landno' => $faker->word,
        'building_no' => $faker->word,
        'building_total_square_meter_percent' => $faker->word,
        'building_total_square_meter' => $faker->word,
        'addr_cntcode' => $faker->word,
        'addr_twnspcode' => $faker->word,
        'addr_street_area' => $faker->word,
        'addr_lane' => $faker->word,
        'addr_alley' => $faker->word,
        'addr_no' => $faker->word,
        'is_household' => $faker->randomDigitNotNull,
        'filers' => $faker->randomDigitNotNull,
        'last_modified_person' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
