<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\tenant_family_info;
use Faker\Generator as Faker;

$factory->define(tenant_family_info::class, function (Faker $faker) {

    return [
        'tenant_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'is_foreigner' => $faker->randomDigitNotNull,
        'id_no' => $faker->word,
        'appellation' => $faker->word,
        'gender' => $faker->word,
        'condition_low_income_households' => $faker->randomDigitNotNull,
        'condition_low_and_middle_income_households' => $faker->randomDigitNotNull,
        'condition_special_circumstances_families' => $faker->randomDigitNotNull,
        'condition_over_sixty_five' => $faker->randomDigitNotNull,
        'condition_domestic_violence' => $faker->randomDigitNotNull,
        'condition_disabled' => $faker->randomDigitNotNull,
        'condition_disabled_type' => $faker->randomDigitNotNull,
        'condition_disabled_level' => $faker->word,
        'condition_aids' => $faker->randomDigitNotNull,
        'condition_aboriginal' => $faker->randomDigitNotNull,
        'condition_disaster_victims' => $faker->randomDigitNotNull,
        'condition_vagabond' => $faker->randomDigitNotNull,
        'condition_Applicant_naturalization' => $faker->randomDigitNotNull,
        'condition_Applicant_not_naturalization' => $faker->randomDigitNotNull,
        'condition_Applicant_not_naturalization_and_study' => $faker->randomDigitNotNull,
        'condition_police_officer' => $faker->randomDigitNotNull,
        'filers' => $faker->randomDigitNotNull,
        'last_modified_person' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
