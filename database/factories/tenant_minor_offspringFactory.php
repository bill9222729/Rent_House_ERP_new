<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\tenant_minor_offspring;
use Faker\Generator as Faker;

$factory->define(tenant_minor_offspring::class, function (Faker $faker) {

    return [
        'tenant_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'is_foreigner' => $faker->randomDigitNotNull,
        'appellation' => $faker->word,
        'id_no' => $faker->word,
        'birthday' => $faker->word,
        'filers' => $faker->randomDigitNotNull,
        'last_modified_person' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
