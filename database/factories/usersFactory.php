<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\users;
use Faker\Generator as Faker;

$factory->define(users::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'email' => $faker->word,
        'passport_english_name' => $faker->word,
        'english_name' => $faker->word,
        'birthday' => $faker->word,
        'id_no' => $faker->word,
        'residence_city' => $faker->word,
        'residence_city_area' => $faker->word,
        'residenceresidence_addr_addr' => $faker->word,
        'mailing_city' => $faker->word,
        'mailing_city_area' => $faker->word,
        'mailing_addr' => $faker->word,
        'tel' => $faker->word,
        'cellphone' => $faker->word,
        'company_cellphone' => $faker->word,
        'bank_name' => $faker->word,
        'bank_branch_code' => $faker->word,
        'bank_owner_name' => $faker->word,
        'bank_account' => $faker->word,
        'email_verified_at' => $faker->date('Y-m-d H:i:s'),
        'password' => $faker->word,
        'role' => $faker->word,
        'remember_token' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'stoken' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
