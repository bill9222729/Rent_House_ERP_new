<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandlordManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landlord_management', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('id_num')->unique();
            $table->string('address');
            $table->string('phone');
            $table->date('birthday');
            $table->string('bank_acc');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landlord_management');
    }
}
