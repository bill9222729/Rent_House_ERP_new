<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildingManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_management', function (Blueprint $table) {
            $table->id();
            $table->date('apply_date')->comment("");
            $table->boolean('charter')->default(false)->comment("包租");
            $table->boolean('escrow')->default(false)->comment("代管");
            $table->integer('request_form_type')->nullable()->comment("申請單類型");
            $table->string('address_city')->nullable()->comment("地址-縣市");
            $table->string('address_cityarea')->nullable()->comment("地址-鄉鎮區");
            $table->string('address_street')->nullable()->comment("地址-街道");
            $table->string('address_ln')->nullable()->comment("地址-巷");
            $table->string('address_aly')->nullable()->comment("地址-弄");
            $table->string('address_num')->nullable()->comment("地址-號");
            $table->string('address_num_hyphen')->nullable()->comment("地址-之O");
            $table->string('located_city')->nullable()->comment("建物坐落-縣市");
            $table->string('located_cityarea')->nullable()->comment("建物坐落-鄉鎮區");
            $table->string('located_segment_num')->nullable()->comment("建物坐落-段號");
            $table->string('located_small_lot')->nullable()->comment("建物坐落-地段小段");
            $table->string('located_land_num')->nullable()->comment("建物坐落-地號");
            $table->string('located_build_num')->nullable()->comment("建物坐落-建號");
            $table->string('pattern')->nullable()->comment("格局");
            $table->text('pattern_remark')->nullable()->comment("格局備註");
            $table->string('h_info_type')->nullable()->comment("房屋資訊-類型");
            $table->string('h_info_age')->nullable()->comment("房屋資訊-屋齡");
            $table->date('h_info_completion_date')->nullable()->comment("房屋資訊-完工日期");
            $table->string('h_info_fl')->nullable()->comment("房屋資訊-樓層");
            $table->string('h_info_hyphen')->nullable()->comment("房屋資訊-樓層之O");
            $table->string('h_info_suite')->nullable()->comment("房屋資訊-室/房號");
            $table->string('h_info_total_fl')->nullable()->comment("房屋資訊-總層數");
            $table->string('h_info_pattern_room')->nullable()->comment("房屋資訊-隔間O房");
            $table->string('h_info_pattern_hall')->nullable()->comment("房屋資訊-隔間O廳");
            $table->string('h_info_pattern_bath')->nullable()->comment("房屋資訊-隔間O衛");
            $table->string('h_info_pattern_material')->nullable()->comment("房屋資訊-隔間材質");
            $table->string('h_info_warrant_ping_num')->nullable()->comment("房屋資訊-權狀坪數(平方公尺)");
            $table->string('h_info_actual_ping_num')->nullable()->comment("房屋資訊-實際坪數(平方公尺)");
            $table->string('h_info_usage')->nullable()->comment("房屋資訊-建物主要用途");
            $table->string('h_info_material')->nullable()->comment("房屋資訊-建材");
            $table->string('landlord_expect_rent')->nullable()->comment("房東期待-租金");
            $table->string('landlord_expect_deposit_month')->nullable()->comment("房東期待-押金(月)");
            $table->string('landlord_expect_deposit')->nullable()->comment("房東期待-押金(元)");
            $table->boolean('landlord_expect_bargain')->default(false)->comment("可議價");
            $table->boolean('manage_fee')->default(false)->comment("包含管理費");
            $table->string('manage_fee_month')->nullable()->comment("管理費(每月)");
            $table->string('manage_fee_ping')->nullable()->comment("管理費(每坪)");
            $table->boolean('contain_fee_ele')->default(false)->comment("包含費用-電");
            $table->boolean('contain_fee_water')->default(false)->comment("包含費用-水");
            $table->boolean('contain_fee_gas')->default(false)->comment("包含費用-瓦斯");
            $table->boolean('contain_fee_clean')->default(false)->comment("包含費用-清潔");
            $table->boolean('contain_fee_pay_tv')->default(false)->comment("包含費用-第四台");
            $table->boolean('contain_fee_net')->default(false)->comment("包含費用-網路");
            $table->boolean('equipment_tv')->default(false)->comment("提供設備-電視");
            $table->boolean('equipment_refrigerator')->default(false)->comment("提供設備-冰箱");
            $table->boolean('equipment_pay_tv')->default(false)->comment("提供設備-有線電視");
            $table->boolean('equipment_air_conditioner')->default(false)->comment("提供設備-冷氣");
            $table->boolean('equipment_water_heater')->default(false)->comment("提供設備-熱水器");
            $table->boolean('equipment_net')->default(false)->comment("提供設備-網路");
            $table->boolean('equipment_washer')->default(false)->comment("提供設備-洗衣機");
            $table->boolean('equipment_gas')->default(false)->comment("提供設備-天然瓦斯");
            $table->boolean('equipment_bed')->default(false)->comment("提供設備-床");
            $table->boolean('equipment_wardrobe')->default(false)->comment("提供設備-衣櫃");
            $table->boolean('equipment_table')->default(false)->comment("提供設備-桌");
            $table->boolean('equipment_chair')->default(false)->comment("提供設備-椅");
            $table->boolean('equipment_sofa')->default(false)->comment("提供設備-沙發");
            $table->boolean('equipment_other')->default(false)->comment("提供設備-其他");
            $table->text('equipment_other_detail')->nullable()->comment("其他設備描述");
            $table->boolean('can_Cook')->default(false)->comment("可炊煮");
            $table->boolean('parking')->default(false)->comment("附車位");
            $table->boolean('Barrier_free_facility')->default(false)->comment("無障礙設施");
            $table->boolean('curfew')->default(false)->comment("有無門禁管理");
            $table->boolean('curfew_management')->default(false)->comment("有無管理員");
            $table->boolean('curfew_card')->default(false)->comment("刷卡門禁");
            $table->boolean('curfew_other')->default(false)->comment("其他門禁");
            $table->text('curfew_other_detail')->nullable()->comment("其他門禁描述");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_management');
    }
}
