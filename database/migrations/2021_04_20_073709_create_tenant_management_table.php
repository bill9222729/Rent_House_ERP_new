<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_management', function (Blueprint $table) {
            $table->id();
            $table->date('apply_date')->comment("申請日期");
            $table->boolean('charter')->default(false)->comment("包租");
            $table->boolean('escrow')->default(false)->comment("代管");
            $table->string('lessee_name')->nullable()->comment("承租人-姓名");
            $table->string('lessee_gender')->nullable()->comment("承租人-姓別");
            $table->date('lessee_birthday')->nullable()->comment("承租人-出生年月日");
            $table->string('lessee_id_num')->nullable()->comment("承租人-身分證字號");
            $table->string('lessee_hc_num')->nullable()->comment("承租人-戶口名簿戶號");
            $table->string('lessee_telephone_d')->nullable()->comment("承租人-電話(日)");
            $table->string('lessee_telephone_n')->nullable()->comment("承租人-電話(夜)");
            $table->string('lessee_cellphone')->nullable()->comment("承租人-手機");
            $table->string('lessee_email')->nullable()->comment("承租人-電子信箱");
            $table->string('qualifications')->nullable()->comment("承租人及其家庭成員資格");
            $table->string('hr_city')->nullable()->comment("戶籍-縣市");
            $table->string('hr_cityarea')->nullable()->comment("戶籍-鄉鎮區");
            $table->string('hr_address')->nullable()->comment("戶籍-地址");
            $table->string('contact_city')->nullable()->comment("通訊-縣市");
            $table->string('contact_cityarea')->nullable()->comment("通訊-鄉鎮區");
            $table->string('contact_address')->nullable()->comment("通訊-地址");
            $table->boolean('receive_subsidy')->default(false)->comment("承租人及家庭成員其是否領有政府最近年度核發之租金補貼核定函或承租右列住宅");
            $table->string('subsidy_rent')->nullable()->comment("OOO年度租金補貼");
            $table->string('subsidy_low_income')->nullable()->comment("OOO年度低收入戶及中低收入戶租金補貼");
            $table->string('subsidy_disability')->nullable()->comment("OOO年度身心障礙者房屋租金補貼");
            $table->string('subsidy_decree')->nullable()->comment("OOO年度依其他法令相關租金補貼規定之租金補貼");
            $table->string('lease_nr_sr_city')->nullable()->comment("OO縣市承租政府興建之國民住宅或社會住宅");
            $table->string('legal_agent_num')->nullable()->comment("法定代理人數");
            $table->string('first_agent_name')->nullable()->comment("第一法定代理人-姓名");
            $table->string('first_agent_telephone')->nullable()->comment("第一法定代理人-電話");
            $table->string('first_agent_cellphone')->nullable()->comment("第一法定代理人-手機");
            $table->string('first_agent_address')->nullable()->comment("第一法定代理人-地址");
            $table->string('second_agent_name')->nullable()->comment("第二法定代理人-姓名");
            $table->string('second_agent_telephone')->nullable()->comment("第二法定代理人-電話");
            $table->string('second_agent_cellphone')->nullable()->comment("第二法定代理人-手機");
            $table->string('second_agent_address')->nullable()->comment("第二法定代理人-地址");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_management');
    }
}
