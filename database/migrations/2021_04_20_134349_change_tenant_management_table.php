<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTenantManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->dropColumn("charter");
            $table->dropColumn("escrow");
            $table->string("re_type")->comment("本人欲提供住宅出租予租屋服務事業再轉租類型");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            //
        });
    }
}
