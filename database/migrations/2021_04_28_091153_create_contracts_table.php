<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contract_number_of_periods')->nullable();
            $table->string('contract_category')->nullable();
            $table->string('contract_business_sign_back')->nullable();
            $table->string('contract_match_date')->nullable();
            $table->string('landlord_match_number')->nullable();
            $table->string('landlord_existing_tenant')->nullable();
            $table->string('landlord_contract_date')->nullable();
            $table->string('landlord_contract_expiry_date')->nullable();
            $table->string('landlord_item_number')->nullable();
            $table->string('landlord_ministry_of_the_interior_number')->nullable();
            $table->string('landlord_types_of')->nullable();
            $table->string('tenant_case_number')->nullable();
            $table->string('tenant_ministry_of_the_interior_number')->nullable();
            $table->string('rent_evaluation_form_market_rent')->nullable();
            $table->string('rent_evaluation_form_assess_rent')->nullable();
            $table->string('actual_contract_rent_to_landlord')->nullable();
            $table->string('actual_contract_tenant_pays_rent')->nullable();
            $table->string('actual_contract_rent_subsidy')->nullable();
            $table->string('actual_contract_deposit')->nullable();
            $table->string('to_the_landlord_rent_day')->nullable();
            $table->string('package_escrow_fee_month_minute')->nullable();
            $table->string('package_escrow_fee_amount')->nullable();
            $table->string('package_escrow_fee_number_of_periods')->nullable();
            $table->string('rent_subsidy_month_minute')->nullable();
            $table->string('rent_subsidy_amount')->nullable();
            $table->string('rent_subsidy_number_of_periods')->nullable();
            $table->string('development_matching_fees_month')->nullable();
            $table->string('development_matching_fees_amount')->nullable();
            $table->string('notary_fees_notarization_date')->nullable();
            $table->string('notary_fees_month')->nullable();
            $table->string('notary_fees_amount')->nullable();
            $table->string('repair_cost_month_minute')->nullable();
            $table->string('repair_cost_amount')->nullable();
            $table->string('insurance_month_minute')->nullable();
            $table->string('insurance_amount')->nullable();
            $table->string('insurance_period_three_year')->nullable();
            $table->string('insurance_period_three_start_and_end_date')->nullable();
            $table->string('remark_1')->nullable();
            $table->string('remark_2')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contracts');
    }
}
