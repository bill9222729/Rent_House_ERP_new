<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToTenantManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->string('tenant_case_number')->nullable()->comment("房客_案件編號");
            $table->string('tenant_ministry_of_the_interior_number')->nullable()->comment("房客_內政部編號");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->dropColumn("tenant_case_number");
            $table->dropColumn("tenant_ministry_of_the_interior_number");
        });
    }
}
