<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToLandlordManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landlord_management', function (Blueprint $table) {
            $table->string('landlord_match_number')->nullable()->comment("房客_案件編號");
            $table->string('landlord_existing_tenant')->nullable()->comment("房客_內政部編號");
            $table->string('landlord_contract_date')->nullable()->comment("房客_案件編號");
            $table->string('landlord_contract_expiry_date')->nullable()->comment("房客_內政部編號");
            $table->string('landlord_item_number')->nullable()->comment("房客_案件編號");
            $table->string('landlord_ministry_of_the_interior_number')->nullable()->comment("房客_內政部編號");
            $table->string('landlord_types_of')->nullable()->comment("房客_案件編號");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landlord_management', function (Blueprint $table) {
            $table->dropColumn("landlord_match_number");
            $table->dropColumn("landlord_existing_tenant");
            $table->dropColumn("landlord_contract_date");
            $table->dropColumn("landlord_contract_expiry_date");
            $table->dropColumn("landlord_item_number");
            $table->dropColumn("landlord_ministry_of_the_interior_number");
            $table->dropColumn("landlord_types_of");
        });
    }
}
