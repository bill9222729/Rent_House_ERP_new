<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeLandlordManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landlord_management', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('id_num')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->date('birthday')->nullable()->change();
            $table->string('bank_acc')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landlord_management', function (Blueprint $table) {
            $table->string('name')->change();
            $table->string('id_num')->unique()->change();
            $table->string('address')->change();
            $table->string('phone')->change();
            $table->date('birthday')->change();
            $table->string('bank_acc')->change();
        });
    }
}
