<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColBuildingManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->string('status')->nullable()->comment("物件狀態");
            $table->string('num')->nullable()->comment("物件編號");
            $table->string('Lessor_name')->nullable()->comment("出租人姓名");
            $table->string('Lessor_gender')->nullable()->comment("性別");
            $table->string('Lessor_birthday')->nullable()->comment("出生年月日");
            $table->string('Lessor_ID_num')->nullable()->comment("身分證字號");
            $table->string('Lessor_phone')->nullable()->comment("連絡電話");
            $table->string('residence_address_city')->nullable()->comment("戶籍地址-縣市");
            $table->string('residence_address_cityarea')->nullable()->comment("戶籍地址-鄉鎮區");
            $table->string('residence_address_street')->nullable()->comment("戶籍地址-街道");
            $table->string('residence_address_ln')->nullable()->comment("戶籍地址-巷");
            $table->string('residence_address_aly')->nullable()->comment("戶籍地址-弄");
            $table->string('residence_address_num')->nullable()->comment("戶籍地址-號");
            $table->string('residence_address_num_hyphen')->nullable()->comment("戶籍地址-之O");
            $table->string('collection_agent')->nullable()->comment("收款代理人");
            $table->string('agent_ID_num')->nullable()->comment("代理人身分證");
            $table->string('agent_bank_name')->nullable()->comment("銀行名稱");
            $table->string('agent_branch_name')->nullable()->comment("分行名稱");
            $table->string('landlord_bank_account')->nullable()->comment("房東銀行帳號");
            $table->string('remark_1')->nullable()->comment("備註1");
            $table->string('remark_2')->nullable()->comment("備註2");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('num');
            $table->dropColumn('Lessor_name');
            $table->dropColumn('Lessor_gender');
            $table->dropColumn('Lessor_birthday');
            $table->dropColumn('Lessor_ID_num');
            $table->dropColumn('Lessor_phone');
            $table->dropColumn('residence_address_city');
            $table->dropColumn('residence_address_cityarea');
            $table->dropColumn('residence_address_street');
            $table->dropColumn('residence_address_ln');
            $table->dropColumn('residence_address_aly');
            $table->dropColumn('residence_address_num');
            $table->dropColumn('residence_address_num_hyphen');
            $table->dropColumn('collection_agent');
            $table->dropColumn('agent_ID_num');
            $table->dropColumn('agent_bank_name');
            $table->dropColumn('agent_branch_name');
            $table->dropColumn('landlord_bank_account');
            $table->dropColumn('remark_1');
            $table->dropColumn('remark_2');
        });
    }
}
