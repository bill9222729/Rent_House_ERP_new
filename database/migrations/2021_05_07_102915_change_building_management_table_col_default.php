<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBuildingManagementTableColDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->date("apply_date")->nullable()->change();
            $table->string("re_type")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->date("apply_date")->change();
            $table->string("re_type")->change();
        });
    }
}
