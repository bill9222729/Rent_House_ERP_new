<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColToTenantManagmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->string("status")->nullable()->comment("房客狀態");
            $table->string("remark_1")->nullable()->comment("備註1");
            $table->string("remark_2")->nullable()->comment("備註2");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->dropColumn("status");
            $table->dropColumn("remark_1");
            $table->dropColumn("remark_2");
        });
    }
}
