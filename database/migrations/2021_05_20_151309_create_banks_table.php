<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_code')->comment("收受行代號");
            $table->string('bank_account')->comment("收受者帳號");
            $table->string('tax')->comment("收受者統編");
            $table->integer('price')->comment("金額");
            $table->string('user_number')->comment("用戶號碼");
            $table->string('company_stack_code')->nullable()->comment("公司股市代號");
            $table->string('Creator')->nullable()->comment("發動者專用區");
            $table->string('search')->nullable()->comment("查詢專用區");
            $table->string('bank_noted')->nullable()->comment("存摺摘要");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banks');
    }
}
