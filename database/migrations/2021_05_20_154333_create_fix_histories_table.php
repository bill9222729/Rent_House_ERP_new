<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixHistoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fix_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales')->comment("業務姓名");
            $table->date('pay_date')->comment("請款月報");
            $table->string('item_code')->comment("物件編號");
            $table->string('item_address')->comment("物件地址");
            $table->integer('pay_year')->comment("請款年度");
            $table->string('fix_vendor')->nullable()->comment("修繕廠商");
            $table->string('call_fix_date')->comment("報修日");
            $table->string('item_owner')->comment("房東姓名");
            $table->string('owner_phone')->comment("房東電話");
            $table->string('client_name')->comment("房客姓名");
            $table->string('client_phone')->comment("房客電話");
            $table->string('fix_item')->comment("修繕項目");
            $table->string('fix_source')->nullable()->comment("損壞原因");
            $table->string('check_date')->comment("會勘日期");
            $table->string('fix_date')->comment("修繕日期");
            $table->date('fix_done')->comment("完成日期");
            $table->string('fix_record')->comment("修繕紀錄");
            $table->integer('fix_price')->comment("修繕金額");
            $table->integer('receipt')->comment("收據");
            $table->integer('final_price')->comment("本次請領金額");
            $table->integer('balance')->comment("餘額");
            $table->date('owner_start_date')->comment("包租租約開始");
            $table->date('owner_end_date')->comment("包租租約結束");
            $table->date('change_start_date')->comment("轉租租約開始");
            $table->date('change_end_date')->comment("轉租租約結束");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fix_histories');
    }
}
