<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReturnFormatsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_formats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('t_id')->comment("交易序號");
            $table->string('t_code')->nullable()->comment("交易代號");
            $table->string('tax')->comment("收受者統一編號");
            $table->string('bank_account')->comment("收受者帳號");
            $table->string('user_name')->nullable()->comment("用戶姓名");
            $table->integer('t_price')->comment("交易金額");
            $table->string('t_status')->comment("交易結果");
            $table->string('name')->nullable()->comment("姓名");
            $table->string('item_address')->comment("物件地址");
            $table->integer('item_month_price')->comment("租金");
            $table->integer('item_price_support')->nullable()->comment("租金補助");
            $table->integer('watch_price')->nullable()->comment("管理費");
            $table->integer('parking_price')->nullable()->comment("停車費");
            $table->integer('fix_price')->nullable()->comment("修繕費");
            $table->integer('t_cost')->nullable()->comment("扣手續費");
            $table->integer('other_price')->nullable()->comment("其他");
            $table->integer('total_price')->comment("小計");
            $table->text('note')->nullable()->comment("備註");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('return_formats');
    }
}
