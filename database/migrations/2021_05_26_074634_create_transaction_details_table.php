<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->string('account')->nullable()->comment("帳號");
            $table->date('t_date')->nullable()->comment("交易日期");
            $table->dateTime('t_time')->nullable()->comment("交易時間");
            $table->date('a_date')->nullable()->comment("帳務日期");
            $table->string('detail')->nullable()->comment("交易說明");
            $table->integer('expense')->nullable()->comment("支出金額");
            $table->integer('income')->nullable()->comment("存入金額");
            $table->integer('balance')->nullable()->comment("餘額");
            $table->string('remark_1')->nullable()->comment("備註1");
            $table->string('remark_2')->nullable()->comment("備註2");
            $table->string('remark_3')->nullable()->comment("備註2");
            $table->string('ticket_num')->nullable()->comment("票據號碼");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
