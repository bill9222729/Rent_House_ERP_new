<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBankColToTenantManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->string('bank_name')->nullable()->comment('銀行名稱');
            $table->string('bank_code')->nullable()->comment('銀行代號');
            $table->string('bank_account')->nullable()->comment('銀行帳號');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->dropColumn('bank_name');
            $table->dropColumn('bank_code');
            $table->dropColumn('bank_account');
        });
    }
}
