<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChandeColToNullableForBuilding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->string('address_num')->nullable()->change();
            $table->string('address_num_hyphen')->nullable()->change();
            $table->string('residence_address_num')->nullable()->change();
            $table->string('residence_address_num_hyphen')->nullable()->change();
            $table->boolean('landlord_expect_bargain')->nullable()->change();
            $table->string('located_small_lot')->nullable()->change();
            $table->string('pattern_remark')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->string('address_num')->change();
            $table->string('address_num_hyphen')->change();
            $table->string('residence_address_num')->change();
            $table->string('residence_address_num_hyphen')->change();
            $table->boolean('landlord_expect_bargain')->change();
            $table->string('located_small_lot')->change();
            $table->string('pattern_remark')->change();
        });
    }
}
