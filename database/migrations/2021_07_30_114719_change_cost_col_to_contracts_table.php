<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCostColToContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('package_escrow_fee_month_minute')->nullable()->change();
            $table->string('package_escrow_fee_amount')->nullable()->change();
            $table->string('package_escrow_fee_number_of_periods')->nullable()->change();
            $table->string('rent_subsidy_month_minute')->nullable()->change();
            $table->string('rent_subsidy_amount')->nullable()->change();
            $table->string('rent_subsidy_number_of_periods')->nullable()->change();
            $table->string('development_matching_fees_month')->nullable()->change();
            $table->string('development_matching_fees_amount')->nullable()->change();
            $table->string('notary_fees_notarization_date')->nullable()->change();
            $table->string('notary_fees_month')->nullable()->change();
            $table->string('notary_fees_amount')->nullable()->change();
            $table->string('repair_cost_month_minute')->nullable()->change();
            $table->string('repair_cost_amount')->nullable()->change();
            $table->string('insurance_month_minute')->nullable()->change();
            $table->string('insurance_amount')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('package_escrow_fee_month_minute')->change();
            $table->string('package_escrow_fee_amount')->change();
            $table->string('package_escrow_fee_number_of_periods')->change();
            $table->string('rent_subsidy_month_minute')->change();
            $table->string('rent_subsidy_amount')->change();
            $table->string('rent_subsidy_number_of_periods')->change();
            $table->string('development_matching_fees_month')->change();
            $table->string('development_matching_fees_amount')->change();
            $table->string('notary_fees_notarization_date')->change();
            $table->string('notary_fees_month')->change();
            $table->string('notary_fees_amount')->change();
            $table->string('repair_cost_month_minute')->change();
            $table->string('repair_cost_amount')->change();
            $table->string('insurance_month_minute')->change();
            $table->string('insurance_amount')->change();
        });
    }
}
