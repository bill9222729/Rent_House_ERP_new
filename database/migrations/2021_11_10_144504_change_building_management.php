<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBuildingManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->dropColumn(['re_type']);
            $table->string("sublet")->nullable()->comment("本人欲提供住宅出租予租屋服務事業再轉租(包租):");
            $table->string("rent")->nullable()->comment("本人欲提供住宅經由租屋服務事業協助出租(代管):");
            $table->string("notarization")->nullable()->comment("公證");
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management', function (Blueprint $table) {
        });
    }
}
