<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBusinessManagements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_managements', function (Blueprint $table) {
            $table->string("english_name")->nullable()->comment("英文名字");
            $table->string("due_date")->nullable()->comment("到值日期");
            $table->string("r_address")->nullable()->comment("戶籍地址");
            $table->string("cellphone")->nullable()->comment("手機");
            $table->string("labor")->nullable()->comment("勞保退休金額");
            $table->string("f_name")->nullable()->comment("眷屬姓名");
            $table->string("f_title")->nullable()->comment("眷屬稱謂");
            $table->string("f_identity")->nullable()->comment("眷屬身分證字號");
            $table->string("f_birthday")->nullable()->comment("眷屬出生年月日");
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_managements', function (Blueprint $table) {
           
            
        });
    }
}
