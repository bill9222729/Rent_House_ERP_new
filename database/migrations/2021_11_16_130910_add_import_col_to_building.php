<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImportColToBuilding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->text('collection_agent_address')->nullable()->comment('代理人地址');
            $table->text('collection_agent_phone')->nullable()->comment('代理人電話');
            $table->text('sales')->nullable()->comment('業務');
            $table->text('v_account')->nullable()->comment('虛擬帳號');
            $table->text('empty_house')->nullable()->comment('空屋率');
            $table->text('source')->nullable()->comment('來源');
            $table->text('fire_alert')->nullable()->comment('火災警報器');
            $table->text('fire_alert_in_house')->nullable()->comment('屋內滅火器');
            $table->text('notary')->nullable()->comment('公證');
            $table->text('n_name')->nullable()->comment('社區名稱');
            $table->text('car_position')->nullable()->comment('車位');
            $table->text('p_code')->nullable()->comment('內政部媒合編號');
            $table->text('email')->nullable()->comment('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->dropColumn('collection_agent_address');
            $table->dropColumn('collection_agent_phone');
            $table->dropColumn('sales');
            $table->dropColumn('v_account');
            $table->dropColumn('empty_house');
            $table->dropColumn('source');
            $table->dropColumn('fire_alert');
            $table->dropColumn('fire_alert_in_house');
            $table->dropColumn('notary');
            $table->dropColumn('n_name');
            $table->dropColumn('car_position');
            $table->dropColumn('p_code');
            $table->dropColumn('email');
        });
    }
}
