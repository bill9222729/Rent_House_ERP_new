<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImportColToTenantManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->date('pay_date')->nullable()->comment('繳款日');
            $table->string('sales')->nullable()->comment('業務');
            $table->string('weak_item')->nullable()->comment('弱勢項目');
            $table->string('res')->nullable()->comment('原因');
            $table->string('tp')->nullable()->comment('租補');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management', function (Blueprint $table) {
            $table->dropColumn('pay_date');
            $table->dropColumn('sales');
            $table->dropColumn('weak_item');
            $table->dropColumn('res');
            $table->dropColumn('tp');
        });
    }
}
