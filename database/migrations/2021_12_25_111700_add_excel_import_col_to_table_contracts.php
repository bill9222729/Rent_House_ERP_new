<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExcelImportColToTableContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('b_id')->nullable()->comment('包租約編號');
            $table->string('tb_id')->nullable()->comment('轉租約編號');
            $table->integer('develop_price')->nullable()->comment('開發費');
            $table->integer('bmanagement_price')->nullable()->comment('包管費');
            $table->integer('search_price')->nullable()->comment('媒合費');
            $table->integer('management_price')->nullable()->comment('代管費');
            $table->string('notary')->nullable()->comment('公證');
            $table->string('real_code')->nullable()->comment('實價登錄編碼');
            $table->string('info_code')->nullable()->comment('資訊提供編碼');
            $table->date('real_date')->nullable()->comment('實價登錄日期');
            $table->date('info_date')->nullable()->comment('資訊提供日期');
            $table->string('recive_bank')->nullable()->comment('收款人銀行名稱');
            $table->string('recive_bank_code')->nullable()->comment('收款人分行代號');
            $table->string('recive_bank_account')->nullable()->comment('收款人帳號');
            $table->string('recive_id')->nullable()->comment('收款人身分證字號');
            $table->string('bank_note')->nullable()->comment('存摺摘要');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('b_id');
            $table->dropColumn('tb_id');
            $table->dropColumn('develop_price');
            $table->dropColumn('bmanagement_price');
            $table->dropColumn('search_price');
            $table->dropColumn('management_price');
            $table->dropColumn('notary');
            $table->dropColumn('real_code');
            $table->dropColumn('info_code');
            $table->dropColumn('real_date');
            $table->dropColumn('info_code');
            $table->dropColumn('recive_bank');
            $table->dropColumn('recive_bank_code');
            $table->dropColumn('recive_bank_account');
            $table->dropColumn('recive_id');
            $table->dropColumn('bank_note');
        });
    }
}
