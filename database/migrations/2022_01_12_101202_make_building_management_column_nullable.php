<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeBuildingManagementColumnNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->boolean('manage_fee')->nullable()->change();
            $table->boolean('contain_fee_ele')->nullable()->change();
            $table->boolean('contain_fee_water')->nullable()->change();
            $table->boolean('contain_fee_gas')->nullable()->change();
            $table->boolean('contain_fee_clean')->nullable()->change();
            $table->boolean('contain_fee_pay_tv')->nullable()->change();
            $table->boolean('contain_fee_net')->nullable()->change();
            $table->boolean('equipment_tv')->nullable()->change();
            $table->boolean('equipment_refrigerator')->nullable()->change();
            $table->boolean('equipment_pay_tv')->nullable()->change();
            $table->boolean('equipment_air_conditioner')->nullable()->change();
            $table->boolean('equipment_water_heater')->nullable()->change();
            $table->boolean('equipment_net')->nullable()->change();
            $table->boolean('equipment_washer')->nullable()->change();
            $table->boolean('equipment_gas')->nullable()->change();
            $table->boolean('equipment_bed')->nullable()->change();
            $table->boolean('equipment_wardrobe')->nullable()->change();
            $table->boolean('equipment_table')->nullable()->change();
            $table->boolean('equipment_chair')->nullable()->change();
            $table->boolean('equipment_sofa')->nullable()->change();
            $table->boolean('equipment_other')->nullable()->change();
            $table->boolean('can_Cook')->nullable()->change();
            $table->boolean('parking')->nullable()->change();
            $table->boolean('Barrier_free_facility')->nullable()->change();
            $table->boolean('curfew')->nullable()->change();
            $table->boolean('curfew_management')->nullable()->change();
            $table->boolean('curfew_card')->nullable()->change();
            $table->boolean('curfew_other')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management', function (Blueprint $table) {
            $table->boolean('manage_fee')->nullable(false)->change();
            $table->boolean('contain_fee_ele')->nullable(false)->change();
            $table->boolean('contain_fee_water')->nullable(false)->change();
            $table->boolean('contain_fee_gas')->nullable(false)->change();
            $table->boolean('contain_fee_clean')->nullable(false)->change();
            $table->boolean('contain_fee_pay_tv')->nullable(false)->change();
            $table->boolean('contain_fee_net')->nullable(false)->change();
            $table->boolean('equipment_tv')->nullable(false)->change();
            $table->boolean('equipment_refrigerator')->nullable(false)->change();
            $table->boolean('equipment_pay_tv')->nullable(false)->change();
            $table->boolean('equipment_air_conditioner')->nullable(false)->change();
            $table->boolean('equipment_water_heater')->nullable(false)->change();
            $table->boolean('equipment_net')->nullable(false)->change();
            $table->boolean('equipment_washer')->nullable(false)->change();
            $table->boolean('equipment_gas')->nullable(false)->change();
            $table->boolean('equipment_bed')->nullable(false)->change();
            $table->boolean('equipment_wardrobe')->nullable(false)->change();
            $table->boolean('equipment_table')->nullable(false)->change();
            $table->boolean('equipment_chair')->nullable(false)->change();
            $table->boolean('equipment_sofa')->nullable(false)->change();
            $table->boolean('equipment_other')->nullable(false)->change();
            $table->boolean('can_Cook')->nullable(false)->change();
            $table->boolean('parking')->nullable(false)->change();
            $table->boolean('Barrier_free_facility')->nullable(false)->change();
            $table->boolean('curfew')->nullable(false)->change();
            $table->boolean('curfew_management')->nullable(false)->change();
            $table->boolean('curfew_card')->nullable(false)->change();
            $table->boolean('curfew_other')->nullable(false)->change();
        });
    }
}
