<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceItemOwnerItemCodeToContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('price')->nullable()->comment('金額');
            $table->string('item_owner')->nullable()->comment('房東姓名');
            $table->string('item_code')->nullable()->comment('物件編號');
            $table->string('insurer')->nullable()->default('旺旺友聯')->comment('保險業者');
            $table->timestamp('insurance_period_three_start_and_end_date_end')->nullable()->comment('保險訖日');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('item_owner');
            $table->dropColumn('item_code');
            $table->dropColumn('insurer');
            $table->dropColumn('insurance_period_three_start_and_end_date_end');
        });
    }
}
