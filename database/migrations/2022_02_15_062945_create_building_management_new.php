<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildingManagementNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_management_new', function (Blueprint $table) {
            $table->id();
            $table->string('building_address_city')->nullable()->comment('建物所在縣市');
            $table->string('building_address_city_area')->nullable()->comment('建物所在區域');
            $table->string('building_address_street')->nullable()->comment('建物所在道路');
            $table->string('building_address_ln')->nullable()->comment('建物所在巷');
            $table->string('building_address_aly')->nullable()->comment('建物所在弄');
            $table->string('building_address_num')->nullable()->comment('建物所在地址的幾號');
            $table->string('building_address_num_hyphen')->nullable()->comment('建物所在地址幾號之幾的之幾');
            $table->string('building_address_floor')->nullable()->comment('建物所在地址的幾樓');
            $table->string('building_address_floor_sub')->nullable()->comment('建物所在地址的幾樓之幾的之幾');
            $table->string('building_address_room_num')->nullable()->comment('建物所在地址的幾樓之幾的幾室');
            $table->string('building_address_building_floor')->nullable()->comment('建物總共有幾樓');
            $table->string('building_address_building_desc')->nullable()->comment('建物描述');
            $table->string('building_located_city')->nullable()->comment('建物坐落縣市');
            $table->string('building_located_city_area')->nullable()->comment('建物坐落區域');
            $table->string('building_located_lot')->nullable()->comment('建物坐落小段');
            $table->string('building_located_land_num')->nullable()->comment('建物坐落段號');
            $table->string('building_num')->nullable()->comment('建號');
            $table->string('building_rent_type')->nullable()->comment('出租類型');
            $table->string('building_pattren')->nullable()->comment('房屋格局');
            $table->string('building_type')->nullable()->comment('房屋類型');
            $table->string('building_age')->nullable()->comment('屋齡');
            $table->string('building_complete_date')->nullable()->comment('建物完工日期');
            $table->string('building_rooms')->nullable()->comment('有幾房');
            $table->string('building_livingrooms')->nullable()->comment('有幾廳');
            $table->string('building_bathrooms')->nullable()->comment('有幾衛');
            $table->string('building_compartment_material')->nullable()->comment('隔間材質');
            $table->string('building_total_square_meter')->nullable()->comment('權狀坪數');
            $table->string('building_use_square_meter')->nullable()->comment('實際使用坪數');
            $table->string('building_materials')->nullable()->comment('建材');
            $table->string('building_rent')->nullable()->comment('房東期待租金');
            $table->string('building_deposit_amount')->nullable()->comment('押金金額');
            $table->string('building_is_including_electricity_fees')->nullable()->comment('是否包含電費');
            $table->string('building_is_cooking')->nullable()->comment('是否可炊煮');
            $table->string('building_is_including_water_fees')->nullable()->comment('是否包含水費');
            $table->string('building_is_including_parking_space')->nullable()->comment('是否包含車位');
            $table->string('building_is_including_accessible_equipment')->nullable()->comment('是否有無障礙設施');
            $table->string('building_is_including_natural_gas')->nullable()->comment('是否包含瓦斯/天然瓦斯');
            $table->string('building_is_including_cable')->nullable()->comment('是否包含第四台');
            $table->string('building_is_including_internet')->nullable()->comment('是否包含網路');
            $table->string('building_is_including_cleaning_fee')->nullable()->comment('是否包含清潔費');
            $table->string('building_offer_tv')->nullable()->comment('是否提供電視');
            $table->string('building_offer_refrigerator')->nullable()->comment('是否提供冰箱');
            $table->string('building_offer_cable')->nullable()->comment('是否提供有線電視(第四台)');
            $table->string('building_offer_air_conditioner')->nullable()->comment('是否有提供冷氣');
            $table->string('building_offer_geyser')->nullable()->comment('是否有提供熱水器');
            $table->string('building_offer_internet')->nullable()->comment('是否有提供網際網路');
            $table->string('building_offer_washing_machine')->nullable()->comment('是否有提供洗衣機');
            $table->string('building_offer_natural_gas')->nullable()->comment('是否有提供天然瓦斯');
            $table->string('building_offer_bed')->nullable()->comment('是否有提供床');
            $table->string('building_offer_wardrobe')->nullable()->comment('是否有提供衣櫃');
            $table->string('building_offer_desk')->nullable()->comment('是否有提供桌子');
            $table->string('building_offer_chair')->nullable()->comment('是否有提供椅子');
            $table->string('building_offer_sofa')->nullable()->comment('是否有提供沙發');
            $table->string('building_offer_other')->nullable()->comment('其他提供設備');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_management_new');
    }
}
