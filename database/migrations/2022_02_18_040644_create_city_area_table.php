<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_area', function (Blueprint $table) {
            $table->id();
            $table->string('city_value')->nullable()->comment('縣市編號');
            $table->string('value')->nullable()->comment('區域編號');
            $table->string('name')->nullable()->comment('區域名稱');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_area');
    }
}
