<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocatedLotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('located_lot', function (Blueprint $table) {
            $table->id();
            $table->string('city_area_value')->nullable()->comment('區域編號');
            $table->string('value')->nullable()->comment('段號');
            $table->string('name')->nullable()->comment('段名');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('located_lot');
    }
}
