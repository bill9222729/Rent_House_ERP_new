<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToBuildingManagementNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->date('apply_date')->nullable()->comment('申請日期');
            $table->text('case_no')->nullable()->comment('案件編號');
            $table->integer('case_type_chartering')->nullable()->comment('本人欲提供住宅出租予租屋服務事業再轉租(包租)');
            $table->integer('case_type_escrow')->nullable()->comment('本人欲提供住宅經由租屋服務事業協助出租(代管)');
            $table->text('case_state')->nullable()->comment('案件狀態');

            $table->text('name')->nullable()->comment('出租人姓名');
            $table->integer('gender')->nullable()->comment('出租人性別');
            $table->date('birthday')->nullable()->comment('出租人生日');
            $table->integer('is_foreigner')->nullable()->comment('出租人是否為外籍人士');
            $table->text('id_no')->nullable()->comment('出租人身分/居留證號');
            $table->text('passport_code')->nullable()->comment('護照號碼');
            $table->text('house_no')->nullable()->comment('戶口名簿號');
            $table->text('tal_day')->nullable()->comment('電話(日)');
            $table->text('tal_night')->nullable()->comment('電話(夜)');
            $table->text('cellphone')->nullable()->comment('手機');
            $table->text('email')->nullable()->comment('信箱');
            $table->text('residence_city')->nullable()->comment('戶籍地址-縣市');
            $table->text('residence_city_area')->nullable()->comment('戶籍地址-區域');
            $table->text('residence_addr')->nullable()->comment('戶籍地址-地址');
            $table->text('mailing_city')->nullable()->comment('通訊地址-縣市');
            $table->text('mailing_city_area')->nullable()->comment('通訊地址-區域');
            $table->text('mailing_addr')->nullable()->comment('通訊地址-地址');
            $table->integer('legal_agent_num')->nullable()->comment('法定代理人人數');
            $table->text('single_proxy_reason')->nullable()->comment('只有一個代理人的原因');
            $table->text('first_legal_agent_name')->nullable()->comment('第一個法定代理人姓名');
            $table->text('first_legal_agent_tel')->nullable()->comment('第一個法定代理人電話');
            $table->text('first_legal_agent_phone')->nullable()->comment('第一個法定代理人手機');
            $table->text('first_legal_agent_addr')->nullable()->comment('第一個法定代理人地址');
            $table->text('second_legal_agent_name')->nullable()->comment('第二個法定代理人姓名');
            $table->text('second_legal_agent_tel')->nullable()->comment('第二個法定代理人電話');
            $table->text('second_legal_agent_phone')->nullable()->comment('第二個法定代理人手機');
            $table->text('second_legal_agent_addr')->nullable()->comment('第二個法定代理人地址');
            $table->text('bank_name')->nullable()->comment('金融機構名稱');
            $table->text('bank_branch_code')->nullable()->comment('分行代碼');
            $table->text('bank_owner_name')->nullable()->comment('金融機構戶名');
            $table->text('bank_account')->nullable()->comment('金融機構帳號');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->dropColumn('apply_date');
            $table->dropColumn('case_no');
            $table->dropColumn('case_type_chartering');
            $table->dropColumn('case_type_escrow');
            $table->dropColumn('case_state');

            $table->dropColumn('name');
            $table->dropColumn('gender');
            $table->dropColumn('birthday');
            $table->dropColumn('is_foreigner');
            $table->dropColumn('id_no');
            $table->dropColumn('passport_code');
            $table->dropColumn('house_no');
            $table->dropColumn('tal_day');
            $table->dropColumn('tal_night');
            $table->dropColumn('cellphone');
            $table->dropColumn('email');
            $table->dropColumn('residence_city');
            $table->dropColumn('residence_city_area');
            $table->dropColumn('residence_addr');
            $table->dropColumn('mailing_city');
            $table->dropColumn('mailing_city_area');
            $table->dropColumn('mailing_addr');
            $table->dropColumn('legal_agent_num');
            $table->dropColumn('single_proxy_reason');
            $table->dropColumn('first_legal_agent_name');
            $table->dropColumn('first_legal_agent_tel');
            $table->dropColumn('first_legal_agent_phone');
            $table->dropColumn('first_legal_agent_addr');
            $table->dropColumn('second_legal_agent_name');
            $table->dropColumn('second_legal_agent_tel');
            $table->dropColumn('second_legal_agent_phone');
            $table->dropColumn('second_legal_agent_addr');
            $table->dropColumn('bank_name');
            $table->dropColumn('bank_branch_code');
            $table->dropColumn('bank_owner_name');
            $table->dropColumn('bank_account');
        });
    }
}
