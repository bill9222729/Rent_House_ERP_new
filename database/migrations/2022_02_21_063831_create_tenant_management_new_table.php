<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantManagementNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_management_new', function (Blueprint $table) {
            $table->id();
            $table->date('apply_date')->nullable()->comment('申請日期');
            $table->string('case_no')->nullable()->comment('案件編號');
            $table->integer('case_type_chartering')->nullable()->comment('本人欲承租-租屋服務事業轉租之住宅(包租)');
            $table->integer('case_type_escrow')->nullable()->comment('本人欲承租-經由租屋服務事業協助出租(代管)');
            $table->string('case_status')->nullable()->comment('案件狀態');
            $table->string('name')->nullable()->comment('姓名');
            $table->string('gender')->nullable()->comment('性別');
            $table->date('birthday')->nullable()->comment('生日');
            $table->string('id_no')->nullable()->comment('身分證字號');
            $table->string('house_no')->nullable()->comment('戶口名簿號');
            $table->string('tel_day')->nullable()->comment('電話(日)');
            $table->string('tel_night')->nullable()->comment('電話(夜)');
            $table->string('cellphone')->nullable()->comment('手機');
            $table->string('email')->nullable()->comment('信箱');
            $table->integer('membtype')->nullable()->comment('承租人及其家庭成員資格');
            $table->string('residence_city')->nullable()->comment('戶籍地址-縣市');
            $table->string('residence_city_area')->nullable()->comment('戶籍地址-區域');
            $table->string('residence_addr')->nullable()->comment('戶籍地址-地址');
            $table->string('mailing_city')->nullable()->comment('通訊地址-縣市');
            $table->string('mailing_city_area')->nullable()->comment('通訊地址-區域');
            $table->string('mailing_addr')->nullable()->comment('通訊地址-地址');
            $table->integer('is_have_other_subsidy')->nullable()->comment('承租人及家庭成員其是否領有政府最近年度核發之租金補貼核定函或承租右列住宅');
            $table->integer('isOtherSubsidy01')->nullable()->comment('是否有年度租金補貼');
            $table->integer('isOtherSubsidy01Info')->nullable()->comment('年度租金補貼金額');
            $table->integer('isOtherSubsidy02')->nullable()->comment('是否有年度低收入戶及中低收入戶租金補貼');
            $table->integer('isOtherSubsidy02Info')->nullable()->comment('年度低收入戶及中低收入戶租金補貼金額');
            $table->integer('isOtherSubsidy03')->nullable()->comment('是否有年度身心障礙者房屋租金補貼年度依其他法令相關租金補貼規定之租金補貼');
            $table->integer('isOtherSubsidy03Info')->nullable()->comment('年度身心障礙者房屋租金補貼年度依其他法令相關租金補貼規定之租金補貼金額');
            $table->integer('isOtherSubsidy04')->nullable()->comment('是否有年度依其他法令相關租金補貼規定之租金補貼');
            $table->integer('isOtherSubsidy04Info')->nullable()->comment('年度依其他法令相關租金補貼規定之租金補貼金額');
            $table->integer('isOtherSubsidy05')->nullable()->comment('是否有縣市承租政府興建之國民住宅或社會住宅');
            $table->integer('isOtherSubsidy05Info')->nullable()->comment('縣市承租政府興建之國民住宅或社會住宅金額');
            $table->integer('legal_agent_num')->nullable()->comment('法定代理人人數');
            $table->string('single_proxy_reason')->nullable()->comment('只有一個代理人的原因');
            $table->string('first_legal_agent_name')->nullable()->comment('第一個法定代理人姓名');
            $table->string('first_legal_agent_tel')->nullable()->comment('第一個法定代理人電話');
            $table->string('first_legal_agent_phone')->nullable()->comment('第一個法定代理人手機');
            $table->string('first_legal_agent_addr')->nullable()->comment('第一個法定代理人地址');
            $table->string('second_legal_agent_name')->nullable()->comment('第二個法定代理人姓名');
            $table->string('second_legal_agent_tel')->nullable()->comment('第二個法定代理人電話');
            $table->string('second_legal_agent_phone')->nullable()->comment('第二個法定代理人手機');
            $table->string('second_legal_agent_addr')->nullable()->comment('第二個法定代理人地址');
            $table->string('bank_name')->nullable()->comment('金融機構名稱');
            $table->string('bank_branch_code')->nullable()->comment('分行代碼');
            $table->string('bank_owner_name')->nullable()->comment('金融機構戶名');
            $table->string('bank_account')->nullable()->comment('金融機構帳號');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_management_new');
    }
}
