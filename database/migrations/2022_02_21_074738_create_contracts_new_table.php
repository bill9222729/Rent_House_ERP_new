<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts_new', function (Blueprint $table) {
            $table->id();
            $table->string('building_case_id')->nullable()->comment('物件編號');
            $table->string('tenant_case_id')->nullable()->comment('物件編號');
            $table->date('lease_start_date')->nullable()->comment('租賃起始時間');
            $table->date('lease_end_date')->nullable()->comment('租賃中止時間');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts_new');
    }
}
