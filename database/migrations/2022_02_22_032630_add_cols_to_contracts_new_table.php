<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts_new', function (Blueprint $table) {
            $table->string('charter_case_id')->nullable()->comment('包租約編號');
            $table->string('contract_type')->nullable()->comment('合約類型');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts_new', function (Blueprint $table) {
            $table->dropColumn('charter_case_id');
            $table->dropColumn('contract_type');
        });
    }
}
