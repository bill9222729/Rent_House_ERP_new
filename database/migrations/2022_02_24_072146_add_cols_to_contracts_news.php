<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractsNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts_new', function (Blueprint $table) {
            $table->string('match_id')->nullable()->comment('媒合編號');
            $table->string('tenant_case_id')->comment('租客編號')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts_new', function (Blueprint $table) {
            $table->dropColumn('match_id');
            $table->string('tenant_case_id')->comment('物件編號')->change();
        });
    }
}
