<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_management', function (Blueprint $table) {
            $table->id();
            $table->string('match_id')->nullable()->comment('媒合編號');
            $table->string('building_case_id')->nullable()->comment('物件編號');
            $table->string('tenant_case_id')->nullable()->comment('租客編號');
            $table->date('lease_start_date')->nullable()->comment('租賃起始時間');
            $table->date('lease_end_date')->nullable()->comment('租賃中止時間');
            $table->string('charter_case_id')->nullable()->comment('包租約編號');
            $table->string('contract_type')->nullable()->comment('契約類型');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_management');
    }
}
