<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_management', function (Blueprint $table) {
            $table->id();
            $table->string('case_status')->nullable()->comment('契約狀態');
            $table->date('signing_date')->nullable()->comment('簽約日期');
            $table->string('other_landlord_desc')->nullable()->comment('其他房東的資料');
            $table->date('lease_start_date')->nullable()->comment('租賃開始時間');
            $table->date('lease_end_date')->nullable()->comment('租賃結束時間');
            $table->integer('rent')->nullable()->comment('租金');
            $table->string('rent_pay_type')->nullable()->comment('租金支付方式');
            $table->string('bank_name')->nullable()->comment('銀行名稱');
            $table->string('bank_branch_code')->nullable()->comment('銀行代碼');
            $table->string('bank_owner_name')->nullable()->comment('銀行戶名');
            $table->string('bank_account')->nullable()->comment('銀行帳號');
            $table->integer('deposit_amount')->nullable()->comment('押租金');
            $table->string('contract_note')->nullable()->comment('契約備註說明');
            $table->integer('apply_man_id')->nullable()->comment('業務編號');
            $table->string('remark')->nullable()->comment('備註');
            $table->string('file_path')->nullable()->comment('檢附文件路徑');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_management');
    }
}
