<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormStateToMatchManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('match_management', function (Blueprint $table) {
            $table->string('form_state')->nullable()->comment('表單狀態')->after('contract_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_management', function (Blueprint $table) {
            $table->dropColumn('form_state');
        });
    }
}
