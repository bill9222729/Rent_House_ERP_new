<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->string('building_case_no')->nullable()->comment('建物案件編號')->after('id');
            $table->string('tenant_case_no')->nullable()->comment('租客案件編號')->after('building_case_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->dropColumn('building_case_no');
            $table->dropColumn('tenant_case_no');
        });
    }
}
