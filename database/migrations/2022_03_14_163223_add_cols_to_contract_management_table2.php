<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractManagementTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->string('bank_owner_name')->default("易居管理顧問股份有限公司")->change();
            $table->string('burden')->after('rent_pay_type')->change();
            $table->integer('has_tanants')->nullable()->comment('現在有沒有房客')->after('bank_account');
            $table->integer('notary_fees')->nullable()->comment('公證費總共')->after('deposit_amount');
            $table->integer('notary_fees_share1')->nullable()->comment('公證費由出租人負擔')->after('notary_fees');
            $table->integer('notary_fees_share2')->nullable()->comment('公證費由承租人負擔')->after('notary_fees_share1');
            $table->integer('matchmaking_fee')->nullable()->comment('媒合費')->after('notary_fees_share2');
            $table->integer('escrow_fee')->nullable()->comment('代管費')->after('matchmaking_fee');
            $table->integer('management_fee')->nullable()->comment('管理費')->after('escrow_fee');
            $table->integer('parking_fee')->nullable()->comment('車位費用')->after('management_fee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->string('bank_owner_name')->default(null)->change();
            $table->string('burden')->after('id')->change();
            $table->dropColumn('has_tanants');
            $table->dropColumn('notary_fees');
            $table->dropColumn('notary_fees_share1');
            $table->dropColumn('notary_fees_share2');
            $table->dropColumn('matchmaking_fee');
            $table->dropColumn('escrow_fee');
            $table->dropColumn('management_fee');
            $table->dropColumn('parking_fee');
        });
    }
}
