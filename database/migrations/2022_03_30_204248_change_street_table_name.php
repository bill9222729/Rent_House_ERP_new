<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeStreetTableName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename("located_lot", "located_lot_old");
        Schema::rename("street", "located_lot");
        Schema::rename("located_lot_old", "street");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename("located_lot", "located_lot_old");
        Schema::rename("street", "located_lot");
        Schema::rename("located_lot_old", "street");
    }
}
