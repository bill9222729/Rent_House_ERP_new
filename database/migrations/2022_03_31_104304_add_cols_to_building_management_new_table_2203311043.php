<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToBuildingManagementNewTable2203311043 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->integer('building_deposit_amount_month')->nullable()->comment('押金月數')->after('building_rent');
            $table->integer('can_bargain')->nullable()->comment('可否議價')->after('building_deposit_amount');
            $table->integer('has_management_fee')->nullable()->comment('是否有管理費')->after('can_bargain');
            $table->integer('management_fee_month')->nullable()->comment('管理費每月多少')->after('has_management_fee');
            $table->integer('management_fee_square_meter')->nullable()->comment('管理費每坪多少')->after('management_fee_month');
            $table->text('building_offer_other_desc')->nullable()->comment('提供設備其他的詳細名稱')->after('building_offer_other');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->dropColumn('building_deposit_amount_month');
            $table->dropColumn('can_bargain');
            $table->dropColumn('has_management_fee');
            $table->dropColumn('management_fee_month');
            $table->dropColumn('management_fee_square_meter');
            $table->dropColumn('building_offer_other_desc');
        });
    }
}
