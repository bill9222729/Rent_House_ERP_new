<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileManagmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_managment', function (Blueprint $table) {
            $table->id();
            $table->string('file_origin_name')->nullable()->comment('檔案原始名稱');
            $table->string('file_server_name')->nullable()->comment('檔案在伺服器的名稱');
            $table->integer('file_size')->nullable()->comment('檔案大小');
            $table->string('file_source')->nullable()->comment('檔案來源');
            $table->integer('file_source_id')->nullable()->comment('檔案來源的id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_managment');
    }
}
