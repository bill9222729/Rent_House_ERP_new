<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToBuildingManagementNew220406Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->text('building_source')->nullable()->comment('物件來源')->after('case_state');
            $table->text('representative')->nullable()->comment('代表人')->after('name');
            $table->text('tax_number')->nullable()->comment('統一編號')->after('representative');
            $table->text('agent_name')->nullable()->comment('代理人姓名')->after('tax_number');
            $table->text('agent_tel')->nullable()->comment('代理人電話')->after('agent_name');
            $table->text('agent_cellphone')->nullable()->comment('代理人手機')->after('agent_tel');
            $table->text('agent_addr')->nullable()->comment('代理人地址')->after('agent_cellphone');
            $table->text('lessor_type')->nullable()->comment('出租人類型')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->dropColumn('building_source');
            $table->dropColumn('representative');
            $table->dropColumn('tax_number');
            $table->dropColumn('agent_name');
            $table->dropColumn('agent_tel');
            $table->dropColumn('agent_cellphone');
            $table->dropColumn('agent_addr');
            $table->dropColumn('lessor_type');
        });
    }
}
