<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChangeHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_history', function (Blueprint $table) {
            $table->id();
            $table->date('apply_date')->nullable()->comment('異動日期');
            $table->string('lessor_type')->nullable()->comment('出租人類型');
            $table->string('name')->nullable()->comment('姓名(名稱)');
            $table->string('phone')->nullable()->comment('電話');
            $table->string('reason')->nullable()->comment('變更原因');
            $table->integer('edit_member_id')->nullable()->comment('建檔人員');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_history');
    }
}
