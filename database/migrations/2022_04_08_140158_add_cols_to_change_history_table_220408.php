<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToChangeHistoryTable220408 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('change_history', function (Blueprint $table) {
            $table->date('change_date')->nullable()->comment('事實發生日')->after('apply_date');
            $table->string('change_note')->nullable()->comment('變更備註')->after('reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('change_history', function (Blueprint $table) {
            $table->dropColumn('change_date');
            $table->dropColumn('change_note');
        });
    }
}
