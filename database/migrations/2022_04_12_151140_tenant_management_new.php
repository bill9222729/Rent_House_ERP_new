<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TenantManagementNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->integer('sales_id')->nullable()->comment('業務ID')->after('apply_date');
            $table->string('weak_item')->nullable()->comment('弱勢項目')->after('membtype');
            $table->string('government_no')->nullable()->comment('內政部編號')->after('sales_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->dropColumn('sales_id');
            $table->dropColumn('weak_item');
            $table->dropColumn('government_no');
        });
    }
}
