<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantFamilyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_family_info', function (Blueprint $table) {
            $table->id();
            $table->integer('tenant_id')->nullable()->comment('對應的租客id');
            $table->string('name')->nullable()->comment('姓名');
            $table->integer('is_foreigner')->nullable()->comment('是否為外籍');
            $table->string('id_no')->nullable()->comment('身分/居留證號');
            $table->string('appellation')->nullable()->comment('稱謂');
            $table->string('gender')->nullable()->comment('性別');
            $table->integer('condition_low_income_households')->nullable()->comment('低收入戶');
            $table->integer('condition_low_and_middle_income_households')->nullable()->comment('中低收入戶');
            $table->integer('condition_special_circumstances_families')->nullable()->comment('特殊境遇家庭');
            $table->integer('condition_over_sixty_five')->nullable()->comment('65歲以上');
            $table->integer('condition_domestic_violence')->nullable()->comment('於家庭暴力或性侵害之受害者及其子女');
            $table->integer('condition_disabled')->nullable()->comment('身心障礙者');
            $table->integer('condition_disabled_type')->nullable()->comment('障礙類別');
            $table->string('condition_disabled_level')->nullable()->comment('障礙程度');
            $table->integer('condition_aids')->nullable()->comment('感染或罹患AIDS者');
            $table->integer('condition_aboriginal')->nullable()->comment('原住民');
            $table->integer('condition_disaster_victims')->nullable()->comment('災民');
            $table->integer('condition_vagabond')->nullable()->comment('遊民');
            $table->integer('condition_Applicant_naturalization')->nullable()->comment('申請人設籍：與申請人設籍之家庭成員');
            $table->integer('condition_Applicant_not_naturalization')->nullable()->comment('申請人未設籍：實際居住之家庭成員');
            $table->integer('condition_Applicant_not_naturalization_and_study')->nullable()->comment('未設籍當地且在該地區就學就業者');
            $table->integer('condition_police_officer')->nullable()->comment('警消人員');
            $table->integer('filers')->nullable()->comment('建檔人員id，對應user資料表');
            $table->integer('last_modified_person')->nullable()->comment('最後修改人員id，對應user資料表');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_family_info');
    }
}
