<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantMinorOffspringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_minor_offspring', function (Blueprint $table) {
            $table->id();
            $table->integer('tenant_id')->nullable()->comment('對應的租客id');
            $table->string('name')->nullable()->comment('姓名');
            $table->integer('is_foreigner')->nullable()->comment('是否為外籍');
            $table->string('appellation')->nullable()->comment('稱謂');
            $table->string('id_no')->nullable()->comment('身分/居留證號');
            $table->date('birthday')->nullable()->comment('出生年月日');
            $table->integer('filers')->nullable()->comment('建檔人員id，對應user資料表');
            $table->integer('last_modified_person')->nullable()->comment('最後修改人員id，對應user資料表');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_minor_offspring');
    }
}
