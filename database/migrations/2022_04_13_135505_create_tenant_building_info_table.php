<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantBuildingInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_building_info', function (Blueprint $table) {
            $table->id();
            $table->integer('tenant_id')->nullable()->comment('對應的租客id');
            $table->string('name')->nullable()->comment('持有者姓名');
            $table->string('located_city')->nullable()->comment('座落縣市');
            $table->string('located_Twn_spcode_area')->nullable()->comment('坐落地段-區');
            $table->string('located_lot_area')->nullable()->comment('坐落地段-段');
            $table->string('located_landno')->nullable()->comment('坐落地段-地號');
            $table->string('building_no')->nullable()->comment('建號');
            $table->string('building_total_square_meter_percent')->nullable()->comment('持分');
            $table->string('building_total_square_meter')->nullable()->comment('持分面積(平方公尺)');
            $table->string('addr_cntcode')->nullable()->comment('地址-縣市');
            $table->string('addr_twnspcode')->nullable()->comment('地址-區域');
            $table->string('addr_street_area')->nullable()->comment('地址-街道');
            $table->string('addr_lane')->nullable()->comment('地址-巷');
            $table->string('addr_alley')->nullable()->comment('地址-弄');
            $table->string('addr_no')->nullable()->comment('地址-號');
            $table->integer('is_household')->nullable()->comment('家庭成員是否設籍於該處');
            $table->integer('filers')->nullable()->comment('建檔人員id，對應user資料表');
            $table->integer('last_modified_person')->nullable()->comment('最後修改人員id，對應user資料表');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_building_info');
    }
}
