<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToTenantManagementNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->integer('building_pattern_01')->nullable()->comment('希望格局-套房')->after('bank_account');
            $table->integer('building_pattern_02')->nullable()->comment('希望格局-雅房')->after('building_pattern_01');
            $table->integer('building_pattern_03')->nullable()->comment('希望格局-1房')->after('building_pattern_02');
            $table->integer('building_pattern_04')->nullable()->comment('希望格局-2房')->after('building_pattern_03');
            $table->integer('building_pattern_05')->nullable()->comment('希望格局-3房以上')->after('building_pattern_04');
            $table->integer('building_type_01')->nullable()->comment('希望房屋類型-公寓')->after('building_pattern_05');
            $table->integer('building_type_02')->nullable()->comment('希望房屋類型-電梯大樓')->after('building_type_01');
            $table->integer('building_type_03')->nullable()->comment('希望房屋類型-透天厝')->after('building_type_02');
            $table->integer('building_type_04')->nullable()->comment('希望房屋類型-平房')->after('building_type_03');
            $table->integer('building_use_area_01')->nullable()->comment('希望實際使用坪數-10坪以下')->after('building_type_04');
            $table->integer('building_use_area_02')->nullable()->comment('希望實際使用坪數-10~20坪')->after('building_use_area_01');
            $table->integer('building_use_area_03')->nullable()->comment('希望實際使用坪數-20~30坪')->after('building_use_area_02');
            $table->integer('building_use_area_04')->nullable()->comment('希望實際使用坪數-30~40坪')->after('building_use_area_03');
            $table->integer('building_use_area_05')->nullable()->comment('希望實際使用坪數-40坪以上')->after('building_use_area_04');
            $table->integer('wish_floor_01')->nullable()->comment('希望居住樓層-1樓')->after('building_use_area_05');
            $table->integer('wish_floor_02')->nullable()->comment('希望居住樓層-2~6樓')->after('wish_floor_01');
            $table->integer('wish_floor_03')->nullable()->comment('希望居住樓層-6~12樓')->after('wish_floor_02');
            $table->integer('wish_floor_04')->nullable()->comment('希望居住樓層-12樓以上')->after('wish_floor_03');
            $table->integer('wish_floor_exact')->nullable()->comment('希望居住樓層-確切樓層')->after('wish_floor_04');
            $table->integer('wish_room_num')->nullable()->comment('希望隔間-幾房')->after('wish_floor_exact');
            $table->integer('wish_living_rooms_num')->nullable()->comment('希望隔間-幾廳')->after('wish_room_num');
            $table->integer('wish_bathrooms_num')->nullable()->comment('希望隔間-幾衛')->after('wish_living_rooms_num');
            $table->integer('req_access_control')->nullable()->comment('需要門禁')->after('wish_bathrooms_num');
            $table->integer('rent_min')->nullable()->comment('希望租金下限')->after('req_access_control');
            $table->integer('rent_max')->nullable()->comment('希望租金上限')->after('rent_min');
            $table->integer('building_offer_tv')->nullable()->comment('是否提供電視')->after('rent_min');
            $table->integer('building_offer_refrigerator')->nullable()->comment('是否提供冰箱')->after('rent_min');
            $table->integer('building_offer_cable')->nullable()->comment('是否提供有線電視(第四台)')->after('rent_min');
            $table->integer('building_offer_air_conditioner')->nullable()->comment('是否有提供冷氣')->after('rent_min');
            $table->integer('building_offer_geyser')->nullable()->comment('是否有提供熱水器')->after('rent_min');
            $table->integer('building_offer_internet')->nullable()->comment('是否有提供網際網路')->after('rent_min');
            $table->integer('building_offer_washing_machine')->nullable()->comment('是否有提供洗衣機')->after('rent_min');
            $table->integer('building_offer_natural_gas')->nullable()->comment('是否有提供天然瓦斯')->after('rent_min');
            $table->integer('building_offer_bed')->nullable()->comment('是否有提供床')->after('rent_min');
            $table->integer('building_offer_wardrobe')->nullable()->comment('是否有提供衣櫃')->after('rent_min');
            $table->integer('building_offer_desk')->nullable()->comment('是否有提供桌子')->after('rent_min');
            $table->integer('building_offer_chair')->nullable()->comment('是否有提供椅子')->after('rent_min');
            $table->integer('building_offer_sofa')->nullable()->comment('是否有提供沙發')->after('rent_min');
            $table->integer('building_offer_other')->nullable()->comment('其他提供設備')->after('rent_min');
            $table->integer('is_live_together')->nullable()->comment('是否有共住人')->after('building_offer_tv');
            $table->string('is_live_together_desc')->nullable()->comment('共住說明')->after('is_live_together');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->dropColumn('building_pattern_01');
            $table->dropColumn('building_pattern_02');
            $table->dropColumn('building_pattern_03');
            $table->dropColumn('building_pattern_04');
            $table->dropColumn('building_pattern_05');
            $table->dropColumn('building_type_01');
            $table->dropColumn('building_type_02');
            $table->dropColumn('building_type_03');
            $table->dropColumn('building_type_04');
            $table->dropColumn('building_use_area_01');
            $table->dropColumn('building_use_area_02');
            $table->dropColumn('building_use_area_03');
            $table->dropColumn('building_use_area_04');
            $table->dropColumn('building_use_area_05');
            $table->dropColumn('wish_floor_01');
            $table->dropColumn('wish_floor_02');
            $table->dropColumn('wish_floor_03');
            $table->dropColumn('wish_floor_04');
            $table->dropColumn('wish_floor_exact');
            $table->dropColumn('wish_room_num');
            $table->dropColumn('wish_living_rooms_num');
            $table->dropColumn('wish_bathrooms_num');
            $table->dropColumn('req_access_control');
            $table->dropColumn('rent_min');
            $table->dropColumn('rent_max');
            $table->dropColumn('building_offer_tv');
            $table->dropColumn('building_offer_refrigerator');
            $table->dropColumn('building_offer_cable');
            $table->dropColumn('building_offer_air_conditioner');
            $table->dropColumn('building_offer_geyser');
            $table->dropColumn('building_offer_internet');
            $table->dropColumn('building_offer_washing_machine');
            $table->dropColumn('building_offer_natural_gas');
            $table->dropColumn('building_offer_bed');
            $table->dropColumn('building_offer_wardrobe');
            $table->dropColumn('building_offer_desk');
            $table->dropColumn('building_offer_chair');
            $table->dropColumn('building_offer_sofa');
            $table->dropColumn('building_offer_other');
            $table->dropColumn('is_live_together');
            $table->dropColumn('is_live_together_desc');
        });
    }
}
