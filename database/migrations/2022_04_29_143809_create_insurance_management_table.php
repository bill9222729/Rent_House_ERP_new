<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_management', function (Blueprint $table) {
            $table->id();
            $table->integer('sales_id')->nullable()->comment('業務id');
            $table->string('match_id')->nullable()->comment('媒合編號');
            $table->string('building_id')->nullable()->comment('物件編號，對應到building_management_new的case_no欄位');
            $table->integer('insurance_premium')->nullable()->comment('保費');
            $table->string('insurer')->nullable()->comment('保險業者');
            $table->date('insurance_start')->nullable()->comment('保險開始時間');
            $table->date('insurance_end')->nullable()->comment('保險結束時間');
            $table->date('monthly_application_month')->nullable()->comment('月報申請月份');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_management');
    }
}
