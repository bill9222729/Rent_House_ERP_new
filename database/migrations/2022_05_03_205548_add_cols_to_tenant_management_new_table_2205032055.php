<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToTenantManagementNewTable2205032055 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->string('condition_disabled_level')->nullable()->comment('障礙程度')->after('weak_item');
            $table->integer('condition_disabled_type')->nullable()->comment('障礙類別')->after('weak_item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->dropColumn('condition_disabled_level');
            $table->dropColumn('condition_disabled_type');
        });
    }
}
