<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractManagementTable2205101505 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->string('development_fee')->nullable()->comment('開發費')->after('parking_fee');
            $table->string('guarantee_fee')->nullable()->comment('包管費')->after('parking_fee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->dropColumn('development_fee');
            $table->dropColumn('guarantee_fee');
        });
    }
}
