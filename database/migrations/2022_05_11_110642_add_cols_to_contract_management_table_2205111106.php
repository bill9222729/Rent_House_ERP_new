<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractManagementTable2205111106 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->integer('rent_self_pay')->nullable()->comment('開發費')->after('rent');
            $table->integer('rent_government_pay')->nullable()->comment('包管費')->after('rent_self_pay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->dropColumn('rent_self_pay');
            $table->dropColumn('rent_government_pay');
        });
    }
}
