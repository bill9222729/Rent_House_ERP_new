<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixDetailManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fix_detail_management', function (Blueprint $table) {
            $table->id();
            $table->date('report_date')->nullable()->comment('報修日期');
            $table->integer('fix_id')->nullable()->comment('修繕總表編號');
            $table->string('repair_item')->nullable()->comment('修繕項目');
            $table->string('repair_manufacturers')->nullable()->comment('修繕廠商');
            $table->string('repair_records')->nullable()->comment('修繕紀錄');
            $table->date('meeting_date')->nullable()->comment('會勘日期');
            $table->string('meeting_master')->nullable()->comment('會勘師傅');
            $table->date('fix_date')->nullable()->comment('修繕日期');
            $table->date('completion_date')->nullable()->comment('完成日期');
            $table->integer('please_amount')->nullable()->comment('請領金額');
            $table->integer('reply_amount')->nullable()->comment('覆價金額');
            $table->date('monthly_application_month')->nullable()->comment('月報申請月份');
            $table->integer('balance')->nullable()->comment('餘額');
            $table->string('note')->nullable()->comment('備註');
            $table->integer('no_reference')->nullable()->comment('不備查');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fix_detail_management');
    }
}
