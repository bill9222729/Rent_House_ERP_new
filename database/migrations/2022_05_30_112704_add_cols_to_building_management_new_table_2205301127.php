<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToBuildingManagementNewTable2205301127 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->string('virtual_bank_name')->nullable()->comment('虛擬金融機構名稱')->after('bank_account');
            $table->integer('virtual_bank_branch_code')->nullable()->comment('虛擬分行代碼')->after('virtual_bank_name');
            $table->string('virtual_bank_owner_name')->nullable()->comment('虛擬金融機構戶名')->after('virtual_bank_branch_code');
            $table->string('virtual_bank_account')->nullable()->comment('虛擬金融機構帳號')->after('virtual_bank_owner_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->dropColumn('virtual_bank_name');
            $table->dropColumn('virtual_bank_branch_code');
            $table->dropColumn('virtual_bank_owner_name');
            $table->dropColumn('virtual_bank_account');
        });
    }
}
