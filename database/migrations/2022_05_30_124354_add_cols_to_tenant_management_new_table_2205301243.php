<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToTenantManagementNewTable2205301243 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->string('virtual_bank_account')->nullable()->comment('虛擬金融機構帳號')->after('bank_account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->dropColumn('virtual_bank_account');
        });
    }
}
