<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToMatchManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('match_management', function (Blueprint $table) {
            $table->date('contract_stop_date')->nullable()->comment('終止合約日期')->after('form_state');
            $table->string('contract_stop_reason')->nullable()->comment('終止合約原因')->after('contract_stop_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_management', function (Blueprint $table) {
            $table->dropColumn('contract_stop_date');
            $table->dropColumn('contract_stop_reason');
        });
    }
}
