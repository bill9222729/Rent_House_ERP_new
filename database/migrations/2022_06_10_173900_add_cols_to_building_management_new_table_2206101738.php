<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToBuildingManagementNewTable2206101738 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->text('note')->nullable()->comment('備註')->after('entrusted_management_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->dropColumn('note');
        });
    }
}
