<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToTenantManagementNewTable2206131412 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            //Guild Edition
            $table->integer('guild_edition')->nullable()->comment('是否為公會版')->after('apply_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->dropColumn('guild_edition');
        });
    }
}
