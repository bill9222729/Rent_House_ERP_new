<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToUsersTable2206221125 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('passport_english_name')->nullable()->comment('護照英文姓名')->after('email');
            $table->string('english_name')->nullable()->comment('英文姓名')->after('passport_english_name');
            $table->date('birthday')->nullable()->comment('生日')->after('english_name');
            $table->string('id_no')->nullable()->comment('身分證字號')->after('birthday');
            $table->string('residence_city')->nullable()->comment('戶籍地址-縣市')->after('id_no');
            $table->string('residence_city_area')->nullable()->comment('戶籍地址-區域')->after('residence_city');
            $table->string('residenceresidence_addr_addr')->nullable()->comment('戶籍地址-地址')->after('residence_city_area');
            $table->string('mailing_city')->nullable()->comment('通訊地址-城市')->after('residenceresidence_addr_addr');
            $table->string('mailing_city_area')->nullable()->comment('通訊地址-區域')->after('mailing_city');
            $table->string('mailing_addr')->nullable()->comment('通訊地址-地址')->after('mailing_city_area');
            $table->string('tel')->nullable()->comment('電話')->after('mailing_addr');
            $table->string('cellphone')->nullable()->comment('手機')->after('tel');
            $table->string('company_cellphone')->nullable()->comment('公司手機')->after('cellphone');
            $table->string('bank_name')->nullable()->comment('銀行名稱')->after('company_cellphone');
            $table->string('bank_branch_code')->nullable()->comment('銀行代號')->after('bank_name');
            $table->string('bank_owner_name')->nullable()->comment('帳戶名稱')->after('bank_branch_code');
            $table->string('bank_account')->nullable()->comment('銀行帳號')->after('bank_owner_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('passport_english_name');
            $table->dropColumn('english_name');
            $table->dropColumn('birthday');
            $table->dropColumn('id_no');
            $table->dropColumn('residence_city');
            $table->dropColumn('residence_city_area');
            $table->dropColumn('residenceresidence_addr_addr');
            $table->dropColumn('mailing_city');
            $table->dropColumn('mailing_city_area');
            $table->dropColumn('mailing_addr');
            $table->dropColumn('tel');
            $table->dropColumn('cellphone');
            $table->dropColumn('company_cellphone');
            $table->dropColumn('bank_name');
            $table->dropColumn('bank_branch_code');
            $table->dropColumn('bank_owner_name');
            $table->dropColumn('bank_account');
        });
    }
}
