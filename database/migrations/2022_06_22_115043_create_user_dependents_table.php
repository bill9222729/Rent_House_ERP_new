<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_dependents', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable()->comment('使用者id');
            $table->string('appellation')->nullable()->comment('稱謂');
            $table->string('id_no')->nullable()->comment('身分證字號');
            $table->date('birthday')->nullable()->comment('生日');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_dependents');
    }
}
