<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToTenantManagementNewTable2206231447 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->date("condition_end_date")->nullable()->comment('弱勢資格有效期限')->after('weak_item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_management_new', function (Blueprint $table) {
            $table->dropColumn('condition_end_date');
        });
    }
}
