<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractManagementTable2206241706 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->string("stakeholders")->nullable()->comment('共同持分人')->after('signing_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->dropColumn('stakeholders');
        });
    }
}
