<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToContractManagementTable2206291507 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->string("information_feed")->nullable()->comment('資訊提供')->after('file_path');
            $table->date("information_feed_date")->nullable()->comment('資訊提供日期')->after('information_feed');
            $table->text("net_price_login")->nullable()->comment('實價登陸')->after('information_feed_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_management', function (Blueprint $table) {
            $table->dropColumn('information_feed');
            $table->dropColumn('information_feed_date');
            $table->dropColumn('net_price_login');
        });
    }
}
