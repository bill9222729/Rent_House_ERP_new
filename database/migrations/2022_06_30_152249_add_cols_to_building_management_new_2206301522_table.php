<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToBuildingManagementNew2206301522Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->string("data_status")->nullable()->comment('資料狀態')->after('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_management_new', function (Blueprint $table) {
            $table->dropColumn('data_status');
        });
    }
}
