<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToMatchManagementTable2207011508 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('match_management', function (Blueprint $table) {
            $table->string("data_status")->nullable()->comment('資料狀態')->after('contract_stop_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_management', function (Blueprint $table) {
            $table->dropColumn('data_status');
        });
    }
}
