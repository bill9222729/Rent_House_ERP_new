<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToFixDetailManagementTable2207081501 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->integer("building_id")->nullable()->comment('對應物件編號')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->dropColumn('building_id');
        });
    }
}
