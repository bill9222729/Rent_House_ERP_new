<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToFixDetailManagementTable2207080708 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->integer("broken_reason")->nullable()->comment('損壞原因')->after('repair_item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->dropColumn('broken_reason');
        });
    }
}
