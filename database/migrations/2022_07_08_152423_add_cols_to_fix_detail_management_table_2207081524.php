<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToFixDetailManagementTable2207081524 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->integer("has_receipt")->nullable()->comment('有無收據')->after('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->dropColumn('has_receipt');
        });
    }
}
