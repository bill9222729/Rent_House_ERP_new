<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToFixDetailManagementTable07081911 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->date("invoice_date")->nullable()->comment('發票日期')->after('report_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fix_detail_management', function (Blueprint $table) {
            $table->dropColumn('invoice_date');
        });
    }
}
