// 欄位驗證
function addFormValid(formId, callback=null) {
    $("#" + formId).validate({
        onkeyup: function (element, event) {
            //去除左側空白
            let value = this.elementValue(element).replace(/^\s+/g, "");
            $(element).val(value);
        },
        rules: {
            building_source: {
                required: true,
            },
            case_state: {
                required: true,
            },
            sales_id: {
                required: true,
            },
            name: {
                required: true,
            },
            gender: {
                required: true,
            },
            birthday: {
                required: true,
            },
            id_no: {
                required: true,
            },
            cellphone: {
                required: true,
            },
            residence_city: {
                required: true,
            },
            residence_city_area: {
                required: true,
            },
            residence_addr: {
                required: true,
            },
            mailing_city: {
                required: true,
            },
            mailing_city_area: {
                required: true,
            },
            mailing_addr: {
                required: true,
            },
            building_address_city: {
                required: true,
            },
            building_address_city_area: {
                required: true,
            },
            building_address_street: {
                required: true,
            },
            building_address_num: {
                required: true,
            },
            building_located_city: {
                required: true,
            },
            building_located_city_area: {
                required: true,
            },
            building_located_lot: {
                required: true,
            },
            building_located_land_num: {
                required: true,
            },
            building_num: {
                required: true,
            },
            building_age: {
                required: true,
            },
            building_total_square_meter: {
                required: true,
            },
            building_use_square_meter: {
                required: true,
            },
            building_rent: {
                required: true,
            },
            building_is_including_electricity_fees: {
                required: true,
            },
            building_is_including_water_fees: {
                required: true,
            },
            building_is_including_parking_space: {
                required: true,
            },
            building_type: {
                required: true,
            },
            building_pattren: {
                required: true,
            },
            building_deposit_amount_month: {
                required: true,
            },
            has_management_fee: {
                required: true,
            },
            management_fee_month: {
                required: true,
            },
            management_fee_square_meter: {
                required: true,
            },
            representative: {
                required: true,
            },
            tax_number: {
                required: true,
            },
        },
        messages: {
            building_source: {
                required: "請選擇物件來源",
            },
            case_state: {
                required: "請選擇物件狀態",
            },
            sales_id: {
                required: "請選擇業務",
            },
            name: {
                required: "請輸入出租人姓名",
            },
            gender: {
                required: "請選擇出租人性別",
            },
            birthday: {
                required: "請選擇出租人生日",
            },
            id_no: {
                required: "請選輸入身份證字號",
            },
            cellphone: {
                required: "請選輸入手機號碼",
            },
            residence_city: {
                required: "必填",
            },
            residence_city_area: {
                required: "必填",
            },
            residence_addr: {
                required: "必填",
            },
            mailing_city: {
                required: "必填",
            },
            mailing_city_area: {
                required: "必填",
            },
            mailing_addr: {
                required: "必填",
            },
            building_address_city: {
                required: "必填",
            },
            building_address_city_area: {
                required: "必填",
            },
            building_address_street: {
                required: "必填",
            },
            building_address_num: {
                required: "必填",
            },
            building_located_city: {
                required: "必填",
            },
            building_located_city_area: {
                required: "必填",
            },
            building_located_lot: {
                required: "必填",
            },
            building_located_land_num: {
                required: "必填",
            },
            building_num: {
                required: "必填",
            },
            building_age: {
                required: "必填",
            },
            building_total_square_meter: {
                required: "必填",
            },
            building_use_square_meter: {
                required: "必填",
            },
            building_rent: {
                required: "必填",
            },
            building_is_including_electricity_fees: {
                required: "必填",
            },
            building_is_including_water_fees: {
                required: "必填",
            },
            building_is_including_parking_space: {
                required: "必填",
            },
            building_type: {
                required: "必填",
            },
            building_pattren: {
                required: "必填",
            },
            building_deposit_amount_month: {
                required: "必填",
            },
            has_management_fee: {
                required: "必填",
            },
            management_fee_month: {
                required: "必填",
            },
            management_fee_square_meter: {
                required: "必填",
            },
            representative: {
                required: "必填",
            },
            tax_number: {
                required: "必填",
            },
        },
        submitHandler: function (form) {
            console.log("送出");
            callback();
        },
    });
}

// 送出資料按鈕的事件
const changeDataStatusToStaff = () => {
    Swal.fire({
        icon: 'info',
        title: '送出資料之後就不能修改了喔!',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: '送出',
        denyButtonText: `稍等`,
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
            method: "PATCH",
            url: `/api/building_management_news/${buildingManagementNew.id}`,
            data: {data_status:"staff"},
            dataType: "json",
            success: function(data) {
                console.log(data);
                Swal.fire(
                    '成功',
                    '資料已送出',
                    'success'
                ).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = "/buildingManagementNews";
                    }
                })
            },
            error: function(err) {
                console.error(err);
            }
        })
        } else if (result.isDenied) {
          Swal.fire('資料還沒被送出', '', 'info')
        }
      })
}

// 退回資料按鈕的事件
const changeDataStatusToSales = () => {
    Swal.fire({
        icon: 'info',
        title: '確定要退回資料給業務修改？',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: '退回',
        denyButtonText: `稍等`,
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
            method: "PATCH",
            url: `/api/building_management_news/${buildingManagementNew.id}`,
            data: {data_status:"sales"},
            dataType: "json",
            success: function(data) {
                console.log(data);
                Swal.fire(
                    '成功',
                    '資料已退回',
                    'success'
                ).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = "/buildingManagementNews";
                    }
                })
            },
            error: function(err) {
                console.error(err);
            }
        })
        } else if (result.isDenied) {
          Swal.fire('資料還沒被退回', '', 'info')
        }
      })
}
