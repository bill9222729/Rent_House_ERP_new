// 取得今天日期
function getTodayDate() {
    let fullDate = new Date();
    let yyyy = fullDate.getFullYear();
    let MM = fullDate.getMonth() + 1 >= 10 ? fullDate.getMonth() + 1 : "0" + (fullDate.getMonth() + 1);
    let dd = fullDate.getDate() < 10 ? "0" + fullDate.getDate() : fullDate.getDate();
    let today = yyyy + "-" + MM + "-" + dd;
    return today;
}

// 取得建物資料
async function getBuildingData(building_id) {
    let retVal = "";
    let url = building_id ? `/api/building_management_news?id=${building_id}` : `/api/building_management_news`;
    await $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log("建物資料：");
            retVal = building_id ? data["data"][0] : data["data"];
            console.log(retVal);
        },
    });
    return retVal;
}

// 取得租客資料
async function getTenantData(tenant_id) {
    let retVal = "";
    let url = tenant_id ? `/api/tenant_management_news?id=${tenant_id}` : `/api/tenant_management_news`;
    await $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log("租客資料：");
            retVal = tenant_id ? data["data"][0] : data["data"];
            console.log(retVal);
        },
    });
    return retVal;
}

// 取得業務資料
async function getSalesmanData(salesman_id) {
    let retVal = "";
    let url = salesman_id ? `/api/users?is_sales=1&id=${salesman_id}` : `/api/users?is_sales=1`;
    await $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log("業務資料");
            retVal = data["data"];
            console.log(retVal);
        },
    });
    return retVal;
}

// 取得所有使用者資料
async function getUsersData(users_id) {
    let retVal = "";
    let url = users_id ? `/api/users?id=${users_id}` : `/api/users`;
    await $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log("使用者資料");
            retVal = data["data"];
            console.log(retVal);
        },
    });
    return retVal;
}

// 西元年轉民國年
function convertToRCDate(date) {
    if (typeof date === "object") {
        return date.getFullYear() - 1911 + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getDate();
    } else {
        date = new Date(date);
        return (date.getFullYear() - 1911).toString().padStart(3, "0") + "-" + ("0" + (date.getMonth() + 1)).slice(-2).padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
    }
}

// 民國年轉西元年
function convertToCEDate(date) {
    if (typeof date === "object") {
        return date.getFullYear() + 1911 + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getDate();
    } else {
        let dateArr = date.split("-");
        dateArr[0] = dateArr[0].padStart(4, "0");
        dateArr[1] = dateArr[1].padStart(2, "0");
        dateArr[2] = dateArr[2].padStart(2, "0");
        date = new Date(dateArr.join("-"));
        return date.getFullYear() + 1911 + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getDate();
    }
}

/**
 * 判斷日期是否在區間內
 * @param {*} startDate1 要判斷的日期開頭
 * @param {*} endDate1 要判斷的日期結尾
 * @param {*} startDate2 被判斷的日期開頭
 * @param {*} endDate2 被判斷的日期結尾
 * @returns
 */
function checkInDate(startDate1, endDate1, startDate2, endDate2) {
    const getDate = (date) => new Date(date);
    return !(getDate(endDate1) < getDate(startDate2) || getDate(startDate1) > getDate(endDate2));
}

// 取得縣市列表
async function getCityArr() {
    let retVal = null;
    const settings = {
        url: "/api/cities",
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal = response.data;
    });

    return retVal;
}
// 生成縣市option
async function ganerateCityOption() {
    let htmlContent = '<option value="">請選擇</option>';
    let cityArr = await getCityArr();
    cityArr.map((item) => {
        htmlContent += `<option value="${item.name}">${item.name}</option>`;
    });
    return htmlContent;
}

// 取得區域列表
async function getCityAreaArr(cityName) {
    let retVal = null;
    const settings = {
        url: `/api/city_areas?city_value=${cityName}`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal = response.data;
    });

    return retVal;
}

// 生成區域option
async function ganerateCityAreaOption(city_value) {
    let htmlContent = '<option value="">請選擇</option>';
    let cityArr = await getCityAreaArr(city_value);
    cityArr.map((item) => {
        htmlContent += `<option value="${item.value}">${item.name}</option>`;
    });
    return htmlContent;
}

// 取得街道列表
async function getStreetArr(cityAreaValue) {
    let retVal = null;
    const settings = {
        url: `/api/streets?city_area_value=${cityAreaValue}`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal = response.data;
    });

    return retVal;
}

// 生成街道option
async function ganerateStreetOption(cityAreaValue) {
    let htmlContent = '<option value="">請選擇</option>';
    let streetArr = await getStreetArr(cityAreaValue);
    streetArr.map((item) => {
        htmlContent += `<option value="${item.name}">${item.name}</option>`;
    });
    return htmlContent;
}

// 取得段號列表
async function getLocatedLotArr(cityAreaValue) {
    let retVal = null;
    const settings = {
        url: `/api/located_lots?city_area_value=${cityAreaValue}`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal = response.data;
    });

    return retVal;
}

// 生成段號option
async function ganerateLocatedLotOption(cityAreaValue) {
    let htmlContent = '<option value="">請選擇</option>';
    let streetArr = await getLocatedLotArr(cityAreaValue);
    streetArr.map((item) => {
        htmlContent += `<option value="${item.name}">${item.name}</option>`;
    });
    return htmlContent;
}

// 取得所有物件
const getAllBuildings = async () => {
    let retVal = null;
    const settings = {
        url: `/api/building_management_news`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal = response.data;
    });

    return retVal;
};

// 取得所有媒合資料
const getAllMatchs = async () => {
    let retVal = null;
    const settings = {
        url: `/api/match_managements`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal = response.data;
    });
    console.log(retVal);
    return retVal;
};

// 取得檔案列表
async function getFileArr(source, source_id) {
    const fileType = {
        mp4: "video",
        pdf: "pdf",
        jpg: "image",
        JPG: "image",
    };
    let retVal = {};
    const settings = {
        url: `/api/file_managments?file_source=${source}&file_source_id=${source_id}`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal["initialPreview"] = response.data.map((item) => {
            return `https://rent-house-erp.echouse.co/storage/buildings_file/${item.file_server_name}`;
        });
        retVal["initialPreviewConfig"] = response.data.map((item) => {
            return {
                type: fileType[item.file_origin_name.split(".").pop()],
                size: item.file_size,
                downloadUrl: `https://rent-house-erp.echouse.co/storage/buildings_file/${item.file_server_name}`,
                filename: item.file_origin_name,
                caption: item.file_origin_name,
                url: `/api/file_managments/${item.id}`,
            };
        });
    });

    return retVal;
}

// 初始化共用元件
function initPageElement() {
    // 初始化日期選擇器
    $(".input-group.date.day").datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        showOnFocus: false,
        todayHighlight: true,
        todayBtn: "linked",
        clearBtn: true,
    });
    // 初始化月份選擇器
    $(".input-group.date.month").datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        showOnFocus: false,
        todayHighlight: true,
        todayBtn: "linked",
        clearBtn: true,
        viewMode: "months",
        minViewMode: "months",
    });
}

// 房東管理頁面的ready
async function initBuildingManagementsPage(buildingManagementNew = null) {
    // 取得上傳的檔案
    let fileData = await getFileArr("building_management", buildingManagementNew ? buildingManagementNew["id"] : "");
    let fileDataPhoto = await getFileArr("building_management_photo", buildingManagementNew ? buildingManagementNew["id"] : "");
    // 上傳檔案部分的初始化
    $("#input-700").fileinput({
        language: "zh-TW",
        // uploadUrl: "/api/building_management_news/upload",
        uploadUrl: "/api/file_managments",
        maxFileCount: 0,
        maxTotalFileCount: 0,
        uploadExtraData: {
            file_source_id: buildingManagementNew ? buildingManagementNew["id"] : "",
            file_source: "building_management",
        },
        overwriteInitial: false,
        initialPreview: fileData["initialPreview"],
        initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
        initialPreviewFileType: "pdf", // image is the default and can be overridden in config below
        initialPreviewConfig: fileData["initialPreviewConfig"],
        ajaxDeleteSettings: {
            type: "DELETE", // This should override the ajax as $.ajax({ type: 'DELETE' })
        },
    });
    // 上傳檔案(照片)部分的初始化
    $("#building_photo").fileinput({
        language: "zh-TW",
        // uploadUrl: "/api/building_management_news/upload",
        uploadUrl: "/api/file_managments",
        maxFileCount: 0,
        maxTotalFileCount: 0,
        uploadExtraData: {
            file_source_id: buildingManagementNew ? buildingManagementNew["id"] : "",
            file_source: "building_management_photo",
        },
        overwriteInitial: false,
        initialPreview: fileDataPhoto["initialPreview"],
        initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
        initialPreviewFileType: "pdf", // image is the default and can be overridden in config below
        initialPreviewConfig: fileDataPhoto["initialPreviewConfig"],
        ajaxDeleteSettings: {
            type: "DELETE", // This should override the ajax as $.ajax({ type: 'DELETE' })
        },
    });
    // 初始化日期選擇器
    $(".input-group.date.day").datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        showOnFocus: false,
        todayHighlight: true,
        todayBtn: "linked",
        clearBtn: true,
    });
    // 初始化月份選擇器
    $(".input-group.date.month").datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        showOnFocus: false,
        todayHighlight: true,
        todayBtn: "linked",
        clearBtn: true,
        viewMode: "months",
        minViewMode: "months",
    });
    // 初始化縣市select
    $("#building_address_city,#building_located_city").html(await ganerateCityOption());
    // 設定縣市區域select連動
    $("#building_address_city,#building_located_city").on("change", async (e) => {
        let target_id = $(e.target).attr("id");
        let city_value = $(`#${target_id}`).val();
        $(`#${target_id}_area`).html(await ganerateCityAreaOption(city_value));
    });
    // 設定街道與縣市區域的連動
    $("#building_address_city_area").on("change", async (e) => {
        let target_id = $(e.target).attr("id");
        let city_area_value = $(`#${target_id}`).val();
        $("#building_address_street").html(await ganerateStreetOption(city_area_value));
        $("#building_address_street").selectpicker("refresh");
        $("#building_address_street").selectpicker("val", "");
    });
    // 設定段號與縣市區域的連動
    $("#building_located_city_area").on("change", async (e) => {
        let target_id = $(e.target).attr("id");
        let city_area_value = $(`#${target_id}`).val();
        $("#building_located_lot").html(await ganerateLocatedLotOption(city_area_value));
        $("#building_located_lot").selectpicker("refresh");
        $("#building_located_lot").selectpicker("val", "請選擇");
    });
    // 設定平方公尺與坪數換算(權狀坪數)
    $("#building_total_square_meter").on("change", (e) => {
        let building_total_square_meter_val = $(e.target).val();
        let square_feet = building_total_square_meter_val * 0.3025;
        $("#building_total_square_feet").val(square_feet);
    });
    // 設定平方公尺與坪數換算(實際使用坪數)
    $("#building_use_square_meter").on("change", (e) => {
        let building_use_square_meter_val = $(e.target).val();
        let square_feet = building_use_square_meter_val * 0.3025;
        $("#building_use_square_feet").val(square_feet);
    });
}

// 取得頁面所有資料
const getAllfieldsValue = () => {
    let data = {};
    $("#accordionPanelsStayOpenExample input[type!='search'][name!='_token'][type!='file']:not([class~='kv-fileinput-caption']),#accordionPanelsStayOpenExample select,#accordionPanelsStayOpenExample textarea").each((item, val) => {
        let field_id = $(val).attr("id");
        console.log(field_id, val);
        // 例外處理 checkbox 以及 radio 部分
        if ($(val).attr("type") === "checkbox" || $(val).attr("type") === "radio") {
            let field_val = $(val).is(":checked") ? "1" : "0";
            data[`${field_id}`] = field_val;
            return;
        }
        // 其他
        let field_val = $(val).val();
        data[`${field_id}`] = field_val;
    });
    // 例外處理 各種日期
    data["apply_date"] = data["apply_date"] ? convertToCEDate(data["apply_date"]) : "";
    data["birthday"] = data["birthday"] ? convertToCEDate(data["birthday"]) : "";
    data["building_complete_date"] = data["building_complete_date"] ? convertToCEDate(data["building_complete_date"]) : "";
    data["entrusted_lease_start_date"] = data["entrusted_lease_start_date"] ? convertToCEDate(data["entrusted_lease_start_date"]) : "";
    data["entrusted_lease_end_date"] = data["entrusted_lease_end_date"] ? convertToCEDate(data["entrusted_lease_end_date"]) : "";
    data["entrusted_management_start_date"] = data["entrusted_management_start_date"] ? convertToCEDate(data["entrusted_management_start_date"]) : "";
    data["entrusted_management_end_date"] = data["entrusted_management_end_date"] ? convertToCEDate(data["entrusted_management_end_date"]) : "";
    data["insurance_start"] = data["insurance_start"] ? convertToCEDate(data["insurance_start"]) : "";
    data["insurance_end"] = data["insurance_end"] ? convertToCEDate(data["insurance_end"]) : "";
    data["monthly_application_month"] = data["monthly_application_month"] ? convertToCEDate(data["monthly_application_month"]) : "";
    data["condition_end_date"] = data["condition_end_date"] ? convertToCEDate(data["condition_end_date"]) : "";
    data["report_date"] = data["report_date"] ? convertToCEDate(data["report_date"]) : "";
    data["arrival_date"] = data["arrival_date"] ? convertToCEDate(data["arrival_date"]) : "";

    // 例外處理 出租類型
    data["whole_floor_for_rent"] === "1" ? (data["building_rent_type"] = "1") : (data["building_rent_type"] = "2");
    return data;
};

// 取得變更出租人-自然人的所有欄位資料
function getChangeLessorModalAllfieldsValue(type) {
    let data = {};
    $(`#${type} input[type!='search'][name!='_token'][type!='file']:not([class~='kv-fileinput-caption']), #${type} select, #${type} textarea`).each((index, val) => {
        let field_id = $(val).attr("id");
        // 例外處理 checkbox 以及 radio 部分
        if ($(val).attr("type") === "checkbox" || $(val).attr("type") === "radio") {
            let field_val = $(val).is(":checked") ? "1" : "0";
            data[`${field_id}`] = field_val;
            return;
        }
        // 其他
        let field_val = $(val).val();
        data[`${field_id}`] = field_val;
    });
    // 例外處理 共同持分人
    let stakeholders = [];
    $(`#${type} .stakeholders-row`).each((idx, val) => {
        let name = $(val).find("input[name='name']").val();
        let is_foreigner = $(val).find("input[name='is_foreigner']").is(":checked") ? "1" : "0";
        let id_no = $(val).find("input[name='id_no']").val();
        let molecular = $(val).find("input[name='molecular']").val();
        let denominator = $(val).find("input[name='denominator']").val();
        stakeholders.push({
            name: name,
            is_foreigner: is_foreigner,
            id_no: id_no,
            molecular: molecular,
            denominator: denominator,
        });
    });
    stakeholders = JSON.stringify(stakeholders);
    data["stakeholders"] = stakeholders;

    // 例外處理 各種日期
    data["change_date"] = data["change_date"] ? convertToCEDate(data["change_date"]) : "";
    data["birthday"] = data["birthday"] ? convertToCEDate(data["birthday"]) : "";
    data["building_complete_date"] = data["building_complete_date"] ? convertToCEDate(data["building_complete_date"]) : "";
    console.log(data);
    return data;
}

// 初始化資料庫資料到畫面上
async function fillDataToEditPage(pageData) {
    $.each(pageData, async function (index, value) {
        // 預處理日期
        if (index === "apply_date" || index === "birthday" || index === "building_complete_date" || index === "entrusted_lease_start_date" || index === "entrusted_lease_end_date" || index === "entrusted_management_start_date" || index === "entrusted_management_end_date" || index === "condition_end_date") {
            console.log(value);
            value = value ? convertToRCDate(value) : "";
        }
        // checkbox處理
        if ($(`#${index}`).attr("type") === "checkbox") {
            let isChecked = value === "1" || value === 1 ? true : false;
            $(`#${index}`).prop("checked", isChecked);
            return;
        }
        // radio button 處理
        if (index === "building_rent_type") {
            value === "1" ? $("#whole_floor_for_rent").prop("checked", true) : $("#partial_rental").prop("checked", true);
        }
        if (index === "case_type_chartering") {
            value === 1 ? $("#case_type_chartering").prop("checked", true) : $("#case_type_escrow").prop("checked", true);
        }
        $(`#accordionPanelsStayOpenExample #${index}`).val(value);
    });
}

// 取得租客本人的弱勢項目
async function getTenantSelfWeakItem(id) {
    let retVal = null;
    const settings = {
        url: `/api/tenant_family_infos?tenant_id=${id}&appellation=本人`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal = response.data.data;
        console.log(response);
    });

    return retVal;
}

// 各頁面index清除搜尋部分的資料
function resetCheckBox() {
    $(".search-accordion input").val("");
    $(".search-accordion select").val("0");
}
