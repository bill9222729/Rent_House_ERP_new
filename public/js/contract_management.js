// 欄位驗證
function addFormValid(formId, callback = null) {
    $("#" + formId).validate({
        onkeyup: function (element, event) {
            //去除左側空白
            let value = this.elementValue(element).replace(/^\s+/g, "");
            $(element).val(value);
        },
        rules: {
            match_id: {
                required: true,
            },
            signing_date: {
                required: true,
            },
            has_tanants: {
                required: true,
            },
            payment_date: {
                required: true,
            },
            rent_pay_type: {
                required: true,
            },
            deposit_amount: {
                required: true,
            },
            notary_fees: {
                required: true,
            },
            matchmaking_fee: {
                required: true,
            },
            escrow_fee: {
                required: true,
            },
            management_fee: {
                required: true,
            },
            parking_fee: {
                required: true,
            },
            guarantee_fee: {
                required: true,
            },
            development_fee: {
                required: true,
            },
            tenant_case_id: {
                required: true,
            },
        },
        messages: {
            match_id: {
                required: "必填",
            },
            signing_date: {
                required: "必填",
            },
            has_tanants: {
                required: "必填",
            },
            payment_date: {
                required: "必填",
            },
            rent_pay_type: {
                required: "必填",
            },
            deposit_amount: {
                required: "必填",
            },
            notary_fees: {
                required: "必填",
            },
            matchmaking_fee: {
                required: "必填",
            },
            escrow_fee: {
                required: "必填",
            },
            management_fee: {
                required: "必填",
            },
            parking_fee: {
                required: "必填",
            },
            guarantee_fee: {
                required: "必填",
            },
            development_fee: {
                required: "必填",
            },
            tenant_case_id: {
                required: "必填",
            },
        },
        submitHandler: function (form) {
            callback();
        },
    });
}

// 新增合約的函式
function createContractManagementsData(requestData, alertConfig = null) {
    $.ajax({
        method: "POST",
        url: "/api/contract_managements",
        data: requestData,
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (alertConfig.hasAlert) {
                Swal.fire(alertConfig.title, alertConfig.text, alertConfig.type).then((result) => {
                    alertConfig.callback(result);
                });
            }
        },
        error: function (err) {
            console.error(err);
        },
    });
}

// 取得合約頁面上所有欄位的資料
function getContractDataInPage() {
    const allFields = $("input, select, textarea");
    const data = {};
    $(allFields).each((idx, item) => {
        let field_id = $(item).attr("id");
        // 例外處理 checkbox 以及 radio 部分
        if ($(item).attr("type") === "checkbox" || $(item).attr("type") === "radio") {
            let field_val = $(item).is(":checked") ? "1" : "0";
            data[`${field_id}`] = field_val;
            return;
        }
        // 其他
        let field_val = $(item).val();
        data[`${field_id}`] = field_val;
    });
    // 例外處理 各種日期
    data["signing_date"] = data["signing_date"] ? convertToCEDate(data["signing_date"]) : "";
    data["lease_start_date"] = data["lease_start_date"] ? convertToCEDate(data["lease_start_date"]) : "";
    data["lease_end_date"] = data["lease_end_date"] ? convertToCEDate(data["lease_end_date"]) : "";
    console.log("取得頁面所有資料：", data);
    return data;
}

// 取得對應媒合編號的合約資料
async function getContractDataByMatchId(match_id) {
    let retVal = {};
    await $.ajax({
        method: "GET",
        url: `/api/contract_managements?match_id=${match_id}`,
        dataType: "json",
        success: function (data) {
            console.log(data);
            retVal = data.data[0];
        },
        error: function (err) {
            console.error(err);
        },
    });
    console.log(`媒合編號${match_id}的合約資料：`, retVal);
    return retVal;
}

// 依照媒合編號去填入合約資料
async function fillContractFieldsByMatchId(match_id) {
    const contractData = await getContractDataByMatchId(match_id);
    // 填入欄位
    $("#has_tanants").val(contractData["has_tanants"]);
    $("#rent").val(contractData["rent"]);
    $("#rent_self_pay").val(contractData["rent_self_pay"]);
    $("#rent_government_pay").val(contractData["rent_government_pay"]);
    $("#payment_date").val(contractData["payment_date"]);
    $("#rent_pay_type").val(contractData["rent_pay_type"]);
    $("#bank_name").val(contractData["bank_name"]);
    $("#bank_branch_code").val(contractData["bank_branch_code"]);
    $("#bank_owner_name").val(contractData["bank_owner_name"]);
    $("#bank_account").val(contractData["bank_account"]);
    $("#deposit_amount").val(contractData["deposit_amount"]);
    $("#notary_fees").val(contractData["notary_fees"]);
    $("#notary_fees_share1").val(contractData["notary_fees_share1"]);
    $("#notary_fees_share2").val(contractData["notary_fees_share2"]);
    $("#development_fee").val(contractData["development_fee"]);
    $("#guarantee_fee").val(contractData["guarantee_fee"]);
    $("#management_fee").val(contractData["management_fee"]);
    $("#parking_fee").val(contractData["parking_fee"]);
    $("#contract_note").val(contractData["contract_note"]);
}

// 送出資料按鈕的事件
const changeDataStatusToAdmin = () => {
    Swal.fire({
        icon: 'info',
        title: '送出資料之後就不能修改了喔!',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: '送出',
        denyButtonText: `稍等`,
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
            method: "PATCH",
            url: `/api/contract_managements/${contractManagement.id}`,
            data: {data_status:"admin"},
            dataType: "json",
            success: function(data) {
                console.log(data);
                // 媒合的資料也要改
                $.ajax({
                    method: "PATCH",
                    url: `/api/match_managements/${matchManagementData.id}`,
                    data: {data_status:"admin"},
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已送出',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = `/matchManagements/${matchManagementData.id}/edit`;
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                })
            },
            error: function(err) {
                console.error(err);
            }
        })
        } else if (result.isDenied) {
          Swal.fire('資料還沒被送出', '', 'info')
        }
      })
}

// 退回資料按鈕的事件
const changeDataStatusToStaff = () => {
    Swal.fire({
        icon: 'info',
        title: '確定要退回資料給內勤修改？',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: '退回',
        denyButtonText: `稍等`,
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
            method: "PATCH",
            url: `/api/contract_managements/${contractManagement.id}`,
            data: {data_status:"staff"},
            dataType: "json",
            success: function(data) {
                // 媒合的資料也要退回
                $.ajax({
                    method: "PATCH",
                    url: `/api/match_managements/${matchManagementData.id}`,
                    data: {data_status:"staff"},
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已退回',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = `/matchManagements/${matchManagementData.id}/edit`;
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                })
            },
            error: function(err) {
                console.error(err);
            }
        })
        } else if (result.isDenied) {
          Swal.fire('資料還沒被退回', '', 'info')
        }
      })
}
