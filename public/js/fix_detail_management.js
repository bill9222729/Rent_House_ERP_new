// 新增租客資料的函式
const saveFixDetailData = () => {
    let requestData = getAllfieldsValue();
    $.ajax({
        method: "POST",
        url: "/api/fix_detail_managements",
        data: requestData,
        dataType: "json",
        success: function (data) {
            console.log(data);
            Swal.fire("成功", "資料已成功新增", "success").then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "/fixDetailManagements";
                }
            });
        },
        error: function (err) {
            console.error(err);
        },
    });
};

// 取得必要資料
const getRequiredData = async (building_id) => {
    let buildingData = null;
    let matchData = null;
    let contractData = null;
    let tenantData = null;
    // 取得物件資料
    await $.ajax({
        method: "GET",
        url: `/api/building_management_news?id=${building_id}`,
        success: function (data) {
            buildingData = data.data ? data.data[0] : {};
        },
    });
    // 取得媒合資料
    await $.ajax({
        method: "GET",
        url: `/api/match_managements?building_case_id=${building_id}`,
        success: function (data) {
            matchData = data.data ? data.data[0] : {};
        },
    });
    // 取得合約資料
    await $.ajax({
        method: "GET",
        url: `/api/contract_managements?match_id=${matchData ? matchData["id"] : ""}`,
        success: function (data) {
            contractData = data.data ? data.data[0] : {};
        },
    });
    // 取得租客資料
    if (!contractData) {
        tenantData = {};
    } else if (contractData.contract_type === "代管約" || contractData.contract_type === "轉租約") {
        await $.ajax({
            method: "GET",
            url: `/api/tenant_management_news/${matchData["tenant_case_id"]}`,
            success: function (data) {
                tenantData = data.data || {};
            },
        });
    } else if (contractData.contract_type === "包租約") {
        let subleaseData = null;
        // 先取得對應轉租約
        await $.ajax({
            method: "GET",
            url: `/api/match_managements?charter_case_id=${matchData["charter_case_id"]}`,
            success: function (data) {
                subleaseData = data.data ? data.data[0] : {};
            },
        });
        // 再從轉租約把租客資料拉出來
        await $.ajax({
            method: "GET",
            url: `/api/tenant_management_news/${subleaseData["tenant_case_id"]}`,
            success: function (data) {
                tenantData = data.data || {};
            },
        });
    }

    return {
        buildingData: buildingData,
        matchData: matchData,
        contractData: contractData,
        tenantData: tenantData,
    };
};

// 拼湊地址
const ganerateAddrFormBuildingData = (building_data) => {
    let building_address_num_hyphen = building_data["building_address_num_hyphen"] ? `之${building_data["building_address_num_hyphen"]}` : "";
    let building_address_num = building_data["building_address_num"] ? `${building_data["building_address_num"]}${building_address_num_hyphen}號` : "";
    let building_address_ln = building_data["building_address_ln"] ? `${building_data["building_address_ln"]}巷` : "";
    let building_address_aly = building_data["building_address_aly"] ? `${building_data["building_address_aly"]}弄` : "";
    let building_address_floor = building_data["building_address_floor"] ? `${building_data["building_address_floor"]}樓` : "";
    let building_address_floor_sub = building_data["building_address_floor_sub"] ? `之${building_data["building_address_floor_sub"]}` : "";
    let building_address_room_num = building_data["building_address_room_num"] ? `${building_data["building_address_room_num"]}室` : "";
    return `${building_data["building_address_city"] ? building_data["building_address_city"] : ""}` + `${building_data["building_address_city_area_name"] ? building_data["building_address_city_area_name"] : ""}` + `${building_data["building_address_street"] ? building_data["building_address_street"] : ""}` + `${building_address_ln}` + `${building_address_aly}` + `${building_address_num}` + `${building_address_floor}` + `${building_address_floor_sub}` + `${building_address_room_num}`;
};
