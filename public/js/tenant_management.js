// 欄位驗證
function addFormValid(formId, callback = null) {
    $('#' + formId).validate({
        onkeyup: function(element, event) {
            //去除左側空白
            let value = this.elementValue(element).replace(/^\s+/g, "");
            $(element).val(value);
        },
        rules: {
            case_status: {
                required: true
            },
            sales_id: {
                required: true
            },
            name: {
                required: true
            },
            gender: {
                required: true
            },
            birthday: {
                required: true
            },
            id_no: {
                required: true
            },
            cellphone: {
                required: true
            },
            membtype: {
                required: true
            },
            residence_city: {
                required: true
            },
            residence_city_area: {
                required: true
            },
            residence_addr: {
                required: true
            },
            mailing_city: {
                required: true
            },
            mailing_city_area: {
                required: true
            },
            mailing_addr: {
                required: true
            },
            house_no: {
                required: true
            },
        },
        messages: {
            case_status: {
                required: "必填"
            },
            sales_id: {
                required: "必填"
            },
            name: {
                required: "必填"
            },
            gender: {
                required: "必填"
            },
            birthday: {
                required: "必填"
            },
            id_no: {
                required: "必填"
            },
            cellphone: {
                required: "必填"
            },
            membtype: {
                required: "必填"
            },
            residence_city: {
                required: "必填"
            },
            residence_city_area: {
                required: "必填"
            },
            residence_addr: {
                required: "必填"
            },
            mailing_city: {
                required: "必填"
            },
            mailing_city_area: {
                required: "必填"
            },
            mailing_addr: {
                required: "必填"
            },
            house_no: {
                required: "必填"
            },
        },
        submitHandler: function(form) {
            callback();
        }
    });
}

// 送出資料按鈕的事件
const changeDataStatusToStaff = () => {
    Swal.fire({
        icon: 'info',
        title: '送出資料之後就不能修改了喔!',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: '送出',
        denyButtonText: `稍等`,
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
            method: "PATCH",
            url: `/api/tenant_management_news/${tenantManagementNew.id}`,
            data: {data_status:"staff"},
            dataType: "json",
            success: function(data) {
                console.log(data);
                Swal.fire(
                    '成功',
                    '資料已送出',
                    'success'
                ).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = "/tenantManagementNews";
                    }
                })
            },
            error: function(err) {
                console.error(err);
            }
        })
        } else if (result.isDenied) {
          Swal.fire('資料還沒被送出', '', 'info')
        }
      })
}

// 退回資料按鈕的事件
const changeDataStatusToSales = () => {
    Swal.fire({
        icon: 'info',
        title: '確定要退回資料給業務修改？',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: '退回',
        denyButtonText: `稍等`,
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
            method: "PATCH",
            url: `/api/tenant_management_news/${tenantManagementNew.id}`,
            data: {data_status:"sales"},
            dataType: "json",
            success: function(data) {
                console.log(data);
                Swal.fire(
                    '成功',
                    '資料已退回',
                    'success'
                ).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = "/tenantManagementNews";
                    }
                })
            },
            error: function(err) {
                console.error(err);
            }
        })
        } else if (result.isDenied) {
          Swal.fire('資料還沒被退回', '', 'info')
        }
      })
}
