// datatables 的初始設定
const usersDatatablesConfig = {
    pageLength: 20,
    order: [[0, "desc"]],
    searching: true,
    scrollX: true,
    dom: "Brtip",
    ajax: {
        type: "GET",
        url: `/api/users`,
        dataSrc: function (data) {
            const roleTranslate = {
                guest: "訪客",
                sales: "業務",
                admin: "管理員",
                staff: "內勤",
                accounting: "會計",
            };
            return data.data.map((item) => {
                item["role"] = item["role"] ? roleTranslate[item["role"]] : "";
                item["birthday"] = item["birthday"] ? convertToRCDate(item["birthday"]) : "";
                return item;
            });
        },
    },
    columns: [
        {
            data: "name",
            title: "姓名",
            className: "dt-body-center dt-head-center",
        },
        {
            data: "birthday",
            title: "生日",
            className: "dt-body-center dt-head-center",
        },
        {
            data: "cellphone",
            title: "手機",
            className: "dt-body-center dt-head-center",
        },
        {
            data: "company_cellphone",
            title: "公務機",
            className: "dt-body-center dt-head-center",
        },
        {
            data: "email",
            title: "電子郵件",
            className: "dt-body-center dt-head-center",
        },
        {
            data: "role",
            title: "類別",
            className: "dt-body-center dt-head-center",
        },
        {
            data: null,
            title: "操作功能",
            render: function (data, type, row) {
                return `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='/users/${data.id}/edit'">編輯</button >` + `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteUserData(${data.id},'users_table');">刪除</button >`;
            },
            className: "dt-body-center dt-head-center",
        },
    ],
    language: {
        url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json",
    },
};

// 刪除使用者的資料函式
async function deleteUserData(user_id, table_id) {
    Swal.fire({
        title: "確定要刪除這筆資料?",
        icon: "warning",
        showDenyButton: true,
        showCancelButton: true,
        showConfirmButton: false,
        cancelButtonText: "取消",
        denyButtonText: `刪除`,
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire("Saved!", "", "success");
        } else if (result.isDenied) {
            $.ajax({
                type: "DELETE",
                url: `/api/users/${user_id}`,
                success: function (data) {
                    $("#" + table_id)
                        .DataTable()
                        .ajax.reload();
                    Swal.fire("資料已被刪除", "", "success").then((result) => {
                        $("#" + table_id)
                            .DataTable()
                            .ajax.reload();
                    });
                },
            });
        }
    });
}

// 取得上傳的檔案
const initFileInput = async (fileType, dataId, inputId) => {
    let fileData = await getFileArr(fileType, dataId);

    $("#" + inputId).fileinput({
        language: "zh-TW",
        uploadUrl: "/api/file_managments",
        maxFileCount: 0,
        maxTotalFileCount: 0,
        uploadExtraData: {
            file_source_id: dataId,
            file_source: fileType,
        },
        overwriteInitial: false,
        initialPreview: fileData["initialPreview"],
        initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
        initialPreviewFileType: "pdf", // image is the default and can be overridden in config below
        initialPreviewConfig: fileData["initialPreviewConfig"],
        ajaxDeleteSettings: {
            type: "DELETE", // This should override the ajax as $.ajax({ type: 'DELETE' })
        },
    });

    return;
};

// 把伺服器拉出來的資料放到畫面上
const initUserField = (userData) => {
    // 取得所有欄位
    const AllFields = $("input[type != 'hidden'], select, textarea");
    // 例外處理原始資料的生日
    userData["birthday"] = userData["birthday"] ? convertToRCDate(userData["birthday"]) : "";
    userData["arrival_date"] = userData["arrival_date"] ? convertToRCDate(userData["arrival_date"]) : "";
    // 填入欄位
    AllFields.each((key, item) => {
        let itemName = $(item).attr("name");
        $(item).val(userData[itemName]);
    });
};

// 儲存會員資料
const updateUserData = (userId) => {
    let requestData = getAllfieldsValue();
    // 送給後端
    $.ajax({
        method: "PATCH",
        url: `/api/users/${userId}`,
        data: requestData,
        dataType: "json",
        success: function (data) {
            Swal.fire("成功", "資料已成功儲存", "success").then((e) => {
                window.location.href = "/users";
            });
        },
    });
    return;
};

// 鎖定或是解除會員資料
const controlUserLock = (user, lock) => {
    let requestData = {
        name : user["name"],
        email : user["email"],
        lock: lock ? 1 : 0,
    };
    // 送給後端
    $.ajax({
        method: "PATCH",
        url: `/api/users/${user["id"]}`,
        data: requestData,
        dataType: "json",
        success: function (data) {
            Swal.fire("成功", lock ? "會員已成功鎖定" : "會員已成功解鎖", "success").then((e) => {
                window.location.href = "/users";
            });
        },
    });
    return;
};
