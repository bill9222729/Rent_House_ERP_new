/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/modal';
import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap/dist/js/bootstrap.min.js';
// require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
require('./components/Example');
require('./components/AddressSelector');
require('./components/BankSelector');
require('./components/BuildingSharer');
require('./components/LegalAgentNum');
require('./components/CitySelector');
require('./components/SalesSelector');
require('./components/TenantHopeLocationInputs');
require('./components/IsHaveOtherSubsidy');
require('./components/ModalTenantFamilyInfo');
require('./components/ModalTenantMinorOffsprings');
require('./components/ModalAddEscrow');
require('./components/ModalAddCharter');
require('./components/ModalAddSublease');
require('./components/IsLiveTogether');
require('./components/tenant_management/WeakConditions');
require('./components/tenant_management/CaseNoInput');
require('./components/building_manaement/MeterToPingInput');
require('./components/match_managements/ModalStopContract');
require('./components/match_managements/TenantSelect');
require('./components/match_managements/NetPriceLogin');
require('./components/users/IndexPage');
