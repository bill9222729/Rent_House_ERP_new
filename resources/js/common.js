// 西元年轉民國年
export const convertToRCDate = (date) => {
    if (typeof date === "object") {
        return date.getFullYear() - 1911 + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getDate();
    } else {
        date = new Date(date);
        return (date.getFullYear() - 1911).toString().padStart(3, "0") + "-" + ("0" + (date.getMonth() + 1)).slice(-2).padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
    }
};

// 民國年轉西元年
export const convertToCEDate = (date) => {
    if (typeof date === "object") {
        return date.getFullYear() + 1911 + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getDate();
    } else {
        let dateArr = date.split("-");
        dateArr[0] = dateArr[0].padStart(4, "0");
        dateArr[1] = dateArr[1].padStart(2, "0");
        dateArr[2] = dateArr[2].padStart(2, "0");
        date = new Date(dateArr.join("-"));
        return date.getFullYear() + 1911 + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getDate() ;
    }
};
