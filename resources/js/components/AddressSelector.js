import React from 'react';
import ReactDOM from 'react-dom';

function AddressSelector(prop) {
    const axios = require('axios').default;
    const [cityArr, setCityArr] = React.useState();
    const [cityAreaArr, setCityAreaArr] = React.useState();
    const handleCitySelectChange = (e) => {
        let cityName = e.target.value;
        getCityAreaArr(cityName);
    }
    // 取得地區陣列
    const getCityAreaArr = (cityName) => {
        axios.get(`/api/city_areas?city_value=${cityName}`)
            .then(function (response) {
                // handle success
                setCityAreaArr(response.data.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }
    // 取得縣市陣列
    const geyCityArr = () => {
        axios.get('/api/cities')
            .then(function (response) {
                // handle success
                setCityArr(response.data.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }
    React.useEffect(() => {
        geyCityArr();
    }, []);

    return (
        <>
            <div className="col-sm-6">
                <select className="form-select form-control" id="address_city" onChange={handleCitySelectChange}>
                    <option value="0" defaultValue>請選擇</option>
                    {cityArr ? cityArr.map((value, idx) => {
                        return (<option key={idx} value={value.name}>{value.name}</option>);
                    }) : ""}
                </select>
            </div>
            <div className="col-sm-6">
                <select className="form-select form-control" id="address_area">
                    <option value="0" defaultValue>請選擇</option>
                    {cityAreaArr ? cityAreaArr.map((value, idx) => {
                        return (<option key={idx} value={value.name}>{value.name}</option>);
                    }) : ""}
                </select>
            </div>
        </>
    );
}

export default AddressSelector;

if (document.getElementById('AddressSelector')) {
    ReactDOM.render(<AddressSelector />, document.getElementById('AddressSelector'));
}
