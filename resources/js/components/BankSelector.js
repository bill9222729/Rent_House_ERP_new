import React from "react";
import ReactDOM from "react-dom";

const defaultData = {
    bank_name: "806",
    bank_branch_code: "0482",
    bank_owner_name: "易居管理顧問股份有限公司",
    bank_account: "",
};
const BankSelector = (props) => {
    console.log(props);
    const [data, setData] = React.useState(defaultData);
    const outerData = JSON.parse(props.data);
    console.log(outerData);
    const handleInputChange = (e) => {
        let newData = { ...data };
        let inputName = e.target.name;
        let inputValue = e.target.value;
        newData[inputName] = inputValue;
        setData(newData);
    };

    React.useEffect(() => {
        if (outerData) {
            setData({
                bank_name: outerData.bank_name,
                bank_branch_code: outerData.bank_branch_code,
                bank_owner_name: outerData.bank_owner_name,
                bank_account: outerData.bank_account,
            });
        }
    }, []);

    return (
        <>
            <div className="input-group mt-3">
                <label className="input-group-text" htmlFor="bank_name">
                    金融機構
                </label>
                <select className="form-select form-control selectpicker" value={data.bank_name || "806"} name="bank_name" id="bank_name" data-live-search="true" data-size="5" data-style="border text-dark bg-white" onChange={(e) => handleInputChange(e)}>
                    <option value="">請選擇</option>
                    <option value="000">000:中央銀行國庫局</option>
                    <option value="004">004:臺灣銀行</option>
                    <option value="005">005:土銀</option>
                    <option value="006">006:合庫</option>
                    <option value="007">007:第一銀行</option>
                    <option value="008">008:華南銀行</option>
                    <option value="009">009:彰化銀行</option>
                    <option value="011">011:上海銀行</option>
                    <option value="012">012:台北富邦</option>
                    <option value="013">013:國泰世華</option>
                    <option value="016">016:高雄銀行</option>
                    <option value="017">017:兆豐商銀</option>
                    <option value="018">018:農業金庫</option>
                    <option value="0200017">0200017:日商瑞穗實業</option>
                    <option value="021">021:花旗(台灣)銀行</option>
                    <option value="022">022:美國銀行台北分行</option>
                    <option value="0220019">0220019:美國銀行</option>
                    <option value="025">025:首都銀行</option>
                    <option value="0250012">0250012:菲律賓首都</option>
                    <option value="039">039:澳商澳盛銀行(原荷蘭銀行)</option>
                    <option value="040">040:中華開發</option>
                    <option value="048">048:王道商業銀行</option>
                    <option value="050">050:臺灣企銀</option>
                    <option value="052">052:渣打國際商銀</option>
                    <option value="053">053:台中商銀</option>
                    <option value="054">054:京城商銀</option>
                    <option value="072">072:德意志銀行</option>
                    <option value="0720014">0720014:德商德意志</option>
                    <option value="075">075:東亞銀行</option>
                    <option value="0750006">0750006:香港商東亞</option>
                    <option value="076">076:美商摩根大通銀行台北分行</option>
                    <option value="0760018">0760018:美商摩根大通</option>
                    <option value="081">081:滙豐銀行</option>
                    <option value="0820017">0820017:法商巴黎</option>
                    <option value="0860011">0860011:法商東方匯理</option>
                    <option value="0920010">0920010:瑞商瑞士</option>
                    <option value="101">101:瑞興銀行</option>
                    <option value="102">102:華泰銀行</option>
                    <option value="103">103:臺灣新光商銀</option>
                    <option value="104">104:台北五信</option>
                    <option value="1040018">1040018:台北市五信</option>
                    <option value="106">106:台北九信</option>
                    <option value="108">108:陽信銀行</option>
                    <option value="114">114:基隆一信</option>
                    <option value="1140015">1140015:基隆一信</option>
                    <option value="115">115:基隆二信</option>
                    <option value="1150014">1150014:基隆市二信</option>
                    <option value="118">118:板信銀行</option>
                    <option value="119">119:淡水一信</option>
                    <option value="1190010">1190010:淡水一信</option>
                    <option value="120">120:淡水信合社</option>
                    <option value="1200016">1200016:淡水信合社</option>
                    <option value="124">124:宜蘭信合社</option>
                    <option value="1240012">1240012:宜蘭信合社</option>
                    <option value="127">127:桃園信合社</option>
                    <option value="1270019">1270019:桃園信合社</option>
                    <option value="130">130:新竹一信</option>
                    <option value="1300013">1300013:新竹一信</option>
                    <option value="132">132:新竹三信</option>
                    <option value="1320039">1320039:新竹三信</option>
                    <option value="139">139:竹南信合社</option>
                    <option value="146">146:台中二信</option>
                    <option value="1460014">1460014:台中二信</option>
                    <option value="147">147:三信銀行</option>
                    <option value="158">158:彰化一信</option>
                    <option value="1580019">1580019:彰化市一信</option>
                    <option value="161">161:彰化五信</option>
                    <option value="1610013">1610013:彰化市五信</option>
                    <option value="162">162:彰化六信</option>
                    <option value="1620021">1620021:彰化六信</option>
                    <option value="163">163:彰化十信</option>
                    <option value="1630011">1630011:彰化十信</option>
                    <option value="165">165:鹿港信合社</option>
                    <option value="1650019">1650019:鹿港信合社</option>
                    <option value="178">178:嘉義三信</option>
                    <option value="1780013">1780013:嘉義三信</option>
                    <option value="179">179:嘉義四信</option>
                    <option value="188">188:台南三信</option>
                    <option value="1880010">1880010:台南市三信</option>
                    <option value="204">204:高雄三信</option>
                    <option value="2040011">2040011:高雄三信</option>
                    <option value="215">215:花蓮一信</option>
                    <option value="2150044">2150044:花蓮一信</option>
                    <option value="216">216:花蓮二信</option>
                    <option value="2160016">2160016:花蓮二信</option>
                    <option value="222">222:澎湖一信</option>
                    <option value="2220017">2220017:澎湖一信</option>
                    <option value="223">223:澎湖二信</option>
                    <option value="2230016">2230016:澎湖二信</option>
                    <option value="224">224:金門信合社</option>
                    <option value="2240015">2240015:金門縣信合社</option>
                    <option value="5010017">5010017:蘇澳區漁會</option>
                    <option value="5020018">5020018:頭城區漁會</option>
                    <option value="5060012">5060012:桃園區漁會</option>
                    <option value="5070013">5070013:新竹區漁會</option>
                    <option value="5080014">5080014:通苑區漁會</option>
                    <option value="5100019">5100019:南龍區漁會</option>
                    <option value="5110010">5110010:彰化區漁會</option>
                    <option value="512">512:漁會</option>
                    <option value="5120011">5120011:雲林區漁會</option>
                    <option value="5130012">5130012:瑞芳區漁會</option>
                    <option value="5140013">5140013:萬里區漁會</option>
                    <option value="5150014">5150014:嘉義區漁會</option>
                    <option value="5160015">5160015:基隆區漁會</option>
                    <option value="5170016">5170016:南市區漁會</option>
                    <option value="5180017">5180017:南縣區漁會</option>
                    <option value="5190018">5190018:新化區農會</option>
                    <option value="5200012">5200012:高雄區漁會</option>
                    <option value="5200023">5200023:小港區漁會</option>
                    <option value="5210024">5210024:興達港區漁會</option>
                    <option value="5210068">5210068:林園區漁會</option>
                    <option value="5210079">5210079:彌陀區漁會</option>
                    <option value="5210091">5210091:永安區漁會</option>
                    <option value="5210116">5210116:梓官區漁會</option>
                    <option value="5230015">5230015:琉球區漁會</option>
                    <option value="5230037">5230037:東港區漁會</option>
                    <option value="5230048">5230048:林邊區漁會</option>
                    <option value="5230130">5230130:枋寮區漁會</option>
                    <option value="5240016">5240016:新港區漁會</option>
                    <option value="5250017">5250017:澎湖區漁會</option>
                    <option value="5260018">5260018:金門區漁會</option>
                    <option value="5380013">5380013:宜蘭市農會</option>
                    <option value="5410019">5410019:白河區農會</option>
                    <option value="5420010">5420010:麻豆區農會</option>
                    <option value="5470015">5470015:後壁區農會</option>
                    <option value="5490017">5490017:下營區農會</option>
                    <option value="5510012">5510012:官田區農會</option>
                    <option value="5520013">5520013:大內區農會</option>
                    <option value="5560017">5560017:學甲區農會</option>
                    <option value="5570018">5570018:新市區農會</option>
                    <option value="5580019">5580019:安定區農會</option>
                    <option value="5590010">5590010:山上區農會</option>
                    <option value="5610015">5610015:左鎮區農會</option>
                    <option value="5620016">5620016:仁德區農會</option>
                    <option value="5640018">5640018:關廟區農會</option>
                    <option value="5650019">5650019:龍崎區農會</option>
                    <option value="5670011">5670011:南化區農會</option>
                    <option value="5680012">5680012:七股區農會</option>
                    <option value="5700017">5700017:南投市農會</option>
                    <option value="5730010">5730010:埔里鎮農會</option>
                    <option value="5740011">5740011:竹山鎮農會</option>
                    <option value="5750012">5750012:中寮鄉農會</option>
                    <option value="5770014">5770014:魚池鄉農會</option>
                    <option value="5780015">5780015:水里鄉農會</option>
                    <option value="5790016">5790016:國姓鄉農會</option>
                    <option value="5800010">5800010:鹿谷鄉農會</option>
                    <option value="5810011">5810011:信義鄉農會</option>
                    <option value="5820012">5820012:仁愛鄉農會</option>
                    <option value="5830013">5830013:東山區農會</option>
                    <option value="5850015">5850015:頭城鎮農會</option>
                    <option value="5860016">5860016:羅東鎮農會</option>
                    <option value="5870017">5870017:礁溪鄉農會</option>
                    <option value="5880018">5880018:壯圍鄉農會</option>
                    <option value="5890019">5890019:員山鄉農會</option>
                    <option value="5960019">5960019:五結鄉農會</option>
                    <option value="5980011">5980011:蘇澳地區農會</option>
                    <option value="5990012">5990012:三星地區農會</option>
                    <option value="6020015">6020015:中華民國農會</option>
                    <option value="6050018">6050018:高雄地區農會</option>
                    <option value="6051336">6051336:小港區農會</option>
                    <option value="6121107">6121107:豐原區農會</option>
                    <option value="6121118">6121118:神岡區農會</option>
                    <option value="6130031">6130031:集集鎮農會</option>
                    <option value="6130075">6130075:名間鄉農會</option>
                    <option value="6140043">6140043:員林市農會</option>
                    <option value="6140087">6140087:二林鎮農會</option>
                    <option value="6140124">6140124:秀水鄉農會</option>
                    <option value="6140179">6140179:埔心鄉農會</option>
                    <option value="6140180">6140180:永靖鄉農會</option>
                    <option value="6140227">6140227:埤頭鄉農會</option>
                    <option value="6140261">6140261:竹塘鄉農會</option>
                    <option value="6141327">6141327:芬園鄉農會</option>
                    <option value="6141338">6141338:芳苑鄉農會</option>
                    <option value="6150011">6150011:基隆市農會</option>
                    <option value="6160012">6160012:斗六市農會</option>
                    <option value="6160023">6160023:虎尾鎮農會</option>
                    <option value="6160034">6160034:西螺鎮農會</option>
                    <option value="6160056">6160056:斗南鎮農會</option>
                    <option value="6160078">6160078:古坑鄉農會</option>
                    <option value="6160089">6160089:大埤鄉農會</option>
                    <option value="6160090">6160090:莿桐鄉農會</option>
                    <option value="6160115">6160115:二崙鄉農會</option>
                    <option value="6160126">6160126:崙背鄉農會</option>
                    <option value="6160137">6160137:台西鄉農會</option>
                    <option value="6160148">6160148:褒忠鄉農會</option>
                    <option value="6160160">6160160:四湖鄉農會</option>
                    <option value="6160171">6160171:口湖鄉農會</option>
                    <option value="617">617:南區農會</option>
                    <option value="6170013">6170013:嘉義市農會</option>
                    <option value="6170024">6170024:朴子市農會</option>
                    <option value="6170035">6170035:布袋鎮農會</option>
                    <option value="6170046">6170046:大林鎮農會</option>
                    <option value="6170057">6170057:民雄鄉農會</option>
                    <option value="6170068">6170068:溪口鄉農會</option>
                    <option value="6170079">6170079:東石鄉農會</option>
                    <option value="6170080">6170080:義竹鄉農會</option>
                    <option value="6170091">6170091:鹿草鄉農會</option>
                    <option value="6170105">6170105:太保市農會</option>
                    <option value="6170116">6170116:水上鄉農會</option>
                    <option value="6170138">6170138:番路鄉農會</option>
                    <option value="6170149">6170149:竹崎地區農會</option>
                    <option value="6170150">6170150:梅山鄉農會</option>
                    <option value="6170161">6170161:新港鄉農會</option>
                    <option value="6170172">6170172:六腳鄉農會</option>
                    <option value="6170806">6170806:六腳鄉農會</option>
                    <option value="6170817">6170817:新港鄉農會</option>
                    <option value="6170862">6170862:嘉義市農會</option>
                    <option value="618">618:永康農會</option>
                    <option value="6180025">6180025:新營區農會</option>
                    <option value="6180036">6180036:鹽水區農會</option>
                    <option value="6180069">6180069:佳里區農會</option>
                    <option value="6180081">6180081:善化區農會</option>
                    <option value="6180092">6180092:柳營區農會</option>
                    <option value="6180139">6180139:六甲區農會</option>
                    <option value="6180162">6180162:西港區農會</option>
                    <option value="6180184">6180184:將軍區農會</option>
                    <option value="6180195">6180195:北門區農會</option>
                    <option value="6180243">6180243:玉井區農會</option>
                    <option value="6180298">6180298:歸仁區農會</option>
                    <option value="6180324">6180324:永康區農會</option>
                    <option value="6181402">6181402:楠西區農會</option>
                    <option value="619">619:農會</option>
                    <option value="6190015">6190015:鳳山區農會</option>
                    <option value="6190026">6190026:岡山區農會</option>
                    <option value="6190037">6190037:旗山區農會</option>
                    <option value="6190048">6190048:美濃區農會</option>
                    <option value="6190059">6190059:橋頭區農會</option>
                    <option value="6190060">6190060:燕巢區農會</option>
                    <option value="6190071">6190071:田寮區農會</option>
                    <option value="6190082">6190082:阿蓮區農會</option>
                    <option value="6190093">6190093:路竹區農會</option>
                    <option value="6190107">6190107:湖內區農會</option>
                    <option value="6190118">6190118:茄萣區農會</option>
                    <option value="6190129">6190129:彌陀區農會</option>
                    <option value="6190130">6190130:永安區農會</option>
                    <option value="6190141">6190141:梓官區農會</option>
                    <option value="6190185">6190185:林園區農會</option>
                    <option value="6190196">6190196:大寮區農會</option>
                    <option value="6190211">6190211:仁武區農會</option>
                    <option value="6190222">6190222:大社區農會</option>
                    <option value="6190244">6190244:杉林區農會</option>
                    <option value="6190255">6190255:甲仙地區農會</option>
                    <option value="6190303">6190303:六龜區農會</option>
                    <option value="6191089">6191089:鳥松區農會</option>
                    <option value="6191104">6191104:大樹區農會</option>
                    <option value="6191115">6191115:內門區農會</option>
                    <option value="6200031">6200031:東港鎮農會</option>
                    <option value="6200042">6200042:恆春鎮農會</option>
                    <option value="6200075">6200075:麟洛鄉農會</option>
                    <option value="6200086">6200086:九如鄉農會</option>
                    <option value="6200112">6200112:里港鄉農會</option>
                    <option value="6200167">6200167:崁頂鄉農會</option>
                    <option value="6200189">6200189:南州地區農會</option>
                    <option value="6200204">6200204:琉球鄉農會</option>
                    <option value="6200215">6200215:滿州鄉農會</option>
                    <option value="6200259">6200259:枋山地區農會</option>
                    <option value="6200868">6200868:屏東市農會</option>
                    <option value="6200879">6200879:車城地區農會</option>
                    <option value="6200880">6200880:屏東縣農會</option>
                    <option value="6200891">6200891:枋寮地區農會</option>
                    <option value="6200905">6200905:竹田鄉農會</option>
                    <option value="6200916">6200916:萬丹鄉農會</option>
                    <option value="6200927">6200927:長治鄉農會</option>
                    <option value="6200938">6200938:林邊鄉農會</option>
                    <option value="6200949">6200949:佳冬鄉農會</option>
                    <option value="6200961">6200961:高樹鄉農會</option>
                    <option value="6200972">6200972:萬巒地區農會</option>
                    <option value="6200983">6200983:潮州鎮農會</option>
                    <option value="6200994">6200994:新園鄉農會</option>
                    <option value="6210021">6210021:吉安鄉農會</option>
                    <option value="6210032">6210032:壽豐鄉農會</option>
                    <option value="6210043">6210043:富里鄉農會</option>
                    <option value="6210098">6210098:新秀地區農會</option>
                    <option value="6220011">6220011:關山鎮農會</option>
                    <option value="6220022">6220022:成功鎮農會</option>
                    <option value="6220033">6220033:池上鄉農會</option>
                    <option value="6220044">6220044:東河鄉農會</option>
                    <option value="6220055">6220055:長濱鄉農會</option>
                    <option value="6220066">6220066:台東地區農會</option>
                    <option value="6220077">6220077:鹿野鄉農會</option>
                    <option value="6220088">6220088:太麻里地區農會</option>
                    <option value="6220103">6220103:東河鄉農會</option>
                    <option value="6240013">6240013:澎湖縣農會</option>
                    <option value="6250014">6250014:臺中地區農會</option>
                    <option value="6270016">6270016:連江縣農會</option>
                    <option value="6280017">6280017:鹿港鎮農會</option>
                    <option value="6290018">6290018:和美鎮農會</option>
                    <option value="6310013">6310013:溪湖鎮農會</option>
                    <option value="6320014">6320014:田中鎮農會</option>
                    <option value="6330015">6330015:北斗鎮農會</option>
                    <option value="6350017">6350017:線西鄉農會</option>
                    <option value="6360018">6360018:伸港鄉農會</option>
                    <option value="6380010">6380010:花壇鄉農會</option>
                    <option value="6390011">6390011:大村鄉農會</option>
                    <option value="6420017">6420017:社頭鄉農會</option>
                    <option value="6430018">6430018:二水鄉農會</option>
                    <option value="6460011">6460011:大城鄉農會</option>
                    <option value="6470012">6470012:溪州鄉農會</option>
                    <option value="6490014">6490014:埔鹽鄉農會</option>
                    <option value="6500018">6500018:福興鄉農會</option>
                    <option value="6510019">6510019:彰化市農會</option>
                    <option value="6830010">6830010:北港鎮農會</option>
                    <option value="6850012">6850012:土庫鎮農會</option>
                    <option value="6930013">6930013:東勢鄉農會</option>
                    <option value="6960016">6960016:水林鄉農會</option>
                    <option value="6970017">6970017:元長鄉農會</option>
                    <option value="6980018">6980018:麥寮鄉農會</option>
                    <option value="6990019">6990019:林內鄉農會</option>
                    <option value="700">700:郵局</option>
                    <option value="7490011">7490011:內埔地區農會</option>
                    <option value="7620010">7620010:大溪區農會</option>
                    <option value="7630011">7630011:桃園區農會</option>
                    <option value="7640012">7640012:平鎮區農會</option>
                    <option value="7650013">7650013:楊梅區農會</option>
                    <option value="7660014">7660014:大園區農會</option>
                    <option value="7670015">7670015:蘆竹區農會</option>
                    <option value="7680016">7680016:龜山區農會</option>
                    <option value="7690017">7690017:八德區農會</option>
                    <option value="7700011">7700011:新屋區農會</option>
                    <option value="7710012">7710012:龍潭區農會</option>
                    <option value="7720013">7720013:復興區農會</option>
                    <option value="7730014">7730014:觀音區農會</option>
                    <option value="7750016">7750016:土城區農會</option>
                    <option value="7760017">7760017:三重區農會</option>
                    <option value="7770018">7770018:中和地區農會</option>
                    <option value="7780019">7780019:淡水區農會</option>
                    <option value="7790010">7790010:樹林區農會</option>
                    <option value="7800014">7800014:鶯歌區農會</option>
                    <option value="7810015">7810015:三峽區農會</option>
                    <option value="7850019">7850019:蘆洲區農會</option>
                    <option value="7860010">7860010:五股區農會</option>
                    <option value="7870011">7870011:林口區農會</option>
                    <option value="7880012">7880012:泰山區農會</option>
                    <option value="7890013">7890013:坪林區農會</option>
                    <option value="7900017">7900017:八里區農會</option>
                    <option value="7910018">7910018:金山地區農會</option>
                    <option value="7920019">7920019:瑞芳地區農會</option>
                    <option value="7930010">7930010:新店地區農會</option>
                    <option value="7950012">7950012:深坑區農會</option>
                    <option value="7960013">7960013:石碇區農會</option>
                    <option value="7970014">7970014:平溪區農會</option>
                    <option value="7980015">7980015:石門區農會</option>
                    <option value="7990016">7990016:三芝區農會</option>
                    <option value="803">803:聯邦銀行</option>
                    <option value="805">805:遠東銀行</option>
                    <option value="806">806:元大銀行</option>
                    <option value="807">807:永豐銀行</option>
                    <option value="808">808:玉山銀行</option>
                    <option value="809">809:凱基商業銀行</option>
                    <option value="810">810:星展銀行</option>
                    <option value="812">812:台新銀行</option>
                    <option value="814">814:大眾銀行</option>
                    <option value="815">815:日盛銀行</option>
                    <option value="816">816:安泰銀行</option>
                    <option value="822">822:中國信託</option>
                    <option value="824">824:連線銀行</option>
                    <option value="8600015">8600015:中埔鄉農會</option>
                    <option value="8660011">8660011:阿里山鄉農會</option>
                    <option value="8680013">8680013:東勢區農會</option>
                    <option value="8690014">8690014:清水區農會</option>
                    <option value="8700018">8700018:梧棲區農會</option>
                    <option value="8710019">8710019:大甲區農會</option>
                    <option value="8720010">8720010:沙鹿區農會</option>
                    <option value="8740012">8740012:霧峰區農會</option>
                    <option value="8750013">8750013:太平區農會</option>
                    <option value="8760014">8760014:烏日區農會</option>
                    <option value="8770015">8770015:后里區農會</option>
                    <option value="8780016">8780016:大雅區農會</option>
                    <option value="8790017">8790017:潭子區農會</option>
                    <option value="8800011">8800011:石岡區農會</option>
                    <option value="8810012">8810012:新社區農會</option>
                    <option value="8820013">8820013:大肚區農會</option>
                    <option value="8830014">8830014:外埔區農會</option>
                    <option value="8840015">8840015:大安區農會</option>
                    <option value="8850016">8850016:龍井區農會</option>
                    <option value="8860017">8860017:和平區農會</option>
                    <option value="8910015">8910015:花蓮市農會</option>
                    <option value="8950019">8950019:瑞穗鄉農會</option>
                    <option value="8960010">8960010:玉溪地區農會</option>
                    <option value="8970011">8970011:鳳榮地區農會</option>
                    <option value="8980012">8980012:光豐地區農會</option>
                    <option value="9010015">9010015:大里區農會</option>
                    <option value="9020016">9020016:苗栗市農會</option>
                    <option value="9030017">9030017:汐止區農會</option>
                    <option value="9040018">9040018:新莊區農會</option>
                    <option value="9060010">9060010:頭份市農會</option>
                    <option value="9070011">9070011:竹南鎮農會</option>
                    <option value="9080012">9080012:通霄鎮農會</option>
                    <option value="9090013">9090013:苑裡鎮農會</option>
                    <option value="9120019">9120019:冬山鄉農會</option>
                    <option value="9130010">9130010:後龍鎮農會</option>
                    <option value="9140011">9140011:卓蘭鎮農會</option>
                    <option value="9150012">9150012:西湖鄉農會</option>
                    <option value="9160013">9160013:草屯鎮農會</option>
                    <option value="9170014">9170014:公館鄉農會</option>
                    <option value="9180015">9180015:銅鑼鄉農會</option>
                    <option value="9190016">9190016:三義鄉農會</option>
                    <option value="9200010">9200010:造橋鄉農會</option>
                    <option value="9210011">9210011:南庄鄉農會</option>
                    <option value="9220012">9220012:臺南地區農會</option>
                    <option value="9230013">9230013:獅潭鄉農會</option>
                    <option value="9240014">9240014:頭屋鄉農會</option>
                    <option value="9250015">9250015:三灣鄉農會</option>
                    <option value="9260016">9260016:大湖地區農會</option>
                    <option value="9280018">9280018:板橋區農會</option>
                    <option value="9290019">9290019:關西鎮農會</option>
                    <option value="9300013">9300013:新埔鎮農會</option>
                    <option value="9310014">9310014:竹北市農會</option>
                    <option value="9320015">9320015:湖口鄉農會</option>
                    <option value="9330016">9330016:芎林鄉農會</option>
                    <option value="9340017">9340017:寶山鄉農會</option>
                    <option value="9350018">9350018:峨眉鄉農會</option>
                    <option value="9360019">9360019:北埔鄉農會</option>
                    <option value="9370010">9370010:竹東地區農會</option>
                    <option value="9380011">9380011:橫山地區農會</option>
                    <option value="9390012">9390012:新豐鄉農會</option>
                    <option value="9400016">9400016:新竹市農會</option>
                    <option value="9520103">9520103:小港區農會</option>
                    <option value="9530012">9530012:田尾鄉農會</option>
                    <option value="9840012">9840012:北投區農會</option>
                    <option value="9850013">9850013:士林區農會</option>
                    <option value="9860014">9860014:內湖區農會</option>
                    <option value="9870015">9870015:南港區農會</option>
                    <option value="9880016">9880016:木柵區農會</option>
                    <option value="9890017">9890017:景美區農會</option>
                </select>
            </div>
            <div className="input-group mt-3">
                <label className="input-group-text" htmlFor="bank_branch_code">
                    分行代碼
                </label>
                <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    value={data.bank_branch_code || "0482"}
                    name="bank_branch_code"
                    id="bank_branch_code"
                    onChange={(e) => {
                        handleInputChange(e);
                    }}
                />
            </div>
            <div className="input-group mt-3">
                <label className="input-group-text" htmlFor="bank_owner_name">
                    &emsp;戶名&emsp;
                </label>
                <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    value={data.bank_owner_name || "易居管理顧問股份有限公司"}
                    name="bank_owner_name"
                    id="bank_owner_name"
                    onChange={(e) => {
                        handleInputChange(e);
                    }}
                />
            </div>
            <div className="input-group mt-3">
                <label className="input-group-text" htmlFor="bank_account">
                    &emsp;帳號&emsp;
                </label>
                <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    value={data.bank_account || ""}
                    name="bank_account"
                    id="bank_account"
                    onChange={(e) => {
                        handleInputChange(e);
                    }}
                />
            </div>
        </>
    );
};

export default BankSelector;

const el = document.getElementsByClassName("bankSelector");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<BankSelector data={item.getAttribute("data-param") || {}} />, item);
    });
}
