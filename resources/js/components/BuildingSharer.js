import React from 'react'
import ReactDOM from 'react-dom';

const BuildingSharer = (props) => {
    const [dataList, setDataList] = React.useState(JSON.parse(props.data));
    console.log(dataList);

    // 新增一筆資料
    const addNewData = () => {
        const temp = [...dataList];
        if (temp.length < 5) {
            temp.push({ name: '', is_foreigner: '', id_no: '', molecular: '', denominator: '' });
            setDataList(temp);
        }
    }

    // 刪除一筆資料
    const delData = (itemIndex) => {
        let temp = [...dataList];
        if (temp.length > 0) {
            temp.splice(itemIndex, 1);
            setDataList(temp);
        }
    }

    const BuildingSharerItem = (prop) => {
        const [data, setData] = React.useState(prop.data);

        // 當input失去焦點時觸發
        const handleDataChange = (index) => {
            let temp = [...dataList];
            temp[index] = data;
            setDataList(temp);
        }
        // 當input value更改時觸發
        const handleInputChange = (inputName, value) => {
            let temp = { ...data };
            if (inputName === "is_foreigner") {
                temp[inputName] = temp[inputName] === "1" ? "0" : "1";
            } else {
                temp[inputName] = value;
            }
            setData(temp);
        }

        console.log(data);

        return (
            <div className="row col-md-12 text-nowrap align-items-end justify-content-center stakeholders-row">
                <div className="row col-1 text-nowrap align-self-center">
                    {prop.order + 1}.
                </div>
                <div className="row col-3 text-nowrap align-items-center">
                    <div className="col-auto align-items-center">
                        持分人姓名
                    </div>
                    <div className="col-auto align-items-center">
                        <input name="name" className="form-control " type="text" id={`stakeholders_name_${prop.order + 1}`} value={data.name} onChange={(e) => handleInputChange(e.target.name, e.target.value)} onBlur={(e) => { handleDataChange(prop.order) }} />
                    </div>
                </div>
                <div className="row col-md-3 text-nowrap align-items-center">
                    <div className="col-auto align-items-center">
                        持分人身分/居留證號
                    </div>
                    <div className="col-auto align-items-center">
                        <div className="input-group">
                            <div className="input-group-text">
                                <input name="is_foreigner" className="form-check-input mt-0 " type="checkbox" value="" id={`stakeholders_is_foreigner_${prop.order + 1}`} checked={data.is_foreigner === "1" ? true : false} onChange={(e) => handleInputChange(e.target.name, e.target.value)} onBlur={(e) => { handleDataChange(prop.order) }} />
                                外籍
                            </div>
                            <input name="id_no" type="text" className="form-control " id={`stakeholders_id_no_${prop.order + 1}`} value={data.id_no} onChange={(e) => handleInputChange(e.target.name, e.target.value)} onBlur={(e) => { handleDataChange(prop.order) }} />
                        </div>
                    </div>
                </div>
                <div className="row col-md-3 text-nowrap align-items-center">
                    <div className="col-12 align-items-center">
                        持有比例
                    </div>
                    <div className="col-7 align-items-center">
                        <div className="input-group">
                            <input name="molecular" type="text" id={`stakeholders_molecular_${prop.order + 1}`} className="form-control"
                                placeholder="" value={data.molecular} onChange={(e) => handleInputChange(e.target.name, e.target.value)} onBlur={(e) => { handleDataChange(prop.order, e.target.name, e.target.value) }} />
                            <span className="input-group-text">/</span>
                            <input name="denominator" type="text" id={`stakeholders_denominator_${prop.order + 1}`} className="form-control"
                                placeholder="" value={data.denominator} onChange={(e) => handleInputChange(e.target.name, e.target.value)} onBlur={(e) => { handleDataChange(prop.order, e.target.name, e.target.value) }} />
                        </div>
                    </div>
                </div>
                <div className="col-auto align-items-center">
                    <button type="button" className="btn btn-secondary" onClick={() => delData(prop.order)}>刪除</button>
                </div>
            </div>
        )
    }

    const ItemList = dataList.map((item, idx) => {
        return (<BuildingSharerItem key={idx} id={item} order={idx} data={item} />)
    });

    return (
        <div className="row">
            <div className="row col-md-1 mb-3">
                <button type="button" className="btn btn-primary" onClick={addNewData}>新增</button>
            </div>
            <input type="hidden" value={JSON.stringify(dataList)} id="stakeholders"></input>
            {ItemList}
        </div>
    )
}

export default BuildingSharer


if (document.getElementsByClassName('buildingSharer')) {
    const el = document.getElementsByClassName('buildingSharer');
    Array.from(el).forEach((item) => {
        ReactDOM.render(<BuildingSharer data={item.getAttribute('data-param') || "[]"} />, item);
    });
}
