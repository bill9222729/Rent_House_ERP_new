import React from "react";
import ReactDOM from "react-dom";

const axios = require("axios").default;
const CitySelector = (props) => {
    let data = JSON.parse(props.data);
    const [cityArr, setCityArr] = React.useState([]);
    const [cityAreaArr, setCityAreaArr] = React.useState([]);
    const [currentCity, setCurrentCity] = React.useState(data.page_data && !data.is_modal ? data.page_data[data.city_select_id] : "");
    const [currentCityArea, setCurrentCityArea] = React.useState(data.page_data && !data.is_modal ? data.page_data[data.city_area_select_id] : "");
    const [currentAddr, setCurrentAddr] = React.useState(data.page_data && !data.is_modal ? data.page_data[data.addr_input_id] : "");
    const handleCitySelectChange = (e) => {
        let cityName = e.target.value;
        setCurrentCity(cityName);
        getCityAreaArr(cityName);
    };
    const handleCitySelectAreaChange = (e) => {
        let cityAreaName = e.target.value;
        setCurrentCityArea(cityAreaName);
    };
    // 同上按鈕點擊事件
    const handleDittoClick = (e) => {
        setCurrentCity(e.target.closest(".CitySelector").previousSibling.previousSibling.querySelector("#residence_city").value);
        setCurrentCityArea(e.target.closest(".CitySelector").previousSibling.previousSibling.querySelector("#residence_city_area").value);
        setCurrentAddr(e.target.closest(".CitySelector").previousSibling.previousSibling.querySelector("#residence_addr").value);
    };
    // 取得地區列表
    const getCityAreaArr = (cityName) => {
        axios
            .get(`/api/city_areas?city_value=${cityName}`)
            .then(function (response) {
                // handle success
                setCityAreaArr(response.data.data);
                response.data.data.map((item, idx) => {
                    return (
                        <option key={idx} value={item.value}>
                            {item.name}
                        </option>
                    );
                });
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    };

    React.useEffect(() => {
        axios
            .get("/api/cities")
            .then(function (response) {
                // handle success
                setCityArr(response.data.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }, []);

    React.useEffect(() => {
        if (currentCity) {
            getCityAreaArr(currentCity);
        }
    }, [currentCity]);

    let CityOptions = cityArr.map((item, idx) => {
        return (
            <option key={idx} value={item.name}>
                {item.name}
            </option>
        );
    });
    let CityAreaOptions = cityAreaArr.map((item, idx) => {
        return (
            <option key={idx} value={item.value}>
                {item.name}
            </option>
        );
    });
    return (
        <>
            <div className="row col-md-12 align-items-center">
                <label className={`col-md-12 col-form-label fw-bolder text-nowrap ${data.required ? "text-danger" : ""}`} htmlFor="mailing_addr">
                    {data.title}
                </label>
                <div className="col-md-11 align-items-center">
                    <div className="input-group">
                        <select className="form-select mailing_city" value={currentCity || ""} id={data.city_select_id} name={data.city_select_id} onChange={handleCitySelectChange}>
                            <option value="">請選擇</option>
                            {CityOptions}
                        </select>
                        <select className="form-select mailing_city_area" value={currentCityArea} id={data.city_area_select_id} name={data.city_area_select_id} onChange={handleCitySelectAreaChange}>
                            <option value="">請選擇</option>
                            {CityAreaOptions}
                        </select>
                        <input type="text" className="form-control" placeholder="" id={data.addr_input_id} value={currentAddr || ""} onChange={(e) => setCurrentAddr(e.target.value)} name={data.addr_input_id} style={{ flexGrow: "4" }} />
                    </div>
                </div>
                {data.ditto ? (
                    <div className="col-md-1 pt-2 pt-md-0">
                        <button className="btn btn-primary text-nowrap" type="button" onClick={(e) => handleDittoClick(e)}>
                            同上
                        </button>
                    </div>
                ) : (
                    ""
                )}
            </div>
        </>
    );
};

export default CitySelector;

const el = document.getElementsByClassName("CitySelector");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<CitySelector data={item.getAttribute("data-param") || ""} />, item);
    });
}
