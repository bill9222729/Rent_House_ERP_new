import React from 'react';
import ReactDOM from 'react-dom';
import Accordion from 'react-bootstrap/Accordion'

function Example(prop) {
    console.log(prop);
    return (
        <Accordion defaultActiveKey={['0']} alwaysOpen>
            <Accordion.Item eventKey="0">
                <Accordion.Header>編輯媒合設定維護</Accordion.Header>
                <Accordion.Body>
                    <div className="container">
                        <div className="row g-3 align-items-center mb-4">
                            <div className="col-1">
                                <label htmlFor="caseNo" className="col-form-label">媒合編號</label>
                            </div>
                            <div className="col-8">
                                <input type="text" id="caseNo" className="form-control" aria-describedby="passwordHelpInline" readOnly />
                            </div>
                        </div>

                        <div className="row g-3 align-items-center mb-4">
                            <div className="col-1">
                                <label htmlFor="buildingInfo" className="col-form-label">出租物件</label>
                            </div>
                            <div className="col-8">
                                {/* <input type="text" id="buildingInfo" className="form-control" aria-describedby="passwordHelpInline" /> */}
                                <a>[案件編號：易居B1H00200126,洪讚明(N103525177),狀態：同意]</a>
                            </div>
                        </div>

                        <div className="row g-3 align-items-center mb-4">
                            <div className="col-1">
                                <label htmlFor="tenantInfo" className="col-form-label">房客</label>
                            </div>
                            <div className="col-8">
                                {/* <input type="text" id="tenantInfo" className="form-control" aria-describedby="passwordHelpInline" /> */}
                                <a>[案件編號：易居B1T00200758,關宇珊(F228245774),狀態：待審核]</a>
                            </div>
                        </div>

                        <div className="row g-3 align-items-center mb-4">
                            <div className="col-1">
                                <label htmlFor="leaseStartDt" className="col-form-label">租賃期間</label>
                            </div>
                            <div className="col-4">
                                <input type="text" id="leaseStartDt" className="form-control" aria-describedby="passwordHelpInline" value={prop.data.lease_start_date} />
                            </div>
                            ~
                            <div className="col-4">
                                <input type="text" id="leaseEndDt" className="form-control" aria-describedby="passwordHelpInline" value={prop.data.lease_end_date} />
                            </div>
                        </div>
                    </div>

                </Accordion.Body>
            </Accordion.Item>
        </Accordion>
    );
}

export default Example;

if (document.getElementById('MatchManagementComponent')) {
    ReactDOM.render(<Example />, document.getElementById('MatchManagementComponent'));
}
