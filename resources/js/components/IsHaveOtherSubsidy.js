import React from 'react';
import ReactDOM from 'react-dom';

const axios = require('axios').default;
const IsHaveOtherSubsidy = (props) => {
    const outer_data = JSON.parse(props.data);
    console.log(outer_data);
    const defauleData = {
        "isOtherSubsidy01": outer_data.isOtherSubsidy01 === 1,
        "isOtherSubsidy01Info": outer_data.isOtherSubsidy01Info,
        "isOtherSubsidy02": outer_data.isOtherSubsidy02 === 1,
        "isOtherSubsidy02Info": outer_data.isOtherSubsidy02Info,
        "isOtherSubsidy03": outer_data.isOtherSubsidy03 === 1,
        "isOtherSubsidy03Info": outer_data.isOtherSubsidy03Info,
        "isOtherSubsidy04": outer_data.isOtherSubsidy04 === 1,
        "isOtherSubsidy04Info": outer_data.isOtherSubsidy04Info,
        "isOtherSubsidy05": outer_data.isOtherSubsidy05 === 1,
        "isOtherSubsidy05Info": outer_data.isOtherSubsidy05Info,
    }
    const [have_other_subsidy, set_have_other_subsidy] = React.useState(outer_data.is_have_other_subsidy === 1);
    const [dataObj, setDataObj] = React.useState(defauleData);
    const handleChackBoxClick = (colName) => {
        let old_data = { ...dataObj };
        old_data[colName] = !old_data[colName];
        if (old_data[colName] === false) old_data[colName + "Info"] = "";
        setDataObj(old_data);
    }

    const handleInputChange = (colName, e) => {
        let val = e.target.value;
        let old_data = { ...dataObj };
        old_data[colName + "Info"] = val;
        setDataObj(old_data);
    }

    return (
        <>
            <div className="row col-md-12">
                <label htmlFor="case_state"
                    className="col-md-12 col-form-label fw-bolder">承租人及家庭成員其是否領有政府最近年度核發之租金補貼核定函或承租右列住宅</label>
                <div className="row col-md-12 ps-4">
                    <div className="form-check col-sm-1">
                        <input className="form-check-input" type="radio" name="not_have_other_subsidy" value="not_have_other_subsidy"
                            id="not_have_other_subsidy" checked={!have_other_subsidy} onChange={() => { set_have_other_subsidy(false); setDataObj(defauleData) }} />
                        <label className="form-check-label" htmlFor="not_have_other_subsidy">
                            否
                        </label>
                    </div>
                    <div className="form-check col-sm-1">
                        <input className="form-check-input" type="radio" name="is_have_other_subsidy" value="is_have_other_subsidy"
                            id="is_have_other_subsidy" checked={have_other_subsidy} onChange={() => set_have_other_subsidy(true)} />
                        <label className="form-check-label" htmlFor="is_have_other_subsidy">
                            是
                        </label>
                    </div>
                    <div className="input-group mb-3">
                        <div className="input-group-text">
                            <input className="form-check-input mt-0" type="checkbox" id="isOtherSubsidy01" value={dataObj.isOtherSubsidy01 || ""} onChange={() => { handleChackBoxClick("isOtherSubsidy01") }}
                                disabled={!have_other_subsidy} checked={dataObj.isOtherSubsidy01} />
                        </div>
                        <input type="text" className="form-control isHaveOtherSubsidy-input" id="isOtherSubsidy01Info" aria-label="Text input with checkbox" disabled={!have_other_subsidy || !dataObj.isOtherSubsidy01} value={dataObj.isOtherSubsidy01Info || ""} onChange={(e) => handleInputChange("isOtherSubsidy01", e)} />
                        <span className="input-group-text isHaveOtherSubsidy-span">年度租金補貼</span>
                    </div>
                    <div className="input-group mb-3">
                        <div className="input-group-text">
                            <input className="form-check-input mt-0" type="checkbox" id="isOtherSubsidy02" value={dataObj.isOtherSubsidy02 || ""} onChange={() => { handleChackBoxClick("isOtherSubsidy02") }}
                                disabled={!have_other_subsidy} checked={dataObj.isOtherSubsidy02} />
                        </div>
                        <input type="text" className="form-control isHaveOtherSubsidy-input" id="isOtherSubsidy02Info" aria-label="Text input with checkbox" disabled={!have_other_subsidy || !dataObj.isOtherSubsidy02} value={dataObj.isOtherSubsidy02Info || ""} onChange={(e) => handleInputChange("isOtherSubsidy02", e)} />
                        <span className="input-group-text isHaveOtherSubsidy-span">年度低收入戶及中低收入戶租金補貼</span>
                    </div>
                    <div className="input-group mb-3">
                        <div className="input-group-text">
                            <input className="form-check-input mt-0" type="checkbox" id="isOtherSubsidy03" value={dataObj.isOtherSubsidy03 || ""} onChange={() => { handleChackBoxClick("isOtherSubsidy03") }}
                                disabled={!have_other_subsidy} checked={dataObj.isOtherSubsidy03} />
                        </div>
                        <input type="text" className="form-control isHaveOtherSubsidy-input" id="isOtherSubsidy03Info" aria-label="Text input with checkbox" disabled={!have_other_subsidy || !dataObj.isOtherSubsidy03} value={dataObj.isOtherSubsidy03Info || ""} onChange={(e) => handleInputChange("isOtherSubsidy03", e)} />
                        <span className="input-group-text isHaveOtherSubsidy-span">年度身心障礙者房屋租金補貼</span>
                    </div>

                    <div className="input-group mb-3">
                        <div className="input-group-text">
                            <input className="form-check-input mt-0" type="checkbox" id="isOtherSubsidy04" value={dataObj.isOtherSubsidy04 || ""} onChange={() => { handleChackBoxClick("isOtherSubsidy04") }}
                                disabled={!have_other_subsidy} checked={dataObj.isOtherSubsidy04} />
                        </div>
                        <input type="text" className="form-control isHaveOtherSubsidy-input" id="isOtherSubsidy04Info" aria-label="Text input with checkbox" disabled={!have_other_subsidy || !dataObj.isOtherSubsidy04} value={dataObj.isOtherSubsidy04Info || ""} onChange={(e) => handleInputChange("isOtherSubsidy04", e)} />
                        <span className="input-group-text isHaveOtherSubsidy-span">年度依其他法令相關租金補貼規定之租金補貼</span>
                    </div>

                    <div className="input-group mb-3">
                        <div className="input-group-text">
                            <input className="form-check-input mt-0" type="checkbox" id="isOtherSubsidy05" value={dataObj.isOtherSubsidy05 || ""} onChange={() => { handleChackBoxClick("isOtherSubsidy05") }}
                                disabled={!have_other_subsidy} checked={dataObj.isOtherSubsidy05} />
                        </div>
                        <input type="text" className="form-control isHaveOtherSubsidy-input" id="isOtherSubsidy05Info" aria-label="Text input with checkbox" disabled={!have_other_subsidy || !dataObj.isOtherSubsidy05} value={dataObj.isOtherSubsidy05Info || ""} onChange={(e) => handleInputChange("isOtherSubsidy05", e)} />
                        <span className="input-group-text isHaveOtherSubsidy-span">縣市承租政府興建之國民住宅或社會住宅</span>
                    </div>
                </div>
            </div>
        </>
    )
}

export default IsHaveOtherSubsidy

const el = document.getElementsByClassName('IsHaveOtherSubsidy');
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<IsHaveOtherSubsidy data={item.getAttribute('data-param') || ""} />, item);
    })
}
