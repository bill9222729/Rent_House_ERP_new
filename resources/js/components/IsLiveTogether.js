import React from "react";
import ReactDOM from "react-dom";
import "../../../public/js/common.js";

// 取得檔案列表
async function getFileArr(source, source_id) {
    const fileType = {
        mp4: "video",
        pdf: "pdf",
        jpg: "image",
        JPG: "image",
    };
    let retVal = {};
    const settings = {
        url: `/api/file_managments?file_source=${source}&file_source_id=${source_id}`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal["initialPreview"] = response.data.map((item) => {
            return `https://rent-house-erp.echouse.co/storage/buildings_file/${item.file_server_name}`;
        });
        retVal["initialPreviewConfig"] = response.data.map((item) => {
            return {
                type: fileType[item.file_origin_name.split(".").pop()],
                size: item.file_size,
                downloadUrl: `https://rent-house-erp.echouse.co/storage/buildings_file/${item.file_server_name}`,
                filename: item.file_origin_name,
                caption: item.file_origin_name,
                url: `/api/file_managments/${item.id}`,
            };
        });
    });

    return retVal;
}

const IsLiveTogether = (props) => {
    const data = props.data ? JSON.parse(props.data) : {};
    const [isLiveTogether, setIsLiveTogether] = React.useState(
        data.is_live_together || 0
    );
    const [isLiveTogetherDesc, setIsLiveTogetherDesc] = React.useState(
        data.is_live_together_desc || ""
    );
    const handleRadioChange = (e) => {
        setIsLiveTogether(e.target.value === "1" ? 1 : 0);
    };
    React.useEffect(() => {
        const fetchData = async () => {
            let fileData = await getFileArr(
                "tenant_live_together",
                data ? data["id"] : ""
            );
            $("#tenant_live_together").fileinput({
                language: "zh-TW",
                uploadUrl: "/api/file_managments",
                maxFileCount: 0,
                maxTotalFileCount: 0,
                uploadExtraData: {
                    file_source_id: data ? data["id"] : "",
                    file_source: "tenant_live_together",
                },
                overwriteInitial: false,
                initialPreview: fileData["initialPreview"],
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: "pdf", // image is the default and can be overridden in config below
                initialPreviewConfig: fileData["initialPreviewConfig"],
                ajaxDeleteSettings: {
                    type: "DELETE", // This should override the ajax as $.ajax({ type: 'DELETE' })
                },
            });
        };
        fetchData();
    }, [isLiveTogether]);
    return (
        <>
            <label
                className="col-sm-2 col-form-label text-end fw-bolder text-nowrap"
                htmlFor="name"
            >
                共住
            </label>
            <div className="col-sm-10">
                <div>
                    本案是否有共住人
                    <div className="form-check form-check-inline">
                        <input
                            className="form-check-input"
                            type="radio"
                            name="is_live_together"
                            value="1"
                            checked={isLiveTogether === 1}
                            onChange={(e) => handleRadioChange(e)}
                        />
                        <label
                            className="form-check-label"
                            htmlFor="is_live_together"
                        >
                            是
                        </label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input
                            className="form-check-input"
                            type="radio"
                            name="is_live_together"
                            value="0"
                            checked={isLiveTogether === 0}
                            onChange={(e) => handleRadioChange(e)}
                        />
                        <label
                            className="form-check-label"
                            htmlFor="is_live_together"
                        >
                            否
                        </label>
                    </div>
                </div>
                {isLiveTogether ? (
                    <>
                        <div className="mt-3">
                            <div className="d-inline-block align-top">
                                共住說明
                            </div>
                            <div className="d-inline-block">
                                <textarea
                                    className="form-control"
                                    rows="3"
                                    value={isLiveTogetherDesc}
                                    onChange={(e) =>
                                        setIsLiveTogetherDesc(e.target.value)
                                    }
                                />
                            </div>
                        </div>
                    </>
                ) : (
                    ""
                )}
            </div>
            {isLiveTogether ? (
                <div className="col-sm-12">
                    <div className="file-loading">
                        <input
                            id="tenant_live_together"
                            name="source"
                            type="file"
                            multiple
                        />
                    </div>
                </div>
            ) : (
                ""
            )}

            <input type="hidden" id="is_live_together" value={isLiveTogether} />
            <input
                type="hidden"
                id="is_live_together_desc"
                value={isLiveTogetherDesc}
            />
        </>
    );
};

export default IsLiveTogether;

const el = document.getElementById("IsLiveTogether");
if (el) {
    ReactDOM.render(
        <IsLiveTogether data={el.getAttribute("data-param") || {}} />,
        el
    );
}
