import React from 'react'
import ReactDOM from 'react-dom';

const LegalAgentNum = (props) => {
    let data = JSON.parse(props.data);
    const [legalAgentNumVal, setLegalAgentNumVal] = React.useState(data["legal_agent_num"] + "");
    const [singleProxyReason, setSingleProxyReason] = React.useState(data["single_proxy_reason"]);
    const [firstLegalAgentName, setFirstLegalAgentName] = React.useState(data["first_legal_agent_name"]);
    const [firstLegalAgentTel, setFirstLegalAgentTel] = React.useState(data["first_legal_agent_tel"]);
    const [firstLegalAgentPhone, setFirstLegalAgentPhone] = React.useState(data["first_legal_agent_phone"]);
    const [firstLegalAgentAddr, setFirstLegalAgentAddr] = React.useState(data["first_legal_agent_addr"]);
    const [secondLegalAgentName, setSecondLegalAgentName] = React.useState(data["second_legal_agent_name"]);
    const [secondLegalAgentTel, setSecondLegalAgentTel] = React.useState(data["second_legal_agent_tel"]);
    const [secondLegalAgentPhone, setSecondLegalAgentPhone] = React.useState(data["second_legal_agent_phone"]);
    const [secondLegalAgentAddr, setSecondLegalAgentAddr] = React.useState(data["second_legal_agent_addr"]);
    return (
        <>
            <div className="row mb-3">
                <div className="row col-md-12 align-items-center">
                    <label className="col-md-12 col-form-label fw-bolder" htmlFor="legal_agent_num">法定代理人人數</label>
                    <span className="fw-bold" style={{ "color": "#17a2b8" }}>未滿二十歲，且為單獨監護者之申請人，法定代理人數請選擇一位</span>
                    <div className="col-md-6 align-items-center">
                            <select className="form-select legal_agent_num" name="legal_agent_num" id="legal_agent_num" value={legalAgentNumVal} onChange={(e) => setLegalAgentNumVal(e.target.value)}>
                                <option value="0">請選擇</option>
                                <option value="1">一位</option>
                                <option value="2">二位</option>
                            </select>
                        {
                            legalAgentNumVal === "1" &&
                            (
                                <div className="input-group" id="single_proxy_reason_row">
                                    <label className="input-group-text" htmlFor="single_proxy_reason">簡述原因</label>
                                    <input type="text" className="form-control" placeholder="" id="single_proxy_reason" value={singleProxyReason} onChange={(e) => setSingleProxyReason(e.target.value)} name="single_proxy_reason" />
                                </div>
                            )
                        }

                    </div>
                </div>
            </div>
            {
                (legalAgentNumVal === "1" || legalAgentNumVal === "2") &&
                (
                    <div className="row mb-3" id="first_proxy_row">
                        <div className="row col-md-6 align-items-center">
                            <label className="col-sm-4 col-form-label text-end fw-bolder text-nowrap"
                                htmlFor="first_legal_agent_name">法定代理人姓名</label>
                            <div className="col-sm-6 align-items-center">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="" id="first_legal_agent_name" value={firstLegalAgentName} onChange={(e) => setFirstLegalAgentName(e.target.value)} name="first_legal_agent_name" />
                                    <span className="input-group-text">(無代理人免填)</span>
                                </div>
                            </div>
                        </div>
                        <div className="row col-md-6 align-items-center">
                            <label className="col-sm-4 col-form-label text-end fw-bolder" htmlFor="proxy_tel_1">聯絡方式</label>
                            <div className="col-sm-8 align-items-center">
                                <div className="input-group">
                                    <label className="input-group-text" htmlFor="first_legal_agent_tel">電話</label>
                                    <input type="text" className="form-control" placeholder="" id="first_legal_agent_tel" value={firstLegalAgentTel} onChange={(e) => setFirstLegalAgentTel(e.target.value)} name="first_legal_agent_tel" />
                                    <label className="input-group-text" htmlFor="first_legal_agent_phone">手機</label>
                                    <input type="text" className="form-control" placeholder="" id="first_legal_agent_phone" value={firstLegalAgentPhone} onChange={(e) => setFirstLegalAgentPhone(e.target.value)} name="first_legal_agent_phone" />
                                </div>
                                <div className="input-group">
                                    <label className="input-group-text" htmlFor="first_legal_agent_addr">地址</label>
                                    <input type="text" className="form-control" placeholder="" id="first_legal_agent_addr" value={firstLegalAgentAddr} onChange={(e) => setFirstLegalAgentAddr(e.target.value)} name="first_legal_agent_addr" />
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
            {
                legalAgentNumVal === "2" &&
                (
                    <div className="row mb-3" id="second_proxy_row">
                        <div className="row col-md-6 align-items-center">
                            <label className="col-sm-4 col-form-label text-end fw-bolder"
                                htmlFor="second_legal_agent_name">法定代理人姓名</label>
                            <div className="col-sm-6 align-items-center">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="" id="second_legal_agent_name" value={secondLegalAgentName} onChange={(e) => setSecondLegalAgentName(e.target.value)} name="second_legal_agent_name" />
                                    <span className="input-group-text">(無代理人免填)</span>
                                </div>
                            </div>
                        </div>
                        <div className="row col-md-6 align-items-center">
                            <label className="col-sm-4 col-form-label text-end fw-bolder"
                                htmlFor="second_legal_agent_tel">聯絡方式</label>
                            <div className="col-sm-8 align-items-center">
                                <div className="input-group">
                                    <label className="input-group-text" htmlFor="second_legal_agent_tel">電話</label>
                                    <input type="text" className="form-control" placeholder="" id="second_legal_agent_tel" value={secondLegalAgentTel} onChange={(e) => setSecondLegalAgentTel(e.target.value)} name="second_legal_agent_tel" />
                                    <label className="input-group-text" htmlFor="second_legal_agent_phone">手機</label>
                                    <input type="text" className="form-control" placeholder="" id="second_legal_agent_phone" value={secondLegalAgentPhone} onChange={(e) => setSecondLegalAgentPhone(e.target.value)} name="second_legal_agent_phone" />
                                </div>
                                <div className="input-group">
                                    <label className="input-group-text" htmlFor="second_legal_agent_addr">地址</label>
                                    <input type="text" className="form-control" placeholder="" id="second_legal_agent_addr" value={secondLegalAgentAddr} onChange={(e) => setSecondLegalAgentAddr(e.target.value)} name="second_legal_agent_addr" />
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        </>
    )
}

export default LegalAgentNum


if (document.getElementsByClassName('legalAgentNum')) {
    const el = document.getElementsByClassName('legalAgentNum');
    Array.from(el).forEach((item) => {
        ReactDOM.render(<LegalAgentNum data={item.getAttribute('data-param') || ""} />, item);
    })
}
