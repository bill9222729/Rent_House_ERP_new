import React from "react";
import ReactDOM from "react-dom";
import BuildingSelect from "./match_managements/BuildingSelect";

const axios = require("axios").default;
const defaultData = {
    match_id: "",
    building_case_id: "",
    lease_start_date: "",
    lease_end_date: "",
    contract_type: "包租約",
};

const ModalAddCharter = () => {
    const [data, setData] = React.useState(defaultData);
    // 欄位更動事件
    const handleFieldChange = (e) => {
        let fieldName = e.target.name;
        let fieldValue = e.target.value;
        let newData = { ...data };
        newData[fieldName] = fieldValue;
        setData(newData);
    };
    // 儲存按鈕事件
    const handleSaveBtnClick = () => {
        let requestData = { ...data };
        requestData["lease_start_date"] = convertToCEDate(
            $("#ModalAddCharter input[name='lease_start_date']").val()
        );
        requestData["lease_end_date"] = convertToCEDate(
            $("#ModalAddCharter input[name='lease_end_date']").val()
        );
        axios
            .post("/api/match_managements", requestData)
            .then(function (response) {
                Swal.fire({
                    title: "成功",
                    text: "資料已成功新增",
                    icon: "success",
                }).then((result) => {
                    $("#match_management_table").DataTable().ajax.reload();
                    $("#ModalAddCharter").modal("hide");
                });
                console.log(response);
            });
    };
    // datepicker的欄位更動事件
    $(`#ModalAddCharter input[name='lease_start_date'],
    #ModalAddCharter input[name='lease_end_date']`).on("change", (e) => {
        handleFieldChange(e);
    });
    return (
        <div className="modal-dialog modal-dialog-centered modal-xl">
            <div className="modal-content">
                <div className="modal-header">
                    <div className="container-fluid">
                        <h5 className="modal-title">新增包租約</h5>
                    </div>
                    <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                </div>
                <div className="modal-body">
                    <div className="container-fluid">
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <div className="row col-md-6">
                                    <label
                                        htmlFor=""
                                        className="col-sm-2 col-form-label text-nowrap"
                                    >
                                        媒合編號
                                    </label>
                                    <div className="col-sm-10">
                                        <input
                                            type="text"
                                            className="form-control"
                                            value={data.match_id}
                                            name="match_id"
                                            onChange={(e) =>
                                                handleFieldChange(e)
                                            }
                                        />
                                    </div>
                                </div>
                                <div className="row col-md-6">
                                    <label
                                        htmlFor=""
                                        className="col-sm-2 col-form-label text-nowrap"
                                    >
                                        出租物件
                                    </label>
                                    <BuildingSelect
                                        func={setData}
                                        data={data}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <div className="row col-md-6">
                                    <label
                                        htmlFor=""
                                        className="col-sm-2 col-form-label text-nowrap"
                                    >
                                        租賃期間
                                    </label>
                                    <div className="row col-sm-10">
                                        <div className="col">
                                            <div className="input-group date">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    autoComplete="off"
                                                    maxLength="10"
                                                    size="10"
                                                    placeholder="YYY-MM-DD"
                                                    aria-label="民國年"
                                                    aria-describedby="button-addon1"
                                                    value={
                                                        data.lease_start_date
                                                    }
                                                    name="lease_start_date"
                                                    onChange={(e) => {
                                                        handleFieldChange(e);
                                                    }}
                                                />
                                                <button
                                                    className="btn btn-outline-secondary"
                                                    type="button"
                                                    id="button-addon1"
                                                >
                                                    <i
                                                        className="fa fa-fw fa-calendar"
                                                        data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar"
                                                    >
                                                        &nbsp;
                                                    </i>
                                                </button>
                                            </div>
                                        </div>
                                        ~
                                        <div className="col">
                                            <div className="input-group date">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    autoComplete="off"
                                                    maxLength="10"
                                                    size="10"
                                                    placeholder="YYY-MM-DD"
                                                    aria-label="民國年"
                                                    aria-describedby="button-addon2"
                                                    value={data.lease_end_date}
                                                    name="lease_end_date"
                                                    onChange={(e) => {
                                                        handleFieldChange(e);
                                                    }}
                                                />
                                                <button
                                                    className="btn btn-outline-secondary"
                                                    type="button"
                                                    id="button-addon2"
                                                >
                                                    <i
                                                        className="fa fa-fw fa-calendar"
                                                        data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar"
                                                    >
                                                        &nbsp;
                                                    </i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button
                        type="button"
                        className="btn btn-secondary"
                        data-bs-dismiss="modal"
                    >
                        關閉
                    </button>
                    <button
                        type="button"
                        className="btn btn-primary"
                        onClick={() => handleSaveBtnClick()}
                    >
                        儲存
                    </button>
                </div>
            </div>
        </div>
    );
};

export default ModalAddCharter;

const el = document.getElementById("ModalAddCharter");
if (el) {
    el.addEventListener("show.bs.modal", function (event) {
        const button = event.relatedTarget;
        const recipient = button.getAttribute("data-bs-id");
        const isUpdate = button.getAttribute("data-bs-update") || false;
        const tenant_id = button.getAttribute("data-bs-tenant-id") || "0";
        ReactDOM.render(
            <ModalAddCharter
                family_id={recipient}
                is_update={isUpdate}
                tenant_id={tenant_id}
            />,
            el
        );
    });
    ReactDOM.render(<ModalAddCharter />, el);
}
