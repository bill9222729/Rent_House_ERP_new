import React from "react";
import ReactDOM from "react-dom";
const axios = require("axios").default;
const defaultCheckBoxDataArr = [
    {
        text: "低收入戶",
        name: "condition_low_income_households",
        checked: false,
        special: false,
    },
    {
        text: "中低收入戶",
        name: "condition_low_and_middle_income_households",
        checked: false,
        special: false,
    },
    {
        text: "特殊境遇家庭",
        name: "condition_special_circumstances_families",
        checked: false,
        special: false,
    },
    {
        text: "65歲以上",
        name: "condition_over_sixty_five",
        checked: false,
        special: false,
    },
    {
        text: "於家庭暴力或性侵害之受害者及其子女",
        name: "condition_domestic_violence",
        checked: false,
        special: false,
    },
    {
        text: "感染或罹患AIDS者",
        name: "condition_aids",
        checked: false,
        special: false,
    },
    {
        text: "原住民",
        name: "condition_aboriginal",
        checked: false,
        special: false,
    },
    {
        text: "災民",
        name: "condition_disaster_victims",
        checked: false,
        special: false,
    },
    {
        text: "遊民",
        name: "condition_vagabond",
        checked: false,
        special: false,
    },
    {
        text: "申請人設籍：與申請人設籍之家庭成員",
        name: "condition_Applicant_naturalization",
        checked: false,
        special: false,
    },
    {
        text: "申請人未設籍：實際居住之家庭成員",
        name: "condition_Applicant_not_naturalization",
        checked: false,
        special: false,
    },
    {
        text: "未設籍當地且在該地區就學就業者",
        name: "condition_Applicant_not_naturalization_and_study",
        checked: false,
        special: false,
    },
    {
        text: "警消人員",
        name: "condition_police_officer",
        checked: false,
        special: false,
    },
    {
        text: "身心障礙者",
        name: "condition_disabled",
        checked: false,
        special: true,
    },
];

const defaultData = {
    tenant_id: 0,
    name: "",
    is_foreigner: false,
    id_no: "",
    appellation: "",
    gender: "",
    condition_low_income_households: 0,
    condition_low_and_middle_income_households: 0,
    condition_special_circumstances_families: 0,
    condition_over_sixty_five: 0,
    condition_domestic_violence: 0,
    condition_disabled: 0,
    condition_disabled_type: 0,
    condition_disabled_level: "",
    condition_aids: 0,
    condition_aboriginal: 0,
    condition_disaster_victims: 0,
    condition_vagabond: 0,
    condition_Applicant_naturalization: 0,
    condition_Applicant_not_naturalization: 0,
    condition_Applicant_not_naturalization_and_study: 0,
    condition_police_officer: 0,
    condition_Applicant_not_naturalization: 0,
    filers: 0,
    last_modified_person: 0,
};
const GetTenantFamilyInfo = async (id) => {
    let retVal = null;
    await axios
        .get(`/api/tenant_family_infos?id=${id}`)
        .then(function (response) {
            retVal = response.data.data;
            console.log(response);
        });
    return retVal;
};

const ModalTenantFamilyInfo = (props) => {
    const family_id = props.family_id;
    const is_update = props.is_update;
    const tenant_id = props.tenant_id;
    const [checkBoxDataArr, setCheckBoxDataArr] = React.useState(
        defaultCheckBoxDataArr
    );
    const [allData, setAllData] = React.useState(defaultData);
    const updateSelfWeakItem = (id, weakItemsObj) => {
        let weakItems = weakItemsObj.filter((item) => {
            return item.checked;
        });
        weakItems = weakItems.map((item) => {
            return item.text;
        });
        // axios.patch(`api/tenant_management_news/${id}`, requestData);
    };
    const handleSaveBtnClick = () => {
        let requestData = { ...allData };
        requestData["is_foreigner"] = requestData["is_foreigner"] ? "1" : "0";
        requestData["tenant_id"] = tenant_id;
        if (requestData["appellation"] === "本人") {
            updateSelfWeakItem(tenant_id, checkBoxDataArr);
        }
        axios
            .post("/api/tenant_family_infos", requestData)
            .then(function (response) {
                Swal.fire({
                    title: "成功",
                    text: "資料已成功新增",
                    icon: "success",
                }).then((result) => {
                    // 清空modal
                    setAllData(defaultData);
                    // 重整datatables
                    const tenant_family_info_table = $(
                        "#tenant_family_info"
                    ).DataTable();
                    tenant_family_info_table.ajax.reload();
                    // 隱藏modal
                    $("#ModalTenantFamilyInfo").modal("hide");
                });
            });
    };
    const handleUpdateBtnClick = (id) => {
        axios
            .patch(`/api/tenant_family_infos/${id}`, allData)
            .then(function (response) {
                Swal.fire("成功!", "您的資料已成功更新!", "success");
                const tenant_family_info_table = $(
                    "#tenant_family_info"
                ).DataTable();
                tenant_family_info_table.ajax.reload();
            });
    };

    const handleFieldChange = (e) => {
        let value = e.target.value;
        if (e.target.type === "checkbox") {
            value = e.target.checked ? 1 : 0;
        }
        let field_name = e.target.name;
        let old_data = { ...allData };
        old_data[field_name] = value;
        setAllData(old_data);
    };

    React.useEffect(() => {
        const fetchData = async () => {
            let familyInfo = await GetTenantFamilyInfo(family_id);
            setAllData(familyInfo[0]);
        };
        family_id ? fetchData() : setAllData(defaultData);
    }, [family_id]);
    let checkBoxDom = checkBoxDataArr.map((item) => {
        if (item.special) {
            return (
                <div className="form-check" key={item.name}>
                    <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckChecked"
                        name="condition"
                    />
                    <label
                        className="form-check-label"
                        htmlFor="flexCheckChecked"
                    >
                        身心障礙者&emsp;
                    </label>
                    <div className="input-group mb-1">
                        <span className="input-group-text" id="basic-addon1">
                            障礙類別
                        </span>
                        <select
                            className="form-select"
                            name="condition_disabled_type"
                            id="condition_disabled_type"
                            value={allData.condition_disabled_type}
                            onChange={(e) => handleFieldChange(e)}
                        >
                            <option value="0">請選擇</option>
                            <option value="1">
                                (一類) 神經系統構造及精神、心智功能
                            </option>
                            <option value="2">
                                (二類) 眼、耳及相關構造與感官功能及疼痛
                            </option>
                            <option value="3">
                                (三類) 涉及聲音與言語構造及其功能
                            </option>
                            <option value="4">
                                (四類) 循環、造血、免疫與呼吸系統構造及其功能
                            </option>
                            <option value="5">
                                (五類)消化、新陳代謝與內分泌系統相關構造及其功能
                            </option>
                            <option value="6">
                                (六類) 泌尿與生殖系統相關構造及其功能
                            </option>
                            <option value="7">
                                (七類) 神經、肌肉、骨骼之移動相關構造及其功能
                            </option>
                            <option value="8">
                                (八類) 皮膚與相關構造及其功能
                            </option>
                            <option value="9">(九類) 罕見疾病</option>
                            <option value="10">(十類) 其它類</option>
                            <option value="11">(十一類) 發展遲緩類</option>
                            <option value="12">(多類) 多重障礙類型</option>
                        </select>
                    </div>
                    <div className="input-group mb-1">
                        <span className="input-group-text" id="basic-addon1">
                            障礙程度
                        </span>
                        <select
                            className="form-select"
                            name="condition_disabled_level"
                            id="condition_disabled_level"
                            value={allData.condition_disabled_level}
                            onChange={(e) => handleFieldChange(e)}
                        >
                            <option value="0">請選擇</option>
                            <option value="1">極重度</option>
                            <option value="2">重度</option>
                            <option value="3">中度</option>
                            <option value="4">輕度</option>
                        </select>
                    </div>
                </div>
            );
        }
        return (
            <div className="form-check" key={item.name}>
                <input
                    className="form-check-input"
                    type="checkbox"
                    name={item.name}
                    id={item.name}
                    onChange={(e) => handleFieldChange(e)}
                    checked={allData[item.name] === 1 ? true : false}
                />
                <label className="form-check-label" htmlFor={item.name}>
                    {item.text}
                </label>
            </div>
        );
    });
    return (
        <div className="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                        家庭成員資料
                    </h5>
                    <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                </div>
                <div className="modal-body">
                    <div className="container-fluid">
                        <div className="row mb-3">
                            <div className="row col-md-6">
                                <label
                                    htmlFor=""
                                    className="col-sm-4 col-form-label text-end fw-bolder text-nowrap text-danger"
                                >
                                    *姓名
                                </label>
                                <div className="col-sm-6 align-items-center">
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="name"
                                        value={allData.name}
                                        onChange={(e) => {
                                            handleFieldChange(e);
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="row col-md-6">
                                <label
                                    htmlFor="id_no"
                                    className="col-sm-4 col-form-label text-end fw-bolder text-nowrap text-danger"
                                >
                                    *身分/居留證號
                                </label>
                                <div className="col-sm-6 align-items-center">
                                    <div className="input-group">
                                        <div className="input-group-text">
                                            <input
                                                className="form-check-input mt-0"
                                                type="checkbox"
                                                id="is_foreigner"
                                                value={allData.is_foreigner}
                                                onChange={(e) => {
                                                    handleFieldChange(e);
                                                }}
                                                name="is_foreigner"
                                                checked={allData.is_foreigner}
                                            />
                                            外籍
                                        </div>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="id_no"
                                            value={allData.id_no}
                                            onChange={(e) => {
                                                handleFieldChange(e);
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="row col-md-6">
                                <label
                                    htmlFor="appellation"
                                    className="col-sm-4 col-form-label text-end fw-bolder text-nowrap text-danger"
                                >
                                    *稱謂
                                </label>
                                <div className="col-sm-6 align-items-center">
                                    <div className="input-group mb-3">
                                        <select
                                            className="form-select"
                                            name="appellation"
                                            id="appellation"
                                            value={allData.appellation}
                                            onChange={(e) => {
                                                handleFieldChange(e);
                                            }}
                                        >
                                            <option value="請選擇">
                                                請選擇
                                            </option>
                                            <option value="配偶">配偶</option>
                                            <option value="兄弟">兄弟</option>
                                            <option value="姊妹">姊妹</option>
                                            <option value="兒子">兒子</option>
                                            <option value="媳婦">媳婦</option>
                                            <option value="女兒">女兒</option>
                                            <option value="女婿">女婿</option>
                                            <option value="孫子">孫子</option>
                                            <option value="孫女">孫女</option>
                                            <option value="孫媳婦">
                                                孫媳婦
                                            </option>
                                            <option value="孫女婿">
                                                孫女婿
                                            </option>
                                            <option value="父親">父親</option>
                                            <option value="母親">母親</option>
                                            <option value="岳父母">
                                                岳父母
                                            </option>
                                            <option value="公婆">公婆</option>
                                            <option value="祖父">祖父</option>
                                            <option value="祖母">祖母</option>
                                            <option value="外祖父">
                                                外祖父
                                            </option>
                                            <option value="外祖母">
                                                外祖母
                                            </option>
                                            <option value="其他親戚">
                                                其他親戚
                                            </option>
                                            <option value="胎兒">胎兒</option>
                                            <option value="其他">其他</option>
                                        </select>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="appellation_other"
                                            id="appellation_other"
                                            value={allData.appellation_other}
                                            onChange={(e) =>
                                                handleFieldChange(e)
                                            }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row col-md-6">
                                <label
                                    htmlFor="gender"
                                    className="col-sm-4 col-form-label text-end fw-bolder text-nowrap"
                                >
                                    性別
                                </label>
                                <div className="col-sm-6 align-items-center">
                                    <select
                                        className="form-select form-control"
                                        name="gender"
                                        id="gender"
                                        value={allData.gender}
                                        onChange={(e) => {
                                            handleFieldChange(e);
                                        }}
                                    >
                                        <option value="">請選擇</option>
                                        <option value="男">男</option>
                                        <option value="女">女</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <label
                                    htmlFor="gender"
                                    className="col-sm-2 col-form-label text-end fw-bolder text-nowrap"
                                >
                                    是否具備以下條件
                                </label>
                                <div className="col-sm-10 align-items-center">
                                    {checkBoxDom}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button
                        type="button"
                        className="btn btn-secondary"
                        data-bs-dismiss="modal"
                    >
                        關閉
                    </button>
                    {is_update ? (
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => handleUpdateBtnClick(family_id)}
                        >
                            更新
                        </button>
                    ) : (
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => handleSaveBtnClick()}
                        >
                            儲存
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
};

export default ModalTenantFamilyInfo;

// 設定modal 彈出的事件
const el = document.getElementById("ModalTenantFamilyInfo");

// const el = document.getElementById('ModalTenantFamilyInfo');
if (el) {
    el.addEventListener("show.bs.modal", function (event) {
        // Button that triggered the modal
        const button = event.relatedTarget;
        // Extract info from data-bs-* attributes
        const recipient = button.getAttribute("data-bs-id");
        const isUpdate = button.getAttribute("data-bs-update") || false;
        const tenant_id = button.getAttribute("data-bs-tenant-id") || "0";
        ReactDOM.render(
            <ModalTenantFamilyInfo
                family_id={recipient}
                is_update={isUpdate}
                tenant_id={tenant_id}
            />,
            el
        );
    });
    ReactDOM.render(<ModalTenantFamilyInfo />, el);
}
