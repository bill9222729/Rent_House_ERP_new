import React from "react";
import ReactDOM from "react-dom";
const axios = require("axios").default;
const defaultData = {
    tenant_id: 0,
    name: "",
    is_foreigner: false,
    id_no: "",
    appellation: "",
    birthday: "",
    filers: 0,
    last_modified_person: 0,
};
const GetModalTenantMinorOffspringsInfo = async (id) => {
    let retVal = null;
    await axios
        .get(`/api/tenant_minor_offsprings?id=${id}`)
        .then(function (response) {
            retVal = response.data.data;
        });
    return retVal;
};

const ModalTenantMinorOffsprings = (props) => {
    const minor_id = props.minor_id;
    const is_update = props.is_update;
    const tenant_id = props.tenant_id;
    const [allData, setAllData] = React.useState(defaultData);
    console.log(allData);
    const handleSaveBtnClick = () => {
        let new_data = { ...allData };
        new_data["birthday"] = convertToCEDate(new_data["birthday"]);
        new_data["is_foreigner"] = new_data["is_foreigner"] ? 1 : 0;
        new_data["tenant_id"] = tenant_id;
        axios
            .post("/api/tenant_minor_offsprings", new_data)
            .then(function (response) {
                Swal.fire("成功!", "資料已新增!", "success").then(() => {
                    // 清空modal
                    setAllData(defaultData);
                    // 重整datatables
                    const tenant_minor_offspring_table = $(
                        "#tenant_minor_offspring"
                    ).DataTable();
                    tenant_minor_offspring_table.ajax.reload();
                    // 隱藏modal
                    $("#ModalTenantMinorOffsprings").modal("hide");
                });
            });
    };
    const handleUpdateBtnClick = (id) => {
        axios
            .patch(`/api/tenant_minor_offsprings/${id}`, allData)
            .then(function (response) {
                Swal.fire("成功!", "您的資料已成功更新!", "success").then(
                    () => {
                        const tenant_minor_offspring_table = $(
                            "#tenant_minor_offspring"
                        ).DataTable();
                        tenant_minor_offspring_table.ajax.reload();
                    }
                );
            });
    };
    const handleFieldChange = (e) => {
        let value = e.target.value;
        if (e.target.type === "checkbox") {
            value = e.target.checked ? 1 : 0;
        }
        let field_name = e.target.name;
        let old_data = { ...allData };
        old_data[field_name] = value;
        console.log(old_data);
        setAllData(old_data);
    };
    React.useEffect(() => {
        const fetchData = async () => {
            let minorInfo = await GetModalTenantMinorOffspringsInfo(minor_id);
            setAllData(minorInfo[0]);
        };
        minor_id ? fetchData() : setAllData(defaultData);
    }, [minor_id]);

    $("#modal_birthday").on("change", (e) => {
        handleFieldChange(e);
    });

    return (
        <div className="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                        未成年子女資料
                    </h5>
                    <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                </div>
                <div className="modal-body">
                    <div className="container-fluid">
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <label
                                    htmlFor=""
                                    className="col-sm-2 col-form-label text-end fw-bolder text-nowrap text-danger"
                                >
                                    *姓名
                                </label>
                                <div className="col-sm-3 align-items-center">
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="name"
                                        value={allData.name}
                                        onChange={(e) => {
                                            handleFieldChange(e);
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <label
                                    htmlFor="id_no"
                                    className="col-sm-2 col-form-label text-end fw-bolder text-nowrap text-danger"
                                >
                                    *身分/居留證號
                                </label>
                                <div className="col-sm-3 align-items-center">
                                    <div className="input-group">
                                        <div className="input-group-text">
                                            <input
                                                className="form-check-input mt-0"
                                                type="checkbox"
                                                id="is_foreigner"
                                                value={allData.is_foreigner}
                                                onChange={(e) => {
                                                    handleFieldChange(e);
                                                }}
                                                name="is_foreigner"
                                                checked={allData.is_foreigner}
                                            />
                                            外籍
                                        </div>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="id_no"
                                            value={allData.id_no}
                                            onChange={(e) => {
                                                handleFieldChange(e);
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <label
                                    htmlFor="appellation"
                                    className="col-sm-2 col-form-label text-end fw-bolder text-nowrap text-danger"
                                >
                                    *稱謂
                                </label>
                                <div className="col-sm-3 align-items-center">
                                    <div className="input-group mb-3">
                                        <select
                                            className="form-select"
                                            name="appellation"
                                            id="appellation"
                                            value={allData.appellation}
                                            onChange={(e) =>
                                                handleFieldChange(e)
                                            }
                                        >
                                            <option value="">請選擇</option>
                                            <option value="兒子">兒子</option>
                                            <option value="女兒">女兒</option>
                                            <option value="孫子">孫子</option>
                                            <option value="孫女">孫女</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <label className="col-sm-2 col-form-label text-end fw-bolder text-nowrap text-danger">
                                    *出生年月日
                                </label>
                                <div className="col-sm-3 align-items-center">
                                    <div className="input-group date day">
                                        <input
                                            type="text"
                                            name="birthday"
                                            value={allData.birthday}
                                            className="form-control"
                                            autoComplete="off"
                                            maxLength="10"
                                            size="10"
                                            placeholder="YYY-MM-DD"
                                            id="modal_birthday"
                                            aria-label="民國年"
                                            aria-describedby="button-addon2"
                                            onChange={(e) =>
                                                handleFieldChange(e)
                                            }
                                        />
                                        <button
                                            className="btn btn-outline-secondary"
                                            type="button"
                                            id="button-addon2"
                                        >
                                            <i
                                                className="fa fa-fw fa-calendar"
                                                data-time-icon="fa fa-fw fa-clock-o"
                                                data-date-icon="fa fa-fw fa-calendar"
                                            >
                                                &nbsp;
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button
                        type="button"
                        className="btn btn-secondary"
                        data-bs-dismiss="modal"
                    >
                        關閉
                    </button>
                    {is_update ? (
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => handleUpdateBtnClick(minor_id)}
                        >
                            更新
                        </button>
                    ) : (
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => handleSaveBtnClick()}
                        >
                            儲存
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
};

export default ModalTenantMinorOffsprings;

// 設定modal 彈出的事件
const el = document.getElementById("ModalTenantMinorOffsprings");

if (el) {
    el.addEventListener("show.bs.modal", function (event) {
        // Button that triggered the modal
        const button = event.relatedTarget;
        // Extract info from data-bs-* attributes
        const recipient = button.getAttribute("data-bs-id");
        const isUpdate = button.getAttribute("data-bs-update") || false;
        const tenant_id = button.getAttribute("data-bs-tenant-id") || "0";
        console.log(recipient);

        ReactDOM.render(
            <ModalTenantMinorOffsprings
                minor_id={recipient}
                is_update={isUpdate}
                tenant_id={tenant_id}
            />,
            el
        );
    });
    ReactDOM.render(<ModalTenantMinorOffsprings />, el);
}
