import React from "react";
import ReactDOM from "react-dom";

const axios = require("axios").default;
// 取得業務列表
const getSalesArr = async () => {
    let retVal = null;
    await axios
        .get(`/api/users?is_sales=1`)
        .then(function (response) {
            // handle success
            retVal = response.data.data;
        })
        .catch(function (error) {
            // handle error
        })
        .then(function () {
            // always executed
        });
    return retVal;
};
const SalesSelector = (props) => {
    const [salesArr, setSalesArr] = React.useState([]);
    const [currentSales, setCurrentSales] = React.useState(props.params ? JSON.parse(props.params).sales_id : "");
    console.log(JSON.parse(props.params));
    // 初始化取得業務列表
    React.useEffect(() => {
        async function fetchData() {
            setSalesArr(await getSalesArr());
        }
        fetchData();
    }, []);

    // 生成select選項
    let SalesOptions = salesArr.map((item, idx) => {
        return <option key={idx} value={item.id}>{`(${item.id})${item.name}`}</option>;
    });

    return (
        <>
            <label htmlFor="sales_id" className={`col-md-12 col-form-label fw-bolder text-nowrap ${props.required ? "text-danger" : ""}`}>
                {`${props.required ? "*業務" : "業務"}`}
            </label>
            <div className="col-md-12">
                <select className="form-select form-control" value={currentSales} name="sales_id" id="sales_id" onChange={(e) => setCurrentSales(e.target.value)}>
                    <option value="">請選擇</option>
                    {SalesOptions}
                </select>
            </div>
        </>
    );
};

export default SalesSelector;

const el = document.getElementsByClassName("SalesSelector");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<SalesSelector required={item.getAttribute("required") || false} params={item.getAttribute("data-param") || {}} />, item);
    });
}
