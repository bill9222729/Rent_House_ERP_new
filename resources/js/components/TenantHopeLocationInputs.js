import React from "react";
import ReactDOM from "react-dom";

const axios = require("axios").default;
// 取得縣市列表
const GetCityArr = async () => {
    let retval = null;
    await axios.get("/api/cities").then(function (response) {
        retval = response.data.data;
    });
    return retval;
};

// 取得區域列表
const GetCityAreaArr = async () => {
    let retval = null;
    await axios.get(`/api/city_areas`).then(function (response) {
        retval = response.data.data;
    });
    return retval;
};
// 取得街道列表
const GetStreetArr = async () => {
    let retval = null;
    await axios.get(`/api/streets`).then(function (response) {
        retval = response.data.data;
    });
    return retval;
};

const TenantHopeLocationInputs = (props) => {
    const outer_data = props.data ? JSON.parse(props.data) : [];
    const [dataArr, setDataArr] = React.useState(outer_data);
    const [cityArr, setCityArr] = React.useState([]);
    const [cityAreaArr, setCityAreaArr] = React.useState([]);
    const [streetArr, setStreetArr] = React.useState([]);
    const handleAddClick = () => {
        let old_arr = [...dataArr];
        old_arr.push({
            city: "",
            city_area: "",
            street: "",
        });
        setDataArr(old_arr);
    };
    React.useEffect(() => {
        async function fetchData() {
            setCityArr(await GetCityArr());
            setCityAreaArr(await GetCityAreaArr());
            setStreetArr(await GetStreetArr());
        }
        fetchData();
    }, []);
    const CitySelector = (props) => {
        const data = props.data;
        const order = props.order + 1;
        const originData = props.originData;
        const setDataArr = props.setDataArr;
        const [currentCity, setCurrentCity] = React.useState(data.city || "");
        const [currentCityArea, setCurrentCityArea] = React.useState(data.city_area || "");
        const [currentStreet, setCurrentStreet] = React.useState(data.street || "");
        const handleDelClick = () => {
            let old_arr = [...originData];
            old_arr.splice(order - 1, 1);
            setDataArr(old_arr);
        };
        const handleSelectChange = (e) => {
            let value = e.target.value;
            let name = e.target.name;
            if (name === "city") {
                setCurrentCity(value);
                setCurrentCityArea("");
                setCurrentStreet("");
            }
            if (name === "city_area") {
                setCurrentCityArea(value);
                setCurrentStreet("");
            }
            if (name === "street") {
                setCurrentStreet(value);
            }
            let new_arr = [...originData];
            new_arr[order - 1][name] = value;
            setDataArr(new_arr);
        };

        let cityOptions = cityArr.map((item, idx) => {
            return (
                <option key={idx} value={item.name}>
                    {item.name}
                </option>
            );
        });
        let cityAreaOptions = cityAreaArr.map((item, idx) => {
            if (item.city_value === currentCity) {
                return (
                    <option key={idx} value={item.value}>
                        {item.name}
                    </option>
                );
            }
        });
        let streetOptions = streetArr.map((item, idx) => {
            if (item.city_area_value === currentCityArea) {
                return (
                    <option key={idx} value={item.name}>
                        {item.name}
                    </option>
                );
            }
        });
        return (
            <div className="input-group mb-3 gx-0">
                <span className="input-group-text">{order}. </span>
                <select className="form-select" name="city" value={currentCity} onChange={(e) => handleSelectChange(e)}>
                    <option value="">請選擇</option>
                    {cityOptions}
                </select>
                <select className="form-select" name="city_area" value={currentCityArea} onChange={(e) => handleSelectChange(e)}>
                    <option value="">請選擇</option>
                    {cityAreaOptions}
                </select>
                <select className="form-select" name="street" value={currentStreet} onChange={(e) => handleSelectChange(e)}>
                    <option value="">請選擇</option>
                    {streetOptions}
                </select>
                <button className="btn btn-secondary" type="button" id="button-addon2" onClick={handleDelClick}>
                    刪除
                </button>
                <input type="hidden" name="tenant_hope_locations" id="tenant_hope_locations" value={JSON.stringify(dataArr)}></input>
            </div>
        );
    };
    let citySelectors = dataArr.map((item, idx) => {
        return <CitySelector data={item} order={idx} setDataArr={setDataArr} originData={dataArr} key={idx} />;
    });
    return (
        <>
            <label htmlFor="sales_id" className="col-md-12 col-form-label fw-bolder text-nowrap">
                區位&emsp;
                    <button type="button" className="btn btn-primary text-nowrap " onClick={handleAddClick}>
                        新增
                    </button>
            </label>


            <div className="row col-md-12">
                <div className="row">{citySelectors}</div>
            </div>
        </>
    );
};

export default TenantHopeLocationInputs;

const el = document.getElementsByClassName("TenantHopeLocationInputs");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<TenantHopeLocationInputs data={item.getAttribute("data-param") || ""} />, item);
    });
}
