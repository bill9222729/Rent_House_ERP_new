import React from "react";
import ReactDOM from "react-dom";

const MeterToPingInput = (props) => {
    const data = JSON.parse(props.data);
    const elementId = props.elementId;
    const title = props.title;
    const [square_meter, set_square_meter] = React.useState(
        data[elementId] || 0
    );
    return (
        <>
            <label
                htmlFor={elementId}
                className="col-md-12 col-form-label text-danger fw-bolder"
            >
                {title}
            </label>
            <div className="col-md-12">
                <div className="input-group mb-3">
                    <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        id={elementId}
                        name={elementId}
                        value={square_meter}
                        onChange={(e) => set_square_meter(e.target.value)}
                    />
                    <span className="input-group-text">平方公尺</span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder=""
                        value={Math.round(square_meter * 0.3025 * 1000) / 1000}
                        name={elementId + "_result"}
                        readOnly
                    />
                    <span className="input-group-text">坪</span>
                </div>
            </div>
        </>
    );
};

export default MeterToPingInput;

if (document.getElementsByClassName("MeterToPingInput")) {
    const el = document.getElementsByClassName("MeterToPingInput");
    Array.from(el).forEach((item) => {
        ReactDOM.render(
            <MeterToPingInput
                data={item.getAttribute("data-param") || "[]"}
                elementId={item.getAttribute("elementId") || ""}
                title={item.getAttribute("title") || ""}
            />,
            item
        );
    });
}
