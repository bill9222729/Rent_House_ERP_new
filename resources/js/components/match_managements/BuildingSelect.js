import React from "react";
import ReactDOM from "react-dom";

const axios = require("axios").default;
// 取得所有物件
const getAllBuildings = async () => {
    let retVal = null;
    await axios.get("api/building_management_news").then((response) => {
        retVal = response.data.data;
    });
    return retVal;
};

const BuildingSelect = (props) => {
    const setParentData = props.func;
    const parentData = props.data;
    const [buildingsInfo, setBuildingsInfo] = React.useState([]);
    const selectEl = React.useRef(null);
    React.useEffect(() => {
        const fetchData = async () => {
            setBuildingsInfo(await getAllBuildings());
            $(selectEl.current).selectpicker("refresh");
        };
        fetchData();
    }, []);

    const handleSelectChange = (e) => {
        let value = e.target.value;
        let newData = { ...parentData };
        newData["building_case_id"] = value;
        setParentData(newData);
    };

    let selectOptions = buildingsInfo.map((item) => {
        return (
            <option value={item.id} key={item.id}>
                {item.government_no}
            </option>
        );
    });

    return (
        <div className="col-sm-10">
            <select className="form-select form-control selectpicker" ref={selectEl} data-live-search="true" data-style="border text-dark bg-white" data-size="5" name="building_case_id" value={parentData.building_case_id} onChange={(e) => handleSelectChange(e)}>
                <option>請選擇</option>
                {selectOptions}
            </select>
        </div>
    );
};

export default BuildingSelect;

const el = document.getElementsByClassName("BuildingSelect");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<BuildingSelect />, item);
    });
}
