import React from "react";

const axios = require("axios").default;
// 取得所有物件
const getAllCharters = async () => {
    let retVal = null;
    await axios.get("api/match_managements?contract_type=包租約").then((response) => {
        console.log(response.data.data);
        retVal = response.data.data;
    });
    return retVal;
};

const CharterSelect = (props) => {
    const setParentData = props.func;
    const parentData = props.data;
    const [chartersInfo, setChartersInfo] = React.useState([]);
    const selectEl = React.useRef(null);

    React.useEffect(() => {
        const fetchData = async () => {
            setChartersInfo(await getAllCharters());
            $(selectEl.current).selectpicker("refresh");
        };
        fetchData();
    }, []);

    const handleSelectChange = (e) => {
        let value = e.target.value;
        let newData = { ...parentData };
        newData["charter_case_id"] = value;
        setParentData(newData);
    };

    let selectOptions = chartersInfo.map((item) => {
        return (
            <option value={item.id} key={item.id}>
                {item.match_id}
            </option>
        );
    });

    return (
        <div className="col-sm-10">
            <select className="form-select form-control selectpicker" ref={selectEl} data-live-search="true" data-style="border text-dark bg-white" data-size="5" name="charter_case_id" value={parentData.charter_case_id} onChange={(e) => handleSelectChange(e)}>
                <option>請選擇</option>
                {selectOptions}
            </select>
        </div>
    );
};

export default CharterSelect;
