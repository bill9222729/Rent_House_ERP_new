import React from "react";
import ReactDOM from "react-dom";

const axios = require("axios").default;
const defaultData = {
    match_id: "",
    contract_stop_date: "",
    contract_stop_reason: "",
};
const getMatchData = async (data_id) => {
    let retVal = {};
    await axios.get(`/api/match_managements?id=${data_id}`).then(function (response) {
        retVal = response.data.data[response.data.data.length - 1];
    });
    return retVal;
};

// 取得檔案列表
async function getFileArr(source, source_id) {
    console.log("source", source_id);
    const fileType = {
        mp4: "video",
        pdf: "pdf",
        jpg: "image",
        JPG: "image",
    };
    let retVal = {};
    const settings = {
        url: `/api/file_managments?file_source=${source}&file_source_id=${source_id}`,
        method: "GET",
        timeout: 0,
    };

    await $.ajax(settings).done(function (response) {
        retVal["initialPreview"] = response.data.map((item) => {
            return `https://rent-house-erp.echouse.co/storage/buildings_file/${item.file_server_name}`;
        });
        retVal["initialPreviewConfig"] = response.data.map((item) => {
            return {
                type: fileType[item.file_origin_name.split(".").pop()],
                size: item.file_size,
                downloadUrl: `https://rent-house-erp.echouse.co/storage/buildings_file/${item.file_server_name}`,
                filename: item.file_origin_name,
                caption: item.file_origin_name,
                url: `/api/file_managments/${item.id}`,
            };
        });
    });

    return retVal;
}

// 上傳檔案部分的初始化
const initFileInput = async (input_id, data_id) => {
    let fileData = await getFileArr("contract_stop_file", data_id);
    console.log(fileData["initialPreview"]);
    $(`#${input_id}`).fileinput('destroy');
    $(`#${input_id}`).fileinput({
        language: "zh-TW",
        uploadUrl: "/api/file_managments",
        maxFileCount: 0,
        maxTotalFileCount: 0,
        uploadExtraData: ()=> {
            return {
                file_source_id: sessionStorage.getItem("match_data_id"),
                file_source: "contract_stop_file"
            }
        },
        overwriteInitial: false,
        initialPreview: fileData["initialPreview"],
        initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
        initialPreviewFileType: "pdf", // image is the default and can be overridden in config below
        initialPreviewConfig: fileData["initialPreviewConfig"],
        ajaxDeleteSettings: {
            type: "DELETE", // This should override the ajax as $.ajax({ type: 'DELETE' })
        },
    });
};

const ModalStopContract = (params) => {
    const [data, setData] = React.useState(defaultData);
    const [match_data, set_match_data] = React.useState();
    const fetchData = async () => {
        let match_date = await getMatchData(params.data_id);
        if (match_date) {
            let newData = {
                match_id: match_date.match_id || "",
                contract_stop_date: match_date.contract_stop_date ? convertToRCDate(match_date.contract_stop_date) : "",
                contract_stop_reason: match_date.contract_stop_reason || "",
            };
            setData(newData);
        }
        set_match_data(match_date);
    };

    // 初始化
    React.useEffect(() => {
        let newData = { ...data };
        sessionStorage.setItem("match_data_id", params.data_id);
        newData["match_id"] = params.match_id;
        setData(newData);
        fetchData();
        initFileInput("input-700", params.data_id);
        console.log(newData);
    }, [params.data_id]);
    // 欄位更動事件
    const handleFieldChange = (e) => {
        let fieldName = e.target.name;
        let fieldValue = e.target.value;
        let newData = { ...data };
        newData[fieldName] = fieldValue;
        setData(newData);
    };
    // 儲存按鈕事件
    const handleSaveBtnClick = () => {
        let requestData = { ...data };
        requestData["contract_stop_date"] = convertToCEDate($("#ModalStopContract input[name='contract_stop_date']").val());
        console.log(requestData);
        axios.patch(`/api/match_managements/${match_data.id}`, requestData).then(function (response) {
            Swal.fire({
                title: "成功",
                text: "合約已成功終止",
                icon: "success",
            }).then((result) => {
                $("#match_management_table").DataTable().ajax.reload();
                $("#ModalStopContract").modal("hide");
            });
            console.log(response);
        });
    };
    // datepicker的欄位更動事件
    $(`#ModalStopContract input[name='contract_stop_date'], #ModalStopContract input[name='contract_stop_date']`).on("change", (e) => {
        handleFieldChange(e);
    });
    return (
        <div className="modal-dialog modal-dialog-centered modal-xl">
            <div className="modal-content">
                <div className="modal-header">
                    <div className="container-fluid">
                        <h5 className="modal-title">終止合約</h5>
                    </div>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <div className="container-fluid">
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <div className="row col-md-6">
                                    <label htmlFor="" className="col-sm-2 col-form-label text-nowrap">
                                        媒合編號
                                    </label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" value={data.match_id || ""} name="match_id" readOnly={true} />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <div className="row col-md-6">
                                    <label htmlFor="" className="col-sm-2 col-form-label text-nowrap">
                                        終止日期
                                    </label>
                                    <div className="row col-sm-10">
                                        <div className="col">
                                            <div className="input-group date">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    autoComplete="off"
                                                    maxLength="10"
                                                    size="10"
                                                    placeholder="YYY-MM-DD"
                                                    value={data.contract_stop_date || ""}
                                                    name="contract_stop_date"
                                                    onChange={(e) => {
                                                        handleFieldChange(e);
                                                    }}
                                                />
                                                <button className="btn btn-outline-secondary" type="button" id="button-addon1">
                                                    <i className="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">
                                                        &nbsp;
                                                    </i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <div className="row col-md-6">
                                    <label htmlFor="" className="col-sm-2 col-form-label text-nowrap">
                                        終止原因
                                    </label>
                                    <div className="col-sm-10">
                                        <textarea type="text" className="form-control" value={data.contract_stop_reason || ""} name="contract_stop_reason" onChange={(e) => handleFieldChange(e)}></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="row col-md-12">
                                <input id="input-700" name="source" type="file" multiple></input>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">
                        關閉
                    </button>
                    <button type="button" className="btn btn-primary" onClick={() => handleSaveBtnClick()}>
                        儲存
                    </button>
                </div>
            </div>
        </div>
    );
};

export default ModalStopContract;

const el = document.getElementById("ModalStopContract");
if (el) {
    el.addEventListener("show.bs.modal", function (event) {
        const button = event.relatedTarget;
        const match_id = button.getAttribute("data-bs-match-id");
        const data_id = button.getAttribute("data-bs-id");
        ReactDOM.render(<ModalStopContract match_id={match_id} data_id={data_id} />, el);
    });
    ReactDOM.render(<ModalStopContract />, el);
}
