import React from "react";
import ReactDOM from "react-dom";

const RowItem = (props) => {
    const [data, setData] = React.useState(props.data);

    const handleInputChange = (e) => {
        let value = e.target.value;
        let newData = { ...data };
        let inputName = e.target.name;
        newData[inputName] = value;
        setData(newData);
    };

    const handleInputOnBlur = (e) => {
        let newParentData = [...props.parentData];
        let itemId = e.target.getAttribute("data-item-id");
        newParentData.splice(itemId, 1, data);
        props.parentFunc(newParentData);
    };

    const handleDeleteBtn = (e) => {
        let newData = [...props.parentData];
        let itemId = e.target.getAttribute("data-item-id");
        newData.splice(itemId, 1);
        props.parentFunc(newData);
    };

    const handleLogOutBtn = (e) => {
        let newData = [...props.parentData];
        let itemId = e.target.getAttribute("data-item-id");
        newData[itemId]["is_log_out"] = !newData[itemId]["is_log_out"];
        props.parentFunc(newData);
    };

    return (
        <div className="row col-md-12 mb-3">
            <div className="col-md-4 col-12">
                <input
                    type="text"
                    className="form-control"
                    name="net_price_login_id"
                    id="net_price_login_id"
                    placeholder="編號"
                    value={data.net_price_login_id}
                    data-item-id={props.itemIdex}
                    disabled={data.is_log_out}
                    onBlur={(e) => {
                        handleInputOnBlur(e);
                    }}
                    onChange={(e) => {
                        handleInputChange(e);
                    }}
                />
            </div>
            <div className="col-md-4 mt-md-0 mt-3">
                <div className="input-group date day">
                    <input
                        type="text"
                        className="form-control"
                        name="net_price_login_date"
                        autoComplete="off"
                        maxLength="10"
                        size="10"
                        placeholder="YYY-MM-DD"
                        id="net_price_login_date"
                        aria-label="民國年"
                        aria-describedby="button-addon1"
                        value={data.net_price_login_date}
                        data-item-id={props.itemIdex}
                        disabled={data.is_log_out}
                        onBlur={(e) => {
                            handleInputOnBlur(e);
                        }}
                        onChange={(e) => {
                            handleInputChange(e);
                        }}
                    />
                    <button className="btn btn-outline-secondary" type="button" id="button-addon1" disabled={data.is_log_out}>
                        <i className="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">
                            &nbsp;
                        </i>
                    </button>
                </div>
            </div>
            <div className="col-md-4 col-auto mt-3 mt-md-0">
                <button
                    type="button"
                    className="btn btn-secondary col-md-5 col-auto text-nowrap me-md-2 me-1"
                    data-item-id={props.itemIdex}
                    onClick={(e) => {
                        handleDeleteBtn(e);
                    }}
                >
                    刪除
                </button>
                <button
                    type="button"
                    className="btn btn-secondary col-md-5 col-auto text-nowrap me-md-2 me-1"
                    data-item-id={props.itemIdex}
                    onClick={(e) => {
                        handleLogOutBtn(e);
                    }}
                >
                    {data.is_log_out ? "已註銷" : "註銷"}
                </button>
            </div>
        </div>
    );
};

const NetPriceLogin = (props) => {
    const [data, setData] = React.useState(JSON.parse(props["data-param"]));
    const [itemList, setItemList] = React.useState();

    const handleAddBtnClick = () => {
        if (data.length >= 10) {
            return;
        }
        setData([...data, { net_price_login_id: "", net_price_login_date: "", is_log_out: false }]);
    };

    React.useEffect(() => {
        console.log(data);
        setItemList(
            data
                ? data.map((item, idx) => {
                      return item ? <RowItem key={JSON.stringify(item) + idx} data={item} itemIdex={idx} parentFunc={setData} parentData={data} /> : "";
                  })
                : ""
        );
    }, [data]);

    React.useEffect(() => {
        // 初始化日期選擇器
        $(".input-group.date.day").datepicker({
            format: "twy-mm-dd",
            language: "zh-TW",
            showOnFocus: false,
            todayHighlight: true,
            todayBtn: "linked",
            clearBtn: true,
        });
        $(".input-group.date.day").datepicker().on('changeDate', function (e) {
            let inputVal = $(e.target).children("input").val();
            let inputId = $(e.target).children("input").attr("data-item-id");
            let newData = [...data];
            newData[inputId]["net_price_login_date"] = inputVal;
            setData(newData);
        });
    }, [itemList]);

    return (
        <div className="row mb-3">
            <div className="row col-md-12">
                <label htmlFor="net_price_login" className="col-md-12 col-form-label fw-bolder text-nowrap">
                    實價登錄&emsp;
                    <button
                        type="button"
                        className="btn btn-primary"
                        onClick={() => {
                            handleAddBtnClick();
                        }}
                    >
                        新增
                    </button>
                </label>
                {itemList}
            </div>
            <input type="hidden" name="net_price_login" id="net_price_login" value={data ? JSON.stringify(data) : ""}></input>
        </div>
    );
};

export default NetPriceLogin;

const el = document.getElementsByClassName("NetPriceLogin");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<NetPriceLogin data-param={item.getAttribute("data-param") || "[]"} />, item);
    });
}
