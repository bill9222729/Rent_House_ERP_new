import React from "react";
import ReactDOM from "react-dom";

/**
 * 這個元件有兩種模式，一種是單獨用在blade上面的，不須要帶入任何參數
 * 另外一種模式就是附加在另一個react元件裡面的，需要帶入父元件的資料：func以及data
 * 詳情請見resources\js\components\ModalAddEscrow.js
 */

const axios = require("axios").default;
// 取得所有物件
const getAllTenant = async () => {
    let retVal = null;
    await axios.get("/api/tenant_management_news").then((response) => {
        console.log(response.data.data);
        retVal = response.data.data;
    });
    return retVal;
};

const TenantSelect = (props) => {
    const setParentData = props.func;
    const parentData = props.data;
    const [selectVal, setSelectVal] = React.useState("");
    const [tenantInfo, setTenantInfo] = React.useState([]);
    const selectEl = React.useRef(null);
    React.useEffect(() => {
        const fetchData = async () => {
            setTenantInfo(await getAllTenant());
            $(selectEl.current).selectpicker("refresh");
        };
        fetchData();
    }, []);

    const handleSelectChange = (e) => {
        let value = e.target.value;
        if (setParentData) {
            let newData = { ...parentData };
            newData["tenant_case_id"] = value;
            setParentData(newData);
        } else {
            setSelectVal(value);
        }
    };

    let selectOptions = tenantInfo.map((item) => {
        return (
            <option value={item.id} key={item.id}>
                {item.government_no}
            </option>
        );
    });

    return (
        <div className="col-md-12">
            <select
                className="form-select form-control selectpicker"
                ref={selectEl}
                data-live-search="true"
                data-style="border text-dark bg-white"
                data-size="5"
                name="tenant_case_id"
                id="tenant_case_id"
                value={parentData ? parentData.tenant_case_id : selectVal}
                onChange={(e) => {
                    handleSelectChange(e);
                }}
            >
                <option>請選擇</option>
                {selectOptions}
            </select>
        </div>
    );
};

export default TenantSelect;

const el = document.getElementsByClassName("TenantSelect");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<TenantSelect />, item);
    });
}
