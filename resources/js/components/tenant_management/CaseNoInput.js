import React from "react";
import ReactDOM from "react-dom";

const defaultData = {
    guild_edition: "0",
    case_no: "",
};

const CaseNoInput = (props) => {
    const outer_data = props.data ? JSON.parse(props.data) : "";
    const [data, setData] = React.useState(
        outer_data
            ? {
                  guild_edition: outer_data.guild_edition,
                  case_no: outer_data.case_no,
              }
            : defaultData
    );
    const handleInputChange = (e) => {
        let new_data = { ...data };
        if (e.target.type === "checkbox") {
            new_data["guild_edition"] = e.target.checked ? 1 : 0;
        }
        if (e.target.type === "text") {
            new_data["case_no"] = e.target.value;
        }
        setData(new_data);
    };
    console.log(data);
    return (
        <div className="input-group">
            <div className="input-group-text">
                <input className="form-check-input mt-0" name="guild_edition" id="guild_edition" type="checkbox" defaultChecked={data.guild_edition === 1} onChange={(e) => handleInputChange(e)} />
            </div>
            <span className="input-group-text">公會版</span>
            <input type="text" name="case_no" id="case_no" style={{ "textTransform": "uppercase" }} className="form-control" value={data.case_no || ""} placeholder="" disabled={data.guild_edition === 1} onChange={(e) => handleInputChange(e)} />
        </div>
    );
};

export default CaseNoInput;

if (document.getElementsByClassName("CaseNoInput")) {
    const el = document.getElementsByClassName("CaseNoInput");
    Array.from(el).forEach((item) => {
        ReactDOM.render(<CaseNoInput data={item.getAttribute("data-param") || "[]"} />, item);
    });
}
