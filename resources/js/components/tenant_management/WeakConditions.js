import React from "react";
import ReactDOM from "react-dom";

const WeakConditionsArr = ["低收入戶", "中低收入戶", "特殊境遇家庭", "65歲以上", "於家庭暴力或性侵害之受害者及其子女", "感染或罹患AIDS者", "原住民", "災民", "遊民", "申請人設籍：與申請人設籍之家庭成員", "申請人未設籍：實際居住之家庭成員", "未設籍當地且在該地區就學就業者", "警消人員", "育有未成年子女三人以上", "身心障礙者"];
const WeakConditions = (props) => {
    const data = JSON.parse(props.data);
    console.log(data);
    const [condition_disabled_level, set_condition_disabled_level] = React.useState(data.condition_disabled_level || "");
    const [condition_disabled_type, set_condition_disabled_type] = React.useState(data.condition_disabled_type || "");
    const [weak_conditions, set_weak_conditions] = React.useState(data.weak_item ? JSON.parse(data.weak_item) : []);
    let checkBoxDom = WeakConditionsArr.map((item) => {
        let checked = weak_conditions.indexOf(item) === -1 ? false : true;
        return (
            <div className="form-check" key={item}>
                <input className="form-check-input" type="checkbox" name={item} id={item} onChange={(e) => handleCheckBoxClick(e)} checked={checked} />
                <label className="form-check-label" htmlFor={item}>
                    {item}
                </label>
            </div>
        );
    });

    const handleCheckBoxClick = (e) => {
        let checked = e.target.checked;
        let id = e.target.id;
        let new_weak_conditions = [...weak_conditions];
        if (checked) {
            new_weak_conditions = [];
            new_weak_conditions.push(id);
        } else {
            new_weak_conditions.map((item, idx) => {
                if (id === item) {
                    new_weak_conditions.splice(idx, 1);
                }
            });
        }

        if (id !== "身心障礙者") {
            set_condition_disabled_level("");
            set_condition_disabled_type("");
        }
        set_weak_conditions(new_weak_conditions);
    };

    return (
        <>
            <div className="row col-md-6">
                <input type="hidden" value={JSON.stringify(weak_conditions)} id="weak_item" />
                <label htmlFor="gender" className="col-md-12 col-form-label fw-bolder text-nowrap">
                    是否具備以下條件
                </label>
                <div className="col-md-12 align-items-center">
                    {checkBoxDom}
                    {weak_conditions.indexOf("身心障礙者") != -1 ? (
                        <>
                            <div className="input-group mb-1">
                                <span className="input-group-text" id="basic-addon1">
                                    障礙類別
                                </span>
                                <select className="form-select" name="condition_disabled_type" id="condition_disabled_type" value={condition_disabled_type} onChange={(e) => set_condition_disabled_type(e.target.value)}>
                                    <option value="">請選擇</option>
                                    <option value="1">(一類) 神經系統構造及精神、心智功能</option>
                                    <option value="2">(二類) 眼、耳及相關構造與感官功能及疼痛</option>
                                    <option value="3">(三類) 涉及聲音與言語構造及其功能</option>
                                    <option value="4">(四類) 循環、造血、免疫與呼吸系統構造及其功能</option>
                                    <option value="5">(五類) 消化、新陳代謝與內分泌系統相關構造及其功能</option>
                                    <option value="6">(六類) 泌尿與生殖系統相關構造及其功能</option>
                                    <option value="7">(七類) 神經、肌肉、骨骼之移動相關構造及其功能</option>
                                    <option value="8">(八類) 皮膚與相關構造及其功能</option>
                                    <option value="9">(多類) 多重障礙類型</option>
                                    <option value="10">(九類) 罕見疾病</option>
                                    <option value="11">(十類) 其它類</option>
                                    <option value="12">(十一類) 發展遲緩類</option>
                                </select>
                            </div>
                            <div className="input-group mb-1">
                                <span className="input-group-text">障礙程度</span>
                                <select className="form-select" name="condition_disabled_level" id="condition_disabled_level" value={condition_disabled_level} onChange={(e) => set_condition_disabled_level(e.target.value)}>
                                    <option value="">請選擇</option>
                                    <option value="1">極重度</option>
                                    <option value="2">重度</option>
                                    <option value="3">中度</option>
                                    <option value="4">輕度</option>
                                </select>
                            </div>
                        </>
                    ) : (
                        ""
                    )}
                    {weak_conditions.indexOf("身心障礙者") != -1 || weak_conditions.indexOf("特殊境遇家庭") != -1 || weak_conditions.indexOf("於家庭暴力或性侵害之受害者及其子女") != -1 ? (
                        <div className="input-group mb-1">
                            <span className="input-group-text">到期日期</span>
                            <input type="text" className="form-control" name="condition_end_date" id="condition_end_date" placeholder="YYY-MM-DD"></input>
                        </div>
                    ) : (
                        ""
                    )}
                </div>
            </div>
        </>
    );
};

export default WeakConditions;

if (document.getElementsByClassName("WeakConditions")) {
    const el = document.getElementsByClassName("WeakConditions");
    Array.from(el).forEach((item) => {
        ReactDOM.render(<WeakConditions data={item.getAttribute("data-param") || "[]"} />, item);
    });
}
