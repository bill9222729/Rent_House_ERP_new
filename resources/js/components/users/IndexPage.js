import React from "react";
import ReactDOM from "react-dom";

const Index = () => {
    return (
        <>
            <div className="container-fluid container-fixed-lg">
                <h3 className="page-title">員工管理</h3>
            </div>
            <div className="container-fluid container-fixed-lg">
                <div className="accordion" id="accordionPanelsStayOpenExample">
                    <div className="accordion-item search-accordion">
                        <h2 className="accordion-header" id="panelsStayOpen-headingOne">
                            <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                查詢條件
                            </button>
                        </h2>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Index;

const el = document.getElementsByClassName("index");
if (el) {
    Array.from(el).forEach((item) => {
        ReactDOM.render(<Index data={item.getAttribute("data-param") || {}} />, item);
    });
}
