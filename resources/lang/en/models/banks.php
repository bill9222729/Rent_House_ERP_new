<?php

return array (
  'singular' => 'Bank',
  'plural' => 'Banks',
  'fields' => 
  array (
    'id' => 'Id',
    'bank_code' => 'Bank Code',
    'bank_account' => 'Bank Account',
    'tax' => 'Tax',
    'price' => 'Price',
    'user_number' => 'User Number',
    'company_stack_code' => 'Company Stack Code',
    'Creator' => 'Creator',
    'search' => 'Search',
    'bank_noted' => 'Bank Noted',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
