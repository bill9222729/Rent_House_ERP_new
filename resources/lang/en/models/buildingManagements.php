<?php

return array (
  'singular' => 'BuildingManagement',
  'plural' => 'BuildingManagements',
  'fields' => 
  array (
    'id' => 'Id',
    'apply_date' => 'Apply Date',
    'charter' => 'Charter',
    'escrow' => 'Escrow',
    'request_form_type' => 'Request Form Type',
    'address_city' => 'Address City',
    'address_cityarea' => 'Address Cityarea',
    'address_street' => 'Address Street',
    'address_ln' => 'Address Ln',
    'address_aly' => 'Address Aly',
    'address_num' => 'Address Num',
    'address_num_hyphen' => 'Address Num Hyphen',
    'located_city' => 'Located City',
    'located_cityarea' => 'Located Cityarea',
    'located_segment_num' => 'Located Segment Num',
    'located_small_lot' => 'Located Small Lot',
    'located_land_num' => 'Located Land Num',
    'located_build_num' => 'Located Build Num',
    'pattern' => 'Pattern',
    'pattern_remark' => 'Pattern Remark',
    'h_info_type' => 'H Info Type',
    'h_info_age' => 'H Info Age',
    'h_info_completion_date' => 'H Info Completion Date',
    'h_info_fl' => 'H Info Fl',
    'h_info_hyphen' => 'H Info Hyphen',
    'h_info_suite' => 'H Info Suite',
    'h_info_total_fl' => 'H Info Total Fl',
    'h_info_pattern_room' => 'H Info Pattern Room',
    'h_info_pattern_hall' => 'H Info Pattern Hall',
    'h_info_pattern_bath' => 'H Info Pattern Bath',
    'h_info_pattern_material' => 'H Info Pattern Material',
    'h_info_warrant_ping_num' => 'H Info Warrant Ping Num',
    'h_info_actual_ping_num' => 'H Info Actual Ping Num',
    'h_info_usage' => 'H Info Usage',
    'h_info_material' => 'H Info Material',
    'landlord_expect_rent' => 'Landlord Expect Rent',
    'landlord_expect_deposit_month' => 'Landlord Expect Deposit Month',
    'landlord_expect_deposit' => 'Landlord Expect Deposit',
    'landlord_expect_bargain' => 'Landlord Expect Bargain',
    'manage_fee' => 'Manage Fee',
    'manage_fee_month' => 'Manage Fee Month',
    'manage_fee_ping' => 'Manage Fee Ping',
    'contain_fee_ele' => 'Contain Fee Ele',
    'contain_fee_water' => 'Contain Fee Water',
    'contain_fee_gas' => 'Contain Fee Gas',
    'contain_fee_clean' => 'Contain Fee Clean',
    'contain_fee_pay_tv' => 'Contain Fee Pay Tv',
    'contain_fee_net' => 'Contain Fee Net',
    'equipment_tv' => 'Equipment Tv',
    'equipment_refrigerator' => 'Equipment Refrigerator',
    'equipment_pay_tv' => 'Equipment Pay Tv',
    'equipment_air_conditioner' => 'Equipment Air Conditioner',
    'equipment_water_heater' => 'Equipment Water Heater',
    'equipment_net' => 'Equipment Net',
    'equipment_washer' => 'Equipment Washer',
    'equipment_gas' => 'Equipment Gas',
    'equipment_bed' => 'Equipment Bed',
    'equipment_wardrobe' => 'Equipment Wardrobe',
    'equipment_table' => 'Equipment Table',
    'equipment_chair' => 'Equipment Chair',
    'equipment_sofa' => 'Equipment Sofa',
    'equipment_other' => 'Equipment Other',
    'equipment_other_detail' => 'Equipment Other Detail',
    'can_Cook' => 'Can Cook',
    'parking' => 'Parking',
    'Barrier_free_facility' => 'Barrier Free Facility',
    'curfew' => 'Curfew',
    'curfew_management' => 'Curfew Management',
    'curfew_card' => 'Curfew Card',
    'curfew_other' => 'Curfew Other',
    'curfew_other_detail' => 'Curfew Other Detail',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'house_id' => 'House Id',
    'landlord_id' => 'Landlord Id',
  ),
);
