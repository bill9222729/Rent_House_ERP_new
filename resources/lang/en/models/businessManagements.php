<?php

return array (
  'singular' => 'business_management',
  'plural' => 'business_managements',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'address' => 'Address',
    'phone' => 'Phone',
    'id_number' => 'Id Number',
    'birthday' => 'Birthday',
    'bank_account' => 'Bank Account',
    'management_fee_set' => 'Management Fee Set',
    'escrow_quota' => 'Escrow Quota',
    'box_quota' => 'Box Quota',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
