<?php

return array (
  'singular' => 'change_history',
  'plural' => 'change_histories',
  'fields' => 
  array (
    'id' => 'Id',
    'building_id' => 'Building Id',
    'apply_date' => 'Apply Date',
    'change_date' => 'Change Date',
    'lessor_type' => 'Lessor Type',
    'name' => 'Name',
    'phone' => 'Phone',
    'reason' => 'Reason',
    'change_note' => 'Change Note',
    'edit_member_id' => 'Edit Member Id',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
