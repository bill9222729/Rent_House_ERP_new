<?php

return array (
  'singular' => 'city',
  'plural' => 'cities',
  'fields' => 
  array (
    'id' => 'Id',
    'value' => 'Value',
    'name' => 'Name',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
