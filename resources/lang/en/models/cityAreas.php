<?php

return array (
  'singular' => 'city_area',
  'plural' => 'city_areas',
  'fields' => 
  array (
    'id' => 'Id',
    'city_value' => 'City Value',
    'value' => 'Value',
    'name' => 'Name',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
