<?php

return array (
  'singular' => 'contract_management',
  'plural' => 'contract_managements',
  'fields' => 
  array (
    'id' => 'Id',
    'case_status' => 'Case Status',
    'signing_date' => 'Signing Date',
    'other_landlord_desc' => 'Other Landlord Desc',
    'lease_start_date' => 'Lease Start Date',
    'lease_end_date' => 'Lease End Date',
    'rent' => 'Rent',
    'rent_pay_type' => 'Rent Pay Type',
    'bank_name' => 'Bank Name',
    'bank_branch_code' => 'Bank Branch Code',
    'bank_owner_name' => 'Bank Owner Name',
    'bank_account' => 'Bank Account',
    'deposit_amount' => 'Deposit Amount',
    'contract_note' => 'Contract Note',
    'apply_man_id' => 'Apply Man Id',
    'remark' => 'Remark',
    'file_path' => 'File Path',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
