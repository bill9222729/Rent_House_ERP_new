<?php

return array (
  'singular' => 'contracts_new',
  'plural' => 'contracts_news',
  'fields' => 
  array (
    'id' => 'Id',
    'building_case_id' => 'Building Case Id',
    'tenant_case_id' => 'Tenant Case Id',
    'lease_start_date' => 'Lease Start Date',
    'lease_end_date' => 'Lease End Date',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'charter_case_id' => 'Charter Case Id',
    'contract_type' => 'Contract Type',
    'match_id' => 'Match Id',
  ),
);
