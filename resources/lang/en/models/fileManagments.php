<?php

return array (
  'singular' => 'file_managment',
  'plural' => 'file_managments',
  'fields' => 
  array (
    'id' => 'Id',
    'file_origin_name' => 'File Origin Name',
    'file_server_name' => 'File Server Name',
    'file_size' => 'File Size',
    'file_source' => 'File Source',
    'file_source_id' => 'File Source Id',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
