<?php

return array (
  'singular' => 'fix_detail_management',
  'plural' => 'fix_detail_managements',
  'fields' => 
  array (
    'id' => 'Id',
    'report_date' => 'Report Date',
    'fix_id' => 'Fix Id',
    'repair_item' => 'Repair Item',
    'repair_manufacturers' => 'Repair Manufacturers',
    'repair_records' => 'Repair Records',
    'meeting_date' => 'Meeting Date',
    'meeting_master' => 'Meeting Master',
    'fix_date' => 'Fix Date',
    'completion_date' => 'Completion Date',
    'please_amount' => 'Please Amount',
    'reply_amount' => 'Reply Amount',
    'monthly_application_month' => 'Monthly Application Month',
    'balance' => 'Balance',
    'note' => 'Note',
    'no_reference' => 'No Reference',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
