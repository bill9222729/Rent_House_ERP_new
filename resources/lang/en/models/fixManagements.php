<?php

return array (
  'singular' => 'fix_management',
  'plural' => 'fix_managements',
  'fields' => 
  array (
    'id' => 'Id',
    'match_id' => 'Match Id',
    'building_case_no' => 'Building Case No',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
