<?php

return array (
  'singular' => 'HousePayHistory',
  'plural' => 'HousePayHistories',
  'fields' => 
  array (
    'id' => 'Id',
    'contract_id' => 'Contract Id',
    'month' => 'Month',
    'money' => 'Money',
    'type' => 'Type',
    'freq' => 'Freq',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
