<?php

return array (
  'singular' => 'insurance_management',
  'plural' => 'insurance_managements',
  'fields' => 
  array (
    'id' => 'Id',
    'sales_id' => 'Sales Id',
    'match_id' => 'Match Id',
    'building_id' => 'Building Id',
    'insurance_premium' => 'Insurance Premium',
    'insurer' => 'Insurer',
    'insurance_start' => 'Insurance Start',
    'insurance_end' => 'Insurance End',
    'monthly_application_month' => 'Monthly Application Month',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
