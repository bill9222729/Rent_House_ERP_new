<?php

return array (
  'singular' => 'LandlordManagement',
  'plural' => 'LandlordManagements',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'id_num' => 'Id Num',
    'address' => 'Address',
    'phone' => 'Phone',
    'birthday' => 'Birthday',
    'bank_acc' => 'Bank Acc',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
