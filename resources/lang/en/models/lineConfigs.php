<?php

return array (
  'singular' => 'LineConfigs',
  'plural' => 'LineConfigs',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'val' => 'Val',
    'nickname' => 'Nickname',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
