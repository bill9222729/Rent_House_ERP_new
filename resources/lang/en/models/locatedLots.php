<?php

return array (
  'singular' => 'located_lot',
  'plural' => 'located_lots',
  'fields' => 
  array (
    'id' => 'Id',
    'city_area_value' => 'City Area Value',
    'value' => 'Value',
    'name' => 'Name',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
