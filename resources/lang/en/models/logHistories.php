<?php

return array (
  'singular' => 'LogHistory',
  'plural' => 'LogHistories',
  'fields' => 
  array (
    'id' => 'Id',
    'url' => 'Url',
    'request_body' => 'Request Body',
    'user_id' => 'User Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
