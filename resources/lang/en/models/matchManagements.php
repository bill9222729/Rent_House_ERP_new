<?php

return array (
  'singular' => 'match_management',
  'plural' => 'match_managements',
  'fields' => 
  array (
    'id' => 'Id',
    'match_id' => 'Match Id',
    'building_case_id' => 'Building Case Id',
    'tenant_case_id' => 'Tenant Case Id',
    'lease_start_date' => 'Lease Start Date',
    'lease_end_date' => 'Lease End Date',
    'charter_case_id' => 'Charter Case Id',
    'contract_type' => 'Contract Type',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
