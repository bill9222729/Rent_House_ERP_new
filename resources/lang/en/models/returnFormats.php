<?php

return array (
  'singular' => 'ReturnFormat',
  'plural' => 'ReturnFormats',
  'fields' => 
  array (
    'id' => 'Id',
    't_id' => 'T Id',
    't_code' => 'T Code',
    'tax' => 'Tax',
    'bank_account' => 'Bank Account',
    'user_name' => 'User Name',
    't_price' => 'T Price',
    't_status' => 'T Status',
    'name' => 'Name',
    'item_address' => 'Item Address',
    'item_month_price' => 'Item Month Price',
    'item_price_support' => 'Item Price Support',
    'watch_price' => 'Watch Price',
    'parking_price' => 'Parking Price',
    'fix_price' => 'Fix Price',
    't_cost' => 'T Cost',
    'other_price' => 'Other Price',
    'total_price' => 'Total Price',
    'note' => 'Note',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
