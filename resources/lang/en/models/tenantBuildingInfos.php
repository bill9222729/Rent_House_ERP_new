<?php

return array (
  'singular' => 'tenant_building_info',
  'plural' => 'tenant_building_infos',
  'fields' => 
  array (
    'id' => 'Id',
    'tenant_id' => 'Tenant Id',
    'name' => 'Name',
    'located_city' => 'Located City',
    'located_Twn_spcode_area' => 'Located Twn Spcode Area',
    'located_lot_area' => 'Located Lot Area',
    'located_landno' => 'Located Landno',
    'building_no' => 'Building No',
    'building_total_square_meter_percent' => 'Building Total Square Meter Percent',
    'building_total_square_meter' => 'Building Total Square Meter',
    'addr_cntcode' => 'Addr Cntcode',
    'addr_twnspcode' => 'Addr Twnspcode',
    'addr_street_area' => 'Addr Street Area',
    'addr_lane' => 'Addr Lane',
    'addr_alley' => 'Addr Alley',
    'addr_no' => 'Addr No',
    'is_household' => 'Is Household',
    'filers' => 'Filers',
    'last_modified_person' => 'Last Modified Person',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
