<?php

return array (
  'singular' => 'tenant_minor_offspring',
  'plural' => 'tenant_minor_offsprings',
  'fields' => 
  array (
    'id' => 'Id',
    'tenant_id' => 'Tenant Id',
    'name' => 'Name',
    'is_foreigner' => 'Is Foreigner',
    'appellation' => 'Appellation',
    'id_no' => 'Id No',
    'birthday' => 'Birthday',
    'filers' => 'Filers',
    'last_modified_person' => 'Last Modified Person',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
