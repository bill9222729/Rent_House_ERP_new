<?php

return array (
  'singular' => 'TransactionDetails',
  'plural' => 'TransactionDetails',
  'fields' => 
  array (
    'id' => 'Id',
    'account' => 'Account',
    't_date' => 'T Date',
    't_time' => 'T Time',
    'a_date' => 'A Date',
    'detail' => 'Detail',
    'expense' => 'Expense',
    'income' => 'Income',
    'balance' => 'Balance',
    'remark_1' => 'Remark 1',
    'remark_2' => 'Remark 2',
    'remark_3' => 'Remark 3',
    'ticket_num' => 'Ticket Num',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
