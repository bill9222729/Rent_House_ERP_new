<?php

return [

    'add_new'      => '新增',
    'cancel'       => '取消',
    'save'         => '儲存',
    'edit'         => '編輯',
    'detail'       => 'Detail',
    'back'         => 'Back',
    'action'       => '動作',
    'id'           => 'Id',
    'created_at'   => 'Created At',
    'updated_at'   => 'Updated At',
    'deleted_at'   => 'Deleted At',
    'are_you_sure' => 'Are you sure?',
];
