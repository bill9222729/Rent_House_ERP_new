<?php

return array (
  'singular' => '匯款資料',
  'plural' => '匯款資料',
  'fields' =>
  array (
    'id' => 'Id',
    'bank_code' => '收受行代號',
    'bank_account' => '收受者帳號',
    'tax' => '收受者統編',
    'price' => '金額',
    'user_number' => '物件編號',
    'company_stack_code' => '公司股市代號',
    'Creator' => '發動者專用區',
    'search' => '查詢專用區',
    'bank_noted' => '存摺摘要',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
