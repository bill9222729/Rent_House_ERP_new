<?php

return array (
  'singular' => '業務管理',
  'plural' => '業務管理',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => '姓名',
    'address' => '地址',
    'phone' => '電話',
    'id_number' => '身分證字號',
    'birthday' => '出生年月日',
    'bank_account' => '銀行帳戶',
    'management_fee_set' => '管理費設定',
    'match_fee_set' => '媒合費設定',
    'escrow_quota' => '代管定額',
    'box_quota' => '包管定額',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
