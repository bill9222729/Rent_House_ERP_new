<?php

return array (
  'singular' => 'HousePayHistory',
  'plural' => 'HousePayHistories',
  'fields' => 
  array (
    'id' => 'Id',
    'contract_id' => '合約ID',
    'month' => '月份',
    'money' => '金額',
    'type' => '類別',
    'freq' => '期數',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
