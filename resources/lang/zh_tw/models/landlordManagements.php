<?php

return array (
  'singular' => '房東管理',
  'plural' => '房東管理',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => '姓名',
    'id_num' => '身分證字號',
    'address' => '地址',
    'phone' => '電話',
    'birthday' => '出生年月日',
    'bank_acc' => '銀行帳戶',
    'landlord_match_number' => '房東_媒合編號',
    'landlord_existing_tenant' => '房東_現有房客',
    'landlord_contract_date' => '房東_契約起日',
    'landlord_contract_expiry_date' => '房東_契約訖日',
    'landlord_item_number' => '房東_物件編號',
    'landlord_ministry_of_the_interior_number' => '房東_內政部編號',
    'landlord_types_of' => '房東_類型',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
