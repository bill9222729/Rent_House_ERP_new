<?php

return array (
  'singular' => 'Line設定',
  'plural' => 'Line設定',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => '客戶ID',
    'val' => '數值',
    'nickname' => '暱稱',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
