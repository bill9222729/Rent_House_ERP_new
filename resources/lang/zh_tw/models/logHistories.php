<?php

return array (
  'singular' => '日誌',
  'plural' => '日誌',
  'fields' => 
  array (
    'id' => 'Id',
    'url' => '路徑',
    'request_body' => '訪問內容',
    'user_id' => '使用者ID',
    'method' => '方法',
    'user' => '使用者',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
