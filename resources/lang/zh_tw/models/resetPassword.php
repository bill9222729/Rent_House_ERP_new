<?php

return array (
    'Reset Password Notification' => '重設密碼通知',
    'You are receiving this email because we received a password reset request for your account.' => "您會收到這封信是因為我們收到了您重設密碼的請求。",
    "Reset Password" => "重設密碼",
    "This password reset link will expire in :count minutes." => "此重設密碼的連結只在 :count 分鐘內有效。",
    "If you did not request a password reset, no further action is required." => "如果您並沒有送出重設密碼的請求，請不要點選此連結。",
    "Regards" => "敬上",
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n models/resetPassword.into your web browser:" => "如果點擊 \":actionText\" 按鈕無效, 請直接複製以下網址到您的瀏覽器\n",
    "Hello!" => "您好!",
);
