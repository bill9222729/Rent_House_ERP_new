<?php

return array (
  'singular' => '回傳格式',
  'plural' => '回傳格式',
  'fields' =>
  array (
    'id' => 'Id',
    't_id' => '交易序號',
    't_code' => '媒合編號',
    'tax' => '收受者統一編號',
    'bank_account' => '收受者帳號',
    'user_name' => '物件編號',
    't_price' => '交易金額',
    't_status' => '交易結果',
    'name' => '姓名',
    'item_address' => '物件地址',
    'item_month_price' => '自付額',
    'item_price_support' => '租金補助',
    'watch_price' => '管理費',
    'parking_price' => '停車費',
    'fix_price' => '修繕費',
    't_cost' => '扣手續費',
    'other_price' => '其他',
    'total_price' => '小計',
    'note' => '備註',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
