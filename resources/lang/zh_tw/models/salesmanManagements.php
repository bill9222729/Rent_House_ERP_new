<?php

return array (
  'singular' => '業務員管理',
  'plural' => '業務員管理',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => '姓名',
    'id_num' => '身分證字號',
    'address' => '地址',
    'phone' => '電話',
    'birthday' => '出生年月日',
    'bank_acc' => '銀行帳戶',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
