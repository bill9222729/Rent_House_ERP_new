<?php

return array (
  'singular' => '交易明細',
  'plural' => '交易明細',
  'fields' => 
  array (
    'id' => 'Id',
    'account' => '帳號',
    't_date' => '交易日期',
    't_time' => '交易時間',
    'a_date' => '帳務日期',
    'detail' => '交易說明',
    'expense' => '支出金額',
    'income' => '存入金額',
    'balance' => '餘額',
    'remark_1' => '備註 1',
    'remark_2' => '備註 2',
    'remark_3' => '備註 3',
    'ticket_num' => '票據號碼',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
