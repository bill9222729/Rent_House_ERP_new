<?php

return array (
  'singular' => '使用者',
  'plural' => '使用者',
  'fields' =>
  array (
    'id' => 'ID',
    'name' => '名稱',
    'email' => 'Email',
    'email_verified_at' => 'Email 認證於',
    'password' => '密碼',
    'role' => '角色',
    'remember_token' => 'Remember Token',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'stoken' => 'Stoken',
  ),
);
