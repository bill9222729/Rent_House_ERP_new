<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => ':attribute 必填',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'apply_date' => __('models/buildingManagements.fields.apply_date'),
        're_type' => __('models/buildingManagements.fields.re_type'),
        'request_form_type' => __('models/buildingManagements.fields.request_form_type'),
        'address_city' => __('models/buildingManagements.fields.address_city'),
        'address_cityarea' => __('models/buildingManagements.fields.address_cityarea'),
        'address_street' => __('models/buildingManagements.fields.address_street'),
        'address_ln' => __('models/buildingManagements.fields.address_ln'),
        'address_aly' => __('models/buildingManagements.fields.address_aly'),
        'address_num' => __('models/buildingManagements.fields.address_num'),
        'address_num_hyphen' => __('models/buildingManagements.fields.address_num_hyphen'),
        'located_city' => __('models/buildingManagements.fields.located_city'),
        'located_cityarea' => __('models/buildingManagements.fields.located_cityarea'),
        'located_build_address' => __('models/buildingManagements.fields.located_build_address'),
        'located_segment_num' => __('models/buildingManagements.fields.located_segment_num'),
        'located_small_lot' => __('models/buildingManagements.fields.located_small_lot'),
        'located_land_num' => __('models/buildingManagements.fields.located_land_num'),
        'located_build_num' => __('models/buildingManagements.fields.located_build_num'),
        'pattern' => __('models/buildingManagements.fields.pattern'),
        'pattern_remark' => __('models/buildingManagements.fields.pattern_remark'),
        'h_info_type' => __('models/buildingManagements.fields.h_info_type'),
        'h_info_age' => __('models/buildingManagements.fields.h_info_age'),
        'h_info_completion_date' => __('models/buildingManagements.fields.h_info_completion_date'),
        'h_info_fl' => __('models/buildingManagements.fields.h_info_fl'),
        'h_info_hyphen' => __('models/buildingManagements.fields.h_info_hyphen'),
        'h_info_suite' => __('models/buildingManagements.fields.h_info_suite'),
        'h_info_total_fl' => __('models/buildingManagements.fields.h_info_total_fl'),
        'h_info_pattern_room' => __('models/buildingManagements.fields.h_info_pattern_room'),
        'h_info_pattern_hall' => __('models/buildingManagements.fields.h_info_pattern_hall'),
        'h_info_pattern_bath' => __('models/buildingManagements.fields.h_info_pattern_bath'),
        'h_info_pattern_material' => __('models/buildingManagements.fields.h_info_pattern_material'),
        'h_info_warrant_ping_num' => __('models/buildingManagements.fields.h_info_warrant_ping_num'),
        'h_info_actual_ping_num' => __('models/buildingManagements.fields.h_info_actual_ping_num'),
        'h_info_usage' => __('models/buildingManagements.fields.h_info_usage'),
        'h_info_material' => __('models/buildingManagements.fields.h_info_material'),
        'landlord_expect_rent' => __('models/buildingManagements.fields.landlord_expect_rent'),
        'landlord_expect_deposit_month' => __('models/buildingManagements.fields.landlord_expect_deposit_month'),
        'landlord_expect_deposit' => __('models/buildingManagements.fields.landlord_expect_deposit'),
        'landlord_expect_bargain' => __('models/buildingManagements.fields.landlord_expect_bargain'),
        'manage_fee' => __('models/buildingManagements.fields.manage_fee'),
        'manage_fee_month' => __('models/buildingManagements.fields.manage_fee_month'),
        'manage_fee_ping' => __('models/buildingManagements.fields.manage_fee_ping'),
        'contain_fee_ele' => __('models/buildingManagements.fields.contain_fee_ele'),
        'contain_fee_water' => __('models/buildingManagements.fields.contain_fee_water'),
        'contain_fee_gas' => __('models/buildingManagements.fields.contain_fee_gas'),
        'contain_fee_clean' => __('models/buildingManagements.fields.contain_fee_clean'),
        'contain_fee_pay_tv' => __('models/buildingManagements.fields.contain_fee_pay_tv'),
        'contain_fee_net' => __('models/buildingManagements.fields.contain_fee_net'),
        'equipment_tv' => __('models/buildingManagements.fields.equipment_tv'),
        'equipment_refrigerator' => __('models/buildingManagements.fields.equipment_refrigerator'),
        'equipment_pay_tv' => __('models/buildingManagements.fields.equipment_pay_tv'),
        'equipment_air_conditioner' => __('models/buildingManagements.fields.equipment_air_conditioner'),
        'equipment_water_heater' => __('models/buildingManagements.fields.equipment_water_heater'),
        'equipment_net' => __('models/buildingManagements.fields.equipment_net'),
        'equipment_washer' => __('models/buildingManagements.fields.equipment_washer'),
        'equipment_gas' => __('models/buildingManagements.fields.equipment_gas'),
        'equipment_bed' => __('models/buildingManagements.fields.equipment_bed'),
        'equipment_wardrobe' => __('models/buildingManagements.fields.equipment_wardrobe'),
        'equipment_table' => __('models/buildingManagements.fields.equipment_table'),
        'equipment_chair' => __('models/buildingManagements.fields.equipment_chair'),
        'equipment_sofa' => __('models/buildingManagements.fields.equipment_sofa'),
        'equipment_other' => __('models/buildingManagements.fields.equipment_other'),
        'equipment_other_detail' => __('models/buildingManagements.fields.equipment_other_detail'),
        'can_Cook' => __('models/buildingManagements.fields.can_Cook'),
        'parking' => __('models/buildingManagements.fields.parking'),
        'Barrier_free_facility' => __('models/buildingManagements.fields.Barrier_free_facility'),
        'curfew' => __('models/buildingManagements.fields.curfew'),
        'curfew_management' => __('models/buildingManagements.fields.curfew_management'),
        'curfew_card' => __('models/buildingManagements.fields.curfew_card'),
        'curfew_other' => __('models/buildingManagements.fields.curfew_other'),
        'curfew_other_detail' => __('models/buildingManagements.fields.curfew_other_detail'),
        'house_id' => __('models/buildingManagements.fields.house_id'),
        'landlord_id' => __('models/buildingManagements.fields.landlord_id'),
        'status' => __('models/buildingManagements.fields.status'),
        'num' => __('models/buildingManagements.fields.num'),
        'Lessor_name' => __('models/buildingManagements.fields.Lessor_name'),
        'Lessor_gender' => __('models/buildingManagements.fields.Lessor_gender'),
        'Lessor_birthday' => __('models/buildingManagements.fields.Lessor_birthday'),
        'Lessor_ID_num' => __('models/buildingManagements.fields.Lessor_ID_num'),
        'Lessor_phone' => __('models/buildingManagements.fields.Lessor_phone'),
        'residence_address_city' => __('models/buildingManagements.fields.residence_address_city'),
        'residence_address_cityarea' => __('models/buildingManagements.fields.residence_address_cityarea'),
        'residence_address_street' => __('models/buildingManagements.fields.residence_address_street'),
        'residence_address_ln' => __('models/buildingManagements.fields.residence_address_ln'),
        'residence_address_aly' => __('models/buildingManagements.fields.residence_address_aly'),
        'residence_address_num' => __('models/buildingManagements.fields.residence_address_num'),
        'residence_address_num_hyphen' => __('models/buildingManagements.fields.residence_address_num_hyphen'),
        'collection_agent' => __('models/buildingManagements.fields.collection_agent'),
        'agent_ID_num' => __('models/buildingManagements.fields.agent_ID_num'),
        'agent_bank_name' => __('models/buildingManagements.fields.agent_bank_name'),
        'agent_branch_name' => __('models/buildingManagements.fields.agent_branch_name'),
        'landlord_bank_account' => __('models/buildingManagements.fields.landlord_bank_account'),
        'remark_1' => __('models/buildingManagements.fields.remark_1'),
        'remark_2' => __('models/buildingManagements.fields.remark_2'),
        'park_mang_fee' => __('models/buildingManagements.fields.park_mang_fee'),
        'lessee_name' => __('models/tenantManagements.fields.lessee_name'),
        'lessee_gender' => __('models/tenantManagements.fields.lessee_gender'),
        'lessee_birthday' => __('models/tenantManagements.fields.lessee_birthday'),
        'lessee_id_num' => __('models/tenantManagements.fields.lessee_id_num'),
        'lessee_hc_num' => __('models/tenantManagements.fields.lessee_hc_num'),
        'lessee_telephone_d' => __('models/tenantManagements.fields.lessee_telephone_d'),
        'lessee_telephone_n' => __('models/tenantManagements.fields.lessee_telephone_n'),
        'lessee_cellphone' => __('models/tenantManagements.fields.lessee_cellphone'),
        'lessee_email' => __('models/tenantManagements.fields.lessee_email'),
        'qualifications' => __('models/tenantManagements.fields.qualifications'),
        'hr_city' => __('models/tenantManagements.fields.hr_city'),
        'hr_cityarea' => __('models/tenantManagements.fields.hr_cityarea'),
        'hr_address' => __('models/tenantManagements.fields.hr_address'),
        'contact_city' => __('models/tenantManagements.fields.contact_city'),
        'contact_cityarea' => __('models/tenantManagements.fields.contact_cityarea'),
        'contact_address' => __('models/tenantManagements.fields.contact_address'),
        'contract_number_of_periods' => __('models/contracts.fields.contract_number_of_periods'),
        'contract_category' => __('models/contracts.fields.contract_category'),
        'contract_business_sign_back' => __('models/contracts.fields.contract_business_sign_back'),
        'contract_match_date' => __('models/contracts.fields.contract_match_date'),
        'landlord_match_number' => __('models/contracts.fields.landlord_match_number'),
        'landlord_existing_tenant' => __('models/contracts.fields.landlord_existing_tenant'),
        'landlord_contract_date' => __('models/contracts.fields.landlord_contract_date'),
        'landlord_contract_expiry_date' => __('models/contracts.fields.landlord_contract_expiry_date'),
        'landlord_item_number' => __('models/contracts.fields.landlord_item_number'),
        'landlord_ministry_of_the_interior_number' => __('models/contracts.fields.landlord_ministry_of_the_interior_number'),
        'landlord_types_of' => __('models/contracts.fields.landlord_types_of'),
        'tenant_case_number' => __('models/contracts.fields.tenant_case_number'),
        'tenant_ministry_of_the_interior_number' => __('models/contracts.fields.tenant_ministry_of_the_interior_number'),
        'rent_evaluation_form_market_rent' => __('models/contracts.fields.rent_evaluation_form_market_rent'),
        'rent_evaluation_form_assess_rent' => __('models/contracts.fields.rent_evaluation_form_assess_rent'),
        'actual_contract_rent_to_landlord' => __('models/contracts.fields.actual_contract_rent_to_landlord'),
        'actual_contract_tenant_pays_rent' => __('models/contracts.fields.actual_contract_tenant_pays_rent'),
        'actual_contract_rent_subsidy' => __('models/contracts.fields.actual_contract_rent_subsidy'),
        'actual_contract_deposit' => __('models/contracts.fields.actual_contract_deposit'),
        'to_the_landlord_rent_day' => __('models/contracts.fields.to_the_landlord_rent_day'),
        'package_escrow_fee_month_minute' => __('models/contracts.fields.package_escrow_fee_month_minute'),
        'package_escrow_fee_amount' => __('models/contracts.fields.package_escrow_fee_amount'),
        'package_escrow_fee_number_of_periods' => __('models/contracts.fields.package_escrow_fee_number_of_periods'),
        'rent_subsidy_month_minute' => __('models/contracts.fields.rent_subsidy_month_minute'),
        'rent_subsidy_amount' => __('models/contracts.fields.rent_subsidy_amount'),
        'rent_subsidy_number_of_periods' => __('models/contracts.fields.rent_subsidy_number_of_periods'),
        'development_matching_fees_month' => __('models/contracts.fields.development_matching_fees_month'),
        'development_matching_fees_amount' => __('models/contracts.fields.development_matching_fees_amount'),
        'notary_fees_notarization_date' => __('models/contracts.fields.notary_fees_notarization_date'),
        'notary_fees_month' => __('models/contracts.fields.notary_fees_month'),
        'notary_fees_amount' => __('models/contracts.fields.notary_fees_amount'),
        'repair_cost_month_minute' => __('models/contracts.fields.repair_cost_month_minute'),
        'repair_cost_amount' => __('models/contracts.fields.repair_cost_amount'),
        'insurance_month_minute' => __('models/contracts.fields.insurance_month_minute'),
        'insurance_amount' => __('models/contracts.fields.insurance_amount'),
        'insurance_period_three_year' => __('models/contracts.fields.insurance_period_three_year'),
        'insurance_period_three_start_and_end_date' => __('models/contracts.fields.insurance_period_three_start_and_end_date'),
    ],

];
