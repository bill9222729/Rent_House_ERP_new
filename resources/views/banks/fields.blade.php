<div class="row">
<!-- Bank Code Field -->
<div class="form-group col-sm-4">
    {!! Form::label('bank_code', __('models/banks.fields.bank_code').':') !!}
    {!! Form::text('bank_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Account Field -->
<div class="form-group col-sm-4">
    {!! Form::label('bank_account', __('models/banks.fields.bank_account').':') !!}
    {!! Form::text('bank_account', null, ['class' => 'form-control','maxlength'=>'14','minlength'=>'12']) !!}
</div>

<!-- Tax Field -->
<div class="form-group col-sm-4">
    {!! Form::label('tax', __('models/banks.fields.tax').':') !!}
    {!! Form::text('tax', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-4">
    {!! Form::label('price', __('models/banks.fields.price').':') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- User Number Field -->
<div class="form-group col-sm-4">
    {!! Form::label('user_number', __('models/banks.fields.user_number').':') !!}
    {!! Form::text('user_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Company Stack Code Field -->
<div class="form-group col-sm-4">
    {!! Form::label('company_stack_code', __('models/banks.fields.company_stack_code').':') !!}
    {!! Form::text('company_stack_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Field -->
<div class="form-group col-sm-4">
    {!! Form::label('Creator', __('models/banks.fields.Creator').':') !!}
    {!! Form::text('Creator', null, ['class' => 'form-control']) !!}
</div>

<!-- Search Field -->
<div class="form-group col-sm-4">
    {!! Form::label('search', __('models/banks.fields.search').':') !!}
    {!! Form::text('search', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Noted Field -->
<div class="form-group col-sm-4">
    {!! Form::label('bank_noted', __('models/banks.fields.bank_noted').':') !!}
    {!! Form::text('bank_noted', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('banks.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
