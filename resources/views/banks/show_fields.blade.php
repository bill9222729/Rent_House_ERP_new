<!-- Bank Code Field -->
<div class="form-group col-12">
    {!! Form::label('bank_code', __('models/banks.fields.bank_code').':') !!}
    <p>{{ $bank->bank_code }}</p>
</div>


<!-- Bank Account Field -->
<div class="form-group col-12">
    {!! Form::label('bank_account', __('models/banks.fields.bank_account').':') !!}
    <p>{{ $bank->bank_account }}</p>
</div>


<!-- Tax Field -->
<div class="form-group col-12">
    {!! Form::label('tax', __('models/banks.fields.tax').':') !!}
    <p>{{ $bank->tax }}</p>
</div>


<!-- Price Field -->
<div class="form-group col-12">
    {!! Form::label('price', __('models/banks.fields.price').':') !!}
    <p>{{ $bank->price }}</p>
</div>


<!-- User Number Field -->
<div class="form-group col-12">
    {!! Form::label('user_number', __('models/banks.fields.user_number').':') !!}
    <p>{{ $bank->user_number }}</p>
</div>


<!-- Company Stack Code Field -->
<div class="form-group col-12">
    {!! Form::label('company_stack_code', __('models/banks.fields.company_stack_code').':') !!}
    <p>{{ $bank->company_stack_code }}</p>
</div>


<!-- Creator Field -->
<div class="form-group col-12">
    {!! Form::label('Creator', __('models/banks.fields.Creator').':') !!}
    <p>{{ $bank->Creator }}</p>
</div>


<!-- Search Field -->
<div class="form-group col-12">
    {!! Form::label('search', __('models/banks.fields.search').':') !!}
    <p>{{ $bank->search }}</p>
</div>


<!-- Bank Noted Field -->
<div class="form-group col-12">
    {!! Form::label('bank_noted', __('models/banks.fields.bank_noted').':') !!}
    <p>{{ $bank->bank_noted }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-12">
    {!! Form::label('created_at', __('models/banks.fields.created_at').':') !!}
    <p>{{ $bank->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-12">
    {!! Form::label('updated_at', __('models/banks.fields.updated_at').':') !!}
    <p>{{ $bank->updated_at }}</p>
</div>


