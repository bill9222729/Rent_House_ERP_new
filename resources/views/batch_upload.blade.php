@extends('layouts.app')

@section('content')
    <style>
        .lds-roller {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }

        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 40px 40px;
        }

        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 7px;
            height: 7px;
            border-radius: 50%;
            background: #fff;
            margin: -4px 0 0 -4px;
        }

        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }

        .lds-roller div:nth-child(1):after {
            top: 63px;
            left: 63px;
        }

        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }

        .lds-roller div:nth-child(2):after {
            top: 68px;
            left: 56px;
        }

        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }

        .lds-roller div:nth-child(3):after {
            top: 71px;
            left: 48px;
        }

        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }

        .lds-roller div:nth-child(4):after {
            top: 72px;
            left: 40px;
        }

        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }

        .lds-roller div:nth-child(5):after {
            top: 71px;
            left: 32px;
        }

        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }

        .lds-roller div:nth-child(6):after {
            top: 68px;
            left: 24px;
        }

        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }

        .lds-roller div:nth-child(7):after {
            top: 63px;
            left: 17px;
        }

        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }

        .lds-roller div:nth-child(8):after {
            top: 56px;
            left: 12px;
        }

        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .spinner {
            display: none;
            position: absolute;
            place-items: center;
            background: rgba(32, 32, 32, .6);
            width: 100vw;
            height: 100vh;
            top: 0;
            left: 0;
            z-index: 132313;
        }

    </style>
    <div class="container">
        <div class="row">
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="landlord-file" accept=".xlsx,.xls">
                    <label class="custom-file-label" for="landlord-file">選擇房東資料</label>
                </div>
            </div>
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="tenant-file" accept=".xlsx,.xls">
                    <label class="custom-file-label" for="tenant-file">選擇房客資料</label>
                </div>
            </div>
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="contract-file" accept=".xlsx,.xls">
                    <label class="custom-file-label" for="contract-file">選擇媒合資料</label>
                </div>
            </div>
            <button class="btn-primary btn" id="upload">上傳</button>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>檔案</th>
                        <th>行號</th>
                        <th>案號</th>
                        <th>結果</th>
                    </tr>
                </thead>
                <tbody id="result-container">
                </tbody>
            </table>
        </div>
    </div>
    <div class="spinner">
        <div class="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        $('[id$="-file"]').on('change', e => {
            e.target.nextElementSibling.innerHTML = e.target.files[0].name
        })
        $('#upload').on('click', async () => {
            $('.spinner').css('display', 'grid')
            $result = $('#result-container')
            let stop = false
            let fileInput = $('#landlord-file')[0]
            let file = fileInput.files ? fileInput.files[0] : null
            if (file) {
                const landlordFilename = $('#landlord-file')[0].files[0].name
                const landlordFormData = new FormData()
                landlordFormData.append('type', 'landlord')
                landlordFormData.append('file', $('#landlord-file')[0].files[0])
                await $.ajax({
                    url: '/api/batch_upload',
                    data: landlordFormData,
                    type: 'post',
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        data.forEach(row => {
                            $result.prepend($(`
                            <tr>
                                <td>${landlordFilename}</td>
                                <td>${row.line}</td>
                                <td>${row.case_num}</td>
                                <td>${row.result}</td>
                            </tr>
                        `))
                        })
                        $('#landlord-file').val(null)
                        $('#landlord-file + label').text('已上傳房東資料')
                    },
                    error: () => {
                        Swal.fire(
                            '房東檔案格式錯誤',
                            '',
                            'error'
                        )
                    },
                }).catch(() => {
                    stop = true
                })
            }
            if (stop) return $('.spinner').css('display', '')
            fileInput = $('#tenant-file')[0]
            file = fileInput.files ? fileInput.files[0] : null
            if (file) {
                const tenantFilename = $('#tenant-file')[0].files[0].name
                const tenantFormData = new FormData()
                tenantFormData.append('type', 'tenant')
                tenantFormData.append('file', $('#tenant-file')[0].files[0])
                await $.ajax({
                    url: '/api/batch_upload',
                    data: tenantFormData,
                    type: 'post',
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        data.forEach(row => {
                            $result.prepend($(`
                            <tr>
                                <td>${tenantFilename}</td>
                                <td>${row.line}</td>
                                <td>${row.case_num}</td>
                                <td>${row.result}</td>
                            </tr>
                        `))
                        })
                        $('#tenant-file').val(null)
                        $('#tenant-file + label').text('已上傳房客資料')
                    },
                    error: () => {
                        Swal.fire(
                            '房客檔案格式錯誤',
                            '',
                            'error'
                        )
                    },
                }).catch(() => {
                    stop = true
                })
            }
            if (stop) return $('.spinner').css('display', '')
            fileInput = $('#contract-file')[0]
            file = fileInput.files ? fileInput.files[0] : null
            if (file) {
                const contractFilename = $('#contract-file')[0].files[0].name
                const contractFormData = new FormData()
                contractFormData.append('type', 'contract')
                contractFormData.append('file', $('#contract-file')[0].files[0])
                await $.ajax({
                    url: '/api/batch_upload',
                    data: contractFormData,
                    type: 'post',
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        data.forEach(row => {
                            $result.prepend($(`
                            <tr>
                                <td>${contractFilename}</td>
                                <td>${row.line}</td>
                                <td>${row.match_num}</td>
                                <td>${row.result}</td>
                            </tr>
                        `))
                        })
                        $('#contract-file').val(null)
                        $('#contract-file + label').text('已上傳媒合資料')
                    },
                    error: () => {
                        Swal.fire(
                            '媒合檔案格式錯誤',
                            '',
                            'error'
                        )
                    },
                }).catch(() => {})
            }
            $('.spinner').css('display', '')
        })
    </script>
@endpush
