<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingThree">
        <button class="accordion-button" type="button" data-bs-toggle="collapse"
            data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true"
            aria-controls="panelsStayOpen-collapseThree">
            共同持分人
        </button>
    </h2>
    <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse show"
        aria-labelledby="panelsStayOpen-headingThree">
        <div class="accordion-body">
            {{-- 這個是react元件，寫在resources\js\components\BuildingSharer.js --}}
            <div class="container buildingSharer" id="buildingSharer"
                data-param="{{ isset($buildingManagementNew) ? $buildingManagementNew['stakeholders'] : '[]' }}">
            </div>
        </div>
    </div>
</div>
