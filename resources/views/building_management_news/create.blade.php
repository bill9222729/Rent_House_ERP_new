@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

        .input-group label.error {
            position: absolute;
            bottom: -1rem;
        }
    </style>
@endsection
@section('content')
    <form id="building_create_form">
        <div class="container-fluid container-fixed-lg">
            <h3 class="page-title">房東管理</h3>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                @if ($_GET['type'] == 'NaturalPerson')
                    <input type="hidden" name="lessor_type" id="lessor_type" value="NaturalPerson" />
                    @include('building_management_news.feilds_A')
                @else
                    <input type="hidden" name="lessor_type" id="lessor_type" value="LegalPerson" />
                    @include('building_management_news.feilds_B')
                @endif
                @include('building_management_news.feilds_C')
            </div>
        </div>
        <div class="container-fluid py-5">
            <div class="row justify-content-center">
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary" id="create-button">暫存</button>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-secondary" id="cancel-button">取消</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/building_management.js') }}"></script>
    <script>
        $(document).ready(async function() {
            let salesmanData = await getSalesmanData();
            // 設定搜尋部份的業務選項
            salesmanData.forEach((item) => {
                $("#sales_id").append($("<option></option>").attr("value", item.id).text(
                    `(${item.id})${item.name}`));
            });

            initBuildingManagementsPage();

            // 新增資料的函式
            const saveBuildingData = () => {
                let data = getAllfieldsValue();
                data["creator"] = "{{Auth::user()->id}}";
                $("#create-button").attr("disabled",true);
                // 存進資料庫
                $.ajax({
                    method: "POST",
                    url: "/api/building_management_news",
                    data: data,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已成功新增',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "/buildingManagementNews";
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                }).then(()=> {
                    $("#create-button").attr("disabled",false);
                });
            }

            // 表單驗證
            addFormValid("building_create_form", saveBuildingData);
        });
    </script>
@endpush
