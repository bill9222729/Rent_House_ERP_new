@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

        .input-group label.error {
            position: absolute;
            bottom: -1rem;
        }
    </style>
@endsection
@section('content')
    <form id="building_edit_form">
        <div class="container-fluid container-fixed-lg">
            <h3 class="page-title">房東管理-編輯</h3>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                @if ($buildingManagementNew['lessor_type'] == 'NaturalPerson')
                    @include('building_management_news.feilds_A')
                @else
                    @include('building_management_news.feilds_B')
                @endif
                @include('building_management_news.feilds_C')
            </div>
            @include('building_management_news.modal_change_NP')
            @include('building_management_news.modal_change_LP')
            @include('building_management_news.modal_change_history')
        </div>
        <div class="container-fluid py-5">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary" id="create-button">暫存</button>
                </div>
                @can('sales')
                    <div class="col-auto">
                        <button type="button" class="btn btn-success text-nowrap" onclick="changeDataStatusToStaff()">送出資料</button>
                    </div>
                @endcan
                @canany(['staff', 'admin'])
                    @if ($buildingManagementNew->data_status == 'staff')
                        <div class="col-auto">
                            <button type="button" class="btn btn-success text-nowrap" onclick="changeDataStatusToSales()">退回資料</button>
                        </div>
                    @endif
                @endcan
                <div class="col-auto">
                    <button type="button" class="btn btn-secondary" id="cancel-button" onclick="window.location.href='/buildingManagementNews'">取消</button>
                </div>
            </div>
        </div>
    </form>
@endsection
{{-- 取得使用者資料 --}}
@php
$user = Auth::user();
@endphp
{{-- javascript --}}
@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/building_management.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const buildingManagementNew = @json($buildingManagementNew);
        const user = @json($user);
        console.log(user);
        console.log("頁面資料：buildingManagementNew");
        console.log(buildingManagementNew);
        $(document).ready(async function() {
            // 初始化各種東西
            initBuildingManagementsPage(buildingManagementNew);
            // 初始化業務select
            let salesmanData = await getSalesmanData();
            // 設定搜尋部份的業務選項
            salesmanData.forEach((item) => {
                $("#sales_id").append($("<option></option>").attr("value", item.id).text(item.name));
            });
            // 填值銀行select
            $("#bank_name").selectpicker('val', buildingManagementNew['bank_name']);

            $("#building_address_city_area").html(
                await ganerateCityAreaOption(buildingManagementNew["building_address_city"])
            );
            $("#building_address_street").html(
                await ganerateStreetOption(buildingManagementNew["building_address_city_area"])
            );
            $('#building_address_street').selectpicker('refresh');
            $('#building_address_street').val(buildingManagementNew["building_address_street"]);
            $('#building_address_street').selectpicker('render');

            $("#building_located_city_area").html(
                await ganerateCityAreaOption(buildingManagementNew["building_located_city"])
            );
            $("#building_located_lot").html(
                await ganerateLocatedLotOption(buildingManagementNew["building_located_city_area"])
            );
            $('#building_located_lot').selectpicker('refresh');
            $('#building_located_lot').val(buildingManagementNew["building_located_lot"]);
            $('#building_located_lot').selectpicker('render');
            // 初始化資料庫資料到畫面上
            fillDataToEditPage(buildingManagementNew);
            // 儲存資料按鈕的事件
            const updateBuildingData = () => {
                let data = getAllfieldsValue();
                data["sales_id"] = parseInt(data["sales_id"]);
                // 存進資料庫
                $.ajax({
                    method: "PATCH",
                    url: `https://rent-house-erp.echouse.co/api/building_management_news/${buildingManagementNew.id}`,
                    data: data,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已更新',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "/buildingManagementNews";
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                })
            }
            // modal的儲存事件
            function submitModal(type) {
                let data = getChangeLessorModalAllfieldsValue(type);
                // 前處理資料
                data["apply_date"] = getTodayDate();
                data["phone"] = data["cellphone"];
                data["building_id"] = buildingManagementNew["id"];
                data["edit_member_id"] = user["id"];
                data["lessor_type"] = type === "natural_person" ? "NaturalPerson" : "LegalPerson";
                // 存進資料庫

                // 先放一筆到變更歷程
                $.ajax({
                    method: "POST",
                    url: `https://rent-house-erp.echouse.co/api/change_histories`,
                    data: data,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已更新',
                            'success'
                        ).then((result) => {
                            // if (result.isConfirmed) {
                            //     window.location.href = "/buildingManagementNews";
                            // }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                })

                // 再更動building_management_new裡面的資料
                $.ajax({
                    method: "PATCH",
                    url: `https://rent-house-erp.echouse.co/api/building_management_news/${buildingManagementNew.id}`,
                    data: data,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已更新',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload()
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                })
            }

            // modal的儲存按鈕事件(自然人)
            $("#natural_person .submit").on("click", () => {
                submitModal("natural_person");
            })

            // modal的儲存按鈕事件(法人)
            $("#legal_person .submit").on("click", () => {
                submitModal("legal_person");
            })

            // 初始化變更歷程的datatable
            console.log(`/api/change_histories?building_id=${buildingManagementNew.id}`);
            const change_history_table = $('#change_history_table').DataTable({
                "responsive": true,
                "searching": false,
                "dom": 'Bfrtip',
                "ajax": {
                    type: 'GET',
                    url: `/api/change_histories?building_id=${buildingManagementNew.id}`,
                    dataSrc: function(data) {
                        const lessor_type = {
                            "NaturalPerson": "自然人",
                            "LegalPerson": "法人",
                        }
                        return data.data.map((value, index, array) => {
                            value["lessor_type"] = lessor_type[value["lessor_type"]];
                            return value
                        });
                    }
                },
                "columns": [{
                        data: 'change_date',
                        title: "異動日期"
                    },
                    {
                        data: 'lessor_type',
                        title: "承租人類型"
                    },
                    {
                        data: 'name',
                        title: "姓名(名稱)"
                    },
                    {
                        data: 'phone',
                        title: "電話"
                    },
                    {
                        data: 'reason',
                        title: '變更原因'
                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });

            // 如果是檢視模式的話就把所有欄位鎖起來
            let targetUrl = new URL(location.href);
            let cantEdit = targetUrl.searchParams.get("cantedit") === "true";
            if (cantEdit) {
                $("input").attr("readonly", "readonly");
                $("input[type='checkbox']").attr("disabled", true);
                $("input[type='radio']").attr("disabled", true);
                $("select").selectpicker("destroy");
                $("select, button").attr("disabled", "disabled");
                $("textarea").attr("disabled", "disabled");
            }

            // 欄位驗證
            addFormValid("building_edit_form", updateBuildingData);
        });
    </script>
@endpush
