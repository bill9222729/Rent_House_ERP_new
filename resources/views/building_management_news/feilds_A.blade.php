<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse"
            data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
            aria-controls="panelsStayOpen-collapseOne">
            編輯物件出租申請
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
        aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap">申請日期</label>
                        <div class="col-md-12 gx-1">
                            <div class="input-group date day">
                                <input type="text" name="apply_date" value="" class="form-control" autocomplete="off"
                                    maxlength="10" size="10" placeholder="YYY-MM-DD" id="apply_date" aria-label="民國年"
                                    aria-describedby="button-addon1">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap">本人欲提供住宅</label>
                        <div class="col-md-12">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="case_type_chartering_escrow"
                                    id="case_type_chartering" value="case_type_chartering">
                                <label class="form-check-label" for="case_type_chartering">
                                    出租予租屋服務事業再轉租(包租)
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="case_type_chartering_escrow"
                                    id="case_type_escrow" value="case_type_escrow" checked>
                                <label class="form-check-label" for="case_type_escrow">
                                    經由租屋服務事業協助出租(代管)
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap">委託租賃</label>
                        <div class="row col-md-12">
                            <div class="col gx-0">
                                <div class="input-group date day ">
                                    <input type="text" name="entrusted_lease_start_date" value="" class="form-control"
                                        autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                        id="entrusted_lease_start_date" aria-label="民國年"
                                        aria-describedby="button-addon1">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                            &emsp;~&emsp;
                            <div class="col gx-0">
                                <div class="input-group date day ">
                                    <input type="text" name="entrusted_lease_end_date" value="" class="form-control"
                                        autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                        id="entrusted_lease_end_date" aria-label="民國年" aria-describedby="button-addon2">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap">委託管理</label>
                        <div class="row col-md-12">
                            <div class="col gx-0">
                                <div class="input-group date day ">
                                    <input type="text" name="entrusted_management_start_date" value=""
                                        class="form-control" autocomplete="off" maxlength="10" size="10"
                                        placeholder="YYY-MM-DD" id="entrusted_management_start_date" aria-label="民國年"
                                        aria-describedby="button-addon1">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                            &emsp;~&emsp;
                            <div class="col gx-0">
                                <div class="input-group date day ">
                                    <input type="text" name="entrusted_management_end_date" value=""
                                        class="form-control" autocomplete="off" maxlength="10" size="10"
                                        placeholder="YYY-MM-DD" id="entrusted_management_end_date" aria-label="民國年"
                                        aria-describedby="button-addon2">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="row col-md-6">
                    <label for="government_no"
                        class="col-md-12 col-form-label fw-bolder text-nowrap">內政部編號</label>
                    <div class="col-md-12">
                        <input type="text" name="government_no" class="form-control" id="government_no" style="text-transform:uppercase">
                    </div>
                </div>
                <div class="row col-md-6">
                    <label for="case_no" class="col-md-12 col-form-label fw-bolder text-nowrap">物件編號</label>
                    <div class="col-md-12">
                        <input type="text" name="case_no" class="form-control" id="case_no" style="text-transform:uppercase">
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="row col-md-6">
                    <label for="building_source"
                        class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*物件來源</label>
                    <div class="col-md-12">
                        <select class="form-select form-control" name="building_source" id="building_source">
                            <option value="">請選擇</option>
                            <option value="無">無</option>
                            <option value="591">591</option>
                            <option value="公司資源">公司資源</option>
                            <option value="房東說明會">房東說明會</option>
                            <option value="保全介紹">保全介紹</option>
                            <option value="客戶介紹">客戶介紹</option>
                            <option value="掃街">掃街</option>
                            <option value="經營">經營</option>
                            <option value="緣故">緣故</option>
                            <option value="賣不出去">賣不出去</option>
                            <option value="親友開發">親友開發</option>
                            <option value="轉介">轉介</option>
                        </select>
                    </div>
                </div>
                <div class="row col-md-6">
                    <label for="case_no" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*物件狀態</label>
                    <div class="col-md-12">
                        <select class="form-select form-control" name="case_state" id="case_state">
                            <option value="" selected>請選擇</option>
                            <option value="招租中">招租中</option>
                            <option value="已媒合">已媒合</option>
                            <option value="暫停">暫停</option>
                            <option value="警示客戶">警示客戶</option>
                            <option value="退出計畫">退出計畫</option>
                            <option value="待追蹤">待追蹤</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="row col-md-6">
                    <label class="col-md-12 col-form-label fw-bolder text-nowrap text-danger" for="sales_id">*業務</label>
                    <div class="col-md-12">
                        <select class="form-select form-control" name="sales_id" id="sales_id">
                            <option value="" selected>請選擇</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@include('building_management_news.lessor_information_NP')
