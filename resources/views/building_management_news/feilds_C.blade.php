@include('building_management_news.building_sharer')
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingFour">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="true" aria-controls="panelsStayOpen-collapseFour">
            出租住宅基本資料
        </button>
    </h2>
    <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFour">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mt-5">
                    <label for="building_address_city" class="col-md-12 col-form-label text-danger fw-bolder">*地址</label>
                    <div class="col-md-8 col-12 col-xl-6 row">
                        <div class="input-group mb-3 gx-0">
                            <select class="form-select" name="building_address_city" id="building_address_city">
                                <option value="">請選擇</option>
                            </select>
                            <select class="form-select" name="building_address_city_area" id="building_address_city_area">
                                <option value="">請選擇</option>
                            </select>
                            <select class="form-select form-control selectpicker" id="building_address_street" name="building_address_street" data-live-search="true" data-size="5" data-style="border text-dark bg-white" data-width="auto" style="flex-grow: 3">
                                <option value="">請選擇</option>
                            </select>
                        </div>
                        <div class="input-group mb-3 gx-0">
                            <input type="text" class="form-control" placeholder="" name="building_address_ln" id="building_address_ln">
                            <span class="input-group-text">巷</span>
                            <input type="text" class="form-control" placeholder="" name="building_address_aly" id="building_address_aly">
                            <span class="input-group-text">弄</span>
                            <input type="text" class="form-control" placeholder="" name="building_address_num" id="building_address_num">
                            <span class="input-group-text">號之</span>
                            <input type="text" class="form-control" placeholder="" name="building_address_num_hyphen" id="building_address_num_hyphen">
                        </div>

                        <div class="input-group mb-3 gx-0">
                            <span class="input-group-text">第</span>
                            <input type="text" class="form-control" placeholder="" name="building_address_floor" id="building_address_floor">
                            <span class="input-group-text">層之</span>
                            <input type="text" class="form-control" placeholder="" name="building_address_floor_sub" id="building_address_floor_sub">
                        </div>
                        <div class="input-group mb-3 gx-0">
                            <input type="text" class="form-control" placeholder="" name="building_address_room_num" id="building_address_room_num">
                            <span class="input-group-text">室/房號，共</span>
                            <input type="text" class="form-control" placeholder="" name="building_address_building_floor" id="building_address_building_floor">
                            <span class="input-group-text">層</span>
                        </div>
                        <div class="input-group mb-3 gx-0">
                            <input type="text" class="form-control" style="flex-grow: 3" placeholder="補充說明 Ex：承租整樓" name="building_address_building_desc" id="building_address_building_desc">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="building_located_city" class="col-md-12 col-form-label text-danger fw-bolder">*建物坐落</label>
                    <div class="col-md-8 col-12 col-xl-6 row">
                        <div class="input-group mb-3 gx-0">
                            <select class="form-select" name="building_located_city" id="building_located_city">
                                <option value="">請選擇</option>
                            </select>
                            <select class="form-select" name="building_located_city_area" id="building_located_city_area">
                                <option value="">請選擇</option>
                            </select>
                            <select class="form-select form-control selectpicker" id="building_located_lot" name="building_located_lot" data-live-search="true" data-size="5" data-style="border text-dark bg-white" data-width="10em">
                                <option value="">請選擇</option>
                            </select>
                        </div>
                        <div class="input-group mb-3 gx-0">
                            <input type="text" class="form-control" placeholder="" name="building_located_land_num" id="building_located_land_num">
                            <span class="input-group-text">地號</span>
                            <span class="input-group-text" style="color:#17a2b8;">※若有多個地號，請以半形逗號隔開</span>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label for="building_num" class="col-md-12 col-form-label text-danger fw-bolder">*建號</label>
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="" name="building_num" id="building_num">
                                <span class="input-group-text" style="color:#17a2b8;">※若無，請填寫房屋稅籍編號</span>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label for="whole_floor_for_rent" class="col-md-12 col-form-label text-danger fw-bolder">*出租類型</label>
                        <div class="col-md-12">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="whole_floor_for_rent" value="option1" checked>
                                <label class="form-check-label" for="whole_floor_for_rent">整層出租</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="partial_rental" value="option2">
                                <label class="form-check-label" for="partial_rental">部分出租</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_pattren" class="col-md-12 col-form-label text-danger fw-bolder">*格局
                        </label>
                        <div class="col-md-12">
                            <select class="form-select" name="building_pattren" id="building_pattren">
                                <option value="">請選擇</option>
                                <option value="套房">套房</option>
                                <option value="雅房">雅房</option>
                                <option value="1房">1房</option>
                                <option value="2房">2房</option>
                                <option value="3房">3房</option>
                                <option value="4房以上">4房以上</option>
                                <option value="其他">其他</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label for="building_type" class="col-md-12 col-form-label text-danger fw-bolder">*房屋類型</label>
                        <div class="col-md-12">
                            <select class="form-select" id="building_type" name="building_type">
                                <option value="">請選擇</option>
                                <option value="公寓">公寓</option>
                                <option value="電梯大樓">電梯大樓</option>
                                <option value="透天厝">透天厝</option>
                                <option value="平房">平房</option>
                                <option value="華廈">華廈</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_age" class="col-md-12 col-form-label text-danger fw-bolder">*屋齡</label>
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="" name="building_age" id="building_age">
                                <span class="input-group-text">年</span>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_complete_date" class="col-md-12 col-form-label fw-bolder">建築完成日期</label>
                        <div class="col-md-12">
                            <div class="input-group date day">
                                <input type="text" name="building_complete_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="building_complete_date" id="building_complete_date" aria-label="民國年" aria-describedby="button-addon1">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_rooms" class="col-md-12 col-form-label text-danger fw-bolder">*格間</label>
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="" name="building_rooms" id="building_rooms">
                                <span class="input-group-text">房</span>
                                <input type="text" class="form-control" placeholder="" name="building_livingrooms" id="building_livingrooms">
                                <span class="input-group-text">廳</span>
                                <input type="text" class="form-control" placeholder="" name="building_bathrooms" id="building_bathrooms">
                                <span class="input-group-text">衛</span>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_compartment_material" class="col-md-12 col-form-label fw-bolder">隔間材質</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="building_compartment_material" id="building_compartment_material">
                        </div>
                    </div>
                </div>
                <div class="row">
                    {{-- react元件MeterToPingInput --}}
                    <div class="row col-md-6 MeterToPingInput" data-param="{{ isset($buildingManagementNew) ? $buildingManagementNew : '' }}" elementId="building_total_square_meter" title="*權狀坪數"></div>
                    <div class="row col-md-6 MeterToPingInput" data-param="{{ isset($buildingManagementNew) ? $buildingManagementNew : '' }}" elementId="building_use_square_meter" title="*實際使用坪數"></div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_use" class="col-md-12 col-form-label fw-bolder text-nowrap">建物主要用途</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="building_use" id="building_use">
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label for="building_materials" class="col-md-12 col-form-label fw-bolder">建材</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="building_materials" id="building_materials">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_rent" class="col-md-12 col-form-label  fw-bolder text-danger text-nowrap">*房東期待租金</label>
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="" name="building_rent" id="building_rent" value="0">
                                <span class="input-group-text">元</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-12 align-items-center">
                        <label for="building_deposit_amount_month" class="col-md-12 col-form-label  fw-bolder text-danger">*押金</label>
                        <div class="row col-md-12">
                            <div class="input-group">
                                <input type="number" class="form-control" value="0" placeholder="" name="building_deposit_amount_month" id="building_deposit_amount_month">
                                <span class="input-group-text">個月(以每月租金金額計算) 或</span>
                                <input type="text" class="form-control" value="0" placeholder="" name="building_deposit_amount" id="building_deposit_amount">
                                <span class="input-group-text">元</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="can_bargain" class="col-md-12 col-form-label  fw-bolder text-nowrap">是否可議價</label>
                        <div class="col-md-12">
                            <select class="form-select" name="can_bargain" id="can_bargain">
                                <option value="">請選擇</option>
                                <option value="1">否</option>
                                <option value="2">可</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="has_management_fee" class="col-md-12 col-form-label  fw-bolder text-danger">*管理費</label>
                        <div class="row col-md-12">
                            <div class="input-group">
                                <select class="form-select" name="has_management_fee" id="has_management_fee">
                                    <option value="">請選擇</option>
                                    <option value="1">包含</option>
                                    <option value="2">不包含</option>
                                </select>
                                <span class="input-group-text">每月另付：</span>
                                <input type="text" class="form-control" value="0" placeholder="" name="management_fee_month" id="management_fee_month">
                                <span class="input-group-text">元/每坪</span>
                                <input type="text" class="form-control" placeholder="" name="management_fee_square_meter" value="0" id="management_fee_square_meter">
                                <span class="input-group-text">元</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_is_including_electricity_fees" class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger">*是否包含電費</label>
                        <div class="col-md-12">
                            <select class="form-select" id="building_is_including_electricity_fees" name="building_is_including_electricity_fees">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_is_cooking" class="col-md-12 col-form-label  fw-bolder text-nowrap">是否可炊煮</label>
                        <div class="col-md-12">
                            <select class="form-select" id="building_is_cooking" name="building_is_cooking">
                                <option value="">請選擇</option>
                                <option value="1">否</option>
                                <option value="2">可</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_is_including_water_fees" class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger">*是否包含水費</label>
                        <div class="col-md-12">
                            <select class="form-select" id="building_is_including_water_fees" name="building_is_including_water_fees">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_is_including_parking_space" class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger">*是否包含車位</label>
                        <div class="col-md-12">
                            <select class="form-select" name="building_is_including_parking_space" id="building_is_including_parking_space">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_is_including_accessible_equipment" class="col-md-12 col-form-label  fw-bolder text-nowrap">是否有無障礙設施</label>
                        <div class="col-md-12">
                            <select class="form-select" name="building_is_including_accessible_equipment" id="building_is_including_accessible_equipment">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_is_including_natural_gas" class="col-md-12 col-form-label  fw-bolder text-nowrap">是否包含瓦斯</label>
                        <div class="col-md-12">
                            <select class="form-select" name="building_is_including_natural_gas" id="building_is_including_natural_gas">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_is_including_cable" class="col-md-12 col-form-label  fw-bolder text-nowrap">是否包含第四台</label>
                        <div class="col-md-12">
                            <select class="form-select" name="building_is_including_cable" id="building_is_including_cable">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_is_including_internet" class="col-md-12 col-form-label  fw-bolder text-nowrap">是否包含網路</label>
                        <div class="col-md-12">
                            <select class="form-select" name="building_is_including_internet" id="building_is_including_internet">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_is_including_cleaning_fee" class="col-md-12 col-form-label  fw-bolder text-nowrap">是否包含清潔費</label>
                        <div class="col-md-12">
                            <select class="form-select" name="building_is_including_cleaning_fee" id="building_is_including_cleaning_fee">
                                <option value="">請選擇</option>
                                <option value="1">包含</option>
                                <option value="2">不包含</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-12 align-items-center">
                        <label for="test" class="col-md-12 col-form-label  fw-bolder text-nowrap">提供設備</label>
                        <div class="col-md-12 align-items-center">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_tv" id="building_offer_tv" value="option2">
                                <label class="form-check-label" for="building_offer_tv">電視</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_refrigerator" id="building_offer_refrigerator" value="option2">
                                <label class="form-check-label" for="building_offer_refrigerator">冰箱</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_cable" id="building_offer_cable" value="option2">
                                <label class="form-check-label" for="building_offer_cable">有線電視(第四臺)</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_air_conditioner" id="building_offer_air_conditioner" value="option2">
                                <label class="form-check-label" for="building_offer_air_conditioner">冷氣</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_geyser" id="building_offer_geyser" value="option2">
                                <label class="form-check-label" for="building_offer_geyser">熱水器</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_internet" id="building_offer_internet" value="option2">
                                <label class="form-check-label" for="building_offer_internet">網際網路</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_washing_machine" id="building_offer_washing_machine" value="option2">
                                <label class="form-check-label" for="building_offer_washing_machine">洗衣機</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_natural_gas" id="building_offer_natural_gas" value="option2">
                                <label class="form-check-label" for="building_offer_natural_gas">瓦斯/天然瓦斯</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_bed" id="building_offer_bed" value="option2">
                                <label class="form-check-label" for="building_offer_bed">床</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_wardrobe" id="building_offer_wardrobe" value="option2">
                                <label class="form-check-label" for="building_offer_wardrobe">衣櫃</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_desk" id="building_offer_desk" value="option2">
                                <label class="form-check-label" for="building_offer_desk">桌</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_chair" id="building_offer_chair" value="option2">
                                <label class="form-check-label" for="building_offer_chair">椅子</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_sofa" id="building_offer_sofa" value="option2">
                                <label class="form-check-label" for="building_offer_sofa">沙發</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_other" id="building_offer_other" value="option2">
                                <label class="form-check-label" for="building_offer_other">其他</label>
                                <input class="checkbox-input" name="building_offer_other_desc" id="building_offer_other_desc" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 備註 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingEight">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseEight" aria-expanded="true" aria-controls="panelsStayOpen-collapseEight">
            備註
        </button>
    </h2>
    <div id="panelsStayOpen-collapseEight" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingEight">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-md-2 col-form-label  fw-bolder text-nowrap" for="note">備註</label>
                        <div class="col-md-10 align-items-center">
                            <textarea type="text" class="form-control" name="note" id="note"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if (isset($buildingManagementNew))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                檢附文件
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="input-700" name="source" type="file" multiple {{ app('request')->input('cantedit') ? "readonly='true'" : '' }} />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                照片資料
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="building_photo" name="source" type="file" multiple {{ app('request')->input('cantedit') ? "readonly='true'" : '' }} />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
