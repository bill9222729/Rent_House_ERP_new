<div class="row">
<!-- Building Address City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_city', __('models/buildingManagementNews.fields.building_address_city').':') !!}
    {!! Form::text('building_address_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address City Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_city_area', __('models/buildingManagementNews.fields.building_address_city_area').':') !!}
    {!! Form::text('building_address_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Street Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_street', __('models/buildingManagementNews.fields.building_address_street').':') !!}
    {!! Form::text('building_address_street', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Ln Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_ln', __('models/buildingManagementNews.fields.building_address_ln').':') !!}
    {!! Form::text('building_address_ln', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Aly Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_aly', __('models/buildingManagementNews.fields.building_address_aly').':') !!}
    {!! Form::text('building_address_aly', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_num', __('models/buildingManagementNews.fields.building_address_num').':') !!}
    {!! Form::text('building_address_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Num Hyphen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_num_hyphen', __('models/buildingManagementNews.fields.building_address_num_hyphen').':') !!}
    {!! Form::text('building_address_num_hyphen', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Floor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_floor', __('models/buildingManagementNews.fields.building_address_floor').':') !!}
    {!! Form::text('building_address_floor', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Floor Sub Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_floor_sub', __('models/buildingManagementNews.fields.building_address_floor_sub').':') !!}
    {!! Form::text('building_address_floor_sub', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Room Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_room_num', __('models/buildingManagementNews.fields.building_address_room_num').':') !!}
    {!! Form::text('building_address_room_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Building Floor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_building_floor', __('models/buildingManagementNews.fields.building_address_building_floor').':') !!}
    {!! Form::text('building_address_building_floor', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Address Building Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_address_building_desc', __('models/buildingManagementNews.fields.building_address_building_desc').':') !!}
    {!! Form::text('building_address_building_desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Located City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_located_city', __('models/buildingManagementNews.fields.building_located_city').':') !!}
    {!! Form::text('building_located_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Located City Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_located_city_area', __('models/buildingManagementNews.fields.building_located_city_area').':') !!}
    {!! Form::text('building_located_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Located Lot Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_located_lot', __('models/buildingManagementNews.fields.building_located_lot').':') !!}
    {!! Form::text('building_located_lot', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Located Land Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_located_land_num', __('models/buildingManagementNews.fields.building_located_land_num').':') !!}
    {!! Form::text('building_located_land_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_num', __('models/buildingManagementNews.fields.building_num').':') !!}
    {!! Form::text('building_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Rent Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_rent_type', __('models/buildingManagementNews.fields.building_rent_type').':') !!}
    {!! Form::text('building_rent_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Pattren Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_pattren', __('models/buildingManagementNews.fields.building_pattren').':') !!}
    {!! Form::text('building_pattren', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_type', __('models/buildingManagementNews.fields.building_type').':') !!}
    {!! Form::text('building_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Age Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_age', __('models/buildingManagementNews.fields.building_age').':') !!}
    {!! Form::text('building_age', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Complete Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_complete_date', __('models/buildingManagementNews.fields.building_complete_date').':') !!}
    {!! Form::text('building_complete_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Rooms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_rooms', __('models/buildingManagementNews.fields.building_rooms').':') !!}
    {!! Form::text('building_rooms', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Livingrooms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_livingrooms', __('models/buildingManagementNews.fields.building_livingrooms').':') !!}
    {!! Form::text('building_livingrooms', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Bathrooms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_bathrooms', __('models/buildingManagementNews.fields.building_bathrooms').':') !!}
    {!! Form::text('building_bathrooms', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Compartment Material Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_compartment_material', __('models/buildingManagementNews.fields.building_compartment_material').':') !!}
    {!! Form::text('building_compartment_material', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Total Square Meter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_total_square_meter', __('models/buildingManagementNews.fields.building_total_square_meter').':') !!}
    {!! Form::text('building_total_square_meter', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Use Square Meter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_use_square_meter', __('models/buildingManagementNews.fields.building_use_square_meter').':') !!}
    {!! Form::text('building_use_square_meter', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Materials Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_materials', __('models/buildingManagementNews.fields.building_materials').':') !!}
    {!! Form::text('building_materials', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Rent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_rent', __('models/buildingManagementNews.fields.building_rent').':') !!}
    {!! Form::text('building_rent', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Deposit Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_deposit_amount', __('models/buildingManagementNews.fields.building_deposit_amount').':') !!}
    {!! Form::text('building_deposit_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Electricity Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_electricity_fees', __('models/buildingManagementNews.fields.building_is_including_electricity_fees').':') !!}
    {!! Form::text('building_is_including_electricity_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Cooking Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_cooking', __('models/buildingManagementNews.fields.building_is_cooking').':') !!}
    {!! Form::text('building_is_cooking', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Water Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_water_fees', __('models/buildingManagementNews.fields.building_is_including_water_fees').':') !!}
    {!! Form::text('building_is_including_water_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Parking Space Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_parking_space', __('models/buildingManagementNews.fields.building_is_including_parking_space').':') !!}
    {!! Form::text('building_is_including_parking_space', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Accessible Equipment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_accessible_equipment', __('models/buildingManagementNews.fields.building_is_including_accessible_equipment').':') !!}
    {!! Form::text('building_is_including_accessible_equipment', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Natural Gas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_natural_gas', __('models/buildingManagementNews.fields.building_is_including_natural_gas').':') !!}
    {!! Form::text('building_is_including_natural_gas', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Cable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_cable', __('models/buildingManagementNews.fields.building_is_including_cable').':') !!}
    {!! Form::text('building_is_including_cable', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Internet Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_internet', __('models/buildingManagementNews.fields.building_is_including_internet').':') !!}
    {!! Form::text('building_is_including_internet', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Is Including Cleaning Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_is_including_cleaning_fee', __('models/buildingManagementNews.fields.building_is_including_cleaning_fee').':') !!}
    {!! Form::text('building_is_including_cleaning_fee', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Tv Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_tv', __('models/buildingManagementNews.fields.building_offer_tv').':') !!}
    {!! Form::text('building_offer_tv', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Refrigerator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_refrigerator', __('models/buildingManagementNews.fields.building_offer_refrigerator').':') !!}
    {!! Form::text('building_offer_refrigerator', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Cable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_cable', __('models/buildingManagementNews.fields.building_offer_cable').':') !!}
    {!! Form::text('building_offer_cable', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Air Conditioner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_air_conditioner', __('models/buildingManagementNews.fields.building_offer_air_conditioner').':') !!}
    {!! Form::text('building_offer_air_conditioner', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Geyser Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_geyser', __('models/buildingManagementNews.fields.building_offer_geyser').':') !!}
    {!! Form::text('building_offer_geyser', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Internet Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_internet', __('models/buildingManagementNews.fields.building_offer_internet').':') !!}
    {!! Form::text('building_offer_internet', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Washing Machine Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_washing_machine', __('models/buildingManagementNews.fields.building_offer_washing_machine').':') !!}
    {!! Form::text('building_offer_washing_machine', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Natural Gas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_natural_gas', __('models/buildingManagementNews.fields.building_offer_natural_gas').':') !!}
    {!! Form::text('building_offer_natural_gas', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Bed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_bed', __('models/buildingManagementNews.fields.building_offer_bed').':') !!}
    {!! Form::text('building_offer_bed', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Wardrobe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_wardrobe', __('models/buildingManagementNews.fields.building_offer_wardrobe').':') !!}
    {!! Form::text('building_offer_wardrobe', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Desk Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_desk', __('models/buildingManagementNews.fields.building_offer_desk').':') !!}
    {!! Form::text('building_offer_desk', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Chair Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_chair', __('models/buildingManagementNews.fields.building_offer_chair').':') !!}
    {!! Form::text('building_offer_chair', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Sofa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_sofa', __('models/buildingManagementNews.fields.building_offer_sofa').':') !!}
    {!! Form::text('building_offer_sofa', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Offer Other Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_offer_other', __('models/buildingManagementNews.fields.building_offer_other').':') !!}
    {!! Form::text('building_offer_other', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Use Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_use', __('models/buildingManagementNews.fields.building_use').':') !!}
    {!! Form::text('building_use', null, ['class' => 'form-control']) !!}
</div>

<!-- Apply Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apply_date', __('models/buildingManagementNews.fields.apply_date').':') !!}
    {!! Form::date('apply_date', null, ['class' => 'form-control','id'=>'apply_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#apply_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Case No Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('case_no', __('models/buildingManagementNews.fields.case_no').':') !!}
    {!! Form::textarea('case_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Type Chartering Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_type_chartering', __('models/buildingManagementNews.fields.case_type_chartering').':') !!}
    {!! Form::number('case_type_chartering', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Type Escrow Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_type_escrow', __('models/buildingManagementNews.fields.case_type_escrow').':') !!}
    {!! Form::number('case_type_escrow', null, ['class' => 'form-control']) !!}
</div>

<!-- Case State Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('case_state', __('models/buildingManagementNews.fields.case_state').':') !!}
    {!! Form::textarea('case_state', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name', __('models/buildingManagementNews.fields.name').':') !!}
    {!! Form::textarea('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', __('models/buildingManagementNews.fields.gender').':') !!}
    {!! Form::number('gender', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', __('models/buildingManagementNews.fields.birthday').':') !!}
    {!! Form::date('birthday', null, ['class' => 'form-control','id'=>'birthday']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#birthday').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Is Foreigner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_foreigner', __('models/buildingManagementNews.fields.is_foreigner').':') !!}
    {!! Form::number('is_foreigner', null, ['class' => 'form-control']) !!}
</div>

<!-- Id No Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('id_no', __('models/buildingManagementNews.fields.id_no').':') !!}
    {!! Form::textarea('id_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Passport Code Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('passport_code', __('models/buildingManagementNews.fields.passport_code').':') !!}
    {!! Form::textarea('passport_code', null, ['class' => 'form-control']) !!}
</div>

<!-- House No Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('house_no', __('models/buildingManagementNews.fields.house_no').':') !!}
    {!! Form::textarea('house_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Tal Day Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tal_day', __('models/buildingManagementNews.fields.tal_day').':') !!}
    {!! Form::textarea('tal_day', null, ['class' => 'form-control']) !!}
</div>

<!-- Tal Night Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tal_night', __('models/buildingManagementNews.fields.tal_night').':') !!}
    {!! Form::textarea('tal_night', null, ['class' => 'form-control']) !!}
</div>

<!-- Cellphone Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('cellphone', __('models/buildingManagementNews.fields.cellphone').':') !!}
    {!! Form::textarea('cellphone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/buildingManagementNews.fields.email').':') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence City Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('residence_city', __('models/buildingManagementNews.fields.residence_city').':') !!}
    {!! Form::textarea('residence_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence City Area Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('residence_city_area', __('models/buildingManagementNews.fields.residence_city_area').':') !!}
    {!! Form::textarea('residence_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence Addr Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('residence_addr', __('models/buildingManagementNews.fields.residence_addr').':') !!}
    {!! Form::textarea('residence_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing City Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('mailing_city', __('models/buildingManagementNews.fields.mailing_city').':') !!}
    {!! Form::textarea('mailing_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing City Area Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('mailing_city_area', __('models/buildingManagementNews.fields.mailing_city_area').':') !!}
    {!! Form::textarea('mailing_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing Addr Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('mailing_addr', __('models/buildingManagementNews.fields.mailing_addr').':') !!}
    {!! Form::textarea('mailing_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Legal Agent Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('legal_agent_num', __('models/buildingManagementNews.fields.legal_agent_num').':') !!}
    {!! Form::number('legal_agent_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Single Proxy Reason Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('single_proxy_reason', __('models/buildingManagementNews.fields.single_proxy_reason').':') !!}
    {!! Form::textarea('single_proxy_reason', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Name Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('first_legal_agent_name', __('models/buildingManagementNews.fields.first_legal_agent_name').':') !!}
    {!! Form::textarea('first_legal_agent_name', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Tel Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('first_legal_agent_tel', __('models/buildingManagementNews.fields.first_legal_agent_tel').':') !!}
    {!! Form::textarea('first_legal_agent_tel', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Phone Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('first_legal_agent_phone', __('models/buildingManagementNews.fields.first_legal_agent_phone').':') !!}
    {!! Form::textarea('first_legal_agent_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Addr Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('first_legal_agent_addr', __('models/buildingManagementNews.fields.first_legal_agent_addr').':') !!}
    {!! Form::textarea('first_legal_agent_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Name Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('second_legal_agent_name', __('models/buildingManagementNews.fields.second_legal_agent_name').':') !!}
    {!! Form::textarea('second_legal_agent_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Tel Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('second_legal_agent_tel', __('models/buildingManagementNews.fields.second_legal_agent_tel').':') !!}
    {!! Form::textarea('second_legal_agent_tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Phone Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('second_legal_agent_phone', __('models/buildingManagementNews.fields.second_legal_agent_phone').':') !!}
    {!! Form::textarea('second_legal_agent_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Addr Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('second_legal_agent_addr', __('models/buildingManagementNews.fields.second_legal_agent_addr').':') !!}
    {!! Form::textarea('second_legal_agent_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Name Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bank_name', __('models/buildingManagementNews.fields.bank_name').':') !!}
    {!! Form::textarea('bank_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Branch Code Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bank_branch_code', __('models/buildingManagementNews.fields.bank_branch_code').':') !!}
    {!! Form::textarea('bank_branch_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Owner Name Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bank_owner_name', __('models/buildingManagementNews.fields.bank_owner_name').':') !!}
    {!! Form::textarea('bank_owner_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Account Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bank_account', __('models/buildingManagementNews.fields.bank_account').':') !!}
    {!! Form::textarea('bank_account', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
