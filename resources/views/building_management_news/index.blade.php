@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">房東管理</h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item search-accordion">
                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        查詢條件
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">
                        <div class="container">
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">申請日期</label>
                                        <div class="row col-sm-10">
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="apply_date_start" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="apply_date_start" aria-label="民國年" aria-describedby="button-addon1">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                            ~
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="apply_date_end" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="apply_date_end" aria-label="民國年" aria-describedby="button-addon2">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="contract_type">物件狀態</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="case_state">
                                                <option value="0" selected>請選擇</option>
                                                <option value="招租中">招租中</option>
                                                <option value="已媒合">已媒合</option>
                                                <option value="暫停">暫停</option>
                                                <option value="警示客戶">警示客戶</option>
                                                <option value="退出計畫">退出計畫</option>
                                                <option value="待追蹤">待追蹤</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="sales">業務</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="sales">
                                                <option value="0" selected>請選擇</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="case_type">包租/代管</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="case_type">
                                                <option value="0" selected>請選擇</option>
                                                <option value="包租">包租</option>
                                                <option value="代管">代管</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="case_no" class="col-sm-2 col-form-label">物件編號</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="case_no" style="text-transform:uppercase">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label for="government_no" class="col-sm-2 col-form-label text-nowrap">內政部編號</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="government_no" style="text-transform:uppercase">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="name" class="col-sm-2 col-form-label">房東姓名</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">地址</label>
                                        <div class="row col-sm-10" id="AddressSelector"></div>
                                        {{-- 這是一個react元件，寫在resources\js\components\AddressSelector.js --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">委託租賃</label>
                                        <div class="row col-sm-10 form-inline">
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="entrusted_lease_start_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="entrusted_lease_start_date" aria-label="民國年" aria-describedby="button-addon3">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon3">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                            ~
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="entrusted_lease_end_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="entrusted_lease_end_date" aria-label="民國年" aria-describedby="button-addon4">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon4">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">委託管理</label>
                                        <div class="row col-sm-10 form-inline">
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="entrusted_management_start_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="entrusted_management_start_date" aria-label="民國年" aria-describedby="button-addon5">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon5">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                            ~
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="entrusted_management_end_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="entrusted_management_end_date" aria-label="民國年" aria-describedby="button-addon6">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon6">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="col-sm-9 col-md-10">
                                <button class="btn btn-primary" type="button" id="search">
                                    查詢
                                </button>
                                <button class="btn btn-outline-secondary" type="reset" id="reset" onclick="resetCheckBox();">
                                    清除條件
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                        查詢結果
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                    <div class="accordion-body">
                        <div class="container-fluid">
                            <table id="example2" class="display nowrap" style="width:100%"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        $.fn.dataTable.ext.search.push(
            // 申請日期
            function(settings, data, dataIndex) {
                let apply_date_start = $("#apply_date_start").val();
                let apply_date_end = $("#apply_date_end").val();
                let apply_date = data[0];
                if (checkInDate(apply_date_start, apply_date_end, apply_date, apply_date)) {
                    return true;
                }
                return false;
            },
            // 物件狀態
            function(settings, data, dataIndex) {
                let case_state = $("#case_state").val();
                let case_state_data = data[1];
                if (case_state === case_state_data || case_state === "0") {
                    return true;
                }
                return false;
            },
            // 業務
            function(settings, data, dataIndex) {
                let sales = $("#sales").val();
                let sales_data = data[2];
                if (sales == sales_data || sales === "0") {
                    return true;
                }
                return false;
            },
            // 包租/代管
            function(settings, data, dataIndex) {
                let case_type = $("#case_type").val();
                let case_type_data = data[3];
                if (case_type_data.includes(case_type) || case_type === "0") {
                    return true;
                }
                return false;
            },
            // 物件編號
            function(settings, data, dataIndex) {
                let case_no = $("#case_no").val();
                let case_no_data = data[4];
                if (case_no_data.includes(case_no) || case_no === "") {
                    return true;
                }
                return false;
            },
            // 內政部編號
            function(settings, data, dataIndex) {
                let government_no = $("#government_no").val();
                let government_no_data = data[5];
                if (government_no_data.includes(government_no) || government_no === "") {
                    return true;
                }
                return false;
            },
            // 房東姓名
            function(settings, data, dataIndex) {
                let name = $("#name").val();
                let name_data = data[6];
                if (name_data.includes(name) || name === "") {
                    return true;
                }
                return false;
            },
            // 地址
            function(settings, data, dataIndex) {
                let cityName = $("#address_city").val();
                let cityAreaName = $("#address_area").val();
                let addrData = name_data = data[7];
                if (addrData.includes(cityName) && cityAreaName === "0") {
                    return true;
                }
                if (addrData.includes(cityName) && addrData.includes(cityAreaName)) {
                    return true;
                }
                if (cityName === "0" && cityAreaName === "0") {
                    return true;
                }
                return false;
            },
            // 委託租賃
            function(settings, data, dataIndex) {
                let entrusted_lease_start = $("#entrusted_lease_start_date").val();
                let entrusted_lease_end = $("#entrusted_lease_end_date").val();
                let entrusted_lease_start_data = data[8] ? data[8].split("~")[0] : "";
                let entrusted_lease_end_data = data[8] ? data[8].split("~")[1] : "";
                if (checkInDate(
                        entrusted_lease_start,
                        entrusted_lease_end,
                        entrusted_lease_start_data,
                        entrusted_lease_end_data)) {
                    return true;
                }
                return false;
            }
        );
        async function deleteBuildingData(building_id) {
            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    $.ajax({
                        type: 'GET',
                        url: `/api/match_managements?building_case_id=${building_id}`,
                        success: function(data) {
                            if (data.data.length > 0) {
                                Swal.fire({
                                    title: '無法刪除',
                                    text: `媒合資料${data.data[0].match_id}已經使用此物件，故無法刪除`,
                                    icon: 'warning',
                                    showCancelButton: true,
                                    showConfirmButton: true,
                                    cancelButtonText: '取消',
                                    confirmButtonText: '確定',
                                })
                            } else {
                                $.ajax({
                                    type: 'DELETE',
                                    url: `/api/building_management_news/${building_id}`,
                                    success: function(data) {
                                        $('#example2').DataTable().ajax.reload();
                                        Swal.fire('資料已被刪除', '', 'success').then((
                                            result) => {
                                            $('#example2').DataTable().ajax
                                                .reload();
                                        })
                                    }
                                });
                            }
                        }
                    });
                }
            })
        }
        $(document).ready(async function() {
            let salesmanData = await getSalesmanData();
            // 設定搜尋部份的業務選項
            salesmanData.forEach((item) => {
                $("#sales").append($("<option></option>").attr("value", item.name).text(item.name));
            });
            // 初始化日期選擇器
            $('.input-group').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });
            // 初始化datatables
            const table = $('#example2').DataTable({
                "pageLength": 20,
                "order": [
                    [10, 'desc']
                ],
                "responsive": true,
                // "searching": false,
                // scrollX: true,
                "dom": 'Bfrtip',
                "buttons": [{
                    text: '新增物件-自然人',
                    action: function(e, dt, node, config) {
                        window.location.href =
                            `/buildingManagementNews/create?type=NaturalPerson`
                    }
                }, {
                    text: '新增物件-法人',
                    action: function(e, dt, node, config) {
                        window.location.href =
                            `/buildingManagementNews/create?type=LegalPerson`
                    }
                }],
                "ajax": {
                    type: 'GET',
                    url: `/api/building_management_news`,
                    dataSrc: function(data) {
                        console.log(data.data);
                        data = data.data.map((value, index, array) => {
                            let building_address_num_hyphen =
                                value['building_address_num_hyphen'] ?
                                `之${value['building_address_num_hyphen']}` :
                                "";
                            let building_address_num =
                                value['building_address_num'] ?
                                `${value['building_address_num']}${building_address_num_hyphen}號` :
                                "";
                            let building_address_ln =
                                value['building_address_ln'] ?
                                `${value['building_address_ln']}巷` :
                                "";
                            let building_address_aly =
                                value['building_address_aly'] ?
                                `${value['building_address_aly']}弄` :
                                "";
                            let building_address_floor =
                                value['building_address_floor'] ?
                                `${value['building_address_floor']}樓` :
                                "";
                            let building_address_floor_sub =
                                value['building_address_floor_sub'] ?
                                `之${value['building_address_floor_sub']}` :
                                "";
                            let building_address_room_num =
                                value['building_address_room_num'] ?
                                `${value['building_address_room_num']}室` :
                                "";
                            let entrusted_lease_start =
                                value['entrusted_lease_date'] ?
                                convertToRCDate(value['entrusted_lease_date'].split("~")[0]) : "";
                            let entrusted_lease_end =
                                value['entrusted_lease_date'] ?
                                convertToRCDate(value['entrusted_lease_date'].split("~")[1]) : "";
                            let entrusted_management_start =
                                value['entrusted_management_date'] ?
                                convertToRCDate(value['entrusted_management_date'].split("~")[0]) : "";
                            let entrusted_management_end =
                                value['entrusted_management_date'] ?
                                convertToRCDate(value['entrusted_management_date'].split("~")[1]) : "";
                            value['apply_date'] = value['apply_date'] ? convertToRCDate(value['apply_date']) : "";
                            value['entrusted_lease_date'] =
                                value['entrusted_lease_date'] ?
                                `${entrusted_lease_start} ~ ${entrusted_lease_end}` :
                                "";
                            value['entrusted_management_date'] =
                                value['entrusted_management_date'] ?
                                `${entrusted_management_start} ~ ${entrusted_management_end}` :
                                "";
                            value['building_address'] = `
                                ${value['building_address_city']?value['building_address_city']:""}
                                ${value['building_address_city_area_name']?value['building_address_city_area_name']:""}
                                ${value['building_address_street']?value['building_address_street']:""}
                                ${building_address_ln}
                                ${building_address_aly}
                                ${building_address_num}
                                ${building_address_floor}
                                ${building_address_floor_sub}
                                ${building_address_room_num}
                            `;
                            value['building_pattren_and_age'] = `
                                ${value['building_pattren']?value['building_pattren']:"-"}<br>
                                ${value['building_age']?value['building_age']:"-"}
                            `;
                            value['case_type'] = `
                                ${value['case_type_chartering']?"包租":""}
                                ${value['case_type_escrow']?"代管":""}
                            `;
                            return value;
                        });

                        // 如果登入的使用者是業務就只會看到他的資料
                        @can('sales')
                            data = data.filter((item) => {
                                return item.sales_id === {{ Auth::user()->id }}
                            });
                        @endcan
                        return data;
                    }
                },
                "columns": [{
                        data: 'apply_date',
                        title: "申請日期",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'case_state',
                        title: "物件狀態",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'sales_name',
                        title: "業務",
                        render: function(data, type, row) {
                            return row.sales_name
                        },
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'case_type',
                        title: "包租/代管",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'case_no',
                        title: '物件編號',
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'government_no',
                        title: '內政部編號',
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'name',
                        title: '房東姓名',
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'building_address',
                        title: "房屋地址",
                        className: 'dt-head-center',
                    },
                    {
                        data: 'entrusted_lease_date',
                        title: '委託租賃',
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'entrusted_management_date',
                        title: '委託管理',
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'created_at',
                        title: '資料建立時間(隱藏)',
                        className: 'dt-body-center dt-head-center',
                        visible: false,
                    },
                    {
                        data: 'creator_name',
                        title: '建檔人',
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            let buttonHtmlContent = '';
                            @can('sales')
                                if (data.data_status !== "staff" && data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='buildingManagementNews/${data.id}/edit'">編輯</button >`;
                                }
                                buttonHtmlContent += `<button type="button" class="btn btn-primary btn-sm mx-1" onclick="location.href='buildingManagementNews/${data.id}/edit?cantedit=true'">檢視</button >`;
                                if (data.data_status !== "staff" && data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteBuildingData(${data.id});">刪除</button >`;
                                }
                            @elsecan("staff")
                                if (data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='buildingManagementNews/${data.id}/edit'">編輯</button >`;
                                }
                                buttonHtmlContent += `<button type="button" class="btn btn-primary btn-sm mx-1" onclick="location.href='buildingManagementNews/${data.id}/edit?cantedit=true'">檢視</button >`;
                                if (data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteBuildingData(${data.id});">刪除</button >`;
                                }
                            @elsecan("admin")
                                buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='buildingManagementNews/${data.id}/edit'">編輯</button >` +
                                    `<button type="button" class="btn btn-primary btn-sm mx-1" onclick="location.href='buildingManagementNews/${data.id}/edit?cantedit=true'">檢視</button >` +
                                    `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteBuildingData(${data.id});">刪除</button >`
                            @endcan

                            return buttonHtmlContent;
                        },
                        className: 'dt-body-center dt-head-center',

                    }
                ],
                // responsive: {
                //     details: {
                //         type: 'column'
                //     }
                // },
                // columnDefs: [{
                //     className: 'dtr-control',
                //     orderable: false,
                //     targets: 0
                // }],
                // order: [1, 'asc'],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });

            $('#search').on('click', function() {
                table.draw();
            });
        });
    </script>
@endpush
