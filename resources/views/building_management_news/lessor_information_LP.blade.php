<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
            出租人基本資料
        </button>
    </h2>
    <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
        <div class="accordion-body">
            @if (isset($buildingManagementNew))
                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#natural_person">變更出租人-自然人</button>
                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#legal_person">變更出租人-法人</button>
                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#change_history">變更歷程</button>
            @endif
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger" for="name">*公司名稱</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger" for="representative">*代表人</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="representative" id="representative">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger" for="tax_number">*統一編號</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="tax_number" id="tax_number">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger" for="tel_day">*公司電話</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="tel_day" id="tel_day">
                        </div>
                    </div>
                </div>
                <div class="row mb-3 CitySelector" data-param='{"title":"*法人通訊地址","required":true,"city_select_id":"mailing_city","city_area_select_id":"mailing_city_area","addr_input_id":"mailing_addr", "page_data":{{ $buildingManagementNew ?? '{}' }}, "is_modal":{{ isset($isModal) ? 'true' : 'false' }}}'>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap" for="agent_name">代理人姓名</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="" id="agent_name">
                                <span class="input-group-text">(無代理人免填)</span>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap" for="agent_tel">聯絡方式</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group">
                                <span class="input-group-text">電話</span>
                                <input type="text" id="agent_tel" name="agent_tel" class="form-control" placeholder="">
                                <span class="input-group-text">手機</span>
                                <input type="text" id="agent_cellphone" name="agent_cellphone" class="form-control" placeholder="">
                            </div>
                            <div class="input-group">
                                <span class="input-group-text">地址</span>
                                <input type="text" id="agent_addr" name="agent_addr" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-12 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder" for="proxy_name_1">領款資訊</label>
                        <div class="row col-sm-10 bankSelector" id="bankSelector" data-param="{{ isset($buildingManagementNew) ? $buildingManagementNew : '[]' }}"></div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap" for="virtual_bank_account">虛擬帳號</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" id="virtual_bank_account">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
