<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
            出租人基本資料
        </button>
    </h2>
    <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
        <div class="accordion-body">
            @if (isset($buildingManagementNew))
                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#natural_person">變更出租人-自然人</button>
                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#legal_person">變更出租人-法人</button>
                <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#change_history">變更歷程</button>
            @endif
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger" for="name">*出租人姓名</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="gender" class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger">*性別</label>
                        <div class="col-md-12 align-items-center">
                            <select class="form-select form-control" name="gender" id="gender">
                                <option value="">請選擇</option>
                                <option value="0">男</option>
                                <option value="1">女</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger">*出生年月日</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date day">
                                <input type="text" name="birthday" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="birthday" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="id_no" class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger">*身分/居留證號</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <input class="form-check-input mt-0" type="checkbox" id="is_foreigner" name="is_foreigner">外籍
                                </div>
                                <input type="text" class="form-control" id="id_no" name="id_no" style="text-transform:capitalize">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <div class="col-md-12 col-form-label  fw-bolder text-nowrap">護照號碼</div>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="passport_code" id="passport_code" style="text-transform:uppercase">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap" for="house_no">戶口名簿戶號</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="house_no" id="house_no" style="text-transform:uppercase">
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap" for="tel_day">電話</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group">
                                <span class="input-group-text">(日)</span>
                                <input type="text" id="tel_day" name="tel_day" class="form-control" placeholder="">
                                <span class="input-group-text">(夜)</span>
                                <input type="text" id="tel_night" name="tel_night" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap text-danger" for="cellphone">*手機</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="cellphone" id="cellphone">
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap" for="email">電子信箱</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="email" id="email">
                        </div>
                    </div>
                </div>
                <div class="row mb-3 CitySelector" data-param='{"title":"*戶籍地址","required":true,"city_select_id":"residence_city","city_area_select_id":"residence_city_area","addr_input_id":"residence_addr", "page_data":{{ $buildingManagementNew ?? '[]' }}, "is_modal":{{ isset($isModal) ? 'true' : 'false' }}}'>
                </div>
                <div class="row mb-3 CitySelector" data-param='{"title":"*通訊地址","required":true,"city_select_id":"mailing_city","city_area_select_id":"mailing_city_area","addr_input_id":"mailing_addr", "page_data":{{ $buildingManagementNew ?? '[]' }}, "is_modal":{{ isset($isModal) ? 'true' : 'false' }}, "ditto":"true"}'>
                </div>
                <div class="legalAgentNum" data-param="{{ isset($buildingManagementNew) ? $buildingManagementNew : '{}' }}">
                </div>
                <div class="row mb-3">
                    <div class="row col-md-12 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder" for="proxy_name_1">領款資訊</label>
                        <div class="row col-sm-10 bankSelector" data-param="{{ isset($buildingManagementNew) ? $buildingManagementNew : '[]' }}"></div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label  fw-bolder text-nowrap" for="virtual_bank_account">虛擬帳號</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="virtual_bank_account" id="virtual_bank_account">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
