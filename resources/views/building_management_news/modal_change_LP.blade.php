<!-- Modal -->
<div class="modal fade" id="legal_person" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <input type="hidden" name="lessor_type" id="lessor_type" value="LegalPerson" />
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">變更出租人</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="accordion" id="accordionExample">
                    @include(
                        'building_management_news.lessor_information_LP',
                        ['isModal' => true]
                    )

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                變更原因
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse show" aria-labelledby="headingThree">
                            <div class="accordion-body">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="row col-md-6">
                                            <label for="change_reason"
                                                class="col-sm-4 col-form-label text-end fw-bolder text-nowrap text-danger">*變更原因</label>
                                            <div class="col-sm-6">
                                                <select class="form-select form-control" id="change_reason">
                                                    <option value="" selected>請選擇</option>
                                                    <option value="繼承">繼承</option>
                                                    <option value="買賣">買賣</option>
                                                    <option value="其他">其他</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row col-md-6">
                                            <label
                                                class="col-sm-4 col-form-label text-end fw-bolder text-nowrap text-danger">*事實發生日</label>
                                            <div class="col-sm-6">
                                                <div class="input-group date">
                                                    <input type="text" name="change_date" value=""
                                                        class="form-control" autocomplete="off" maxlength="10"
                                                        size="10" placeholder="YYY-MM-DD" id="change_date" aria-label="民國年"
                                                        aria-describedby="button-addon1">
                                                    <button class="btn btn-outline-secondary" type="button"
                                                        id="button-addon1">
                                                        <i class="fa fa-fw fa-calendar"
                                                            data-time-icon="fa fa-fw fa-clock-o"
                                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="row col-md-12">
                                            <label for="change_reason"
                                                class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">變更備註</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit">儲存</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">關閉</button>
            </div>
        </div>
    </div>
</div>
