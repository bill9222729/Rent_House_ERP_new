<!-- Building Address City Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_city', __('models/buildingManagementNews.fields.building_address_city').':') !!}
    <p>{{ $buildingManagementNew->building_address_city }}</p>
</div>


<!-- Building Address City Area Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_city_area', __('models/buildingManagementNews.fields.building_address_city_area').':') !!}
    <p>{{ $buildingManagementNew->building_address_city_area }}</p>
</div>


<!-- Building Address Street Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_street', __('models/buildingManagementNews.fields.building_address_street').':') !!}
    <p>{{ $buildingManagementNew->building_address_street }}</p>
</div>


<!-- Building Address Ln Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_ln', __('models/buildingManagementNews.fields.building_address_ln').':') !!}
    <p>{{ $buildingManagementNew->building_address_ln }}</p>
</div>


<!-- Building Address Aly Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_aly', __('models/buildingManagementNews.fields.building_address_aly').':') !!}
    <p>{{ $buildingManagementNew->building_address_aly }}</p>
</div>


<!-- Building Address Num Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_num', __('models/buildingManagementNews.fields.building_address_num').':') !!}
    <p>{{ $buildingManagementNew->building_address_num }}</p>
</div>


<!-- Building Address Num Hyphen Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_num_hyphen', __('models/buildingManagementNews.fields.building_address_num_hyphen').':') !!}
    <p>{{ $buildingManagementNew->building_address_num_hyphen }}</p>
</div>


<!-- Building Address Floor Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_floor', __('models/buildingManagementNews.fields.building_address_floor').':') !!}
    <p>{{ $buildingManagementNew->building_address_floor }}</p>
</div>


<!-- Building Address Floor Sub Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_floor_sub', __('models/buildingManagementNews.fields.building_address_floor_sub').':') !!}
    <p>{{ $buildingManagementNew->building_address_floor_sub }}</p>
</div>


<!-- Building Address Room Num Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_room_num', __('models/buildingManagementNews.fields.building_address_room_num').':') !!}
    <p>{{ $buildingManagementNew->building_address_room_num }}</p>
</div>


<!-- Building Address Building Floor Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_building_floor', __('models/buildingManagementNews.fields.building_address_building_floor').':') !!}
    <p>{{ $buildingManagementNew->building_address_building_floor }}</p>
</div>


<!-- Building Address Building Desc Field -->
<div class="form-group col-12">
    {!! Form::label('building_address_building_desc', __('models/buildingManagementNews.fields.building_address_building_desc').':') !!}
    <p>{{ $buildingManagementNew->building_address_building_desc }}</p>
</div>


<!-- Building Located City Field -->
<div class="form-group col-12">
    {!! Form::label('building_located_city', __('models/buildingManagementNews.fields.building_located_city').':') !!}
    <p>{{ $buildingManagementNew->building_located_city }}</p>
</div>


<!-- Building Located City Area Field -->
<div class="form-group col-12">
    {!! Form::label('building_located_city_area', __('models/buildingManagementNews.fields.building_located_city_area').':') !!}
    <p>{{ $buildingManagementNew->building_located_city_area }}</p>
</div>


<!-- Building Located Lot Field -->
<div class="form-group col-12">
    {!! Form::label('building_located_lot', __('models/buildingManagementNews.fields.building_located_lot').':') !!}
    <p>{{ $buildingManagementNew->building_located_lot }}</p>
</div>


<!-- Building Located Land Num Field -->
<div class="form-group col-12">
    {!! Form::label('building_located_land_num', __('models/buildingManagementNews.fields.building_located_land_num').':') !!}
    <p>{{ $buildingManagementNew->building_located_land_num }}</p>
</div>


<!-- Building Num Field -->
<div class="form-group col-12">
    {!! Form::label('building_num', __('models/buildingManagementNews.fields.building_num').':') !!}
    <p>{{ $buildingManagementNew->building_num }}</p>
</div>


<!-- Building Rent Type Field -->
<div class="form-group col-12">
    {!! Form::label('building_rent_type', __('models/buildingManagementNews.fields.building_rent_type').':') !!}
    <p>{{ $buildingManagementNew->building_rent_type }}</p>
</div>


<!-- Building Pattren Field -->
<div class="form-group col-12">
    {!! Form::label('building_pattren', __('models/buildingManagementNews.fields.building_pattren').':') !!}
    <p>{{ $buildingManagementNew->building_pattren }}</p>
</div>


<!-- Building Type Field -->
<div class="form-group col-12">
    {!! Form::label('building_type', __('models/buildingManagementNews.fields.building_type').':') !!}
    <p>{{ $buildingManagementNew->building_type }}</p>
</div>


<!-- Building Age Field -->
<div class="form-group col-12">
    {!! Form::label('building_age', __('models/buildingManagementNews.fields.building_age').':') !!}
    <p>{{ $buildingManagementNew->building_age }}</p>
</div>


<!-- Building Complete Date Field -->
<div class="form-group col-12">
    {!! Form::label('building_complete_date', __('models/buildingManagementNews.fields.building_complete_date').':') !!}
    <p>{{ $buildingManagementNew->building_complete_date }}</p>
</div>


<!-- Building Rooms Field -->
<div class="form-group col-12">
    {!! Form::label('building_rooms', __('models/buildingManagementNews.fields.building_rooms').':') !!}
    <p>{{ $buildingManagementNew->building_rooms }}</p>
</div>


<!-- Building Livingrooms Field -->
<div class="form-group col-12">
    {!! Form::label('building_livingrooms', __('models/buildingManagementNews.fields.building_livingrooms').':') !!}
    <p>{{ $buildingManagementNew->building_livingrooms }}</p>
</div>


<!-- Building Bathrooms Field -->
<div class="form-group col-12">
    {!! Form::label('building_bathrooms', __('models/buildingManagementNews.fields.building_bathrooms').':') !!}
    <p>{{ $buildingManagementNew->building_bathrooms }}</p>
</div>


<!-- Building Compartment Material Field -->
<div class="form-group col-12">
    {!! Form::label('building_compartment_material', __('models/buildingManagementNews.fields.building_compartment_material').':') !!}
    <p>{{ $buildingManagementNew->building_compartment_material }}</p>
</div>


<!-- Building Total Square Meter Field -->
<div class="form-group col-12">
    {!! Form::label('building_total_square_meter', __('models/buildingManagementNews.fields.building_total_square_meter').':') !!}
    <p>{{ $buildingManagementNew->building_total_square_meter }}</p>
</div>


<!-- Building Use Square Meter Field -->
<div class="form-group col-12">
    {!! Form::label('building_use_square_meter', __('models/buildingManagementNews.fields.building_use_square_meter').':') !!}
    <p>{{ $buildingManagementNew->building_use_square_meter }}</p>
</div>


<!-- Building Materials Field -->
<div class="form-group col-12">
    {!! Form::label('building_materials', __('models/buildingManagementNews.fields.building_materials').':') !!}
    <p>{{ $buildingManagementNew->building_materials }}</p>
</div>


<!-- Building Rent Field -->
<div class="form-group col-12">
    {!! Form::label('building_rent', __('models/buildingManagementNews.fields.building_rent').':') !!}
    <p>{{ $buildingManagementNew->building_rent }}</p>
</div>


<!-- Building Deposit Amount Field -->
<div class="form-group col-12">
    {!! Form::label('building_deposit_amount', __('models/buildingManagementNews.fields.building_deposit_amount').':') !!}
    <p>{{ $buildingManagementNew->building_deposit_amount }}</p>
</div>


<!-- Building Is Including Electricity Fees Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_electricity_fees', __('models/buildingManagementNews.fields.building_is_including_electricity_fees').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_electricity_fees }}</p>
</div>


<!-- Building Is Cooking Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_cooking', __('models/buildingManagementNews.fields.building_is_cooking').':') !!}
    <p>{{ $buildingManagementNew->building_is_cooking }}</p>
</div>


<!-- Building Is Including Water Fees Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_water_fees', __('models/buildingManagementNews.fields.building_is_including_water_fees').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_water_fees }}</p>
</div>


<!-- Building Is Including Parking Space Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_parking_space', __('models/buildingManagementNews.fields.building_is_including_parking_space').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_parking_space }}</p>
</div>


<!-- Building Is Including Accessible Equipment Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_accessible_equipment', __('models/buildingManagementNews.fields.building_is_including_accessible_equipment').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_accessible_equipment }}</p>
</div>


<!-- Building Is Including Natural Gas Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_natural_gas', __('models/buildingManagementNews.fields.building_is_including_natural_gas').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_natural_gas }}</p>
</div>


<!-- Building Is Including Cable Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_cable', __('models/buildingManagementNews.fields.building_is_including_cable').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_cable }}</p>
</div>


<!-- Building Is Including Internet Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_internet', __('models/buildingManagementNews.fields.building_is_including_internet').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_internet }}</p>
</div>


<!-- Building Is Including Cleaning Fee Field -->
<div class="form-group col-12">
    {!! Form::label('building_is_including_cleaning_fee', __('models/buildingManagementNews.fields.building_is_including_cleaning_fee').':') !!}
    <p>{{ $buildingManagementNew->building_is_including_cleaning_fee }}</p>
</div>


<!-- Building Offer Tv Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_tv', __('models/buildingManagementNews.fields.building_offer_tv').':') !!}
    <p>{{ $buildingManagementNew->building_offer_tv }}</p>
</div>


<!-- Building Offer Refrigerator Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_refrigerator', __('models/buildingManagementNews.fields.building_offer_refrigerator').':') !!}
    <p>{{ $buildingManagementNew->building_offer_refrigerator }}</p>
</div>


<!-- Building Offer Cable Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_cable', __('models/buildingManagementNews.fields.building_offer_cable').':') !!}
    <p>{{ $buildingManagementNew->building_offer_cable }}</p>
</div>


<!-- Building Offer Air Conditioner Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_air_conditioner', __('models/buildingManagementNews.fields.building_offer_air_conditioner').':') !!}
    <p>{{ $buildingManagementNew->building_offer_air_conditioner }}</p>
</div>


<!-- Building Offer Geyser Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_geyser', __('models/buildingManagementNews.fields.building_offer_geyser').':') !!}
    <p>{{ $buildingManagementNew->building_offer_geyser }}</p>
</div>


<!-- Building Offer Internet Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_internet', __('models/buildingManagementNews.fields.building_offer_internet').':') !!}
    <p>{{ $buildingManagementNew->building_offer_internet }}</p>
</div>


<!-- Building Offer Washing Machine Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_washing_machine', __('models/buildingManagementNews.fields.building_offer_washing_machine').':') !!}
    <p>{{ $buildingManagementNew->building_offer_washing_machine }}</p>
</div>


<!-- Building Offer Natural Gas Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_natural_gas', __('models/buildingManagementNews.fields.building_offer_natural_gas').':') !!}
    <p>{{ $buildingManagementNew->building_offer_natural_gas }}</p>
</div>


<!-- Building Offer Bed Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_bed', __('models/buildingManagementNews.fields.building_offer_bed').':') !!}
    <p>{{ $buildingManagementNew->building_offer_bed }}</p>
</div>


<!-- Building Offer Wardrobe Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_wardrobe', __('models/buildingManagementNews.fields.building_offer_wardrobe').':') !!}
    <p>{{ $buildingManagementNew->building_offer_wardrobe }}</p>
</div>


<!-- Building Offer Desk Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_desk', __('models/buildingManagementNews.fields.building_offer_desk').':') !!}
    <p>{{ $buildingManagementNew->building_offer_desk }}</p>
</div>


<!-- Building Offer Chair Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_chair', __('models/buildingManagementNews.fields.building_offer_chair').':') !!}
    <p>{{ $buildingManagementNew->building_offer_chair }}</p>
</div>


<!-- Building Offer Sofa Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_sofa', __('models/buildingManagementNews.fields.building_offer_sofa').':') !!}
    <p>{{ $buildingManagementNew->building_offer_sofa }}</p>
</div>


<!-- Building Offer Other Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_other', __('models/buildingManagementNews.fields.building_offer_other').':') !!}
    <p>{{ $buildingManagementNew->building_offer_other }}</p>
</div>


<!-- Building Use Field -->
<div class="form-group col-12">
    {!! Form::label('building_use', __('models/buildingManagementNews.fields.building_use').':') !!}
    <p>{{ $buildingManagementNew->building_use }}</p>
</div>


<!-- Apply Date Field -->
<div class="form-group col-12">
    {!! Form::label('apply_date', __('models/buildingManagementNews.fields.apply_date').':') !!}
    <p>{{ $buildingManagementNew->apply_date }}</p>
</div>


<!-- Case No Field -->
<div class="form-group col-12">
    {!! Form::label('case_no', __('models/buildingManagementNews.fields.case_no').':') !!}
    <p>{{ $buildingManagementNew->case_no }}</p>
</div>


<!-- Case Type Chartering Field -->
<div class="form-group col-12">
    {!! Form::label('case_type_chartering', __('models/buildingManagementNews.fields.case_type_chartering').':') !!}
    <p>{{ $buildingManagementNew->case_type_chartering }}</p>
</div>


<!-- Case Type Escrow Field -->
<div class="form-group col-12">
    {!! Form::label('case_type_escrow', __('models/buildingManagementNews.fields.case_type_escrow').':') !!}
    <p>{{ $buildingManagementNew->case_type_escrow }}</p>
</div>


<!-- Case State Field -->
<div class="form-group col-12">
    {!! Form::label('case_state', __('models/buildingManagementNews.fields.case_state').':') !!}
    <p>{{ $buildingManagementNew->case_state }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/buildingManagementNews.fields.name').':') !!}
    <p>{{ $buildingManagementNew->name }}</p>
</div>


<!-- Gender Field -->
<div class="form-group col-12">
    {!! Form::label('gender', __('models/buildingManagementNews.fields.gender').':') !!}
    <p>{{ $buildingManagementNew->gender }}</p>
</div>


<!-- Birthday Field -->
<div class="form-group col-12">
    {!! Form::label('birthday', __('models/buildingManagementNews.fields.birthday').':') !!}
    <p>{{ $buildingManagementNew->birthday }}</p>
</div>


<!-- Is Foreigner Field -->
<div class="form-group col-12">
    {!! Form::label('is_foreigner', __('models/buildingManagementNews.fields.is_foreigner').':') !!}
    <p>{{ $buildingManagementNew->is_foreigner }}</p>
</div>


<!-- Id No Field -->
<div class="form-group col-12">
    {!! Form::label('id_no', __('models/buildingManagementNews.fields.id_no').':') !!}
    <p>{{ $buildingManagementNew->id_no }}</p>
</div>


<!-- Passport Code Field -->
<div class="form-group col-12">
    {!! Form::label('passport_code', __('models/buildingManagementNews.fields.passport_code').':') !!}
    <p>{{ $buildingManagementNew->passport_code }}</p>
</div>


<!-- House No Field -->
<div class="form-group col-12">
    {!! Form::label('house_no', __('models/buildingManagementNews.fields.house_no').':') !!}
    <p>{{ $buildingManagementNew->house_no }}</p>
</div>


<!-- Tal Day Field -->
<div class="form-group col-12">
    {!! Form::label('tal_day', __('models/buildingManagementNews.fields.tal_day').':') !!}
    <p>{{ $buildingManagementNew->tal_day }}</p>
</div>


<!-- Tal Night Field -->
<div class="form-group col-12">
    {!! Form::label('tal_night', __('models/buildingManagementNews.fields.tal_night').':') !!}
    <p>{{ $buildingManagementNew->tal_night }}</p>
</div>


<!-- Cellphone Field -->
<div class="form-group col-12">
    {!! Form::label('cellphone', __('models/buildingManagementNews.fields.cellphone').':') !!}
    <p>{{ $buildingManagementNew->cellphone }}</p>
</div>


<!-- Email Field -->
<div class="form-group col-12">
    {!! Form::label('email', __('models/buildingManagementNews.fields.email').':') !!}
    <p>{{ $buildingManagementNew->email }}</p>
</div>


<!-- Residence City Field -->
<div class="form-group col-12">
    {!! Form::label('residence_city', __('models/buildingManagementNews.fields.residence_city').':') !!}
    <p>{{ $buildingManagementNew->residence_city }}</p>
</div>


<!-- Residence City Area Field -->
<div class="form-group col-12">
    {!! Form::label('residence_city_area', __('models/buildingManagementNews.fields.residence_city_area').':') !!}
    <p>{{ $buildingManagementNew->residence_city_area }}</p>
</div>


<!-- Residence Addr Field -->
<div class="form-group col-12">
    {!! Form::label('residence_addr', __('models/buildingManagementNews.fields.residence_addr').':') !!}
    <p>{{ $buildingManagementNew->residence_addr }}</p>
</div>


<!-- Mailing City Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_city', __('models/buildingManagementNews.fields.mailing_city').':') !!}
    <p>{{ $buildingManagementNew->mailing_city }}</p>
</div>


<!-- Mailing City Area Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_city_area', __('models/buildingManagementNews.fields.mailing_city_area').':') !!}
    <p>{{ $buildingManagementNew->mailing_city_area }}</p>
</div>


<!-- Mailing Addr Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_addr', __('models/buildingManagementNews.fields.mailing_addr').':') !!}
    <p>{{ $buildingManagementNew->mailing_addr }}</p>
</div>


<!-- Legal Agent Num Field -->
<div class="form-group col-12">
    {!! Form::label('legal_agent_num', __('models/buildingManagementNews.fields.legal_agent_num').':') !!}
    <p>{{ $buildingManagementNew->legal_agent_num }}</p>
</div>


<!-- Single Proxy Reason Field -->
<div class="form-group col-12">
    {!! Form::label('single_proxy_reason', __('models/buildingManagementNews.fields.single_proxy_reason').':') !!}
    <p>{{ $buildingManagementNew->single_proxy_reason }}</p>
</div>


<!-- First Legal Agent Name Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_name', __('models/buildingManagementNews.fields.first_legal_agent_name').':') !!}
    <p>{{ $buildingManagementNew->first_legal_agent_name }}</p>
</div>


<!-- First Legal Agent Tel Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_tel', __('models/buildingManagementNews.fields.first_legal_agent_tel').':') !!}
    <p>{{ $buildingManagementNew->first_legal_agent_tel }}</p>
</div>


<!-- First Legal Agent Phone Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_phone', __('models/buildingManagementNews.fields.first_legal_agent_phone').':') !!}
    <p>{{ $buildingManagementNew->first_legal_agent_phone }}</p>
</div>


<!-- First Legal Agent Addr Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_addr', __('models/buildingManagementNews.fields.first_legal_agent_addr').':') !!}
    <p>{{ $buildingManagementNew->first_legal_agent_addr }}</p>
</div>


<!-- Second Legal Agent Name Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_name', __('models/buildingManagementNews.fields.second_legal_agent_name').':') !!}
    <p>{{ $buildingManagementNew->second_legal_agent_name }}</p>
</div>


<!-- Second Legal Agent Tel Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_tel', __('models/buildingManagementNews.fields.second_legal_agent_tel').':') !!}
    <p>{{ $buildingManagementNew->second_legal_agent_tel }}</p>
</div>


<!-- Second Legal Agent Phone Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_phone', __('models/buildingManagementNews.fields.second_legal_agent_phone').':') !!}
    <p>{{ $buildingManagementNew->second_legal_agent_phone }}</p>
</div>


<!-- Second Legal Agent Addr Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_addr', __('models/buildingManagementNews.fields.second_legal_agent_addr').':') !!}
    <p>{{ $buildingManagementNew->second_legal_agent_addr }}</p>
</div>


<!-- Bank Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_name', __('models/buildingManagementNews.fields.bank_name').':') !!}
    <p>{{ $buildingManagementNew->bank_name }}</p>
</div>


<!-- Bank Branch Code Field -->
<div class="form-group col-12">
    {!! Form::label('bank_branch_code', __('models/buildingManagementNews.fields.bank_branch_code').':') !!}
    <p>{{ $buildingManagementNew->bank_branch_code }}</p>
</div>


<!-- Bank Owner Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_owner_name', __('models/buildingManagementNews.fields.bank_owner_name').':') !!}
    <p>{{ $buildingManagementNew->bank_owner_name }}</p>
</div>


<!-- Bank Account Field -->
<div class="form-group col-12">
    {!! Form::label('bank_account', __('models/buildingManagementNews.fields.bank_account').':') !!}
    <p>{{ $buildingManagementNew->bank_account }}</p>
</div>


