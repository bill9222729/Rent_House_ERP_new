<style>
    .text-area {
        background-color: #fff;
        background-image: none;
        border: 1px solid rgba(0, 0, 0, 0.07);
        font-family: Arial, sans-serif;
        -webkit-appearance: none;
        color: #373e43;
        outline: 0;
        padding: 8px 12px;
        line-height: normal;
        font-size: 14px;
        font-weight: normal;
        vertical-align: middle;
        min-height: 30px;
        margin-left: 3px;
    }
    .lign-mid {
        margin-left: -7%;
        padding-top: 5px;
        text-align: right;
    }
    .necessary {
        color: #FF0000;
    }
</style>

<div class="row">
<div class="form-group col-sm-2">
    <label class="necessary">物件狀態:</label>
    {!! Form::select('status', ['招租中'=>'招租中', '已媒合' => '已媒合', '暫停' => '暫停', '警示客戶' => '警示客戶', '退出計畫' => '退出計畫', '待追蹤' => '待追蹤'],  $buildingManagement->status, ['class' => 'form-control'])!!}
</div>
<div class="form-group col-sm-2">
    <label>物件編號:</label>
    {!! Form::text('num', $buildingManagement->num, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<!-- Apply Date Field -->
<div class="form-group col-sm-2">
    {!! Form::label('apply_date', __('models/buildingManagements.fields.apply_date').':') !!}
    @isset($buildingManagement)
    {!! Form::text('apply_date', ((int)date('Y',strtotime($buildingManagement->apply_date)) - 1911) . date('-m-d',strtotime($buildingManagement->apply_date)), ['class' => 'form-control','id'=>'apply_date', 'style' => 'height: 35px;']) !!}
    @else
    {!! Form::text('apply_date', null, ['class' => 'form-control','id'=>'apply_date', 'style' => 'height: 35px;']) !!}
    @endisset
</div>

<!-- Charter Field -->
<div class="form-group col-sm-4">
    {!! Form::label('re_type', __('models/buildingManagements.fields.re_type').':', ['class' => 'necessary']) !!}
    {!! Form::select('re_type', array('包租' => '包租', '代管' => '代管'), $buildingManagement->re_type, ['class' => 'form-control', 'id' => 're_type']) !!}
</div>

<!-- Request Form Type Field -->
<div class="form-group col-sm-2">
    {!! Form::label('request_form_type', __('models/buildingManagements.fields.request_form_type').':', ['class' => 'necessary']) !!}
    {!! Form::select('request_form_type', array(1 => '自然人', 2 => '法人'), $buildingManagement->request_form_type, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-2">
    <label class="necessary">出租人姓名:</label>
    {!! Form::text('Lessor_name', $buildingManagement->Lessor_name, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div class="form-group col-sm-2">
    <label class="necessary">性別:</label>
    {!! Form::select('Lessor_gender', array('男' => '男', '女' => '女'), $buildingManagement->Lessor_gender, ['class' => 'form-control', 'style' => 'height: 30px; padding: 5px;']) !!}
    {{-- {!! Form::text('Lessor_gender', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!} --}}
</div>
<div class="form-group col-sm-2">
    <label class="necessary">出生年月日:</label>
    @isset($buildingManagement)
    {!! Form::text('Lessor_birthday', ((int)date('Y',strtotime($buildingManagement->Lessor_birthday)) - 1911) . date('-m-d',strtotime($buildingManagement->Lessor_birthday)), ['class' => 'form-control','id'=>'Lessor_birthday', 'style' => 'height: 35px;']) !!}
    @else
    {!! Form::text('Lessor_birthday', null, ['class' => 'form-control','id'=>'Lessor_birthday', 'style' => 'height: 35px;']) !!}
    @endisset
    {{-- {!! Form::text('Lessor_birthday', null, ['class' => 'text-area', 'style' => 'width: 100%; height: 30px;']) !!} --}}
</div>
<div class="form-group col-sm-3">
    <label class="necessary">身分證字號:</label>
    {!! Form::text('Lessor_ID_num', $buildingManagement->Lessor_ID_num, ['class' => 'form-control','maxlength'=>'10', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div class="form-group col-sm-3">
    <label class="necessary">連絡電話:</label>
    {!! Form::text('Lessor_phone', $buildingManagement->Lessor_phone, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>

<div class="form-group col-sm-12">
    <label>地址:</label><br>

    <label class="necessary">縣市</label>
    {!! Form::text('address_city', $buildingManagement->address_city, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">鄉鎮區</label>{!! Form::text('address_cityarea', $buildingManagement->address_cityarea, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">街道</label>{!! Form::text('address_street', $buildingManagement->address_street, ['class' => 'text-area', 'style' => 'width: 20%; height: 30px;']) !!}

    {!! Form::text('address_ln', $buildingManagement->address_ln, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}<label>巷</label>

    {!! Form::text('address_aly', $buildingManagement->address_aly, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}<label>弄</label>

    {!! Form::text('address_num', $buildingManagement->address_num, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}<label class="necessary">號</label>

    <label class="necessary">之</label>{!! Form::text('address_num_hyphen', $buildingManagement->address_num_hyphen, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}

</div>
<div style="clear: left;float: left;width: 1px;"></div>
<div class="form-group col-sm-12">
    <label>戶籍地址:</label><br>

    <label class="necessary">縣市</label>
    {!! Form::text('residence_address_city', $buildingManagement->residence_address_city, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">鄉鎮區</label>{!! Form::text('residence_address_cityarea', $buildingManagement->residence_address_cityarea, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">街道</label>{!! Form::text('residence_address_street', $buildingManagement->residence_address_street, ['class' => 'text-area', 'style' => 'width: 20%; height: 30px;']) !!}

    {!! Form::text('residence_address_ln', $buildingManagement->residence_address_ln, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}<label>巷</label>

    {!! Form::text('residence_address_aly', $buildingManagement->residence_address_aly, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}<label>弄</label>

    {!! Form::text('residence_address_num', $buildingManagement->residence_address_num, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}<label class="necessary">號</label>

    <label class="necessary">之</label>{!! Form::text('residence_address_num_hyphen', $buildingManagement->residence_address_num_hyphen, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}

</div>

<div class="form-group col-sm-6">
    <label class="necessary">收款代理人:</label>
    {!! Form::text('collection_agent', $buildingManagement->collection_agent, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div class="form-group col-sm-6">
    <label class="necessary">代理人身分證:</label>
    {!! Form::text('agent_ID_num', $buildingManagement->agent_ID_num, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div class="form-group col-sm-3">
    <label class="necessary">銀行名稱:</label>
    {!! Form::text('agent_bank_name', $buildingManagement->agent_bank_name, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div class="form-group col-sm-3">
    <label class="necessary">分行代號:</label>
    {!! Form::text('agent_branch_name', $buildingManagement->agent_branch_name, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div class="form-group col-sm-6">
    <label class="necessary">房東銀行帳號:</label>
    {!! Form::text('landlord_bank_account', $buildingManagement->landlord_bank_account, ['class' => 'form-control','maxlength'=>'14', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div style="clear: left;float: left;width: 1px;"></div>
<div class="form-group col-sm-12">
    <label>建物坐落:</label><br>

    <label class="necessary">縣市</label>{!! Form::text('located_city', $buildingManagement->located_city, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">鄉鎮區</label>{!! Form::text('located_cityarea', $buildingManagement->located_cityarea, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">段號</label>{!! Form::text('located_segment_num', $buildingManagement->located_segment_num, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">地段小段</label>{!! Form::text('located_small_lot', $buildingManagement->located_small_lot, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}

    <label class="necessary">地號</label>{!! Form::text('located_land_num', $buildingManagement->located_land_num, ['class' => 'text-area','maxlength'=>'9', 'style' => 'width: 10%; height: 30px;']) !!}

    <label class="necessary">建號</label>{!! Form::text('located_build_num', $buildingManagement->located_build_num, ['class' => 'text-area','maxlength'=>'9', 'style' => 'width: 10%; height: 30px;']) !!}

    <label class="necessary">地址</label>{!! Form::text('located_build_address', $buildingManagement->located_build_address, ['class' => 'text-area', 'style' => 'width: 25%; height: 30px;']) !!}
</div>

<!-- Pattern Field -->
<div class="form-group col-sm-4">
    {!! Form::label('pattern', __('models/buildingManagements.fields.pattern').':', ['class' => 'necessary']) !!}
    {!! Form::select('pattern', array('套房' => '套房', '雅房' => '雅房', '1房' => '1房', '2房' => '2房', '3房' => '3房', '4房以上' => '4房以上', '其他' => '其他'), $buildingManagement->pattern, ['class' => 'form-control']) !!}
</div>

<!-- Pattern Remark Field -->
<div class="form-group col-sm-8">
    {!! Form::label('pattern_remark', __('models/buildingManagements.fields.pattern_remark').':', ['class' => 'necessary']) !!}
    {!! Form::text('pattern_remark', $buildingManagement->pattern_remark, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <label>房屋資訊:</label><br>
    <label class="necessary">類型:</label>
    {!! Form::select('h_info_type', array('電梯大樓' => '電梯大樓', '公寓' => '公寓', '透天厝' => '透天厝', '平房' => '平房'), $buildingManagement->h_info_type, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px; padding: 5px;']) !!}

    <label class="necessary">屋齡:</label>{!! Form::text('h_info_age', $buildingManagement->h_info_age, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!}

    <label class="necessary">完工日期:</label>
    @isset($buildingManagement)
        {!! Form::text('h_info_completion_date', ((int)date('Y',strtotime($buildingManagement->h_info_completion_date)) - 1911) . date('-m-d',strtotime($buildingManagement->h_info_completion_date)), ['class' => 'text-area','id'=>'h_info_completion_date', 'style' => 'width: 30%; height: 30px;']) !!}
    @else
        {!! Form::text('h_info_completion_date', null, ['class' => 'text-area','id'=>'h_info_completion_date', 'style' => 'width: 30%; height: 30px;']) !!}
    @endisset

    @push('scripts')
    <script type="text/javascript">
        $('#h_info_completion_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
    @endpush
</div>

<div class="form-group col-sm-12">
    <label>樓層:</label><br>

    {!! Form::text('h_info_fl', $buildingManagement->h_info_fl, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}<label class="necessary">樓</label>

    <label class="necessary">之</label>{!! Form::text('h_info_hyphen', $buildingManagement->h_info_hyphen, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}

    {!! Form::text('h_info_suite', $buildingManagement->h_info_suite, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}<label>室/房，</label>

    <label class="necessary">總層數</label>{!! Form::text('h_info_total_fl', $buildingManagement->h_info_total_fl, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}<label class="necessary">層</label>

</div>
<div class="form-group col-sm-12">
    <label>隔間:</label><br>

    {!! Form::text('h_info_pattern_room', $buildingManagement->h_info_pattern_room, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}<label class="necessary">房</label>

    {!! Form::text('h_info_pattern_hall', $buildingManagement->h_info_pattern_hall, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}<label class="necessary">廳</label>

    {!! Form::text('h_info_pattern_bath', $buildingManagement->h_info_pattern_bath, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}<label class="necessary">衛</label>

</div>

<div class="form-group col-sm-2">
    <label>隔間材質:</label>{!! Form::text('h_info_pattern_material', $buildingManagement->h_info_pattern_material, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-2">
    <label class="necessary">權狀坪數(平方公尺):</label>{!! Form::text('h_info_warrant_ping_num', $buildingManagement->h_info_warrant_ping_num, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-2">
    <label class="necessary">實際坪數(平方公尺):</label>{!! Form::text('h_info_actual_ping_num', $buildingManagement->h_info_actual_ping_num, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-3">
    <label class="necessary">建物主要用途:</label>{!! Form::text('h_info_usage', $buildingManagement->h_info_usage, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-3">
    <label class="necessary">建材:</label>{!! Form::text('h_info_material', $buildingManagement->h_info_material, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    <label>房東期待:</label><br>

    <label class="necessary">租金:</label>{!! Form::text('landlord_expect_rent', $buildingManagement->landlord_expect_rent, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label class="necessary">押金(月):</label>{!! Form::text('landlord_expect_deposit_month', $buildingManagement->landlord_expect_deposit_month, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}

    <label class="necessary">押金(元):</label>{!! Form::text('landlord_expect_deposit', $buildingManagement->landlord_expect_deposit, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label>車位管理費(元):</label>{!! Form::text('park_mang_fee', $buildingManagement->park_mang_fee, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label>可議價:</label>
    {!! Form::select('landlord_expect_bargain', array('1' => '可', '0' => '不可'), $buildingManagement->landlord_expect_bargain, ['class' => 'text-area', 'style' => 'width: 4%; height: 30px; padding: 5px;']) !!}
    {{-- <label class="checkbox-inline">
        {!! Form::hidden('landlord_expect_bargain', 0) !!}
        {!! Form::checkbox('landlord_expect_bargain', '1', null) !!}
    </label> --}}

    <label class="necessary">包含管理費:</label>
    {!! Form::select('manage_fee', array('1' => '包含', '0' => '不包含'), $buildingManagement->manage_fee, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px; padding: 5px;']) !!}
    {{-- <label class="checkbox-inline">
        {!! Form::hidden('manage_fee', 0) !!}
        {!! Form::checkbox('manage_fee', '1', null) !!}
    </label> --}}

    <label>管理費(每月):</label>{!! Form::text('manage_fee_month', $buildingManagement->manage_fee_month, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

    <label>管理費(每坪):</label>{!! Form::text('manage_fee_ping', $buildingManagement->manage_fee_ping, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}
</div>

<div class="form-group col-sm-12">
    <label>租金包含費用:</label><br>
    <div class="row">
    <label class="col-sm-2 lign-mid necessary">是否包含電</label>
    {!! Form::select('contain_fee_ele', array('1' => '包含', '0' => '不包含'), $buildingManagement->contain_fee_ele, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}<br>
    </div>
    <div class="row">
    <label class="col-sm-2 lign-mid necessary">是否包含水費</label>
    {!! Form::select('contain_fee_water', array('1' => '包含', '0' => '不包含'), $buildingManagement->contain_fee_water, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}<br>
    </div>
    <div class="row">
    <label class="col-sm-2 lign-mid necessary">是否包含瓦斯</label>
    {!! Form::select('contain_fee_gas', array('1' => '包含', '0' => '不包含'), $buildingManagement->contain_fee_gas, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}<br>
    </div>
    <div class="row">
    <label class="col-sm-2 lign-mid necessary">是否包含清潔</label>
    {!! Form::select('contain_fee_clean', array('1' => '包含', '0' => '不包含'), $buildingManagement->contain_fee_clean, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}<br>
    </div>
    <div class="row">
    <label class="col-sm-2 lign-mid necessary">是否包含第四台</label>
    {!! Form::select('contain_fee_pay_tv', array('1' => '包含', '0' => '不包含'), $buildingManagement->contain_fee_pay_tv, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}<br>
    </div>
    <div class="row">
    <label class="col-sm-2 lign-mid necessary">是否包含網路</label>
    {!! Form::select('contain_fee_net', array('1' => '包含', '0' => '不包含'), $buildingManagement->contain_fee_net, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}<br>
    </div>
    <div class="row">
    <label class="col-sm-2 lign-mid necessary">是否包含車位</label>
    {!! Form::select('Barrier_free_facility', array('1' => '包含', '0' => '不包含'), $buildingManagement->Barrier_free_facility, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}<br>
    </div>
</div>

<div class="form-group col-sm-12">
    <label>提供設備:</label><br>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_tv', 0) !!}
        {!! Form::checkbox('equipment_tv', '1', $buildingManagement->equipment_tv) !!}
    </label>
    <label>電視</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_refrigerator', 0) !!}
        {!! Form::checkbox('equipment_refrigerator', '1', $buildingManagement->equipment_refrigerator) !!}
    </label>
    <label>冰箱</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_pay_tv', 0) !!}
        {!! Form::checkbox('equipment_pay_tv', '1', $buildingManagement->equipment_pay_tv) !!}
    </label>
    <label>有線電視</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_air_conditioner', 0) !!}
        {!! Form::checkbox('equipment_air_conditioner', '1', $buildingManagement->equipment_air_conditioner) !!}
    </label>
    <label>冷氣</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_water_heater', 0) !!}
        {!! Form::checkbox('equipment_water_heater', '1', $buildingManagement->equipment_water_heater) !!}
    </label>
    <label>熱水器</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_net', 0) !!}
        {!! Form::checkbox('equipment_net', '1', $buildingManagement->equipment_net) !!}
    </label>
    <label>網路</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_washer', 0) !!}
        {!! Form::checkbox('equipment_washer', '1', $buildingManagement->equipment_washer) !!}
    </label>
    <label>洗衣機</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_gas', 0) !!}
        {!! Form::checkbox('equipment_gas', '1', $buildingManagement->equipment_gas) !!}
    </label>
    <label>天然瓦斯</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_bed', 0) !!}
        {!! Form::checkbox('equipment_bed', '1', $buildingManagement->equipment_bed) !!}
    </label>
    <label>床</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_wardrobe', 0) !!}
        {!! Form::checkbox('equipment_wardrobe', '1', $buildingManagement->equipment_wardrobe) !!}
    </label>
    <label>衣櫃</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_table', 0) !!}
        {!! Form::checkbox('equipment_table', '1', $buildingManagement->equipment_table) !!}
    </label>
    <label>桌</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_chair', 0) !!}
        {!! Form::checkbox('equipment_chair', '1', $buildingManagement->equipment_chair) !!}
    </label>
    <label>椅</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_sofa', 0) !!}
        {!! Form::checkbox('equipment_sofa', '1', $buildingManagement->equipment_sofa) !!}
    </label>
    <label>沙發</label>

    <label class="checkbox-inline">
        {!! Form::hidden('equipment_other', 0) !!}
        {!! Form::checkbox('equipment_other', '1', $buildingManagement->equipment_other) !!}
    </label>

    <label>其他</label>
</div>

<!-- Equipment Other Detail Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('equipment_other_detail', __('models/buildingManagements.fields.equipment_other_detail').':') !!}
    {!! Form::text('equipment_other_detail', $buildingManagement->equipment_other_detail, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    <label>其他:</label><br>

    <label class="checkbox-inline">
        {!! Form::hidden('can_Cook', 0) !!}
        {!! Form::checkbox('can_Cook', '1', $buildingManagement->can_Cook) !!}
    </label>
    <label class="necessary">可炊煮</label>
    <label class="checkbox-inline">
        {!! Form::hidden('parking', 0) !!}
        {!! Form::checkbox('parking', '1', $buildingManagement->parking) !!}
    </label>
    <label>無障礙設施</label>
    <label class="checkbox-inline">
        {!! Form::hidden('curfew', 0) !!}
        {!! Form::checkbox('curfew', '1', $buildingManagement->curfew) !!}
    </label>
    <label>門禁管理</label>
    <label class="checkbox-inline">
        {!! Form::hidden('curfew_management', 0) !!}
        {!! Form::checkbox('curfew_management', '1', $buildingManagement->curfew_management) !!}
    </label>
    <label>有無管理員</label>
    <label class="checkbox-inline">
        {!! Form::hidden('curfew_card', 0) !!}
        {!! Form::checkbox('curfew_card', '1', $buildingManagement->curfew_card) !!}
    </label>
    <label>刷卡門禁</label>
    <label class="checkbox-inline">
        {!! Form::hidden('curfew_other', 0) !!}
        {!! Form::checkbox('curfew_other', '1', $buildingManagement->curfew_other) !!}
    </label>
    <label>其他門禁:</label>
    {!! Form::text('curfew_other_detail', $buildingManagement->curfew_other_detail, ['class' => 'form-control', 'style' => 'width: 35%; display: inline;']) !!}
</div>

<!-- House Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('house_id', __('models/buildingManagements.fields.house_id').':') !!}
    {!! Form::text('house_id', $buildingManagement->house_id, ['class' => 'form-control']) !!}
</div>

<!-- Landlord Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('landlord_id', __('models/buildingManagements.fields.landlord_id').':') !!}
    {!! Form::text('landlord_id', $buildingManagement->landlord_id, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12">
    <label>加入社宅起日:</label>
    @isset($buildingManagement)
    {!! Form::text('remark_1', ((int)date('Y',strtotime($buildingManagement->remark_1)) - 1911) . date('-m-d',strtotime($buildingManagement->remark_1)), ['class' => 'form-control','id'=>'remark_1', 'style' => 'height: 35px;']) !!}
    @else
    {!! Form::text('remark_1', null, ['class' => 'form-control','id'=>'remark_1', 'style' => 'height: 35px;']) !!}
    @endisset
</div>

<div class="form-group col-sm-12">
    <label>備註:</label>
    {!! Form::text('remark_2', $buildingManagement->remark_2, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>

@php
    use Illuminate\Support\Facades\Auth;
    $user_data = Auth::user();
@endphp

@if(isset($buildingManagement->modal_mode) && $buildingManagement->modal_mode == 1)

@else

    {{-- @if(Request::is('buildingManagements*')) --}}
{{--    @if($user_data->role == "admin" || ($user_data->role == "sales" && $buildingManagement->status != "已媒合"))--}}

{{--        <!-- Submit Field -->--}}
{{--        <div class="form-group col-sm-12">--}}
{{--            <button type="button" class="btn btn-primary" id="buildingManagementSubmit" onclick="changeAllB()">@lang('crud.save')</button>--}}
{{--        </div>--}}

{{--    @endif--}}

@endif

</div>
