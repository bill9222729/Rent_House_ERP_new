@php
    $b = \App\Models\BuildingManagement::where('id', $id)->first();

@endphp
<div class='btn-group'>
    <a href="/buildingManagements/export/{{$id}}" class='btn btn-default btn-xs' target="_blank">
        下載個人文件
    </a>
    @if($b->status == '已媒合' && auth()->user()->role == 'SALES')
    @elseif($b->status == '已媒合')
        <a href="{{ route('buildingManagements.edit', $id) }}" class='btn btn-default btn-xs'>
            <i class="glyphicon glyphicon-edit"></i>
        </a>
        {!! Form::open(['route' => ['buildingManagements.destroy', $id], 'method' => 'delete']) !!}
        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
        ]) !!}
        {!! Form::close() !!}
    @else
        <button id="updateStatus" type="button" class="btn btn-default" onclick="updateStatus()">轉已媒合</button>
        <a href="{{ route('buildingManagements.edit', $id) }}" class='btn btn-default btn-xs'>
            <i class="glyphicon glyphicon-edit"></i>
        </a>
        {!! Form::open(['route' => ['buildingManagements.destroy', $id], 'method' => 'delete']) !!}
        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
        ]) !!}
        {!! Form::close() !!}
            <script>
                function updateStatus() {
                    $.ajax({
                        type: "PATCH",
                        timeout: 60000,
                        url: "api/building_managements/" + {{$id}},
                        data: {
                            'status': '已媒合'
                        },
                        success: function (data) {
                            // Swal.fire({
                            //     icon: 'success',
                            //     title: '轉已媒合成功'
                            // })
                            location.reload();
                        },
                        error: function (data) {
                            Swal.fire({
                                icon: 'error',
                                title: '轉已媒合失敗',
                            })
                        },
                    });
                }
            </script>
    @endif
</div>
