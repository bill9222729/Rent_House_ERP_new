@extends('layouts.app')
@php

    $fix = \App\Models\FixHistory::where('item_code', $buildingManagement->num)->get();

@endphp
@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">
            @lang('models/buildingManagements.singular')
        </h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        @include('adminlte-templates::common.errors')
        <div class="card card-transparent">
            <div class="card-body">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">
                    修繕表
                </button>
                <div class="row">
                    {!! Form::model($buildingManagement, ['route' => ['buildingManagements.update', $buildingManagement->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                            @include('building_managements.fields')

                    {!! Form::close() !!}
                    <!-- Modal -->
                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" style="width: 1080px !important;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">修繕表</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-responsive" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th title="業務姓名" class="sorting_desc" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-sort="descending" aria-label="業務姓名: activate to sort column ascending" style="width: 48px;">業務姓名
                                            </th>
                                            <th title="請款月報" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="請款月報: activate to sort column ascending" style="width: 56.5px;">請款月報
                                            </th>
                                            <th title="物件編號" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="物件編號: activate to sort column ascending" style="width: 25.5px;">物件編號
                                            </th>
                                            <th title="物件地址" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="物件地址: activate to sort column ascending" style="width: 48.5px;">物件地址
                                            </th>
                                            <th title="請款年度" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="請款年度: activate to sort column ascending" style="width: 25.5px;">請款年度
                                            </th>
                                            <th title="報修日" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="報修日: activate to sort column ascending" style="width: 38.5px;">報修日
                                            </th>
                                            <th title="房東姓名" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="房東姓名: activate to sort column ascending" style="width: 56.5px;">房東姓名
                                            </th>
                                            <th title="房東電話" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="房東電話: activate to sort column ascending" style="width: 87.5px;">房東電話
                                            </th>
                                            <th title="房客姓名" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="房客姓名: activate to sort column ascending" style="width: 65.5px;">房客姓名
                                            </th>
                                            <th title="房客電話" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="房客電話: activate to sort column ascending" style="width: 64.5px;">房客電話
                                            </th>
                                            <th title="修繕項目" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="修繕項目: activate to sort column ascending" style="width: 56.5px;">修繕項目
                                            </th>
                                            <th title="損壞原因" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="損壞原因: activate to sort column ascending" style="width: 56.5px;">損壞原因
                                            </th>
                                            <th title="會勘日期" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="會勘日期: activate to sort column ascending" style="width: 38.5px;">會勘日期
                                            </th>
                                            <th title="修繕日期" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="修繕日期: activate to sort column ascending" style="width: 38.5px;">修繕日期
                                            </th>
                                            <th title="完成日期" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="完成日期: activate to sort column ascending" style="width: 56.5px;">完成日期
                                            </th>
                                            <th title="修繕紀錄" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="修繕紀錄: activate to sort column ascending" style="width: 57.5px;">修繕紀錄
                                            </th>
                                            <th title="修繕金額" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="修繕金額: activate to sort column ascending" style="width: 25.5px;">修繕金額
                                            </th>
                                            <th title="收據" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="收據: activate to sort column ascending" style="width: 11.5px;">收據
                                            </th>
                                            <th title="本次請領金額" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="本次請領金額: activate to sort column ascending" style="width: 25.5px;">本次請領金額
                                            </th>
                                            <th title="餘額" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="餘額: activate to sort column ascending" style="width: 25.5px;">餘額
                                            </th>
                                            <th title="包租租約開始" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="包租租約開始: activate to sort column ascending" style="width: 56.5px;">包租租約開始
                                            </th>
                                            <th title="包租租約結束" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="包租租約結束: activate to sort column ascending" style="width: 56.5px;">包租租約結束
                                            </th>
                                            <th title="轉租租約開始" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="轉租租約開始: activate to sort column ascending" style="width: 56.5px;">轉租租約開始
                                            </th>
                                            <th title="轉租租約結束" class="sorting" tabindex="0" aria-controls="dataTableBuilder" rowspan="1" colspan="1"
                                                aria-label="轉租租約結束: activate to sort column ascending" style="width: 56.5px;">轉租租約結束
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($fix))
                                            @foreach($fix as $f)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{$f->sales}}</td>
                                            <td>{{((int)date('Y',strtotime($f->pay_date)) - 1911) . date('-m', strtotime($f->pay_date))}}</td>
                                            <td>{{$f->item_code}}</td>
                                            <td>{{$f->item_address}}</td>
                                            <td>{{$f->pay_year}}</td>
                                            <td>
                                                @if($f->call_fix_date == "房東自修" || $f->call_fix_date == "日期格式錯誤")
                                                {{$f->call_fix_date}}
                                                @else
                                                {{((int)date('Y',strtotime($f->call_fix_date)) - 1911) . date('-m-d', strtotime($f->call_fix_date))}}
                                                @endif
                                            </td>
                                            <td>{{$f->item_owner}}</td>
                                            <td>{{$f->owner_phone}}</td>
                                            <td>{{$f->client_name}}</td>
                                            <td>{{$f->client_phone}}</td>
                                            <td>{{$f->fix_item}}</td>
                                            <td>{{$f->fix_source}}</td>
                                            <td>
                                                @if($f->check_date == "房東自修" || $f->check_date == "日期格式錯誤")
                                                {{$f->check_date}}
                                                @else
                                                {{((int)date('Y',strtotime($f->check_date)) - 1911) . date('-m-d', strtotime($f->check_date))}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($f->fix_date == "房東自修" || $f->fix_date == "日期格式錯誤")
                                                {{$f->fix_date}}
                                                @else
                                                {{((int)date('Y',strtotime($f->fix_date)) - 1911) . date('-m-d', strtotime($f->fix_date))}}
                                                @endif
                                            </td>
                                            <td>{{((int)date('Y',strtotime($f->fix_done)) - 1911) . date('-m-d', strtotime($f->fix_done))}}</td>
                                            <td>{{$f->fix_record}}</td>
                                            <td>{{$f->fix_price}}</td>
                                            <td>{{$f->receipt}}</td>
                                            <td>{{$f->final_price}}</td>
                                            <td>{{$f->balance}}</td>
                                            <td>{{((int)date('Y',strtotime($f->owner_start_date)) - 1911) . date('-m-d', strtotime($f->owner_start_date))}}</td>
                                            <td>{{((int)date('Y',strtotime($f->owner_end_date)) - 1911) . date('-m-d', strtotime($f->owner_end_date))}}</td>
                                            <td>{{((int)date('Y',strtotime($f->change_start_date)) - 1911) . date('-m-d', strtotime($f->change_start_date))}}</td>
                                            <td>{{((int)date('Y',strtotime($f->change_end_date)) - 1911) . date('-m-d', strtotime($f->change_end_date))}}</td>
                                        </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-modal-lg">新增</button>
                                </div>
                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-modal-lg" style="background-color:white; width: 100%; height: 100%;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" style="max-width: 100%;
        width: 1500px;
        height: 800px;">
            <div class="modal-content">
                @include('building_managements.fix_fields')
            </div>
        </div>
    </div>
@endsection
        @push("scripts")
            <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script>
                $('#fix_form').on('submit', function (e) {
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        timeout: 60000,
                        url: "/api/fix_histories",
                        data: $("#fix_form").serialize(),
                        processData: false,
                        success: function (data) {
                            Swal.fire({
                                icon: 'success',
                                title: '新增成功',
                            })
                        },
                        error: function (data) {
                            Swal.fire({
                                icon: 'error',
                                title: '新增失敗',
                            })
                        },
                    });
                })

            </script>
    @endpush
