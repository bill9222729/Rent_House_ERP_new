<style>
    .text-area {
        background-color: #fff;
        background-image: none;
        border: 1px solid rgba(0, 0, 0, 0.07);
        font-family: Arial, sans-serif;
        -webkit-appearance: none;
        color: #373e43;
        outline: 0;
        padding: 8px 12px;
        line-height: normal;
        font-size: 14px;
        font-weight: normal;
        vertical-align: middle;
        min-height: 30px;
        margin-left: 3px;
    }

    .lign-mid {
        margin-left: -7%;
        padding-top: 5px;
        text-align: right;
    }

    .necessary {
        color: #FF0000;
    }

    .upload_button {
        background-color: blue;
        color: #fff;
        margin-top: 5px;
        width: 80px;
        height: 20px;
        text-align: center;
    }

    /* #app {
        display: none;
    } */

    input,
    select {
        color: #000 !important;
    }

</style>

<div class="row" id="app">
    <div class="form-group col-sm-2">
        <label class="necessary">物件狀態:</label>
        @isset($buildingManagement)
            {!! Form::select('status', ['招租中' => '招租中', '已媒合' => '已媒合', '暫停' => '暫停', '警示客戶' => '警示客戶', '退出計畫' => '退出計畫', '待追蹤' => '待追蹤'], null, ['class' => 'form-control']) !!}
        @else
            {!! Form::select('status', ['招租中' => '招租中', '已媒合' => '已媒合', '暫停' => '暫停', '警示客戶' => '警示客戶', '退出計畫' => '退出計畫', '待追蹤' => '待追蹤'], null, ['class' => 'form-control']) !!}
        @endisset
    </div>
    <div class="form-group col-sm-2">
        <label>物件編號:</label>
        {!! Form::text('num', null, ['class' => 'form-control maxlength_10', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <!-- Apply Date Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('apply_date', __('models/buildingManagements.fields.apply_date') . ':') !!}
        @isset($buildingManagement)
            {!! Form::text('apply_date', (int) date('Y', strtotime($buildingManagement->apply_date)) - 1911 . date('-m-d', strtotime($buildingManagement->apply_date)), ['class' => 'form-control datepickerTW', 'id' => 'apply_date', 'style' => 'height: 35px;']) !!}
        @else
            {!! Form::text('apply_date', null, ['class' => 'form-control datepickerTW', 'id' => 'apply_date', 'style' => 'height: 35px;']) !!}
        @endisset
    </div>

    <!-- Charter Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('sublet', '本人欲提供住宅出租予租屋服務事業再轉租(包租): ' . ':', ['class' => 'necessary']) !!}
        {!! Form::select('sublet', ['0' => '0:否', '1' => '1:是'], null, ['class' => 'form-control', 'id' => 'sublet']) !!}
        {!! Form::label('rent', '本人欲提供住宅經由租屋服務事業協助出租(代管): ' . ':', ['class' => 'necessary']) !!}
        {!! Form::select('rent', ['0' => '0:否', '1' => '1:是'], null, ['class' => 'form-control', 'id' => 'rent']) !!}
    </div>

    <!-- Request Form Type Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('request_form_type', __('models/buildingManagements.fields.request_form_type') . ':', ['class' => 'necessary']) !!}
        {!! Form::select('request_form_type', [1 => '1.自然人', 2 => '2.法人'], null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-2">
        <label class="necessary">出租人姓名:</label>
        {!! Form::text('Lessor_name', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-2">
        <label class="necessary">性別:</label>
        {!! Form::select('Lessor_gender', ['男' => '男', '女' => '女'], null, ['class' => 'form-control', 'style' => 'height: 30px; padding: 5px;']) !!}
        {{-- {!! Form::text('Lessor_gender', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!} --}}
    </div>
    <div class="form-group col-sm-2">
        <label class="necessary">出生年月日:</label>
        @isset($buildingManagement)
            {!! Form::text('Lessor_birthday', str_pad((string) ((int) date('Y', strtotime($buildingManagement->Lessor_birthday)) - 1911), 3, '0', STR_PAD_LEFT) . date('-m-d', strtotime($buildingManagement->Lessor_birthday)), ['class' => 'form-control datepickerTW', 'id' => 'Lessor_birthday', 'style' => 'height: 35px;'], ['readonly']) !!}
        @else
            {!! Form::text('Lessor_birthday', null, ['class' => 'form-control datepickerTW', 'id' => 'Lessor_birthday', 'style' => 'height: 35px;']) !!}
        @endisset
        {{-- {!! Form::text('Lessor_birthday', null, ['class' => 'text-area', 'style' => 'width: 100%; height: 30px;']) !!} --}}
    </div>
    <div class="form-group col-sm-3">
        <label class="necessary">身分證字號:</label>
        {!! Form::text('Lessor_ID_num', null, ['class' => 'form-control', 'v-model' => 'Lessor_ID_num', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式(如：A123456789)', 'maxlength' => '10', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-3">
        <label class="necessary">連絡電話:</label>
        {!! Form::text('Lessor_phone', null, ['class' => 'form-control', 'v-model' => 'Lessor_phone', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入包含區碼的固定電話(如：02-12345678)', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-3">
        <label class="necessary">連絡手機:</label>
        {!! Form::text('cellphone', null, ['class' => 'form-control', 'v-model' => 'cellphone', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式的手機號碼(如：0912345678)', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Email:</label>
        {!! Form::email('email', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>社區名稱:</label>
        {!! Form::text('n_name', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-12" id="address">
        <label>地址:</label><br>

        <label class="necessary">縣市</label>
        {{-- {!! Form::text('address_cityarea', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!} --}}
        <select class="text-area" name="address_city" style="width: 8%; height: 40px;"
            v-model="address_city_active">
            <option v-for="item in twZip.city" :value="item.name" :key="item.name">&nbsp;@{{ item.name }}</option>
        </select>
        <label class="necessary">鄉鎮區</label>
        {{-- {!! Form::text('address_cityarea', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!} --}}
        <select class="text-area" name="address_cityarea" style="width: 8%; height: 40px;"
            v-model="address_cityarea_active">
            <option v-for="item in twZip.area" :value="item.name" :key="item.name">
                @{{ item.name }}
            </option>
        </select>

        <label class="necessary">街道</label>
        {{-- <select class="text-area" name="address_street" style="width: 20%; height: 40px;"
            v-model="address_street_active">
            <option v-for="item in twZip.street" :value="item.name" :key="item.name">
                @{{ item.name }}
            </option>
        </select> --}}

        @isset($buildingManagement)
            {!! Form::text('address_street', null, ['class' => 'text-area', 'style' => 'width: 20%; height: 30px;']) !!}
        @else
            {!! Form::text('address_street', null, ['class' => 'text-area', 'style' => 'width: 20%; height: 30px;']) !!}
        @endisset

        {!! Form::text('address_ln', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        <label>巷</label>

        {!! Form::text('address_aly', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        <label>弄</label>

        {!! Form::text('address_num', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        <label class="necessary">號</label>

        <label class="necessary">之</label>
        {!! Form::text('address_num_hyphen', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        <button class="btn btn-primary">儲存</button>
        <br>
        <label class="necessary">第</label>
        {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        <label class="necessary">層之</label>
        {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        <label class="necessary">室/號房，共</label>
        {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
        <label class="necessary">層</label>
        <input type="text" name="" id="" class="text-area" placeholder="補充說明 Ex: 承租整棟">

    </div>
    <div style="clear: left;float: left;width: 1px;"></div>
    <div class="form-group col-sm-12">

        <label>戶籍地址: </label>
        @if (!isset($buildingManagement))
            <label> 同地址 <input type="checkbox" name="same_address" id="same_address"> </label><br>
        @endif
        <div class="residence_address_area">

            <label class="necessary">縣市</label>
            {{-- {!! Form::text('residence_address_city', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!} --}}
            <select class="text-area" name="residence_address_city" style="width: 8%; height: 40px;"
                v-model="residence_address_city_active">
                <option v-for="item in residenceTwZip.city" :value="item.name" :key="item.name">
                    &nbsp;@{{ item.name }}
                </option>
            </select>

            <label class="necessary">鄉鎮區</label>
            {{-- {!! Form::text('residence_address_cityarea', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!} --}}
            <select class="text-area" name="residence_address_cityarea" style="width: 8%; height: 40px;"
                v-model="residence_address_cityarea_active">
                <option v-for="item in residenceTwZip.area" :value="item.name" :key="item.name">
                    &nbsp;@{{ item.name }}
                </option>
            </select>

            <label class="necessary">街道</label>
            {{-- <select class="text-area" name="residence_address_street" style="width: 20%; height: 40px;"
                v-model="residence_address_street_active">
                <option v-for="item in residenceTwZip.street" :value="item.name" :key="item.name">
                    @{{ item.name }}
                </option>
            </select> --}}
            {!! Form::text('residence_address_street', null, ['class' => 'text-area', 'style' => 'width: 20%; height: 30px;']) !!}

            {!! Form::text('residence_address_ln', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            <label>巷</label>

            {!! Form::text('residence_address_aly', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            <label>弄</label>

            {!! Form::text('residence_address_num', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            <label class="necessary">號</label>

            <label class="necessary">之</label>
            {!! Form::text('residence_address_num_hyphen', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            <button class="btn btn-primary">儲存</button>
            <br>
            <label class="necessary">第</label>
            {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            <label class="necessary">層之</label>
            {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            <label class="necessary">室/號房，共</label>
            {!! Form::text('', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}
            <label class="necessary">層</label>
            <input type="text" name="" id="" class="text-area" placeholder="補充說明 Ex: 承租整棟">

        </div>
    </div>

    <div class="form-group col-sm-6">
        <label class="necessary">代理人:</label>
        {!! Form::text('collection_agent', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label class="necessary">代理人聯絡地址:</label>
        {!! Form::text('collection_agent_address', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label class="necessary">代理人電話:</label>
        {!! Form::text('collection_agent_phone', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;', 'placeholder' => '請輸入包含區碼的固定電話(如：02-12345678)']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label class="necessary">代理人身分證:</label>
        {!! Form::text('agent_ID_num', null, ['class' => 'form-control', 'v-model' => 'agent_ID_num', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-3">
        <label class="necessary">銀行名稱:</label>
        {{-- {!! Form::text('agent_bank_name', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!} --}}
        <select class="form-control" name="agent_bank_name" style="width: 100%; height: 30px;"
            v-model="agent_bank_name_active">
            <option v-for="item in bankCode.value" :value="item[2]" :key="item[2]">&nbsp;@{{ item[2] }}</option>
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label class="necessary">分行代號:</label>
        {{-- {!! Form::text('agent_branch_name', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!} --}}
        <input class="form-control" name="agent_branch_name" type="text" style="width: 100%; height: 30px;"
            v-model="agent_branch_name">
    </div>
    <div class="form-group col-sm-6">
        <label class="necessary">房東銀行帳號:</label>
        {!! Form::text('landlord_bank_account', null, ['class' => 'form-control', 'maxlength' => '14', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div style="clear: left;float: left;width: 1px;"></div>
    <div class="form-group col-sm-12">
        <label>建物坐落:</label><br>

        <label class="necessary">縣市</label>
        {{-- {!! Form::text('located_city', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!} --}}
        <select class="text-area" name="located_city" style="width: 8%; height: 40px;"
            v-model="located_city_active">
            <option v-for="item in locatedTwZip.city" :value="item.name" :key="item.name">&nbsp;@{{ item.name }}
            </option>
        </select>

        <label class="necessary">鄉鎮區</label>
        {{-- {!! Form::text('located_cityarea', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!} --}}
        <select class="text-area" name="located_cityarea" style="width: 8%; height: 40px;"
            v-model="located_cityarea_active">
            <option v-for="item in locatedTwZip.area" :value="item.name" :key="item.name">&nbsp;@{{ item.name }}
            </option>
        </select>

        <label class="necessary">段號</label>
        {!! Form::text('located_segment_num', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

        <label class="necessary">地段小段</label>
        {!! Form::text('located_small_lot', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}

        <label class="necessary">地號</label>
        {!! Form::text('located_land_num', null, ['class' => 'text-area', 'v-model' => 'located_land_num', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '0123-0000', 'maxlength' => '9', 'style' => 'width: 10%; height: 30px;']) !!}

        <label class="necessary">建號</label>
        {!! Form::text('located_build_num', null, ['class' => 'text-area', 'v-model' => 'located_build_num', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '01234-000', 'maxlength' => '9', 'style' => 'width: 10%; height: 30px;']) !!}

        {{-- <label class="necessary">地址</label> --}}
        {{-- {!! Form::text('located_build_address', null, ['class' => 'text-area', 'style' => 'width: 25%; height: 30px;']) !!} --}}
    </div>

    <!-- Pattern Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('pattern', __('models/buildingManagements.fields.pattern') . ':', ['class' => 'necessary']) !!}
        {!! Form::select('pattern', ['套房' => '1：套房', '雅房' => '2：雅房', '1房' => '3：1房', '2房' => '4：2房', '3房' => '5：3房', '4房以上' => '6：4房以上', '其他' => '99：其他'], null, ['class' => 'form-control']) !!}
    </div>

    <!-- Pattern Remark Field -->
    <div class="form-group col-sm-8">
        {!! Form::label('pattern_remark', __('models/buildingManagements.fields.pattern_remark') . ':', ['class' => 'necessary']) !!}
        {!! Form::text('pattern_remark', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        <label>房屋資訊:</label><br>
        <label class="necessary">類型:</label>
        {!! Form::select('h_info_type', ['公寓' => '1：公寓', '電梯大樓' => '2：電梯大樓', '透天厝' => '3：透天厝', '平房' => '4：平房'], null, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px; padding: 5px;']) !!}

        <label class="necessary">屋齡:</label>
        {!! Form::text('h_info_age', null, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!}

        <label class="necessary">完工日期:</label>
        @isset($buildingManagement)
            {!! Form::text('h_info_completion_date', (int) date('Y', strtotime($buildingManagement->h_info_completion_date)) - 1911 . date('-m-d', strtotime($buildingManagement->h_info_completion_date)), ['class' => 'text-area datepickerTW', 'id' => 'h_info_completion_date', 'style' => 'width: 30%; height: 30px;']) !!}
        @else
            {!! Form::text('h_info_completion_date', null, ['class' => 'text-area datepickerTW', 'id' => 'h_info_completion_date', 'style' => 'width: 30%; height: 30px;']) !!}
        @endisset

        @push('scripts')
            <script type="text/javascript">
                $('#h_info_completion_date').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss',
                    useCurrent: false
                })
            </script>
        @endpush
    </div>

    <div class="form-group col-sm-12">
        <label>樓層(透天請寫1):</label><br>

        {!! Form::text('h_info_fl', null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}
        <label class="necessary">樓</label>

        <label class="necessary">之</label>
        {!! Form::text('h_info_hyphen', null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}

        {!! Form::text('h_info_suite', null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}
        <label>室/房，</label>

        <label class="necessary">總層數</label>
        {!! Form::text('h_info_total_fl', null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}
        <label class="necessary">層</label>

    </div>
    <div class="form-group col-sm-12">
        <label>隔間:</label><br>

        {!! Form::text('h_info_pattern_room', null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}
        <label class="necessary">房</label>

        {!! Form::text('h_info_pattern_hall', null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}
        <label class="necessary">廳</label>

        {!! Form::text('h_info_pattern_bath', null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px;']) !!}
        <label class="necessary">衛</label>

    </div>

    <div class="form-group col-sm-2">
        <label>隔間材質:</label>{!! Form::text('h_info_pattern_material', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-2">
        <label class="necessary">權狀坪數(平方公尺):</label>{!! Form::text('h_info_warrant_ping_num', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-2">
        <label class="necessary">實際坪數(平方公尺):</label>{!! Form::text('h_info_actual_ping_num', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label class="necessary">建物主要用途:</label>{!! Form::text('h_info_usage', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label class="necessary">建材:</label>{!! Form::text('h_info_material', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-12">
        <label>房東期待:</label><br>

        <label class="necessary">租金:</label>{!! Form::text('landlord_expect_rent', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

        <label class="necessary">押金(月):</label>{!! Form::text('landlord_expect_deposit_month', null, ['class' => 'text-area', 'style' => 'width: 5%; height: 30px;']) !!}

        <label class="necessary">押金(元):</label>{!! Form::text('landlord_expect_deposit', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

        <label class="necessary">公證:</label>{!! Form::select('notarization', [1 => '1.是', 2 => '2.否'], null, ['class' => 'text-area', 'id' => 'notarization', 'style' => 'width: 8%; height: 30px;font-size: 12px']) !!}

        車位 {!! Form::text('park_mang_fee', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!} 位/月

        <label>可議價:</label>
        {!! Form::select('landlord_expect_bargain', [null => '請選擇', '0' => '0：否', '1' => '1：是'], null, ['class' => 'text-area', 'style' => 'width: 4%; height: 30px; padding: 5px;']) !!}
        {{-- <label class="checkbox-inline">
            {!! Form::hidden('landlord_expect_bargain', 0) !!}
            {!! Form::checkbox('landlord_expect_bargain', '1', null) !!}
        </label> --}}

        <label class="necessary">包含管理費:</label>
        {!! Form::select('manage_fee', ['0' => '0：不包含', '1' => '1：包含'], null, ['class' => 'text-area', 'style' => 'width: 6%; height: 30px; padding: 5px;']) !!}
        {{-- <label class="checkbox-inline">
            {!! Form::hidden('manage_fee', 0) !!}
            {!! Form::checkbox('manage_fee', '1', null) !!}
        </label> --}}

        <label>管理費(每月):</label>
        <label> 租客自繳 <input type="checkbox" name="pay_by_tenant" id="pay_by_tenant"> </label>
        {!! Form::text('manage_fee_month', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}

        <label>管理費(每坪):</label>{!! Form::text('manage_fee_ping', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-12">
        <label>租金包含費用:</label><br>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">是否包含水費</label>
            {!! Form::select('contain_fee_water', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">是否包含電費</label>
            {!! Form::select('contain_fee_ele', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">是否包含車位</label>
            {!! Form::select('Barrier_free_facility', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">是否包含瓦斯/天然瓦斯</label>
            {!! Form::select('contain_fee_gas', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">是否包含第四台</label>
            {!! Form::select('contain_fee_pay_tv', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">是否包含網路</label>
            {!! Form::select('contain_fee_net', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">是否包含清潔</label>
            {!! Form::select('contain_fee_clean', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        {{-- <div class="row"> --}}
        {{-- <label class="col-sm-2 lign-mid necessary">是否包含電</label> --}}
        {{-- {!! Form::select('contain_fee_ele', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!} --}}
        {{-- <br> --}}
        {{-- </div> --}}
    </div>

    <div class="form-group col-sm-12">
        <label>提供設備:</label><br>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">電視</label>
            {!! Form::select('equipment_tv', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">冰箱</label>
            {!! Form::select('equipment_refrigerator', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">有線電視(第四臺)</label>
            {!! Form::select('equipment_pay_tv', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">冷氣</label>
            {!! Form::select('equipment_air_conditioner', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">熱水器</label>
            {!! Form::select('equipment_water_heater', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">網路</label>
            {!! Form::select('equipment_net', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">洗衣機</label>
            {!! Form::select('equipment_washer', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        {{-- <div class="row">
            <label class="col-sm-2 lign-mid necessary">天然瓦斯</label>
            {!! Form::select('equipment_gas', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div> --}}
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">床</label>
            {!! Form::select('equipment_bed', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">衣櫃</label>
            {!! Form::select('equipment_wardrobe', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">桌</label>
            {!! Form::select('equipment_table', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">椅</label>
            {!! Form::select('equipment_chair', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">沙發</label>
            {!! Form::select('equipment_sofa', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">其他</label>
            {!! Form::select('equipment_other', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
    </div>

    <!-- Equipment Other Detail Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('equipment_other_detail', __('models/buildingManagements.fields.equipment_other_detail') . ':') !!}
        {!! Form::text('equipment_other_detail', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-12">
        <label>其他:</label><br>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">可炊煮</label>
            {!! Form::select('can_Cook', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">無障礙設施</label>
            {!! Form::select('parking', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">門禁管理</label>
            {!! Form::select('curfew', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">管理員</label>
            {!! Form::select('curfew_management', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">刷卡門禁</label>
            {!! Form::select('curfew_other', ['0' => '0：否', '1' => '1：是'], null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
        <div class="row">
            <label class="col-sm-2 lign-mid necessary">其他</label>
            {!! Form::text('curfew_other_detail', null, ['class' => 'col-sm-1 text-area', 'style' => 'width: 10%;']) !!}
            <br>
        </div>
    </div>
    <!--
    <div class="form-group col-sm-12">
        <label>其他:</label><br>

        <label class="checkbox-inline">
            {!! Form::hidden('can_Cook', 0) !!}
            {!! Form::checkbox('can_Cook', '1', null) !!}
        </label>
        <label class="necessary">可炊煮</label>
        <label class="checkbox-inline">
            {!! Form::hidden('parking', 0) !!}
            {!! Form::checkbox('parking', '1', null) !!}
        </label>
        <label>無障礙設施</label>
        <label style="margin-left: 2rem;">門禁管理</label>
        <label class="checkbox-inline">
            {{ Form::radio('curfew', 0, 0) }}
            <label>0：無</label>
            {{ Form::radio('curfew', 1, 1) }}
            <label>1：有</label>
        </label>
        <label>(</label>
        <label class="checkbox-inline">
            {!! Form::hidden('curfew_management', 0) !!}
            {!! Form::checkbox('curfew_management', '1', null) !!}
        </label>
        <label>管理員</label>
        <label class="checkbox-inline">
            {!! Form::hidden('curfew_card', 0) !!}
            {!! Form::checkbox('curfew_card', '1', null) !!}
        </label>
        <label>刷卡門禁</label>
        <label class="checkbox-inline">
            {!! Form::hidden('curfew_other', 0) !!}
            {!! Form::checkbox('curfew_other', '1', null) !!}
        </label>
        <label>其他:</label>
        {!! Form::text('curfew_other_detail', null, ['class' => 'form-control', 'style' => 'width: 35%; display: inline;']) !!}
        <label>)</label>
    </div>
    -->
    <div class="form-group col-sm-12">
        <label>加入社宅起日:</label>
        @isset($buildingManagement)
            {!! Form::text('remark_1', (int) date('Y', strtotime($buildingManagement->remark_1)) - 1911 . date('-m-d', strtotime($buildingManagement->remark_1)), ['class' => 'form-control datepickerTW', 'id' => 'remark_1', 'style' => 'height: 35px;']) !!}
            {{-- <input class="form-c':?ontrol datepickerTW" id="remark_1" name="remark_1" type="text" style="height: 35px;"> --}}
        @else
            {!! Form::text('remark_1', null, ['class' => 'form-control datepickerTW', 'id' => 'remark_1', 'style' => 'height: 35px;']) !!}
        @endisset
    </div>

    <div class="form-group col-sm-6">
        @php
            $bs = \App\Models\business_management::all();
            $saless = [];
            foreach ($bs as $b) {
                if (in_array($b->name, $saless)) {
                    continue;
                }

                $saless[$b->name] = $b->name;
            }
        @endphp
        <label>業務:</label>
        {!! Form::select('sales', $saless, null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>虛擬帳號:</label>
        {!! Form::number('v_account', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        @php
            $options = [
                '空屋' => '空屋',
                '現有房客' => '現有房客',
            ];
            $sourceOptions = [
                '591' => '591',
                'FB' => 'FB',
                '介紹' => '介紹',
                '公司資源' => '公司資源',
                '同學' => '同學',
                '自己找上門' => '自己找上門',
                '自己找到' => '自己找到',
                '佈告欄' => '佈告欄',
                '房客介紹' => '房客介紹',
                '社區拜訪' => '社區拜訪',
                '保全介紹' => '保全介紹',
                '客戶介紹' => '客戶介紹',
                '客戶反開發' => '客戶反開發',
                '陌生開發' => '陌生開發',
                '值班資源' => '值班資源',
                '桃園PASS' => '桃園PASS',
                '商圈經營' => '商圈經營',
                '經營' => '經營',
                '說明會' => '說明會',
                '慕名而來' => '慕名而來',
                '緣故' => '緣故',
                '臉書' => '臉書',
                '轉介' => '轉介',
                '轉案' => '轉案',
            ];
        @endphp
        <label>空屋率:</label>
        {!! Form::select('empty_house', $options, null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>來源:</label>
        {!! Form::select('source', $sourceOptions, null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>火災警報器:</label>
        {!! Form::select('fire_alert', ['有' => '有', '無' => '無'], null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>屋內滅火器:</label>
        {!! Form::select('fire_alert_in_house', ['有' => '有', '無' => '無'], null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>公證:</label>
        {!! Form::select('notary', ['是' => '是', '否' => '否'], null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>車位:</label>
        {!! Form::number('notary', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-12">
        <label>備註:</label>
        {!! Form::text('remark_2', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    @php
        if (isset($buildingManagement)) {
            $files = json_decode($buildingManagement->files, true);
        }
    @endphp
    <div class="form-group col-sm-3">
        <label>出租人出租住宅申請書:</label>
        <input type="file" @change="upload_file($event,'lessor_application')" name="lessor_application">
        <div class="upload_button" @click="add_file('lessor_application')">上傳</div>
        @if (isset($files))
            @foreach ($files['lessor_application'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="{{asset('/storage/'.$file) }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>委託出租契約書:</label>
        <input type="file" @change="upload_file($event,'com_rent')" name="com_rent">
        <div class="upload_button" @click="add_file('com_rent')">上傳</div>
        @if (isset($files))
            @foreach ($files['com_rent'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>委託管理契約書:</label>
        <input type="file" @change="upload_file($event,'com_manage')" name="com_manage">
        <div class="upload_button" @click="add_file('com_manage')">上傳</div>
        @if (isset($files))
            @foreach ($files['com_manage'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>身分證:</label>
        <input type="file" @change="upload_file($event,'identity')" name="identity">
        <div class="upload_button" @click="add_file('identity')">上傳</div>
        @if (isset($files))
            @foreach ($files['identity'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>建物謄本(權狀):</label>
        <input type="file" @change="upload_file($event,'building')" name="building">
        <div class="upload_button" @click="add_file('building')">上傳</div>
        @if (isset($files))
            @foreach ($files['building'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>房屋稅單:</label>
        <input type="file" @change="upload_file($event,'house_tax')" name="house_tax">
        <div class="upload_button" @click="add_file('house_tax')">上傳</div>
        @if (isset($files))
            @foreach ($files['house_tax'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>地價稅單:</label>
        <input type="file" @change="upload_file($event,'land_tax')" name="land_tax">
        <div class="upload_button" @click="add_file('land_tax')">上傳</div>
        @if (isset($files))
            @foreach ($files['land_tax'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>水費單:</label>
        <input type="file" @change="upload_file($event,'water_fee')" name="water_fee">
        <div class="upload_button" @click="add_file('water_fee')">上傳</div>
        @if (isset($files))
            @foreach ($files['water_fee'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>電費單:</label>
        <input type="file" @change="upload_file($event,'ele_fee')" name="ele_fee">
        <div class="upload_button" @click="add_file('ele_fee')">上傳</div>
        @if (isset($files))
            @foreach ($files['ele_fee'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>銀行帳號:</label>
        <input type="file" @change="upload_file($event,'bank_account')" name="bank_account">
        <div class="upload_button" @click="add_file('bank_account')">上傳</div>
        @if (isset($files))
            @foreach ($files['bank_account'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>屋況及租屋安全檢核表:</label>
        <input type="file" @change="upload_file($event,'house_checklist')" name="house_checklist">
        <div class="upload_button" @click="add_file('house_checklist')">上傳</div>
        @if (isset($files))
            @foreach ($files['house_checklist'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>家具設備清單:</label>
        <input type="file" @change="upload_file($event,'furniture_checklist')" name="furniture_checklist">
        <div class="upload_button" @click="add_file('furniture_checklist')">上傳</div>
        @if (isset($files))
            @foreach ($files['furniture_checklist'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>照片:</label>
        <input type="file" @change="upload_file($event,'picture')" name="picture">
        <div class="upload_button" @click="add_file('picture')">上傳</div>
        @if (isset($files))
            @foreach ($files['picture'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>
    <div class="form-group col-sm-3">
        <label>其他:</label>
        <input type="file" @change="upload_file($event,'other')" name="other">
        <div class="upload_button" @click="add_file('other')">上傳</div>
        @if (isset($files))
            @foreach ($files['other'] as $file)
                <a href="/storage/{{ $file }}" target="_blank" rel="noreferrer noopener">預覽檔案</a>
                <img src="/storage/{{ $file }}">
            @endforeach
        @endif
    </div>

    <!-- 用來記所有檔案json的text(隱藏) -->
    <input type="text" v-model='file_json' name='file_json' v-show=false>

    @php
        use Illuminate\Support\Facades\Auth;
        $user_data = Auth::user();
    @endphp

    @if (isset($buildingManagement->modal_mode) && $buildingManagement->modal_mode == 1)

    @else

        {{-- @if (Request::is('buildingManagements*')) --}}
        @if ($user_data->role == 'admin' || ($user_data->role == 'sales' && !isset($buildingManagement)) || ($user_data->role == 'sales' && $buildingManagement->status != '已媒合'))

            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('buildingManagements.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
            </div>

        @endif

    @endif

</div>

@push('scripts')
    <script src="https://unpkg.com/vue@next"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.23.0/axios.min.js"
        integrity="sha512-Idr7xVNnMWCsgBQscTSCivBNWWH30oo/tzYORviOCrLKmBaRxRflm2miNhTFJNVmXvCtzgms5nlJF4az2hiGnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        const {
            onMounted,
            reactive,
            ref,
            watch,
        } = Vue;
        const App = {
            setup() {
                // 鄉鎮地址部分
                const twZip = reactive({
                    city: [],
                    area: [],
                    street: []
                });
                const residenceTwZip = reactive({
                    city: [],
                    area: [],
                    street: []
                });
                const locatedTwZip = reactive({
                    city: [],
                    area: [],
                    street: []
                });
                const address_city_active = ref('');
                const address_cityarea_active = ref('');
                const address_street_active = ref('');
                const residence_address_city_active = ref('');
                const residence_address_cityarea_active = ref('');
                const residence_address_street_active = ref('');
                const located_city_active = ref('');
                const located_cityarea_active = ref('');
                @if (isset($buildingManagement))
                    const dbData = reactive({
                    "address_city": '{{ $buildingManagement->address_city }}',
                    "address_cityarea": '{{ $buildingManagement->address_cityarea }}',
                    "address_street": '{{ $buildingManagement->address_street }}',
                    "residence_address_city": '{{ $buildingManagement->residence_address_city }}',
                    "residence_address_cityarea": '{{ $buildingManagement->residence_address_cityarea }}',
                    "residence_address_street": '{{ $buildingManagement->residence_address_street }}',
                    "located_city": '{{ $buildingManagement->located_city }}',
                    "located_cityarea": '{{ $buildingManagement->located_cityarea }}',
                    "address_city": '{{ $buildingManagement->address_city }}',
                    "address_cityarea": '{{ $buildingManagement->address_cityarea }}',
                    "agent_bank_name": '{{ $buildingManagement->agent_bank_name }}',
                    "agent_branch_name": '{{ $buildingManagement->agent_branch_name }}',
                    "lessor_phone": '{{ $buildingManagement->Lessor_phone }}',
                    "cellphone": '{{ $buildingManagement->cellphone }}',
                    "Lessor_ID_num": '{{ $buildingManagement->Lessor_ID_num }}',
                    "agent_ID_num": '{{ $buildingManagement->agent_ID_num }}',
                    "located_land_num": '{{ $buildingManagement->located_land_num }}',
                    "located_build_num": '{{ $buildingManagement->located_build_num }}',
                    });
                @else
                    const dbData = reactive({
                    "address_city": '',
                    "address_cityarea": '',
                    "residence_address_city": '',
                    "residence_address_cityarea": '',
                    "located_city": '',
                    "located_cityarea": '',
                    "address_city": '',
                    "address_cityarea": '',
                    "agent_bank_name": '',
                    "agent_branch_name": '',
                    "lessor_phone": '',
                    "cellphone": '',
                    "Lessor_ID_num": '',
                    "located_land_num": '',
                    "located_build_num": '',
                    "agent_ID_num": '',
                    });
                @endif


                watch(address_city_active, (newCity) => {
                    const newArr = twZip.city.filter((city) => city.name === newCity);
                    if (newArr.length != 0) {
                        twZip.area = newArr[0].area;
                    }
                });

                watch(address_cityarea_active, (newArea) => {
                    const newArr = twZip.area.filter((area) => area.name === newArea);
                    if (newArr.length != 0) {
                        twZip.street = newArr[0].road;
                    }
                });

                watch(residence_address_city_active, (newCity) => {
                    const newArr = residenceTwZip.city.filter((city) => city.name === newCity);
                    if (newArr.length != 0) {
                        residenceTwZip.area = newArr[0].area;
                    }
                });

                watch(residence_address_cityarea_active, (newArea) => {
                    const newArr = residenceTwZip.area.filter((area) => area.name === newArea);
                    if (newArr.length != 0) {
                        residenceTwZip.street = newArr[0].road;
                    }
                });

                watch(located_city_active, (newCity) => {
                    const newArr = locatedTwZip.city.filter((city) => city.name === newCity);
                    if (newArr.length != 0) {
                        locatedTwZip.area = newArr[0].area;
                    }
                });

                // 銀行部分
                const bankCode = reactive({
                    value: []
                });
                const agent_bank_name_active = ref('');
                const agent_branch_name = ref('');


                watch(agent_bank_name_active, (newBank) => {

                    console.log(newBank);
                    const newArr = bankCode.value.filter((bank) => bank[2] === newBank);
                    if (newArr.length != 0) {
                        agent_branch_name.value = newArr[0][0];
                    }
                });

                // 表單驗證
                const cellphone = ref(dbData.cellphone);
                const Lessor_phone = ref(dbData.lessor_phone);
                const Lessor_ID_num = ref(dbData.Lessor_ID_num);
                const agent_ID_num = ref(dbData.agent_ID_num);
                const located_land_num = ref(dbData.located_land_num);
                const located_build_num = ref(dbData.located_build_num);
                const inputFocus = ref(false);

                watch(inputFocus, (newInputFocus) => {
                    if (newInputFocus.value === true) {
                        return;
                    }
                    // 檢查家電號碼
                    if (!/^0\d{1,3}-\d{6,8}(#\d{1,3})?$/.test(Lessor_phone.value)) {
                        Lessor_phone.value = "";
                    }
                    // 檢查手機號碼
                    if (!/^09\d{8}$/.test(cellphone.value)) {
                        cellphone.value = "";
                    }
                    // 檢查身分證字號
                    if (!/^[A-Za-z][12]\d{8}$/.test(Lessor_ID_num.value)) {
                        Lessor_ID_num.value = "";
                    }
                    if (!/^[A-Za-z][12]\d{8}$/.test(agent_ID_num.value)) {
                        agent_ID_num.value = "";
                    }
                    // 檢查地號
                    if (!/^\d{4}-\d{4}$/.test(located_land_num.value)) {
                        located_land_num.value = "";
                    }
                    // 檢查建號
                    if (!/^\d{5}-\d{3}$/.test(located_build_num.value)) {
                        located_build_num.value = "";
                    }
                });

                onMounted(() => {
                    axios.get("/json/twZip.json").then((res) => {
                        twZip.city = res.data;
                        // 設定資料庫內的地址縣市資料
                        address_city_active.value = dbData.address_city;
                        // 設定資料庫內的地址鄉鎮區資料
                        let currentArea = twZip.city.filter((city) => city.name === dbData
                            .address_city);
                        if (currentArea.length != 0) {
                            twZip.area = currentArea[0].area;
                        }
                        address_cityarea_active.value = dbData.address_cityarea;
                        address_street_active.value = dbData.address_street;

                        residenceTwZip.city = res.data;
                        // 設定資料庫內的戶籍地址縣市資料
                        residence_address_city_active.value = dbData.residence_address_city;
                        // 設定資料庫內的戶籍地址鄉鎮區資料
                        let tempData = residenceTwZip.city.filter((city) => city.name === dbData
                            .residence_address_city);
                        if (tempData.length != 0) {
                            residenceTwZip.area = tempData[0].area;
                        }
                        residence_address_cityarea_active.value = dbData.residence_address_cityarea;
                        residence_address_street_active.value = dbData.residence_address_street;

                        locatedTwZip.city = res.data;
                        // 設定資料庫內的建物坐落縣市資料
                        located_city_active.value = dbData.located_city;
                        // 設定資料庫內的建物坐落鄉鎮區資料
                        let tempData2 = locatedTwZip.city.filter((city) => city.name === dbData
                            .located_city);
                        if (tempData2.length != 0) {
                            locatedTwZip.area = tempData2[0].area;
                        }
                        located_cityarea_active.value = dbData.located_cityarea;
                    });

                    axios.get("/json/bankCode.json").then((res) => {
                        bankCode.value = res.data;
                        agent_bank_name_active.value = dbData.agent_bank_name;
                        agent_branch_name.value = dbData.agent_branch_name;
                    });
                });


                // 上傳檔案
                const file_arr = reactive({
                    'lessor_application': [],
                    'com_rent': [],
                    'com_manage': [],
                    'identity': [],
                    'building': [],
                    'house_tax': [],
                    'land_tax': [],
                    'water_fee': [],
                    'ele_fee': [],
                    'bank_account': [],
                    'house_checklist': [],
                    'furniture_checklist': [],
                    'picture': [],
                    'other': []
                });
                const temp_file = reactive({});
                const file_json = ref(null);
                const upload_file = (e, name) => {
                    temp_file[name] = e.target.files[0].name;

                }
                const add_file = (name) => {
                    if (temp_file[name] != null) {
                        file_arr[name].push(temp_file[name]);
                        file_json.value = JSON.stringify(file_arr);
                        alert('上傳成功');
                    }
                }

                return {
                    twZip,
                    residenceTwZip,
                    locatedTwZip,
                    dbData,
                    address_city_active,
                    address_cityarea_active,
                    address_street_active,
                    residence_address_city_active,
                    residence_address_cityarea_active,
                    residence_address_street_active,
                    located_city_active,
                    located_cityarea_active,
                    bankCode,
                    agent_bank_name_active,
                    agent_branch_name,
                    Lessor_phone,
                    cellphone,
                    Lessor_ID_num,
                    agent_ID_num,
                    inputFocus,
                    upload_file,
                    file_json,
                    add_file,
                    located_land_num,
                    located_build_num,
                };
            }
        }

        Vue.createApp(App).mount("#app");
    </script>
@endpush

@push('scripts')
    @once
        <script src="/js/bootstrap-datepicker.js"></script>
        <script src="/js/bootstrap-datepicker.zh-TW.min.js"></script>
        <script>
            console.log("哈哈哈哈哈");
            var remark_1_set = "{{@$buildingManagement}}";
            if (remark_1_set) {
                $("#remark_1").datepicker({
                    format: "twy-mm-dd",
                    language: "zh-TW",
                    showOnFocus: false,
                    todayHighlight: true
                }).on("show", function(e) {
                    $("div.box").css({
                        minHeight: "480px"
                    });
                }).on("hide", function(e) {
                    $("div.box").css({
                        minHeight: "auto"
                    });
                });
            }else{
                var dateNative = new Date(),
                    dateTW = new Date(
                        dateNative.getFullYear() - 1911,
                        dateNative.getMonth(),
                        dateNative.getDate()
                    );
                $("#remark_1").datepicker( "setDate" , dateTW);
            }
        </script>
    @endonce
@endpush


@push('scripts')
    @once
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
        <script>
            (function() {


                var yearTextSelector = '.ui-datepicker-year';

                var dateNative = new Date(),
                    dateTW = new Date(
                        dateNative.getFullYear() - 1911,
                        dateNative.getMonth(),
                        dateNative.getDate()
                    );


                function leftPad(val, length) {
                    var str = '' + val;
                    while (str.length < length) {
                        str = '0' + str;
                    }
                    return str;
                }

                // 應該有更好的做法
                var funcColle = {
                    onSelect: {
                        basic: function(dateText, inst) {
                            /*
                            var yearNative = inst.selectedYear < 1911
                                ? inst.selectedYear + 1911 : inst.selectedYear;*/
                            dateNative = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

                            // 年分小於100會被補成19**, 要做例外處理
                            var yearTW = inst.selectedYear > 1911 ?
                                leftPad(inst.selectedYear - 1911, 4) :
                                inst.selectedYear;
                            var monthTW = leftPad(inst.selectedMonth + 1, 2);
                            var dayTW = leftPad(inst.selectedDay, 2);
                            console.log(monthTW);
                            dateTW = new Date(
                                yearTW + '-' +
                                monthTW + '-' +
                                dayTW + 'T00:00:00.000Z'
                            );
                            console.log(dateTW);
                            return $.datepicker.formatDate(twSettings.dateFormat, dateTW);
                        }
                    }
                };

                var twSettings = {
                    closeText: '關閉',
                    prevText: '上個月',
                    nextText: '下個月',
                    currentText: '今天',
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                    dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                    dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                    weekHeader: '周',
                    dateFormat: 'yy/mm/dd',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: true,
                    yearSuffix: '年',
                    yearRange: "1950:2200",

                    onSelect: function(dateText, inst) {
                        $(this).val(funcColle.onSelect.basic(dateText, inst));
                        if (typeof funcColle.onSelect.newFunc === 'function') {
                            funcColle.onSelect.newFunc(dateText, inst);
                        }
                    }
                };

                // 把yearText換成民國
                var replaceYearText = function() {
                    var $yearText = $('.ui-datepicker-year');

                    if (twSettings.changeYear !== true) {
                        $yearText.text('民國' + dateTW.getFullYear());
                    } else {
                        // 下拉選單
                        if ($yearText.prev('span.datepickerTW-yearPrefix').length === 0) {
                            $yearText.before("<span class='datepickerTW-yearPrefix'>民國</span>");
                        }
                        $yearText.children().each(function() {
                            if (parseInt($(this).text()) > 1911) {
                                $(this).text(parseInt($(this).text()) - 1911);
                            }
                        });
                    }
                };

                $.fn.datepickerTW = function(options) {

                    // setting on init,
                    if (typeof options === 'object') {
                        //onSelect例外處理, 避免覆蓋
                        if (typeof options.onSelect === 'function') {
                            funcColle.onSelect.newFunc = options.onSelect;
                            options.onSelect = twSettings.onSelect;
                        }
                        // year range正規化成西元, 小於1911的數字都會被當成民國年
                        if (options.yearRange) {
                            var temp = options.yearRange.split(':');
                            for (var i = 0; i < temp.length; i += 1) {
                                //民國前處理
                                if (parseInt(temp[i]) < 1) {
                                    temp[i] = parseInt(temp[i]) + 1911;
                                } else {
                                    temp[i] = parseInt(temp[i]) < 1911 ?
                                        parseInt(temp[i]) + 1911 :
                                        temp[i];
                                }
                            }
                            options.yearRange = temp[0] + ':' + temp[1];
                        }
                        // if input val not empty
                        if ($(this).val() !== '') {
                            options.defaultDate = $(this).val();
                        }
                    }

                    // setting after init
                    if (arguments.length > 1) {
                        // 目前還沒想到正常的解法, 先用轉換成init setting obj的形式
                        if (arguments[0] === 'option') {
                            options = {};
                            options[arguments[1]] = arguments[2];
                        }
                    }

                    // override settings
                    $.extend(twSettings, options);

                    // init
                    $(this).datepicker(twSettings);

                    // beforeRender
                    $(this).click(function() {
                        var isFirstTime = ($(this).val() === '');

                        // year range and default date

                        if ((twSettings.defaultDate || twSettings.yearRange) && isFirstTime) {

                            if (twSettings.defaultDate) {
                                $(this).datepicker('setDate', twSettings.defaultDate);
                            }

                            // 當有year range時, select初始化設成range的最末年
                            if (twSettings.yearRange) {
                                var $yearSelect = $('.ui-datepicker-year'),
                                    nowYear = twSettings.defaultDate ?
                                    $(this).datepicker('getDate').getFullYear() :
                                    dateNative.getFullYear();

                                $yearSelect.children(':selected').removeAttr('selected');
                                if ($yearSelect.children('[value=' + nowYear + ']').length > 0) {
                                    $yearSelect.children('[value=' + nowYear + ']').attr('selected',
                                        'selected');
                                } else {
                                    $yearSelect.children().last().attr('selected', 'selected');
                                }
                            }
                        } else {
                            @if (!isset($buildingManagement))
                                $(this).datepicker('setDate', dateNative);
                            @endif
                        }
                        @if (!isset($buildingManagement))
                            $(this).val($.datepicker.formatDate(twSettings.dateFormat, dateTW));
                        @endif

                        replaceYearText();

                        if (isFirstTime) {
                            $(this).val('');
                        }
                    });

                    // afterRender
                    $(this).focus(function() {
                        replaceYearText();
                    });

                    return this;
                };
                console.log(123);
                @if (isset($show))
                    $("input").attr("disabled", true);
                    $("select").attr("disabled", true);
                @endif

            })();


            $('.datepickerTW').datepickerTW({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '-10:2018',
                // defaultDate: '86-11-01',

            });
            $('.datepicker').datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '1911:2018',
                // defaultDate: '2016-11-01'
            });

            $('#sublet').change(function() {
                if ($(this).val() === '0') {
                    $('#rent').val(1)
                    $('#rent').attr('disabled', true);
                } else {
                    $('#rent').val(0)
                    $('#rent').attr('disabled', true);
                }
            })
            $('#rent').change(function() {
                if ($(this).val() === '0') {
                    $('#sublet').val(1)
                    $('#sublet').attr('disabled', true);
                } else {
                    $('#sublet').val(0)
                    $('#sublet').attr('disabled', true);
                }
            })

            $(".disabled").prop('disabled', true); //disable
            $(".disabled").css("color", "#000");
            $("#same_address").click(function() {
                if ($("#same_address").prop("checked")) {
                    // $(".residence_address_area").css("display", "none");
                    $("select[name='residence_address_city']").val($(
                        "select[name='address_city']").val());
                    let residence_address_cityarea = $("select[name='address_cityarea']").val();
                    $("select[name='residence_address_cityarea").html(
                        `<option value="${residence_address_cityarea}">${residence_address_cityarea}</option>`)
                    $("select[name='residence_address_cityarea']").val($(
                        "select[name='address_cityarea']").val());
                    $("input[name='residence_address_street']").val($(
                        "input[name='address_street']").val());
                    $("input[name='residence_address_ln']").val($(
                        "input[name='address_ln']").val());
                    $("input[name='residence_address_aly']").val($(
                        "input[name='address_aly']").val());
                    $("input[name='residence_address_num']").val($(
                        "input[name='address_num']").val());
                    $("input[name='residence_address_num_hyphen']").val($(
                        "input[name='address_num_hyphen']").val());
                } else {
                    $(".residence_address_area").css("display", "block");
                }
            });
            $('.maxlength_10').attr('maxlength', 10);
            $("#pay_by_tenant").click(function() {
                if ($("#pay_by_tenant").prop('checked')) {
                    $("input[name='manage_fee_month']").val(0);
                }
            });
        </script>
    @endonce
@endpush
