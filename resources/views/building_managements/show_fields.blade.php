<div class="float_modal">
    {!! Form::model($buildingManagement, ['route' => ['buildingManagements.update', $buildingManagement->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

    @include('building_managements.fields')

    {!! Form::close() !!}
</div>
@push('scripts')
<script type="text/javascript">
    $(".float_modal input,.float_modal select").attr('readonly',1).attr('disabled','disabled')
</script>
@endpush