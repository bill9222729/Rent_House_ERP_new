<div class="row">
    <!-- Name Field -->
    <div class="form-group col-12">
        {!! Form::label('name', __('models/businessManagements.fields.name') . ':') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- 英文名字 -->
    <div class="form-group col-12">
        {!! Form::label('english_name', '英文名字: ') !!}
        {!! Form::text('english_name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- 到值日期 -->
    <div class="form-group col-12">
        {!! Form::label('due_date', '到值日期: ') !!}
        @isset($businessManagement)
            {!! Form::text('due_date', (int) date('Y', strtotime($businessManagement->birthday)) - 1911 . date('-m-d', strtotime($businessManagement->birthday)), ['class' => 'form-control datepickerTW']) !!}
        @else
            {!! Form::text('due_date', null, ['class' => 'form-control datepickerTW']) !!}
        @endisset
    </div>

    <!-- 戶籍地址 -->
    <div class="form-group col-12">
        {!! Form::label('r_address', '戶籍地址: ') !!}
        {!! Form::text('r_address', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Address Field -->
    <div class="form-group col-12">
        {!! Form::label('address', __('models/businessManagements.fields.address') . ':') !!}
        {!! Form::text('address', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Phone Field -->
    <div class="form-group col-12">
        {!! Form::label('phone', __('models/businessManagements.fields.phone') . ':') !!}
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>

    <!-- 手機 -->
    <div class="form-group col-12">
        {!! Form::label('cellphone', '手機') !!}
        {!! Form::text('cellphone', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Id Number Field -->
    <div class="form-group col-12">
        {!! Form::label('id_number', __('models/businessManagements.fields.id_number') . ':') !!}
        {!! Form::text('id_number', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Birthday Field -->
    <div class="form-group col-12">
        {!! Form::label('birthday', __('models/businessManagements.fields.birthday') . ':') !!}
        @isset($businessManagement)
            {!! Form::text('birthday', (int) date('Y', strtotime($businessManagement->birthday)) - 1911 . date('-m-d', strtotime($businessManagement->birthday)), ['class' => 'form-control datepickerTW']) !!}
        @else
            {!! Form::text('birthday', null, ['class' => 'form-control datepickerTW']) !!}
        @endisset
    </div>

    <!-- Bank Account Field -->
    <div class="form-group col-12">
        {!! Form::label('bank_account', __('models/businessManagements.fields.bank_account') . ':') !!}
        {!! Form::text('bank_account', null, ['class' => 'form-control', 'maxlength' => '14', 'minlength' => '12']) !!}
    </div>


    <!-- Management Fee Set Field -->
    <div class="form-group col-12">
        {!! Form::label('management_fee_set', __('models/businessManagements.fields.management_fee_set') . ':') !!}
        {!! Form::text('management_fee_set', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Match Fee Set Field -->
    <div class="form-group col-12">
        {!! Form::label('match_fee_set', __('models/businessManagements.fields.match_fee_set') . ':') !!}
        {!! Form::text('match_fee_set', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Escrow Quota Field -->
    <div class="form-group col-12">
        {!! Form::label('escrow_quota', __('models/businessManagements.fields.escrow_quota') . ':') !!}
        {!! Form::number('escrow_quota', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Box Quota Field -->
    <div class="form-group col-12">
        {!! Form::label('box_quota', __('models/businessManagements.fields.box_quota') . ':') !!}
        {!! Form::number('box_quota', null, ['class' => 'form-control']) !!}
    </div>


    <!-- 勞保退休金額 -->
    <div class="form-group col-12">
        {!! Form::label('labor', '勞保退休金額:') !!}
        {!! Form::number('labor', null, ['class' => 'form-control']) !!}
    </div>

    <!-- 眷屬名稱 -->
    <div class="form-group col-12 row">
        {!! Form::label('family', '眷屬名稱:', ['class' => 'form-group col-12']) !!}
        <div class="form-group col-3">
            {!! Form::label('f_name', '姓名:') !!}
            {!! Form::text('f_name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-3">
            {!! Form::label('f_title', '稱謂:') !!}
            {!! Form::text('f_title', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-3">
            {!! Form::label('f_identity', '身分證字號:') !!}
            {!! Form::text('f_identity', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-3">
            {!! Form::label('f_birthday', '出生年月日:') !!}
            @isset($businessManagement)
                {!! Form::text('f_birthday', (int) date('Y', strtotime($businessManagement->birthday)) - 1911 . date('-m-d', strtotime($businessManagement->birthday)), ['class' => 'form-control datepickerTW']) !!}
            @else
                {!! Form::text('f_birthday', null, ['class' => 'form-control datepickerTW']) !!}
            @endisset
        </div>
    </div>


    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('businessManagements.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
    </div>
</div>

@push('scripts')
    @once
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
        <script>
            (function() {

                var yearTextSelector = '.ui-datepicker-year';

                var dateNative = new Date(),
                    dateTW = new Date(
                        dateNative.getFullYear() - 1911,
                        dateNative.getMonth(),
                        dateNative.getDate()
                    );


                function leftPad(val, length) {
                    var str = '' + val;
                    while (str.length < length) {
                        str = '0' + str;
                    }
                    return str;
                }

                // 應該有更好的做法
                var funcColle = {
                    onSelect: {
                        basic: function(dateText, inst) {
                            /*
                            var yearNative = inst.selectedYear < 1911
                                ? inst.selectedYear + 1911 : inst.selectedYear;*/
                            dateNative = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

                            // 年分小於100會被補成19**, 要做例外處理
                            var yearTW = inst.selectedYear > 1911 ?
                                leftPad(inst.selectedYear - 1911, 4) :
                                inst.selectedYear;
                            var monthTW = leftPad(inst.selectedMonth + 1, 2);
                            var dayTW = leftPad(inst.selectedDay, 2);
                            console.log(monthTW);
                            dateTW = new Date(
                                yearTW + '-' +
                                monthTW + '-' +
                                dayTW + 'T00:00:00.000Z'
                            );
                            console.log(dateTW);
                            return $.datepicker.formatDate(twSettings.dateFormat, dateTW);
                        }
                    }
                };

                var twSettings = {
                    closeText: '關閉',
                    prevText: '上個月',
                    nextText: '下個月',
                    currentText: '今天',
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                    dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                    dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                    weekHeader: '周',
                    dateFormat: 'yy/mm/dd',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: true,
                    yearSuffix: '年',

                    onSelect: function(dateText, inst) {
                        $(this).val(funcColle.onSelect.basic(dateText, inst));
                        if (typeof funcColle.onSelect.newFunc === 'function') {
                            funcColle.onSelect.newFunc(dateText, inst);
                        }
                    }
                };

                // 把yearText換成民國
                var replaceYearText = function() {
                    var $yearText = $('.ui-datepicker-year');

                    if (twSettings.changeYear !== true) {
                        $yearText.text('民國' + dateTW.getFullYear());
                    } else {
                        // 下拉選單
                        if ($yearText.prev('span.datepickerTW-yearPrefix').length === 0) {
                            $yearText.before("<span class='datepickerTW-yearPrefix'>民國</span>");
                        }
                        $yearText.children().each(function() {
                            if (parseInt($(this).text()) > 1911) {
                                $(this).text(parseInt($(this).text()) - 1911);
                            }
                        });
                    }
                };

                $.fn.datepickerTW = function(options) {

                    // setting on init,
                    if (typeof options === 'object') {
                        //onSelect例外處理, 避免覆蓋
                        if (typeof options.onSelect === 'function') {
                            funcColle.onSelect.newFunc = options.onSelect;
                            options.onSelect = twSettings.onSelect;
                        }
                        // year range正規化成西元, 小於1911的數字都會被當成民國年
                        if (options.yearRange) {
                            var temp = options.yearRange.split(':');
                            for (var i = 0; i < temp.length; i += 1) {
                                //民國前處理
                                if (parseInt(temp[i]) < 1) {
                                    temp[i] = parseInt(temp[i]) + 1911;
                                } else {
                                    temp[i] = parseInt(temp[i]) < 1911 ?
                                        parseInt(temp[i]) + 1911 :
                                        temp[i];
                                }
                            }
                            options.yearRange = temp[0] + ':' + temp[1];
                        }
                        // if input val not empty
                        if ($(this).val() !== '') {
                            options.defaultDate = $(this).val();
                        }
                    }

                    // setting after init
                    if (arguments.length > 1) {
                        // 目前還沒想到正常的解法, 先用轉換成init setting obj的形式
                        if (arguments[0] === 'option') {
                            options = {};
                            options[arguments[1]] = arguments[2];
                        }
                    }

                    // override settings
                    $.extend(twSettings, options);

                    // init
                    $(this).datepicker(twSettings);

                    // beforeRender
                    $(this).click(function() {
                        var isFirstTime = ($(this).val() === '');

                        // year range and default date

                        if ((twSettings.defaultDate || twSettings.yearRange) && isFirstTime) {

                            if (twSettings.defaultDate) {
                                $(this).datepicker('setDate', twSettings.defaultDate);
                            }

                            // 當有year range時, select初始化設成range的最末年
                            if (twSettings.yearRange) {
                                var $yearSelect = $('.ui-datepicker-year'),
                                    nowYear = twSettings.defaultDate ?
                                    $(this).datepicker('getDate').getFullYear() :
                                    dateNative.getFullYear();

                                $yearSelect.children(':selected').removeAttr('selected');
                                if ($yearSelect.children('[value=' + nowYear + ']').length > 0) {
                                    $yearSelect.children('[value=' + nowYear + ']').attr('selected',
                                        'selected');
                                } else {
                                    $yearSelect.children().last().attr('selected', 'selected');
                                }
                            }
                        } else {
                            $(this).datepicker('setDate', dateNative);
                        }

                        $(this).val($.datepicker.formatDate(twSettings.dateFormat, dateTW));

                        replaceYearText();

                        if (isFirstTime) {
                            $(this).val('');
                        }
                    });

                    // afterRender
                    $(this).focus(function() {
                        replaceYearText();
                    });

                    return this;
                };

            })();


            $('.datepickerTW').datepickerTW({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '-10:2018',
                // defaultDate: '86-11-01',

            });
            $('.datepicker').datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '1911:2018',
                // defaultDate: '2016-11-01'
            });
        </script>
    @endonce
@endpush
