<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/businessManagements.fields.name').':') !!}
    <p>{{ $businessManagement->name }}</p>
</div>


<!-- Address Field -->
<div class="form-group col-12">
    {!! Form::label('address', __('models/businessManagements.fields.address').':') !!}
    <p>{{ $businessManagement->address }}</p>
</div>


<!-- Phone Field -->
<div class="form-group col-12">
    {!! Form::label('phone', __('models/businessManagements.fields.phone').':') !!}
    <p>{{ $businessManagement->phone }}</p>
</div>


<!-- Id Number Field -->
<div class="form-group col-12">
    {!! Form::label('id_number', __('models/businessManagements.fields.id_number').':') !!}
    <p>{{ $businessManagement->id_number }}</p>
</div>


<!-- Birthday Field -->
<div class="form-group col-12">
    {!! Form::label('birthday', __('models/businessManagements.fields.birthday').':') !!}
    <p>{{ $businessManagement->birthday }}</p>
</div>


<!-- Bank Account Field -->
<div class="form-group col-12">
    {!! Form::label('bank_account', __('models/businessManagements.fields.bank_account').':') !!}
    <p>{{ $businessManagement->bank_account }}</p>
</div>


<!-- Management Fee Set Field -->
<div class="form-group col-12">
    {!! Form::label('management_fee_set', __('models/businessManagements.fields.management_fee_set').':') !!}
    <p>{{ $businessManagement->management_fee_set }}</p>
</div>


<!-- Escrow Quota Field -->
<div class="form-group col-12">
    {!! Form::label('escrow_quota', __('models/businessManagements.fields.escrow_quota').':') !!}
    <p>{{ $businessManagement->escrow_quota }}</p>
</div>


<!-- Box Quota Field -->
<div class="form-group col-12">
    {!! Form::label('box_quota', __('models/businessManagements.fields.box_quota').':') !!}
    <p>{{ $businessManagement->box_quota }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-12">
    {!! Form::label('created_at', __('models/businessManagements.fields.created_at').':') !!}
    <p>{{ $businessManagement->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-12">
    {!! Form::label('updated_at', __('models/businessManagements.fields.updated_at').':') !!}
    <p>{{ $businessManagement->updated_at }}</p>
</div>


