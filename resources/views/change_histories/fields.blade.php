<div class="row">
<!-- Building Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_id', __('models/changeHistories.fields.building_id').':') !!}
    {!! Form::text('building_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Apply Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apply_date', __('models/changeHistories.fields.apply_date').':') !!}
    {!! Form::date('apply_date', null, ['class' => 'form-control','id'=>'apply_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#apply_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Change Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('change_date', __('models/changeHistories.fields.change_date').':') !!}
    {!! Form::date('change_date', null, ['class' => 'form-control','id'=>'change_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#change_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Lessor Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lessor_type', __('models/changeHistories.fields.lessor_type').':') !!}
    {!! Form::text('lessor_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/changeHistories.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', __('models/changeHistories.fields.phone').':') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Reason Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reason', __('models/changeHistories.fields.reason').':') !!}
    {!! Form::text('reason', null, ['class' => 'form-control']) !!}
</div>

<!-- Change Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('change_note', __('models/changeHistories.fields.change_note').':') !!}
    {!! Form::text('change_note', null, ['class' => 'form-control']) !!}
</div>

<!-- Edit Member Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('edit_member_id', __('models/changeHistories.fields.edit_member_id').':') !!}
    {!! Form::number('edit_member_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
