<!-- Building Id Field -->
<div class="form-group col-12">
    {!! Form::label('building_id', __('models/changeHistories.fields.building_id').':') !!}
    <p>{{ $changeHistory->building_id }}</p>
</div>


<!-- Apply Date Field -->
<div class="form-group col-12">
    {!! Form::label('apply_date', __('models/changeHistories.fields.apply_date').':') !!}
    <p>{{ $changeHistory->apply_date }}</p>
</div>


<!-- Change Date Field -->
<div class="form-group col-12">
    {!! Form::label('change_date', __('models/changeHistories.fields.change_date').':') !!}
    <p>{{ $changeHistory->change_date }}</p>
</div>


<!-- Lessor Type Field -->
<div class="form-group col-12">
    {!! Form::label('lessor_type', __('models/changeHistories.fields.lessor_type').':') !!}
    <p>{{ $changeHistory->lessor_type }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/changeHistories.fields.name').':') !!}
    <p>{{ $changeHistory->name }}</p>
</div>


<!-- Phone Field -->
<div class="form-group col-12">
    {!! Form::label('phone', __('models/changeHistories.fields.phone').':') !!}
    <p>{{ $changeHistory->phone }}</p>
</div>


<!-- Reason Field -->
<div class="form-group col-12">
    {!! Form::label('reason', __('models/changeHistories.fields.reason').':') !!}
    <p>{{ $changeHistory->reason }}</p>
</div>


<!-- Change Note Field -->
<div class="form-group col-12">
    {!! Form::label('change_note', __('models/changeHistories.fields.change_note').':') !!}
    <p>{{ $changeHistory->change_note }}</p>
</div>


<!-- Edit Member Id Field -->
<div class="form-group col-12">
    {!! Form::label('edit_member_id', __('models/changeHistories.fields.edit_member_id').':') !!}
    <p>{{ $changeHistory->edit_member_id }}</p>
</div>


