<!-- Value Field -->
<div class="form-group col-12">
    {!! Form::label('value', __('models/cities.fields.value').':') !!}
    <p>{{ $city->value }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/cities.fields.name').':') !!}
    <p>{{ $city->name }}</p>
</div>


