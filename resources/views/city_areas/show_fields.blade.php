<!-- City Value Field -->
<div class="form-group col-12">
    {!! Form::label('city_value', __('models/cityAreas.fields.city_value').':') !!}
    <p>{{ $cityArea->city_value }}</p>
</div>


<!-- Value Field -->
<div class="form-group col-12">
    {!! Form::label('value', __('models/cityAreas.fields.value').':') !!}
    <p>{{ $cityArea->value }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/cityAreas.fields.name').':') !!}
    <p>{{ $cityArea->name }}</p>
</div>


