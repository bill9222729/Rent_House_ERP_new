@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

        .input-group label.error {
            position: absolute;
            bottom: -1rem;
        }
    </style>
@endsection

@section('content')
    <form id="contract_create_form">
        <div class="container-fluid container-fixed-lg">
            <h3 class="page-title">
                @if ($matchManagement['contract_type'] == '代管約')
                    新增代管約
                @endif
                @if ($matchManagement['contract_type'] == '包租約')
                    新增包租約
                @endif
                @if ($matchManagement['contract_type'] == '轉租約')
                    新增轉租約
                @endif
            </h3>
        </div>

        <div class="container-fluid container-fixed-lg">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                @if ($matchManagement['contract_type'] == '代管約')
                    @include('contract_managements.escrow_fields')
                @endif
                @if ($matchManagement['contract_type'] == '包租約')
                    @include('contract_managements.charter_fields')
                @endif
                @if ($matchManagement['contract_type'] == '轉租約')
                    @include('contract_managements.sublease_fields')
                @endif
            </div>
        </div>

        <div class="container-fluid py-5">
            <div class="row justify-content-center">
                <div class="col-sm-1">
                    @if (app('request')->input('renew'))
                        <button type="submit" class="btn btn-primary" id="renew_button">續約</button>
                    @else
                        <button type="submit" class="btn btn-primary" id="save_button">儲存</button>
                    @endif
                </div>
                <div class="col-sm-1">
                    <a href="javascript:history.go(-1)" class="btn btn-secondary">取消</a>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/contract_management.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        // 取得頁面資料
        const matchManagementData = @json($matchManagement);
        // 取得是不是續約頁面
        const isRenew = {{ app('request')->input('renew') ? app('request')->input('renew') : 'false' }};
        if (isRenew) {
            document.getElementById("match_id").removeAttribute("readonly");
            $("#match_id").removeClass("form-control-plaintext");
            $("#match_id").addClass("form-control");
            // 依照媒合編號填入合約欄位
            fillContractFieldsByMatchId(matchManagementData.id);

        }
        // 打api新增match_management的資料
        async function createMatchManagementData(requestData, alertConfig = null) {
            retVal = "";
            await $.ajax({
                method: "POST",
                url: "/api/match_managements",
                data: requestData,
                dataType: "json",
                success: function(data) {
                    console.log("成功新增match_data：");
                    console.log(data);
                    retVal = data;
                },
                error: function(err) {
                    console.error(err);
                }
            });
            return retVal;
        }
        // 取得畫面上需要更新到match_management的資料
        function getMatchManagementData() {
            let matchRequestData = @json($matchManagement);
            matchRequestData["match_id"] = $("#match_id").val();
            matchRequestData["lease_start_date"] = $("#lease_start_date").val() ?
                convertToCEDate($("#lease_start_date").val()) : "";
            matchRequestData["lease_end_date"] = $("#lease_end_date").val() ?
                convertToCEDate($("#lease_end_date").val()) : "";
            matchRequestData["contract_stop_date"] = null;
            matchRequestData["contract_stop_reason"] = null;
            matchRequestData["tenant_case_id"] = $("#tenant_case_id").val() || matchRequestData["tenant_case_id"];
            return matchRequestData;
        }
        // 續約按鈕
        const handleRenewButtonClick = async () => {
            console.log(matchManagementData);
            let requestData = getContractDataInPage();
            let newMatchData = await createMatchManagementData(getMatchManagementData());
            requestData["match_id"] = newMatchData.data["id"];
            createContractManagementsData(
                requestData, {
                    hasAlert: true,
                    title: "成功",
                    text: "續約資料已成功新增",
                    type: "success",
                    callback: (result) => {
                        if (result.isConfirmed) {
                            window.location.href =
                                `/matchManagements/${newMatchData.data['id']}/edit`;
                        }
                    }
                });
        }
        // 儲存事件
        const saveContractData = () => {
            let requestData = getContractDataInPage();
            requestData["match_id"] = matchManagementData["id"];

            let matchRequestData = {
                "lease_start_date": $("#lease_start_date").val() ?
                    convertToCEDate($("#lease_start_date").val()) : "",
                "lease_end_date": $("#lease_end_date").val() ?
                    convertToCEDate($("#lease_end_date").val()) : "",
            }
            console.log(requestData);

            // 更新match_management表
            $.ajax({
                method: "PATCH",
                url: `https://rent-house-erp.echouse.co/api/match_managements/${matchManagementData.id}`,
                data: matchRequestData,
                dataType: "json",
                success: function(data) {
                    console.log("match_managements資料已更新");
                },
                error: function(err) {
                    console.error(err);
                }
            });

            // 更新contract_managements表
            $.ajax({
                method: "POST",
                url: "https://rent-house-erp.echouse.co/api/contract_managements",
                data: requestData,
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    Swal.fire(
                        '成功',
                        '資料已成功新增',
                        'success'
                    ).then((result) => {
                        if (result.isConfirmed) {
                            window.location.href =
                                `/matchManagements/${matchManagementData['id']}/edit`;
                        }
                    })
                },
                error: function(err) {
                    console.error(err);
                }
            });
        }

        // 取得包租約資料
        async function getCharterData(match_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/match_managements?id=${match_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }
        $(document).ready(async function() {
            // 先用ajax取得資料
            console.log(matchManagementData);
            const charterData = await getCharterData(matchManagementData["charter_case_id"]);
            const buildingData = await getBuildingData(matchManagementData["contract_type"] === "轉租約" ?
                charterData['building_case_id'] : matchManagementData['building_case_id']);
            const tenantData = await getTenantData(matchManagementData['tenant_case_id']);
            console.log(matchManagementData);
            console.log(tenantData);
            console.log(buildingData);
            // 拼湊門牌資料
            let address_ln = buildingData['building_address_ln'] ? `${buildingData['building_address_ln']}巷` :
                "";
            let address_aly = buildingData['building_address_aly'] ?
                `${buildingData['building_address_aly']}弄` : "";
            let address_num_hyphen = buildingData['building_address_num_hyphen'] ?
                `之${buildingData['building_address_num_hyphen']}` : "";
            let address_floor = buildingData['building_address_floor'] ?
                `${buildingData['building_address_floor']}樓` : "";
            let address_floor_sub = buildingData['building_address_floor_sub'] ?
                `之${buildingData['building_address_floor_sub']}` : "";
            let address_room_num = buildingData['building_address_room_num'] ?
                `${buildingData['building_address_room_num']}室` : "";
            // 填入欄位
            $("#match_id").val(matchManagementData["match_id"]);
            console.log(buildingData['building_address_ln']);
            $("#building_address").val(
                `${buildingData['building_address_city']?buildingData['building_address_city']:""}${buildingData['building_address_city_area_name']?buildingData['building_address_city_area_name']:""}${buildingData['building_address_street']?buildingData['building_address_street']:""}${address_ln}${address_aly}${buildingData['building_address_num']?buildingData['building_address_num']:""}${address_num_hyphen}號${address_floor}${address_floor_sub}${address_room_num}`
            );
            $("#building_located").val(
                `${buildingData['building_located_city']?buildingData['building_located_city']:""}${buildingData['building_located_city_area_name']?buildingData['building_located_city_area_name']:""}${buildingData['building_located_lot']?buildingData['building_located_lot']:""}${buildingData['building_located_land_num'] ? buildingData['building_located_land_num']+"地號" : ""}`
            );
            $("#building_num").val(buildingData['building_num']);
            $("#tenant_name").val(`承租人：${tenantData ? tenantData['name'] : ""}(乙方)`);
            $("#lessor_name").val(`出租人：${buildingData['name']}(甲方)`);
            $("#membtype").val(tenantData ? tenantData['membtype'] : "");
            if (matchManagementData.contract_type === "代管約") {}
            if (matchManagementData.contract_type === "包租約") {
                $("#tenant_name").val(`承租人：易居管理顧問股份有限公司(乙方)`);
            }
            if (matchManagementData.contract_type === "轉租約") {
                $("#lessor_name").val(`出租人：易居管理顧問股份有限公司(甲方)`);
            }
            $("#lease_start_date").val(matchManagementData.lease_start_date ? convertToRCDate(
                matchManagementData.lease_start_date) : "");
            $("#lease_end_date").val(matchManagementData.lease_end_date ? convertToRCDate(matchManagementData
                .lease_end_date) : "");
            $("#membtype").text(tenantData["membtype"] ? tenantData["membtype"] : "無資料"); // 弱勢資格
            $("#tenant_case_id").val(tenantData ? tenantData["government_no"] : ""); // 租客內政部編號
            // 初始化日期選擇器
            $('.input-group.date').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });

            // 表單驗證
            @if (app('request')->input('renew'))
                addFormValid("contract_create_form", handleRenewButtonClick);
            @else
                addFormValid("contract_create_form", saveContractData);
            @endif
        });
    </script>
@endpush
