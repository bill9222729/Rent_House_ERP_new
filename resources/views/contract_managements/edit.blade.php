@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

        .input-group label.error {
            position: absolute;
            bottom: -1rem;
        }
    </style>
@endsection

@section('content')
    <form id="contract_edit_form">
        @include('adminlte-templates::common.errors')
        <div class="container-fluid container-fixed-lg">
            <h3 class="page-title">
                @if ($matchManagement['contract_type'] == '代管約')
                    代管約
                @endif
                @if ($matchManagement['contract_type'] == '包租約')
                    包租約
                @endif
                @if ($matchManagement['contract_type'] == '轉租約')
                    轉租約
                @endif
            </h3>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                @if ($matchManagement['contract_type'] == '代管約')
                    @include('contract_managements.escrow_fields')
                @endif
                @if ($matchManagement['contract_type'] == '包租約')
                    @include('contract_managements.charter_fields')
                @endif
                @if ($matchManagement['contract_type'] == '轉租約')
                    @include('contract_managements.sublease_fields')
                @endif
            </div>
        </div>
        <div class="container-fluid container-fixed-lg py-5">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary" id="save_button">更新</button>
                </div>
                @canany(['staff', 'admin'])
                    @if ($contractManagement->data_status != 'admin')
                        <div class="col-auto">
                            <button type="button" class="btn btn-success text-nowrap" onclick="changeDataStatusToAdmin()">送出資料</button>
                        </div>
                    @endif
                @endcan
                @can('admin')
                    @if ($contractManagement->data_status == 'admin')
                        <div class="col-auto">
                            <button type="button" class="btn btn-danger text-nowrap" onclick="changeDataStatusToStaff()">退回資料</button>
                        </div>
                    @endif
                @endcan
                <div class="col-auto">
                    <button type="submit" class="btn btn-secondary" onclick="javascript:history.go(-1)">取消</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/contract_management.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const matchManagementData = @json($matchManagement);
        const contractManagement = @json($contractManagement);
        // 取得建物資料
        async function getBuildingData(building_id) {
            let retVal = "";
            console.log(`/api/building_management_news?id=${building_id}`);
            await $.ajax({
                type: 'GET',
                url: `/api/building_management_news?id=${building_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }
        // 取得租客資料
        async function getTenantData(tenant_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/tenant_management_news?id=${tenant_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }
        // 取得包租約資料
        async function getCharterData(match_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/match_managements?id=${match_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }
        $(document).ready(async function() {
            // 先用ajax取得資料
            const charterData = await getCharterData(matchManagementData["charter_case_id"]);
            const buildingData = await getBuildingData(matchManagementData["contract_type"] === "轉租約" ?
                charterData['building_case_id'] : matchManagementData['building_case_id']);
            const tenantData = await getTenantData(matchManagementData['tenant_case_id']);
            console.log(matchManagementData);
            console.log(contractManagement);
            console.log(charterData);
            console.log(tenantData);
            console.log(buildingData);
            // 拼湊門牌資料
            let address_ln = buildingData['building_address_ln'] ? `${buildingData['building_address_ln']}巷` : "";
            let address_aly = buildingData['building_address_aly'] ? `${buildingData['building_address_aly']}弄` : "";
            let address_num_hyphen = buildingData['building_address_num_hyphen'] ? `之${buildingData['building_address_num_hyphen']}` : "";
            let address_floor = buildingData['building_address_floor'] ? `${buildingData['building_address_floor']}樓` : "";
            let address_floor_sub = buildingData['building_address_floor_sub'] ? `之${buildingData['building_address_floor_sub']}` : "";
            let address_room_num = buildingData['building_address_room_num'] ? `${buildingData['building_address_room_num']}室` : "";
            // 填入欄位
            $("#match_id").val(matchManagementData["match_id"]);
            $("#building_address").val(`${buildingData['building_address_city']?buildingData['building_address_city']:""}${buildingData['building_address_city_area_name']?buildingData['building_address_city_area_name']:""}${buildingData['building_address_street']?buildingData['building_address_street']:""}${address_ln}${address_aly}${buildingData['building_address_num']?buildingData['building_address_num']:""}${address_num_hyphen}號${address_floor}${address_floor_sub}${address_room_num}`);
            $("#building_located").val(`${buildingData['building_located_city']?buildingData['building_located_city']:""}${buildingData['building_located_city_area_name']?buildingData['building_located_city_area_name']:""}${buildingData['building_located_lot']?buildingData['building_located_lot']:""}${buildingData['building_located_land_num'] ? buildingData['building_located_land_num']+"地號" : ""}`);
            $("#building_num").val(buildingData['building_num']);
            $("#tenant_name").val(`承租人：${tenantData ? tenantData['name'] : ""}(乙方)`);
            $("#lessor_name").val(`出租人：${buildingData['name']}(甲方)`);
            $("#membtype").val(tenantData ? tenantData['membtype'] : "");
            $("#signing_date").val(contractManagement["signing_date"] ? convertToRCDate(contractManagement["signing_date"]) : "");
            $("#lease_start_date").val(matchManagementData["lease_start_date"] ? convertToRCDate(matchManagementData["lease_start_date"]) : "");
            $("#lease_end_date").val(matchManagementData["lease_end_date"] ? convertToRCDate(matchManagementData["lease_end_date"]) : "");
            $("#has_tanants").val(contractManagement["has_tanants"] ? contractManagement["has_tanants"] : "0");
            $("#rent").val(contractManagement["rent"] ? contractManagement["rent"] : "0");
            $("#rent_self_pay").val(contractManagement["rent_self_pay"] ? contractManagement["rent_self_pay"] : "0");
            $("#rent_government_pay").val(contractManagement["rent_government_pay"] ? contractManagement["rent_government_pay"] : "0");
            $("#payment_date").val(contractManagement["payment_date"] ? contractManagement["payment_date"] : "未設定");
            $("#rent_pay_type").val(contractManagement["rent_pay_type"] ? contractManagement["rent_pay_type"] : "");
            $("#bank_name").selectpicker('val', contractManagement['bank_name']);
            $("#bank_branch_code").val(contractManagement["bank_branch_code"] ? contractManagement["bank_branch_code"] : "");
            $("#bank_owner_name").val(contractManagement["bank_owner_name"] ? contractManagement["bank_owner_name"] : "");
            $("#bank_account").val(contractManagement["bank_account"] ? contractManagement["bank_account"] : "");
            $("#deposit_amount").val(contractManagement["deposit_amount"] ? contractManagement["deposit_amount"] : "");
            $("#notary_fees").val(contractManagement["notary_fees"] ? contractManagement["notary_fees"] : "0"); // 公證費
            $("#notary_fees_share1").val(contractManagement["notary_fees_share1"] ? contractManagement["notary_fees_share1"] : "0"); // 公證費由出租人負擔
            $("#notary_fees_share2").val(contractManagement["notary_fees_share2"] ? contractManagement["notary_fees_share2"] : "0"); // 公證費由承租人負擔
            $("#development_fee").val(contractManagement["development_fee"] ? contractManagement["development_fee"] : "0"); // 開發費
            $("#guarantee_fee").val(contractManagement["guarantee_fee"] ? contractManagement["guarantee_fee"] : "0"); // 包管費
            $("#matchmaking_fee").val(contractManagement["matchmaking_fee"] ? contractManagement["matchmaking_fee"] : "0"); // 媒合費
            $("#management_fee").val(contractManagement["management_fee"] ? contractManagement["management_fee"] : "0"); // 管理費
            $("#parking_fee").val(contractManagement["parking_fee"] ? contractManagement["parking_fee"] : "0"); // 車位費
            $("#escrow_fee").val(contractManagement["escrow_fee"] ? contractManagement["escrow_fee"] : "0"); // 代管費
            $("#contract_note").val(contractManagement["contract_note"] ? contractManagement["contract_note"] : ""); // 契約備註
            $("#stakeholders").val(contractManagement ? contractManagement["stakeholders"] : ""); // 共同持分人
            $("#membtype").text(tenantData ? tenantData["membtype"] : "無資料"); // 弱勢資格
            $("#information_feed").val(contractManagement["information_feed"] ? contractManagement["information_feed"] : ""); // 資訊提供
            $("#information_feed_date").val(contractManagement["information_feed_date"] ? convertToRCDate(contractManagement["information_feed_date"]) : ""); // 資訊提供日期
            if (matchManagementData.contract_type === "代管約") {}
            if (matchManagementData.contract_type === "包租約") {
                $("#tenant_name").val(`承租人：易居管理顧問股份有限公司(乙方)`);
            }
            if (matchManagementData.contract_type === "轉租約") {
                $("#lessor_name").val(`出租人：易居管理顧問股份有限公司(甲方)`);
            }
            // 初始化日期選擇器
            $('.input-group.date').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });
            // 儲存按鈕
            const handleSaveButtonClick = () => {
                let requestData = {
                    "match_id": matchManagementData['id'],
                    "signing_date": $("#signing_date").val() ?
                        convertToCEDate($("#signing_date").val()) : "",
                    "lease_start_date": $("#lease_start_date").val() ?
                        convertToCEDate($("#lease_start_date").val()) : "",
                    "lease_end_date": $("#lease_end_date").val() ?
                        convertToCEDate($("#lease_end_date").val()) : "",
                    "has_tanants": $("#has_tanants").val() || "0",
                    "rent": $("#rent").val(),
                    "rent_self_pay": $("#rent_self_pay").val(),
                    "rent_government_pay": $("#rent_government_pay").val(),
                    "payment_date": $("#payment_date").val(),
                    "rent_pay_type": $("#rent_pay_type").val(),
                    "bank_name": $("#bank_name").val(),
                    "bank_branch_code": $("#bank_branch_code").val(),
                    "bank_owner_name": $("#bank_owner_name").val(),
                    "bank_account": $("#bank_account").val(),
                    "deposit_amount": $("#deposit_amount").val(),
                    "notary_fees": $("#notary_fees").val(),
                    "notary_fees_share1": $("#notary_fees_share1").val(),
                    "notary_fees_share2": $("#notary_fees_share2").val(),
                    "matchmaking_fee": $("#matchmaking_fee").val(),
                    "escrow_fee": $("#escrow_fee").val(),
                    "management_fee": $("#management_fee").val(),
                    "parking_fee": $("#parking_fee").val(),
                    "development_fee": $("#development_fee").val(),
                    "guarantee_fee": $("#guarantee_fee").val(),
                    "contract_note": $("#contract_note").val(),
                    "stakeholders": $("#stakeholders").val(),
                    "information_feed": $("#information_feed").val(),
                    "information_feed_date": $("#information_feed_date").val() ? convertToCEDate($("#information_feed_date").val()) : "",
                    "net_price_login": $("#net_price_login").val() || "[]",
                }
                $.ajax({
                    method: "PATCH",
                    url: `/api/contract_managements/${contractManagement['id']}`,
                    data: requestData,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已成功更新',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href =
                                    `/matchManagements/${matchManagementData.id}/edit`;
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                });
                $.ajax({
                    method: "PATCH",
                    url: `/api/match_managements/${matchManagementData['id']}`,
                    data: {
                        "lease_start_date": $("#lease_start_date").val() ?
                            convertToCEDate($("#lease_start_date").val()) : "",
                        "lease_end_date": $("#lease_end_date").val() ?
                            convertToCEDate($("#lease_end_date").val()) : "",
                    },
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(err) {
                        console.error(err);
                    }
                });
            }
            // 取得上傳的檔案
            let fileData = await getFileArr(
                "contract_management",
                contractManagement ? contractManagement["id"] : ""
            );
            // 上傳檔案部分初始化
            $("#contract_file_input").fileinput({
                language: "zh-TW",
                uploadUrl: "/api/file_managments",
                maxFileCount: 0,
                maxTotalFileCount: 0,
                maxFileSize: 20000,
                msgSizeTooLarge: `檔案 "{name}" (<b>{size} KB</b>) 大小超過上限 <b>5 MB</b>`,
                uploadExtraData: {
                    file_source_id: contractManagement ?
                        contractManagement["id"] : "",
                    file_source: "contract_management",
                },
                overwriteInitial: false,
                initialPreview: fileData["initialPreview"],
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: "pdf", // image is the default and can be overridden in config below
                initialPreviewConfig: fileData["initialPreviewConfig"],
                ajaxDeleteSettings: {
                    type: "DELETE", // This should override the ajax as $.ajax({ type: 'DELETE' })
                },
            });

            // 如果是檢視模式的話就把所有欄位鎖起來
            let targetUrl = new URL(location.href);
            let cantEdit = targetUrl.searchParams.get("cantedit") === "true";
            if (cantEdit) {
                $("input").attr("readonly", "readonly");
                $("input[type='checkbox']").attr("disabled", true);
                $("input[type='radio']").attr("disabled", true);
                $("select").selectpicker("destroy");
                $("select, button").attr("disabled", "disabled");
                $("textarea").attr("disabled", "disabled");
            }

            // 欄位驗證
            addFormValid("contract_edit_form", handleSaveButtonClick);
        });
    </script>
@endpush
