@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg pb-5">
        @include('adminlte-templates::common.errors')
        <div class="card card-transparent">
            <div class="card-body">
                <h3 class="page-title">
                    新增代管約
                </h3>
                <div class="accordion" id="accordionPanelsStayOpenExample">
                    @include('contract_managements.fields')
                </div>
            </div>
            <div class="card-footer text-muted text-center">
                <a href="javascript:void(0)" onclick="handleSaveButtonClick()" id="save_button"
                    class="btn btn-primary">儲存</a>
                <a href="javascript:history.go(-1)" class="btn btn-secondary">取消</a>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        // 取得建物資料
        async function getBuildingData(building_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/building_management_news?case_no=${building_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }
        // 取得租客資料
        async function getTenantData(tenant_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/tenant_management_news?case_no=${tenant_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }
        $(document).ready(async function() {
            // 先用ajax取得資料
            const matchManagementData = @json($matchManagement);
            const buildingData = await getBuildingData(matchManagementData['building_case_id']);
            const tenantData = await getTenantData(matchManagementData['tenant_case_id']);
            // 拼湊門牌資料
            let address_ln = buildingData['building_located_ln'] ? `${buildingData['building_located_ln']}巷` :
                "";
            let address_aly = buildingData['building_address_aly'] ?
                `${buildingData['building_address_aly']}弄` : "";
            let address_num_hyphen = buildingData['building_address_num_hyphen'] ?
                `之${buildingData['building_address_num_hyphen']}` : "";
            let address_floor = buildingData['building_address_floor'] ?
                `${buildingData['building_address_floor']}樓` : "";
            let address_floor_sub = buildingData['building_address_floor_sub'] ?
                `之${buildingData['building_address_floor_sub']}` : "";
            let address_room_num = buildingData['building_address_room_num'] ?
                `${buildingData['building_address_room_num']}室` : "";
            // 填入欄位
            $("#match_id").val(matchManagementData["match_id"]);
            $("#building_address").val(
                `${buildingData['building_address_city']?buildingData['building_address_city']:""}${buildingData['building_address_city_area_name']?buildingData['building_address_city_area_name']:""}${buildingData['building_address_street']?buildingData['building_address_street']:""}${address_ln}${address_aly}${buildingData['building_address_num']?buildingData['building_address_num']:""}${address_num_hyphen}號${address_floor}${address_floor_sub}${address_room_num}`
            );
            $("#building_located").val(
                `${buildingData['building_located_city']?buildingData['building_located_city']:""}${buildingData['building_located_city_area_name']?buildingData['building_located_city_area_name']:""}${buildingData['building_located_lot']?buildingData['building_located_lot']:""}${buildingData['building_located_land_num'] ? buildingData['building_located_land_num']+"地號" : ""}`
            );
            $("#building_num").val(buildingData['building_num']);
            $("#lessor_name").val(`出租人：${buildingData['name']}(甲方)`);
            $("#tenant_name").val(`承租人：${tenantData['name']}(乙方)`);
            $("#membtype").val(tenantData['membtype']);
            // 初始化日期選擇器
            $('.input-group.date').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });
            // 儲存按鈕
            function handleSaveButtonClick() {
                let requestData = {
                    "match_id": matchManagementData['match_id'],
                    "signing_date": $("#signing_date").val() ?
                        convertToCEDate($("#signing_date").val()) : "",
                    "lease_start_date": $("#lease_start_date").val() ?
                        convertToCEDate($("#lease_start_date").val()) : "",
                    "lease_end_date": $("#lease_end_date").val() ?
                        convertToCEDate($("#lease_end_date").val()) : "",
                    "has_tanants": $("#has_tanants").val() || "0",
                    "rent": $("#rent").val(),
                    "rent_self_pay": $("#rent_self_pay").val(),
                    "rent_government_pay": $("#rent_government_pay").val(),
                    "payment_date": $("#payment_date").val(),
                    "rent_pay_type": $("#rent_pay_type").val(),
                    "bank_name": $("#bank_name").val(),
                    "bank_branch_code": $("#bank_branch_code").val(),
                    "bank_owner_name": $("#bank_owner_name").val(),
                    "bank_account": $("#bank_account").val(),
                    "deposit_amount": $("#deposit_amount").val(),
                    "notary_fees": $("#notary_fees").val(),
                    "notary_fees_share1": $("#notary_fees_share1").val(),
                    "notary_fees_share2": $("#notary_fees_share2").val(),
                    "matchmaking_fee": $("#matchmaking_fee").val(),
                    "escrow_fee": $("#escrow_fee").val(),
                    "management_fee": $("#management_fee").val(),
                    "parking_fee": $("#parking_fee").val(),
                    "contract_note": $("#contract_note").val(),
                }
                $.ajax({
                    method: "POST",
                    url: "https://rent-house-erp.echouse.co/api/contract_managements",
                    data: requestData,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已成功新增',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                // window.location.href = "/tenantManagementNews";
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                });
            }

            $("#save_button").on("click", () => {
                handleSaveButtonClick();
            })
        });
    </script>
@endpush
