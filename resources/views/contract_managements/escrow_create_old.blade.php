{{-- 代管約 契約新增頁面 --}}
@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">
            新增代管約
        </h3>
    </div>

    <div class="accordion" id="accordionPanelsStayOpenExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
                    aria-controls="panelsStayOpen-collapseOne">
                    編輯媒合設定維護
                </button>
            </h2>
            <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
                aria-labelledby="panelsStayOpen-headingOne">
                <div class="accordion-body">
                    <div class="container-fluid container-fixed-lg">
                        @include('adminlte-templates::common.errors')
                        <div class="card card-transparent">
                            <div class="card-body">
                                {{-- 上方按鈕 --}}
                                <div class="row g-3 align-items-start justify-content-evenly mb-4">
                                    <div class="col-3">
                                        <button type="button" class="btn btn-primary col-10">房東資料</button>
                                    </div>
                                    <div class="col-3">
                                        <button type="button" class="btn btn-primary col-10">租客資料</button>
                                    </div>
                                    <div class="col-3">
                                        <button type="button" class="btn btn-danger col-10">終止契約</button>
                                    </div>
                                    <div class="col-3">
                                        <button type="button" class="btn btn-primary col-10">修繕紀錄</button>
                                    </div>
                                </div>

                                {{-- 媒合編號 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label htmlFor="matchId" class="col-form-label">媒合編號</label>
                                    </div>
                                    <div class="col-8">
                                        <input type="text" id="matchId" class="form-control"
                                            aria-describedby="passwordHelpInline" value="" />
                                    </div>
                                </div>

                                {{-- 簽約日期 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label htmlFor="signingDt" class="col-form-label">簽約日期</label>
                                    </div>
                                    <div class="col-8">
                                        <span class="form-inline">
                                            <div class="input-group"><input type="text" name="signingDt"
                                                    value="{{ date('Y/m/d') }}" class="form-control" autocomplete="off"
                                                    maxlength="10" size="10" placeholder="YYY-MM-DD" id="signingDt">
                                                <span class="input-group-addon add-on">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </span>
                                            </div>
                                        </span>
                                    </div>
                                </div>

                                {{-- 門牌 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label">門牌</label>
                                    </div>
                                    <div class="col-8">
                                        <label class="col-form-label" id="building_address">讀取中...</label>
                                    </div>
                                </div>

                                {{-- 建物坐落 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label">建物坐落</label>
                                    </div>
                                    <div class="col-8">
                                        <label class="col-form-label" id="building_located">讀取中...</label>
                                    </div>
                                </div>

                                {{-- 建號 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label">建號</label>
                                    </div>
                                    <div class="col-8">
                                        <label class="col-form-label" id="building_num">讀取中...</label>
                                    </div>
                                </div>

                                {{-- 立書契約人 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label">立書契約人</label>
                                    </div>
                                    <div class="col-8">
                                        <label class="col-form-label" id="lessor_name">讀取中...</label><br>
                                        <label style="color:#1a9ca5;">(註：有共同持分者，請填列姓名+ID，若不只一位，中間請以"，"隔開。)</label><br>
                                        <textarea name="otherLandlordDesc" class="form-control" style="width:350px;display:inline-block;"
                                            id="otherLandlordDesc"></textarea><br>
                                        <label class="col-form-label" id="lessee_name">讀取中...</label><br>
                                        <label style="color:orange;">乙方核定資格</label>
                                        <select name="membtype" class="form-control"
                                            style="width: 200px; display: inline-block; background-color: rgb(224, 224, 224); color:#000"
                                            id="membtype" disabled>
                                            <option value="1">一般戶</option>
                                            <option value="2">第一類弱勢戶</option>
                                            <option value="3">第二類弱勢戶</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- 租賃期間 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label htmlFor="matchId" class="col-form-label text-danger">*租賃期間</label>
                                    </div>
                                    <div class="col-8">
                                        <div class="row">
                                            <div class="col-md-12 form-inline">
                                                <span class="form-inline">
                                                    <div class="input-group"><input type="text" name="leaseStartDt"
                                                            value="{{ date('Y/m/d') }}" class="form-control"
                                                            autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                            id="leaseStartDt">
                                                        <span class="input-group-addon add-on">
                                                            <i class="fa fa-fw fa-calendar"
                                                                data-time-icon="fa fa-fw fa-clock-o"
                                                                data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                        </span>
                                                    </div>
                                                </span>
                                                ~
                                                <span class="form-inline">
                                                    <div class="input-group mgdate "><input type="text" name="leaseEndDt"
                                                            value="{{ date('Y/m/d') }}" class="form-control"
                                                            autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                            id="leaseEndDt">
                                                        <span class="input-group-addon add-on">
                                                            <i class="fa fa-fw fa-calendar"
                                                                data-time-icon="fa fa-fw fa-clock-o"
                                                                data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                        </span>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- 簽約租金 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*簽約租金</label>
                                    </div>
                                    <div class="col-8">
                                        <label class="col-form-label form-inline">
                                            每月新台幣
                                            <input type="number" name="rent" value="" class="form-control"
                                                style="width:90px;" id="rent">
                                            元整
                                        </label><br>
                                        <label class="col-form-label form-inline">
                                            承租人支付租金為新台幣
                                            <input type="number" name="burden" value="" class="form-control"
                                                style="width:90px;" id="burden">
                                            整
                                        </label>
                                    </div>
                                </div>

                                {{-- 繳款日 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*繳款日</label>
                                    </div>
                                    <div class="col-8">
                                        <select name="paymentDate" class="form-control"
                                            style="width: 200px; display: inline-block; background-color: rgb(224, 224, 224); color:#000"
                                            id="paymentDate">
                                            <option value="1">5日</option>
                                            <option value="2">20日</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- 租金支付方式 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*租金支付方式</label>
                                    </div>
                                    <div class="col-8">
                                        <div class="row mb-3">
                                            <select name="rent_pay_type" class="form-control"
                                                style="width: 200px; display: inline-block; background-color: rgb(224, 224, 224); color:#000"
                                                id="rent_pay_type">
                                                <option value="1">轉帳繳付</option>
                                                <option value="2">現金繳付</option>
                                            </select>
                                        </div>
                                        <div class="row align-items-center">
                                            <label class="col-form-label col-auto">金融機構：</label>
                                            <select name="bank_name" class="form-control col-auto"
                                                style="width: 200px; display: inline-block; background-color: rgb(224, 224, 224); color:#000"
                                                id="bank_name">
                                                <option value="1">007:第一銀行</option>
                                                <option value="2">008:華南銀行</option>
                                            </select>
                                            <label class="col-form-label col-auto">分行代碼：</label>
                                            <input type="text" name="bank_branch_code" value="" class="form-control"
                                                style="width:90px;" id="bank_branch_code">
                                            <label class="col-form-label col-auto">戶名：</label>
                                            <input type="text" name="bank_owner_name" value="易居管理顧問股份有限公司"
                                                id="bank_owner_name" class="form-control" style="width:90px;">
                                            <label class="col-form-label col-auto">帳號：</label>
                                            <input type="text" name="bank_account" value="" id="bank_account"
                                                class="form-control" style="width:90px;">
                                        </div>
                                    </div>
                                </div>

                                {{-- 現有房客 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*現有房客</label>
                                    </div>
                                    <div class="col-8">
                                        <select name="had_tenants" class="form-control"
                                            style="width: 200px; display: inline-block; background-color: rgb(224, 224, 224); color:#000"
                                            id="had_tenants">
                                            <option value="1">是</option>
                                            <option value="2">否</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- 押租金 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*押租金</label>
                                    </div>
                                    <div class="col-8 form-inline">
                                        新台幣<input type="number" name="deposit_amount" value="" id="deposit_amount"
                                            class="form-control" style="width:90px;">元整
                                    </div>
                                </div>

                                {{-- 公證費 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*公證費</label>
                                    </div>
                                    <div class="col-8">
                                        <div class="row form-inline mb-3">
                                            新台幣<input type="number" name="notary_fees" value="" id="notary_fees"
                                                class="form-control" style="width:90px;">元整
                                        </div>
                                        <div class="row form-inline mb-3">
                                            由出租人負擔<input type="number" name="notary_fees_share1" value=""
                                                id="notary_fees_share1" class="form-control" style="width:90px;">元整
                                        </div>
                                        <div class="row form-inline mb-3">
                                            由承租人負擔<input type="number" name="notary_fees_share2" value=""
                                                id="notary_fees_share2" class="form-control" style="width:90px;">元整
                                        </div>
                                    </div>
                                </div>

                                {{-- 媒合費 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*媒合費</label>
                                    </div>
                                    <div class="col-8 form-inline mb-3">
                                        新台幣<input type="number" name="matchmaking_fee" value="" id="matchmaking_fee"
                                            class="form-control" style="width:90px;">元整
                                    </div>
                                </div>

                                {{-- 代管費 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*代管費</label>
                                    </div>
                                    <div class="col-8 form-inline mb-3">
                                        新台幣<input type="number" name="escrow_fee" value="" id="escrow_fee"
                                            class="form-control" style="width:90px;">元整
                                    </div>
                                </div>

                                {{-- 管理費 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*管理費</label>
                                    </div>
                                    <div class="col-8 form-inline mb-3">
                                        新台幣<input type="number" name="management_fee" value="" id="management_fee"
                                            class="form-control" style="width:90px;">元整
                                    </div>
                                </div>

                                {{-- 車位費 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label text-danger">*車位</label>
                                    </div>
                                    <div class="col-8 form-inline mb-3">
                                        新台幣<input type="number" name="parking_fee" value="" id="parking_fee"
                                            class="form-control" style="width:90px;">元整
                                    </div>
                                </div>

                                {{-- 契約備註說明 --}}
                                <div class="row g-3 align-items-start mb-4">
                                    <div class="col-1">
                                        <label class="col-form-label">契約備註說明</label>
                                    </div>
                                    <div class="col-8">
                                        <textarea name="contract_note" class="form-control" style="width:350px;display:inline-block;"
                                            id="contract_note"></textarea>
                                    </div>
                                </div>

                                {{-- 儲存按鈕 --}}
                                <div class="row g-3 align-items-start justify-content-start mb-4">
                                    <div class="col-1">
                                        <button type="button" class="btn btn-primary col-10"
                                            onclick="saveData()">儲存</button>
                                    </div>
                                    <div class="col-1">
                                        <button type="button" class="btn btn-secondary col-10"
                                            onclick="goBackMatchManagementsPage('test')">取消</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseTwo">
                    檢附文件
                </button>
            </h2>
            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse"
                aria-labelledby="panelsStayOpen-headingTwo">
                <div class="accordion-body">
                    <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse
                    plugin adds the appropriate classes that we use to style each element. These classes control the overall
                    appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with
                    custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go
                    within the <code>.accordion-body</code>, though the transition does limit overflow.
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseThree">
                    租屋服務事業
                </button>
            </h2>
            <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse"
                aria-labelledby="panelsStayOpen-headingThree">
                <div class="accordion-body">
                    <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse
                    plugin adds the appropriate classes that we use to style each element. These classes control the overall
                    appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with
                    custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go
                    within the <code>.accordion-body</code>, though the transition does limit overflow.
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingFour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseThree">
                    縣市確認
                </button>
            </h2>
            <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse"
                aria-labelledby="panelsStayOpen-headingFour">
                <div class="accordion-body">
                    <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse
                    plugin adds the appropriate classes that we use to style each element. These classes control the overall
                    appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with
                    custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go
                    within the <code>.accordion-body</code>, though the transition does limit overflow.
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseThree">
                    終止契約
                </button>
            </h2>
            <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse"
                aria-labelledby="panelsStayOpen-headingFive">
                <div class="accordion-body">
                    <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse
                    plugin adds the appropriate classes that we use to style each element. These classes control the overall
                    appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with
                    custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go
                    within the <code>.accordion-body</code>, though the transition does limit overflow.
                </div>
            </div>
        </div>
    </div>
@endsection
@section('match_management')
    @parent
    <script>
        const matchManagementData = @json($matchManagement);
        console.log(matchManagementData);

        // 取得建物資料
        async function getBuildingData(building_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/building_management_news?case_no=${building_id}`,
                success: function(data) {
                    console.log("建物資料");
                    retVal = data["data"][0];
                    console.log(retVal);
                }
            });
            return retVal;
        }

        // 取得租客資料
        async function getTenantData(tenant_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/tenant_management_news?case_no=${tenant_id}`,
                success: function(data) {
                    console.log("租客資料");
                    retVal = data["data"][0];
                    console.log(retVal);
                }
            });
            return retVal;
        }

        // 儲存資料
        async function saveData(id) {
            // contract_management
            let matchId = $("#matchId").val(); // 媒合編號
            let signingDt = $("#signingDt").val(); // 簽約日期
            let leaseStartDt = $("#leaseStartDt").val(); // 租賃開始時間
            let leaseEndDt = $("#leaseEndDt").val(); // 租賃結束時間
            let rent = $("#rent").val(); // 簽約租金
            let burden = $("#burden").val(); // 承租人支付租金
            let payment_date = $("#payment_date").val(); // 繳款日
            let rent_pay_type = $("#rent_pay_type").val(); // 租金支付方式
            let bank_name = $("#bank_name").val(); // 金融機構
            let bank_branch_code = $("#bank_branch_code").val(); // 分行代碼
            let bank_owner_name = $("#bank_owner_name").val(); // 戶名
            let bank_account = $("#bank_account").val(); // 銀行帳號
            let deposit_amount = $("#deposit_amount").val(); // 押租金
            let had_tenants = $("#had_tenants").val(); // 現在是否有房客
            let notary_fees = $("#notary_fees").val(); // 公證費
            let notary_fees_share1 = $("#notary_fees_share1").val(); // 公證費出租人負擔的部分
            let notary_fees_share2 = $("#notary_fees_share2").val(); // 公證費承租人負擔的部分
            let matchmaking_fee = $("#matchmaking_fee").val(); // 媒合費
            let escrow_fee = $("#escrow_fee").val(); // 代管費
            let management_fee = $("#management_fee").val(); // 管理費
            let parking_fee = $("#parking_fee").val(); // 車位費
            let contract_note = $("#contract_note").val(); // 契約備註說明
            let request_data = {
                match_id: matchId,
                signing_date: signingDt,
                lease_start_date: leaseStartDt,
                lease_end_date: leaseEndDt,
                rent: rent,
                burden: burden,
                payment_date: payment_date,
                rent_pay_type: rent_pay_type,
                bank_name: bank_name,
                bank_branch_code: bank_branch_code,
                bank_owner_name: bank_owner_name,
                bank_account: bank_account,
                deposit_amount: deposit_amount,
                had_tenants: had_tenants,
                notary_fees: notary_fees,
                notary_fees_share1: notary_fees_share1,
                notary_fees_share2: notary_fees_share2,
                matchmaking_fee: matchmaking_fee,
                escrow_fee: escrow_fee,
                management_fee: management_fee,
                parking_fee: parking_fee,
                contract_note: contract_note,
            }
            console.log(request_data);
            await $.ajax({
                type: 'POST',
                url: `/api/contract_managements`,
                dataType: "json",
                data: request_data,
                success: function(data) {
                    console.log("租客資料");
                    console.log(data);
                    Swal.fire({
                        title: '成功',
                        text: '資料已儲存!',
                        icon: 'success'
                    }).then((result) => {
                        window.location.href =
                            `https://rent-house-erp.echouse.co/matchManagements/${matchManagementData['id']}/edit`;
                    })
                }
            });
        }
        $(document).ready(async function() {
            // 先用ajax取得資料
            let buildingData = await getBuildingData(matchManagementData['building_case_id']);
            let tenantData = await getTenantData(matchManagementData['tenant_case_id']);

            // 拼湊地址資料
            let address_ln = buildingData['building_address_ln'] ? `${buildingData['building_address_ln']}巷` :
                "";
            let address_aly = buildingData['building_address_aly'] ?
                `${buildingData['building_address_aly']}弄` : "";
            let address_num_hyphen = buildingData['building_address_num_hyphen'] ?
                `之${buildingData['building_address_num_hyphen']}` : "";
            let address_floor = buildingData['building_address_floor'] ?
                `${buildingData['building_address_floor']}樓` : "";
            let address_floor_sub = buildingData['building_address_floor_sub'] ?
                `之${buildingData['building_address_floor_sub']}` : "";
            let address_room_num = buildingData['building_address_room_num'] ?
                `${buildingData['building_address_room_num']}室` : "";
            $("#matchId").val(matchManagementData['match_id']);
            $("#building_address").html(
                `
                ${buildingData['building_address_city']?buildingData['building_address_city']:""}
                ${buildingData['building_address_city_area']?buildingData['building_address_city_area']:""}
                ${buildingData['building_address_street']?buildingData['building_address_street']:""}
                ${address_ln}
                ${address_aly}
                ${buildingData['building_address_num']?buildingData['building_address_num']:""}
                ${address_num_hyphen}號
                ${address_floor}
                ${address_floor_sub}
                ${address_room_num}
                `
            );
            $("#building_located").html(
                `
                ${buildingData['building_located_city'] ? buildingData['building_located_city']:""}
                ${buildingData['building_located_city_area'] ? buildingData['building_located_city_area']:""}
                ${buildingData['building_located_lot'] ? buildingData['building_located_lot']:""}
                ${buildingData['building_located_land_num'] ? buildingData['building_located_land_num'] + "地號":""}
                `
            );
            $("#building_num").html(buildingData['building_num']);
            $("#lessor_name").html(`出租人：${buildingData['name']}(甲方)`);
            $("#lessee_name").html(`承租人：${tenantData['name']}(乙方)`);
            $("#membtype").val(tenantData['membtype']);
        });
    </script>
    {{-- <script>
        const contractManagementData = @json($contractManagement);
        console.log(contractManagementData);
        // 取得建物資料
        async function getBuildingData(building_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/building_management_news?case_no=${building_id}`,
                success: function(data) {
                    console.log("建物資料");
                    retVal = data["data"][0];
                    console.log(retVal);
                }
            });
            return retVal;
        }
        // 取得租客資料
        async function getTenantData(tenant_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/tenant_management_news?case_no=${tenant_id}`,
                success: function(data) {
                    console.log("租客資料");
                    retVal = data["data"][0];
                    console.log(retVal);
                }
            });
            return retVal;
        }

        // 回到媒合編輯頁面
        async function goBackMatchManagementsPage(match_id) {
            await $.ajax({
                type: 'GET',
                url: `/api/match_managements?match_id=${match_id}`,
                success: function(data) {
                    console.log("媒合資料");
                    let match_management_id = data["data"][0]["id"];
                    window.location.href =
                        `https://rent-house-erp.echouse.co/matchManagements/${match_management_id}/edit`
                }
            });
        }
        // 儲存資料
        async function saveData(id) {
            // contract_management
            let matchId = $("#matchId").val(); // 媒合編號
            let signingDt = $("#signingDt").val(); // 簽約日期
            let leaseStartDt = $("#leaseStartDt").val(); // 租賃開始時間
            let leaseEndDt = $("#leaseEndDt").val(); // 租賃結束時間
            let rent = $("#rent").val(); // 簽約租金
            let burden = $("#burden").val(); // 承租人支付租金
            let payment_date = $("#payment_date").val(); // 繳款日
            let rent_pay_type = $("#rent_pay_type").val(); // 租金支付方式
            let bank_name = $("#bank_name").val(); // 金融機構
            let bank_branch_code = $("#bank_branch_code").val(); // 分行代碼
            let bank_owner_name = $("#bank_owner_name").val(); // 戶名
            let bank_account = $("#bank_account").val(); // 銀行帳號
            let deposit_amount = $("#deposit_amount").val(); // 押租金
            let had_tenants = $("#had_tenants").val(); // 現在是否有房客
            let notary_fees = $("#notary_fees").val(); // 公證費
            let notary_fees_share1 = $("#notary_fees_share1").val(); // 公證費出租人負擔的部分
            let notary_fees_share2 = $("#notary_fees_share2").val(); // 公證費承租人負擔的部分
            let matchmaking_fee = $("#matchmaking_fee").val(); // 媒合費
            let escrow_fee = $("#escrow_fee").val(); // 代管費
            let management_fee = $("#management_fee").val(); // 管理費
            let parking_fee = $("#parking_fee").val(); // 車位費
            let contract_note = $("#contract_note").val(); // 契約備註說明
            await $.ajax({
                type: 'PATCH',
                url: `/api/contract_managements/${id}`,
                dataType: "json",
                data: {
                    matchId: matchId,
                    signingDt: signingDt,
                    leaseStartDt: leaseStartDt,
                    leaseEndDt: leaseEndDt,
                    rent: rent,
                    burden: burden,
                    payment_date: payment_date,
                    rent_pay_type: rent_pay_type,
                    bank_name: bank_name,
                    bank_branch_code: bank_branch_code,
                    bank_owner_name: bank_owner_name,
                    bank_account: bank_account,
                    deposit_amount: deposit_amount,
                    had_tenants: had_tenants,
                    notary_fees: notary_fees,
                    notary_fees_share1: notary_fees_share1,
                    notary_fees_share2: notary_fees_share2,
                    matchmaking_fee: matchmaking_fee,
                    escrow_fee: escrow_fee,
                    management_fee: management_fee,
                    parking_fee: parking_fee,
                    contract_note: contract_note,
                },
                success: function(data) {
                    console.log("租客資料");
                    console.log(data);
                    Swal.fire(
                        '成功!',
                        '資料已儲存!',
                        'success'
                    )
                }
            });
        }
        $(document).ready(async function() {
            // 填入欄位資料
            let buildingData = await getBuildingData(contractManagementData['building_case_no']);
            let tenantData = await getTenantData(contractManagementData['tenant_case_no'])
            let address_ln = buildingData['building_address_ln'] ? `${buildingData['building_address_ln']}巷` :
                "";
            let address_aly = buildingData['building_address_aly'] ?
                `${buildingData['building_address_aly']}弄` : "";
            let address_num_hyphen = buildingData['building_address_num_hyphen'] ?
                `之${buildingData['building_address_num_hyphen']}` : "";
            let address_floor = buildingData['building_address_floor'] ?
                `${buildingData['building_address_floor']}樓` : "";
            let address_floor_sub = buildingData['building_address_floor_sub'] ?
                `之${buildingData['building_address_floor_sub']}` : "";
            let address_room_num = buildingData['building_address_room_num'] ?
                `${buildingData['building_address_room_num']}室` : "";
            $("#matchId").val(contractManagementData['match_id']);
            $("#signingDt").val(contractManagementData['signing_date']);
            $("#building_address").html(
                `
                ${buildingData['building_address_city']?buildingData['building_address_city']:""}
                ${buildingData['building_address_city_area']?buildingData['building_address_city_area']:""}
                ${buildingData['building_address_street']?buildingData['building_address_street']:""}
                ${address_ln}
                ${address_aly}
                ${buildingData['building_address_num']?buildingData['building_address_num']:""}
                ${address_num_hyphen}號
                ${address_floor}
                ${address_floor_sub}
                ${address_room_num}
                `
            );
            $("#building_located").html(
                `
                ${buildingData['building_located_city'] ? buildingData['building_located_city']:""}
                ${buildingData['building_located_city_area'] ? buildingData['building_located_city_area']:""}
                ${buildingData['building_located_lot'] ? buildingData['building_located_lot']:""}
                ${buildingData['building_located_land_num'] ? buildingData['building_located_land_num'] + "地號":""}
                `
            );
            $("#building_num").html(buildingData['building_num']);
            $("#lessor_name").html(`出租人：${buildingData['name']}(甲方)`);
            $("#lessee_name").html(`承租人：${tenantData['name']}(乙方)`);
            $("#membtype").val(tenantData['membtype']);
            $("#leaseStartDt").val(contractManagementData['lease_start_date']);
            $("#leaseEndDt").val(contractManagementData['lease_end_date']);
            $("#rent").val(contractManagementData['rent']);
            $("#burden").val(contractManagementData['burden']);
            $("#paymentDate").val(contractManagementData['payment_date']);
            $("#rent_pay_type").val(contractManagementData['rent_pay_type']);
            $("#bank_name").val(contractManagementData['bank_name']);
            $("#bank_branch_code").val(contractManagementData['bank_branch_code']);
            $("#bank_owner_name").val(contractManagementData['bank_owner_name']);
            $("#bank_account").val(contractManagementData['bank_account']);
            $("#deposit_amount").val(contractManagementData['deposit_amount']);
            $("#had_tenants").val(contractManagementData['had_tenants']);
            $("#notary_fees").val(contractManagementData['notary_fees']);
            $("#notary_fees_share1").val(contractManagementData['notary_fees_share1']);
            $("#notary_fees_share2").val(contractManagementData['notary_fees_share2']);
            $("#matchmaking_fee").val(contractManagementData['matchmaking_fee']);
            $("#escrow_fee").val(contractManagementData['escrow_fee']);
            $("#management_fee").val(contractManagementData['management_fee']);
            $("#parking_fee").val(contractManagementData['parking_fee']);
            $("#contract_note").val(contractManagementData['contract_note']);
        });
    </script> --}}
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
