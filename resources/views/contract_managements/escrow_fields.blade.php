{{-- 代管約 --}}
{{-- 編輯房客承租申請 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
            編輯媒合設定維護
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3">
                    {{-- 媒合編號 --}}
                    <div class="row col-md-6">
                        <label for="match_id" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*媒合編號</label>
                        <div class="col-md-12">
                            <input type="text" name="match_id" class="form-control-plaintext" id="match_id" readonly />
                        </div>
                    </div>
                    {{-- 簽約日期 --}}
                    <div class="row col-md-6">
                        <label for="signing_date" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*簽約日期</label>
                        <div class="col-md-12">
                            <div class="input-group date">
                                <input type="text" name="signing_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="signing_date" aria-label="民國年" aria-describedby="button-addon1">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    {{-- 門牌 --}}
                    <div class="row col-md-6">
                        <label for="building_address" class="col-md-12 col-form-label fw-bolder text-nowrap">門牌</label>
                        <div class="col-md-12">
                            <input type="text" name="building_address" class="form-control-plaintext" id="building_address" readonly />
                        </div>
                    </div>
                    {{-- 建物坐落 --}}
                    <div class="row col-md-6">
                        <label for="building_located" class="col-md-12 col-form-label fw-bolder text-nowrap">建物坐落</label>
                        <div class="col-md-12">
                            <input type="text" name="building_located" class="form-control-plaintext" id="building_located" readonly />
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    {{-- 租客編號 --}}
                    @if (app('request')->input('renew'))
                        <div class="row col-md-6">
                            <label for="tenant_case_id" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">
                                *租客內政部編號
                            </label>
                            {{-- 這是一個react元件：resources\js\components\match_managements\TenantSelect.js --}}
                            <div class="col-md-12 TenantSelect">
                            </div>
                        </div>
                    @endif
                    {{-- 建號 --}}
                    <div class="row col-md-6">
                        <label for="building_num" class="col-md-12 col-form-label fw-bolder text-nowrap">建號</label>
                        <div class="col-md-12">
                            <input type="text" name="building_num" class="form-control-plaintext" id="building_num" readonly />
                        </div>
                    </div>
                </div>

                {{-- 立書契約人 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="lessor_name" class="col-md-12 col-form-label fw-bolder text-nowrap">立書契約人</label>
                        <div class="col-md-12">
                            <input type="text" name="lessor_name" class="form-control-plaintext" id="lessor_name" readonly />
                            <div class="col-sm-12 fw-bold" style="color:#1a9ca5;">(註：有共同持分者，請填列姓名+ID，若不只一位，中間請以"，"隔開。)
                            </div>
                            <textarea name="stakeholders" id="stakeholders"></textarea>
                            <input type="text" name="tenant_name" class="form-control-plaintext" id="tenant_name" readonly />
                            <div class="col-sm-12 fw-bold" style="color:orange;">乙方核定資格：<span id="membtype" class="text-dark">一般戶</span>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 租賃期間 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="lease_start_date" class="col-md-12 col-form-label fw-bolder text-nowrap">租賃期間</label>
                        <div class="row col-md-12">
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <input type="text" name="lease_start_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="lease_start_date" aria-label="民國年" aria-describedby="button-addon1">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-1 text-center">~</div>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <input type="text" name="lease_end_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="lease_end_date" aria-label="民國年" aria-describedby="button-addon2">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 現有房客 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="has_tanants" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*現有房客</label>
                        <div class="col-md-12">
                            <select class="form-select form-control" id="has_tanants" name="has_tanants">
                                <option value="" selected>請選擇</option>
                                <option value="1">是</option>
                                <option value="2">否</option>
                            </select>
                        </div>
                    </div>
                </div>

                {{-- 簽約租金 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="rent" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*簽約租金</label>
                        <div class="col-md-12">
                            <div class="input-group mb-3">
                                <span class="input-group-text">每月新台幣</span>
                                <input type="text" class="form-control" id="rent" name="rent" value="0">
                                <span class="input-group-text">元整</span>
                            </div>
                            <div class="input-group mb-3">
                                <span class="input-group-text">承租人支付租金為新台幣</span>
                                <input type="text" class="form-control" id="rent_self_pay" name="rent_self_pay" value="0">
                                <span class="input-group-text">元整</span>
                            </div>
                            <div class="input-group">
                                <span class="input-group-text">政府租金補助為新台幣&emsp;</span>
                                <input type="text" class="form-control" id="rent_government_pay" name="rent_government_pay" value="0">
                                <span class="input-group-text">元整</span>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 繳款日 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="payment_date" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*繳款日</label>
                        <div class="col-md-6">
                            <select class="form-select form-control" name="payment_date" id="payment_date">
                                <option value="" selected>請選擇</option>
                                <option value="5日">5日</option>
                                <option value="20日">20日</option>
                            </select>
                        </div>
                    </div>
                </div>

                {{-- 租金支付方式 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="rent_pay_type" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*租金支付方式</label>
                        <div class="col-md-6">
                            <select class="form-select form-control" name="rent_pay_type" id="rent_pay_type">
                                <option value="" selected>請選擇</option>
                                <option value="轉帳繳付">轉帳繳付</option>
                                <option value="現金繳付">現金繳付</option>
                            </select>
                            <div class="bankSelector" data-param="{{ isset($contractManagement) ? $contractManagement : '[]' }}"></div>
                        </div>
                    </div>
                </div>

                {{-- 押租金 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="deposit_amount" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*押租金</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="deposit_amount" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" name="deposit_amount" id="deposit_amount" value="0">
                                <label for="deposit_amount" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 公證費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="notary_fees" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*公證費</label>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <label for="notary_fees" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" name="notary_fees" id="notary_fees" value="0">
                                <label for="notary_fees" class="input-group-text">元整</label>
                            </div>
                            <div class="input-group mb-3">
                                <label for="notary_fees_share1" class="input-group-text">由出租人負擔</label>
                                <input type="text" class="form-control" name="notary_fees_share1" id="notary_fees_share1" value="0">
                                <label for="notary_fees_share1" class="input-group-text">元整</label>
                            </div>
                            <div class="input-group mb-3">
                                <label for="notary_fees_share2" class="input-group-text">由承租人負擔</label>
                                <input type="text" class="form-control" name="notary_fees_share2" id="notary_fees_share2" value="0">
                                <label for="notary_fees_share2" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 媒合費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="matchmaking_fee" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*媒合費</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="matchmaking_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" name="matchmaking_fee" id="matchmaking_fee" value="0">
                                <label for="matchmaking_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 代管費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="escrow_fee" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*代管費</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="escrow_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" name="escrow_fee" id="escrow_fee" value="0">
                                <label for="escrow_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 管理費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="management_fee" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*管理費</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="management_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" name="management_fee" id="management_fee" value="0">
                                <label for="management_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 車位 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="parking_fee" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*車位</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="parking_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" name="parking_fee" id="parking_fee" value="0">
                                <label for="parking_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 資訊提供 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="information_feed" class="col-md-12 col-form-label fw-bolder text-nowrap">資訊提供</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="information_feed" id="information_feed" placeholder="編號">
                        </div>
                        <div class="col-md-6 mt-md-0 mt-3">
                            <div class="input-group date">
                                <input type="text" value="" class="form-control" name="information_feed_date" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="information_feed_date" aria-label="民國年" aria-describedby="button-addon1">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 實價登錄 --}}
                <div class="NetPriceLogin" data-param="{{ isset($contractManagement) ? $contractManagement['net_price_login'] : '[]' }}">

                    {{-- 契約備註說明 --}}
                    <div class="row mb-3">
                        <div class="row col-md-12">
                            <label for="contract_note" class="col-md-12 col-form-label fw-bolder text-nowrap">契約備註說明</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="contract_note" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- 檢附文件 --}}
    @if (isset($contractManagement))
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                    檢附文件
                </button>
            </h2>
            <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
                <div class="accordion-body">
                    <div class="container-fluid">
                        <div class="file-loading">
                            <input id="contract_file_input" name="source" type="file" multiple {{ app('request')->input('cantedit') ? "readonly='true'" : '' }} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
