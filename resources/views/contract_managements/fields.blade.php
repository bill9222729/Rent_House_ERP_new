{{-- 編輯房客承租申請 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse"
            data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
            aria-controls="panelsStayOpen-collapseOne">
            編輯媒合設定維護
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
        aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <div class="container-fluid">
                {{-- 媒合編號 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="match_id" class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">媒合編號</label>
                        <div class="col-sm-6">
                            <input type="text" name="match_id" class="form-control-plaintext" id="match_id" readonly />
                        </div>
                    </div>
                </div>
                {{-- 簽約日期 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="signing_date"
                            class="col-sm-4 col-form-label text-end fw-bolder text-nowrap text-danger">*簽約日期</label>
                        <div class="col-sm-6">
                            <div class="input-group date">
                                <input type="text" name="signing_date" value="" class="form-control"
                                    autocomplete="off" maxlength="10" size="10" placeholder="民國年" id="signing_date"
                                    aria-label="民國年" aria-describedby="button-addon1">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- 門牌 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_address"
                            class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">門牌</label>
                        <div class="col-sm-7">
                            <input type="text" name="building_address" class="form-control-plaintext"
                                id="building_address" readonly />
                        </div>
                    </div>
                </div>
                {{-- 建物坐落 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_located"
                            class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">建物坐落</label>
                        <div class="col-sm-7">
                            <input type="text" name="building_located" class="form-control-plaintext"
                                id="building_located" readonly />
                        </div>
                    </div>
                </div>
                {{-- 建號 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="building_num"
                            class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">建號</label>
                        <div class="col-sm-6">
                            <input type="text" name="building_num" class="form-control-plaintext" id="building_num"
                                readonly />
                        </div>
                    </div>
                </div>

                {{-- 立書契約人 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="lessor_name"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">立書契約人</label>
                        <div class="col-sm-6">
                            <input type="text" name="lessor_name" class="form-control-plaintext" id="lessor_name"
                                readonly />
                            <div class="col-sm-12 fw-bold" style="color:#1a9ca5;">(註：有共同持分者，請填列姓名+ID，若不只一位，中間請以"，"隔開。)
                            </div>
                            <textarea></textarea>
                            <input type="text" name="tenant_name" class="form-control-plaintext" id="tenant_name"
                                readonly />
                            <div class="col-sm-12 fw-bold" style="color:orange;">乙方核定資格：<span id="membtype"
                                    class="text-dark">一般戶</span>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 租賃期間 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="lease_start_date"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">租賃期間</label>
                        <div class="row col-sm-4">
                            <div class="col">
                                <div class="input-group date">
                                    <input type="text" name="lease_start_date" value="" class="form-control"
                                        autocomplete="off" maxlength="10" size="10" placeholder="民國年"
                                        id="lease_start_date" aria-label="民國年" aria-describedby="button-addon1">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                            ~
                            <div class="col">
                                <div class="input-group date">
                                    <input type="text" name="lease_end_date" value="" class="form-control"
                                        autocomplete="off" maxlength="10" size="10" placeholder="民國年"
                                        id="lease_end_date" aria-label="民國年" aria-describedby="button-addon2">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 現有房客 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="has_tanants"
                            class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">現有房客</label>
                        <div class="col-sm-6">
                            <select class="form-select form-control" id="has_tanants">
                                <option value="0" selected>請選擇</option>
                                <option value="1">是</option>
                                <option value="2">否</option>
                            </select>
                        </div>
                    </div>
                </div>

                {{-- 簽約租金 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="rent"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">簽約租金</label>
                        <div class="col-sm-10">
                            <div class="input-group mb-3">
                                <span class="input-group-text">每月新台幣</span>
                                <input type="text" class="form-control" id="rent">
                                <span class="input-group-text">元整</span>
                            </div>
                            <div class="input-group">
                                <span class="input-group-text">承租人支付租金為新台幣</span>
                                <input type="text" class="form-control" id="rent_self_pay">
                                <span class="input-group-text">元整，政府租金補助為新台幣</span>
                                <input type="text" class="form-control" id="rent_government_pay">
                                <span class="input-group-text">元整</span>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 繳款日 --}}
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="payment_date"
                            class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">繳款日</label>
                        <div class="col-sm-6">
                            <select class="form-select form-control" id="payment_date">
                                <option value="未設定" selected>請選擇</option>
                                <option value="5日">5日</option>
                                <option value="20日">20日</option>
                            </select>
                        </div>
                    </div>
                </div>

                {{-- 租金支付方式 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="rent_pay_type"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">租金支付方式</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control" id="rent_pay_type">
                                <option selected>請選擇</option>
                                <option value="轉帳繳付">轉帳繳付</option>
                                <option value="現金繳付">現金繳付</option>
                            </select>
                            <div class="input-group mt-3">
                                <label for="bank_name" class="input-group-text">金融機構：</label>
                                <select class="form-select form-control" id="bank_name">
                                    <option selected>請選擇</option>
                                    <option value="轉帳繳付">轉帳繳付</option>
                                    <option value="現金繳付">現金繳付</option>
                                </select>
                                <label for="bank_branch_code" class="input-group-text">分行代碼：</label>
                                <input type="text" class="form-control" id="bank_branch_code">
                                <label for="bank_owner_name" class="input-group-text">戶名：</label>
                                <input type="text" class="form-control" id="bank_owner_name">
                                <label for="bank_account" class="input-group-text">帳號：</label>
                                <input type="text" class="form-control" id="bank_account">
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 押租金 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="deposit_amount"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">押租金</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <label for="deposit_amount" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" id="deposit_amount">
                                <label for="deposit_amount" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 公證費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="notary_fees"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">公證費</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <label for="notary_fees" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" id="notary_fees">
                                <label for="notary_fees" class="input-group-text">元整</label>
                            </div>
                            <div class="input-group">
                                <label for="notary_fees_share1" class="input-group-text">由出租人負擔</label>
                                <input type="text" class="form-control" id="notary_fees_share1">
                                <label for="notary_fees_share1" class="input-group-text">元整</label>
                            </div>
                            <div class="input-group">
                                <label for="notary_fees_share2" class="input-group-text">由承租人負擔</label>
                                <input type="text" class="form-control" id="notary_fees_share2">
                                <label for="notary_fees_share2" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 媒合費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="matchmaking_fee"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">媒合費</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <label for="matchmaking_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" id="matchmaking_fee">
                                <label for="matchmaking_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 代管費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="escrow_fee"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">代管費</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <label for="escrow_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" id="escrow_fee">
                                <label for="escrow_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 管理費 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="management_fee"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">管理費</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <label for="management_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" id="management_fee">
                                <label for="management_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 車位 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="parking_fee"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">車位</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <label for="parking_fee" class="input-group-text">新台幣</label>
                                <input type="text" class="form-control" id="parking_fee">
                                <label for="parking_fee" class="input-group-text">元整</label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 契約備註說明 --}}
                <div class="row mb-3">
                    <div class="row col-md-12">
                        <label for="contract_note"
                            class="col-sm-2 col-form-label text-end fw-bolder text-nowrap">契約備註說明</label>
                        <div class="col-sm-3">
                            <textarea class="form-control" id="contract_note" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 檢附文件 --}}
@if (isset($contractManagement))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true"
                aria-controls="panelsStayOpen-collapseFive">
                檢附文件
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show"
            aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="contract_file_input" name="source" type="file" multiple>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
