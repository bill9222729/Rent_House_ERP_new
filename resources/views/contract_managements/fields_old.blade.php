<div class="row">
    <!-- Case Status Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('case_status', __('models/contractManagements.fields.case_status') . ':') !!}
        {!! Form::text('case_status', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Signing Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('signing_date', __('models/contractManagements.fields.signing_date') . ':') !!}
        {!! Form::date('signing_date', null, ['class' => 'form-control', 'id' => 'signing_date']) !!}
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#signing_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })
        </script>
    @endpush

    <!-- Other Landlord Desc Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('other_landlord_desc', __('models/contractManagements.fields.other_landlord_desc') . ':') !!}
        {!! Form::text('other_landlord_desc', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Lease Start Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('lease_start_date', __('models/contractManagements.fields.lease_start_date') . ':') !!}
        {!! Form::date('lease_start_date', null, ['class' => 'form-control', 'id' => 'lease_start_date']) !!}
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#lease_start_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })
        </script>
    @endpush

    <!-- Lease End Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('lease_end_date', __('models/contractManagements.fields.lease_end_date') . ':') !!}
        {!! Form::date('lease_end_date', null, ['class' => 'form-control', 'id' => 'lease_end_date']) !!}
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#lease_end_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })
        </script>
    @endpush

    <!-- Rent Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('rent', __('models/contractManagements.fields.rent') . ':') !!}
        {!! Form::number('rent', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Rent Pay Type Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('rent_pay_type', __('models/contractManagements.fields.rent_pay_type') . ':') !!}
        {!! Form::text('rent_pay_type', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Bank Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('bank_name', __('models/contractManagements.fields.bank_name') . ':') !!}
        {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Bank Branch Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('bank_branch_code', __('models/contractManagements.fields.bank_branch_code') . ':') !!}
        {!! Form::text('bank_branch_code', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Bank Owner Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('bank_owner_name', __('models/contractManagements.fields.bank_owner_name') . ':') !!}
        {!! Form::text('bank_owner_name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Bank Account Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('bank_account', __('models/contractManagements.fields.bank_account') . ':') !!}
        {!! Form::text('bank_account', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Deposit Amount Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('deposit_amount', __('models/contractManagements.fields.deposit_amount') . ':') !!}
        {!! Form::number('deposit_amount', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Contract Note Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('contract_note', __('models/contractManagements.fields.contract_note') . ':') !!}
        {!! Form::text('contract_note', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Apply Man Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('apply_man_id', __('models/contractManagements.fields.apply_man_id') . ':') !!}
        {!! Form::number('apply_man_id', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Remark Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('remark', __('models/contractManagements.fields.remark') . ':') !!}
        {!! Form::text('remark', null, ['class' => 'form-control']) !!}
    </div>

    <!-- File Path Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('file_path', __('models/contractManagements.fields.file_path') . ':') !!}
        {!! Form::text('file_path', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
        <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
    </div>
</div>
