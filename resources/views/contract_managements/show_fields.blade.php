<!-- Case Status Field -->
<div class="form-group col-12">
    {!! Form::label('case_status', __('models/contractManagements.fields.case_status').':') !!}
    <p>{{ $contractManagement->case_status }}</p>
</div>


<!-- Signing Date Field -->
<div class="form-group col-12">
    {!! Form::label('signing_date', __('models/contractManagements.fields.signing_date').':') !!}
    <p>{{ $contractManagement->signing_date }}</p>
</div>


<!-- Other Landlord Desc Field -->
<div class="form-group col-12">
    {!! Form::label('other_landlord_desc', __('models/contractManagements.fields.other_landlord_desc').':') !!}
    <p>{{ $contractManagement->other_landlord_desc }}</p>
</div>


<!-- Lease Start Date Field -->
<div class="form-group col-12">
    {!! Form::label('lease_start_date', __('models/contractManagements.fields.lease_start_date').':') !!}
    <p>{{ $contractManagement->lease_start_date }}</p>
</div>


<!-- Lease End Date Field -->
<div class="form-group col-12">
    {!! Form::label('lease_end_date', __('models/contractManagements.fields.lease_end_date').':') !!}
    <p>{{ $contractManagement->lease_end_date }}</p>
</div>


<!-- Rent Field -->
<div class="form-group col-12">
    {!! Form::label('rent', __('models/contractManagements.fields.rent').':') !!}
    <p>{{ $contractManagement->rent }}</p>
</div>


<!-- Rent Pay Type Field -->
<div class="form-group col-12">
    {!! Form::label('rent_pay_type', __('models/contractManagements.fields.rent_pay_type').':') !!}
    <p>{{ $contractManagement->rent_pay_type }}</p>
</div>


<!-- Bank Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_name', __('models/contractManagements.fields.bank_name').':') !!}
    <p>{{ $contractManagement->bank_name }}</p>
</div>


<!-- Bank Branch Code Field -->
<div class="form-group col-12">
    {!! Form::label('bank_branch_code', __('models/contractManagements.fields.bank_branch_code').':') !!}
    <p>{{ $contractManagement->bank_branch_code }}</p>
</div>


<!-- Bank Owner Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_owner_name', __('models/contractManagements.fields.bank_owner_name').':') !!}
    <p>{{ $contractManagement->bank_owner_name }}</p>
</div>


<!-- Bank Account Field -->
<div class="form-group col-12">
    {!! Form::label('bank_account', __('models/contractManagements.fields.bank_account').':') !!}
    <p>{{ $contractManagement->bank_account }}</p>
</div>


<!-- Deposit Amount Field -->
<div class="form-group col-12">
    {!! Form::label('deposit_amount', __('models/contractManagements.fields.deposit_amount').':') !!}
    <p>{{ $contractManagement->deposit_amount }}</p>
</div>


<!-- Contract Note Field -->
<div class="form-group col-12">
    {!! Form::label('contract_note', __('models/contractManagements.fields.contract_note').':') !!}
    <p>{{ $contractManagement->contract_note }}</p>
</div>


<!-- Apply Man Id Field -->
<div class="form-group col-12">
    {!! Form::label('apply_man_id', __('models/contractManagements.fields.apply_man_id').':') !!}
    <p>{{ $contractManagement->apply_man_id }}</p>
</div>


<!-- Remark Field -->
<div class="form-group col-12">
    {!! Form::label('remark', __('models/contractManagements.fields.remark').':') !!}
    <p>{{ $contractManagement->remark }}</p>
</div>


<!-- File Path Field -->
<div class="form-group col-12">
    {!! Form::label('file_path', __('models/contractManagements.fields.file_path').':') !!}
    <p>{{ $contractManagement->file_path }}</p>
</div>


