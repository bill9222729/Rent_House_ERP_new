{!! Form::open(['route' => ['contracts.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @php
        $c = \App\Models\contract::where('id', $id)->first();
        $filePaths = [];
        if ($c->file_upload) {
            $filePaths = explode(',', $c->file_upload);
        }
    @endphp
    @if (count($filePaths) > 0)
        <!-- Button trigger modal -->
        {{-- <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal{{$id}}"> --}}
        {{-- 查看附件 --}}
        {{-- </button> --}}
    @endif
    <a href="{{ route('contracts.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
    'type' => 'submit',
    'class' => 'btn btn-danger btn-xs',
    'onclick' => 'return confirm("' . __('crud.are_you_sure') . '")',
]) !!}
</div>
{!! Form::close() !!}

<!-- Modal -->
<div class="modal fade" id="exampleModal{{ $id }}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">合約附件</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if (count($filePaths) > 0)
                    @foreach ($filePaths as $key => $filePath)
                        @if ($filePath)
                            <a href="/storage/{{ $filePath }}" class='btn btn-default btn-xs' target="_blank">
                                下載合約附件{{ $key + 1 }}
                            </a>
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
            </div>
        </div>
    </div>
</div>
