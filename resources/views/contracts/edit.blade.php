@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">
            @lang('models/contracts.singular')
        </h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        @include('adminlte-templates::common.errors')
        <div class="card card-transparent">
            <div class="card-body">
                <div class="row">
                    {!! Form::model($contract, ['route' => ['contracts.update', $contract->id], 'method' => 'patch', 'enctype' => 'multipart/form-data', 'id' => 'contract_form']) !!}

                            @include('contracts.fields')

                    {!! Form::close() !!}
                    <input type="button" id="faker_submit" value="儲存" onclick="faker_send()" class="btn btn-primary">
                    <a href="{{ route('contracts.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
                </div>
            </div>
        </div>
    </div>
@endsection
