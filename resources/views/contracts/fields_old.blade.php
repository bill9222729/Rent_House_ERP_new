<style>
    .vue_list_box {
        background: #eee;
    }

    .blur {
        filter: blur(4px);
    }

    .text-area {
        background-color: #fff;
        background-image: none;
        border: 1px solid rgba(0, 0, 0, 0.07);
        font-family: Arial, sans-serif;
        -webkit-appearance: none;
        color: #373e43;
        outline: 0;
        padding: 8px 12px;
        line-height: normal;
        font-size: 14px;
        font-weight: normal;
        vertical-align: middle;
        min-height: 30px;
        margin-left: 3px;
    }

    .close-btn {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 2rem;
    }

    #shadow-form1 {}

    #shadow-form1,
    #shadow-form2,
    #shadow-form3 {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 1000;
        height: 100%;
        width: 100%;
        display: grid;
        place-items: center;
        background: rgba(0, 0, 0, .4);
    }

    #shadow-form1 form,
    #shadow-form2 form,
    #shadow-form3 form {
        max-width: 800px;
        width: 85vw;
        margin: 4rem 0;
        padding: 1.5rem;
        overflow: auto;
        background: #f6f9fa;
        border-radius: 16px;
    }

    #function_btn {
        margin-bottom: 20px;
    }

    #function_btn .btn {
        margin-right: 20px;
    }

    .Modal {
        position: fixed;
        z-index: 99999;
        left: 20%;
        min-height: 50vh;
        display: none;
    }

    .Modal .card-body:first-child {
        border: 1px #0000007a solid;
    }

    .Modal #card-advance {
        min-height: 70vh;
    }

    .Modal #card-advance {
        overflow: scroll;
        height: 60vh;
        overflow-x: hidden;
    }

    .Modal #card-advance .card-header {
        margin-bottom: 50px;
    }

    .Modal .card {
        margin-bottom: 0px !important;
    }

    .necessary {
        color: #FF0000;
    }

    .abtn {
        cursor: pointer;
        font-size: 2rem;
    }

    .abtn-add {
        color: #ff0000 !important;
    }

    .abtn-delete {
        color: #00d8f5 !important;
    }

    .abtn-padding {
        padding-top: 3rem;
    }

    .field-container {
        padding: 0 !important;
    }

    #submit_btn {
        display: none;
        visibility: hidden;
    }

</style>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />


{{-- 【房東管理】浮動框 --}}
@php

$buildingManagement = null;
$tenantManagement = null;
$fixHistory = null;
if (isset($contract)) {
    $buildingManagement = $contract->buildingManagement;
    $tenantManagement = $contract->tenantManagement;
    $fixHistory = $contract->fix_histories;
}

@endphp

@php
use App\Models\HousePayHistory;

$contract_id = null;
$housePayHistory = null;
$packageEscrowFee = null;
$rentSubsidy = null;
$developmentMatchingFees = null;
$notaryFees = null;
$repairCost = null;
$insurance = null;
$insurance_day = null;

if (isset($contract)) {
    $contract_id = $contract->id;
    $housePayHistory = HousePayHistory::where('contract_id', $contract->id)->get();
    $packageEscrowFee = HousePayHistory::where('contract_id', $contract->id)
        ->where('type', 'package_escrow_fee')
        ->get();
    $rentSubsidy = HousePayHistory::where('contract_id', $contract->id)
        ->where('type', 'rent_subsidy')
        ->get();
    $developmentMatchingFees = HousePayHistory::where('contract_id', $contract->id)
        ->where('type', 'development_matching_fees')
        ->get();
    $notaryFees = HousePayHistory::where('contract_id', $contract->id)
        ->where('type', 'notary_fees')
        ->get();
    $repairCost = HousePayHistory::where('contract_id', $contract->id)
        ->where('type', 'repair_cost')
        ->get();
    $insurance = HousePayHistory::where('contract_id', $contract->id)
        ->where('type', 'insurance')
        ->get();
    $insurance_day = HousePayHistory::where('contract_id', $contract->id)
        ->where('type', 'insurance_day')
        ->get();
}

@endphp

@if ($buildingManagement != null)
    <div class="col-lg-8 sm-no-padding Modal" id="Build_Modal">
        <div class="card card-transparent">
            <div class="card-body no-padding">
                <div id="card-advance" class="card card-default">
                    <div class="card-header  ">
                        <div class="card-title">
                            <h2>房東資料</h2>
                            <a href="#" onclick="Close_Build_Manage()" class="close-btn">x</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('building_managements.contract_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function Show_Build_Manage() {
            $("#Build_Modal").fadeIn();
        }

        function Close_Build_Manage() {
            $("#Build_Modal").fadeOut();
        }
    </script>
@endif

@if ($tenantManagement != null)
    <div class="col-lg-8 sm-no-padding Modal" id="Tenant_Modal">
        <div class="card card-transparent">
            <div class="card-body no-padding">
                <div id="card-advance" class="card card-default">
                    <div class="card-header  ">
                        <div class="card-title">
                            <h2>租客資料</h2>
                            <a href="#" onclick="Close_Tenant_Manage()" class="close-btn">x</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('tenant_managements.contract_fields')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function Show_Tenant_Manage() {
            $("#Tenant_Modal").fadeIn();
        }

        function Close_Tenant_Manage() {
            $("#Tenant_Modal").fadeOut();
        }
    </script>
@endif

@if ($fixHistory != null)
    <div class="col-lg-8 sm-no-padding Modal" id="Fix_Modal">
        <div class="card card-transparent">
            <div class="card-body no-padding">
                <div id="card-advance" class="card card-default">
                    <div class="card-header  ">
                        <div class="card-title">
                            <h2>修繕資料</h2>
                            <a href="#" onclick="Close_Fix_Manage()" class="close-btn">x</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table>
                            <thead>
                                <tr>
                                    <th>業務</th>
                                    <th>請款月報</th>
                                    <th>物件編號</th>
                                    <th>物件地址</th>
                                    <th>損壞原因</th>
                                    <th>會勘日期</th>
                                    <th>修繕日期</th>
                                    <th>完成日期</th>
                                    <th>修繕記錄</th>
                                    <th>修繕金額</th>
                                    <th>收據</th>
                                    <th>本次請領金額</th>
                                    <th>餘額</th>
                                    <th>(包租)租約起迄日</th>
                                    <th>(轉租)租約起迄日</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($fixHistory as $fix)
                                    <tr>
                                        <td>{{ $fix->sales }}</td>
                                        <td>{{ $fix->pay_date }}</td>
                                        <td>{{ $fix->item_code }}</td>
                                        <td>{{ $fix->item_address }}</td>
                                        <td>{{ $fix->fix_source }}</td>
                                        <td>{{ $fix->check_date }}</td>
                                        <td>{{ $fix->fix_date }}</td>
                                        <td>{{ $fix->fix_done }}</td>
                                        <td>{{ $fix->fix_record }}</td>
                                        <td>{{ $fix->fix_price }}</td>
                                        <td>{{ $fix->receipt }}</td>
                                        <td>{{ $fix->final_price }}</td>
                                        <td>{{ $fix->balance }}</td>
                                        <td>{{ $fix->owner_start_date }} ~ {{ $fix->owner_end_date }}
                                        </td>
                                        <td>{{ $fix->change_start_date }} ~ {{ $fix->change_end_date }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function Show_Fix_Manage() {
            $("#Fix_Modal").fadeIn();
        }

        function Close_Fix_Manage() {
            $("#Fix_Modal").fadeOut();
        }
    </script>
@endif

<div class="row" id="function_btn">

    @if (isset($contract))
        @if (isset($contract) && $contract->tenantManagement)
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">租約終止</button>
        @endif


        @if ($contract->buildingManagement)
            <a onclick="Show_Build_Manage(this)" href="#" class="btn btn-info" id="b_b">房東資料</a>
        @else
            <a href="#" class="btn btn-danger">無房東資料</a>
        @endif
        @if ($contract->tenantManagement)
            <a onclick="Show_Tenant_Manage(this)" href="#" class="btn btn-info" id="t_b">租客資料</a>
        @else
            <a href="#" class="btn btn-danger">無租客資料</a>
        @endif
        @if ($contract->fix_histories)
            <a href="#" onclick="Show_Fix_Manage(this)" class="btn btn-info" id="f_b">修繕資料</a>
        @else
            <a href="#" class="btn btn-danger">無修繕資料</a>
        @endif
    @endif
</div>

<div id="contract_all_field">


    <div class="row">

        <!-- Contract Number Of Periods Field -->
        <div class="form-group col-3">
            {!! Form::label('contract_number_of_periods', __('models/contracts.fields.contract_number_of_periods') . ':', ['class' => 'necessary']) !!}
            {!! Form::select('contract_number_of_periods', ['第一期' => '第一期', '第二期' => '第二期', '第三期' => '第三期'], null, ['class' => 'form-control']) !!}
            {{-- {!! Form::text('contract_number_of_periods', null, ['class' => 'form-control']) !!} --}}
        </div>


        <!-- Contract Category Field -->
        <div class="form-group col-3">
            {!! Form::label('contract_category', __('models/contracts.fields.contract_category') . ':', ['class' => 'necessary']) !!}
            {!! Form::select('contract_category', ['新件' => '新件', '第二次媒合' => '第二次媒合', '第三次媒合' => '第三次媒合', '續約' => '續約', '無開發媒合費之特殊案例' => '無開發媒合費之特殊案例'], null, ['class' => 'form-control']) !!}
            {{-- {!! Form::text('contract_category', null, ['class' => 'form-control']) !!} --}}
        </div>


        <!-- Contract Business Sign Back Field -->
        <div class="form-group col-3">
            {!! Form::label('contract_business_sign_back', __('models/contracts.fields.contract_business_sign_back') . ':', ['class' => 'necessary']) !!}
            {{-- {!! Form::text('contract_business_sign_back', null, ['class' => 'form-control']) !!} --}}

            @php
                use App\Models\business_management;
                $all_source = business_management::select('name')
                    ->get()
                    ->toArray();
                $all_sales_name = [];
                foreach ($all_source as $key => $value) {
                    $all_sales_name[$value['name']] = $value['name'];
                }
                // dd($all_sales_name);
            @endphp
            {!! Form::select('contract_business_sign_back', $all_sales_name, null, ['class' => 'form-control']) !!}

        </div>


        <!-- Contract Match Date Field -->
        <div class="form-group col-3">
            {!! Form::label('contract_match_date', __('models/contracts.fields.contract_match_date') . ':', ['class' => 'necessary']) !!}
            @isset($contract)
                {!! Form::text('contract_match_date', (int) date('Y', strtotime($contract->contract_match_date)) - 1911 . date('-m-d', strtotime($contract->contract_match_date)), ['class' => 'form-control datepickerTW', 'id' => 'contract_match_date', 'style' => 'height: 35px;']) !!}
            @else
                {!! Form::text('contract_match_date', null, ['class' => 'form-control datepickerTW', 'id' => 'contract_match_date', 'style' => 'height: 35px;']) !!}
            @endisset
        </div>


        <label class="form-group col-12">房東</label>
        <!-- Landlord Item Number Field -->
        <div class="form-group col-2">
            {!! Form::label('landlord_item_number', '物件編號:', ['class' => 'necessary']) !!}
            {!! Form::text('landlord_item_number', null, ['class' => 'form-control', 'id' => 'landlordItemNumber']) !!}
        </div>

        <!-- Landlord Match Number Field -->
        <div class="form-group col-2">
            {!! Form::label('landlord_match_number', '媒合編號:', ['class' => 'necessary']) !!}
            {!! Form::text('landlord_match_number', null, ['class' => 'form-control', 'maxlength' => 11]) !!}
        </div>


        <!-- Landlord Existing Tenant Field -->
        <div class="form-group col-1">
            {!! Form::label('landlord_existing_tenant', '現有房客:', ['class' => 'necessary']) !!}
            {!! Form::select('landlord_existing_tenant', ['是' => '是', '否' => '否'], null, ['class' => 'form-control']) !!}
            {{-- {!! Form::text('landlord_existing_tenant', null, ['class' => 'form-control']) !!} --}}
        </div>


        <!-- Landlord Contract Date Field -->
        <div class="form-group col-2">
            {!! Form::label('landlord_contract_date', '契約起日:', ['class' => 'necessary']) !!}
            @isset($contract)
                {!! Form::text('landlord_contract_date', (int) date('Y', strtotime($contract->landlord_contract_date)) - 1911 . date('-m-d', strtotime($contract->landlord_contract_date)), ['class' => 'form-control datepickerTW', 'id' => 'landlord_contract_date', 'style' => 'height: 35px;']) !!}
            @else
                {!! Form::text('landlord_contract_date', null, ['class' => 'form-control datepickerTW', 'id' => 'landlord_contract_date', 'style' => 'height: 35px;']) !!}
            @endisset
        </div>


        <!-- Landlord Contract Expiry Date Field -->
        <div class="form-group col-2">
            {!! Form::label('landlord_contract_expiry_date', '契約訖日:', ['class' => 'necessary']) !!}
            @isset($contract)
                {!! Form::text('landlord_contract_expiry_date', (int) date('Y', strtotime($contract->landlord_contract_expiry_date)) - 1911 . date('-m-d', strtotime($contract->landlord_contract_expiry_date)), ['class' => 'form-control datepickerTW', 'id' => 'landlord_contract_expiry_date', 'style' => 'height: 35px;']) !!}
            @else
                {!! Form::text('landlord_contract_expiry_date', null, ['class' => 'form-control datepickerTW', 'id' => 'landlord_contract_expiry_date', 'style' => 'height: 35px;']) !!}
            @endisset
        </div>

        <!-- Landlord Ministry Of The Interior Number Field -->
        <div class="form-group col-2">
            {!! Form::label('landlord_ministry_of_the_interior_number', '內政部編號:', ['class' => 'necessary']) !!}
            {!! Form::text('landlord_ministry_of_the_interior_number', null, ['class' => 'form-control', 'maxlength' => '11']) !!}
        </div>


        <!-- Landlord Types Of Field -->
        <div class="form-group col-1">
            {!! Form::label('landlord_types_of', '類型:', ['class' => 'necessary']) !!}
            {!! Form::select('landlord_types_of', ['包租' => '包租', '代管' => '代管', '租賃' => '租賃', '買賣' => '買賣'], null, ['class' => 'form-control', 'id' => 'types']) !!}
            {{-- {!! Form::text('landlord_types_of', null, ['class' => 'form-control']) !!} --}}
        </div>


        <label class="form-group col-12">房客</label>
        <!-- Tenant Case Number Field -->
        <div class="form-group col-6">
            {!! Form::label('tenant_case_number', '案件編號:', ['class' => 'necessary']) !!}
            {!! Form::text('tenant_case_number', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Tenant Ministry Of The Interior Number Field -->
        <div class="form-group col-6">
            {!! Form::label('tenant_ministry_of_the_interior_number', '內政部編號:', ['class' => 'necessary']) !!}
            {!! Form::text('tenant_ministry_of_the_interior_number', null, ['class' => 'form-control', 'maxlength' => '11']) !!}
        </div>


        <label class="form-group col-12">租評表</label>
        <!-- Rent Evaluation Form Market Rent Field -->
        <div class="form-group col-6">
            {!! Form::label('rent_evaluation_form_market_rent', '市價租金:', ['class' => 'necessary']) !!}
            {!! Form::text('rent_evaluation_form_market_rent', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Rent Evaluation Form Assess Rent Field -->
        <div class="form-group col-6">
            {!! Form::label('rent_evaluation_form_assess_rent', '評定租金:', ['class' => 'necessary']) !!}
            {!! Form::text('rent_evaluation_form_assess_rent', null, ['class' => 'form-control']) !!}
        </div>


        <label class="form-group col-12">實際簽約</label>
        <!-- Actual Contract Rent To Landlord Field -->
        <div class="form-group col-3">
            {!! Form::label('actual_contract_rent_to_landlord', '給房東租金:', ['class' => 'necessary']) !!}
            {!! Form::text('actual_contract_rent_to_landlord', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Actual Contract Tenant Pays Rent Field -->
        <div class="form-group col-3">
            {!! Form::label('actual_contract_tenant_pays_rent', '房客自付租金:', ['class' => 'necessary']) !!}
            {!! Form::text('actual_contract_tenant_pays_rent', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Actual Contract Rent Subsidy Field -->
        <div class="form-group col-3">
            {!! Form::label('actual_contract_rent_subsidy', '租金補助:', ['class' => 'necessary']) !!}
            {!! Form::text('actual_contract_rent_subsidy', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Actual Contract Rent Subsidy Field -->
        <div class="form-group col-3">
            {!! Form::label('recive_bank', '收款人銀行名稱:', ['class' => 'necessary']) !!}
            {!! Form::text('recive_bank', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Actual Contract Rent Subsidy Field -->
        <div class="form-group col-3">
            {!! Form::label('recive_bank_code', '收款人分行代號:', ['class' => 'necessary']) !!}
            {!! Form::text('recive_bank_code', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Actual Contract Rent Subsidy Field -->
        <div class="form-group col-3">
            {!! Form::label('recive_bank_account', '收款人帳號:', ['class' => 'necessary']) !!}
            {!! Form::text('recive_bank_account', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Actual Contract Rent Subsidy Field -->
        <div class="form-group col-3">
            {!! Form::label('recive_id', '收款人身分證字號:', ['class' => 'necessary']) !!}
            {!! Form::text('recive_id', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-3">
            {!! Form::label('bank_note', '存摺摘要:', ['class' => 'necessary']) !!}
            {!! Form::text('bank_note', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Actual Contract Deposit Field -->
        <div class="form-group col-3">
            {!! Form::label('actual_contract_deposit', '押金:', ['class' => 'necessary']) !!}
            {!! Form::number('actual_contract_deposit', null, ['class' => 'form-control']) !!}
        </div>


        <!-- To The Landlord Rent Day Field -->
        <div class="form-group col-12">
            {!! Form::label('to_the_landlord_rent_day', __('models/contracts.fields.to_the_landlord_rent_day') . ':', ['class' => 'necessary']) !!}
            {!! Form::text('to_the_landlord_rent_day', null, ['class' => 'form-control']) !!}
        </div>

        {{-- <label class="form-group col-12">包/代管費</label> --}}
        {{-- <!-- Package Escrow Fee Month Minute Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('package_escrow_fee_month_minute', '月分:', ['class' => 'necessary']) !!} --}}
        {{-- @if (isset($contract)) --}}
        {{-- {!! Form::text('package_escrow_fee_month_minute', null, ['class' => 'form-control']) !!} --}}
        {{-- @else --}}
        {{-- {!! Form::text('package_escrow_fee_month_minute', date('m'), ['class' => 'form-control']) !!} --}}
        {{-- @endif --}}
        {{-- </div> --}}


        {{-- <!-- Package Escrow Fee Amount Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('package_escrow_fee_amount', '金額:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('package_escrow_fee_amount', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Package Escrow Fee Number Of Periods Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('package_escrow_fee_number_of_periods', '期數:', ['class' => 'necessary']) !!} --}}
        {{-- @if (isset($contract)) --}}
        {{-- {!! Form::text('package_escrow_fee_number_of_periods', null, ['class' => 'form-control']) !!} --}}
        {{-- @else --}}
        {{-- {!! Form::text('package_escrow_fee_number_of_periods', \Carbon\Carbon::now()->addMonth()->format('m'), ['class' => 'form-control']) !!} --}}
        {{-- @endif --}}
        {{-- </div> --}}
        <label class="form-group col-12">包/代管費</label>
        <div class="col-12 vue_list_box" id="package_escrow_fee">
            <div class="form-group col-xl-4 col-lg-4 col-md-4 col-3">
                <label for="package_escrow_fee_amount" class="necessary">金額:</label>
                <input class="form-control" name="" type="number" v-model="packageAmount"
                    id="package_escrow_fee_amount">
            </div>
            <template v-for="(item, index) in package_escrow_fee_List">

                <div class="field-container row col-12">
                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-4">
                        <label for="package_escrow_fee_year_minute" class="necessary">年份:</label>
                        <select name="" v-model="item.year" id="package_escrow_fee_year_minute" class="form-control">
                            <option :value="i" v-for="i in (100, 300)">@{{ i }}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-4">
                        <label for="package_escrow_fee_month_minute" class="necessary">月份:</label>
                        <select name="" v-model="item.month" id="package_escrow_fee_month_minute"
                            class="form-control">
                            <option :value="i" v-for="i in (1, 12)">@{{ i }}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-3">
                        <label for="package_escrow_fee_number_of_periods" class="necessary">期數:</label>
                        <select name="" v-model="item.freq" id="package_escrow_fee_number_of_periods"
                            class="form-control">
                            <option value="12">12</option>
                            <option value="24">24</option>
                        </select>
                    </div>
                    <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-1 col-2">
                        <a class="abtn abtn-add" @click="addField('package_escrow_fee')"><i
                                class="fas fa-plus fa-fw"></i></a>
                        <a class="abtn abtn-delete" @click="deleteField('package_escrow_fee',index)" v-if="index > 0"><i
                                class="fas fa-trash-alt fa-fw"></i></a>
                    </div>
                </div>
            </template>
        </div>

        {{-- <label class="form-group col-12">租金補助</label> --}}
        {{-- <!-- Rent Subsidy Month Minute Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('rent_subsidy_month_minute', '月分:', ['class' => 'necessary']) !!} --}}
        {{-- @if (isset($contract)) --}}
        {{-- {!! Form::text('rent_subsidy_month_minute', null, ['class' => 'form-control']) !!} --}}
        {{-- @else --}}
        {{-- {!! Form::text('rent_subsidy_month_minute', date('m'), ['class' => 'form-control']) !!} --}}
        {{-- @endif --}}
        {{-- </div> --}}


        {{-- <!-- Rent Subsidy Amount Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('rent_subsidy_amount', '金額:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('rent_subsidy_amount', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Rent Subsidy Number Of Periods Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('rent_subsidy_number_of_periods', '期數:', ['class' => 'necessary']) !!} --}}
        {{-- @if (isset($contract)) --}}
        {{-- {!! Form::text('rent_subsidy_number_of_periods', null, ['class' => 'form-control']) !!} --}}
        {{-- @else --}}
        {{-- {!! Form::text('rent_subsidy_number_of_periods', \Carbon\Carbon::now()->addMonth()->format('m'), ['class' => 'form-control']) !!} --}}
        {{-- @endif --}}
        {{-- </div> --}}
        <label class="form-group col-12">租金補助</label>
        <div class="col-12 vue_list_box" id="rent_subsidy">
            <!-- 租金補助 -->

            <template v-for="(item,index) in rent_subsidy_List">
                <div class="field-container row col-12">
                    <div class="form-group col-3">
                        <label for="package_escrow_fee_year_minute" class="necessary">年份:</label>
                        <select name="" v-model="item.year" id="package_escrow_fee_year_minute" class="form-control">
                            <option :value="i" v-for="i in (100, 300)">@{{ i }}</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="rent_subsidy_month_minute" class="necessary">月分:</label>
                        <input class="form-control" name="" type="text" v-model="item.month"
                            id="rent_subsidy_month_minute">
                    </div>
                    <div class="form-group col-3">
                        <label for="rent_subsidy_amount" class="necessary">金額:</label>
                        <input class="form-control" name="" type="text" v-model="item.money" id="rent_subsidy_amount">
                    </div>
                    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-3">
                        <label for="rent_subsidy_number_of_periods" class="necessary">期數:</label>
                        <input class="form-control" name="" type="text" v-model="item.freq"
                            id="rent_subsidy_number_of_periods">
                    </div>
                    <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-1 col-2">
                        <a class="abtn abtn-add" @click="addField('rent_subsidy')"><i
                                class="fas fa-plus fa-fw"></i></a>
                        <a class="abtn abtn-delete" @click="deleteField('rent_subsidy',index)" v-if="index >= 1"><i
                                class="fas fa-trash-alt fa-fw"></i></a>
                    </div>
                </div>
            </template>
        </div>
        {{-- <label class="form-group col-12">開發/媒合費</label> --}}
        {{-- <!-- Development Matching Fees Month Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('development_matching_fees_month', '月份:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::date('development_matching_fees_month', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Development Matching Fees Amount Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('development_matching_fees_amount', '金額:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('development_matching_fees_amount', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}
        <label class="form-group col-12">開發/媒合費</label>
        <div class="col-12 vue_list_box" id="development_matching_fees">

            <template v-for="(item,index) in development_matching_fees_List">
                <div class="field-container row col-12">
                    <div class="form-group col-3">
                        <label for="development_matching_fees_month" class="necessary">年份:</label>
                        <select name="" v-model="item.year" id="development_matching_fees_month"
                            class="form-control">
                            <option :value="i" v-for="i in (100, 300)">@{{ i }}</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="development_matching_fees_month" class="necessary">月分:</label>
                        <select name="" v-model="item.month" id="development_matching_fees_month"
                            class="form-control">
                            <option :value="i" v-for="i in (1, 12)">@{{ i }}</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="development_matching_fees_amount" class="necessary">金額:</label>
                        <input class="form-control" name="" type="number" v-model="item.money"
                            id="development_matching_fees_amount">
                    </div>
                    <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-2 col-2">
                        <a class="abtn abtn-add" @click="addField('development_matching_fees')"><i
                                class="fas fa-plus fa-fw"></i></a>
                        <a class="abtn abtn-delete" @click="deleteField('development_matching_fees',index)"
                            v-if="index >= 1"><i class="fas fa-trash-alt fa-fw"></i></a>
                    </div>
                </div>
            </template>
        </div>

        {{-- <label class="form-group col-12">公證費</label> --}}
        {{-- <!-- Notary Fees Notarization Date Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('notary_fees_notarization_date', '公證日:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('notary_fees_notarization_date', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Notary Fees Month Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('notary_fees_month', '月份:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('notary_fees_month', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Notary Fees Amount Field --> --}}
        {{-- <div class="form-group col-4"> --}}
        {{-- {!! Form::label('notary_fees_amount', '金額:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('notary_fees_amount', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}

        <label class="form-group col-12">公證費</label>
        <div class="col-12 vue_list_box" id="notary_fees">

            <template v-for="(item,index) in notary_fees_List">
                <div class="field-container row col-12">
                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-4">
                        <label for="notary_fees_notarization_date" class="necessary">公證日:</label>
                        <input class="form-control datepickerTW" name="" type="text" v-model="item.date"
                            id="notary_fees_notarization_date">
                    </div>
                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-3">
                        <label for="notary_fees_month" class="necessary">申請月報月份:</label>
                        <select name="" v-model="item.month" id="notary_fees_month" class="form-control">
                            <option :value="i" v-for="i in (1, 12)">@{{ i }}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-3">
                        <label for="notary_fees_amount" class="necessary">金額:</label>
                        <input class="form-control" name="" type="number" v-model="item.money"
                            id="notary_fees_amount">
                    </div>
                    <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-1 col-2">
                        <a class="abtn abtn-add" @click="addField('notary_fees')"><i
                                class="fas fa-plus fa-fw"></i></a>
                        <a class="abtn abtn-delete" @click="deleteField('notary_fees',index)" v-if="index >=1"><i
                                class="fas fa-trash-alt fa-fw"></i></a>
                    </div>
                </div>
            </template>
        </div>

        {{-- <label class="form-group col-12">修繕費</label> --}}
        {{-- <!-- Repair Cost Month Minute Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('repair_cost_month_minute', '月分:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('repair_cost_month_minute', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Repair Cost Amount Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('repair_cost_amount', '金額:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('repair_cost_amount', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}
        <label class="form-group col-12">修繕費</label>
        <div class="col-12 vue_list_box" id="repair_cost">

            <template v-for="(item,index) in repair_cost_List">
                <div class="field-container row col-12">
                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-6">
                        <label for="repair_cost_month_minute" class="necessary">月分:</label>
                        <input class="form-control" name="" type="number" v-model="item.month" max="12" min="1"
                            maxlength="2" id="repair_cost_month_minute" readonly>
                    </div>
                    <div class="form-group col-xl-5 col-lg-5 col-md-4 col-4">
                        <label for="repair_cost_amount" class="necessary">金額:</label>
                        <input class="form-control" name="" type="number" v-model="item.money"
                            id="repair_cost_amount" readonly>
                    </div>
                    {{-- <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-2 col-2"> --}}
                    {{-- <a class="abtn abtn-add" @click="addField('repair_cost')"><i class="fas fa-plus fa-fw"></i></a> --}}
                    {{-- <a class="abtn abtn-delete" @click="deleteField('repair_cost',index)" v-if="index >=1"><i --}}
                    {{-- class="fas fa-trash-alt fa-fw"></i></a> --}}
                    {{-- </div> --}}
                </div>
            </template>
        </div>

        {{-- <label class="form-group col-12">保險費</label> --}}
        {{-- <!-- Insurance Month Minute Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('insurance_month_minute', '月分:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('insurance_month_minute', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Insurance Amount Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('insurance_amount', '金額:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('insurance_amount', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}
        <label class="form-group col-12">保險費</label>
        <div class="col-12 vue_list_box" id="insurance">

            <template v-for="(item,index) in insurance_List">
                <div class="field-container row col-12">
                    <div class="form-group col-2">
                        <label for="insurance_month_minute" class="necessary">年度:</label>
                        <input class="form-control" name="" type="text" v-model="item.year" id="insurance_year">
                    </div>
                    <div class="form-group col-2">
                        <label for="insurance_month_minute" class="necessary">月分:</label>
                        <input class="form-control" name="" type="text" v-model="item.month"
                            id="insurance_month_minute">
                    </div>
                    <div class="form-group col-2">
                        <label for="insurance_amount" class="necessary">金額:</label>
                        <input class="form-control" name="" type="text" v-model="item.money" id="insurance_amount">
                    </div>
                    <div class="form-group col-2">
                        <label for="insurance_amount" class="necessary">起訖日:</label>
                        <input class="form-control datepickerTW" name="" type="text" v-model="item.date"
                            id="insurance_date">
                    </div>
                    <div class="form-group col-3">
                        <label for="insurance_company" class="necessary">保險業者:</label>
                        <input class="form-control" name="" type="text" v-model="item.company"
                            id="insurance_company">
                    </div>
                    <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-2 col-2">
                        <a class="abtn abtn-add" @click="addField('insurance')"><i class="fas fa-plus fa-fw"></i></a>
                        <a class="abtn abtn-delete" @click="deleteField('insurance',index)" v-if="index >=1 "><i
                                class="fas fa-trash-alt fa-fw"></i></a>
                    </div>
                </div>
            </template>
        </div>

        <?php
            if(isset($contract)){
                $park_mang_fee_get = \App\Models\BuildingManagement::where("landlord_id", auth()->user()->id)->first();
                $park_mang_fee_get = $park_mang_fee_get->park_mang_fee;
            }
        ?>
        <label class="form-group col-12">車位管理費</label>
        <!-- Repair Cost Month Minute Field -->
        <div class="form-group col-6">
            {!! Form::label('park_mang_fee', '金額:', ['class' => 'necessary']) !!}
            @isset($park_mang_fee_get)
                <input type="number" name="park_mang_fee" class="form-control" id="park_mang_fee"
                    value="{{ @$park_mang_fee_get }}">
            @else
                <input type="number" name="park_mang_fee" class="form-control" id="park_mang_fee"
                    value="{{ @$contract->park_mang_fee }}">
            @endisset
            {{-- <input type="number" name="park_mang_fee" class="form-control" id="park_mang_fee" value="{{@$contract->park_mang_fee}}"> --}}
            {{-- {!! Form::number('park_mang_fee', null, ['class' => 'form-control', 'id' => 'park_mang_fee']) !!} --}}
        </div>

        {{-- <label class="form-group col-12">保險期間</label> --}}
        {{-- <div class="col-12 vue_list_box" id="insurance_day"> --}}

        {{-- <template v-for="(item,index) in insurance_day_list"> --}}
        {{-- <div class="field-container row col-12"> --}}
        {{-- <div class="form-group col-xl-6 col-lg-6 col-md-6 col-6"> --}}
        {{-- <label for="insurance_month_minute" class="necessary">年度:</label> --}}
        {{-- <input class="form-control" name="" type="text" v-model="item.year" id="insurance_year"> --}}
        {{-- </div> --}}
        {{-- <div class="form-group col-xl-5 col-lg-5 col-md-4 col-4"> --}}
        {{-- <label for="insurance_amount" class="necessary">起訖日:</label> --}}
        {{-- <input class="form-control datepickerTW" name="" type="text" v-model="item.date" --}}
        {{-- id="insurance_date"> --}}
        {{-- </div> --}}
        {{-- <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-2 col-2"> --}}
        {{-- <a class="abtn abtn-add" @click="addField('insurance_day')"><i --}}
        {{-- class="fas fa-plus fa-fw"></i></a> --}}
        {{-- <a class="abtn abtn-delete" @click="deleteField('insurance_day',index)" v-if="index >=1 "><i --}}
        {{-- class="fas fa-trash-alt fa-fw"></i></a> --}}
        {{-- </div> --}}
        {{-- </div> --}}
        {{-- </template> --}}
        {{-- </div> --}}
        {{-- <!-- Insurance Period Three Year Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('insurance_period_three_year', '年度:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::select('insurance_period_three_year', ['1' => '1', '2' => '2', '3' => '3'], null, ['class' => 'form-control']) !!} --}}
        {{--  --}}{{-- {!! Form::text('insurance_period_three_year', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}


        {{-- <!-- Insurance Period Three Start And End Date Field --> --}}
        {{-- <div class="form-group col-6"> --}}
        {{-- {!! Form::label('insurance_period_three_start_and_end_date', '起訖日:', ['class' => 'necessary']) !!} --}}
        {{-- {!! Form::text('insurance_period_three_start_and_end_date', null, ['class' => 'form-control']) !!} --}}
        {{-- </div> --}}



        <div class="form-group col-12">
            {!! Form::label('real_date', '實價登錄日期:', ['class' => 'necessary']) !!}
            {!! Form::date('real_date', null, ['class' => 'form-control']) !!}
        </div>



        <div class="form-group col-12">
            {!! Form::label('real_code', '實價登錄編碼:', ['class' => 'necessary']) !!}
            {!! Form::text('real_code', null, ['class' => 'form-control']) !!}
            <input type="hidden" v-model="JSON.stringify(real_code)">
            <template v-for="(item,index) in real_code_List">
                <div class="field-container row col-12">
                    <div class="form-group col-3">
                        <label for="" class="necessary">實價登入編碼:</label>
                        <input class="form-control" name="" type="text" v-model="item.code" id="">
                    </div>
                    <div class="form-group col-3">
                        <label for="rent_subsidy_month_minute" class="necessary">註銷:</label>
                        <select name="" id="" v-model="item.cancel" class="form-control">
                            <option value="1">是</option>
                            <option value="0">否</option>
                        </select>
                    </div>
                    <div class="abtn-padding form-group col-xl-1 col-lg-1 col-md-1 col-2">
                        <a class="abtn abtn-add" @click="addField('real_code')"><i class="fas fa-plus fa-fw"></i></a>
                        <a class="abtn abtn-delete" @click="deleteField('real_code',index)" v-if="index >= 1"><i
                                class="fas fa-trash-alt fa-fw"></i></a>
                    </div>
                </div>
            </template>
        </div>


        <div class="form-group col-12">
            {!! Form::label('資訊提供日期', '資訊提供日期:', ['class' => 'necessary']) !!}
            {!! Form::date('資訊提供日期', null, ['class' => 'form-control']) !!}
        </div>



        <div class="form-group col-12">
            {!! Form::label('info_code', '資訊提供編碼:', ['class' => 'necessary']) !!}
            {!! Form::text('info_code', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Remark 1 Field -->
        <div class="form-group col-12">
            {!! Form::label('remark_1', __('models/contracts.fields.remark_1') . ':', ['class' => 'necessary']) !!}
            {!! Form::text('remark_1', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Remark 2 Field -->
        <div class="form-group col-12">
            {!! Form::label('remark_2', __('models/contracts.fields.remark_2') . ':', ['class' => 'necessary']) !!}
            {!! Form::text('remark_2', null, ['class' => 'form-control']) !!}
        </div>


        <div class="form-group col-sm-3">
            <label for="user_file">契約書</label>
            <input type="file" name="contract_book" id="file_upload" class="form-control">
        </div>

        <div class="form-group col-sm-3">
            <label for="user_file">包租契約書</label>
            <input type="file" name="b_contract_book" id="file_upload" class="form-control">
        </div>

        <div class="form-group col-sm-3">
            <label for="user_file">轉租契約書</label>
            <input type="file" name="t_contract_book" id="file_upload" class="form-control">
        </div>

        <div class="form-group col-sm-3">
            <label for="user_file">公證費收據</label>
            <input type="file" name="n_invoke" id="file_upload" class="form-control">
        </div>
        <div class="form-group col-sm-3">
            <label for="user_file">保險要保書</label>
            <input type="file" name="i_book" id="file_upload" class="form-control">
        </div>
        <div class="form-group col-sm-3">
            <label for="user_file">個別磋商條款</label>
            <input type="file" name="s_book" id="file_upload" class="form-control">
        </div>

        <div class="form-group col-sm-3">
            <label for="user_file">終止契約遷出交屋完成確認書</label>
            <input type="file" name="stop_book" id="file_upload" class="form-control">
        </div>
        <div class="form-group col-sm-3">
            <label for="user_file">公文</label>
            <input type="file" name="g_book" id="file_upload" class="form-control">
        </div>
        <div class="form-group col-sm-3">
            <label for="user_file">所有權人變更收款帳號</label>
            <input type="file" name="o_book" id="file_upload" class="form-control">
        </div>


        <!-- Submit Field -->

    </div>

    @if (isset($contract))
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary', 'id' => 'submit_btn']) !!}
    @else
        <div class="form-group col-sm-12">
            {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('contracts.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
        </div>
    @endif

    @if (isset($contract) && $contract->tenantManagement)
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">合約附件</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <select name="status" id="status" class="form-control">
                            <option value="招租中" selected>招租中</option>
                            <option value="退出計畫">退出計畫</option>
                        </select>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="changB()">送出</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </div>
            </div>
        </div>

</div>
@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        document.addEventListener(
            "click",
            function(event) {
                // If user either clicks X button OR clicks outside the modal window, then close modal by calling closeModal()
                if (
                    !event.target.matches("#f_b")
                ) {
                    $("#Fix_Modal").fadeOut();
                }
                if (!event.target.matches("#t_b")) {
                    $("#Tenant_Modal").fadeOut();
                }
                if (!event.target.matches("#b_b")) {
                    $("#Build_Modal").fadeOut();
                }
            },
            false
        )

        function ajaxCU(fill, count) {
            const id = '#shadow-form' + count
            if (fill) {
                Object.keys(fill).forEach(key => {
                    const input = document.querySelector(`${id} [name="${key}"]`)
                    if (!input) return
                    if (input.getAttribute('type') === 'checkbox') input.checked = fill[key] ? true : false
                    else if (input.getAttribute('type') === 'date') input.value = convert(fill[key])
                    else if (input.getAttribute('type') === 'file') {} else input.value = fill[key]
                })

            }
            $(id).show()
        }
        ajaxCU.close = () => {
            $('#shadow-form1').hide()
            $('#shadow-form2').hide()
            $('#shadow-form3').hide()
        }

        function convert(str) {
            var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-");
        }

        function changeAllB() {
            var data = $("#contract_form").serialize();
            console.log(data);
            $.ajax({
                type: "PATCH",
                timeout: 60000,
                url: "/api/building_managements/" + {{ $contract->buildingManagement->id }},
                data: data,
                success: function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: '更新成功',
                    })
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: '更新失敗',
                    })
                },
            });
        }

        function changeAllT() {
            var data = $("#contract_form").serialize();
            console.log(data);
            $.ajax({
                type: "PATCH",
                timeout: 60000,
                url: "/api/tenant_managements/" + {{ $contract->tenantManagement->id }},
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: '更新成功',
                    })
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: '更新失敗',
                    })
                },
            });
        }

        function changB() {
            $.ajax({
                type: "PATCH",
                timeout: 60000,
                url: "/api/building_managements/" + {{ $contract->buildingManagement->id }},
                data: {
                    'status': $('#exampleModal #status').val()
                },
                success: function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: '房東狀態 更新成功',
                    })
                    changT();
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: '房東狀態  更新失敗',
                    })
                },
            });
        }

        function changT() {
            $.ajax({
                type: "PATCH",
                timeout: 60000,
                url: "/api/tenant_managements/" + {{ $contract->tenantManagement->id }},
                data: {
                    'status': "退出"
                },
                success: function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: '房客狀態 更新成功',
                    })
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: '房客狀態 更新失敗',
                    })
                },
            });
        }
    </script>
@endpush
@endif
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
    <script>
        window.vm = new Vue({
            el: "#contract_all_field",
            data: {
                packageAmount: 0,
                index: 0,
                package_escrow_fee_List: [],
                insurance_day_list: [],
                rent_subsidy_List: [],
                @if (isset($contract) && $contract->real_code)
                    {!! $contract->real_code !!}
                @else
                    []
                @endif,
                development_matching_fees_List: [],
                notary_fees_List: [],
                repair_cost_List: [],
                insurance_List: [],
                list_name_map: {},
                divs: [{
                    'id': 1
                }],
                @if (isset($contract_id))
                    {!! $contract_id !!}
                @else
                    ''
                @endif,
                packageEscrowFee: {!! json_encode($packageEscrowFee) !!},
                rentSubsidy: {!! json_encode($rentSubsidy) !!},
                developmentMatchingFees: {!! json_encode($developmentMatchingFees) !!},
                notaryFees: {!! json_encode($notaryFees) !!},
                repairCost: {!! json_encode($repairCost) !!},
                insurance: {!! json_encode($insurance) !!},
                insurance_day: {!! json_encode($insurance_day) !!},

            },
            created: function() {

            },
            mounted: function() {
                //初始化映射map
                this.list_name_map['package_escrow_fee'] = this.package_escrow_fee_List;
                this.list_name_map['rent_subsidy'] = this.rent_subsidy_List;
                this.list_name_map['real_code'] = this.real_code_List;
                this.list_name_map['notary_fees'] = this.notary_fees_List;
                this.list_name_map['repair_cost'] = this.repair_cost_List;
                this.list_name_map['insurance'] = this.insurance_List;
                this.list_name_map['development_matching_fees'] = this.development_matching_fees_List;
                this.list_name_map['insurance_day'] = this.insurance_day_list;

                @if ($housePayHistory != null)
                    this.recover_data( {!! json_encode($housePayHistory) !!} )
                @else
                    var map_keys = Object.keys(this.list_name_map);
                    var self = this;
                    map_keys.forEach(key => {
                    self.create_new_package(key);
                    });
                @endif
            },
            methods: {
                recover_data(recover_data) {

                    var self = this;

                    var map_keys = Object.keys(this.list_name_map);
                    var worked_map_keys = [];
                    recover_data.forEach(element => {

                        var type_str = element.type;
                        var work_list = this.list_name_map[type_str];
                        // console.log(work_list)
                        if (work_list == undefined) {
                            // console.log(type_str);
                        }
                        work_list.push(element);
                        worked_map_keys.push(type_str);
                    });

                    map_keys.forEach(key => {
                        if (!worked_map_keys.hasOwnProperty(key)) {
                            self.create_new_package(key);
                        }

                    });

                },
                //創一列包租代管費
                create_new_package(list_name) {
                    var work_list = this.list_name_map[list_name];
                    switch (list_name) {
                        case 'package_escrow_fee':
                            var pack = {
                                'year': 110,
                                'money': this.packageAmount,
                                'month': 01,
                                'freq': 12
                            };
                            break;
                        case 'rent_subsidy':
                            var pack = {
                                'year': 110,
                                'money': 0,
                                'month': 01,
                                'freq': 12
                            };
                            break;
                        case 'real_code':
                            var pack = {
                                'code': '',
                                'cancel': '0',
                            };
                            break;
                        case 'notary_fees':
                            var pack = {
                                'year': 110,
                                'money': 0,
                                'month': 01,
                                'date': new Date().toISOString().slice(0, 10)
                            };
                            break;
                        case 'repair_cost':
                            var pack = {
                                'year': 110,
                                'money': 0,
                                'month': 01
                            };
                            break;
                        case 'insurance':
                            var pack = {
                                'year': 110,
                                'money': 0,
                                'month': '01',
                                'company': '旺旺友聯',
                                'date': 1
                            };
                            break;
                        case 'development_matching_fees':
                            var pack = {
                                'year': 110,
                                'money': 0,
                                'month': 01
                            };
                            break;
                        case 'insurance_day':
                            var pack = {
                                'date': 1,
                                'year': 1
                            };
                            break;
                        default:
                            return 0;
                            break;
                    }

                    work_list.push(pack);

                },
                addField(list_name) {
                    this.create_new_package(list_name);
                },
                deleteField(list_name, index) {
                    var work_list = this.list_name_map[list_name];
                    work_list.splice(index, 1);
                },
                //儲存所有費用資料
                postPayHistory() {
                    this.chackPackageEscrowFee();
                    this.chackRentSubsidy();
                    this.chackDevelopmentMatchingFees();
                    this.chackNotaryFees();
                    this.chackRepairCost();
                    this.chackInsurance();
                },

                //包代管費
                chackPackageEscrowFee() {
                    var pef_data = window.vm.package_escrow_fee_List;
                    for (let index = 0; index < pef_data.length; index++) {
                        console.log(pef_data[index]);
                        var park = {};
                        park['contract_id'] = this.contract_id;
                        park['type'] = 'package_escrow_fee';
                        park['money'] = pef_data[index]['money'];
                        park['month'] = pef_data[index]['month'];
                        park['freq'] = pef_data[index]['freq'];
                        park['year'] = pef_data[index]['year'];

                        var id = null;
                        if (this.packageEscrowFee.length > 0) {
                            for (let i = 0; i < this.packageEscrowFee.length; i++) {
                                if (this.packageEscrowFee[i]['month'] == park['month'] && this.packageEscrowFee[i][
                                        'year'
                                    ] == park['year']) {
                                    id = this.packageEscrowFee[i]['id'];
                                    this.updateData(id, park);
                                }
                            }
                            if (id == null) {
                                this.postData(park);
                            }
                        } else {
                            this.postData(park);
                        }
                    }
                },

                //租金補助
                chackRentSubsidy() {
                    var rs_data = window.vm.rent_subsidy_List;
                    for (let index = 0; index < rs_data.length; index++) {
                        console.log(rs_data[index]);
                        var park = {};
                        park['contract_id'] = this.contract_id;
                        park['type'] = 'rent_subsidy';
                        park['money'] = rs_data[index]['money'];
                        park['month'] = rs_data[index]['month'];
                        park['freq'] = rs_data[index]['freq'];
                        park['year'] = rs_data[index]['year'];

                        var id = null;
                        if (this.rentSubsidy.length > 0) {
                            for (let i = 0; i < this.rentSubsidy.length; i++) {
                                if (this.rentSubsidy[i]['month'] == park['month'] && this.rentSubsidy[i]['year'] ==
                                    park['year']) {
                                    id = this.rentSubsidy[i]['id'];
                                    this.updateData(id, park);
                                }
                            }
                            if (id == null) {
                                this.postData(park);
                            }
                        } else {
                            this.postData(park);
                        }
                    }
                },

                //開發媒合費
                chackDevelopmentMatchingFees() {
                    var dmf_data = window.vm.development_matching_fees_List;
                    for (let index = 0; index < dmf_data.length; index++) {
                        console.log(dmf_data[index]);
                        var park = {};
                        park['contract_id'] = this.contract_id;
                        park['type'] = 'development_matching_fees';
                        park['money'] = dmf_data[index]['money'];
                        park['month'] = dmf_data[index]['month'];
                        park['year'] = dmf_data[index]['year'];

                        var id = null;
                        if (this.developmentMatchingFees.length > 0) {
                            for (let i = 0; i < this.developmentMatchingFees.length; i++) {
                                if (this.developmentMatchingFees[i]['month'] == park['month'] && this
                                    .developmentMatchingFees[i]['year'] == park['year']) {
                                    id = this.developmentMatchingFees[i]['id'];
                                    this.updateData(id, park);
                                }
                            }
                            if (id == null) {
                                this.postData(park);
                            }
                        } else {
                            this.postData(park);
                        }
                    }
                },

                //公證費
                chackNotaryFees() {
                    var nf_data = window.vm.notary_fees_List;
                    for (let index = 0; index < nf_data.length; index++) {
                        console.log(nf_data[index]);
                        var park = {};
                        park['contract_id'] = this.contract_id;
                        park['type'] = 'notary_fees';
                        park['money'] = nf_data[index]['money'];
                        park['month'] = nf_data[index]['month'];
                        park['freq'] = nf_data[index]['date'];
                        park['year'] = nf_data[index]['year'];

                        var id = null;
                        if (this.notaryFees.length > 0) {
                            for (let i = 0; i < this.notaryFees.length; i++) {
                                if (this.notaryFees[i]['month'] == park['month'] && this.notaryFees[i]['year'] ==
                                    park['year']) {
                                    id = this.notaryFees[i]['id'];
                                    this.updateData(id, park);
                                }
                            }
                            if (id == null) {
                                this.postData(park);
                            }
                        } else {
                            this.postData(park);
                        }
                    }
                },

                //修繕費
                chackRepairCost() {
                    var rc_data = window.vm.repair_cost_List;
                    for (let index = 0; index < rc_data.length; index++) {
                        console.log(rc_data[index]);
                        var park = {};
                        park['contract_id'] = this.contract_id;
                        park['type'] = 'repair_cost';
                        park['money'] = rc_data[index]['money'];
                        park['month'] = rc_data[index]['month'];

                        var id = null;
                        if (this.repairCost.length > 0) {
                            for (let i = 0; i < this.repairCost.length; i++) {
                                if (this.repairCost[i]['month'] == park['month']) {
                                    id = this.repairCost[i]['id'];
                                    this.updateData(id, park);
                                }
                            }
                            if (id == null) {
                                this.postData(park);
                            }
                        } else {
                            this.postData(park);
                        }
                    }
                },

                //保險費
                chackInsurance() {
                    var isr_data = window.vm.insurance_List;
                    for (let index = 0; index < isr_data.length; index++) {
                        console.log(isr_data[index]);
                        var park = {};
                        park['contract_id'] = this.contract_id;
                        park['type'] = 'insurance';
                        park['money'] = isr_data[index]['money'];
                        park['month'] = isr_data[index]['month'];

                        var id = null;
                        if (this.insurance.length > 0) {
                            for (let i = 0; i < this.insurance.length; i++) {
                                if (this.insurance[i]['month'] == park['month']) {
                                    id = this.insurance[i]['id'];
                                    this.updateData(id, park);
                                }
                            }
                            if (id == null) {
                                this.postData(park);
                            }
                        } else {
                            this.postData(park);
                        }
                    }
                },


                postData(data) {
                    $.ajax({
                        type: "POST",
                        timeout: 60000,
                        url: "/api/house_pay_histories/",
                        data: data,
                        success: function(data) {
                            console.log(data);
                        },
                        error: function(data) {
                            Swal.fire({
                                icon: 'error',
                                title: '失敗',
                            })
                        },
                    });
                },


                updateData(id, data) {
                    $.ajax({
                        type: "PATCH",
                        timeout: 60000,
                        url: "/api/house_pay_histories/" + id,
                        data: data,
                        success: function(data) {
                            console.log(data);
                        },
                        error: function(data) {
                            Swal.fire({
                                icon: 'error',
                                title: '失敗',
                            })
                        },
                    });
                }

            }

        })
    </script>
@endpush
@push('scripts')
    <script>
        var buildData = '';

        function faker_send() {
            window.vm.postPayHistory();
            $('#contract_form').addClass('blur');
            $('#contract_form').submit();
        }

        $('#landlordItemNumber').on('change', function() {
            $.ajax({
                type: "GET",
                url: `/api/building_managements?num=${this.value}`,
                data: "",
                success: function(data) {
                    buildData = data.data[0];
                    if (buildData.sublet === 1) {
                        $('#types').val('包租')
                    } else if (buildData.rent === 1) {
                        $('#types').val('代管')
                    }
                    $('#park_mang_fee').val(buildData.park_mang_fee);
                },
                error: function(ajaxContext) {

                }
            });
        });
    </script>
@endpush
@push('scripts')
    @once
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
        <script>
            (function() {


                var yearTextSelector = '.ui-datepicker-year';

                var dateNative = new Date(),
                    dateTW = new Date(
                        dateNative.getFullYear() - 1911,
                        dateNative.getMonth(),
                        dateNative.getDate()
                    );


                function leftPad(val, length) {
                    var str = '' + val;
                    while (str.length < length) {
                        str = '0' + str;
                    }
                    return str;
                }

                // 應該有更好的做法
                var funcColle = {
                    onSelect: {
                        basic: function(dateText, inst) {
                            /*
                            var yearNative = inst.selectedYear < 1911
                                ? inst.selectedYear + 1911 : inst.selectedYear;*/
                            dateNative = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

                            // 年分小於100會被補成19**, 要做例外處理
                            var yearTW = inst.selectedYear > 1911 ?
                                leftPad(inst.selectedYear - 1911, 4) :
                                inst.selectedYear;
                            var monthTW = leftPad(inst.selectedMonth + 1, 2);
                            var dayTW = leftPad(inst.selectedDay, 2);
                            console.log(monthTW);
                            dateTW = new Date(
                                yearTW + '-' +
                                monthTW + '-' +
                                dayTW + 'T00:00:00.000Z'
                            );
                            console.log(dateTW);
                            return $.datepicker.formatDate(twSettings.dateFormat, dateTW);
                        }
                    }
                };

                var twSettings = {
                    closeText: '關閉',
                    prevText: '上個月',
                    nextText: '下個月',
                    currentText: '今天',
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                    dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                    dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                    weekHeader: '周',
                    dateFormat: 'yy/mm/dd',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: true,
                    yearSuffix: '年',
                    yearRange: "2022:1912",

                    onSelect: function(dateText, inst) {
                        $(this).val(funcColle.onSelect.basic(dateText, inst));
                        if (typeof funcColle.onSelect.newFunc === 'function') {
                            funcColle.onSelect.newFunc(dateText, inst);
                        }
                    }
                };

                // 把yearText換成民國
                var replaceYearText = function() {
                    var $yearText = $('.ui-datepicker-year');

                    if (twSettings.changeYear !== true) {
                        $yearText.text('民國' + dateTW.getFullYear());
                    } else {
                        // 下拉選單
                        if ($yearText.prev('span.datepickerTW-yearPrefix').length === 0) {
                            $yearText.before("<span class='datepickerTW-yearPrefix'>民國</span>");
                        }
                        $yearText.children().each(function() {
                            if (parseInt($(this).text()) > 1911) {
                                $(this).text(parseInt($(this).text()) - 1911);
                            }
                        });
                    }
                };

                $.fn.datepickerTW = function(options) {

                    // setting on init,
                    if (typeof options === 'object') {
                        //onSelect例外處理, 避免覆蓋
                        if (typeof options.onSelect === 'function') {
                            funcColle.onSelect.newFunc = options.onSelect;
                            options.onSelect = twSettings.onSelect;
                        }
                        // year range正規化成西元, 小於1911的數字都會被當成民國年
                        if (options.yearRange) {
                            var temp = options.yearRange.split(':');
                            for (var i = 0; i < temp.length; i += 1) {
                                //民國前處理
                                if (parseInt(temp[i]) < 1) {
                                    temp[i] = parseInt(temp[i]) + 1911;
                                } else {
                                    temp[i] = parseInt(temp[i]) < 1911 ?
                                        parseInt(temp[i]) + 1911 :
                                        temp[i];
                                }
                            }
                            options.yearRange = temp[0] + ':' + temp[1];
                        }
                        // if input val not empty
                        if ($(this).val() !== '') {
                            options.defaultDate = $(this).val();
                        }
                    }

                    // setting after init
                    if (arguments.length > 1) {
                        // 目前還沒想到正常的解法, 先用轉換成init setting obj的形式
                        if (arguments[0] === 'option') {
                            options = {};
                            options[arguments[1]] = arguments[2];
                        }
                    }

                    // override settings
                    $.extend(twSettings, options);

                    // init
                    $(this).datepicker(twSettings);

                    // beforeRender
                    $(this).click(function() {
                        var isFirstTime = ($(this).val() === '');

                        // year range and default date

                        if ((twSettings.defaultDate || twSettings.yearRange) && isFirstTime) {

                            if (twSettings.defaultDate) {
                                $(this).datepicker('setDate', twSettings.defaultDate);
                            }

                            // 當有year range時, select初始化設成range的最末年
                            if (twSettings.yearRange) {
                                var $yearSelect = $('.ui-datepicker-year'),
                                    nowYear = twSettings.defaultDate ?
                                    $(this).datepicker('getDate').getFullYear() :
                                    dateNative.getFullYear();

                                $yearSelect.children(':selected').removeAttr('selected');
                                if ($yearSelect.children('[value=' + nowYear + ']').length > 0) {
                                    $yearSelect.children('[value=' + nowYear + ']').attr('selected',
                                        'selected');
                                } else {
                                    $yearSelect.children().last().attr('selected', 'selected');
                                }
                            }
                        } else {
                            @if (!isset($contract))
                                $(this).datepicker('setDate', dateNative);
                            @endif
                        }
                        @if (!isset($contract))
                            $(this).val($.datepicker.formatDate(twSettings.dateFormat, dateTW));
                        @endif
                        replaceYearText();

                        if (isFirstTime) {
                            $(this).val('');
                        }
                    });

                    // afterRender
                    $(this).focus(function() {
                        replaceYearText();
                    });

                    return this;
                };

            })
            ();


            $('.datepickerTW').datepickerTW({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '-10:2018',
                // defaultDate: '86-11-01',

            });
            $('.datepicker').datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '1911:2018',
                // defaultDate: '2016-11-01'
            });
        </script>
    @endonce
@endpush
