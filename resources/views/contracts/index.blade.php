@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">@lang('models/contracts.plural')</h3>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;z-index: 100" href="{{ route('contracts.create') }}">@lang('crud.add_new')</a>
    </div>
    <div class="container-fluid container-fixed-lg" style="margin-top: 40px;">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                {{-- <form action="" id="importFile" method="post" enctype="multipart/form-data">
                    @csrf
                    <div id="app">
                        <button type="button" class="btn btn" onclick="select_file()">選擇檔案</button>
                        <input type="file" name="file" class="hide" id="import_file">
                        <input type="hidden" name="type" value="contract">
                        <button class="btn btn" type="submit">導入</button>
                    </div>
                </form> --}}
                    @include('contracts.table')
            </div>
        </div>
        <div class="text-center">
        </div>
    </div>
@endsection
@push("scripts")
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        $('#importFile').on('submit', function (e) {
            e.preventDefault();
            const formData = new FormData(this);
            $.ajax({
                type: "POST",
                timeout: 60000,
                url: "/api/batch_upload",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    const html =
                    `<table class="table table-responsive" style="max-height: 180px; overflow: auto">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Case</th>
                                <th scope="col">Result</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${ data.map(row => `
                                <tr>
                                    <th scope="row">${row.line}</th>
                                    <td>${row.match_num}</td>
                                    <td>${row.result}</td>
                                </tr>
                            `)}
                        </tbody>
                    </table>`
                    Swal.fire({
                        icon: 'success',
                        title: '匯入成功',
                        html,
                    })
                },
                error: function (data) {
                    Swal.fire({
                        icon: 'error',
                        title: '導入失敗 請確認檔案',
                    })
                },
            });
        })

        function select_file() {
            $('#import_file').val("");
            $('#import_file').click();
        }

    </script>
@endpush

