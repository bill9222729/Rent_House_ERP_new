<!-- Contract Number Of Periods Field -->
<div class="form-group col-12">
    {!! Form::label('contract_number_of_periods', __('models/contracts.fields.contract_number_of_periods').':') !!}
    <p>{{ $contract->contract_number_of_periods }}</p>
</div>


<!-- Contract Category Field -->
<div class="form-group col-12">
    {!! Form::label('contract_category', __('models/contracts.fields.contract_category').':') !!}
    <p>{{ $contract->contract_category }}</p>
</div>


<!-- Contract Business Sign Back Field -->
<div class="form-group col-12">
    {!! Form::label('contract_business_sign_back', __('models/contracts.fields.contract_business_sign_back').':') !!}
    <p>{{ $contract->contract_business_sign_back }}</p>
</div>


<!-- Contract Match Date Field -->
<div class="form-group col-12">
    {!! Form::label('contract_match_date', __('models/contracts.fields.contract_match_date').':') !!}
    <p>{{ $contract->contract_match_date }}</p>
</div>


<!-- Landlord Match Number Field -->
<div class="form-group col-12">
    {!! Form::label('landlord_match_number', __('models/contracts.fields.landlord_match_number').':') !!}
    <p>{{ $contract->landlord_match_number }}</p>
</div>


<!-- Landlord Existing Tenant Field -->
<div class="form-group col-12">
    {!! Form::label('landlord_existing_tenant', __('models/contracts.fields.landlord_existing_tenant').':') !!}
    <p>{{ $contract->landlord_existing_tenant }}</p>
</div>


<!-- Landlord Contract Date Field -->
<div class="form-group col-12">
    {!! Form::label('landlord_contract_date', __('models/contracts.fields.landlord_contract_date').':') !!}
    <p>{{ $contract->landlord_contract_date }}</p>
</div>


<!-- Landlord Contract Expiry Date Field -->
<div class="form-group col-12">
    {!! Form::label('landlord_contract_expiry_date', __('models/contracts.fields.landlord_contract_expiry_date').':') !!}
    <p>{{ $contract->landlord_contract_expiry_date }}</p>
</div>


<!-- Landlord Item Number Field -->
<div class="form-group col-12">
    {!! Form::label('landlord_item_number', __('models/contracts.fields.landlord_item_number').':') !!}
    <p>{{ $contract->landlord_item_number }}</p>
</div>


<!-- Landlord Ministry Of The Interior Number Field -->
<div class="form-group col-12">
    {!! Form::label('landlord_ministry_of_the_interior_number', __('models/contracts.fields.landlord_ministry_of_the_interior_number').':') !!}
    <p>{{ $contract->landlord_ministry_of_the_interior_number }}</p>
</div>


<!-- Landlord Types Of Field -->
<div class="form-group col-12">
    {!! Form::label('landlord_types_of', __('models/contracts.fields.landlord_types_of').':') !!}
    <p>{{ $contract->landlord_types_of }}</p>
</div>


<!-- Tenant Case Number Field -->
<div class="form-group col-12">
    {!! Form::label('tenant_case_number', __('models/contracts.fields.tenant_case_number').':') !!}
    <p>{{ $contract->tenant_case_number }}</p>
</div>


<!-- Tenant Ministry Of The Interior Number Field -->
<div class="form-group col-12">
    {!! Form::label('tenant_ministry_of_the_interior_number', __('models/contracts.fields.tenant_ministry_of_the_interior_number').':') !!}
    <p>{{ $contract->tenant_ministry_of_the_interior_number }}</p>
</div>


<!-- Rent Evaluation Form Market Rent Field -->
<div class="form-group col-12">
    {!! Form::label('rent_evaluation_form_market_rent', __('models/contracts.fields.rent_evaluation_form_market_rent').':') !!}
    <p>{{ $contract->rent_evaluation_form_market_rent }}</p>
</div>


<!-- Rent Evaluation Form Assess Rent Field -->
<div class="form-group col-12">
    {!! Form::label('rent_evaluation_form_assess_rent', __('models/contracts.fields.rent_evaluation_form_assess_rent').':') !!}
    <p>{{ $contract->rent_evaluation_form_assess_rent }}</p>
</div>


<!-- Actual Contract Rent To Landlord Field -->
<div class="form-group col-12">
    {!! Form::label('actual_contract_rent_to_landlord', __('models/contracts.fields.actual_contract_rent_to_landlord').':') !!}
    <p>{{ $contract->actual_contract_rent_to_landlord }}</p>
</div>


<!-- Actual Contract Tenant Pays Rent Field -->
<div class="form-group col-12">
    {!! Form::label('actual_contract_tenant_pays_rent', __('models/contracts.fields.actual_contract_tenant_pays_rent').':') !!}
    <p>{{ $contract->actual_contract_tenant_pays_rent }}</p>
</div>


<!-- Actual Contract Rent Subsidy Field -->
<div class="form-group col-12">
    {!! Form::label('actual_contract_rent_subsidy', __('models/contracts.fields.actual_contract_rent_subsidy').':') !!}
    <p>{{ $contract->actual_contract_rent_subsidy }}</p>
</div>


<!-- Actual Contract Deposit Field -->
<div class="form-group col-12">
    {!! Form::label('actual_contract_deposit', __('models/contracts.fields.actual_contract_deposit').':') !!}
    <p>{{ $contract->actual_contract_deposit }}</p>
</div>


<!-- To The Landlord Rent Day Field -->
<div class="form-group col-12">
    {!! Form::label('to_the_landlord_rent_day', __('models/contracts.fields.to_the_landlord_rent_day').':') !!}
    <p>{{ $contract->to_the_landlord_rent_day }}</p>
</div>


<!-- Package Escrow Fee Month Minute Field -->
<div class="form-group col-12">
    {!! Form::label('package_escrow_fee_month_minute', __('models/contracts.fields.package_escrow_fee_month_minute').':') !!}
    <p>{{ $contract->package_escrow_fee_month_minute }}</p>
</div>


<!-- Package Escrow Fee Amount Field -->
<div class="form-group col-12">
    {!! Form::label('package_escrow_fee_amount', __('models/contracts.fields.package_escrow_fee_amount').':') !!}
    <p>{{ $contract->package_escrow_fee_amount }}</p>
</div>


<!-- Package Escrow Fee Number Of Periods Field -->
<div class="form-group col-12">
    {!! Form::label('package_escrow_fee_number_of_periods', __('models/contracts.fields.package_escrow_fee_number_of_periods').':') !!}
    <p>{{ $contract->package_escrow_fee_number_of_periods }}</p>
</div>


<!-- Rent Subsidy Month Minute Field -->
<div class="form-group col-12">
    {!! Form::label('rent_subsidy_month_minute', __('models/contracts.fields.rent_subsidy_month_minute').':') !!}
    <p>{{ $contract->rent_subsidy_month_minute }}</p>
</div>


<!-- Rent Subsidy Amount Field -->
<div class="form-group col-12">
    {!! Form::label('rent_subsidy_amount', __('models/contracts.fields.rent_subsidy_amount').':') !!}
    <p>{{ $contract->rent_subsidy_amount }}</p>
</div>


<!-- Rent Subsidy Number Of Periods Field -->
<div class="form-group col-12">
    {!! Form::label('rent_subsidy_number_of_periods', __('models/contracts.fields.rent_subsidy_number_of_periods').':') !!}
    <p>{{ $contract->rent_subsidy_number_of_periods }}</p>
</div>


<!-- Development Matching Fees Month Field -->
<div class="form-group col-12">
    {!! Form::label('development_matching_fees_month', __('models/contracts.fields.development_matching_fees_month').':') !!}
    <p>{{ $contract->development_matching_fees_month }}</p>
</div>


<!-- Development Matching Fees Amount Field -->
<div class="form-group col-12">
    {!! Form::label('development_matching_fees_amount', __('models/contracts.fields.development_matching_fees_amount').':') !!}
    <p>{{ $contract->development_matching_fees_amount }}</p>
</div>


<!-- Notary Fees Notarization Date Field -->
<div class="form-group col-12">
    {!! Form::label('notary_fees_notarization_date', __('models/contracts.fields.notary_fees_notarization_date').':') !!}
    <p>{{ $contract->notary_fees_notarization_date }}</p>
</div>


<!-- Notary Fees Month Field -->
<div class="form-group col-12">
    {!! Form::label('notary_fees_month', __('models/contracts.fields.notary_fees_month').':') !!}
    <p>{{ $contract->notary_fees_month }}</p>
</div>


<!-- Notary Fees Amount Field -->
<div class="form-group col-12">
    {!! Form::label('notary_fees_amount', __('models/contracts.fields.notary_fees_amount').':') !!}
    <p>{{ $contract->notary_fees_amount }}</p>
</div>


<!-- Repair Cost Month Minute Field -->
<div class="form-group col-12">
    {!! Form::label('repair_cost_month_minute', __('models/contracts.fields.repair_cost_month_minute').':') !!}
    <p>{{ $contract->repair_cost_month_minute }}</p>
</div>


<!-- Repair Cost Amount Field -->
<div class="form-group col-12">
    {!! Form::label('repair_cost_amount', __('models/contracts.fields.repair_cost_amount').':') !!}
    <p>{{ $contract->repair_cost_amount }}</p>
</div>


<!-- Insurance Month Minute Field -->
<div class="form-group col-12">
    {!! Form::label('insurance_month_minute', __('models/contracts.fields.insurance_month_minute').':') !!}
    <p>{{ $contract->insurance_month_minute }}</p>
</div>


<!-- Insurance Amount Field -->
<div class="form-group col-12">
    {!! Form::label('insurance_amount', __('models/contracts.fields.insurance_amount').':') !!}
    <p>{{ $contract->insurance_amount }}</p>
</div>


<!-- Insurance Period Three Year Field -->
<div class="form-group col-12">
    {!! Form::label('insurance_period_three_year', __('models/contracts.fields.insurance_period_three_year').':') !!}
    <p>{{ $contract->insurance_period_three_year }}</p>
</div>


<!-- Insurance Period Three Start And End Date Field -->
<div class="form-group col-12">
    {!! Form::label('insurance_period_three_start_and_end_date', __('models/contracts.fields.insurance_period_three_start_and_end_date').':') !!}
    <p>{{ $contract->insurance_period_three_start_and_end_date }}</p>
</div>


<!-- Remark 1 Field -->
<div class="form-group col-12">
    {!! Form::label('remark_1', __('models/contracts.fields.remark_1').':') !!}
    <p>{{ $contract->remark_1 }}</p>
</div>


<!-- Remark 2 Field -->
<div class="form-group col-12">
    {!! Form::label('remark_2', __('models/contracts.fields.remark_2').':') !!}
    <p>{{ $contract->remark_2 }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-12">
    {!! Form::label('created_at', __('models/contracts.fields.created_at').':') !!}
    <p>{{ $contract->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-12">
    {!! Form::label('updated_at', __('models/contracts.fields.updated_at').':') !!}
    <p>{{ $contract->updated_at }}</p>
</div>


