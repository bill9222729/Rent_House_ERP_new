{{-- 包租約 --}}

<div class="row">
    {{-- 隱藏的欄位 --}}
    <input name="contract_type" type="hidden" value="包租約">

    <!-- Match Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('match_id', __('models/contractsNews.fields.match_id'), ['class' => 'col-md-2 control-label']) !!}
        {!! Form::text('match_id', null, ['class' => 'form-control col-md-6']) !!}
    </div>

    <!-- Building Case Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('building_case_id', __('models/contractsNews.fields.building_case'), ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6 px-0">
            {!! Form::text('building_case_id', null, ['class' => 'form-control col-md-12']) !!}
            <span id="building_info" class="hidden">已帶入資料</span>
        </div>
        <button type="button" class="btn btn-primary col-md-2 mx-1"
            onclick="handleGetBuildingInfoButtonClick()">代入資料</button>
    </div>

    <!-- Lease Start Date Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('lease_start_date', __('models/contractsNews.fields.lease_date') . ':', ['class' => 'col-md-2 control-label']) !!}
        {!! Form::date('lease_start_date', null, ['class' => 'form-control col-md-3', 'id' => 'lease_start_date']) !!}
        ~
        {!! Form::date('lease_end_date', null, ['class' => 'form-control col-md-3', 'id' => 'lease_end_date']) !!}
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#lease_start_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })
            $('#lease_start_date').datepicker();
        </script>
    @endpush

    @push('scripts')
        <script type="text/javascript">
            // $('#lease_end_date').datetimepicker({
            //     format: 'YYYY-MM-DD HH:mm:ss',
            //     useCurrent: false
            // })

            $('#lease_end_date').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                // showOnFocus: false,
                todayHighlight: true,
                useCurrent: false
            }).on("show", function(e) {
                $("div.box").css({
                    minHeight: "480px"
                });
            }).on("hide", function(e) {
                $("div.box").css({
                    minHeight: "auto"
                });
            });
        </script>
        <script type="text/javascript">
            function getBuildingInfo(buildingId) {
                const settings = {
                    "url": `https://rent-house-erp.echouse.co/api/building_management_news?case_no=${buildingId}`,
                    "method": "GET",
                    "timeout": 0,
                };

                console.log(settings);

                return new Promise((resolve, reject) => {
                    $.ajax(settings).done(function(response) {
                        resolve(response);
                    });
                })
            }

            async function handleGetBuildingInfoButtonClick() {
                let buildingId = $("#shadow-form-charter #building_case_id").val();
                let retval = await getBuildingInfo(buildingId);
                if (retval.data) {
                    console.log(retval.data);
                    $("#shadow-form-charter #building_info").text(
                        `[案件編號：${retval.data[0]['case_no']},${retval.data[0]['name']},狀態：${retval.data[0]['case_state']}]`
                        );
                    $("#shadow-form-charter #building_info").removeClass("hidden");
                } else {
                    console.log("沒資料");
                }
            }
        </script>
    @endpush

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
        <a href="javascript:void(0)" onclick="$('#shadow-form-charter').hide()"
            class="btn btn-default">@lang('crud.cancel')</a>
    </div>
</div>
