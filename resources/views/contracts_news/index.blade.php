@extends('layouts.app')
@section('css')
    <style>
        #shadow-form,
        .shadow-form {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
            height: 100%;
            width: 100%;
            display: grid;
            place-items: center;
            background: rgba(0, 0, 0, .4)
        }

        #shadow-form form,
        .shadow-form form {
            max-width: 800px;
            width: 85vw;
            margin: 4rem 0;
            padding: 1.5rem;
            overflow: auto;
            background: #f6f9fa;
            border-radius: 16px;
        }

        .control-label {
            text-align: right;
            margin-bottom: 0;
            padding-top: 7px;
        }

    </style>
@endsection

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">@lang('models/contractsNews.plural')</h3>
        <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px;z-index: 100; margin-left:5px"
            href="javascript: openShadowForm('shadow-form-escrow')">新增 - 代管約</a>
        <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px;z-index: 100; margin-left:5px"
            href="javascript:openShadowForm('shadow-form-charter')">新增 - 包租約(業者-房東)</a>
        <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px;z-index: 100; margin-left:5px"
            href="javascript:openShadowForm('shadow-form-sublease')">新增 - 轉租約(業者-房客)</a>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('contracts_news.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
    <div id="shadow-form" style="display: none">
        {!! Form::open(['route' => 'contractsNews.store', 'method' => 'PATCH']) !!}
        @include('contracts_news.fields')
        {!! Form::close() !!}
    </div>
    {{-- 代管約 --}}
    <div id="shadow-form-escrow" class="shadow-form" style="display: none">
        {!! Form::open(['route' => 'contractsNews.store', 'method' => 'PATCH']) !!}

        @include('contracts_news.fields_escrow')

        {!! Form::close() !!}
    </div>
    {{-- 包租約 --}}
    <div id="shadow-form-charter" class="shadow-form" style="display: none">
        {!! Form::open(['route' => 'contractsNews.store', 'method' => 'PATCH']) !!}

        @include('contracts_news.fields_charter')

        {!! Form::close() !!}
    </div>
    {{-- 轉租約 --}}
    <div id="shadow-form-sublease" class="shadow-form" style="display: none">
        {!! Form::open(['route' => 'contractsNews.store', 'method' => 'PATCH']) !!}

        @include('contracts_news.fields_sublease')

        {!! Form::close() !!}
    </div>
@endsection

@push('scripts')
    {{-- <script src="http://malsup.github.com/jquery.form.js"></script> --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function ajaxCU(fill) {
            const form = $('#shadow-form form')
            const method = $('#shadow-form [name="_method"]')
            if (fill) {
                Object.keys(fill).forEach(key => {
                    const input = document.querySelector(`#shadow-form [name="${key}"]`)
                    if (!input) return
                    if (input.getAttribute('type') === 'checkbox') input.checked = fill[key] ? true : false
                    else input.value = fill[key]
                })
                form.attr('action', '{{ route('contractsNews.store') }}/' + fill['id'])
                method.val('PATCH')
            } else {
                form.attr('action', '{{ route('contractsNews.store') }}')
                method.val('POST')
            }
            $('#shadow-form').show()
        }
        ajaxCU.close = () => {
            $('#shadow-form').hide()
        }

        function openShadowForm(formId, fill = false) {
            const form = $(`#${formId} form`)
            const method = $(`#${formId} [name="_method"]`)

            if (fill) {
                Object.keys(fill).forEach(key => {
                    const input = document.querySelector(`#${formId} [name="${key}"]`)
                    if (!input) return
                    if (input.getAttribute('type') === 'checkbox') input.checked = fill[key] ? true : false
                    else input.value = fill[key]
                })
                form.attr('action', '{{ route('contractsNews.store') }}/' + fill['id'])
                method.val('PATCH')
            } else {
                form.attr('action', '{{ route('contractsNews.store') }}')
                method.val('POST')
            }
            $(`#${formId}`).show()
        }
    </script>
@endpush
