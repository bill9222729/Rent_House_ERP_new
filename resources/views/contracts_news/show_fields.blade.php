<!-- Building Case Id Field -->
<div class="form-group col-12">
    {!! Form::label('building_case_id', __('models/contractsNews.fields.building_case_id').':') !!}
    <p>{{ $contractsNew->building_case_id }}</p>
</div>


<!-- Tenant Case Id Field -->
<div class="form-group col-12">
    {!! Form::label('tenant_case_id', __('models/contractsNews.fields.tenant_case_id').':') !!}
    <p>{{ $contractsNew->tenant_case_id }}</p>
</div>


<!-- Lease Start Date Field -->
<div class="form-group col-12">
    {!! Form::label('lease_start_date', __('models/contractsNews.fields.lease_start_date').':') !!}
    <p>{{ $contractsNew->lease_start_date }}</p>
</div>


<!-- Lease End Date Field -->
<div class="form-group col-12">
    {!! Form::label('lease_end_date', __('models/contractsNews.fields.lease_end_date').':') !!}
    <p>{{ $contractsNew->lease_end_date }}</p>
</div>


<!-- Charter Case Id Field -->
<div class="form-group col-12">
    {!! Form::label('charter_case_id', __('models/contractsNews.fields.charter_case_id').':') !!}
    <p>{{ $contractsNew->charter_case_id }}</p>
</div>


<!-- Contract Type Field -->
<div class="form-group col-12">
    {!! Form::label('contract_type', __('models/contractsNews.fields.contract_type').':') !!}
    <p>{{ $contractsNew->contract_type }}</p>
</div>


<!-- Match Id Field -->
<div class="form-group col-12">
    {!! Form::label('match_id', __('models/contractsNews.fields.match_id').':') !!}
    <p>{{ $contractsNew->match_id }}</p>
</div>


