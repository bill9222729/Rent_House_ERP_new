<div class="row">
<!-- File Origin Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_origin_name', __('models/fileManagments.fields.file_origin_name').':') !!}
    {!! Form::text('file_origin_name', null, ['class' => 'form-control']) !!}
</div>

<!-- File Server Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_server_name', __('models/fileManagments.fields.file_server_name').':') !!}
    {!! Form::text('file_server_name', null, ['class' => 'form-control']) !!}
</div>

<!-- File Size Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_size', __('models/fileManagments.fields.file_size').':') !!}
    {!! Form::number('file_size', null, ['class' => 'form-control']) !!}
</div>

<!-- File Source Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_source', __('models/fileManagments.fields.file_source').':') !!}
    {!! Form::text('file_source', null, ['class' => 'form-control']) !!}
</div>

<!-- File Source Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_source_id', __('models/fileManagments.fields.file_source_id').':') !!}
    {!! Form::number('file_source_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
