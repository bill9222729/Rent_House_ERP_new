<!-- File Origin Name Field -->
<div class="form-group col-12">
    {!! Form::label('file_origin_name', __('models/fileManagments.fields.file_origin_name').':') !!}
    <p>{{ $fileManagment->file_origin_name }}</p>
</div>


<!-- File Server Name Field -->
<div class="form-group col-12">
    {!! Form::label('file_server_name', __('models/fileManagments.fields.file_server_name').':') !!}
    <p>{{ $fileManagment->file_server_name }}</p>
</div>


<!-- File Size Field -->
<div class="form-group col-12">
    {!! Form::label('file_size', __('models/fileManagments.fields.file_size').':') !!}
    <p>{{ $fileManagment->file_size }}</p>
</div>


<!-- File Source Field -->
<div class="form-group col-12">
    {!! Form::label('file_source', __('models/fileManagments.fields.file_source').':') !!}
    <p>{{ $fileManagment->file_source }}</p>
</div>


<!-- File Source Id Field -->
<div class="form-group col-12">
    {!! Form::label('file_source_id', __('models/fileManagments.fields.file_source_id').':') !!}
    <p>{{ $fileManagment->file_source_id }}</p>
</div>


