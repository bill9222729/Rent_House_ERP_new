@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

        .input-group label.error {
            position: absolute;
            bottom: -1rem;
        }
    </style>
@endsection
@section('content')
    <form id="tenant_create_form">
        <div class="container-fluid container-fixed-lg">
            <h3 class="page-title">新增修繕</h3>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                @include('fix_detail_managements.fields')
            </div>
        </div>
        <div class="container-fluid py-5">
            <div class="row justify-content-center">
                <div class="col-md-auto">
                    <button type="submit" class="btn btn-primary" id="fix-detail-management-create-button">儲存</button>
                </div>
                <div class="col-md-auto">
                    <button type="button" class="btn btn-secondary" id="fix-detail-management-cancel-button">取消</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/fix_detail_management.js') }}"></script>
    <script>
        $(document).ready(async function() {
            let building_id = $("#building_id").val();
            // 初始化頁面元件
            initPageElement();
            // 綁定儲存按鈕事件
            $("#fix-detail-management-create-button").on("click", function(e){
                saveFixDetailData();
                e.preventDefault();
            });
            // 取得必須資料
            let allData = await getRequiredData(building_id);
            console.log(allData);
            // 把資料填入頁面上
            $("#match_id").val(allData["matchData"] ? allData["matchData"]["match_id"] : "該物件沒有媒合資料");
            $("#building_case_no").val(allData["buildingData"]["case_no"]);
            $("#name").val(allData["buildingData"]["name"]);
            $("#sales_name").val(allData["buildingData"]["sales_name"]);
            $("#entrusted_management_date").val(allData["buildingData"]["entrusted_management_date"]);
            $("#building_addr").val(ganerateAddrFormBuildingData(allData["buildingData"]));
            $("#cellphone").val(allData["buildingData"]["cellphone"]);
            $("#tenant_name").val(allData["tenantData"].length === 0 ? allData["tenantData"]["name"] : "該物件沒有對應房客資料");
            $("#tenant_tel").val(allData["tenantData"].length === 0 ? allData["tenantData"]["cellphone"] : "該物件沒有對應房客資料");
            $("#lease_date").val(allData["matchData"] ? `${convertToRCDate(allData["matchData"]["lease_start_date"])} ~ ${convertToRCDate(allData["matchData"]["lease_end_date"])}` : "此物件沒有合約資料");
            // 計算餘額
            $("#calculate_balance").on("click", ()=>{
                let invoice_date = $("#invoice_date").val();
                let please_amount = $("#please_amount").val();
                console.log(invoice_date, please_amount);
            });
        });
    </script>
@endpush
