{{-- 物件基本資料 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
            物件基本資料
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label for="match_id" class="col-md-12 col-form-label fw-bolder text-nowrap">媒合編號</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="match_id" id="match_id" style="text-transform:uppercase">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_case_no" class="col-md-12 col-form-label fw-bolder text-nowrap">物件編號</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="building_case_no" id="building_case_no" style="text-transform:uppercase">
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="name" class="col-md-12 col-form-label fw-bolder text-nowrap">房東姓名</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="name" id="name" style="text-transform:uppercase">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="building_addr" class="col-md-12 col-form-label fw-bolder text-nowrap">物件地址</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="building_addr" id="building_addr" style="text-transform:uppercase">
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="cellphone" class="col-md-12 col-form-label fw-bolder text-nowrap">房東電話</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="cellphone" id="cellphone" style="text-transform:uppercase">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="tenant_name" class="col-md-12 col-form-label fw-bolder text-nowrap">租客姓名</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="tenant_name" id="tenant_name" style="text-transform:uppercase">
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="tenant_tel" class="col-md-12 col-form-label fw-bolder text-nowrap">租客電話</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="tenant_tel" id="tenant_tel" style="text-transform:uppercase">
                        </div>
                    </div>
                    <div for="entrusted_management_date" class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap">委託管理約</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="entrusted_management_date" id="entrusted_management_date" style="text-transform:uppercase">
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="lease_date" class="col-md-12 col-form-label fw-bolder text-nowrap">合約租期</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="lease_date" id="lease_date" style="text-transform:uppercase">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="sales_name" class="col-md-12 col-form-label fw-bolder text-nowrap">業務</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control-plaintext" name="sales_name" id="sales_name" style="text-transform:uppercase">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- 修繕紀錄 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
            修繕紀錄
        </button>
    </h2>
    <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label for="repair_manufacturers" class="col-md-12 col-form-label fw-bolder text-nowrap">修繕廠商</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="repair_manufacturers" id="repair_manufacturers">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="report_date" class="col-md-12 col-form-label fw-bolder text-nowrap">報修日</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date day">
                                <input type="text" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="report_date" id="report_date" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="repair_item" class="col-md-12 col-form-label fw-bolder text-nowrap">修繕項目</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="repair_item" id="repair_item">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="broken_reason" class="col-md-12 col-form-label fw-bolder text-nowrap">損壞原因</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="broken_reason" id="broken_reason">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="meeting_date" class="col-md-12 col-form-label fw-bolder text-nowrap">會勘日期</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date day">
                                <input type="text" name="meeting_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="meeting_date" id="meeting_date" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="fix_date" class="col-md-12 col-form-label fw-bolder text-nowrap">修繕日期</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date day">
                                <input type="text" name="fix_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="fix_date" id="fix_date" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="completion_date" class="col-md-12 col-form-label fw-bolder text-nowrap">資料完成日</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date day">
                                <input type="text" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="completion_date" id="completion_date" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="repair_records" class="col-md-12 col-form-label fw-bolder text-nowrap">修繕紀錄</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="repair_records" id="repair_records">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="reply_amount" class="col-md-12 col-form-label fw-bolder text-nowrap">修繕金額</label>
                        <div class="col-md-12 align-items-center">
                            <input type="number" class="form-control" name="reply_amount" id="reply_amount">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="please_amount" class="col-md-12 col-form-label fw-bolder text-nowrap">本次請領金額</label>
                        <div class="col-md-12 align-items-center">
                            <input type="number" class="form-control" name="please_amount" id="please_amount">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="invoice_date" class="col-md-12 col-form-label fw-bolder text-nowrap">發票日期</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date day">
                                <input type="text" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="invoice_date" id="invoice_date" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="balance" class="col-md-12 col-form-label fw-bolder text-nowrap">補助款餘額</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group">
                                <button class="btn btn-outline-secondary" type="button" id="calculate_balance">計算</button>
                                <input type="number" class="form-control" placeholder="要填入發票日期才能計算" readonly>
                              </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="has_receipt" class="col-md-12 col-form-label fw-bolder text-nowrap">是否有收據</label>
                        <div class="col-md-12 align-items-center row ms-1">
                            <div class="form-check col-md-6">
                                <input class="form-check-input" type="radio" name="has_receipt" id="has_receipt" checked>
                                <label class="form-check-label" for="has_receipt">
                                    有
                                </label>
                            </div>
                            <div class="form-check col-md-6">
                                <input class="form-check-input" type="radio" name="has_receipt" id="no_receipt">
                                <label class="form-check-label" for="no_receipt">
                                    無
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="no_reference" class="col-md-12 col-form-label fw-bolder text-nowrap">是否需要備查</label>
                        <div class="col-md-12 align-items-center row ms-1">
                            <div class="form-check col-md-6">
                                <input class="form-check-input" type="radio" name="no_reference" id="need_reference" checked>
                                <label class="form-check-label" for="need_reference">
                                    是
                                </label>
                            </div>
                            <div class="form-check col-md-6">
                                <input class="form-check-input" type="radio" name="no_reference" id="no_reference">
                                <label class="form-check-label" for="no_reference">
                                    否
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="monthly_application_month" class="col-md-12 col-form-label fw-bolder text-nowrap">月報申請月份</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date month">
                                <input type="text" name="monthly_application_month" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="monthly_application_month" id="monthly_application_month" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="creator" id="creator" value="{{ Auth::user()->id }}" />
                <input type="hidden" name="building_id" id="building_id" value="{{ app('request')->input('building_id') }}" />
            </div>
        </div>
    </div>
</div>

{{-- 檢附文件 --}}
@if (isset($fixDetailManagement) && !app('request')->input('copy'))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                檢附文件
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="tenant_file_input" name="source" type="file" multiple {{ app('request')->input('cantedit') ? "readonly='true'" : '' }} />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

{{-- 照片 --}}
@if (isset($fixDetailManagement) && !app('request')->input('copy'))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingSix">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="true" aria-controls="panelsStayOpen-collapseSix">
                檢附文件
            </button>
        </h2>
        <div id="panelsStayOpen-collapseSix" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingSix">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="tenant_file_input" name="source" type="file" multiple {{ app('request')->input('cantedit') ? "readonly='true'" : '' }} />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
