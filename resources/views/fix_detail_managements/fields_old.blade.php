<div class="row">
<!-- Report Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('report_date', __('models/fixDetailManagements.fields.report_date').':') !!}
    {!! Form::date('report_date', null, ['class' => 'form-control','id'=>'report_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#report_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Fix Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fix_id', __('models/fixDetailManagements.fields.fix_id').':') !!}
    {!! Form::number('fix_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Repair Item Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repair_item', __('models/fixDetailManagements.fields.repair_item').':') !!}
    {!! Form::text('repair_item', null, ['class' => 'form-control']) !!}
</div>

<!-- Repair Manufacturers Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repair_manufacturers', __('models/fixDetailManagements.fields.repair_manufacturers').':') !!}
    {!! Form::text('repair_manufacturers', null, ['class' => 'form-control']) !!}
</div>

<!-- Repair Records Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repair_records', __('models/fixDetailManagements.fields.repair_records').':') !!}
    {!! Form::text('repair_records', null, ['class' => 'form-control']) !!}
</div>

<!-- Meeting Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meeting_date', __('models/fixDetailManagements.fields.meeting_date').':') !!}
    {!! Form::date('meeting_date', null, ['class' => 'form-control','id'=>'meeting_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#meeting_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Meeting Master Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meeting_master', __('models/fixDetailManagements.fields.meeting_master').':') !!}
    {!! Form::text('meeting_master', null, ['class' => 'form-control']) !!}
</div>

<!-- Fix Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fix_date', __('models/fixDetailManagements.fields.fix_date').':') !!}
    {!! Form::date('fix_date', null, ['class' => 'form-control','id'=>'fix_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fix_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Completion Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('completion_date', __('models/fixDetailManagements.fields.completion_date').':') !!}
    {!! Form::date('completion_date', null, ['class' => 'form-control','id'=>'completion_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#completion_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Please Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('please_amount', __('models/fixDetailManagements.fields.please_amount').':') !!}
    {!! Form::number('please_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Reply Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reply_amount', __('models/fixDetailManagements.fields.reply_amount').':') !!}
    {!! Form::number('reply_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Monthly Application Month Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monthly_application_month', __('models/fixDetailManagements.fields.monthly_application_month').':') !!}
    {!! Form::date('monthly_application_month', null, ['class' => 'form-control','id'=>'monthly_application_month']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#monthly_application_month').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('balance', __('models/fixDetailManagements.fields.balance').':') !!}
    {!! Form::number('balance', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', __('models/fixDetailManagements.fields.note').':') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- No Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_reference', __('models/fixDetailManagements.fields.no_reference').':') !!}
    {!! Form::number('no_reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
