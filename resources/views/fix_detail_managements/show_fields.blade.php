<!-- Report Date Field -->
<div class="form-group col-12">
    {!! Form::label('report_date', __('models/fixDetailManagements.fields.report_date').':') !!}
    <p>{{ $fixDetailManagement->report_date }}</p>
</div>


<!-- Fix Id Field -->
<div class="form-group col-12">
    {!! Form::label('fix_id', __('models/fixDetailManagements.fields.fix_id').':') !!}
    <p>{{ $fixDetailManagement->fix_id }}</p>
</div>


<!-- Repair Item Field -->
<div class="form-group col-12">
    {!! Form::label('repair_item', __('models/fixDetailManagements.fields.repair_item').':') !!}
    <p>{{ $fixDetailManagement->repair_item }}</p>
</div>


<!-- Repair Manufacturers Field -->
<div class="form-group col-12">
    {!! Form::label('repair_manufacturers', __('models/fixDetailManagements.fields.repair_manufacturers').':') !!}
    <p>{{ $fixDetailManagement->repair_manufacturers }}</p>
</div>


<!-- Repair Records Field -->
<div class="form-group col-12">
    {!! Form::label('repair_records', __('models/fixDetailManagements.fields.repair_records').':') !!}
    <p>{{ $fixDetailManagement->repair_records }}</p>
</div>


<!-- Meeting Date Field -->
<div class="form-group col-12">
    {!! Form::label('meeting_date', __('models/fixDetailManagements.fields.meeting_date').':') !!}
    <p>{{ $fixDetailManagement->meeting_date }}</p>
</div>


<!-- Meeting Master Field -->
<div class="form-group col-12">
    {!! Form::label('meeting_master', __('models/fixDetailManagements.fields.meeting_master').':') !!}
    <p>{{ $fixDetailManagement->meeting_master }}</p>
</div>


<!-- Fix Date Field -->
<div class="form-group col-12">
    {!! Form::label('fix_date', __('models/fixDetailManagements.fields.fix_date').':') !!}
    <p>{{ $fixDetailManagement->fix_date }}</p>
</div>


<!-- Completion Date Field -->
<div class="form-group col-12">
    {!! Form::label('completion_date', __('models/fixDetailManagements.fields.completion_date').':') !!}
    <p>{{ $fixDetailManagement->completion_date }}</p>
</div>


<!-- Please Amount Field -->
<div class="form-group col-12">
    {!! Form::label('please_amount', __('models/fixDetailManagements.fields.please_amount').':') !!}
    <p>{{ $fixDetailManagement->please_amount }}</p>
</div>


<!-- Reply Amount Field -->
<div class="form-group col-12">
    {!! Form::label('reply_amount', __('models/fixDetailManagements.fields.reply_amount').':') !!}
    <p>{{ $fixDetailManagement->reply_amount }}</p>
</div>


<!-- Monthly Application Month Field -->
<div class="form-group col-12">
    {!! Form::label('monthly_application_month', __('models/fixDetailManagements.fields.monthly_application_month').':') !!}
    <p>{{ $fixDetailManagement->monthly_application_month }}</p>
</div>


<!-- Balance Field -->
<div class="form-group col-12">
    {!! Form::label('balance', __('models/fixDetailManagements.fields.balance').':') !!}
    <p>{{ $fixDetailManagement->balance }}</p>
</div>


<!-- Note Field -->
<div class="form-group col-12">
    {!! Form::label('note', __('models/fixDetailManagements.fields.note').':') !!}
    <p>{{ $fixDetailManagement->note }}</p>
</div>


<!-- No Reference Field -->
<div class="form-group col-12">
    {!! Form::label('no_reference', __('models/fixDetailManagements.fields.no_reference').':') !!}
    <p>{{ $fixDetailManagement->no_reference }}</p>
</div>


