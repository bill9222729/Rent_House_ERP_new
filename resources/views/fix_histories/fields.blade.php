<div class="row">
<!-- Sales Field -->
<div class="form-group col-sm-3">
    {!! Form::label('sales', __('models/fixHistories.fields.sales').':') !!}
    {!! Form::text('sales', null, ['class' => 'form-control']) !!}
</div>

<!-- Pay Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('pay_date', __('models/fixHistories.fields.pay_date').':') !!}
    @isset($fixHistory)
        {!! Form::text('pay_date', ((int)date('Y',strtotime($fixHistory->pay_date)) - 1911) . date('-m', strtotime($fixHistory->pay_date)), ['class' => 'form-control','id'=>'pay_date']) !!}
    @else
        {!! Form::text('pay_date', null, ['class' => 'form-control','id'=>'pay_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#pay_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Item Code Field -->
<div class="form-group col-sm-3">
    {!! Form::label('item_code', __('models/fixHistories.fields.item_code').':') !!}
    {!! Form::text('item_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Item Address Field -->
<div class="form-group col-sm-3">
    {!! Form::label('item_address', __('models/fixHistories.fields.item_address').':') !!}
    {!! Form::text('item_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Pay Year Field -->
<div class="form-group col-sm-3">
    {!! Form::label('pay_year', __('models/fixHistories.fields.pay_year').':') !!}
    {!! Form::number('pay_year', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-3">
    {!! Form::label('fix_vendor', __('models/fixHistories.fields.fix_vendor').':') !!}
    {!! Form::text('fix_vendor', null, ['class' => 'form-control']) !!}
</div>

<!-- Call Fix Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('call_fix_date', __('models/fixHistories.fields.call_fix_date').':') !!}
    @isset($fixHistory)
        @if($fixHistory->call_fix_date == "房東自修" || $fixHistory->call_fix_date == "日期格式錯誤")
            {!! Form::text('call_fix_date', $fixHistory->call_fix_date, ['class' => 'form-control','id'=>'call_fix_date']) !!}
        @else
            {!! Form::text('call_fix_date', ((int)date('Y',strtotime($fixHistory->call_fix_date)) - 1911) . date('-m-d',strtotime($fixHistory->call_fix_date)), ['class' => 'form-control','id'=>'call_fix_date']) !!}
        @endif
    @else
        {!! Form::text('call_fix_date', null, ['class' => 'form-control','id'=>'call_fix_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#call_fix_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Item Owner Field -->
<div class="form-group col-sm-3">
    {!! Form::label('item_owner', __('models/fixHistories.fields.item_owner').':') !!}
    {!! Form::text('item_owner', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Phone Field -->
<div class="form-group col-sm-3">
    {!! Form::label('owner_phone', __('models/fixHistories.fields.owner_phone').':') !!}
    {!! Form::text('owner_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Client Name Field -->
<div class="form-group col-sm-3">
    {!! Form::label('client_name', __('models/fixHistories.fields.client_name').':') !!}
    {!! Form::text('client_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Client Phone Field -->
<div class="form-group col-sm-3">
    {!! Form::label('client_phone', __('models/fixHistories.fields.client_phone').':') !!}
    {!! Form::text('client_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Fix Item Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fix_item', __('models/fixHistories.fields.fix_item').':') !!}
    {!! Form::text('fix_item', null, ['class' => 'form-control']) !!}
</div>

<!-- Fix Source Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fix_source', __('models/fixHistories.fields.fix_source').':') !!}
    {!! Form::text('fix_source', null, ['class' => 'form-control']) !!}
</div>

<!-- Check Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('check_date', __('models/fixHistories.fields.check_date').':') !!}
    @isset($fixHistory)
        @if($fixHistory->check_date == "房東自修" || $fixHistory->check_date == "日期格式錯誤")
        {!! Form::text('check_date', $fixHistory->check_date, ['class' => 'form-control','id'=>'check_date']) !!}
        @else
            {!! Form::text('check_date', ((int)date('Y',strtotime($fixHistory->check_date)) - 1911) . date('-m-d',strtotime($fixHistory->check_date)), ['class' => 'form-control','id'=>'check_date']) !!}
        @endif
    @else
        {!! Form::text('check_date', null, ['class' => 'form-control','id'=>'check_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#check_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Fix Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fix_date', __('models/fixHistories.fields.fix_date').':') !!}
    @isset($fixHistory)
        @if($fixHistory->fix_date == "房東自修" || $fixHistory->fix_date == "日期格式錯誤")
        {!! Form::text('fix_date', $fixHistory->fix_date, ['class' => 'form-control','id'=>'fix_date']) !!}
        @else
        {!! Form::text('fix_date', ((int)date('Y',strtotime($fixHistory->fix_date)) - 1911) . date('-m-d',strtotime($fixHistory->fix_date)), ['class' => 'form-control','id'=>'fix_date']) !!}
        @endif
    @else
        {!! Form::text('fix_date', null, ['class' => 'form-control','id'=>'fix_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fix_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Fix Done Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fix_done', __('models/fixHistories.fields.fix_done').':') !!}
    @isset($fixHistory)
        {!! Form::text('fix_done', ((int)date('Y',strtotime($fixHistory->fix_done)) - 1911) . date('-m-d',strtotime($fixHistory->fix_done)), ['class' => 'form-control','id'=>'fix_done']) !!}
    @else
        {!! Form::text('fix_done', null, ['class' => 'form-control','id'=>'fix_done']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fix_done').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Fix Record Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fix_record', __('models/fixHistories.fields.fix_record').':') !!}
    {!! Form::text('fix_record', null, ['class' => 'form-control']) !!}
</div>

<!-- Fix Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fix_price', __('models/fixHistories.fields.fix_price').':') !!}
    {!! Form::number('fix_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Receipt Field -->
<div class="form-group col-sm-3">
    {!! Form::label('receipt', __('models/fixHistories.fields.receipt').':') !!}
    {!! Form::number('receipt', null, ['class' => 'form-control']) !!}
</div>

<!-- Final Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('final_price', __('models/fixHistories.fields.final_price').':') !!}
    {!! Form::number('final_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-3">
    {!! Form::label('balance', __('models/fixHistories.fields.balance').':') !!}
    {!! Form::number('balance', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Start Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('owner_start_date', __('models/fixHistories.fields.owner_start_date').':') !!}
    @isset($fixHistory)
        {!! Form::text('owner_start_date', ((int)date('Y',strtotime($fixHistory->owner_start_date)) - 1911) . date('-m-d',strtotime($fixHistory->owner_start_date)), ['class' => 'form-control','id'=>'owner_start_date']) !!}
    @else
        {!! Form::text('owner_start_date', null, ['class' => 'form-control','id'=>'owner_start_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#owner_start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Owner End Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('owner_end_date', __('models/fixHistories.fields.owner_end_date').':') !!}
    @isset($fixHistory)
        {!! Form::text('owner_end_date', ((int)date('Y',strtotime($fixHistory->owner_end_date)) - 1911) . date('-m-d',strtotime($fixHistory->owner_end_date)), ['class' => 'form-control','id'=>'owner_end_date']) !!}
    @else
        {!! Form::text('owner_end_date', null, ['class' => 'form-control','id'=>'owner_end_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#owner_end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Change Start Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('change_start_date', __('models/fixHistories.fields.change_start_date').':') !!}
    @isset($fixHistory)
        {!! Form::text('change_start_date', ((int)date('Y',strtotime($fixHistory->change_start_date)) - 1911) . date('-m-d',strtotime($fixHistory->change_start_date)), ['class' => 'form-control','id'=>'change_start_date']) !!}
    @else
        {!! Form::text('change_start_date', null, ['class' => 'form-control','id'=>'change_start_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#change_start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Change End Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('change_end_date', __('models/fixHistories.fields.change_end_date').':') !!}
    @isset($fixHistory)
        {!! Form::text('change_end_date', ((int)date('Y',strtotime($fixHistory->change_end_date)) - 1911) . date('-m-d',strtotime($fixHistory->change_end_date)), ['class' => 'form-control','id'=>'change_end_date']) !!}
    @else
        {!! Form::text('change_end_date', null, ['class' => 'form-control','id'=>'change_end_date']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#change_end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('fixHistories.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
