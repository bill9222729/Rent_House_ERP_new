<!-- Sales Field -->
<div class="form-group col-12">
    {!! Form::label('sales', __('models/fixHistories.fields.sales').':') !!}
    <p>{{ $fixHistory->sales }}</p>
</div>


<!-- Pay Date Field -->
<div class="form-group col-12">
    {!! Form::label('pay_date', __('models/fixHistories.fields.pay_date').':') !!}
    <p>{{ $fixHistory->pay_date }}</p>
</div>


<!-- Item Code Field -->
<div class="form-group col-12">
    {!! Form::label('item_code', __('models/fixHistories.fields.item_code').':') !!}
    <p>{{ $fixHistory->item_code }}</p>
</div>


<!-- Item Address Field -->
<div class="form-group col-12">
    {!! Form::label('item_address', __('models/fixHistories.fields.item_address').':') !!}
    <p>{{ $fixHistory->item_address }}</p>
</div>


<!-- Pay Year Field -->
<div class="form-group col-12">
    {!! Form::label('pay_year', __('models/fixHistories.fields.pay_year').':') !!}
    <p>{{ $fixHistory->pay_year }}</p>
</div>


<!-- Call Fix Date Field -->
<div class="form-group col-12">
    {!! Form::label('call_fix_date', __('models/fixHistories.fields.call_fix_date').':') !!}
    <p>{{ $fixHistory->call_fix_date }}</p>
</div>


<!-- Item Owner Field -->
<div class="form-group col-12">
    {!! Form::label('item_owner', __('models/fixHistories.fields.item_owner').':') !!}
    <p>{{ $fixHistory->item_owner }}</p>
</div>


<!-- Owner Phone Field -->
<div class="form-group col-12">
    {!! Form::label('owner_phone', __('models/fixHistories.fields.owner_phone').':') !!}
    <p>{{ $fixHistory->owner_phone }}</p>
</div>


<!-- Client Name Field -->
<div class="form-group col-12">
    {!! Form::label('client_name', __('models/fixHistories.fields.client_name').':') !!}
    <p>{{ $fixHistory->client_name }}</p>
</div>


<!-- Client Phone Field -->
<div class="form-group col-12">
    {!! Form::label('client_phone', __('models/fixHistories.fields.client_phone').':') !!}
    <p>{{ $fixHistory->client_phone }}</p>
</div>


<!-- Fix Item Field -->
<div class="form-group col-12">
    {!! Form::label('fix_item', __('models/fixHistories.fields.fix_item').':') !!}
    <p>{{ $fixHistory->fix_item }}</p>
</div>


<!-- Fix Source Field -->
<div class="form-group col-12">
    {!! Form::label('fix_source', __('models/fixHistories.fields.fix_source').':') !!}
    <p>{{ $fixHistory->fix_source }}</p>
</div>


<!-- Check Date Field -->
<div class="form-group col-12">
    {!! Form::label('check_date', __('models/fixHistories.fields.check_date').':') !!}
    <p>{{ $fixHistory->check_date }}</p>
</div>


<!-- Fix Date Field -->
<div class="form-group col-12">
    {!! Form::label('fix_date', __('models/fixHistories.fields.fix_date').':') !!}
    <p>{{ $fixHistory->fix_date }}</p>
</div>


<!-- Fix Done Field -->
<div class="form-group col-12">
    {!! Form::label('fix_done', __('models/fixHistories.fields.fix_done').':') !!}
    <p>{{ $fixHistory->fix_done }}</p>
</div>


<!-- Fix Record Field -->
<div class="form-group col-12">
    {!! Form::label('fix_record', __('models/fixHistories.fields.fix_record').':') !!}
    <p>{{ $fixHistory->fix_record }}</p>
</div>


<!-- Fix Price Field -->
<div class="form-group col-12">
    {!! Form::label('fix_price', __('models/fixHistories.fields.fix_price').':') !!}
    <p>{{ $fixHistory->fix_price }}</p>
</div>


<!-- Receipt Field -->
<div class="form-group col-12">
    {!! Form::label('receipt', __('models/fixHistories.fields.receipt').':') !!}
    <p>{{ $fixHistory->receipt }}</p>
</div>


<!-- Final Price Field -->
<div class="form-group col-12">
    {!! Form::label('final_price', __('models/fixHistories.fields.final_price').':') !!}
    <p>{{ $fixHistory->final_price }}</p>
</div>


<!-- Balance Field -->
<div class="form-group col-12">
    {!! Form::label('balance', __('models/fixHistories.fields.balance').':') !!}
    <p>{{ $fixHistory->balance }}</p>
</div>


<!-- Owner Start Date Field -->
<div class="form-group col-12">
    {!! Form::label('owner_start_date', __('models/fixHistories.fields.owner_start_date').':') !!}
    <p>{{ $fixHistory->owner_start_date }}</p>
</div>


<!-- Owner End Date Field -->
<div class="form-group col-12">
    {!! Form::label('owner_end_date', __('models/fixHistories.fields.owner_end_date').':') !!}
    <p>{{ $fixHistory->owner_end_date }}</p>
</div>


<!-- Change Start Date Field -->
<div class="form-group col-12">
    {!! Form::label('change_start_date', __('models/fixHistories.fields.change_start_date').':') !!}
    <p>{{ $fixHistory->change_start_date }}</p>
</div>


<!-- Change End Date Field -->
<div class="form-group col-12">
    {!! Form::label('change_end_date', __('models/fixHistories.fields.change_end_date').':') !!}
    <p>{{ $fixHistory->change_end_date }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-12">
    {!! Form::label('created_at', __('models/fixHistories.fields.created_at').':') !!}
    <p>{{ $fixHistory->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-12">
    {!! Form::label('updated_at', __('models/fixHistories.fields.updated_at').':') !!}
    <p>{{ $fixHistory->updated_at }}</p>
</div>


