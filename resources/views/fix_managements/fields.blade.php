<div class="row">
<!-- Match Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('match_id', __('models/fixManagements.fields.match_id').':') !!}
    {!! Form::text('match_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Case No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_case_no', __('models/fixManagements.fields.building_case_no').':') !!}
    {!! Form::text('building_case_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
