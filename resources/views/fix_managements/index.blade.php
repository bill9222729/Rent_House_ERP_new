@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">修繕總表</h3>
        @can('admin')
    管理者
@elsecan('sales')
    業務
@else
    根本沒有
@endcan
    </div>
    <div class="accordion" id="accordionPanelsStayOpenExample">
        <div class="accordion-item search-accordion">
            <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
                    aria-controls="panelsStayOpen-collapseOne">
                    查詢條件
                </button>
            </h2>
            <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
                aria-labelledby="panelsStayOpen-headingOne">
                <div class="accordion-body">
                    <div class="container-fluid">
                        <div class="row mb-3 mt-5">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="report_date_start" class="col-sm-2 col-form-label">修報日期</label>
                                    <div class="row col-sm-10">
                                        <div class="col">
                                            <div class="input-group date">
                                                <input type="text" name="apply_date_start" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="report_date_start" aria-label="民國年"
                                                    aria-describedby="button-addon1">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                        ~
                                        <div class="col">
                                            <div class="input-group date">
                                                <input type="text" name="apply_date_end" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="report_date_end" aria-label="民國年" aria-describedby="button-addon2">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label for="match_id" class="col-sm-2 col-form-label">媒合編號</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="match_id">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="building_case_no" class="col-sm-2 col-form-label">物件編號</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="building_case_no">
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label class="col-sm-2 col-form-label" for="sales">業務</label>
                                    <div class="col-sm-10">
                                        <select class="form-select form-control" id="sales">
                                            <option value="0" selected>請選擇</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="landlord_name" class="col-sm-2 col-form-label">房東姓名</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="landlord_name">
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label for="building_addr" class="col-sm-2 col-form-label">地址</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="building_addr">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="tenant_name" class="col-sm-2 col-form-label">租客姓名</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="tenant_name">
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label for="fix_date_start" class="col-sm-2 col-form-label">修報日期</label>
                                    <div class="row col-sm-10">
                                        <div class="col">
                                            <div class="input-group date">
                                                <input type="text" name="apply_date_start" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="fix_date_start" aria-label="民國年" aria-describedby="button-addon1">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                        ~
                                        <div class="col">
                                            <div class="input-group date">
                                                <input type="text" name="apply_date_end" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="fix_date_end" aria-label="民國年" aria-describedby="button-addon2">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="please_amount" class="col-sm-2 col-form-label">金額</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="please_amount">
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label for="monthly_application_month_start"
                                        class="col-sm-2 col-form-label">月報申請月份</label>
                                    <div class="row col-sm-10">
                                        <div class="col">
                                            <div class="input-group date">
                                                <input type="text" name="apply_date_start" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="monthly_application_month_start" aria-label="民國年"
                                                    aria-describedby="button-addon1">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                        ~
                                        <div class="col">
                                            <div class="input-group date">
                                                <input type="text" name="apply_date_end" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="monthly_application_month_end" aria-label="民國年"
                                                    aria-describedby="button-addon2">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="col-sm-9 col-md-10">
                            <button class="btn btn-primary" type="button" id="search">
                                查詢
                            </button>
                            <button class="btn btn-outline-secondary" type="reset" id="reset" onclick="resetCheckBox();">
                                清除條件
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true"
                    aria-controls="panelsStayOpen-collapseTwo">
                    查詢結果
                </button>
            </h2>
            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show"
                aria-labelledby="panelsStayOpen-headingTwo">
                <div class="accordion-body">
                    <div class="container-fluid">
                        <table id="fix_management_table" class="display nowrap" style="width:100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        $.fn.dataTable.ext.search.push(
            // 申請日期
            function(settings, data, dataIndex) {
                let apply_date_start = $("#apply_date_start").val();
                let apply_date_end = $("#apply_date_end").val();
                let apply_date = data[0];
                if (checkInDate(apply_date_start, apply_date_end, apply_date, apply_date)) {
                    return true;
                }
                return false;
            },
            // 案件狀態
            function(settings, data, dataIndex) {
                let case_state = $("#case_state").val();
                let case_state_data = data[1];
                if (case_state === case_state_data || case_state == "0") {
                    return true;
                }
                return false;
            },
            // 業務
            function(settings, data, dataIndex) {
                let sales = $("#sales").val();
                let sales_data = data[2];
                if (sales == sales_data || sales === "0") {
                    return true;
                }
                return false;
            },
            // 包租/代管
            function(settings, data, dataIndex) {
                let case_type = $("#case_type").val();
                let case_type_data = data[3];
                if (case_type_data.includes(case_type) || case_type === "0") {
                    return true;
                }
                return false;
            },
            // 租客編號
            function(settings, data, dataIndex) {
                let case_no = $("#case_no").val();
                let case_no_data = data[4];
                if (case_no_data.includes(case_no) || case_no === "") {
                    return true;
                }
                return false;
            },
            // 內政部編號
            function(settings, data, dataIndex) {
                let government_no = $("#government_no").val();
                let government_no_data = data[5];
                if (government_no_data.includes(government_no) || government_no === "") {
                    return true;
                }
                return false;
            },
            // 租客姓名
            function(settings, data, dataIndex) {
                let name = $("#name").val();
                let name_data = data[6];
                if (name_data.includes(name) || name === "") {
                    return true;
                }
                return false;
            },
        );

        function deleteTenantData(tenant_id) {
            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    $.ajax({
                        type: 'DELETE',
                        url: `/api/tenant_management_news/${tenant_id}`,
                        success: function(data) {
                            Swal.fire('資料已被刪除', '', 'success').then((result) => {
                                $('#tenant_management_table').DataTable().ajax.reload();
                            })
                        }
                    });
                }
            })
        }
        $(document).ready(async function() {
            let salesmanData = await getSalesmanData();
            console.log(salesmanData);
            // 設定搜尋部份的業務選項
            salesmanData.forEach((item) => {
                $("#sales").append($("<option></option>").attr("value", item.name).text(item.name));
            });
            // 初始化日期選擇器
            $('.input-group').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });
            // 初始化datatables
            const tenant_management_table = $('#fix_management_table').DataTable({
                "responsive": true,
                "searching": false,
                "dom": 'Bfrtip',
                "buttons": [{
                    text: '新增案件',
                    action: function(e, dt, node, config) {
                        window.location.href =
                            `/tenantManagementNews/create`
                    }
                }],
                "ajax": {
                    type: 'GET',
                    url: `/api/fix_managements`,
                    dataSrc: function(data) {
                        console.log(data);
                        return data.data.map((value, index, array) => {
                            // 前處理地址
                            let building_address_num =
                                value['building_address_num'] ?
                                `${value['building_address_num']}` :
                                "";
                            let building_address_num_hyphen =
                                value['building_address_num_hyphen'] ?
                                `之${value['building_address_num_hyphen']}` :
                                "";
                            let building_address_num_full = building_address_num ?
                                building_address_num +
                                building_address_num_hyphen + "號" : "";
                            let building_address_ln =
                                value['building_address_ln'] ?
                                `${value['building_address_ln']}巷` :
                                "";
                            let building_address_aly =
                                value['building_address_aly'] ?
                                `${value['building_address_aly']}弄` :
                                "";
                            let building_address_floor =
                                value['building_address_floor'] ?
                                `${value['building_address_floor']}樓` :
                                "";
                            let building_address_floor_sub =
                                value['building_address_floor_sub'] ?
                                `之${value['building_address_floor_sub']}` :
                                "";
                            let building_address_room_num =
                                value['building_address_room_num'] ?
                                `${value['building_address_room_num']}室` :
                                "";
                            value['building_address'] = `
                            ${value['building_address_city']?value['building_address_city']:""}
                            ${value['city_value']?value['city_value']:""}
                            ${value['building_address_street']?value['building_address_street']:""}
                            ${building_address_ln}
                            ${building_address_aly}
                            ${building_address_num}
                            ${building_address_floor}
                            ${building_address_floor_sub}
                            ${building_address_room_num}`;
                            // 前處理月報申請月份
                            let monthly_application_year = value[
                                "monthly_application_month"] ? new Date(value[
                                "monthly_application_month"]).getFullYear() - 1911 : "";
                            let monthly_application_month = value[
                                "monthly_application_month"] ? new Date(value[
                                "monthly_application_month"]).getMonth() + 1 : "";
                            value["monthly_application_month"] =
                                `${monthly_application_year}年${monthly_application_month}月`;
                            // 報修日期
                            value["report_date"] = value["report_date"] ? convertToRCDate(
                                value["report_date"]) : "";
                            // 修繕日期
                            value["fix_date"] = value["fix_date"] ? convertToRCDate(value[
                                "fix_date"]) : "";
                            return value;
                        });
                    }
                },
                "columns": [{
                        data: 'report_date',
                        title: "報修日期"
                    },
                    {
                        data: 'match_id',
                        title: "媒合編號"
                    },
                    {
                        data: 'building_case_no',
                        title: "物件編號",
                    },
                    {
                        data: 'sales_name',
                        title: "業務"
                    },
                    {
                        data: 'landlord_name',
                        title: '房東姓名'
                    },
                    {
                        data: 'building_address',
                        title: '地址'
                    },
                    {
                        data: 'tenant_name',
                        title: '租客姓名'
                    },
                    {
                        data: 'fix_date',
                        title: "修繕日期"
                    },
                    {
                        data: 'please_amount',
                        title: '金額',
                    },
                    {
                        data: 'monthly_application_month',
                        title: '月報申請月份',
                    },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            return `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit'">編輯</button >` +
                                `<button type="button" class="btn btn-primary btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit?cantedit=true'">檢視</button >` +
                                `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteTenantData(${data.id});">刪除</button >`
                        }
                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });

            $('#search').on('click', function() {
                tenant_management_table.draw();
            });
        });
    </script>
@endpush
