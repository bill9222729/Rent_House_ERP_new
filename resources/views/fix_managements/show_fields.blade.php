<!-- Match Id Field -->
<div class="form-group col-12">
    {!! Form::label('match_id', __('models/fixManagements.fields.match_id').':') !!}
    <p>{{ $fixManagement->match_id }}</p>
</div>


<!-- Building Case No Field -->
<div class="form-group col-12">
    {!! Form::label('building_case_no', __('models/fixManagements.fields.building_case_no').':') !!}
    <p>{{ $fixManagement->building_case_no }}</p>
</div>


