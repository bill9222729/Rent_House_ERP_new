@extends('layouts.app')

@section('content')
    {{-- <style>
        .table tbody tr:first-child td {
            text-align: center;
        }
    </style> --}}
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>應付帳列表</h1>
                <table class="table">
                    <tr>
                        <td>付款日期</td>
                        <td>金額</td>
                        <td>物件編號</td>
                        <td>租客姓名</td>
                        <td>租客電話</td>
                        <td>負責業務員</td>
                    </tr>
                    @foreach ($payedContracts as $contract)
                        <tr>
                            <td>{{ $contract['payed_at'] }}</td>
                            <td>{{ $contract['price'] }}</td>
                            <td>{{ $contract['item_no'] }}</td>
                            <td>{{ $contract['t_name'] }}</td>
                            <td>{{ $contract['t_phone'] }}</td>
                            <td>{{ $contract['sales'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-md-12">
                <h1>未付帳列表</h1>
                <table class="table">
                    <tr>
                        <td>金額</td>
                        <td>物件編號</td>
                        <td>租客姓名</td>
                        <td>租客電話</td>
                        <td>負責業務員</td>
                    </tr>
                    @foreach ($notPayContracts as $contract)
                        <tr>
                            <td>{{ $contract['price'] }}</td>
                            <td>{{ $contract['item_no'] }}</td>
                            <td>{{ $contract['t_name'] }}</td>
                            <td>{{ $contract['t_phone'] }}</td>
                            <td>{{ $contract['sales'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-md-12">
                <h1>合約將到期列表</h1>
                <table class="table">
                    <tr>
                        <td>合約起始日期</td>
                        <td>合約終止日期</td>
                        <td>金額</td>
                        <td>物件編號</td>
                        <td>租客姓名</td>
                        <td>租客電話</td>
                        <td>負責業務員</td>
                    </tr>
                    @foreach ($contractsEnd30Days as $contract)
                        <tr>
                            <td>{{ $contract->landlord_contract_date }}</td>
                            <td style="color: red;">{{ $contract->landlord_contract_expiry_date }}</td>
                            <td>{{ $contract->actual_contract_rent_to_landlord }}</td>
                            <td>{{ $contract->landlord_item_number }}</td>
                            @if ($contract->tenantManagement)
                                <td>{{ $contract->tenantManagement->lessee_name }}</td>
                                <td>{{ $contract->tenantManagement->lessee_cellphone }}</td>
                            @else
                                <td></td>
                                <td></td>
                            @endif
                            <td>{{ $contract->contract_business_sign_back }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
@endsection
