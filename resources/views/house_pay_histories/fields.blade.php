<div class="row">
<!-- Contract Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contract_id', __('models/housePayHistories.fields.contract_id').':') !!}
    {!! Form::number('contract_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Month Field -->
<div class="form-group col-sm-6">
    {!! Form::label('month', __('models/housePayHistories.fields.month').':') !!}
    {!! Form::number('month', null, ['class' => 'form-control']) !!}
</div>

<!-- Money Field -->
<div class="form-group col-sm-6">
    {!! Form::label('money', __('models/housePayHistories.fields.money').':') !!}
    {!! Form::number('money', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/housePayHistories.fields.type').':') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Freq Field -->
<div class="form-group col-sm-6">
    {!! Form::label('freq', __('models/housePayHistories.fields.freq').':') !!}
    {!! Form::number('freq', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
