<!-- Contract Id Field -->
<div class="form-group col-12">
    {!! Form::label('contract_id', __('models/housePayHistories.fields.contract_id').':') !!}
    <p>{{ $housePayHistory->contract_id }}</p>
</div>


<!-- Month Field -->
<div class="form-group col-12">
    {!! Form::label('month', __('models/housePayHistories.fields.month').':') !!}
    <p>{{ $housePayHistory->month }}</p>
</div>


<!-- Money Field -->
<div class="form-group col-12">
    {!! Form::label('money', __('models/housePayHistories.fields.money').':') !!}
    <p>{{ $housePayHistory->money }}</p>
</div>


<!-- Type Field -->
<div class="form-group col-12">
    {!! Form::label('type', __('models/housePayHistories.fields.type').':') !!}
    <p>{{ $housePayHistory->type }}</p>
</div>


<!-- Freq Field -->
<div class="form-group col-12">
    {!! Form::label('freq', __('models/housePayHistories.fields.freq').':') !!}
    <p>{{ $housePayHistory->freq }}</p>
</div>


