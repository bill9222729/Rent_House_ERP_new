<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Image Uploading with Vue + Laravel</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
</head>

<body class="flex flex-col justify-center">
    <div class="max-w-lg mx-auto mt-24 flex flex-col justify-center items-center">
        <img id="star" src="https://i.imgur.com/N7r31lQ.png" class="animate-bounce w-20" />
        <div class="flex justy-center items-center flex-col hidden" id="test">
            <div class="flex">
                <img class="h-60 animate-bounce" src="https://i.imgur.com/VJRcip7.png" />
                <img class="h-60" src="https://i.imgur.com/ypIFLAy.jpg" />
                <div class="flex justy-center items-center animate-bounce">
                    <img class="h-40" src="https://i.imgur.com/VJRcip7.png" />
                    <h2>←PP用</h1>
                </div>
            </div>
            <h1 class="text-4xl font-bold text-center"></h1>
        </div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $("#star").on("click", () => {
        if ($("#test").hasClass("hidden")) {
            $("#test").removeClass("hidden");
            $("h1").text("阿乏早安~");
        } else {
            $("#test").addClass("hidden");
        }
    });
</script>

</html>
