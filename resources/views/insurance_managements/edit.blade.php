@extends('layouts.app')
<style>
    .checkbox-input {
        color: #212529;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        appearance: none;
        border-radius: 0.25rem;
        line-height: 0;
    }

</style>
@section('content')
    <div class="accordion" id="accordionPanelsStayOpenExample">
        @include('insurance_managements.fields')
    </div>
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-sm-1">
                <button type="button" class="btn btn-primary" id="create-button">儲存</button>
            </div>
            <div class="col-sm-1">
                <button type="button" class="btn btn-secondary" id="cancel-button"
                    onclick="window.location='/insuranceManagements'">取消</button>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        $(document).ready(async function() {
            let insuranceManagement = @json($insuranceManagement)
            // let salesmanData = await getSalesmanData();
            let matchData = await getAllMatchs();
            let buildingData = await getAllBuildings();
            console.log(insuranceManagement);

            // 設定媒合編號的選單
            matchData.forEach((item) => {
                $("#match_id").append($("<option></option>").attr("value", item.match_id).text(item
                    .match_id));
            });
            $("#match_id").selectpicker('refresh');

            // 設定物件清單的選單
            buildingData.forEach((item) => {
                $("#building_id").append($("<option></option>").attr("value", item.case_no).text(item
                    .case_no));
            });
            $("#building_id").selectpicker('refresh');

            // 設定物件選單選擇事件
            $("#building_id").on("change", async (e) => {
                let buildingInfo = await getBuildingData(e.target.value);
                buildingInfo = buildingInfo[0];
                let building_address_num =
                    buildingInfo['building_address_num'] ?
                    `${buildingInfo['building_address_num']}` :
                    "";
                let building_address_num_hyphen =
                    buildingInfo['building_address_num_hyphen'] ?
                    `之${buildingInfo['building_address_num_hyphen']}` :
                    "";
                let building_address_num_full = building_address_num ?
                    building_address_num +
                    building_address_num_hyphen + "號" : "";
                let building_address_ln =
                    buildingInfo['building_address_ln'] ?
                    `${buildingInfo['building_address_ln']}巷` :
                    "";
                let building_address_aly =
                    buildingInfo['building_address_aly'] ?
                    `${buildingInfo['building_address_aly']}弄` :
                    "";
                let building_address_floor =
                    buildingInfo['building_address_floor'] ?
                    `${buildingInfo['building_address_floor']}樓` :
                    "";
                let building_address_floor_sub =
                    buildingInfo['building_address_floor_sub'] ?
                    `之${buildingInfo['building_address_floor_sub']}` :
                    "";
                let building_address_room_num =
                    buildingInfo['building_address_room_num'] ?
                    `${buildingInfo['building_address_room_num']}室` :
                    "";
                buildingInfo['building_address'] = `
                    ${buildingInfo['building_address_city']?buildingInfo['building_address_city']:""}
                    ${buildingInfo['city_value']?buildingInfo['city_value']:""}
                    ${buildingInfo['building_address_street']?buildingInfo['building_address_street']:""}
                    ${building_address_ln}
                    ${building_address_aly}
                    ${building_address_num}
                    ${building_address_floor}
                    ${building_address_floor_sub}
                    ${building_address_room_num}
                `;
                $("#building_addr").val(buildingInfo['building_address'].replace(/ /g, ""));
                $("#name").val(buildingInfo['name']);
                $("#sales_name").val(buildingInfo['sales_name']);
                $("#sales_id").val(buildingInfo['sales_id']);
            });
            initBuildingManagementsPage();
            // 把資料放到畫面上
            $("#match_id").selectpicker('val', insuranceManagement['match_id']);
            $("#building_id").selectpicker('val', insuranceManagement['building_id']);
            $("#building_id").change();
            $("#insurance_start").val(convertToRCDate(insuranceManagement['insurance_start']));
            $("#insurance_end").val(convertToRCDate(insuranceManagement['insurance_end']));
            $("#monthly_application_month").val(convertToRCDate(insuranceManagement[
                'monthly_application_month']));
            $("#insurance_premium").val(insuranceManagement['insurance_premium']);
            // 新增資料按鈕的事件
            $("#create-button").click(function() {
                let data = getAllfieldsValue();
                // 存進資料庫
                $.ajax({
                    method: "PATCH",
                    url: `/api/insurance_managements/${insuranceManagement['id']}`,
                    data: data,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            '成功',
                            '資料已成功新增',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "/insuranceManagements";
                            }
                        })
                    },
                    error: function(err) {
                        console.error(err);
                    }
                })
            })
            // 初始化 上傳檔案部分
            // 取得上傳的檔案
            let fileData = await getFileArr("insurance_management", insuranceManagement ?
                insuranceManagement['id'] : "");
            $("#insurance_file_input").fileinput({
                language: "zh-TW",
                uploadUrl: "/api/file_managments",
                maxFileCount: 0,
                maxTotalFileCount: 0,
                maxFileSize:5000,
                msgSizeTooLarge:`檔案 "{name}" (<b>{size} KB</b>) 大小超過上限 <b>5 MB</b>`,
                uploadExtraData: {
                    "file_source_id": insuranceManagement ? insuranceManagement['id'] : "",
                    "file_source": "insurance_management",
                },
                overwriteInitial: false,
                initialPreview: fileData["initialPreview"],
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: 'pdf', // image is the default and can be overridden in config below
                initialPreviewConfig: fileData["initialPreviewConfig"],
                ajaxDeleteSettings: {
                    type: 'DELETE' // This should override the ajax as $.ajax({ type: 'DELETE' })
                }
            });
        });
    </script>
@endpush
