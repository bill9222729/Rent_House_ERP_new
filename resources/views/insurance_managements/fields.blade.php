<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse"
            data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
            aria-controls="panelsStayOpen-collapseOne">
            編輯物件出租申請
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
        aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">媒合編號</label>
                        <div class="col-sm-6">
                            <select class="form-select form-control selectpicker" data-live-search="true"
                                data-style="border text-dark bg-white" data-size="5" name="match_id" id="match_id">
                                <option>請選擇</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">物件編號</label>
                        <div class="col-sm-6">
                            <select class="form-select form-control selectpicker" data-live-search="true"
                                data-style="border text-dark bg-white" data-size="5" name="building_id"
                                id="building_id">
                                <option>請選擇</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">保險期間</label>
                        <div class="row col-sm-6">
                            <div class="col gx-1">
                                <div class="input-group date day">
                                    <input type="text" name="insurance_start" value="" class="form-control"
                                        autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                        id="insurance_start" aria-label="民國年" aria-describedby="button-addon1">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                            ~
                            <div class="col gx-1">
                                <div class="input-group date day">
                                    <input type="text" name="insurance_end" value="" class="form-control"
                                        autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="insurance_end"
                                        aria-label="民國年" aria-describedby="button-addon2">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                            data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">保險業者</label>
                        <div class="col-sm-6">
                            <select class="form-select form-control selectpicker" data-live-search="true"
                                data-style="border text-dark bg-white" data-size="5" name="insurer" id="insurer">
                                <option value="旺旺友聯" selected>旺旺友聯</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="row col-md-6">
                    <label for="insurance_premium"
                        class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">保費</label>
                    <div class="col-sm-6">
                        <input type="text" name="insurance_premium" class="form-control" id="insurance_premium">
                    </div>
                </div>
                <div class="row col-md-6">
                    <label for="monthly_application_month" class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">月報申請月份</label>
                    <div class="col-sm-6">
                        <div class="input-group date month">
                            <input type="text" name="monthly_application_month" value="" class="form-control" autocomplete="off"
                                maxlength="10" size="10" placeholder="YYY-MM-DD" id="monthly_application_month" aria-label="民國年"
                                aria-describedby="button-addon1">
                            <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                    data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="row col-md-6">
                    <label for="building_addr"
                        class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">物件地址</label>
                    <div class="col-sm-6">
                        <input type="text" name="building_addr" class="form-control" id="building_addr" readonly>
                    </div>
                </div>
                <div class="row col-md-6">
                    <label for="name" class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">所有權人</label>
                    <div class="col-sm-6">
                        <input type="text" name="name" class="form-control" id="name" readonly>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="row col-md-6">
                    <label for="sales_name" class="col-sm-4 col-form-label text-end fw-bolder text-nowrap">負責業務</label>
                    <div class="col-sm-6">
                        <input type="text" name="sales_name" class="form-control" id="sales_name" readonly>
                        <input type="hidden" name="sales_id" class="form-control" id="sales_id">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 檢附文件 --}}
@if (isset($insuranceManagement))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true"
                aria-controls="panelsStayOpen-collapseFive">
                檢附文件
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show"
            aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="insurance_file_input" name="source" type="file" multiple>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
