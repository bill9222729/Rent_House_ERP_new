<div class="row">
<!-- Sales Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sales_id', __('models/insuranceManagements.fields.sales_id').':') !!}
    {!! Form::number('sales_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Match Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('match_id', __('models/insuranceManagements.fields.match_id').':') !!}
    {!! Form::text('match_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_id', __('models/insuranceManagements.fields.building_id').':') !!}
    {!! Form::text('building_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Insurance Premium Field -->
<div class="form-group col-sm-6">
    {!! Form::label('insurance_premium', __('models/insuranceManagements.fields.insurance_premium').':') !!}
    {!! Form::number('insurance_premium', null, ['class' => 'form-control']) !!}
</div>

<!-- Insurance Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('insurance_start', __('models/insuranceManagements.fields.insurance_start').':') !!}
    {!! Form::date('insurance_start', null, ['class' => 'form-control','id'=>'insurance_start']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#insurance_start').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Insurance End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('insurance_end', __('models/insuranceManagements.fields.insurance_end').':') !!}
    {!! Form::date('insurance_end', null, ['class' => 'form-control','id'=>'insurance_end']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#insurance_end').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Monthly Application Month Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monthly_application_month', __('models/insuranceManagements.fields.monthly_application_month').':') !!}
    {!! Form::date('monthly_application_month', null, ['class' => 'form-control','id'=>'monthly_application_month']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#monthly_application_month').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
