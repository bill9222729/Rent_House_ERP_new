@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">保險管理</h3>
    </div>
    <div class="accordion" id="accordionPanelsStayOpenExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
                    aria-controls="panelsStayOpen-collapseOne">
                    查詢條件
                </button>
            </h2>
            <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
                aria-labelledby="panelsStayOpen-headingOne">
                <div class="accordion-body">
                    <div class="container search_condition">
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label class="col-sm-2 col-form-label" for="sales">業務</label>
                                    <div class="col-sm-10">
                                        <select class="form-select form-control" id="sales">
                                            <option value="" selected>請選擇</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label for="match_id" class="col-sm-2 col-form-label">媒合編號</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="match_id">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="case_no" class="col-sm-2 col-form-label">物件編號</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="case_no" style="text-transform:uppercase">
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label for="insurer" class="col-sm-2 col-form-label">保險業者</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="insurer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="building_addr" class="col-sm-2 col-form-label">物件地址</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="building_addr">
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label for="owner" class="col-sm-2 col-form-label">所有權人</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="owner">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label for="insurance_premium" class="col-sm-2 col-form-label">保費</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="insurance_premium">
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <label class="col-sm-2 col-form-label">保險期間</label>
                                    <div class="row col-sm-10">
                                        <div class="col">
                                            <div class="input-group date day">
                                                <input type="text" name="insurance_start" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="insurance_start" aria-label="民國年" aria-describedby="button-addon1">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                        ~
                                        <div class="col">
                                            <div class="input-group date day">
                                                <input type="text" name="insurance_end" value="" class="form-control"
                                                    autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD"
                                                    id="insurance_end" aria-label="民國年" aria-describedby="button-addon2">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="row col-md-12">
                                <div class="row col-md-6">
                                    <label class="col-sm-2 col-form-label" for="monthly_application_month">月報申請月份</label>
                                    <div class="col-sm-6">
                                        <div class="input-group date month">
                                            <input type="text" name="monthly_application_month" value=""
                                                class="form-control" autocomplete="off" maxlength="10" size="10"
                                                placeholder="YYY-MM-DD" id="monthly_application_month" aria-label="民國年"
                                                aria-describedby="button-addon1">
                                            <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                    data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="col-sm-9 col-md-10">
                            <button class="btn btn-primary" type="button" id="search">
                                查詢
                            </button>
                            <button class="btn btn-outline-secondary" type="reset" id="reset" onclick="resetCheckBox();">
                                清除條件
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true"
                    aria-controls="panelsStayOpen-collapseTwo">
                    查詢結果
                </button>
            </h2>
            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show"
                aria-labelledby="panelsStayOpen-headingTwo">
                <div class="accordion-body">
                    <div class="container-fluid">
                        <table id="insurance_management_table" class="display nowrap" style="width:100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        const deleteInsuranceData = async (id) => {
            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    $.ajax({
                        type: 'DELETE',
                        url: `/api/insurance_managements/${id}`,
                        success: function(data) {
                            Swal.fire('資料已被刪除', '', 'success').then((result) => {
                                $('#insurance_management_table').DataTable().ajax
                                    .reload();
                            })
                        }
                    });
                }
            })
        }
        $.fn.dataTable.ext.search.push(
            // 業務
            function(settings, data, dataIndex) {
                let sales = $("#sales").val();
                let sales_data = data[0];
                if (sales == sales_data || sales === "") {
                    return true;
                }
                return false;
            },
            // 媒合編號
            function(settings, data, dataIndex) {
                let match_id = $("#match_id").val();
                let match_id_data = data[1];
                if (match_id_data.includes(match_id) || match_id === "") {
                    return true;
                }
                return false;
            },
            // 物件編號
            function(settings, data, dataIndex) {
                let case_no = $("#case_no").val();
                let case_no_data = data[2];
                if (case_no_data.includes(case_no) || case_no === "") {
                    return true;
                }
                return false;
            },
            // 保險業者
            function(settings, data, dataIndex) {
                let insurer = $("#insurer").val();
                let insurer_data = data[3];
                if (insurer_data.includes(insurer) || insurer === "") {
                    return true;
                }
                return false;
            },
            // 物件地址
            function(settings, data, dataIndex) {
                let building_address = $("#building_addr").val();
                let building_address_data = data[4];
                if (building_address_data.includes(building_address) || building_address === "") {
                    return true;
                }
                return false;
            },
            // 所有權人
            function(settings, data, dataIndex) {
                let owner = $("#owner").val();
                let owner_data = data[5];
                if (owner_data.includes(owner) || owner === "") {
                    return true;
                }
                return false;
            },
            // 保費
            function(settings, data, dataIndex) {
                let insurance_premium = $("#insurance_premium").val();
                let insurance_premium_data = data[6];
                if (insurance_premium === insurance_premium_data || insurance_premium === "") {
                    return true;
                }
                return false;
            },
            // 委託租賃
            function(settings, data, dataIndex) {
                let insurance_start = $("#insurance_start").val();
                let insurance_end = $("#insurance_end").val();
                let insurance_start_data = data[7] ? data[7].split("~")[0] : "";
                let insurance_end_data = data[7] ? data[7].split("~")[1] : "";
                if (checkInDate(
                        insurance_start,
                        insurance_end,
                        insurance_start_data,
                        insurance_end_data)) {
                    return true;
                }
                return false;
            },
            // 月報申請月份
            function(settings, data, dataIndex) {
                let monthly_application_month = $("#monthly_application_month").val();
                let monthly_application_month_data = data[8];
                let monthly_application_month_year = new Date(monthly_application_month).getFullYear();
                let monthly_application_month_month = new Date(monthly_application_month).getMonth() + 1;
                let monthly_application_month_format =
                    `${monthly_application_month_year}年${monthly_application_month_month}月`;
                console.log(monthly_application_month);
                if ((monthly_application_month_format === monthly_application_month_data) ||
                    (monthly_application_month === "")) {
                    return true;
                }
                return false;
            }
        );
        $(document).ready(async function() {
            // 取得業務資料
            let salesmanData = await getSalesmanData();
            let salesObj = {}; // 業務id跟名子的對照陣列
            // 設定搜尋部份的業務選項
            salesmanData.forEach((item) => {
                $("#sales").append($("<option></option>").attr("value", item.name).text(item.name));
                salesObj[item.id] = item.name
            });
            // 初始化日期選擇器
            $(".input-group.date.day").datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });
            // 初始化月份選擇器
            $(".input-group.date.month").datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
                viewMode: "months",
                minViewMode: "months",
            });
            // 初始化datatables
            const table = $('#insurance_management_table').DataTable({
                "pageLength": 20,
                "lengthChange": true,
                "responsive": true,
                "searching": true,
                "dom": 'Bfrtip',
                "buttons": [{
                    text: '新增保險',
                    action: function(e, dt, node, config) {
                        window.location.href =
                            `/insuranceManagements/create`
                    }
                }],
                "ajax": {
                    type: 'GET',
                    url: `/api/insurance_managements`,
                    dataSrc: function(data) {
                        console.log(data.data);
                        return data.data.map((value, index, array) => {
                            // 前處理業務資料
                            let building_address_num =
                                value['building_address_num'] ?
                                `${value['building_address_num']}` :
                                "";
                            let building_address_num_hyphen =
                                value['building_address_num_hyphen'] ?
                                `之${value['building_address_num_hyphen']}` :
                                "";
                            let building_address_num_full = building_address_num ?
                                building_address_num +
                                building_address_num_hyphen + "號" : "";
                            let building_address_ln =
                                value['building_address_ln'] ?
                                `${value['building_address_ln']}巷` :
                                "";
                            let building_address_aly =
                                value['building_address_aly'] ?
                                `${value['building_address_aly']}弄` :
                                "";
                            let building_address_floor =
                                value['building_address_floor'] ?
                                `${value['building_address_floor']}樓` :
                                "";
                            let building_address_floor_sub =
                                value['building_address_floor_sub'] ?
                                `之${value['building_address_floor_sub']}` :
                                "";
                            let building_address_room_num =
                                value['building_address_room_num'] ?
                                `${value['building_address_room_num']}室` :
                                "";
                            let insurance_start =
                                value['insurance_start'] ?
                                convertToRCDate(value['insurance_start']) : "";
                            let insurance_end =
                                value['insurance_end'] ?
                                convertToRCDate(value['insurance_end']) : "";
                            let monthly_application_year = value[
                                "monthly_application_month"] ? new Date(value[
                                "monthly_application_month"]).getFullYear() - 1911 : "";
                            let monthly_application_month = value[
                                "monthly_application_month"] ? new Date(value[
                                "monthly_application_month"]).getMonth() + 1 : "";
                            value["insurance_date"] = insurance_start + "~" + insurance_end;
                            value["monthly_application_month"] =
                                `${monthly_application_year}年${monthly_application_month}月`;
                            value['building_address'] = `
                            ${value['building_address_city']?value['building_address_city']:""}
                            ${value['city_area_name']?value['city_area_name']:""}
                            ${value['building_address_street']?value['building_address_street']:""}
                            ${building_address_ln}
                            ${building_address_aly}
                            ${building_address_num}
                            ${building_address_floor}
                            ${building_address_floor_sub}
                            ${building_address_room_num}
                        `;
                            return value;
                        });
                    }
                },
                "columns": [{
                        data: 'salesman_name',
                        title: "業務",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'match_id',
                        title: "媒合編號",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'building_id',
                        title: "物件編號",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'insurer',
                        title: "保險業者",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'building_address',
                        title: "物件地址"
                    },
                    {
                        data: 'name',
                        title: "所有權人",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'insurance_premium',
                        title: "保費",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'insurance_date',
                        title: "保險期間",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: 'monthly_application_month',
                        title: "月報申請月份",
                        className: 'dt-body-center dt-head-center',
                    },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            return `<button type="button" class="mx-1 btn btn-warning btn-sm" onclick="location.href='insuranceManagements/${data.id}/edit'">編輯</button >` +
                                // `<button type="button" class="mx-1 btn btn-primary btn-sm" onclick="location.href='insuranceManagements/${data.id}/edit?cantedit=true'">檢視</button >` +
                                `<button type="button" class="mx-1 btn btn-danger btn-sm" onclick="deleteInsuranceData(${data.id});">刪除</button >`
                        },
                        className: 'dt-body-center dt-head-center',
                    }
                ],
                // responsive: {
                //     details: {
                //         type: 'column'
                //     }
                // },
                // columnDefs: [{
                //     className: 'dtr-control',
                //     orderable: false,
                //     targets: 0
                // }],
                // order: [1, 'asc'],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });

            $('#search').on('click', function() {
                table.draw();
            });
            $("#reset").on('click', function() {
                $(".search_condition input").val("");
                $(".search_condition select").val("");
            });
        });
    </script>
@endpush
