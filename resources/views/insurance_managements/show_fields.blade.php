<!-- Sales Id Field -->
<div class="form-group col-12">
    {!! Form::label('sales_id', __('models/insuranceManagements.fields.sales_id').':') !!}
    <p>{{ $insuranceManagement->sales_id }}</p>
</div>


<!-- Match Id Field -->
<div class="form-group col-12">
    {!! Form::label('match_id', __('models/insuranceManagements.fields.match_id').':') !!}
    <p>{{ $insuranceManagement->match_id }}</p>
</div>


<!-- Building Id Field -->
<div class="form-group col-12">
    {!! Form::label('building_id', __('models/insuranceManagements.fields.building_id').':') !!}
    <p>{{ $insuranceManagement->building_id }}</p>
</div>


<!-- Insurance Premium Field -->
<div class="form-group col-12">
    {!! Form::label('insurance_premium', __('models/insuranceManagements.fields.insurance_premium').':') !!}
    <p>{{ $insuranceManagement->insurance_premium }}</p>
</div>


<!-- Insurer Field -->
<div class="form-group col-12">
    {!! Form::label('insurer', __('models/insuranceManagements.fields.insurer').':') !!}
    <p>{{ $insuranceManagement->insurer }}</p>
</div>


<!-- Insurance Start Field -->
<div class="form-group col-12">
    {!! Form::label('insurance_start', __('models/insuranceManagements.fields.insurance_start').':') !!}
    <p>{{ $insuranceManagement->insurance_start }}</p>
</div>


<!-- Insurance End Field -->
<div class="form-group col-12">
    {!! Form::label('insurance_end', __('models/insuranceManagements.fields.insurance_end').':') !!}
    <p>{{ $insuranceManagement->insurance_end }}</p>
</div>


<!-- Monthly Application Month Field -->
<div class="form-group col-12">
    {!! Form::label('monthly_application_month', __('models/insuranceManagements.fields.monthly_application_month').':') !!}
    <p>{{ $insuranceManagement->monthly_application_month }}</p>
</div>


