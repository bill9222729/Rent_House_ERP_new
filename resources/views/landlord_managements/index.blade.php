@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">@lang('models/landlordManagements.plural')</h3>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;z-index: 100" href="{{ route('landlordManagements.create') }}">@lang('crud.add_new')</a>
        <!-- Upload Button modal -->
        <button type="button" class="btn btn-warning pull-right" style="margin-top: -10px; margin-right: 6px;" data-toggle="modal" data-target="#UploadModal">上傳excel</button>

        <!-- Modal -->
        <div class="modal fade" id="UploadModal" tabindex="-1" role="dialog" aria-labelledby="UploadModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="UploadModalLabel" style="display:inline;">房東檔案上傳</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <hr>
                    <div class="modal-body" style="display: -webkit-box;">
                        <form action="{{ action('LandlordManagementController@UploadExcel') }}" method="POST" enctype="multipart/form-data" style="float:left; padding:28px 0 0 10px; display:contents;">
                            {{ csrf_field() }}
                            <input name="upload_file" type="file" style="float:left; width:35%;">
                            <button type="submit" class="btn" style="padding: 7px; margin-top: -5px;"><i class="voyager-upload" style="margin-right: 5px;"></i>上傳</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg" style="margin-top: 40px;">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('landlord_managements.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

