<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/landlordManagements.fields.name').':') !!}
    <p>{{ $landlordManagement->name }}</p>
</div>


<!-- Id Num Field -->
<div class="form-group col-12">
    {!! Form::label('id_num', __('models/landlordManagements.fields.id_num').':') !!}
    <p>{{ $landlordManagement->id_num }}</p>
</div>


<!-- Address Field -->
<div class="form-group col-12">
    {!! Form::label('address', __('models/landlordManagements.fields.address').':') !!}
    <p>{{ $landlordManagement->address }}</p>
</div>


<!-- Phone Field -->
<div class="form-group col-12">
    {!! Form::label('phone', __('models/landlordManagements.fields.phone').':') !!}
    <p>{{ $landlordManagement->phone }}</p>
</div>


<!-- Birthday Field -->
<div class="form-group col-12">
    {!! Form::label('birthday', __('models/landlordManagements.fields.birthday').':') !!}
    <p>{{ $landlordManagement->birthday }}</p>
</div>


<!-- Bank Acc Field -->
<div class="form-group col-12">
    {!! Form::label('bank_acc', __('models/landlordManagements.fields.bank_acc').':') !!}
    <p>{{ $landlordManagement->bank_acc }}</p>
</div>

<div class="form-group col-12">
    {!! Form::label('landlord_match_number', __('models/landlordManagements.fields.landlord_match_number').':') !!}
    <p>{{ $landlordManagement->landlord_match_number }}</p>
</div>

<div class="form-group col-12">
    {!! Form::label('landlord_existing_tenant', __('models/landlordManagements.fields.landlord_existing_tenant').':') !!}
    <p>{{ $landlordManagement->landlord_existing_tenant }}</p>
</div>

<div class="form-group col-12">
    {!! Form::label('landlord_contract_date', __('models/landlordManagements.fields.landlord_contract_date').':') !!}
    <p>{{ $landlordManagement->landlord_contract_date }}</p>
</div>

<div class="form-group col-12">
    {!! Form::label('landlord_contract_expiry_date', __('models/landlordManagements.fields.landlord_contract_expiry_date').':') !!}
    <p>{{ $landlordManagement->landlord_contract_expiry_date }}</p>
</div>

<div class="form-group col-12">
    {!! Form::label('landlord_item_number', __('models/landlordManagements.fields.landlord_item_number').':') !!}
    <p>{{ $landlordManagement->landlord_item_number }}</p>
</div>

<div class="form-group col-12">
    {!! Form::label('landlord_ministry_of_the_interior_number', __('models/landlordManagements.fields.landlord_ministry_of_the_interior_number').':') !!}
    <p>{{ $landlordManagement->landlord_ministry_of_the_interior_number }}</p>
</div>

<div class="form-group col-12">
    {!! Form::label('landlord_types_of', __('models/landlordManagements.fields.landlord_types_of').':') !!}
    <p>{{ $landlordManagement->landlord_types_of }}</p>
</div>


