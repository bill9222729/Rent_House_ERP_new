{{-- <li class="{{ Request::is('salesmanManagements*') ? 'active' : '' }}">
    <a href="{{ route('salesmanManagements.index') }}">
        <span class="title">@lang('models/salesmanManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}

{{-- <li class="{{ Request::is('landlordManagements*') ? 'active' : '' }}">
    <a href="{{ route('landlordManagements.index') }}">
        <span class="title">@lang('models/landlordManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}

{{-- <li class="{{ Request::is('housePayHistories*') ? 'active' : '' }}">
    <a href="{{ route('housePayHistories.index') }}">
        <span class="title">@lang('models/housePayHistories.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}
@if (\Illuminate\Support\Facades\Auth::user()->role === 'admin')
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('users.index') }}">
            <span class="title">@lang('models/users.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li>
    {{-- <li class="{{ Request::is('buildingManagements*') ? 'active' : '' }}">
        <a href="{{ route('buildingManagements.index') }}">
            <span class="title">@lang('models/buildingManagements.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li>


    <li class="{{ Request::is('tenantManagements*') ? 'active' : '' }}">
        <a href="{{ route('tenantManagements.index') }}">
            <span class="title">@lang('models/tenantManagements.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li>

    <li class="{{ Request::is('contracts*') ? 'active' : '' }}">
        <a href="{{ route('contracts.index') }}">
            <span class="title">@lang('models/contracts.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li>

    <li class="{{ Request::is('batch_upload*') ? 'active' : '' }}">
        <a href="/batch_upload">
            <span class="title">@lang('common.batch_upload')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
@else
    {{-- 房東管理 --}}
    {{-- <li class="{{ Request::is('buildingManagements*') ? 'active' : '' }}">
        <a href="{{ route('buildingManagements.index') }}">
            <span class="title">@lang('models/buildingManagements.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 租客管理 --}}
    {{-- <li class="{{ Request::is('tenantManagements*') ? 'active' : '' }}">
        <a href="{{ route('tenantManagements.index') }}">
            <span class="title">@lang('models/tenantManagements.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 合約 --}}
    {{-- <li class="{{ Request::is('contracts*') ? 'active' : '' }}">
        <a href="{{ route('contracts.index') }}">
            <span class="title">@lang('models/contracts.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 批次上傳 --}}
    {{-- <li class="{{ Request::is('batch_upload*') ? 'active' : '' }}">
        <a href="/batch_upload">
            <span class="title">@lang('common.batch_upload')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 修繕總表 --}}
    {{-- <li class="{{ Request::is('fixHistories*') ? 'active' : '' }}">
        <a href="{{ route('fixHistories.index') }}">
            <span class="title">@lang('models/fixHistories.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 交易明細 --}}
    {{-- <li class="{{ Request::is('transactionDetails*') ? 'active' : '' }}">
        <a href="{{ route('transactionDetails.index') }}">
            <span class="title">@lang('models/transactionDetails.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 匯款資料 --}}
    {{-- <li class="{{ Request::is('banks*') ? 'active' : '' }}">
        <a href="{{ route('banks.index') }}">
            <span class="title">@lang('models/banks.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 回傳格式 --}}
    {{-- <li class="{{ Request::is('returnFormats*') ? 'active' : '' }}">
        <a href="{{ route('returnFormats.index') }}">
            <span class="title">@lang('models/returnFormats.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 帳務功能 --}}
    {{-- <li class="{{ Request::is('mixedTable') ? 'active' : '' }}">
        <a href="{{ route('mixedTable') }}">
            <span class="title">帳務功能</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 員工管理 --}}
    {{-- <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('users.index') }}">
            <span class="title">@lang('models/users.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 業務管理 --}}
    {{-- <li class="{{ Request::is('businessManagements*') ? 'active' : '' }}">
        <a href="{{ route('businessManagements.index') }}">
            <span class="title">@lang('models/businessManagements.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- 日誌 --}}
    {{-- <li class="{{ Request::is('logHistories*') ? 'active' : '' }}">
        <a href="{{ route('logHistories.index') }}">
            <span class="title">@lang('models/logHistories.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
    {{-- Line設定 --}}
    {{-- <li class="{{ Request::is('lineConfigs*') ? 'active' : '' }}">
        <a href="{{ route('lineConfigs.index') }}">
            <span class="title">@lang('models/lineConfigs.plural')</span>
        </a>
        <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
    </li> --}}
@endif
<li class="{{ Request::is('buildingManagementNews*') ? 'active' : '' }}">
    <a href="{{ route('buildingManagementNews.index') }}">
        <span class="title">@lang('models/buildingManagementNews.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

{{-- <li class="{{ Request::is('cities*') ? 'active' : '' }}">
    <a href="{{ route('cities.index') }}">
        <span class="title">@lang('models/cities.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}

{{-- <li class="{{ Request::is('cityAreas*') ? 'active' : '' }}">
    <a href="{{ route('cityAreas.index') }}">
        <span class="title">@lang('models/cityAreas.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('streets*') ? 'active' : '' }}">
    <a href="{{ route('streets.index') }}">
        <span class="title">@lang('models/streets.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('locatedLots*') ? 'active' : '' }}">
    <a href="{{ route('locatedLots.index') }}">
        <span class="title">@lang('models/locatedLots.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}


{{-- <li class="{{ Request::is('contractsNews*') ? 'active' : '' }}">
    <a href="{{ route('contractsNews.index') }}">
        <span class="title">@lang('models/contractsNews.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}

{{-- <li class="{{ Request::is('tenantManagementNews*') ? 'active' : '' }}">
    <a href="{{ route('tenantManagementNews.index') }}">
        <span class="title">@lang('models/tenantManagementNews.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}

<li class="{{ Request::is('tenantManagementNews*') ? 'active' : '' }}">
    <a href="{{ route('tenantManagementNews.index') }}">
        <span class="title">@lang('models/tenantManagementNews.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('matchManagements*') ? 'active' : '' }}">
    <a href="{{ route('matchManagements.index') }}">
        <span class="title">@lang('models/matchManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

{{-- <li class="{{ Request::is('contractManagements*') ? 'active' : '' }}">
    <a href="{{ route('contractManagements.index') }}">
        <span class="title">@lang('models/contractManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}

{{-- <li class="{{ Request::is('cities*') ? 'active' : '' }}">
    <a href="{{ route('cities.index') }}">
        <span class="title">@lang('models/cities.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('cityAreas*') ? 'active' : '' }}">
    <a href="{{ route('cityAreas.index') }}">
        <span class="title">@lang('models/cityAreas.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('streets*') ? 'active' : '' }}">
    <a href="{{ route('streets.index') }}">
        <span class="title">@lang('models/streets.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('locatedLots*') ? 'active' : '' }}">
    <a href="{{ route('locatedLots.index') }}">
        <span class="title">@lang('models/locatedLots.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}
{{-- <li class="{{ Request::is('fileManagments*') ? 'active' : '' }}">
    <a href="{{ route('fileManagments.index') }}">
        <span class="title">@lang('models/fileManagments.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}
{{-- <li class="{{ Request::is('changeHistories*') ? 'active' : '' }}">
    <a href="{{ route('changeHistories.index') }}">
        <span class="title">@lang('models/changeHistories.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}

{{-- <li class="{{ Request::is('tenantFamilyInfos*') ? 'active' : '' }}">
    <a href="{{ route('tenantFamilyInfos.index') }}">
        <span class="title">@lang('models/tenantFamilyInfos.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('tenantMinorOffsprings*') ? 'active' : '' }}">
    <a href="{{ route('tenantMinorOffsprings.index') }}">
        <span class="title">@lang('models/tenantMinorOffsprings.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

<li class="{{ Request::is('tenantBuildingInfos*') ? 'active' : '' }}">
    <a href="{{ route('tenantBuildingInfos.index') }}">
        <span class="title">@lang('models/tenantBuildingInfos.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}
<li class="{{ Request::is('insuranceManagements*') ? 'active' : '' }}">
    <a href="{{ route('insuranceManagements.index') }}">
        <span class="title">@lang('models/insuranceManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

{{-- <li class="{{ Request::is('salesmanManagements*') ? 'active' : '' }}">
    <a href="{{ route('salesmanManagements.index') }}">
        <span class="title">@lang('models/salesmanManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}


<li class="{{ Request::is('fixManagements*') ? 'active' : '' }}">
    <a href="{{ route('fixManagements.index') }}">
        <span class="title">@lang('models/fixManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li>

{{-- <li class="{{ Request::is('fixDetailManagements*') ? 'active' : '' }}">
    <a href="{{ route('fixDetailManagements.index') }}">
        <span class="title">@lang('models/fixDetailManagements.plural')</span>
    </a>
    <span class="icon-thumbnail"><i class="detailed fa fa-edit"></i></span>
</li> --}}
