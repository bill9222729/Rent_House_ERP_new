<!-- City Area Value Field -->
<div class="form-group col-12">
    {!! Form::label('city_area_value', __('models/locatedLots.fields.city_area_value').':') !!}
    <p>{{ $locatedLot->city_area_value }}</p>
</div>


<!-- Value Field -->
<div class="form-group col-12">
    {!! Form::label('value', __('models/locatedLots.fields.value').':') !!}
    <p>{{ $locatedLot->value }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/locatedLots.fields.name').':') !!}
    <p>{{ $locatedLot->name }}</p>
</div>


