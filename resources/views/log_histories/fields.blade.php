<div class="row">
<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', __('models/logHistories.fields.url').':') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Request Body Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('request_body', __('models/logHistories.fields.request_body').':') !!}
    {!! Form::textarea('request_body', null, ['class' => 'form-control']) !!}
</div>

<!-- Request Body Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('method', __('models/logHistories.fields.method').':') !!}
    {!! Form::textarea('method', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', __('models/logHistories.fields.user_id').':') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
