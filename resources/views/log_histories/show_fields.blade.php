<!-- Url Field -->
<div class="form-group col-12">
    {!! Form::label('url', __('models/logHistories.fields.url').':') !!}
    <p>{{ $logHistory->url }}</p>
</div>


<!-- Request Body Field -->
<div class="form-group col-12">
    {!! Form::label('request_body', __('models/logHistories.fields.request_body').':') !!}
    <p>{{ $logHistory->request_body }}</p>
</div>


<!-- User Id Field -->
<div class="form-group col-12">
    {!! Form::label('user_id', __('models/logHistories.fields.user_id').':') !!}
    <p>{{ $logHistory->user_id }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-12">
    {!! Form::label('created_at', __('models/logHistories.fields.created_at').':') !!}
    <p>{{ $logHistory->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-12">
    {!! Form::label('updated_at', __('models/logHistories.fields.updated_at').':') !!}
    <p>{{ $logHistory->updated_at }}</p>
</div>


