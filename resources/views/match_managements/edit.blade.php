@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">
            @lang('models/matchManagements.singular')
        </h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        編輯媒合設定維護
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">
                        <div class="container">
                            <div class="row g-3 align-items-center mb-4">
                                <div class="col-1">
                                    <label htmlFor="caseNo" class="col-form-label">媒合編號</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="caseNo" class="form-control" aria-describedby="passwordHelpInline" value={{ $matchManagement['match_id'] }} readOnly />
                                </div>
                            </div>
                            @if ($matchManagement['contract_type'] == '代管約')
                                <div class="row g-3 align-items-center mb-4">
                                    <div class="col-1">
                                        <label for="building_info" class="col-form-label">出租物件</label>
                                    </div>
                                    <div class="col-8">
                                        <a id="building_info">讀取中...</a>
                                    </div>
                                </div>

                                <div class="row g-3 align-items-center mb-4">
                                    <div class="col-1">
                                        <label for="tenant_info" class="col-form-label">房客</label>
                                    </div>
                                    <div class="col-8">
                                        <a id="tenant_info">讀取中...</a>
                                    </div>
                                </div>
                            @endif

                            @if ($matchManagement['contract_type'] == '包租約')
                                <div class="row g-3 align-items-center mb-4">
                                    <div class="col-1">
                                        <label for="building_info" class="col-form-label">出租物件</label>
                                    </div>
                                    <div class="col-8">
                                        <a id="building_info">讀取中...</a>
                                    </div>
                                </div>

                                <div class="row g-3 align-items-center mb-4">
                                    <div class="col-1">
                                        <label for="sublease_info" class="col-form-label">轉租約</label>
                                    </div>
                                    <div class="col-8">
                                        <a id="sublease_info">讀取中...</a>
                                    </div>
                                </div>
                            @endif

                            @if ($matchManagement['contract_type'] == '轉租約')
                                <div class="row g-3 align-items-center mb-4">
                                    <div class="col-1">
                                        <label for="charter_info" class="col-form-label">包租約</label>
                                    </div>
                                    <div class="col-8">
                                        <a id="charter_info">讀取中...</a>
                                    </div>
                                </div>

                                <div class="row g-3 align-items-center mb-4">
                                    <div class="col-1">
                                        <label for="tenant_info" class="col-form-label">房客</label>
                                    </div>
                                    <div class="col-8">
                                        <a id="tenant_info">讀取中...</a>
                                    </div>
                                </div>
                            @endif

                            <div class="row g-3 align-items-center mb-4">
                                <div class="col-1">
                                    <label htmlFor="leaseStartDt" class="col-form-label">租賃期間</label>
                                </div>
                                <div class="col-4">
                                    <input type="text" id="leaseStartDt" class="form-control" aria-describedby="passwordHelpInline" />
                                </div>
                                ~
                                <div class="col-4">
                                    <input type="text" id="leaseEndDt" class="form-control" aria-describedby="passwordHelpInline" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                        合約列表
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                    <div class="accordion-body">
                        <div class="container">
                            <table id="example2" class="display nowrap" style="width:100%"></table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('match_management')
    @parent
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        const matchManagementData = @json($matchManagement);
        console.log("頁面資料：matchManagementData");
        console.log(matchManagementData);
        async function getContractData(contract_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/contract_managements?match_id=${contract_id}`,
                success: function(data) {
                    retVal = data;
                }
            });
            return retVal;
        }
        // 取得物件資料
        async function getBuildingData(building_case_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/building_management_news?id=${building_case_id}`,
                success: function(data) {
                    retVal = data.data[0];
                }
            });
            return retVal;
        }

        // 取得租客資料
        async function getTenantData(tenant_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/tenant_management_news?id=${tenant_id}`,
                success: function(data) {
                    console.log("租客資料");
                    retVal = data["data"][0];
                    console.log(retVal);
                }
            });
            return retVal;
        }

        // 取得包租約資料
        async function getCharterData(match_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/match_managements?id=${match_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }

        // 取得轉租約資料
        async function getSubleaseData(match_id) {
            let retVal = "";
            await $.ajax({
                type: 'GET',
                url: `/api/match_managements?charter_case_id=${match_id}`,
                success: function(data) {
                    retVal = data["data"][0];
                }
            });
            return retVal;
        }

        // 刪除合約資料
        async function deleteContractData(contract_id) {
            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    $.ajax({
                        type: 'DELETE',
                        url: `/api/contract_managements/${contract_id}`,
                        success: function(data) {
                            $('#example2').DataTable().ajax.reload();
                            Swal.fire('資料已被刪除', '', 'success').then((result) => {
                                $('#example2').DataTable().ajax.reload();
                            })
                        }
                    });
                }
            })
        }
        $(document).ready(async function() {
            let buildingData = await getBuildingData(matchManagementData.building_case_id);
            let charterData = await getCharterData(matchManagementData.charter_case_id);
            let subleaseData = await getSubleaseData(matchManagementData.id);
            let tenantData = null;
            if (matchManagementData.contract_type === "包租約") {
                tenantData = subleaseData ? await getTenantData(subleaseData.tenant_case_id) : null;
            } else {
                tenantData = await getTenantData(matchManagementData.tenant_case_id);
            }
            console.log("物件資料：");
            console.log(buildingData);
            console.log("租客資料：");
            console.log(tenantData);
            console.log("包租約資料");
            console.log(charterData);
            console.log("轉租約資料");
            console.log(subleaseData);
            // 填入頁面上的資料
            if (matchManagementData) {
                $("#leaseStartDt").val(convertToRCDate(matchManagementData["lease_start_date"]));
                $("#leaseEndDt").val(convertToRCDate(matchManagementData["lease_end_date"]));
            }
            if (buildingData) {
                $("#building_info").html( // [案件編號：易居B1H00200126,洪讚明(N103525177),狀態：同意]
                    `[案件編號：${buildingData.government_no},${buildingData.name}(${buildingData.id_no})]`
                );
                $("#building_info").attr("href",
                    `/buildingManagementNews/${buildingData.id}/edit?cantedit=true`)
            } else {
                $("#building_info").html(
                    `查無物件資料，可能資料已被刪除`
                );
            }
            if (tenantData) {
                $("#tenant_info").html( // [案件編號：易居B1T00200758,關宇珊(F228245774),狀態：待審核]
                    `[案件編號：${tenantData.government_no},${tenantData.name}(${tenantData.id_no})]`
                );
                $("#tenant_info").attr("href", `/tenantManagementNews/${tenantData.id}/edit?cantedit=true`)
            } else {
                $("#tenant_info").html(
                    `查無租客資料，可能資料已被刪除`
                );
            }
            if (subleaseData) {
                $("#sublease_info").html( // [案件編號：易居B1M30200056,房客：葉純宏(E123689004),狀態：同意]
                    `[案件編號：${subleaseData.match_id},房客：${tenantData.name}(${tenantData.id_no})]`
                );
                $("#sublease_info").attr("href", `/matchManagements/${subleaseData.id}/edit`);
            } else {
                $("#sublease_info").html( // [案件編號：易居B1M30200056,房客：葉純宏(E123689004),狀態：同意]
                    `未建立轉租約`
                );
            }

            if (charterData) {
                $("#charter_info").html( // [案件編號：易居B1M20200038,狀態：同意]
                    `[案件編號：${charterData.match_id}]`
                );
                $("#charter_info").attr("href", `/matchManagements/${charterData.id}/edit`);
            }
            $('#example2').DataTable({
                "ajax": {
                    type: 'GET',
                    url: `/api/contract_managements?match_id=${matchManagementData.id}`,
                    dataSrc: (data) => {
                        return data.data.map((value, index, array) => {
                            console.log(value);
                            value["signing_date"] = convertToRCDate(value["signing_date"]);
                            value["created_at"] = convertToRCDate(value["created_at"]);
                            value["updated_at"] = convertToRCDate(value["updated_at"]);
                            let lease_start_date = convertToRCDate(
                                value["lease_start_end_date"].split("~")[0]);
                            let lease_end_date = convertToRCDate(
                                value["lease_start_end_date"].split("~")[1]);
                            value["lease_start_end_date"] =
                                `${lease_start_date}~${lease_end_date}`;
                            return value;
                        });
                    },
                },
                dom: 'Bfrtip',
                scrollX: true,
                @cannot("sales")
                buttons: [{
                    text: '新增合約',
                    action: function(e, dt, node, config) {
                        if (((!buildingData || !tenantData) && matchManagementData
                                .contract_type === "代管約") ||
                            (!buildingData && matchManagementData.contract_type === "包租約") ||
                            (!tenantData && matchManagementData.contract_type === "轉租約")) {
                            Swal.fire(
                                '無法新增合約',
                                '需求資料不完全，請洽工程師',
                                'error'
                            )
                        } else {
                            window.location.href =
                                `/contractManagements/create/${matchManagementData['id']}`
                        }
                    }
                }],
                @endcan
                "columns": [
                    // {
                    //     data: 'case_status',
                    //     title: "契約狀態"
                    // },
                    {
                        data: 'signing_date',
                        title: "簽約日期"
                    },
                    {
                        data: 'rent',
                        title: "租金"
                    },
                    {
                        data: 'lease_start_end_date',
                        title: "租賃期間"
                    },
                    // {
                    //     data: 'case_status',
                    //     title: "審核狀態"
                    // },
                    {
                        data: 'created_at',
                        title: "新增日期"
                    },
                    {
                        data: 'updated_at',
                        title: "最後修改日期"
                    },
                    // {
                    //     data: 'remark',
                    //     title: "系統備註"
                    // },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            let buttonHtmlContent = '';
                            @can('sales')
                                buttonHtmlContent += `<button type="button" class="btn btn-info btn-sm" onclick="location.href='/contractManagements/${data.id}/edit?cantedit=true'">檢視</button> `;
                            @elsecan("staff")
                                if (data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm" onclick="location.href='/contractManagements/${data.id}/edit'">編輯</button> `;
                                }
                                buttonHtmlContent += `<button type="button" class="btn btn-info btn-sm" onclick="location.href='/contractManagements/${data.id}/edit?cantedit=true'">檢視</button> `;
                                if (data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm" onclick="deleteContractData(${data.id});">刪除</button>`;
                                }
                            @else
                                buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm" onclick="location.href='/contractManagements/${data.id}/edit'">編輯</button> `;
                                buttonHtmlContent += `<button type="button" class="btn btn-info btn-sm" onclick="location.href='/contractManagements/${data.id}/edit?cantedit=true'">檢視</button> `;
                                buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm" onclick="deleteContractData(${data.id});">刪除</button>`;
                            @endcan
                            return buttonHtmlContent;
                        }
                    }
                ],
                // responsive: {
                //     details: {
                //         type: 'column'
                //     }
                // },
                // columnDefs: [{
                //     className: 'dtr-control',
                //     orderable: false,
                //     targets: 0
                // }],
                // order: [1, 'asc'],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });
        });
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
