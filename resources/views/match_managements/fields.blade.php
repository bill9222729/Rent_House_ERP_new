<div class="row">
<!-- Match Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('match_id', __('models/matchManagements.fields.match_id').':') !!}
    {!! Form::text('match_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Case Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_case_id', __('models/matchManagements.fields.building_case_id').':') !!}
    {!! Form::text('building_case_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Tenant Case Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tenant_case_id', __('models/matchManagements.fields.tenant_case_id').':') !!}
    {!! Form::text('tenant_case_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Lease Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lease_start_date', __('models/matchManagements.fields.lease_start_date').':') !!}
    {!! Form::date('lease_start_date', null, ['class' => 'form-control','id'=>'lease_start_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#lease_start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Lease End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lease_end_date', __('models/matchManagements.fields.lease_end_date').':') !!}
    {!! Form::date('lease_end_date', null, ['class' => 'form-control','id'=>'lease_end_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#lease_end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Charter Case Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('charter_case_id', __('models/matchManagements.fields.charter_case_id').':') !!}
    {!! Form::text('charter_case_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Contract Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contract_type', __('models/matchManagements.fields.contract_type').':') !!}
    {!! Form::text('contract_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
