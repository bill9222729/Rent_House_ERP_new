{{-- 代管約 --}}

<div class="row">
    {{-- 隱藏的欄位 --}}
    <input name="contract_type" type="hidden" value="代管約">

    <!-- Match Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('match_id', __('models/contractsNews.fields.match_id'), ['class' => 'col-md-2 control-label']) !!}
        {!! Form::text('match_id', null, ['class' => 'form-control col-md-6']) !!}
    </div>

    <!-- Building Case Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('building_case_id', __('models/contractsNews.fields.building_case'), ['class' => 'col-md-2 control-label']) !!}
        {!! Form::text('building_case_id', null, ['class' => 'form-control col-md-6']) !!}
        <button type="button" class="btn btn-primary col-md-2 mx-1">代入資料</button>
    </div>

    <!-- Tenant Case Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tenant_case_id', __('models/contractsNews.fields.tenant_case'), ['class' => 'col-md-2 control-label']) !!}
        {!! Form::text('tenant_case_id', null, ['class' => 'form-control col-md-6']) !!}
        <button type="button" class="btn btn-primary col-md-2 mx-1">代入資料</button>
    </div>

    <!-- Lease Start Date Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('lease_start_date', __('models/contractsNews.fields.lease_date') . ':', ['class' => 'col-md-2 control-label']) !!}
        {!! Form::date('lease_start_date', null, ['class' => 'form-control col-md-3', 'id' => 'lease_start_date']) !!}
        {!! Form::date('lease_end_date', null, ['class' => 'form-control col-md-3', 'id' => 'lease_end_date']) !!}
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#lease_start_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })
            $('#lease_start_date').datepicker();
        </script>
    @endpush

    @push('scripts')
        <script type="text/javascript">
            // $('#lease_end_date').datetimepicker({
            //     format: 'YYYY-MM-DD HH:mm:ss',
            //     useCurrent: false
            // })

            $('#lease_end_date').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                // showOnFocus: false,
                todayHighlight: true,
                useCurrent: false
            }).on("show", function(e) {
                $("div.box").css({
                    minHeight: "480px"
                });
            }).on("hide", function(e) {
                $("div.box").css({
                    minHeight: "auto"
                });
            });
        </script>
    @endpush

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
        <a href="javascript:void(0)" onclick="$('#shadow-form-escrow').hide()"
            class="btn btn-default">@lang('crud.cancel')</a>
    </div>
</div>
