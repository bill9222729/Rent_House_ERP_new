@extends('layouts.app')
@section('css')
    <style>
        #shadow-form,
        .shadow-form {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
            height: 100%;
            width: 100%;
            display: grid;
            place-items: center;
            background: rgba(0, 0, 0, .4)
        }

        #shadow-form form,
        .shadow-form form {
            max-width: 800px;
            width: 85vw;
            margin: 4rem 0;
            padding: 1.5rem;
            overflow: auto;
            background: #f6f9fa;
            border-radius: 16px;
        }

        .control-label {
            text-align: right;
            margin-bottom: 0;
            padding-top: 7px;
        }

        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }
    </style>
@endsection

@section('content')
    <!-- 新增-代管約 Modal -->
    <div class="modal fade" id="ModalAddEscrow" tabindex="-1"></div>
    <!-- 新增-包租約 Modal -->
    <div class="modal fade" id="ModalAddCharter" tabindex="-1"></div>
    <!-- 新增-轉租約 Modal -->
    <div class="modal fade" id="ModalAddSublease" tabindex="-1"></div>
    <!-- 終止合約 Modal -->
    <div class="modal fade" id="ModalStopContract" tabindex="-1"></div>

    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">媒合管理</h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item search-accordion">
                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        查詢條件
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">
                        <div class="container-fluid">
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="match_id" class="col-sm-2 col-form-label">媒合編號</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="match_id">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="contract_type">契約類型</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="contract_type">
                                                <option selected>請選擇</option>
                                                <option value="1">代管約</option>
                                                <option value="2">包租約</option>
                                                <option value="3">轉租約</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="building_id" class="col-sm-2 col-form-label">物件編號</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="building_id">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label for="party_A" class="col-sm-2 col-form-label">房東姓名</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="party_A">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="tenant_id" class="col-sm-2 col-form-label">房客編號</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="tenant_id">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label for="party_B" class="col-sm-2 col-form-label">租客姓名</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="party_B">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">簽約日期</label>
                                        <div class="row col-sm-10">
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="qsigningDt1" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="qsigningDt1" aria-label="民國年" aria-describedby="button-addon1">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                            ~
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="qsigningDt2" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="qsigningDt2" aria-label="民國年" aria-describedby="button-addon2">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">租賃期間</label>
                                        <div class="row col-sm-10">
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="qsigningDt1" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="qsigningDt1" aria-label="民國年" aria-describedby="button-addon1">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                            ~
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="qsigningDt2" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="qsigningDt2" aria-label="民國年" aria-describedby="button-addon2">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">租金</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="rent1">
                                                <span class="input-group-text">~</span>
                                                <input type="text" class="form-control" id="rent2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                                <button class="btn btn-primary" type="button" id="search">查詢</button>
                                <button class="btn btn-outline-secondary" type="reset" id="reset" onclick="resetCheckBox();">
                                    清除條件
                                </button>
                                @canany(["admin", "staff"])
                                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#ModalAddEscrow">
                                    新增 - 代管約
                                </button>
                                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#ModalAddCharter">
                                    新增 - 包租約(業者-房東)
                                </button>
                                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#ModalAddSublease">
                                    新增 - 轉租約(業者-房客)
                                </button>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                        查詢結果
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                    <div class="accordion-body">
                        <div class="container-fluid">
                            <table id="match_management_table" class="display nowrap" style="width:100%"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        function ajaxCU(fill) {
            const form = $('#shadow-form form')
            const method = $('#shadow-form [name="_method"]')
            if (fill) {
                Object.keys(fill).forEach(key => {
                    const input = document.querySelector(`#shadow-form [name="${key}"]`)
                    if (!input) return
                    if (input.getAttribute('type') === 'checkbox') input.checked = fill[key] ? true : false
                    else input.value = fill[key]
                })
                form.attr('action', '{{ route('matchManagements.store') }}/' + fill['id'])
                method.val('PATCH')
            } else {
                form.attr('action', '{{ route('matchManagements.store') }}')
                method.val('POST')
            }
            $('#shadow-form').show()
        }
        ajaxCU.close = () => {
            $('#shadow-form').hide()
        }

        function openShadowForm(formId, fill = false) {
            const form = $(`#${formId} form`)
            const method = $(`#${formId} [name="_method"]`)

            if (fill) {
                Object.keys(fill).forEach(key => {
                    const input = document.querySelector(`#${formId} [name="${key}"]`)
                    if (!input) return
                    if (input.getAttribute('type') === 'checkbox') input.checked = fill[key] ? true : false
                    else input.value = fill[key]
                })
                form.attr('action', '{{ route('matchManagements.store') }}/' + fill['id'])
                method.val('PATCH')
            } else {
                form.attr('action', '{{ route('matchManagements.store') }}')
                method.val('POST')
            }
            $(`#${formId}`).show()
        }

        async function deleteMatchData(building_id) {

            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    $.ajax({
                        type: 'DELETE',
                        url: `/api/match_managements/${building_id}`,
                        success: function(data) {
                            console.log(data);
                            $('#match_management_table').DataTable().ajax.reload();
                            Swal.fire('資料已被刪除', '', 'success').then((result) => {
                                $('#match_management_table').DataTable().ajax.reload();
                            })
                        }
                    });
                }
            })
        }


        // 初始化日期選擇器
        $(".input-group.date").datepicker({
            format: "twy-mm-dd",
            language: "zh-TW",
            showOnFocus: false,
            todayHighlight: true,
            todayBtn: "linked",
            clearBtn: true,
        });

        $('#match_management_table').DataTable({
            "pageLength": 20,
            "searching": false,
            // "responsive": true,
            scrollX: true,
            "order": [
                [9, 'desc']
            ],
            "ajax": {
                type: 'GET',
                url: `/api/match_managements`,
                dataSrc: function(data) {
                    console.log(data);
                    data = data.data.map((value, index, arry) => {
                        let lease_start_date = convertToRCDate(value["lease_start_date"]);
                        let lease_end_date = convertToRCDate(value["lease_end_date"]);
                        if (value['contract_type'] === "包租約") {
                            value['part_b'] = "易居管理顧問股份有限公司";
                            value['tenant_government_no'] = "-";
                        } else if (value['contract_type'] === "轉租約") {
                            value['part_a'] = "易居管理顧問股份有限公司";
                            value['building_government_no'] = "-";
                        }
                        value["lease_start_date"] = lease_start_date;
                        value["lease_date"] = `${lease_start_date} ~ ${lease_end_date}`;
                        return value;
                    });
                    // 如果登入的使用者是業務就只會看到他的資料
                    @can('sales')
                        data = data.filter((item) => {
                            return item.sales_id === {{ Auth::user()->id }}
                        });
                    @endcan
                    return data;
                }
            },
            dom: 'Bfrtip',
            createdRow: function(row, data, dataIndex) {
                console.log(data.contract_stop_date);
                if (data.contract_stop_date) {
                    // $(row).addClass('bg-opacity-25 bg-danger');
                    $(row).addClass('text-danger');
                }
            },
            "columns": [{
                    data: 'lease_start_date',
                    title: "簽約日期",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'match_id',
                    title: "媒合編號",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'contract_type',
                    title: "契約類型",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'part_a',
                    title: "甲方",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'building_government_no',
                    title: "內政部物件編號",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'part_b',
                    title: "乙方",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'tenant_government_no',
                    title: "內政部案件編號",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'lease_date',
                    title: "租賃期間",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'rent',
                    title: "租金",
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: 'created_at',
                    visible: false,
                    className: 'dt-body-center dt-head-center',
                },
                {
                    data: null,
                    title: "操作功能",
                    render: function(data, type, row) {
                        let buttonHtmlContent = '';
                        @can('sales')
                            buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm" onclick="location.href='matchManagements/${data.id}/edit'">編輯</button> `;
                        @elsecan("staff")
                            buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm" onclick="location.href='matchManagements/${data.id}/edit'">編輯</button> `;
                            if (data.data_status !== "admin") {
                                buttonHtmlContent += `<button type="button" class="btn btn-secondary btn-sm" data-bs-toggle="modal" data-bs-target="#ModalStopContract" data-bs-match-id="${data.match_id}" data-bs-id="${data.id}">終止</button> `;
                                buttonHtmlContent += `<button type="button" class="btn btn-primary btn-sm" onclick="location.href='contractManagements/create/${data.id}?renew=true'">續約</button> `;
                                buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm" onclick="deleteMatchData(${data.id});">刪除</button>`;
                            }
                        @else
                            buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm" onclick="location.href='matchManagements/${data.id}/edit'">編輯</button> `;
                            buttonHtmlContent += `<button type="button" class="btn btn-secondary btn-sm" data-bs-toggle="modal" data-bs-target="#ModalStopContract" data-bs-match-id="${data.match_id}" data-bs-id="${data.id}">終止</button> `;
                            buttonHtmlContent += `<button type="button" class="btn btn-primary btn-sm" onclick="location.href='contractManagements/create/${data.id}?renew=true'">續約</button> `;
                            buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm" onclick="deleteMatchData(${data.id});">刪除</button>`;
                        @endcan
                        return buttonHtmlContent;
                    },
                    className: 'dt-body-center dt-head-center',
                }
            ],
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
            },
        });
    </script>
@endpush
