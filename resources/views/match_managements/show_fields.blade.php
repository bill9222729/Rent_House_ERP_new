<!-- Match Id Field -->
<div class="form-group col-12">
    {!! Form::label('match_id', __('models/matchManagements.fields.match_id').':') !!}
    <p>{{ $matchManagement->match_id }}</p>
</div>


<!-- Building Case Id Field -->
<div class="form-group col-12">
    {!! Form::label('building_case_id', __('models/matchManagements.fields.building_case_id').':') !!}
    <p>{{ $matchManagement->building_case_id }}</p>
</div>


<!-- Tenant Case Id Field -->
<div class="form-group col-12">
    {!! Form::label('tenant_case_id', __('models/matchManagements.fields.tenant_case_id').':') !!}
    <p>{{ $matchManagement->tenant_case_id }}</p>
</div>


<!-- Lease Start Date Field -->
<div class="form-group col-12">
    {!! Form::label('lease_start_date', __('models/matchManagements.fields.lease_start_date').':') !!}
    <p>{{ $matchManagement->lease_start_date }}</p>
</div>


<!-- Lease End Date Field -->
<div class="form-group col-12">
    {!! Form::label('lease_end_date', __('models/matchManagements.fields.lease_end_date').':') !!}
    <p>{{ $matchManagement->lease_end_date }}</p>
</div>


<!-- Charter Case Id Field -->
<div class="form-group col-12">
    {!! Form::label('charter_case_id', __('models/matchManagements.fields.charter_case_id').':') !!}
    <p>{{ $matchManagement->charter_case_id }}</p>
</div>


<!-- Contract Type Field -->
<div class="form-group col-12">
    {!! Form::label('contract_type', __('models/matchManagements.fields.contract_type').':') !!}
    <p>{{ $matchManagement->contract_type }}</p>
</div>


