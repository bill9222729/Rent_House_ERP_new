@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">帳務功能</h3>
    </div>
    <div class="container-fluid container-fixed-lg" style="margin-top: 40px;">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <button type="button" class="btn btn" onclick="location.href='mixed/download'">匯出資料</button>
                @include('contracts.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $.fn.dataTable.ext.errMode = 'none';

LaravelDataTables.dataTableBuilder.on('error.dt', function ( e, settings, techNote, message ) {
console.log( 'DataTables Report_Error：', message );
} ) .DataTable( );
    </script>
@endpush
