<div class="row">
<!-- T Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('t_id', __('models/returnFormats.fields.t_id').':') !!}
    {!! Form::text('t_id', null, ['class' => 'form-control']) !!}
</div>

<!-- T Code Field -->
<div class="form-group col-sm-3">
    {!! Form::label('t_code', __('models/returnFormats.fields.t_code').':') !!}
    {!! Form::text('t_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Field -->
<div class="form-group col-sm-3">
    {!! Form::label('tax', __('models/returnFormats.fields.tax').':') !!}
    {!! Form::text('tax', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Account Field -->
<div class="form-group col-sm-3">
    {!! Form::label('bank_account', __('models/returnFormats.fields.bank_account').':') !!}
    {!! Form::text('bank_account', null, ['class' => 'form-control','maxlength'=>'14','minlength'=>'12']) !!}
</div>

<!-- User Name Field -->
<div class="form-group col-sm-3">
    {!! Form::label('user_name', __('models/returnFormats.fields.user_name').':') !!}
    {!! Form::text('user_name', null, ['class' => 'form-control']) !!}
</div>

<!-- T Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('t_price', __('models/returnFormats.fields.t_price').':') !!}
    {!! Form::number('t_price', null, ['class' => 'form-control']) !!}
</div>

<!-- T Status Field -->
<div class="form-group col-sm-3">
    {!! Form::label('t_status', __('models/returnFormats.fields.t_status').':') !!}
    {!! Form::text('t_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-3">
    {!! Form::label('name', __('models/returnFormats.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Item Address Field -->
<div class="form-group col-sm-3">
    {!! Form::label('item_address', __('models/returnFormats.fields.item_address').':') !!}
    {!! Form::text('item_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Item Month Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('item_month_price', __('models/returnFormats.fields.item_month_price').':') !!}
    {!! Form::number('item_month_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Item Price Support Field -->
<div class="form-group col-sm-3">
    {!! Form::label('item_price_support', __('models/returnFormats.fields.item_price_support').':') !!}
    {!! Form::number('item_price_support', null, ['class' => 'form-control']) !!}
</div>

<!-- Watch Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('watch_price', __('models/returnFormats.fields.watch_price').':') !!}
    {!! Form::number('watch_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Parking Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('parking_price', __('models/returnFormats.fields.parking_price').':') !!}
    {!! Form::number('parking_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Fix Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fix_price', __('models/returnFormats.fields.fix_price').':') !!}
    {!! Form::number('fix_price', null, ['class' => 'form-control']) !!}
</div>

<!-- T Cost Field -->
<div class="form-group col-sm-3">
    {!! Form::label('t_cost', __('models/returnFormats.fields.t_cost').':') !!}
    {!! Form::number('t_cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Other Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('other_price', __('models/returnFormats.fields.other_price').':') !!}
    {!! Form::number('other_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('total_price', __('models/returnFormats.fields.total_price').':') !!}
    {!! Form::number('total_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', __('models/returnFormats.fields.note').':') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('returnFormats.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
