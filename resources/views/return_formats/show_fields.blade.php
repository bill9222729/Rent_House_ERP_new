<!-- T Id Field -->
<div class="form-group col-12">
    {!! Form::label('t_id', __('models/returnFormats.fields.t_id').':') !!}
    <p>{{ $returnFormat->t_id }}</p>
</div>


<!-- T Code Field -->
<div class="form-group col-12">
    {!! Form::label('t_code', __('models/returnFormats.fields.t_code').':') !!}
    <p>{{ $returnFormat->t_code }}</p>
</div>


<!-- Tax Field -->
<div class="form-group col-12">
    {!! Form::label('tax', __('models/returnFormats.fields.tax').':') !!}
    <p>{{ $returnFormat->tax }}</p>
</div>


<!-- Bank Account Field -->
<div class="form-group col-12">
    {!! Form::label('bank_account', __('models/returnFormats.fields.bank_account').':') !!}
    <p>{{ $returnFormat->bank_account }}</p>
</div>


<!-- User Name Field -->
<div class="form-group col-12">
    {!! Form::label('user_name', __('models/returnFormats.fields.user_name').':') !!}
    <p>{{ $returnFormat->user_name }}</p>
</div>


<!-- T Price Field -->
<div class="form-group col-12">
    {!! Form::label('t_price', __('models/returnFormats.fields.t_price').':') !!}
    <p>{{ $returnFormat->t_price }}</p>
</div>


<!-- T Status Field -->
<div class="form-group col-12">
    {!! Form::label('t_status', __('models/returnFormats.fields.t_status').':') !!}
    <p>{{ $returnFormat->t_status }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/returnFormats.fields.name').':') !!}
    <p>{{ $returnFormat->name }}</p>
</div>


<!-- Item Address Field -->
<div class="form-group col-12">
    {!! Form::label('item_address', __('models/returnFormats.fields.item_address').':') !!}
    <p>{{ $returnFormat->item_address }}</p>
</div>


<!-- Item Month Price Field -->
<div class="form-group col-12">
    {!! Form::label('item_month_price', __('models/returnFormats.fields.item_month_price').':') !!}
    <p>{{ $returnFormat->item_month_price }}</p>
</div>


<!-- Item Price Support Field -->
<div class="form-group col-12">
    {!! Form::label('item_price_support', __('models/returnFormats.fields.item_price_support').':') !!}
    <p>{{ $returnFormat->item_price_support }}</p>
</div>


<!-- Watch Price Field -->
<div class="form-group col-12">
    {!! Form::label('watch_price', __('models/returnFormats.fields.watch_price').':') !!}
    <p>{{ $returnFormat->watch_price }}</p>
</div>


<!-- Parking Price Field -->
<div class="form-group col-12">
    {!! Form::label('parking_price', __('models/returnFormats.fields.parking_price').':') !!}
    <p>{{ $returnFormat->parking_price }}</p>
</div>


<!-- Fix Price Field -->
<div class="form-group col-12">
    {!! Form::label('fix_price', __('models/returnFormats.fields.fix_price').':') !!}
    <p>{{ $returnFormat->fix_price }}</p>
</div>


<!-- T Cost Field -->
<div class="form-group col-12">
    {!! Form::label('t_cost', __('models/returnFormats.fields.t_cost').':') !!}
    <p>{{ $returnFormat->t_cost }}</p>
</div>


<!-- Other Price Field -->
<div class="form-group col-12">
    {!! Form::label('other_price', __('models/returnFormats.fields.other_price').':') !!}
    <p>{{ $returnFormat->other_price }}</p>
</div>


<!-- Total Price Field -->
<div class="form-group col-12">
    {!! Form::label('total_price', __('models/returnFormats.fields.total_price').':') !!}
    <p>{{ $returnFormat->total_price }}</p>
</div>


<!-- Note Field -->
<div class="form-group col-12">
    {!! Form::label('note', __('models/returnFormats.fields.note').':') !!}
    <p>{{ $returnFormat->note }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-12">
    {!! Form::label('created_at', __('models/returnFormats.fields.created_at').':') !!}
    <p>{{ $returnFormat->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-12">
    {!! Form::label('updated_at', __('models/returnFormats.fields.updated_at').':') !!}
    <p>{{ $returnFormat->updated_at }}</p>
</div>


