<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/salesmanManagements.fields.name').':') !!}
    <p>{{ $salesmanManagement->name }}</p>
</div>


<!-- Id Num Field -->
<div class="form-group col-12">
    {!! Form::label('id_num', __('models/salesmanManagements.fields.id_num').':') !!}
    <p>{{ $salesmanManagement->id_num }}</p>
</div>


<!-- Address Field -->
<div class="form-group col-12">
    {!! Form::label('address', __('models/salesmanManagements.fields.address').':') !!}
    <p>{{ $salesmanManagement->address }}</p>
</div>


<!-- Phone Field -->
<div class="form-group col-12">
    {!! Form::label('phone', __('models/salesmanManagements.fields.phone').':') !!}
    <p>{{ $salesmanManagement->phone }}</p>
</div>


<!-- Birthday Field -->
<div class="form-group col-12">
    {!! Form::label('birthday', __('models/salesmanManagements.fields.birthday').':') !!}
    <p>{{ $salesmanManagement->birthday }}</p>
</div>


<!-- Bank Acc Field -->
<div class="form-group col-12">
    {!! Form::label('bank_acc', __('models/salesmanManagements.fields.bank_acc').':') !!}
    <p>{{ $salesmanManagement->bank_acc }}</p>
</div>


