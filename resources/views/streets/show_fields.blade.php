<!-- City Area Value Field -->
<div class="form-group col-12">
    {!! Form::label('city_area_value', __('models/streets.fields.city_area_value').':') !!}
    <p>{{ $street->city_area_value }}</p>
</div>


<!-- Value Field -->
<div class="form-group col-12">
    {!! Form::label('value', __('models/streets.fields.value').':') !!}
    <p>{{ $street->value }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/streets.fields.name').':') !!}
    <p>{{ $street->name }}</p>
</div>


