<div class="row">
<!-- Tenant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tenant_id', __('models/tenantBuildingInfos.fields.tenant_id').':') !!}
    {!! Form::number('tenant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/tenantBuildingInfos.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Located City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('located_city', __('models/tenantBuildingInfos.fields.located_city').':') !!}
    {!! Form::text('located_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Located Twn Spcode Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('located_Twn_spcode_area', __('models/tenantBuildingInfos.fields.located_Twn_spcode_area').':') !!}
    {!! Form::text('located_Twn_spcode_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Located Lot Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('located_lot_area', __('models/tenantBuildingInfos.fields.located_lot_area').':') !!}
    {!! Form::text('located_lot_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Located Landno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('located_landno', __('models/tenantBuildingInfos.fields.located_landno').':') !!}
    {!! Form::text('located_landno', null, ['class' => 'form-control']) !!}
</div>

<!-- Building No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_no', __('models/tenantBuildingInfos.fields.building_no').':') !!}
    {!! Form::text('building_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Total Square Meter Percent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_total_square_meter_percent', __('models/tenantBuildingInfos.fields.building_total_square_meter_percent').':') !!}
    {!! Form::text('building_total_square_meter_percent', null, ['class' => 'form-control']) !!}
</div>

<!-- Building Total Square Meter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_total_square_meter', __('models/tenantBuildingInfos.fields.building_total_square_meter').':') !!}
    {!! Form::text('building_total_square_meter', null, ['class' => 'form-control']) !!}
</div>

<!-- Addr Cntcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addr_cntcode', __('models/tenantBuildingInfos.fields.addr_cntcode').':') !!}
    {!! Form::text('addr_cntcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Addr Twnspcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addr_twnspcode', __('models/tenantBuildingInfos.fields.addr_twnspcode').':') !!}
    {!! Form::text('addr_twnspcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Addr Street Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addr_street_area', __('models/tenantBuildingInfos.fields.addr_street_area').':') !!}
    {!! Form::text('addr_street_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Addr Lane Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addr_lane', __('models/tenantBuildingInfos.fields.addr_lane').':') !!}
    {!! Form::text('addr_lane', null, ['class' => 'form-control']) !!}
</div>

<!-- Addr Alley Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addr_alley', __('models/tenantBuildingInfos.fields.addr_alley').':') !!}
    {!! Form::text('addr_alley', null, ['class' => 'form-control']) !!}
</div>

<!-- Addr No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addr_no', __('models/tenantBuildingInfos.fields.addr_no').':') !!}
    {!! Form::text('addr_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Household Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_household', __('models/tenantBuildingInfos.fields.is_household').':') !!}
    {!! Form::number('is_household', null, ['class' => 'form-control']) !!}
</div>

<!-- Filers Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filers', __('models/tenantBuildingInfos.fields.filers').':') !!}
    {!! Form::number('filers', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Modified Person Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_modified_person', __('models/tenantBuildingInfos.fields.last_modified_person').':') !!}
    {!! Form::number('last_modified_person', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
