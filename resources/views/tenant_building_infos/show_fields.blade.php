<!-- Tenant Id Field -->
<div class="form-group col-12">
    {!! Form::label('tenant_id', __('models/tenantBuildingInfos.fields.tenant_id').':') !!}
    <p>{{ $tenantBuildingInfo->tenant_id }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/tenantBuildingInfos.fields.name').':') !!}
    <p>{{ $tenantBuildingInfo->name }}</p>
</div>


<!-- Located City Field -->
<div class="form-group col-12">
    {!! Form::label('located_city', __('models/tenantBuildingInfos.fields.located_city').':') !!}
    <p>{{ $tenantBuildingInfo->located_city }}</p>
</div>


<!-- Located Twn Spcode Area Field -->
<div class="form-group col-12">
    {!! Form::label('located_Twn_spcode_area', __('models/tenantBuildingInfos.fields.located_Twn_spcode_area').':') !!}
    <p>{{ $tenantBuildingInfo->located_Twn_spcode_area }}</p>
</div>


<!-- Located Lot Area Field -->
<div class="form-group col-12">
    {!! Form::label('located_lot_area', __('models/tenantBuildingInfos.fields.located_lot_area').':') !!}
    <p>{{ $tenantBuildingInfo->located_lot_area }}</p>
</div>


<!-- Located Landno Field -->
<div class="form-group col-12">
    {!! Form::label('located_landno', __('models/tenantBuildingInfos.fields.located_landno').':') !!}
    <p>{{ $tenantBuildingInfo->located_landno }}</p>
</div>


<!-- Building No Field -->
<div class="form-group col-12">
    {!! Form::label('building_no', __('models/tenantBuildingInfos.fields.building_no').':') !!}
    <p>{{ $tenantBuildingInfo->building_no }}</p>
</div>


<!-- Building Total Square Meter Percent Field -->
<div class="form-group col-12">
    {!! Form::label('building_total_square_meter_percent', __('models/tenantBuildingInfos.fields.building_total_square_meter_percent').':') !!}
    <p>{{ $tenantBuildingInfo->building_total_square_meter_percent }}</p>
</div>


<!-- Building Total Square Meter Field -->
<div class="form-group col-12">
    {!! Form::label('building_total_square_meter', __('models/tenantBuildingInfos.fields.building_total_square_meter').':') !!}
    <p>{{ $tenantBuildingInfo->building_total_square_meter }}</p>
</div>


<!-- Addr Cntcode Field -->
<div class="form-group col-12">
    {!! Form::label('addr_cntcode', __('models/tenantBuildingInfos.fields.addr_cntcode').':') !!}
    <p>{{ $tenantBuildingInfo->addr_cntcode }}</p>
</div>


<!-- Addr Twnspcode Field -->
<div class="form-group col-12">
    {!! Form::label('addr_twnspcode', __('models/tenantBuildingInfos.fields.addr_twnspcode').':') !!}
    <p>{{ $tenantBuildingInfo->addr_twnspcode }}</p>
</div>


<!-- Addr Street Area Field -->
<div class="form-group col-12">
    {!! Form::label('addr_street_area', __('models/tenantBuildingInfos.fields.addr_street_area').':') !!}
    <p>{{ $tenantBuildingInfo->addr_street_area }}</p>
</div>


<!-- Addr Lane Field -->
<div class="form-group col-12">
    {!! Form::label('addr_lane', __('models/tenantBuildingInfos.fields.addr_lane').':') !!}
    <p>{{ $tenantBuildingInfo->addr_lane }}</p>
</div>


<!-- Addr Alley Field -->
<div class="form-group col-12">
    {!! Form::label('addr_alley', __('models/tenantBuildingInfos.fields.addr_alley').':') !!}
    <p>{{ $tenantBuildingInfo->addr_alley }}</p>
</div>


<!-- Addr No Field -->
<div class="form-group col-12">
    {!! Form::label('addr_no', __('models/tenantBuildingInfos.fields.addr_no').':') !!}
    <p>{{ $tenantBuildingInfo->addr_no }}</p>
</div>


<!-- Is Household Field -->
<div class="form-group col-12">
    {!! Form::label('is_household', __('models/tenantBuildingInfos.fields.is_household').':') !!}
    <p>{{ $tenantBuildingInfo->is_household }}</p>
</div>


<!-- Filers Field -->
<div class="form-group col-12">
    {!! Form::label('filers', __('models/tenantBuildingInfos.fields.filers').':') !!}
    <p>{{ $tenantBuildingInfo->filers }}</p>
</div>


<!-- Last Modified Person Field -->
<div class="form-group col-12">
    {!! Form::label('last_modified_person', __('models/tenantBuildingInfos.fields.last_modified_person').':') !!}
    <p>{{ $tenantBuildingInfo->last_modified_person }}</p>
</div>


