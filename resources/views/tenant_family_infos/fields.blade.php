<div class="row">
<!-- Tenant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tenant_id', __('models/tenantFamilyInfos.fields.tenant_id').':') !!}
    {!! Form::number('tenant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/tenantFamilyInfos.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Foreigner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_foreigner', __('models/tenantFamilyInfos.fields.is_foreigner').':') !!}
    {!! Form::number('is_foreigner', null, ['class' => 'form-control']) !!}
</div>

<!-- Id No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_no', __('models/tenantFamilyInfos.fields.id_no').':') !!}
    {!! Form::text('id_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Appellation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('appellation', __('models/tenantFamilyInfos.fields.appellation').':') !!}
    {!! Form::text('appellation', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', __('models/tenantFamilyInfos.fields.gender').':') !!}
    {!! Form::text('gender', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Low Income Households Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_low_income_households', __('models/tenantFamilyInfos.fields.condition_low_income_households').':') !!}
    {!! Form::number('condition_low_income_households', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Low And Middle Income Households Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_low_and_middle_income_households', __('models/tenantFamilyInfos.fields.condition_low_and_middle_income_households').':') !!}
    {!! Form::number('condition_low_and_middle_income_households', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Special Circumstances Families Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_special_circumstances_families', __('models/tenantFamilyInfos.fields.condition_special_circumstances_families').':') !!}
    {!! Form::number('condition_special_circumstances_families', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Over Sixty Five Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_over_sixty_five', __('models/tenantFamilyInfos.fields.condition_over_sixty_five').':') !!}
    {!! Form::number('condition_over_sixty_five', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Domestic Violence Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_domestic_violence', __('models/tenantFamilyInfos.fields.condition_domestic_violence').':') !!}
    {!! Form::number('condition_domestic_violence', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Disabled Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_disabled', __('models/tenantFamilyInfos.fields.condition_disabled').':') !!}
    {!! Form::number('condition_disabled', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Disabled Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_disabled_type', __('models/tenantFamilyInfos.fields.condition_disabled_type').':') !!}
    {!! Form::number('condition_disabled_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Disabled Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_disabled_level', __('models/tenantFamilyInfos.fields.condition_disabled_level').':') !!}
    {!! Form::text('condition_disabled_level', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Aids Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_aids', __('models/tenantFamilyInfos.fields.condition_aids').':') !!}
    {!! Form::number('condition_aids', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Aboriginal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_aboriginal', __('models/tenantFamilyInfos.fields.condition_aboriginal').':') !!}
    {!! Form::number('condition_aboriginal', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Disaster Victims Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_disaster_victims', __('models/tenantFamilyInfos.fields.condition_disaster_victims').':') !!}
    {!! Form::number('condition_disaster_victims', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Vagabond Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_vagabond', __('models/tenantFamilyInfos.fields.condition_vagabond').':') !!}
    {!! Form::number('condition_vagabond', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Applicant Naturalization Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_Applicant_naturalization', __('models/tenantFamilyInfos.fields.condition_Applicant_naturalization').':') !!}
    {!! Form::number('condition_Applicant_naturalization', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Applicant Not Naturalization Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_Applicant_not_naturalization', __('models/tenantFamilyInfos.fields.condition_Applicant_not_naturalization').':') !!}
    {!! Form::number('condition_Applicant_not_naturalization', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Applicant Not Naturalization And Study Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_Applicant_not_naturalization_and_study', __('models/tenantFamilyInfos.fields.condition_Applicant_not_naturalization_and_study').':') !!}
    {!! Form::number('condition_Applicant_not_naturalization_and_study', null, ['class' => 'form-control']) !!}
</div>

<!-- Condition Police Officer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_police_officer', __('models/tenantFamilyInfos.fields.condition_police_officer').':') !!}
    {!! Form::number('condition_police_officer', null, ['class' => 'form-control']) !!}
</div>

<!-- Filers Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filers', __('models/tenantFamilyInfos.fields.filers').':') !!}
    {!! Form::number('filers', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Modified Person Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_modified_person', __('models/tenantFamilyInfos.fields.last_modified_person').':') !!}
    {!! Form::number('last_modified_person', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
