<!-- Tenant Id Field -->
<div class="form-group col-12">
    {!! Form::label('tenant_id', __('models/tenantFamilyInfos.fields.tenant_id').':') !!}
    <p>{{ $tenantFamilyInfo->tenant_id }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/tenantFamilyInfos.fields.name').':') !!}
    <p>{{ $tenantFamilyInfo->name }}</p>
</div>


<!-- Is Foreigner Field -->
<div class="form-group col-12">
    {!! Form::label('is_foreigner', __('models/tenantFamilyInfos.fields.is_foreigner').':') !!}
    <p>{{ $tenantFamilyInfo->is_foreigner }}</p>
</div>


<!-- Id No Field -->
<div class="form-group col-12">
    {!! Form::label('id_no', __('models/tenantFamilyInfos.fields.id_no').':') !!}
    <p>{{ $tenantFamilyInfo->id_no }}</p>
</div>


<!-- Appellation Field -->
<div class="form-group col-12">
    {!! Form::label('appellation', __('models/tenantFamilyInfos.fields.appellation').':') !!}
    <p>{{ $tenantFamilyInfo->appellation }}</p>
</div>


<!-- Gender Field -->
<div class="form-group col-12">
    {!! Form::label('gender', __('models/tenantFamilyInfos.fields.gender').':') !!}
    <p>{{ $tenantFamilyInfo->gender }}</p>
</div>


<!-- Condition Low Income Households Field -->
<div class="form-group col-12">
    {!! Form::label('condition_low_income_households', __('models/tenantFamilyInfos.fields.condition_low_income_households').':') !!}
    <p>{{ $tenantFamilyInfo->condition_low_income_households }}</p>
</div>


<!-- Condition Low And Middle Income Households Field -->
<div class="form-group col-12">
    {!! Form::label('condition_low_and_middle_income_households', __('models/tenantFamilyInfos.fields.condition_low_and_middle_income_households').':') !!}
    <p>{{ $tenantFamilyInfo->condition_low_and_middle_income_households }}</p>
</div>


<!-- Condition Special Circumstances Families Field -->
<div class="form-group col-12">
    {!! Form::label('condition_special_circumstances_families', __('models/tenantFamilyInfos.fields.condition_special_circumstances_families').':') !!}
    <p>{{ $tenantFamilyInfo->condition_special_circumstances_families }}</p>
</div>


<!-- Condition Over Sixty Five Field -->
<div class="form-group col-12">
    {!! Form::label('condition_over_sixty_five', __('models/tenantFamilyInfos.fields.condition_over_sixty_five').':') !!}
    <p>{{ $tenantFamilyInfo->condition_over_sixty_five }}</p>
</div>


<!-- Condition Domestic Violence Field -->
<div class="form-group col-12">
    {!! Form::label('condition_domestic_violence', __('models/tenantFamilyInfos.fields.condition_domestic_violence').':') !!}
    <p>{{ $tenantFamilyInfo->condition_domestic_violence }}</p>
</div>


<!-- Condition Disabled Field -->
<div class="form-group col-12">
    {!! Form::label('condition_disabled', __('models/tenantFamilyInfos.fields.condition_disabled').':') !!}
    <p>{{ $tenantFamilyInfo->condition_disabled }}</p>
</div>


<!-- Condition Disabled Type Field -->
<div class="form-group col-12">
    {!! Form::label('condition_disabled_type', __('models/tenantFamilyInfos.fields.condition_disabled_type').':') !!}
    <p>{{ $tenantFamilyInfo->condition_disabled_type }}</p>
</div>


<!-- Condition Disabled Level Field -->
<div class="form-group col-12">
    {!! Form::label('condition_disabled_level', __('models/tenantFamilyInfos.fields.condition_disabled_level').':') !!}
    <p>{{ $tenantFamilyInfo->condition_disabled_level }}</p>
</div>


<!-- Condition Aids Field -->
<div class="form-group col-12">
    {!! Form::label('condition_aids', __('models/tenantFamilyInfos.fields.condition_aids').':') !!}
    <p>{{ $tenantFamilyInfo->condition_aids }}</p>
</div>


<!-- Condition Aboriginal Field -->
<div class="form-group col-12">
    {!! Form::label('condition_aboriginal', __('models/tenantFamilyInfos.fields.condition_aboriginal').':') !!}
    <p>{{ $tenantFamilyInfo->condition_aboriginal }}</p>
</div>


<!-- Condition Disaster Victims Field -->
<div class="form-group col-12">
    {!! Form::label('condition_disaster_victims', __('models/tenantFamilyInfos.fields.condition_disaster_victims').':') !!}
    <p>{{ $tenantFamilyInfo->condition_disaster_victims }}</p>
</div>


<!-- Condition Vagabond Field -->
<div class="form-group col-12">
    {!! Form::label('condition_vagabond', __('models/tenantFamilyInfos.fields.condition_vagabond').':') !!}
    <p>{{ $tenantFamilyInfo->condition_vagabond }}</p>
</div>


<!-- Condition Applicant Naturalization Field -->
<div class="form-group col-12">
    {!! Form::label('condition_Applicant_naturalization', __('models/tenantFamilyInfos.fields.condition_Applicant_naturalization').':') !!}
    <p>{{ $tenantFamilyInfo->condition_Applicant_naturalization }}</p>
</div>


<!-- Condition Applicant Not Naturalization Field -->
<div class="form-group col-12">
    {!! Form::label('condition_Applicant_not_naturalization', __('models/tenantFamilyInfos.fields.condition_Applicant_not_naturalization').':') !!}
    <p>{{ $tenantFamilyInfo->condition_Applicant_not_naturalization }}</p>
</div>


<!-- Condition Applicant Not Naturalization And Study Field -->
<div class="form-group col-12">
    {!! Form::label('condition_Applicant_not_naturalization_and_study', __('models/tenantFamilyInfos.fields.condition_Applicant_not_naturalization_and_study').':') !!}
    <p>{{ $tenantFamilyInfo->condition_Applicant_not_naturalization_and_study }}</p>
</div>


<!-- Condition Police Officer Field -->
<div class="form-group col-12">
    {!! Form::label('condition_police_officer', __('models/tenantFamilyInfos.fields.condition_police_officer').':') !!}
    <p>{{ $tenantFamilyInfo->condition_police_officer }}</p>
</div>


<!-- Filers Field -->
<div class="form-group col-12">
    {!! Form::label('filers', __('models/tenantFamilyInfos.fields.filers').':') !!}
    <p>{{ $tenantFamilyInfo->filers }}</p>
</div>


<!-- Last Modified Person Field -->
<div class="form-group col-12">
    {!! Form::label('last_modified_person', __('models/tenantFamilyInfos.fields.last_modified_person').':') !!}
    <p>{{ $tenantFamilyInfo->last_modified_person }}</p>
</div>


