@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }

        .input-group label.error {
            position: absolute;
            bottom: -1rem;
        }
    </style>
@endsection
@section('content')
    <form id="tenant_create_form">
        <div class="container-fluid container-fixed-lg">
            <h3 class="page-title">租客管理</h3>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                @include('tenant_management_news.fields')
            </div>
        </div>
        <div class="container-fluid py-5">
            <div class="row justify-content-center">
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary" id="tenant-management-create-button">儲存</button>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-secondary" id="tenant-management-cancel-button">取消</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/tenant_management.js') }}"></script>
    <script>
        $(document).ready(async function() {
            initBuildingManagementsPage();
            // 新增租客資料的函式
            const saveTenantData = () => {
                let requestData = getAllfieldsValue();
                requestData["creator"] = "{{Auth::user()->id}}";
                // 先檢查資料庫內有無重複的內政部編號
                $.ajax({
                    method: 'GET',
                    url: `/api/tenant_management_news?government_no=${requestData.government_no || "不可能會重複的邊號"}`,
                    dataType: 'json',
                    success: function(data) {
                        if (data.data.length === 0) {
                            // 存進資料庫
                            $.ajax({
                                method: "POST",
                                url: "/api/tenant_management_news",
                                data: requestData,
                                dataType: "json",
                                success: function(data) {
                                    console.log(data);
                                    Swal.fire(
                                        '成功',
                                        '資料已成功新增',
                                        'success'
                                    ).then((result) => {
                                        if (result.isConfirmed) {
                                            window.location.href = "/tenantManagementNews";
                                        }
                                    })
                                },
                                error: function(err) {
                                    console.error(err);
                                }
                            })
                        } else {
                            Swal.fire(
                                '儲存失敗',
                                '內政部編號與既有資料重複',
                                'error'
                            )
                        }
                    }
                });
            }

            // 與申請人或配偶未同戶籍之未成年子女(限20歲以下) talbe 初始化
            const tenant_minor_offspring_table = $('#tenant_minor_offspring').DataTable({
                "responsive": true,
                // "searching": false,
                "dom": 'Bfrtip',
                "buttons": [{
                    text: '新增',
                    action: function(e, dt, node, config) {
                        window.location.href =
                            `/tenantManagementNews/create`
                    }
                }],
                "ajax": {
                    type: 'GET',
                    url: `/api/tenant_minor_offsprings`,
                    dataSrc: function(data) {
                        console.log(data.data);
                        return data.data.map((value, index, array) => {
                            value["birthday"] = convertToRCDate(value["birthday"]);
                            return value;
                        });
                    }
                },
                "columns": [{
                        data: 'name',
                        title: "未成年子女姓名"
                    },
                    {
                        data: 'appellation',
                        title: "稱謂"
                    },
                    {
                        data: 'id_no',
                        title: "身分/居留證號"
                    },
                    {
                        data: 'birthday',
                        title: "出生年月日"
                    },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            return `<button type="button" class="btn btn-primary btn-sm" onclick="location.href='buildingManagementNews/${data.id}/edit'">編輯</button >`
                        }
                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });
            // 上傳檔案部分的初始化
            // 取得上傳的檔案
            let buildingManagementNew = null
            let fileData = await getFileArr("building_management", buildingManagementNew ?
                buildingManagementNew['id'] : "");
            $("#tenant_file_input").fileinput({
                language: "zh-TW",
                // uploadUrl: "/api/building_management_news/upload",
                uploadUrl: "/api/file_managments",
                maxFileCount: 0,
                maxTotalFileCount: 0,
                maxFileSize: 5000,
                msgSizeTooLarge: `檔案 "{name}" (<b>{size} KB</b>) 大小超過上限 <b>5 MB</b>`,
                uploadExtraData: {
                    "file_source_id": buildingManagementNew ? buildingManagementNew['id'] : "",
                },
                overwriteInitial: false,
                initialPreview: fileData["initialPreview"],
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: 'pdf', // image is the default and can be overridden in config below
                initialPreviewConfig: fileData["initialPreviewConfig"],
                ajaxDeleteSettings: {
                    type: 'DELETE' // This should override the ajax as $.ajax({ type: 'DELETE' })
                }
            });

            // 表單驗證
            addFormValid("tenant_create_form", saveTenantData);
        });
    </script>
@endpush
