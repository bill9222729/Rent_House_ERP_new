@extends('layouts.app')

@section('content')
    <form id="tenant_edit_form">
        <div class="container-fluid container-fixed-lg">
            <h3 class="page-title">@lang('models/tenantManagementNews.singular')</h3>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                @include('tenant_management_news.fields')
            </div>
        </div>

        <div class="container-fluid py-5">
            <div class="row justify-content-center">
                <div class="col-auto">
                    @if (app('request')->input('copy'))
                        <button type="submit" class="btn btn-success">複製</button>
                    @else
                        <button type="submit" class="btn btn-primary">儲存</button>
                    @endif
                </div>
                @can('sales')
                    <div class="col-auto">
                        <button type="button" class="btn btn-success text-nowrap" onclick="changeDataStatusToStaff()">送出資料</button>
                    </div>
                @endcan
                @canany(['staff', 'admin'])
                    @if ($tenantManagementNew->data_status == 'staff')
                        <div class="col-auto">
                            <button type="button" class="btn btn-success text-nowrap" onclick="changeDataStatusToSales()">退回資料</button>
                        </div>
                    @endif
                @endcan
                <div class="col-auto">
                    <a href="/tenantManagementNews" class="btn btn-secondary">取消</a>
                </div>
            </div>
        </div>

        <!-- 承租人及其家庭成員基本資料 modal -->
        <div class="modal fade" id="ModalTenantFamilyInfo" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
        </div>
        <!-- 與申請人或配偶未同戶籍之未成年子女(限20歲以下) modal -->
        <div class="modal fade" id="ModalTenantMinorOffsprings" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
        </div>
    </form>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/tenant_management.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const tenantManagementNew = @json($tenantManagementNew);
        console.log(tenantManagementNew);
        // 刪除按鈕的事件
        const DeleteTenantFamilyInfo = (id) => {
            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    const settings = {
                        "url": `/api/tenant_family_infos/${id}`,
                        "method": "DELETE",
                        "timeout": 0,
                    };

                    $.ajax(settings).done(function(response) {
                        Swal.fire('資料已被刪除', '', 'success').then((result) => {
                            $('#tenant_family_info').DataTable().ajax.reload();
                        })
                    });
                }
            })
        }
        const DeleteTenantMinorOffsprings = (id) => {
            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    const settings = {
                        "url": `/api/tenant_minor_offsprings/${id}`,
                        "method": "DELETE",
                        "timeout": 0,
                    };

                    $.ajax(settings).done(function(response) {
                        Swal.fire('資料已被刪除', '', 'success').then((result) => {
                            $('#tenant_minor_offspring').DataTable().ajax.reload();
                        })
                    });
                }
            })
        }
        // 儲存按鈕的事件
        const handleSaveButtonClick = () => {
            let data = getAllfieldsValue();
            console.log(data);
            // 存進資料庫
            $.ajax({
                method: "PATCH",
                url: `/api/tenant_management_news/${tenantManagementNew.id}`,
                data: data,
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    Swal.fire(
                        '成功',
                        '資料已更新',
                        'success'
                    ).then((e) => {
                        window.location.href = "/tenantManagementNews";
                    })
                },
                error: function(err) {
                    console.error(err);
                }
            });
        }
        // 複製按鈕的事件
        const handleCopyButtonClick = () => {
            let requestData = getAllfieldsValue();
            console.log(requestData);
            // 先檢查一下資料庫裏面有沒有相同案件編號的資料，有的話就不給存
            $.ajax({
                method: 'GET',
                url: `/api/tenant_management_news?government_no=${requestData.government_no}`,
                dataType: 'json',
                success: function(data) {
                    if (data.data.length === 0) {
                        // 存進資料庫
                        $.ajax({
                            method: "POST",
                            url: `/api/tenant_management_news/`,
                            data: requestData,
                            dataType: "json",
                            success: function(data) {
                                console.log(data);
                                Swal.fire(
                                    '成功',
                                    '資料已成功複製',
                                    'success'
                                ).then((e) => {
                                    window.location.href = "/tenantManagementNews";
                                })
                            },
                            error: function(err) {
                                console.error(err);
                            }
                        });
                    } else {
                        Swal.fire(
                            '儲存失敗',
                            '內政部編號與既有資料重複',
                            'error'
                        )
                    }
                }
            });
            return;
        }
        $(document).ready(async function() {
            // 初始化 各種東西
            initBuildingManagementsPage(tenantManagementNew);
            // 初始化 資料庫資料到畫面上
            fillDataToEditPage(tenantManagementNew);
            // 初始化 承租人及其家庭成員基本資料 的table
            const tenant_family_info_table = $('#tenant_family_info').DataTable({
                "responsive": true,
                "searching": false,
                "dom": 'Bfrtip',
                "buttons": [{
                    text: '新增',
                    className: '',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#ModalTenantFamilyInfo',
                        'data-bs-tenant-id': `${tenantManagementNew.id}`,
                    }
                }],
                "ajax": {
                    type: 'GET',
                    url: `/api/tenant_family_infos?tenant_id=${tenantManagementNew.id}`,
                    dataSrc: function(data) {
                        const ConditionObj = {
                            "condition_low_income_households": "低收入戶",
                            "condition_low_and_middle_income_households": "中低收入戶",
                            "condition_special_circumstances_families": "特殊境遇家庭",
                            "condition_over_sixty_five": "65歲以上",
                            "condition_domestic_violence": "於家庭暴力或性侵害之受害者及其子女",
                            "condition_disabled": "身心障礙者",
                            "condition_aids": "感染或罹患AIDS者",
                            "condition_aboriginal": "原住民",
                            "condition_disaster_victims": "災民",
                            "condition_vagabond": "遊民",
                            "condition_Applicant_naturalization": "申請人設籍：與申請人設籍之家庭成員",
                            "condition_Applicant_not_naturalization": "申請人未設籍：實際居住之家庭成員",
                            "condition_Applicant_not_naturalization_and_study": "未設籍當地且在該地區就學就業者",
                            "condition_police_officer": "警消人員",
                            "condition_disabled_level": {
                                "1": "極重度",
                                "2": "重度",
                                "3": "中度",
                                "4": "輕度",
                            },
                            "condition_disabled_type": {
                                "1": "(一類) 神經系統構造及精神、心智功能",
                                "2": "(二類) 眼、耳及相關構造與感官功能及疼痛",
                                "3": "(三類) 涉及聲音與言語構造及其功能",
                                "4": "(四類) 循環、造血、免疫與呼吸系統構造及其功能",
                                "5": "(五類)消化、新陳代謝與內分泌系統相關構造及其功能",
                                "6": "(六類) 泌尿與生殖系統相關構造及其功能",
                                "7": "(七類) 神經、肌肉、骨骼之移動相關構造及其功能",
                                "8": "(八類) 皮膚與相關構造及其功能",
                                "9": "(九類) 罕見疾病",
                                "10": "(十類) 其它類",
                                "11": "(十一類) 發展遲緩類",
                                "12": "(多類) 多重障礙類型",
                            }
                        }
                        console.log(data.data);
                        return data.data.map((value, index, array) => {
                            // 姓名欄位處理
                            let name_and_id = `
                            ${value['name'] || ""}<br>${value['id_no']}
                            `;
                            value['name_and_id'] = name_and_id;
                            // 資格欄位處理
                            let condition = "";
                            Object.keys(ConditionObj).map(function(objectKey, index) {
                                if ((objectKey === "condition_disabled_level" ||
                                        objectKey === "condition_disabled_type") &&
                                    !(value[objectKey] === "0" || value[
                                        objectKey] === 0)) {
                                    condition +=
                                        `身心障礙者：${ConditionObj[objectKey][value[objectKey]] + '<br>'}`;
                                    return;
                                }
                                condition +=
                                    `${value[objectKey]===1 ? ConditionObj[objectKey]+'<br>' : ""}`;
                            });
                            condition = condition.substring(0, condition.length - 4);
                            value['condition'] = condition;

                            // 回傳處理後的資料
                            return value;
                        });
                    }
                },
                "columns": [{
                        data: 'name_and_id',
                        title: "姓名<br>身分／居留證號"
                    },
                    {
                        data: 'appellation',
                        title: "稱謂"
                    },
                    {
                        data: 'gender',
                        title: "性別"
                    },
                    {
                        data: 'condition',
                        title: "弱勢資格"
                    },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            return `<button type="button" class="btn btn-primary btn-sm mx-1" data-bs-toggle="modal" data-bs-target="#ModalTenantFamilyInfo" data-bs-id="${data.id}" data-bs-update="true" data-bs-tenant-id="${tenantManagementNew.id}">編輯</button >` +
                                `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="DeleteTenantFamilyInfo(${data.id})">刪除</button >`
                        }
                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });
            // 初始化 與申請人或配偶未同戶籍之未成年子女(限20歲以下)
            const tenant_minor_offspring_table = $('#tenant_minor_offspring').DataTable({
                "responsive": true,
                "searching": false,
                "dom": 'Bfrtip',
                "buttons": [{
                    text: '新增',
                    className: '',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#ModalTenantMinorOffsprings',
                        'data-bs-tenant-id': `${tenantManagementNew.id}`,
                    }
                }],
                "ajax": {
                    type: 'GET',
                    url: `/api/tenant_minor_offsprings?tenant_id=${tenantManagementNew.id}`,
                    dataSrc: function(data) {
                        console.log(data.data);
                        return data.data.map((value, index, array) => {
                            // 預處理生日
                            value["birthday"] = convertToRCDate(value["birthday"]);
                            return value;
                        });
                    }
                },
                "columns": [{
                        data: 'name',
                        title: "未成年子女姓名"
                    },
                    {
                        data: 'appellation',
                        title: "稱謂"
                    },
                    {
                        data: 'id_no',
                        title: "身分/居留證號"
                    },
                    {
                        data: 'birthday',
                        title: "出生年月日"
                    },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            return `<button type="button" class="btn btn-primary btn-sm mx-1" data-bs-toggle="modal" data-bs-target="#ModalTenantMinorOffsprings" data-bs-id="${data.id}" data-bs-update="true">編輯</button >` +
                                `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="DeleteTenantMinorOffsprings(${data.id})">刪除</button >`
                        }
                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });
            // 初始化 上傳檔案部分
            // 取得上傳的檔案
            let fileData = await getFileArr("tenant_management", tenantManagementNew ?
                tenantManagementNew['id'] : "");
            $("#tenant_file_input").fileinput({
                language: "zh-TW",
                uploadUrl: "/api/file_managments",
                maxFileCount: 0,
                maxTotalFileCount: 0,
                maxFileSize: 20000,
                msgSizeTooLarge: `檔案 "{name}" (<b>{size} KB</b>) 大小超過上限 <b>5 MB</b>`,
                uploadExtraData: {
                    "file_source_id": tenantManagementNew ? tenantManagementNew['id'] : "",
                    "file_source": "tenant_management",
                },
                overwriteInitial: false,
                initialPreview: fileData["initialPreview"],
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: 'pdf', // image is the default and can be overridden in config below
                initialPreviewConfig: fileData["initialPreviewConfig"],
                ajaxDeleteSettings: {
                    type: 'DELETE' // This should override the ajax as $.ajax({ type: 'DELETE' })
                }
            });

            // 如果是檢視模式的話就把所有欄位鎖起來
            let targetUrl = new URL(location.href);
            let cantEdit = targetUrl.searchParams.get("cantedit") === "true";
            if (cantEdit) {
                $("input").attr("readonly", "readonly");
                $("input[type='checkbox']").attr("disabled", true);
                $("input[type='radio']").attr("disabled", true);
                $("select").selectpicker("destroy");
                $("select, button").attr("disabled", "disabled");
                $("textarea").attr("disabled", "disabled");
            }

            // 欄位驗證
            @if (app('request')->input('copy'))
                console.log("copy");
                addFormValid("tenant_edit_form", handleCopyButtonClick);
            @else
                console.log("update");
                addFormValid("tenant_edit_form", handleSaveButtonClick);
            @endif
        })
    </script>
@endpush
