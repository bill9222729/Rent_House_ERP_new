{{-- 編輯房客承租申請 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
            編輯房客承租申請
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap">申請日期</label>
                        <div class="col-md-12">
                            <div class="input-group date day">
                                <input type="text" name="apply_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="apply_date" id="apply_date" aria-label="民國年" aria-describedby="button-addon1">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*本人欲承租</label>
                        <div class="col-md-12 row ps-4">
                            <div class="form-check col-md-6">
                                <input class="form-check-input" type="radio" name="case_type" name="case_type_chartering" id="case_type_chartering" value="case_type_chartering" checked>
                                <label class="form-check-label" for="case_type_chartering">
                                    租屋服務事業轉租之住宅(包租)
                                </label>
                            </div>
                            <div class="form-check col-md-6">
                                <input class="form-check-input" type="radio" name="case_type" name="case_type_escrow" id="case_type_escrow" value="case_type_escrow">
                                <label class="form-check-label" for="case_type_escrow">
                                    經由租屋服務事業協助出租(代管)
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="case_no" class="col-md-12 col-form-label fw-bolder text-nowrap">案件編號</label>
                        {{-- 他是一個react元件 --}}
                        <div class="col-md-12 CaseNoInput" data-param="{{ isset($tenantManagementNew) ? $tenantManagementNew : '' }}"></div>
                    </div>
                    <div class="row col-md-6">
                        <label for="government_no" class="col-md-12 col-form-label fw-bolder text-nowrap">內政部編號</label>
                        <div class="col-md-12">
                            <input type="text" name="government_no" class="form-control" name="government_no" id="government_no" style="text-transform:uppercase">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="case_status" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*案件狀態</label>
                        <div class="col-md-12">
                            <select class="form-select form-control" name="case_status" id="case_status">
                                <option value="" selected>請選擇</option>
                                <option value="待媒合">待媒合</option>
                                <option value="已媒合">已媒合</option>
                                <option value="已收訂待媒合">已收訂待媒合</option>
                                <option value="退出">退出</option>
                                <option value="警示客戶">警示客戶</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6 SalesSelector" required="true" data-param="{{ isset($tenantManagementNew) ? $tenantManagementNew : '{}' }}"></div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 承租人基本資料 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
            承租人基本資料
        </button>
    </h2>
    <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap text-danger" for="name">*承租人姓名</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="gender" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*性別</label>
                        <div class="col-md-12 align-items-center">
                            <select class="form-select form-control" name="gender" id="gender">
                                <option value="">請選擇</option>
                                <option value="0">男</option>
                                <option value="1">女</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*出生年月日</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group date day">
                                <input type="text" name="birthday" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" name="birthday" id="birthday" aria-label="民國年" aria-describedby="button-addon2">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="id_no" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*國民身分證統一編號</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="id_no" id="id_no" style="text-transform:capitalize">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap text-danger" for="house_no">*戶口名簿戶號</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="house_no" id="house_no" style="text-transform:uppercase">
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap" for="tel_day">電話</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group">
                                <span class="input-group-text">(日)</span>
                                <input type="text" name="tel_day" id="tel_day" name="tel_day" class="form-control" placeholder="">
                                <span class="input-group-text">(夜)</span>
                                <input type="text" name="tel_night" id="tel_night" name="tel_night" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap text-danger" for="cellphone">*手機</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="cellphone" id="cellphone">
                        </div>
                    </div>
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap" for="email">電子信箱</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="email" id="email">
                        </div>
                    </div>
                </div>
                <div class="row mb-3 CitySelector" data-param='{"title":"*戶籍地址","required":true,"city_select_id":"residence_city","city_area_select_id":"residence_city_area","addr_input_id":"residence_addr", "page_data":{{ $tenantManagementNew ?? '{}' }}, "is_modal":{{ isset($isModal) ? 'true' : 'false' }}}'>
                </div>
                <div class="row mb-3 CitySelector" data-param='{"title":"*通訊地址","required":true,"city_select_id":"mailing_city","city_area_select_id":"mailing_city_area","addr_input_id":"mailing_addr", "page_data":{{ $tenantManagementNew ?? '{}' }}, "is_modal":{{ isset($isModal) ? 'true' : 'false' }}, "ditto" : "true"}'>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="membtype" class="col-md-12 col-form-label fw-bolder text-nowrap text-danger">*承租人及其家庭成員資格</label>
                        <div class="col-md-12 align-top">
                            <select class="form-select form-control" name="membtype" id="membtype">
                                <option value="" selected>請選擇</option>
                                <option value="一般戶">一般戶</option>
                                <option value="第一類弱勢戶">第一類弱勢戶</option>
                                <option value="第二類弱勢戶">第二類弱勢戶</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mb-3 WeakConditions" data-param="{{ isset($tenantManagementNew) ? $tenantManagementNew : '{}' }}">
                </div>
                <div class="row mb-3 IsHaveOtherSubsidy" data-param="{{ isset($tenantManagementNew) ? $tenantManagementNew : '{}' }}">
                </div>
                <div class="legalAgentNum" data-param="{{ isset($tenantManagementNew) ? $tenantManagementNew : '{}' }}">
                </div>
                <div class="row mb-3">
                    <div class="row col-md-12 align-items-center">
                        <label class="col-md-12 col-form-label fw-bolder" for="proxy_name_1">領款資訊</label>
                        <div class="row col-md-6 bankSelector" data-param="{{ isset($tenantManagementNew) ? $tenantManagementNew : '[]' }}"></div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6 align-items-center">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap" for="virtual_bank_account">虛擬帳號</label>
                        <div class="col-md-12 align-items-center">
                            <input type="text" class="form-control" name="virtual_bank_account" id="virtual_bank_account">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 承租人及其家庭成員基本資料 --}}
@if (isset($tenantManagementNew))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFour">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="true" aria-controls="panelsStayOpen-collapseFour">
                承租人及其家庭成員基本資料
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFour">
            <div class="accordion-body">
                <div class="container-fluid">
                    <table id="tenant_family_info" class="display nowrap" style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>
@endif

{{-- 與申請人或配偶未同戶籍之未成年子女(限20歲以下) --}}
@if (isset($tenantManagementNew))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                與申請人或配偶未同戶籍之未成年子女(限20歲以下)
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <table id="tenant_minor_offspring" class="display nowrap" style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>
@endif

{{-- 承租房屋基本需求 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingSix">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="true" aria-controls="panelsStayOpen-collapseSix">
            承租房屋基本需求
        </button>
    </h2>
    <div id="panelsStayOpen-collapseSix" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingSix">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12 TenantHopeLocationInputs" data-param='{{ isset($tenantManagementNew) ? $tenantManagementNew['tenant_hope_locations'] : '[]' }}'>
                    </div>
                </div>
                {{-- 格局 --}}
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap" for="name">格局</label>
                        <div class="col-md-12 align-items-center">
                            <label class="col-md-2 col-form-label" style="width:0px" name="fix-label">&nbsp;</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_pattern_01" id="building_pattern_01" value="套房">
                                <label class="form-check-label" for="building_pattern_01">套房</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_pattern_02" id="building_pattern_02" value="雅房">
                                <label class="form-check-label" for="building_pattern_02">雅房</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_pattern_03" id="building_pattern_03" value="1房">
                                <label class="form-check-label" for="building_pattern_03">1房</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_pattern_04" id="building_pattern_04" value="2房">
                                <label class="form-check-label" for="building_pattern_04">2房</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_pattern_05" id="building_pattern_05" value="3房以上">
                                <label class="form-check-label" for="building_pattern_05">3房以上</label>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- 房屋類型 --}}
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap " for="name">房屋類型</label>
                        <div class="col-md-12 align-items-center">
                            <label class="col-md-2 col-form-label" style="width:0px" name="fix-label">&nbsp;</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_type_01" id="building_type_01" value="公寓">
                                <label class="form-check-label" for="building_type_01">公寓</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_type_02" id="building_type_02" value="電梯大樓">
                                <label class="form-check-label" for="building_type_02">電梯大樓</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_type_03" id="building_type_03" value="透天厝">
                                <label class="form-check-label" for="building_type_03">透天厝</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_type_04" id="building_type_04" value="平房">
                                <label class="form-check-label" for="building_type_04">平房</label>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- 實際使用坪數 --}}
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap " for="name">實際使用坪數</label>
                        <div class="col-md-12 align-items-center">
                            <label class="col-md-12 col-form-label" style="width:0px" name="fix-label">&nbsp;</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_use_area_01" id="building_use_area_01" value="10坪以下">
                                <label class="form-check-label" for="building_use_area_01">10坪以下</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_use_area_02" id="building_use_area_02" value="10~20坪">
                                <label class="form-check-label" for="building_use_area_02">10~20坪</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_use_area_03" id="building_use_area_03" value="20~30坪">
                                <label class="form-check-label" for="building_use_area_03">20~30坪</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_use_area_04" id="building_use_area_04" value="30~40坪">
                                <label class="form-check-label" for="building_use_area_04">30~40坪</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_use_area_05" id="building_use_area_05" value="40坪以上">
                                <label class="form-check-label" for="building_use_area_05">40坪以上</label>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- 樓層 --}}
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap " for="name">樓層</label>
                        <div class="col-md-12 align-items-center">
                            <label class="col-md-2 col-form-label" style="width:0px" name="fix-label">&nbsp;</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="wish_floor_01" id="wish_floor_01" value="1樓">
                                <label class="form-check-label" for="wish_floor_01">1樓</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="wish_floor_02" id="wish_floor_02" value="2~6樓">
                                <label class="form-check-label" for="wish_floor_02">2~6樓</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="wish_floor_03" id="wish_floor_03" value="6~12樓">
                                <label class="form-check-label" for="wish_floor_03">6~12樓</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="wish_floor_04" id="wish_floor_04" value="12樓以上">
                                <label class="form-check-label" for="wish_floor_04">12樓以上</label>
                            </div>
                            <div class="d-inline-block">
                                <div class="input-group input-group-sm mb-3">
                                    <span class="input-group-text">第</span>
                                    <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" name="wish_floor_exact" id="wish_floor_exact">
                                    <span class="input-group-text">樓</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3 mt-3">
                    {{-- 隔間 --}}
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap" for="name">隔間</label>
                        <div class="col-md-12">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" name="wish_room_num" id="wish_room_num">
                                <span class="input-group-text">房</span>
                                <input type="text" class="form-control" name="wish_living_rooms_num" id="wish_living_rooms_num">
                                <span class="input-group-text">廳</span>
                                <input type="text" class="form-control" name="wish_bathrooms_num" id="wish_bathrooms_num">
                                <span class="input-group-text">衛</span>
                            </div>
                        </div>
                    </div>
                    {{-- 門禁需求 --}}
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap" for="req_access_control">門禁需求</label>
                        <div class="col-md-12">
                            <select class="form-select form-control" name="req_access_control" id="req_access_control">
                                <option value="" selected>請選擇</option>
                                <option value="1">有</option>
                                <option value="0">無</option>
                            </select>
                        </div>
                    </div>
                </div>
                {{-- 租金 --}}
                <div class="row mb-3 mt-3">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label fw-bolder text-nowrap" for="name">租金</label>
                        <div class="col-md-12 align-items-center">
                            <div class="input-group input-group-sm mb-3">
                                <input type="text" class="form-control" name="rent_min" id="rent_min">
                                <span class="input-group-text">~</span>
                                <input type="text" class="form-control" name="rent_max" id="rent_max">
                                <span class="input-group-text">元</span>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- 提供設備 --}}
                <div class="row mb-3">
                    <div class="row col-md-12 align-items-center">
                        <label for="test" class="col-md-12 col-form-label fw-bolder text-nowrap">提供設備</label>
                        <div class="col-md-12 align-items-center">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_tv" id="building_offer_tv" value="option2">
                                <label class="form-check-label" for="building_offer_tv">電視</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_refrigerator" id="building_offer_refrigerator" value="option2">
                                <label class="form-check-label" for="building_offer_refrigerator">冰箱</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_cable" id="building_offer_cable" value="option2">
                                <label class="form-check-label" for="building_offer_cable">有線電視(第四臺)</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_air_conditioner" id="building_offer_air_conditioner" value="option2">
                                <label class="form-check-label" for="building_offer_air_conditioner">冷氣</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_geyser" id="building_offer_geyser" value="option2">
                                <label class="form-check-label" for="building_offer_geyser">熱水器</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_internet" id="building_offer_internet" value="option2">
                                <label class="form-check-label" for="building_offer_internet">網際網路</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_washing_machine" id="building_offer_washing_machine" value="option2">
                                <label class="form-check-label" for="building_offer_washing_machine">洗衣機</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_natural_gas" id="building_offer_natural_gas" value="option2">
                                <label class="form-check-label" for="building_offer_natural_gas">瓦斯/天然瓦斯</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_bed" id="building_offer_bed" value="option2">
                                <label class="form-check-label" for="building_offer_bed">床</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_wardrobe" id="building_offer_wardrobe" value="option2">
                                <label class="form-check-label" for="building_offer_wardrobe">衣櫃</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_desk" id="building_offer_desk" value="option2">
                                <label class="form-check-label" for="building_offer_desk">桌</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_chair" id="building_offer_chair" value="option2">
                                <label class="form-check-label" for="building_offer_chair">椅子</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_sofa" id="building_offer_sofa" value="option2">
                                <label class="form-check-label" for="building_offer_sofa">沙發</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="building_offer_other" id="building_offer_other" value="option2">
                                <label class="form-check-label" for="building_offer_other">其他</label>
                                <input class="checkbox-input form-control" name="building_offer_other_desc" id="building_offer_other_desc" />
                            </div>
                        </div>
                    </div>
                </div>
                {{-- 變更紀錄 --}}
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-md-12 col-form-label fw-bolder" for="change_note">變更紀錄</label>
                        <div class="col-md-12 align-items-center">
                            <textarea type="text" class="form-control" name="change_note" id="change_note"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 特殊註記 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingSeven">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSeven" aria-expanded="true" aria-controls="panelsStayOpen-collapseSeven">
            特殊註記
        </button>
    </h2>
    <div id="panelsStayOpen-collapseSeven" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingSeven">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-3">
                    {{-- react元件：IsLiveTogether.js --}}
                    <div class="row col-md-12 align-items-start" id="IsLiveTogether" data-param="{{ isset($tenantManagementNew) ? $tenantManagementNew : '{}' }}"></div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 備註 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingEight">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseEight" aria-expanded="true" aria-controls="panelsStayOpen-collapseEight">
            備註
        </button>
    </h2>
    <div id="panelsStayOpen-collapseEight" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingEight">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-sm-2 col-form-label text-end fw-bolder text-nowrap" for="note">備註</label>
                        <div class="col-sm-10 align-items-center">
                            <textarea type="text" class="form-control" name="note" id="note"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- 檢附文件 --}}
@if (isset($tenantManagementNew) && !app('request')->input('copy'))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                檢附文件
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="tenant_file_input" name="source" type="file" multiple {{ app('request')->input('cantedit') ? "readonly='true'" : '' }} />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
