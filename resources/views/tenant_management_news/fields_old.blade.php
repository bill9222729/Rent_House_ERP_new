<div class="row">
<!-- Apply Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apply_date', __('models/tenantManagementNews.fields.apply_date').':') !!}
    {!! Form::date('apply_date', null, ['class' => 'form-control','id'=>'apply_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#apply_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Case No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_no', __('models/tenantManagementNews.fields.case_no').':') !!}
    {!! Form::text('case_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Type Chartering Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_type_chartering', __('models/tenantManagementNews.fields.case_type_chartering').':') !!}
    {!! Form::number('case_type_chartering', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Type Escrow Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_type_escrow', __('models/tenantManagementNews.fields.case_type_escrow').':') !!}
    {!! Form::number('case_type_escrow', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_status', __('models/tenantManagementNews.fields.case_status').':') !!}
    {!! Form::text('case_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/tenantManagementNews.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', __('models/tenantManagementNews.fields.gender').':') !!}
    {!! Form::text('gender', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', __('models/tenantManagementNews.fields.birthday').':') !!}
    {!! Form::date('birthday', null, ['class' => 'form-control','id'=>'birthday']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#birthday').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Id No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_no', __('models/tenantManagementNews.fields.id_no').':') !!}
    {!! Form::text('id_no', null, ['class' => 'form-control']) !!}
</div>

<!-- House No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('house_no', __('models/tenantManagementNews.fields.house_no').':') !!}
    {!! Form::text('house_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Tel Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tel_day', __('models/tenantManagementNews.fields.tel_day').':') !!}
    {!! Form::text('tel_day', null, ['class' => 'form-control']) !!}
</div>

<!-- Tel Night Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tel_night', __('models/tenantManagementNews.fields.tel_night').':') !!}
    {!! Form::text('tel_night', null, ['class' => 'form-control']) !!}
</div>

<!-- Cellphone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cellphone', __('models/tenantManagementNews.fields.cellphone').':') !!}
    {!! Form::text('cellphone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/tenantManagementNews.fields.email').':') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Membtype Field -->
<div class="form-group col-sm-6">
    {!! Form::label('membtype', __('models/tenantManagementNews.fields.membtype').':') !!}
    {!! Form::number('membtype', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('residence_city', __('models/tenantManagementNews.fields.residence_city').':') !!}
    {!! Form::text('residence_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence City Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('residence_city_area', __('models/tenantManagementNews.fields.residence_city_area').':') !!}
    {!! Form::text('residence_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('residence_addr', __('models/tenantManagementNews.fields.residence_addr').':') !!}
    {!! Form::text('residence_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mailing_city', __('models/tenantManagementNews.fields.mailing_city').':') !!}
    {!! Form::text('mailing_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing City Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mailing_city_area', __('models/tenantManagementNews.fields.mailing_city_area').':') !!}
    {!! Form::text('mailing_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mailing_addr', __('models/tenantManagementNews.fields.mailing_addr').':') !!}
    {!! Form::text('mailing_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Have Other Subsidy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_have_other_subsidy', __('models/tenantManagementNews.fields.is_have_other_subsidy').':') !!}
    {!! Form::number('is_have_other_subsidy', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy01 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy01', __('models/tenantManagementNews.fields.isOtherSubsidy01').':') !!}
    {!! Form::number('isOtherSubsidy01', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy01Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy01Info', __('models/tenantManagementNews.fields.isOtherSubsidy01Info').':') !!}
    {!! Form::number('isOtherSubsidy01Info', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy02 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy02', __('models/tenantManagementNews.fields.isOtherSubsidy02').':') !!}
    {!! Form::number('isOtherSubsidy02', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy02Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy02Info', __('models/tenantManagementNews.fields.isOtherSubsidy02Info').':') !!}
    {!! Form::number('isOtherSubsidy02Info', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy03 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy03', __('models/tenantManagementNews.fields.isOtherSubsidy03').':') !!}
    {!! Form::number('isOtherSubsidy03', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy03Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy03Info', __('models/tenantManagementNews.fields.isOtherSubsidy03Info').':') !!}
    {!! Form::number('isOtherSubsidy03Info', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy04 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy04', __('models/tenantManagementNews.fields.isOtherSubsidy04').':') !!}
    {!! Form::number('isOtherSubsidy04', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy04Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy04Info', __('models/tenantManagementNews.fields.isOtherSubsidy04Info').':') !!}
    {!! Form::number('isOtherSubsidy04Info', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy05 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy05', __('models/tenantManagementNews.fields.isOtherSubsidy05').':') !!}
    {!! Form::number('isOtherSubsidy05', null, ['class' => 'form-control']) !!}
</div>

<!-- Isothersubsidy05Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isOtherSubsidy05Info', __('models/tenantManagementNews.fields.isOtherSubsidy05Info').':') !!}
    {!! Form::number('isOtherSubsidy05Info', null, ['class' => 'form-control']) !!}
</div>

<!-- Legal Agent Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('legal_agent_num', __('models/tenantManagementNews.fields.legal_agent_num').':') !!}
    {!! Form::number('legal_agent_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Single Proxy Reason Field -->
<div class="form-group col-sm-6">
    {!! Form::label('single_proxy_reason', __('models/tenantManagementNews.fields.single_proxy_reason').':') !!}
    {!! Form::text('single_proxy_reason', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_legal_agent_name', __('models/tenantManagementNews.fields.first_legal_agent_name').':') !!}
    {!! Form::text('first_legal_agent_name', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Tel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_legal_agent_tel', __('models/tenantManagementNews.fields.first_legal_agent_tel').':') !!}
    {!! Form::text('first_legal_agent_tel', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_legal_agent_phone', __('models/tenantManagementNews.fields.first_legal_agent_phone').':') !!}
    {!! Form::text('first_legal_agent_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- First Legal Agent Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_legal_agent_addr', __('models/tenantManagementNews.fields.first_legal_agent_addr').':') !!}
    {!! Form::text('first_legal_agent_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('second_legal_agent_name', __('models/tenantManagementNews.fields.second_legal_agent_name').':') !!}
    {!! Form::text('second_legal_agent_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Tel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('second_legal_agent_tel', __('models/tenantManagementNews.fields.second_legal_agent_tel').':') !!}
    {!! Form::text('second_legal_agent_tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('second_legal_agent_phone', __('models/tenantManagementNews.fields.second_legal_agent_phone').':') !!}
    {!! Form::text('second_legal_agent_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Legal Agent Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('second_legal_agent_addr', __('models/tenantManagementNews.fields.second_legal_agent_addr').':') !!}
    {!! Form::text('second_legal_agent_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_name', __('models/tenantManagementNews.fields.bank_name').':') !!}
    {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Branch Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_branch_code', __('models/tenantManagementNews.fields.bank_branch_code').':') !!}
    {!! Form::text('bank_branch_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Owner Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_owner_name', __('models/tenantManagementNews.fields.bank_owner_name').':') !!}
    {!! Form::text('bank_owner_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Account Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_account', __('models/tenantManagementNews.fields.bank_account').':') !!}
    {!! Form::text('bank_account', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
