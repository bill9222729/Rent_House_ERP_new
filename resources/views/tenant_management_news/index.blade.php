@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">租客管理</h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item search-accordion">
                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        查詢條件
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">
                        <div class="container-fluid">
                            <div class="row mb-3 mt-5">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label">申請日期</label>
                                        <div class="row col-sm-10">
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="apply_date_start" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="apply_date_start" aria-label="民國年" aria-describedby="button-addon1">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                            ~
                                            <div class="col">
                                                <div class="input-group date">
                                                    <input type="text" name="apply_date_end" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="apply_date_end" aria-label="民國年" aria-describedby="button-addon2">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                        <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="case_state">案件狀態</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="case_state">
                                                <option value="0" selected>請選擇</option>
                                                <option value="待媒合">待媒合</option>
                                                <option value="已媒合">已媒合</option>
                                                <option value="已收訂待媒合">已收訂待媒合</option>
                                                <option value="退出">退出</option>
                                                <option value="警示客戶">警示客戶</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="sales">業務</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="sales">
                                                <option value="0" selected>請選擇</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="case_type">包租/代管</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="case_type">
                                                <option value="0" selected>請選擇</option>
                                                <option value="包租">包租</option>
                                                <option value="代管">代管</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="case_no" class="col-sm-2 col-form-label">案件編號</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="case_no" style="text-transform:uppercase">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label for="government_no" class="col-sm-2 col-form-label">內政部編號</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="government_no" style="text-transform:uppercase">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="name" class="col-sm-2 col-form-label">承租人姓名</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="membtype">資格</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="membtype">
                                                <option value="0" selected>請選擇</option>
                                                <option value="1">一般戶</option>
                                                <option value="2">第一類弱勢戶</option>
                                                <option value="3">第二類弱勢戶</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label class="col-sm-2 col-form-label" for="weak_item">弱勢項目</label>
                                        <div class="col-sm-10">
                                            <select class="form-select form-control" id="weak_item">
                                                <option value="0" selected>請選擇</option>
                                                <option value="低收入戶">低收入戶</option>
                                                <option value="中低收入戶">中低收入戶</option>
                                                <option value="特殊境遇家庭">特殊境遇家庭</option>
                                                <option value="65歲以上">65歲以上</option>
                                                <option value="於家庭暴力或性侵害之受害者及其子女">於家庭暴力或性侵害之受害者及其子女</option>
                                                <option value="身心障礙">身心障礙</option>
                                                <option value="感染或罹患AIDS">感染或罹患AIDS</option>
                                                <option value="原住民">原住民</option>
                                                <option value="災民">災民</option>
                                                <option value="遊民">遊民</option>
                                                <option value="育有未成年子女三人以上">育有未成年子女三人以上</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="col-sm-9 col-md-10">
                                <button class="btn btn-primary" type="button" id="search">
                                    查詢
                                </button>
                                <button class="btn btn-outline-secondary" type="reset" id="reset" onclick="resetCheckBox();">
                                    清除條件
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                        查詢結果
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                    <div class="accordion-body">
                        <div class="container-fluid">
                            <table id="tenant_management_table" class="display nowrap" style="width:100%"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        $.fn.dataTable.ext.search.push(
            // 申請日期
            function(settings, data, dataIndex) {
                let apply_date_start = $("#apply_date_start").val();
                let apply_date_end = $("#apply_date_end").val();
                let apply_date = data[0];
                if (checkInDate(apply_date_start, apply_date_end, apply_date, apply_date)) {
                    return true;
                }
                return false;
            },
            // 案件狀態
            function(settings, data, dataIndex) {
                let case_state = $("#case_state").val();
                let case_state_data = data[1];
                if (case_state === case_state_data || case_state == "0") {
                    return true;
                }
                return false;
            },
            // 業務
            function(settings, data, dataIndex) {
                let sales = $("#sales").val();
                let sales_data = data[2];
                if (sales == sales_data || sales === "0") {
                    return true;
                }
                return false;
            },
            // 包租/代管
            function(settings, data, dataIndex) {
                let case_type = $("#case_type").val();
                let case_type_data = data[3];
                if (case_type_data.includes(case_type) || case_type === "0") {
                    return true;
                }
                return false;
            },
            // 租客編號
            function(settings, data, dataIndex) {
                let case_no = $("#case_no").val();
                let case_no_data = data[4];
                if (case_no_data.includes(case_no) || case_no === "") {
                    return true;
                }
                return false;
            },
            // 內政部編號
            function(settings, data, dataIndex) {
                let government_no = $("#government_no").val();
                let government_no_data = data[5];
                if (government_no_data.includes(government_no) || government_no === "") {
                    return true;
                }
                return false;
            },
            // 租客姓名
            function(settings, data, dataIndex) {
                let name = $("#name").val();
                let name_data = data[6];
                if (name_data.includes(name) || name === "") {
                    return true;
                }
                return false;
            },
        );

        function deleteTenantData(tenant_id) {
            Swal.fire({
                title: '確定要刪除這筆資料?',
                icon: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: '取消',
                denyButtonText: `刪除`,
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire('Saved!', '', 'success')
                } else if (result.isDenied) {
                    $.ajax({
                        type: 'GET',
                        url: `/api/match_managements?tenant_case_id=${tenant_id}`,
                        success: function(data) {
                            if (data.data.length > 0) {
                                Swal.fire({
                                    title: '無法刪除',
                                    text: `媒合資料${data.data[0].match_id}已經使用此物件，故無法刪除`,
                                    icon: 'warning',
                                    showCancelButton: true,
                                    showConfirmButton: true,
                                    cancelButtonText: '取消',
                                    confirmButtonText: '確定',
                                })
                            } else {
                                $.ajax({
                                    type: 'DELETE',
                                    url: `/api/tenant_management_news/${tenant_id}`,
                                    success: function(data) {
                                        Swal.fire('資料已被刪除', '', 'success').then((
                                            result) => {
                                            $('#tenant_management_table')
                                                .DataTable().ajax.reload();
                                        })
                                    }
                                });
                            }
                        }
                    })
                }
            })
        }
        $(document).ready(async function() {
            let salesmanData = await getSalesmanData();
            let usersData = await getUsersData();
            // 設定搜尋部份的業務選項
            salesmanData.forEach((item) => {
                $("#sales").append($("<option></option>").attr("value", item.name).text(item.name));
            });
            // 初始化日期選擇器
            $('.input-group').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });
            // 初始化datatables
            const tenant_management_table = $('#tenant_management_table').DataTable({
                "pageLength": 20,
                "order": [
                    [9, 'desc']
                ],
                // "responsive": true,
                scrollX: true,
                // "searching": false,
                "dom": 'Bfrtip',
                "buttons": [{
                    text: '新增案件',
                    action: function(e, dt, node, config) {
                        window.location.href =
                            `/tenantManagementNews/create`
                    }
                }],
                "ajax": {
                    type: 'GET',
                    url: `/api/tenant_management_news`,
                    dataSrc: function(data) {
                        console.log(data);

                        data = data.data.map((value, index, array) => {
                            // 日期
                            value["apply_date"] = value["apply_date"] ?
                                convertToRCDate(value["apply_date"]) : "";
                            // 包租/代管
                            value["case_type"] = value["case_type_chartering"] === "1" ?
                                "包租" : "代管";
                            // 資格
                            value["membtype"] = value["membtype"] || "未輸入";
                            // 業務
                            let current_sales_obj = salesmanData.filter((item) => item.id === value["sales_id"]);
                            value["sales_name"] = current_sales_obj[0] ?current_sales_obj[0]["name"] : "";
                            // 建檔人
                            let current_user_obj = usersData.filter((item) => item.id === value["creator"]);
                            value["creator"] = current_user_obj[0] ? current_user_obj[0]["name"] : "";
                            console.log(value);
                            // 弱勢資格
                            let weak_item = JSON.parse(value["weak_item"]);
                            value["weak_item"] = value["weak_item"] ? weak_item.join(",") : "";
                            // 案件編號
                            value["case_no"] = value["guild_edition"] === 1 ? "公會版" : value[
                                "case_no"];
                            return value;
                        });

                        // 如果登入的使用者是業務就只會看到他的資料
                        @can('sales')
                            data = data.filter((item) => {
                                return item.sales_id === {{ Auth::user()->id }}
                            });
                        @endcan

                        return data;
                    }
                },
                "columns": [{
                        data: 'apply_date',
                        title: "申請日期",
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'case_status',
                        title: "案件狀態",
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'sales_name',
                        title: "業務",
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'case_type',
                        title: "包租/代管",
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'case_no',
                        title: '案件編號',
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'government_no',
                        title: '內政部編號',
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'name',
                        title: '承租人姓名',
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'membtype',
                        title: "資格",
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'weak_item',
                        title: '弱勢項目',
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: 'created_at',
                        title: '資料建立時間(隱藏)',
                        className: 'dt-body-center dt-head-center',
                        visible: false,
                    },
                    {
                        data: 'creator',
                        title: '建檔人',
                        className: 'dt-body-center dt-head-center',

                    },
                    {
                        data: null,
                        title: "操作功能",
                        render: function(data, type, row) {
                            let buttonHtmlContent = '';
                            @can('sales')
                                if (data.data_status !== "staff" && data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit'">編輯</button >`;
                                }
                                buttonHtmlContent += `<button type="button" class="btn btn-primary btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit?cantedit=true'">檢視</button >`;
                                if (data.data_status !== "staff" && data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-success btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit?copy=true'">複製</button >`;
                                    buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteTenantData(${data.id});">刪除</button >`;
                                }
                            @elsecan("staff")
                                if (data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit'">編輯</button >`;
                                }
                                buttonHtmlContent += `<button type="button" class="btn btn-primary btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit?cantedit=true'">檢視</button >`;
                                if (data.data_status !== "admin") {
                                    buttonHtmlContent += `<button type="button" class="btn btn-success btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit?copy=true'">複製</button >`;
                                    buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteTenantData(${data.id});">刪除</button >`;
                                }
                            @elsecan("admin")
                                buttonHtmlContent += `<button type="button" class="btn btn-warning btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit'">編輯</button >`;
                                buttonHtmlContent += `<button type="button" class="btn btn-primary btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit?cantedit=true'">檢視</button >`;
                                buttonHtmlContent += `<button type="button" class="btn btn-success btn-sm mx-1" onclick="location.href='tenantManagementNews/${data.id}/edit?copy=true'">複製</button >`;
                                buttonHtmlContent += `<button type="button" class="btn btn-danger btn-sm mx-1" onclick="deleteTenantData(${data.id});">刪除</button >`;
                            @endcan

                            return buttonHtmlContent;
                        },
                        className: 'dt-body-center dt-head-center',

                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/zh_Hant.json"
                },
            });

            $('#search').on('click', function() {
                tenant_management_table.draw();
            });
        });
    </script>
@endpush
