<!-- Apply Date Field -->
<div class="form-group col-12">
    {!! Form::label('apply_date', __('models/tenantManagementNews.fields.apply_date').':') !!}
    <p>{{ $tenantManagementNew->apply_date }}</p>
</div>


<!-- Sales Id Field -->
<div class="form-group col-12">
    {!! Form::label('sales_id', __('models/tenantManagementNews.fields.sales_id').':') !!}
    <p>{{ $tenantManagementNew->sales_id }}</p>
</div>


<!-- Government No Field -->
<div class="form-group col-12">
    {!! Form::label('government_no', __('models/tenantManagementNews.fields.government_no').':') !!}
    <p>{{ $tenantManagementNew->government_no }}</p>
</div>


<!-- Case No Field -->
<div class="form-group col-12">
    {!! Form::label('case_no', __('models/tenantManagementNews.fields.case_no').':') !!}
    <p>{{ $tenantManagementNew->case_no }}</p>
</div>


<!-- Case Type Chartering Field -->
<div class="form-group col-12">
    {!! Form::label('case_type_chartering', __('models/tenantManagementNews.fields.case_type_chartering').':') !!}
    <p>{{ $tenantManagementNew->case_type_chartering }}</p>
</div>


<!-- Case Type Escrow Field -->
<div class="form-group col-12">
    {!! Form::label('case_type_escrow', __('models/tenantManagementNews.fields.case_type_escrow').':') !!}
    <p>{{ $tenantManagementNew->case_type_escrow }}</p>
</div>


<!-- Case Status Field -->
<div class="form-group col-12">
    {!! Form::label('case_status', __('models/tenantManagementNews.fields.case_status').':') !!}
    <p>{{ $tenantManagementNew->case_status }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/tenantManagementNews.fields.name').':') !!}
    <p>{{ $tenantManagementNew->name }}</p>
</div>


<!-- Gender Field -->
<div class="form-group col-12">
    {!! Form::label('gender', __('models/tenantManagementNews.fields.gender').':') !!}
    <p>{{ $tenantManagementNew->gender }}</p>
</div>


<!-- Birthday Field -->
<div class="form-group col-12">
    {!! Form::label('birthday', __('models/tenantManagementNews.fields.birthday').':') !!}
    <p>{{ $tenantManagementNew->birthday }}</p>
</div>


<!-- Id No Field -->
<div class="form-group col-12">
    {!! Form::label('id_no', __('models/tenantManagementNews.fields.id_no').':') !!}
    <p>{{ $tenantManagementNew->id_no }}</p>
</div>


<!-- House No Field -->
<div class="form-group col-12">
    {!! Form::label('house_no', __('models/tenantManagementNews.fields.house_no').':') !!}
    <p>{{ $tenantManagementNew->house_no }}</p>
</div>


<!-- Tel Day Field -->
<div class="form-group col-12">
    {!! Form::label('tel_day', __('models/tenantManagementNews.fields.tel_day').':') !!}
    <p>{{ $tenantManagementNew->tel_day }}</p>
</div>


<!-- Tel Night Field -->
<div class="form-group col-12">
    {!! Form::label('tel_night', __('models/tenantManagementNews.fields.tel_night').':') !!}
    <p>{{ $tenantManagementNew->tel_night }}</p>
</div>


<!-- Cellphone Field -->
<div class="form-group col-12">
    {!! Form::label('cellphone', __('models/tenantManagementNews.fields.cellphone').':') !!}
    <p>{{ $tenantManagementNew->cellphone }}</p>
</div>


<!-- Email Field -->
<div class="form-group col-12">
    {!! Form::label('email', __('models/tenantManagementNews.fields.email').':') !!}
    <p>{{ $tenantManagementNew->email }}</p>
</div>


<!-- Membtype Field -->
<div class="form-group col-12">
    {!! Form::label('membtype', __('models/tenantManagementNews.fields.membtype').':') !!}
    <p>{{ $tenantManagementNew->membtype }}</p>
</div>


<!-- Weak Item Field -->
<div class="form-group col-12">
    {!! Form::label('weak_item', __('models/tenantManagementNews.fields.weak_item').':') !!}
    <p>{{ $tenantManagementNew->weak_item }}</p>
</div>


<!-- Residence City Field -->
<div class="form-group col-12">
    {!! Form::label('residence_city', __('models/tenantManagementNews.fields.residence_city').':') !!}
    <p>{{ $tenantManagementNew->residence_city }}</p>
</div>


<!-- Residence City Area Field -->
<div class="form-group col-12">
    {!! Form::label('residence_city_area', __('models/tenantManagementNews.fields.residence_city_area').':') !!}
    <p>{{ $tenantManagementNew->residence_city_area }}</p>
</div>


<!-- Residence Addr Field -->
<div class="form-group col-12">
    {!! Form::label('residence_addr', __('models/tenantManagementNews.fields.residence_addr').':') !!}
    <p>{{ $tenantManagementNew->residence_addr }}</p>
</div>


<!-- Mailing City Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_city', __('models/tenantManagementNews.fields.mailing_city').':') !!}
    <p>{{ $tenantManagementNew->mailing_city }}</p>
</div>


<!-- Mailing City Area Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_city_area', __('models/tenantManagementNews.fields.mailing_city_area').':') !!}
    <p>{{ $tenantManagementNew->mailing_city_area }}</p>
</div>


<!-- Mailing Addr Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_addr', __('models/tenantManagementNews.fields.mailing_addr').':') !!}
    <p>{{ $tenantManagementNew->mailing_addr }}</p>
</div>


<!-- Is Have Other Subsidy Field -->
<div class="form-group col-12">
    {!! Form::label('is_have_other_subsidy', __('models/tenantManagementNews.fields.is_have_other_subsidy').':') !!}
    <p>{{ $tenantManagementNew->is_have_other_subsidy }}</p>
</div>


<!-- Isothersubsidy01 Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy01', __('models/tenantManagementNews.fields.isOtherSubsidy01').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy01 }}</p>
</div>


<!-- Isothersubsidy01Info Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy01Info', __('models/tenantManagementNews.fields.isOtherSubsidy01Info').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy01Info }}</p>
</div>


<!-- Isothersubsidy02 Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy02', __('models/tenantManagementNews.fields.isOtherSubsidy02').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy02 }}</p>
</div>


<!-- Isothersubsidy02Info Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy02Info', __('models/tenantManagementNews.fields.isOtherSubsidy02Info').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy02Info }}</p>
</div>


<!-- Isothersubsidy03 Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy03', __('models/tenantManagementNews.fields.isOtherSubsidy03').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy03 }}</p>
</div>


<!-- Isothersubsidy03Info Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy03Info', __('models/tenantManagementNews.fields.isOtherSubsidy03Info').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy03Info }}</p>
</div>


<!-- Isothersubsidy04 Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy04', __('models/tenantManagementNews.fields.isOtherSubsidy04').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy04 }}</p>
</div>


<!-- Isothersubsidy04Info Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy04Info', __('models/tenantManagementNews.fields.isOtherSubsidy04Info').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy04Info }}</p>
</div>


<!-- Isothersubsidy05 Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy05', __('models/tenantManagementNews.fields.isOtherSubsidy05').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy05 }}</p>
</div>


<!-- Isothersubsidy05Info Field -->
<div class="form-group col-12">
    {!! Form::label('isOtherSubsidy05Info', __('models/tenantManagementNews.fields.isOtherSubsidy05Info').':') !!}
    <p>{{ $tenantManagementNew->isOtherSubsidy05Info }}</p>
</div>


<!-- Legal Agent Num Field -->
<div class="form-group col-12">
    {!! Form::label('legal_agent_num', __('models/tenantManagementNews.fields.legal_agent_num').':') !!}
    <p>{{ $tenantManagementNew->legal_agent_num }}</p>
</div>


<!-- Single Proxy Reason Field -->
<div class="form-group col-12">
    {!! Form::label('single_proxy_reason', __('models/tenantManagementNews.fields.single_proxy_reason').':') !!}
    <p>{{ $tenantManagementNew->single_proxy_reason }}</p>
</div>


<!-- First Legal Agent Name Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_name', __('models/tenantManagementNews.fields.first_legal_agent_name').':') !!}
    <p>{{ $tenantManagementNew->first_legal_agent_name }}</p>
</div>


<!-- First Legal Agent Tel Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_tel', __('models/tenantManagementNews.fields.first_legal_agent_tel').':') !!}
    <p>{{ $tenantManagementNew->first_legal_agent_tel }}</p>
</div>


<!-- First Legal Agent Phone Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_phone', __('models/tenantManagementNews.fields.first_legal_agent_phone').':') !!}
    <p>{{ $tenantManagementNew->first_legal_agent_phone }}</p>
</div>


<!-- First Legal Agent Addr Field -->
<div class="form-group col-12">
    {!! Form::label('first_legal_agent_addr', __('models/tenantManagementNews.fields.first_legal_agent_addr').':') !!}
    <p>{{ $tenantManagementNew->first_legal_agent_addr }}</p>
</div>


<!-- Second Legal Agent Name Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_name', __('models/tenantManagementNews.fields.second_legal_agent_name').':') !!}
    <p>{{ $tenantManagementNew->second_legal_agent_name }}</p>
</div>


<!-- Second Legal Agent Tel Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_tel', __('models/tenantManagementNews.fields.second_legal_agent_tel').':') !!}
    <p>{{ $tenantManagementNew->second_legal_agent_tel }}</p>
</div>


<!-- Second Legal Agent Phone Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_phone', __('models/tenantManagementNews.fields.second_legal_agent_phone').':') !!}
    <p>{{ $tenantManagementNew->second_legal_agent_phone }}</p>
</div>


<!-- Second Legal Agent Addr Field -->
<div class="form-group col-12">
    {!! Form::label('second_legal_agent_addr', __('models/tenantManagementNews.fields.second_legal_agent_addr').':') !!}
    <p>{{ $tenantManagementNew->second_legal_agent_addr }}</p>
</div>


<!-- Bank Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_name', __('models/tenantManagementNews.fields.bank_name').':') !!}
    <p>{{ $tenantManagementNew->bank_name }}</p>
</div>


<!-- Bank Branch Code Field -->
<div class="form-group col-12">
    {!! Form::label('bank_branch_code', __('models/tenantManagementNews.fields.bank_branch_code').':') !!}
    <p>{{ $tenantManagementNew->bank_branch_code }}</p>
</div>


<!-- Bank Owner Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_owner_name', __('models/tenantManagementNews.fields.bank_owner_name').':') !!}
    <p>{{ $tenantManagementNew->bank_owner_name }}</p>
</div>


<!-- Bank Account Field -->
<div class="form-group col-12">
    {!! Form::label('bank_account', __('models/tenantManagementNews.fields.bank_account').':') !!}
    <p>{{ $tenantManagementNew->bank_account }}</p>
</div>


<!-- Building Pattern 01 Field -->
<div class="form-group col-12">
    {!! Form::label('building_pattern_01', __('models/tenantManagementNews.fields.building_pattern_01').':') !!}
    <p>{{ $tenantManagementNew->building_pattern_01 }}</p>
</div>


<!-- Building Pattern 02 Field -->
<div class="form-group col-12">
    {!! Form::label('building_pattern_02', __('models/tenantManagementNews.fields.building_pattern_02').':') !!}
    <p>{{ $tenantManagementNew->building_pattern_02 }}</p>
</div>


<!-- Building Pattern 03 Field -->
<div class="form-group col-12">
    {!! Form::label('building_pattern_03', __('models/tenantManagementNews.fields.building_pattern_03').':') !!}
    <p>{{ $tenantManagementNew->building_pattern_03 }}</p>
</div>


<!-- Building Pattern 04 Field -->
<div class="form-group col-12">
    {!! Form::label('building_pattern_04', __('models/tenantManagementNews.fields.building_pattern_04').':') !!}
    <p>{{ $tenantManagementNew->building_pattern_04 }}</p>
</div>


<!-- Building Pattern 05 Field -->
<div class="form-group col-12">
    {!! Form::label('building_pattern_05', __('models/tenantManagementNews.fields.building_pattern_05').':') !!}
    <p>{{ $tenantManagementNew->building_pattern_05 }}</p>
</div>


<!-- Building Type 01 Field -->
<div class="form-group col-12">
    {!! Form::label('building_type_01', __('models/tenantManagementNews.fields.building_type_01').':') !!}
    <p>{{ $tenantManagementNew->building_type_01 }}</p>
</div>


<!-- Building Type 02 Field -->
<div class="form-group col-12">
    {!! Form::label('building_type_02', __('models/tenantManagementNews.fields.building_type_02').':') !!}
    <p>{{ $tenantManagementNew->building_type_02 }}</p>
</div>


<!-- Building Type 03 Field -->
<div class="form-group col-12">
    {!! Form::label('building_type_03', __('models/tenantManagementNews.fields.building_type_03').':') !!}
    <p>{{ $tenantManagementNew->building_type_03 }}</p>
</div>


<!-- Building Type 04 Field -->
<div class="form-group col-12">
    {!! Form::label('building_type_04', __('models/tenantManagementNews.fields.building_type_04').':') !!}
    <p>{{ $tenantManagementNew->building_type_04 }}</p>
</div>


<!-- Building Use Area 01 Field -->
<div class="form-group col-12">
    {!! Form::label('building_use_area_01', __('models/tenantManagementNews.fields.building_use_area_01').':') !!}
    <p>{{ $tenantManagementNew->building_use_area_01 }}</p>
</div>


<!-- Building Use Area 02 Field -->
<div class="form-group col-12">
    {!! Form::label('building_use_area_02', __('models/tenantManagementNews.fields.building_use_area_02').':') !!}
    <p>{{ $tenantManagementNew->building_use_area_02 }}</p>
</div>


<!-- Building Use Area 03 Field -->
<div class="form-group col-12">
    {!! Form::label('building_use_area_03', __('models/tenantManagementNews.fields.building_use_area_03').':') !!}
    <p>{{ $tenantManagementNew->building_use_area_03 }}</p>
</div>


<!-- Building Use Area 04 Field -->
<div class="form-group col-12">
    {!! Form::label('building_use_area_04', __('models/tenantManagementNews.fields.building_use_area_04').':') !!}
    <p>{{ $tenantManagementNew->building_use_area_04 }}</p>
</div>


<!-- Building Use Area 05 Field -->
<div class="form-group col-12">
    {!! Form::label('building_use_area_05', __('models/tenantManagementNews.fields.building_use_area_05').':') !!}
    <p>{{ $tenantManagementNew->building_use_area_05 }}</p>
</div>


<!-- Wish Floor 01 Field -->
<div class="form-group col-12">
    {!! Form::label('wish_floor_01', __('models/tenantManagementNews.fields.wish_floor_01').':') !!}
    <p>{{ $tenantManagementNew->wish_floor_01 }}</p>
</div>


<!-- Wish Floor 02 Field -->
<div class="form-group col-12">
    {!! Form::label('wish_floor_02', __('models/tenantManagementNews.fields.wish_floor_02').':') !!}
    <p>{{ $tenantManagementNew->wish_floor_02 }}</p>
</div>


<!-- Wish Floor 03 Field -->
<div class="form-group col-12">
    {!! Form::label('wish_floor_03', __('models/tenantManagementNews.fields.wish_floor_03').':') !!}
    <p>{{ $tenantManagementNew->wish_floor_03 }}</p>
</div>


<!-- Wish Floor 04 Field -->
<div class="form-group col-12">
    {!! Form::label('wish_floor_04', __('models/tenantManagementNews.fields.wish_floor_04').':') !!}
    <p>{{ $tenantManagementNew->wish_floor_04 }}</p>
</div>


<!-- Wish Floor Exact Field -->
<div class="form-group col-12">
    {!! Form::label('wish_floor_exact', __('models/tenantManagementNews.fields.wish_floor_exact').':') !!}
    <p>{{ $tenantManagementNew->wish_floor_exact }}</p>
</div>


<!-- Wish Room Num Field -->
<div class="form-group col-12">
    {!! Form::label('wish_room_num', __('models/tenantManagementNews.fields.wish_room_num').':') !!}
    <p>{{ $tenantManagementNew->wish_room_num }}</p>
</div>


<!-- Wish Living Rooms Num Field -->
<div class="form-group col-12">
    {!! Form::label('wish_living_rooms_num', __('models/tenantManagementNews.fields.wish_living_rooms_num').':') !!}
    <p>{{ $tenantManagementNew->wish_living_rooms_num }}</p>
</div>


<!-- Wish Bathrooms Num Field -->
<div class="form-group col-12">
    {!! Form::label('wish_bathrooms_num', __('models/tenantManagementNews.fields.wish_bathrooms_num').':') !!}
    <p>{{ $tenantManagementNew->wish_bathrooms_num }}</p>
</div>


<!-- Req Access Control Field -->
<div class="form-group col-12">
    {!! Form::label('req_access_control', __('models/tenantManagementNews.fields.req_access_control').':') !!}
    <p>{{ $tenantManagementNew->req_access_control }}</p>
</div>


<!-- Rent Min Field -->
<div class="form-group col-12">
    {!! Form::label('rent_min', __('models/tenantManagementNews.fields.rent_min').':') !!}
    <p>{{ $tenantManagementNew->rent_min }}</p>
</div>


<!-- Building Offer Other Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_other', __('models/tenantManagementNews.fields.building_offer_other').':') !!}
    <p>{{ $tenantManagementNew->building_offer_other }}</p>
</div>


<!-- Building Offer Sofa Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_sofa', __('models/tenantManagementNews.fields.building_offer_sofa').':') !!}
    <p>{{ $tenantManagementNew->building_offer_sofa }}</p>
</div>


<!-- Building Offer Chair Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_chair', __('models/tenantManagementNews.fields.building_offer_chair').':') !!}
    <p>{{ $tenantManagementNew->building_offer_chair }}</p>
</div>


<!-- Building Offer Desk Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_desk', __('models/tenantManagementNews.fields.building_offer_desk').':') !!}
    <p>{{ $tenantManagementNew->building_offer_desk }}</p>
</div>


<!-- Building Offer Wardrobe Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_wardrobe', __('models/tenantManagementNews.fields.building_offer_wardrobe').':') !!}
    <p>{{ $tenantManagementNew->building_offer_wardrobe }}</p>
</div>


<!-- Building Offer Bed Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_bed', __('models/tenantManagementNews.fields.building_offer_bed').':') !!}
    <p>{{ $tenantManagementNew->building_offer_bed }}</p>
</div>


<!-- Building Offer Natural Gas Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_natural_gas', __('models/tenantManagementNews.fields.building_offer_natural_gas').':') !!}
    <p>{{ $tenantManagementNew->building_offer_natural_gas }}</p>
</div>


<!-- Building Offer Washing Machine Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_washing_machine', __('models/tenantManagementNews.fields.building_offer_washing_machine').':') !!}
    <p>{{ $tenantManagementNew->building_offer_washing_machine }}</p>
</div>


<!-- Building Offer Internet Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_internet', __('models/tenantManagementNews.fields.building_offer_internet').':') !!}
    <p>{{ $tenantManagementNew->building_offer_internet }}</p>
</div>


<!-- Building Offer Geyser Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_geyser', __('models/tenantManagementNews.fields.building_offer_geyser').':') !!}
    <p>{{ $tenantManagementNew->building_offer_geyser }}</p>
</div>


<!-- Building Offer Air Conditioner Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_air_conditioner', __('models/tenantManagementNews.fields.building_offer_air_conditioner').':') !!}
    <p>{{ $tenantManagementNew->building_offer_air_conditioner }}</p>
</div>


<!-- Building Offer Cable Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_cable', __('models/tenantManagementNews.fields.building_offer_cable').':') !!}
    <p>{{ $tenantManagementNew->building_offer_cable }}</p>
</div>


<!-- Building Offer Refrigerator Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_refrigerator', __('models/tenantManagementNews.fields.building_offer_refrigerator').':') !!}
    <p>{{ $tenantManagementNew->building_offer_refrigerator }}</p>
</div>


<!-- Building Offer Tv Field -->
<div class="form-group col-12">
    {!! Form::label('building_offer_tv', __('models/tenantManagementNews.fields.building_offer_tv').':') !!}
    <p>{{ $tenantManagementNew->building_offer_tv }}</p>
</div>


<!-- Is Live Together Field -->
<div class="form-group col-12">
    {!! Form::label('is_live_together', __('models/tenantManagementNews.fields.is_live_together').':') !!}
    <p>{{ $tenantManagementNew->is_live_together }}</p>
</div>


<!-- Is Live Together Desc Field -->
<div class="form-group col-12">
    {!! Form::label('is_live_together_desc', __('models/tenantManagementNews.fields.is_live_together_desc').':') !!}
    <p>{{ $tenantManagementNew->is_live_together_desc }}</p>
</div>


<!-- Rent Max Field -->
<div class="form-group col-12">
    {!! Form::label('rent_max', __('models/tenantManagementNews.fields.rent_max').':') !!}
    <p>{{ $tenantManagementNew->rent_max }}</p>
</div>


