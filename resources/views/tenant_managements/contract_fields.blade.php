<style>
    .text-area {
        background-color: #fff;
        background-image: none;
        border: 1px solid rgba(0, 0, 0, 0.07);
        font-family: Arial, sans-serif;
        -webkit-appearance: none;
        color: #373e43;
        outline: 0;
        padding: 8px 12px;
        line-height: normal;
        font-size: 14px;
        font-weight: normal;
        vertical-align: middle;
        min-height: 30px;
        margin-left: 3px;
    }
    .necessary {
        color: #FF0000;
    }
</style>

<div class="row">

<div class="form-group col-sm-3">
    <label class="necessary" for="status">房客狀態:</label>
    {!! Form::select('status', ['已媒合' => '已媒合', '已收訂待媒合' => '已收訂待媒合', '退出' => '退出', '警示客戶' => '警示客戶'],  $tenantManagement->status, ['class' => 'form-control'])!!}
</div>

<div class="form-group col-sm-3">
    <label>案件編號:</label>
    {!! Form::text('tenant_case_number', $tenantManagement->tenant_case_number, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>

{!! Form::hidden('ref_code', $tenantManagement->ref_code, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}

<!-- Apply Date Field -->
<div class="form-group col-sm-3">
    {!! Form::label('apply_date', __('models/tenantManagements.fields.apply_date').':') !!}
    @isset($tenantManagement)
        {!! Form::text('apply_date', date('Y-m-d', strtotime($tenantManagement->apply_date)), ['class' => 'form-control','id'=>'apply_date', 'style' => 'height: 35px;']) !!}
    @else
        {!! Form::text('apply_date', null, ['class' => 'form-control','id'=>'apply_date', 'style' => 'height: 35px;']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#apply_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<div class="form-group col-sm-3">
    {!! Form::label('re_type', __('models/tenantManagements.fields.re_type').':', ['class' => 'necessary']) !!}
    {!! Form::select('re_type', ['包租'=>'包租', '代管' => '代管'],  $tenantManagement->re_type, ['class' => 'form-control'])!!}
</div>
<div class="form-group col-sm-2">
    <label class="necessary">承租人姓名:</label>
    {!! Form::text('lessee_name', $tenantManagement->lessee_name, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-2">
    <label class="necessary">性別:</label>
    {!! Form::select('lessee_gender', array('男' => '男', '女' => '女'), $tenantManagement->lessee_gender, ['class' => 'form-control', 'style' => 'height: 30px; padding: 5px;']) !!}
</div>

<div class="form-group col-sm-2">
    <label class="necessary">出生年月日:</label>
    @isset($tenantManagement)
        {!! Form::text('lessee_birthday', date('Y-m-d', strtotime($tenantManagement->lessee_birthday)), ['class' => 'form-control','id'=>'lessee_birthday', 'style' => 'height: 35px;']) !!}
    @else
        {!! Form::text('lessee_birthday', null, ['class' => 'form-control','id'=>'lessee_birthday', 'style' => 'height: 35px;']) !!}
    @endisset
</div>

@push('scripts')
    <script type="text/javascript">
        $('#lessee_birthday').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<div class="form-group col-sm-3">
    <label class="necessary">身分證字號:</label>
    {!! Form::text('lessee_id_num', $tenantManagement->lessee_id_num, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-3">
    <label>戶口名簿戶號:</label>
    {!! Form::text('lessee_hc_num', $tenantManagement->lessee_hc_num, ['class' => 'form-control', 'style' => 'height: 30px;','maxlength'=>'8']) !!}
</div>

<div class="form-group col-sm-3">
    <label class="necessary">電話(日):</label>
    {!! Form::text('lessee_telephone_d', $tenantManagement->lessee_telephone_d, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-3">
    <label class="necessary">電話(夜):</label>
    {!! Form::text('lessee_telephone_n', $tenantManagement->lessee_telephone_n, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-3">
    <label class="necessary">手機:</label>
    {!! Form::text('lessee_cellphone', $tenantManagement->lessee_cellphone, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-3">
    <label>電子信箱:</label>
    {!! Form::text('lessee_email', $tenantManagement->lessee_email, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-3">
    <label>銀行名稱:</label>
    {!! Form::text('bank_name', $tenantManagement->bank_name, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-3">
    <label>銀行代號:</label>
    {!! Form::text('bank_code', $tenantManagement->bank_code, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
</div>

<div class="form-group col-sm-6">
    <label>銀行帳號:</label>
    {!! Form::text('bank_account', $tenantManagement->bank_account, ['class' => 'form-control', 'style' => 'height: 30px;','maxlength'=>'14','minlength'=>'12']) !!}
</div>

<div class="col-sm-3">
    <label class="necessary" style="font-size: 10.5px;">承租人及其家庭成員資格:</label>
    @isset($tenantManagement)
    {!! Form::select('qualifications', array('1' => '一般戶', '2' => '第一類弱勢戶', '3' => '第二類弱勢戶'), $tenantManagement->qualifications, ['class' => 'text-area', 'style' => 'height: 30px; padding: 5px;']) !!}
    @else
    {!! Form::select('qualifications', array('1' => '一般戶', '2' => '第一類弱勢戶', '3' => '第二類弱勢戶'), null, ['class' => 'text-area', 'style' => 'height: 30px; padding: 5px;']) !!}
    @endisset
</div>


<div class="form-group col-sm-12">
    <label>戶籍地址:</label><br>

    <label class="necessary">縣市:</label>
    {!! Form::text('hr_city', $tenantManagement->hr_city, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!}
    <label class="necessary">鄉鎮區:</label>
    {!! Form::text('hr_cityarea', $tenantManagement->hr_cityarea, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!}
    <label class="necessary">地址:</label>
    {!! Form::text('hr_address', $tenantManagement->hr_address, ['class' => 'text-area', 'style' => 'width: 50%; height: 30px;']) !!}
</div>

<div class="form-group col-sm-12">
    <label>通訊地址:</label><br>

    <label class="necessary">縣市:</label>
    {!! Form::text('contact_city', $tenantManagement->contact_city, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!}
    <label class="necessary">鄉鎮區:</label>
    {!! Form::text('contact_cityarea', $tenantManagement->contact_cityarea, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!}
    <label class="necessary">地址:</label>
    {!! Form::text('contact_address', $tenantManagement->contact_address, ['class' => 'text-area', 'style' => 'width: 50%; height: 30px;']) !!}
</div>

<!-- Receive Subsidy Field -->
<div class="form-group col-sm-12">
    {!! Form::label('receive_subsidy', __('models/tenantManagements.fields.receive_subsidy').':') !!}
    {!! Form::hidden('receive_subsidy', 1) !!}
    {!! Form::label('receive_subsidy_radio_yes', '是') !!}
    {!! Form::radio('receive_subsidy_radio', '1') !!}
    {!! Form::label('receive_subsidy_radio_no', '否') !!}
    {!! Form::radio('receive_subsidy_radio', '0') !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::checkbox('subsidy_rent', '1', $tenantManagement->subsidy_rent, ['id' => 'subsidy_rent_checkbox']) !!}
    {!! Form::text('subsidy_rent', $tenantManagement->subsidy_rent, ['id' => 'subsidy_rent_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
    <label>年度租金補貼</label><br>
    {!! Form::checkbox('subsidy_low_income', '1', $tenantManagement->subsidy_low_income, ['id' => 'subsidy_low_income_checkbox']) !!}
    {!! Form::text('subsidy_low_income', $tenantManagement->subsidy_low_income, ['id' => 'subsidy_low_income_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
    <label>年度低收入戶及中低收入戶租金補貼</label><br>
    {!! Form::checkbox('subsidy_disability', '1', $tenantManagement->subsidy_disability, ['id' => 'subsidy_disability_checkbox']) !!}
    {!! Form::text('subsidy_disability', $tenantManagement->subsidy_disability, ['id' => 'subsidy_disability_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
    <label>年度身心障礙者房屋租金補貼</label><br>
    {!! Form::checkbox('subsidy_decree_checkbox', '1', $tenantManagement->subsidy_decree_checkbox, ['id' => 'subsidy_decree_checkbox']) !!}
    {!! Form::text('subsidy_decree', $tenantManagement->subsidy_decree, ['id' => 'subsidy_decree_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
    <label>年度依其他法令相關租金補貼規定之租金補貼</label><br>
    {!! Form::checkbox('lease_nr_sr_city_checkbox', '1', $tenantManagement->lease_nr_sr_city, ['id' => 'lease_nr_sr_city_checkbox']) !!}
    {!! Form::text('lease_nr_sr_city', $tenantManagement->lease_nr_sr_city, ['id' => 'lease_nr_sr_city_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
    <label>縣市承租政府興建之國民住宅或社會住宅</label>
</div>

<!-- Legal Agent Num Field -->
<div class="col-sm-8">
    {!! Form::label('legal_agent_num', __('models/tenantManagements.fields.legal_agent_num',['style' => "font-size: 10.5px;"]).':') !!}
    {!! Form::select('legal_agent_num', array('0' => '0', '1' => '1', '2' => '2'), $tenantManagement->legal_agent_num, ['class' => 'text-area', 'id' => 'legal_agent_num']) !!}
</div>

<div class="form-group col-sm-12" id="legal_agent_1" hidden>
    <label>第一法定代理人:</label><br>
    <label>姓名:</label>
    {!! Form::text('first_agent_name', $tenantManagement->first_agent_name, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}
    <label>電話:</label>
    {!! Form::text('first_agent_telephone', $tenantManagement->first_agent_telephone, ['class' => 'text-area', 'style' => 'width: 10%; height: 30px;']) !!}
    <label>手機:</label>
    {!! Form::text('first_agent_cellphone', $tenantManagement->first_agent_cellphone, ['class' => 'text-area', 'style' => 'width: 10%; height: 30px;']) !!}
    <label>地址:</label>
    {!! Form::text('first_agent_address', $tenantManagement->first_agent_address, ['class' => 'text-area', 'style' => 'width: 40%; height: 30px;']) !!}
</div>

<div class="form-group col-sm-12" id="legal_agent_2" hidden>
    <label>第二法定代理人:</label><br>
    <label>姓名:</label>
    {!! Form::text('second_agent_name', $tenantManagement->second_agent_name, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}
    <label>電話:</label>
    {!! Form::text('second_agent_telephone', $tenantManagement->second_agent_telephone, ['class' => 'text-area', 'style' => 'width: 10%; height: 30px;']) !!}
    <label>手機:</label>
    {!! Form::text('second_agent_cellphone', $tenantManagement->second_agent_cellphone, ['class' => 'text-area', 'style' => 'width: 10%; height: 30px;']) !!}
    <label>地址:</label>
    {!! Form::text('second_agent_address', $tenantManagement->second_agent_address, ['class' => 'text-area', 'style' => 'width: 40%; height: 30px;']) !!}
</div>


<!-- Second Agent Address Field -->
<div class="form-group col-sm-6">
    <label for="user_file">個人文件</label>
    <input type="file" name="user_file" id="user_file" class="form-control">
</div>

<div class="form-group col-sm-12">
    <label>備註1:</label>
    {!! Form::text('remark_1', $tenantManagement->remark_1, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>
<div class="form-group col-sm-12">
    <label>備註2:</label>
    {!! Form::text('remark_2', $tenantManagement->remark_2, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
</div>

@if(isset($tenantManagement->modal_mode) && $tenantManagement->modal_mode == 1)


@else

    @if(Request::is('tenantManagements*'))
<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button type="button" class="btn btn-primary" onclick="changeAllT()">@lang('crud.save')</button>
</div>
        @endif

@endif

</div>
@push('scripts')
<script>
    var legal_agent_num_value = $('#legal_agent_num').val();
    if (legal_agent_num_value == 1) {
        $('#legal_agent_1').attr("hidden",false);
        $('#legal_agent_2').attr("hidden",true);
    }
    if (legal_agent_num_value == 2) {
        $('#legal_agent_1').attr("hidden",false);
        $('#legal_agent_2').attr("hidden",false);
    }
    $('#legal_agent_num').on('change', function(e) {
        if ($('#legal_agent_num').val() == 0) {
            $('#legal_agent_1').attr("hidden",true);
            $('#legal_agent_2').attr("hidden",true);
        }
        if ($('#legal_agent_num').val() == 1) {
            $('#legal_agent_1').attr("hidden",false);
            $('#legal_agent_2').attr("hidden",true);
        }
        if ($('#legal_agent_num').val() == 2) {
            $('#legal_agent_1').attr("hidden",false);
            $('#legal_agent_2').attr("hidden",false);
        }
    });

    if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            $('#subsidy_rent_text').removeAttr("readonly");
            $('#subsidy_low_income_text').removeAttr("readonly");
            $('#subsidy_disability_text').removeAttr("readonly");
            $('#subsidy_decree_text').removeAttr("readonly");
            $('#lease_nr_sr_city_text').removeAttr("readonly");
        }else{
            $('#subsidy_rent_checkbox').prop('checked', false);
            $('#subsidy_low_income_checkbox').prop('checked', false);
            $('#subsidy_disability_checkbox').prop('checked', false);
            $('#subsidy_decree_checkbox').prop('checked', false);
            $('#lease_nr_sr_city_checkbox').prop('checked', false);
            $('#subsidy_rent_text').attr("readonly"," readonly");
            $('#subsidy_low_income_text').attr("readonly"," readonly");
            $('#subsidy_disability_text').attr("readonly"," readonly");
            $('#subsidy_decree_text').attr("readonly"," readonly");
            $('#lease_nr_sr_city_text').attr("readonly"," readonly");
        }
    $("input[name='receive_subsidy_radio']").change(function(){
        if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            $("input[name='receive_subsidy']").val(1)
            $('#subsidy_rent_text').removeAttr("readonly");
            $('#subsidy_low_income_text').removeAttr("readonly");
            $('#subsidy_disability_text').removeAttr("readonly");
            $('#subsidy_decree_text').removeAttr("readonly");
            $('#lease_nr_sr_city_text').removeAttr("readonly");
        }else{
            $("input[name='receive_subsidy']").val(0)
            $('#subsidy_rent_checkbox').prop('checked', false);
            $('#subsidy_low_income_checkbox').prop('checked', false);
            $('#subsidy_disability_checkbox').prop('checked', false);
            $('#subsidy_decree_checkbox').prop('checked', false);
            $('#lease_nr_sr_city_checkbox').prop('checked', false);
            $('#subsidy_rent_text').attr("readonly"," readonly");
            $('#subsidy_low_income_text').attr("readonly"," readonly");
            $('#subsidy_disability_text').attr("readonly"," readonly");
            $('#subsidy_decree_text').attr("readonly"," readonly");
            $('#lease_nr_sr_city_text').attr("readonly"," readonly");
        }
    });

    if ($('#subsidy_rent_checkbox').is(":checked") == false) {
        $('#subsidy_rent_text').attr("readonly"," readonly");
    }
    if ($('#subsidy_low_income_checkbox').is(":checked") == false) {
        $('#subsidy_low_income_text').attr("readonly"," readonly");
    }
    if ($('#subsidy_disability_checkbox').is(":checked") == false) {
        $('#subsidy_disability_text').attr("readonly"," readonly");
    }
    if ($('#subsidy_decree_checkbox').is(":checked") == false) {
        $('#subsidy_decree_text').attr("readonly"," readonly");
    }
    if ($('#lease_nr_sr_city_checkbox').is(":checked") == false) {
        $('#lease_nr_sr_city_text').attr("readonly"," readonly");
    }
    $('#subsidy_rent_checkbox').on('change', function(e) {
        if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            if ($('#subsidy_rent_checkbox').is(":checked") == false) {
                $('#subsidy_rent_text').attr("readonly"," readonly");
            }else{
                $('#subsidy_rent_text').removeAttr("readonly");
            }
        }
    });
    $('#subsidy_low_income_checkbox').on('change', function(e) {
        if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            if ($('#subsidy_low_income_checkbox').is(":checked") == false) {
                $('#subsidy_low_income_text').attr("readonly"," readonly");
            }else{
                $('#subsidy_low_income_text').removeAttr("readonly");
            }
        }
    });
    $('#subsidy_disability_checkbox').on('change', function(e) {
        if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            if ($('#subsidy_disability_checkbox').is(":checked") == false) {
                $('#subsidy_disability_text').attr("readonly"," readonly");
            }else{
                $('#subsidy_disability_text').removeAttr("readonly");
            }
        }
    });
    $('#subsidy_decree_checkbox').on('change', function(e) {
        if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            if ($('#subsidy_decree_checkbox').is(":checked") == false) {
                $('#subsidy_decree_text').attr("readonly"," readonly");
            }else{
                $('#subsidy_decree_text').removeAttr("readonly");
            }
        }
    });
    $('#lease_nr_sr_city_checkbox').on('change', function(e) {
        if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            if ($('#lease_nr_sr_city_checkbox').is(":checked") == false) {
                $('#lease_nr_sr_city_text').attr("readonly"," readonly");
            }else{
                $('#lease_nr_sr_city_text').removeAttr("readonly");
            }
        }
    });
</script>
@endpush
