<style>
    .shadow-form-box {
        position: fixed;
        z-index: 99999;
        display: none;
        background: #fff;
        height: 400px;
        width: 400px;
        left:40%;
        top: 20%;
    }
</style>
<div class='btn-group'>
    @php
        $t = \App\Models\TenantManagement::where('id', $id)->first();
    @endphp
    <a href="/tenantManagements/export/{{$id}}" class='btn btn-default btn-xs' target="_blank">
        下載個人文件
    </a>
    @if($t->line_uid)
    <button class="btn btn-default btn-xs" style="z-index: 100" onclick="Toggle_MessageForm({{$id}})">個人廣播</button>
    @else
    <button class="btn btn-default btn-xs" style="z-index: 100" onclick="Not_Bind_MessageForm()">尚未綁定</button>
    @endif
    {{-- <a href="{{ route('tenantManagements.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a> --}}
    <a href="{{ route('tenantManagements.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::open(['route' => ['tenantManagements.destroy', $id], 'method' => 'delete']) !!}
        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
        ]) !!}
    {!! Form::close() !!}
</div>
@if($t->line_uid)
<div id="shadow-form{{$id}}" data-mid="{{$id}}" class="shadow shadow-form-box">
    <div class="row messageForm" style="margin: 0;">
        <div class="form-group col-12" style="display: flex; justify-content: center; align-items: center; height: 5vh;">
            <label for="message">
                訊息內容
            </label>
            <input type="hidden" value="{{$t->line_uid}}" id="line_uid">
        </div>
        <div class="form-group col-12">
            <textarea class="form-control" name="message" id="message"  style="min-width: 100%; min-height: 300px;"></textarea>
        </div>
        <!-- Submit Field -->
        <div class="form-group col-sm-12" style="height: 5vh;">
            <a href="#" class="btn btn-default float-right" onclick="Toggle_MessageForm({{$id}})">@lang('crud.cancel')</a>
            <a href="#" class="btn btn-primary  float-right submit">送出</a>
        </div>
    </div>
</div>


<script>
    function Not_Bind_MessageForm(){
        Swal.fire({
                    icon: 'error',
                    title: '尚未綁定LINE@'
                })
    }
    function Toggle_MessageForm(id){
        $(".shadow-form-box[data-mid='"+id+"']").toggle();
    }
    $('.messageForm .submit').on('click', function (e) {
        var parent_node = $(e.target).parents(".shadow-form-box");
        var id = $(parent_node).attr("data-mid");
        $.ajax({
            type: "POST",
            timeout: 60000,
            url: "/api/line/send/message",
            data: {
                'message': $(parent_node).find('#message').val(),
                'line_uid': $(parent_node).find('#line_uid').val()
            },
            success: function (data) {
                Swal.fire({
                    icon: 'success',
                    title: '廣播成功'
                })
                Toggle_MessageForm(id);
            },
            error: function (data) {
                Swal.fire({
                    icon: 'error',
                    title: '廣播失敗'
                })
            },
        });
    })
</script>
@endif
