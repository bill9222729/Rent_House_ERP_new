@extends('layouts.app')

@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">
            @lang('models/tenantManagements.singular')
        </h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        @include('adminlte-templates::common.errors')
        <div class="card card-transparent">
            <div class="card-body">
                <div class="row">
                    {!! Form::model($tenantManagement, ['route' => ['tenantManagements.update', $tenantManagement->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                            @include('tenant_managements.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

