<style>
    .text-area {
        background-color: #fff;
        background-image: none;
        border: 1px solid rgba(0, 0, 0, 0.07);
        font-family: Arial, sans-serif;
        -webkit-appearance: none;
        color: #373e43;
        outline: 0;
        padding: 8px 12px;
        line-height: normal;
        font-size: 14px;
        font-weight: normal;
        vertical-align: middle;
        min-height: 30px;
        margin-left: 3px;
    }

    .necessary {
        color: #FF0000;
    }

</style>

<div class="row" id="app">

    <div class="form-group col-sm-3">
        <label class="necessary" for="status">房客狀態:</label>
        {!! Form::select('status', ['已媒合' => '已媒合', '已收訂待媒合' => '已收訂待媒合', '退出' => '退出', '警示客戶' => '警示客戶'], null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label>案件編號:</label>
        {!! Form::text('tenant_case_number', null, ['class' => 'form-control maxlength_10', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>

    {!! Form::hidden('ref_code', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}

    <!-- Apply Date Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('apply_date', __('models/tenantManagements.fields.apply_date') . ':') !!}
        @isset($tenantManagement)
            {{-- {!! Form::text('apply_date', date('Y-m-d', strtotime($tenantManagement->apply_date)), ['class' => 'form-control datepickerTW', 'id' => 'apply_date', 'style' => 'height: 35px;']) !!} --}}
            {{-- {!! Form::text('apply_date', (int) date('Y', strtotime($tenantManagement->apply_date)) - 1911 . date('-m-d', strtotime($tenantManagement->apply_date)), ['class' => 'form-control datepickerTW', 'id' => 'apply_date', 'style' => 'height: 35px;']) !!} --}}
            {!! Form::text('apply_date', str_pad((string) ((int) date('Y', strtotime($tenantManagement->apply_date)) - 1911), 3, '0', STR_PAD_LEFT) . date('-m-d', strtotime($tenantManagement->apply_date)), ['class' => 'form-control datepickerTW', 'id' => 'apply_date', 'style' => 'height: 35px;']) !!}
        @else
            {!! Form::text('apply_date', null, ['class' => 'form-control datepickerTW', 'id' => 'apply_date', 'style' => 'height: 35px;']) !!}
        @endisset
    </div>


    <div class="form-group col-sm-3">
        {!! Form::label('sublet', '本人欲提供住宅出租予租屋服務事業再轉租(包租): ' . ':', ['class' => 'necessary']) !!}
        {!! Form::select('sublet', ['0' => '0:否', '1' => '1:是'], null, ['class' => 'form-control', 'id' => 'sublet']) !!}
        {!! Form::label('rent', '本人欲提供住宅經由租屋服務事業協助出租(代管): ' . ':', ['class' => 'necessary']) !!}
        {!! Form::select('rent', ['0' => '0:否', '1' => '1:是'], null, ['class' => 'form-control', 'id' => 'rent']) !!}
    </div>
    <div class="form-group col-sm-2">
        <label class="necessary">承租人姓名:</label>
        {!! Form::text('lessee_name', null, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-2">
        <label class="necessary">性別:</label>
        {!! Form::select('lessee_gender', ['男' => '0：男', '女' => '1：女'], null, ['class' => 'form-control', 'style' => 'height: 30px; padding: 5px;']) !!}
    </div>

    <div class="form-group col-sm-2">
        <label class="necessary">出生年月日:</label>
        @isset($tenantManagement)
            {{-- {!! Form::text('lessee_birthday', date('Y-m-d', strtotime($tenantManagement->lessee_birthday)), ['class' => 'form-control datepickerTW', 'id' => 'lessee_birthday', 'style' => 'height: 35px;']) !!} --}}
            {!! Form::text('lessee_birthday', str_pad((string) ((int) date('Y', strtotime($tenantManagement->lessee_birthday)) - 1911), 3, '0', STR_PAD_LEFT) . date('-m-d', strtotime($tenantManagement->lessee_birthday)), ['class' => 'form-control datepickerTW', 'id' => 'lessee_birthday', 'style' => 'height: 35px;']) !!}
        @else
            {!! Form::text('lessee_birthday', null, ['class' => 'form-control datepickerTW', 'id' => 'lessee_birthday', 'style' => 'height: 35px;']) !!}
        @endisset
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#lessee_birthday').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })
        </script>
    @endpush

    <div class="form-group col-sm-3">
        <label class="necessary">身分證字號:</label>
        {!! Form::text('lessee_id_num', null, ['class' => 'form-control', 'v-model' => 'lessee_id_num', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式(如：A123456789)', 'style' => 'height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label>戶口名簿戶號:</label>
        {!! Form::text('lessee_hc_num', null, ['class' => 'form-control', 'v-model' => 'lessee_hc_num', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式(如：m1234567)', 'style' => 'height: 30px;', 'maxlength' => '8']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label class="necessary">電話(日):</label>
        {!! Form::text('lessee_telephone_d', null, ['class' => 'form-control', 'v-model' => 'lessee_telephone_d', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入包含區碼的固定電話(如：02-12345678)', 'style' => 'height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label class="necessary">電話(夜):</label>
        {!! Form::text('lessee_telephone_n', null, ['class' => 'form-control', 'v-model' => 'lessee_telephone_n', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入包含區碼的固定電話(如：02-12345678)', 'style' => 'height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label class="necessary">手機:</label>
        {!! Form::text('lessee_cellphone', null, ['class' => 'form-control', 'v-model' => 'lessee_cellphone', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式的手機號碼(如：0912345678)', 'style' => 'height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label>電子信箱:</label>
        {!! Form::text('lessee_email', null, ['class' => 'form-control', 'style' => 'height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-3">
        <label>銀行名稱:</label>
        {{-- {!! Form::text('bank_name', null, ['class' => 'form-control', 'style' => 'height: 30px;']) !!} --}}
        <select class="form-control" name="bank_name" style="width: 100%; height: 30px;"
            v-model="agent_bank_name_active">
            <option v-for="item in bankCode.value" :value="item[2]" :key="item[2]">&nbsp;@{{ item[2] }}</option>
        </select>
    </div>

    <div class="form-group col-sm-3">
        <label>銀行代號:</label>
        {{-- {!! Form::text('bank_code', null, ['class' => 'form-control', 'style' => 'height: 30px;']) !!} --}}
        <input class="form-control" name="bank_code" type="text" style="width: 100%; height: 30px;"
            v-model="agent_branch_name">
    </div>

    <div class="form-group col-sm-6">
        <label>銀行帳號:</label>
        {!! Form::text('bank_account', null, ['class' => 'form-control', 'style' => 'height: 30px;', 'maxlength' => '16']) !!}
    </div>

    <div class="col-sm-3">
        <label class="necessary" style="font-size: 10.5px;">承租人及其家庭成員資格:</label>
        @isset($tenantManagement)
            {!! Form::select('qualifications', ['1' => '1：一般戶', '2' => '2：第一類弱勢戶', '3' => '3：第二類弱勢戶'], $tenantManagement->qualifications, ['class' => 'text-area', 'style' => 'height: 30px; padding: 5px;']) !!}
        @else
            {!! Form::select('qualifications', ['1' => '1：一般戶', '2' => '2：第一類弱勢戶', '3' => '3：第二類弱勢戶'], null, ['class' => 'text-area', 'style' => 'height: 30px; padding: 5px;']) !!}
        @endisset
    </div>


    <div class="form-group col-sm-12">
        <label>戶籍地址:</label><br>

        <label class="necessary">縣市:</label>
        {{-- {!! Form::text('hr_city', null, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!} --}}
        <select class="text-area" name="hr_city" style="width: 8%; height: 40px;" v-model="hr_city_active">
            <option v-for="item in hrTwZip.city" :value="item.name" :key="item.name">&nbsp;@{{ item . name }}
            </option>
        </select>
        <label class="necessary">鄉鎮區:</label>
        {{-- {!! Form::text('hr_cityarea', null, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!} --}}
        <select class="text-area" name="hr_cityarea" style="width: 8%; height: 40px;" v-model="hr_cityarea_active">
            <option v-for="item in hrTwZip.area" :value="item.name" :key="item.name">&nbsp;@{{ item . name }}
            </option>
        </select>
        <label class="necessary">地址:</label>
        {!! Form::text('hr_address', null, ['class' => 'text-area', 'style' => 'width: 50%; height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-12">
        <label>通訊地址:</label><br>

        @if (!isset($tenantManagement))
            <label> 同地址 <input type="checkbox" name="same_address" id="same_address"> </label><br>
        @endif
        <div class="residence_address_area">
            <label class="necessary">縣市:</label>
            {{-- {!! Form::text('contact_city', null, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!} --}}
            <select class="text-area" name="contact_city" style="width: 8%; height: 40px;"
                v-model="contact_city_active">
                <option v-for="item in contactTwZip.city" :value="item.name" :key="item.name">&nbsp;@{{ item . name }}
                </option>
            </select>
            <label class="necessary">鄉鎮區:</label>
            {{-- {!! Form::text('contact_cityarea', null, ['class' => 'text-area', 'style' => 'width: 15%; height: 30px;']) !!} --}}
            <select class="text-area" name="contact_cityarea" style="width: 8%; height: 40px;"
                v-model="contact_cityarea_active">
                <option v-for="item in contactTwZip.area" :value="item.name" :key="item.name">&nbsp;@{{ item . name }}
                </option>
            </select>
            <label class="necessary">地址:</label>
            {!! Form::text('contact_address', null, ['class' => 'text-area', 'style' => 'width: 50%; height: 30px;']) !!}
        </div>
    </div>

    <!-- Receive Subsidy Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('receive_subsidy', __('models/tenantManagements.fields.receive_subsidy') . ':') !!}
        {!! Form::hidden('receive_subsidy', 1) !!}
        {!! Form::label('receive_subsidy_radio_yes', '1：是') !!}
        {!! Form::radio('receive_subsidy_radio', '1') !!}
        {!! Form::label('receive_subsidy_radio_no', '0：否') !!}
        {!! Form::radio('receive_subsidy_radio', '0') !!}
    </div>

    <div class="form-group col-sm-8">
        {!! Form::checkbox('subsidy_rent', '1', null, ['id' => 'subsidy_rent_checkbox']) !!}
        {!! Form::number('subsidy_rent', null, ['id' => 'subsidy_rent_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
        <label>年度租金補貼</label><br>
        {!! Form::checkbox('subsidy_low_income', '1', null, ['id' => 'subsidy_low_income_checkbox']) !!}
        {!! Form::number('subsidy_low_income', null, ['id' => 'subsidy_low_income_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
        <label>年度低收入戶及中低收入戶租金補貼</label><br>
        {!! Form::checkbox('subsidy_disability', '1', null, ['id' => 'subsidy_disability_checkbox']) !!}
        {!! Form::number('subsidy_disability', null, ['id' => 'subsidy_disability_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
        <label>年度身心障礙者房屋租金補貼</label><br>
        {!! Form::checkbox('subsidy_decree_checkbox', '1', null, ['id' => 'subsidy_decree_checkbox']) !!}
        {!! Form::number('subsidy_decree', null, ['id' => 'subsidy_decree_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
        <label>年度依其他法令相關租金補貼規定之租金補貼</label><br>
        {!! Form::checkbox('lease_nr_sr_city_checkbox', '1', null, ['id' => 'lease_nr_sr_city_checkbox']) !!}
        {!! Form::number('lease_nr_sr_city', null, ['id' => 'lease_nr_sr_city_text', 'class' => 'text-area', 'style' => 'width: 10%; height: 15px;']) !!}
        <label>縣市承租政府興建之國民住宅或社會住宅</label>
    </div>

    <!-- Legal Agent Num Field -->
    <div class="col-sm-8">
        {!! Form::label('legal_agent_num', __('models/tenantManagements.fields.legal_agent_num', ['style' => 'font-size: 10.5px;']) . ':') !!}
        {!! Form::select('legal_agent_num', ['0' => '0', '1' => '1', '2' => '2'], null, ['class' => 'text-area', 'id' => 'legal_agent_num']) !!}
    </div>

    <div class="form-group col-sm-12" id="legal_agent_1" hidden>
        <label>第一法定代理人:</label><br>
        <label>姓名:</label>
        {!! Form::text('first_agent_name', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}
        <label>電話:</label>
        {!! Form::text('first_agent_telephone', null, ['class' => 'text-area', 'v-model' => 'first_agent_telephone', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式的電話號碼(如：03-9876543)', 'style' => 'width: 10%; height: 30px;']) !!}
        <label>手機:</label>
        {!! Form::text('first_agent_cellphone', null, ['class' => 'text-area', 'v-model' => 'first_agent_cellphone', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式的手機號碼(如：0912345678)', 'style' => 'width: 10%; height: 30px;']) !!}
        <label>地址:</label>
        {!! Form::text('first_agent_address', null, ['class' => 'text-area', 'style' => 'width: 40%; height: 30px;']) !!}
    </div>

    <div class="form-group col-sm-12" id="legal_agent_2" hidden>
        <label>第二法定代理人:</label><br>
        <label>姓名:</label>
        {!! Form::text('second_agent_name', null, ['class' => 'text-area', 'style' => 'width: 8%; height: 30px;']) !!}
        <label>電話:</label>
        {!! Form::text('second_agent_telephone', null, ['class' => 'text-area', 'v-model' => 'second_agent_telephone', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式的電話號碼(如：03-9876543)', 'style' => 'width: 10%; height: 30px;']) !!}
        <label>手機:</label>
        {!! Form::text('second_agent_cellphone', null, ['class' => 'text-area', 'v-model' => 'second_agent_cellphone', '@focus' => 'inputFocus = true', '@blur' => 'inputFocus = false', 'placeholder' => '請輸入正確格式的手機號碼(如：0912345678)', 'style' => 'width: 10%; height: 30px;']) !!}
        <label>地址:</label>
        {!! Form::text('second_agent_address', null, ['class' => 'text-area', 'style' => 'width: 40%; height: 30px;']) !!}
    </div>


    <!-- Second Agent Address Field -->
    {{-- <div class="form-group col-sm-6">
        <label for="user_file">個人文件</label>
        <input type="file" name="user_file" id="user_file" class="form-control">
    </div> --}}

    <div class="form-group col-sm-12">
        <label>備註1:</label>
        {!! Form::text('remark_1', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-12">
        <label>備註2:</label>
        {!! Form::text('remark_2', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label for="pay_date">繳款日:</label>
        {!! Form::text('pay_date', null, ['class' => 'form-control datepickerTW', 'style' => 'width: 100%; height: 30px;', 'id' => 'pay_date', 'autocomplete' => 'off']) !!}
    </div>
    <div class="form-group col-sm-6">
        @php
            $bs = \App\Models\business_management::all();
            $saless = ['無' => '無'];
            foreach ($bs as $b) {
                if (in_array($b->name, $saless)) {
                    continue;
                }

                $saless[$b->name] = $b->name;
            }
        @endphp
        <label for="sales">業務:</label>
        {!! Form::select('sales', $saless, null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        @php
            $weakItemOptions = [
                '無' => '無',
                '65歲以上' => '65歲以上',
                '65歲以上至第一年期滿' => '65歲以上至第一年期滿',
                '免疫缺乏' => '免疫缺乏',
                '育有三名未成年子女' => '育有三名未成年子女',
                '身心障礙' => '身心障礙',
                '保護令' => '保護令',
                '原住民' => '原住民',
                '家暴' => '家暴',
                '特殊境遇' => '特殊境遇',
                '中低收' => '中低收',
                '低收' => '低收',
            ];
            $resOptions = [
                '無' => '無',
                '未設籍' => '未設籍',
                '在學' => '在學',
                '設籍' => '設籍',
                '就業' => '就業',
            ];
        @endphp
        <label for="weak_item">弱勢項目:</label>
        {!! Form::select('weak_item', $weakItemOptions, null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label for="res">租房原因:</label>
        {!! Form::select('res', $resOptions, null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label for="tp">租補:</label>
        {!! Form::text('tp', null, ['class' => 'form-control', 'style' => 'width: 100%; height: 30px;']) !!}
    </div>


    <div class="form-group col-sm-3">
        <label>房客承租住宅申請書:</label>
        <input type="file" @change="upload_file($event,'tenant_application')">
        <div class="upload_button" @click="add_file('tenant_application')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>身分證:</label>
        <input type="file" @change="upload_file($event,'identity')">
        <div class="upload_button" @click="add_file('identity')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>戶籍謄本:</label>
        <input type="file" @change="upload_file($event,'household')">
        <div class="upload_button" @click="add_file('household')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>銀行帳號:</label>
        <input type="file" @change="upload_file($event,'bank_account')">
        <div class="upload_button" @click="add_file('bank_account')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>弱勢身分證明文件:</label>
        <input type="file" @change="upload_file($event,'proof')">
        <div class="upload_button" @click="add_file('proof')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>綜所稅清單:</label>
        <input type="file" @change="upload_file($event,'comprehensive')">
        <div class="upload_button" @click="add_file('comprehensive')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>財產稅清單:</label>
        <input type="file" @change="upload_file($event,'property')">
        <div class="upload_button" @click="add_file('property')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>放棄租金補貼切結書:</label>
        <input type="file" @change="upload_file($event,'abandonment')">
        <div class="upload_button" @click="add_file('abandonment')">上傳</div>
    </div>
    <div class="form-group col-sm-3">
        <label>其他:</label>
        <input type="file" @change="upload_file($event,'other')">
        <div class="upload_button" @click="add_file('other')">上傳</div>
    </div>

    <!-- 用來記所有檔案json的text(隱藏) -->
    <input type="text" v-model='file_json' name='file_json' v-show=false>

    @if (isset($tenantManagement->modal_mode) && $tenantManagement->modal_mode == 1)


    @else

        @if (Request::is('tenantManagements*'))
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('tenantManagements.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
            </div>
        @endif

    @endif

</div>
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#pay_date').datepicker({
                changeYear: false,
                format: "twy-mm-dd",
                language: "zh-TW",
            });

            $('#subsidy_rent_checkbox').on('click', function() {
                let isChecked = $(this).prop("checked");
                if (!isChecked) {
                    $('#subsidy_rent_text').attr("readonly", " readonly");
                    $('#subsidy_rent_text').val(0);
                } else {
                    $('#subsidy_rent_text').removeAttr("readonly");
                }
            });
            $('#subsidy_low_income_checkbox').on('click', function() {
                let isChecked = $(this).prop("checked");
                if (!isChecked) {
                    $('#subsidy_low_income_text').attr("readonly", " readonly");
                    $('#subsidy_low_income_text').val(0);
                } else {
                    $('#subsidy_low_income_text').removeAttr("readonly");
                }
            });
            $('#subsidy_disability_checkbox').on('click', function() {
                let isChecked = $(this).prop("checked");
                if (!isChecked) {
                    $('#subsidy_disability_text').attr("readonly", " readonly");
                    $('#subsidy_disability_text').val(0);
                } else {
                    $('#subsidy_disability_text').removeAttr("readonly");
                }
            });
            $('#subsidy_decree_checkbox').on('click', function() {
                let isChecked = $(this).prop("checked");
                if (!isChecked) {
                    $('#subsidy_decree_text').attr("readonly", " readonly");
                    $('#subsidy_decree_text').val(0);
                } else {
                    $('#subsidy_decree_text').removeAttr("readonly");
                }
            });
            $('#lease_nr_sr_city_checkbox').on('click', function() {
                let isChecked = $(this).prop("checked");
                if (!isChecked) {
                    $('#lease_nr_sr_city_text').attr("readonly", " readonly");
                    $('#lease_nr_sr_city_text').val(0);
                } else {
                    $('#lease_nr_sr_city_text').removeAttr("readonly");
                }
            });
        });
        $(document).on('change', '#legal_agent_num', function() {
            var legal_agent_num_value = $('#legal_agent_num').val();
            if (legal_agent_num_value == 1) {
                $('#legal_agent_1').attr("hidden", false);
                $('#legal_agent_2').attr("hidden", true);
            }
            if (legal_agent_num_value == 2) {
                $('#legal_agent_1').attr("hidden", false);
                $('#legal_agent_2').attr("hidden", false);
            }
            $('#legal_agent_num').on('change', function(e) {
                if ($('#legal_agent_num').val() == 0) {
                    $('#legal_agent_1').attr("hidden", true);
                    $('#legal_agent_2').attr("hidden", true);
                }
                if ($('#legal_agent_num').val() == 1) {
                    $('#legal_agent_1').attr("hidden", false);
                    $('#legal_agent_2').attr("hidden", true);
                }
                if ($('#legal_agent_num').val() == 2) {
                    $('#legal_agent_1').attr("hidden", false);
                    $('#legal_agent_2').attr("hidden", false);
                }
            });
        });

        if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
            $('#subsidy_rent_text').removeAttr("readonly");
            $('#subsidy_low_income_text').removeAttr("readonly");
            $('#subsidy_disability_text').removeAttr("readonly");
            $('#subsidy_decree_text').removeAttr("readonly");
            $('#lease_nr_sr_city_text').removeAttr("readonly");
        } else {
            $('#subsidy_rent_checkbox').prop('checked', false);
            $('#subsidy_low_income_checkbox').prop('checked', false);
            $('#subsidy_disability_checkbox').prop('checked', false);
            $('#subsidy_decree_checkbox').prop('checked', false);
            $('#lease_nr_sr_city_checkbox').prop('checked', false);
            $('#subsidy_rent_text').attr("readonly", " readonly");
            $('#subsidy_low_income_text').attr("readonly", " readonly");
            $('#subsidy_disability_text').attr("readonly", " readonly");
            $('#subsidy_decree_text').attr("readonly", " readonly");
            $('#lease_nr_sr_city_text').attr("readonly", " readonly");
        }
        $("input[name='receive_subsidy_radio']").change(function() {
            if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
                $("input[name='receive_subsidy']").val(1)
                $('#subsidy_rent_text').removeAttr("readonly");
                $('#subsidy_low_income_text').removeAttr("readonly");
                $('#subsidy_disability_text').removeAttr("readonly");
                $('#subsidy_decree_text').removeAttr("readonly");
                $('#lease_nr_sr_city_text').removeAttr("readonly");
            } else {
                $("input[name='receive_subsidy']").val(0)
                $('#subsidy_rent_checkbox').prop('checked', false);
                $('#subsidy_low_income_checkbox').prop('checked', false);
                $('#subsidy_disability_checkbox').prop('checked', false);
                $('#subsidy_decree_checkbox').prop('checked', false);
                $('#lease_nr_sr_city_checkbox').prop('checked', false);
                $('#subsidy_rent_text').attr("readonly", " readonly");
                $('#subsidy_low_income_text').attr("readonly", " readonly");
                $('#subsidy_disability_text').attr("readonly", " readonly");
                $('#subsidy_decree_text').attr("readonly", " readonly");
                $('#lease_nr_sr_city_text').attr("readonly", " readonly");
            }
        });

        // if ($('#subsidy_rent_checkbox').is(":checked") == false) {
        //     $('#subsidy_rent_text').attr("readonly", " readonly");
        // }
        // if ($('#subsidy_low_income_checkbox').is(":checked") == false) {
        //     $('#subsidy_low_income_text').attr("readonly", " readonly");
        // }
        // if ($('#subsidy_disability_checkbox').is(":checked") == false) {
        //     $('#subsidy_disability_text').attr("readonly", " readonly");
        // }
        // if ($('#subsidy_decree_checkbox').is(":checked") == false) {
        //     $('#subsidy_decree_text').attr("readonly", " readonly");
        // }
        // if ($('#lease_nr_sr_city_checkbox').is(":checked") == false) {
        //     $('#lease_nr_sr_city_text').attr("readonly", " readonly");
        // }

        $('#subsidy_rent_checkbox').on('change', function(e) {
            console.log("嗨");
            if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
                if ($('#subsidy_rent_checkbox').is(":checked") == false) {
                    $('#subsidy_rent_text').attr("readonly", " readonly");
                } else {
                    $('#subsidy_rent_text').removeAttr("readonly");
                }
            }
        });
        $('#subsidy_low_income_checkbox').on('change', function(e) {
            if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
                if ($('#subsidy_low_income_checkbox').is(":checked") == false) {
                    $('#subsidy_low_income_text').attr("readonly", " readonly");
                } else {
                    $('#subsidy_low_income_text').removeAttr("readonly");
                }
            }
        });
        $('#subsidy_disability_checkbox').on('change', function(e) {
            if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
                if ($('#subsidy_disability_checkbox').is(":checked") == false) {
                    $('#subsidy_disability_text').attr("readonly", " readonly");
                } else {
                    $('#subsidy_disability_text').removeAttr("readonly");
                }
            }
        });
        $('#subsidy_decree_checkbox').on('change', function(e) {
            if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
                if ($('#subsidy_decree_checkbox').is(":checked") == false) {
                    $('#subsidy_decree_text').attr("readonly", " readonly");
                } else {
                    $('#subsidy_decree_text').removeAttr("readonly");
                }
            }
        });
        $('#lease_nr_sr_city_checkbox').on('change', function(e) {
            if ($("input[name='receive_subsidy_radio']:checked").val() == 1) {
                if ($('#lease_nr_sr_city_checkbox').is(":checked") == false) {
                    $('#lease_nr_sr_city_text').attr("readonly", " readonly");
                } else {
                    $('#lease_nr_sr_city_text').removeAttr("readonly");
                }
            }
        });
    </script>
@endpush

@push('scripts')
    <script src="https://unpkg.com/vue@next"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.23.0/axios.min.js"
        integrity="sha512-Idr7xVNnMWCsgBQscTSCivBNWWH30oo/tzYORviOCrLKmBaRxRflm2miNhTFJNVmXvCtzgms5nlJF4az2hiGnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        const {
            onMounted,
            reactive,
            ref,
            watch
        } = Vue;
        const App = {
            setup() {
                const hrTwZip = reactive({
                    city: [],
                    area: [],
                    road: []
                });
                const contactTwZip = reactive({
                    city: [],
                    area: [],
                    road: []
                });
                const hr_city_active = ref('');
                const hr_cityarea_active = ref('');
                const contact_city_active = ref('');
                const contact_cityarea_active = ref('');
                @if (isset($tenantManagement))
                    const dbData = reactive({
                    "hr_city": '{{ $tenantManagement->hr_city }}',
                    "hr_cityarea": '{{ $tenantManagement->hr_cityarea }}',
                    "contact_city": '{{ $tenantManagement->contact_city }}',
                    "contact_cityarea": '{{ $tenantManagement->contact_cityarea }}',
                    "agent_bank_name": '{{ $tenantManagement->bank_name }}',
                    "agent_branch_name": '{{ $tenantManagement->bank_code }}',
                    "lessee_telephone_d": '{{ $tenantManagement->lessee_telephone_d }}',
                    "lessee_telephone_n": '{{ $tenantManagement->lessee_telephone_n }}',
                    "first_agent_telephone": '{{ $tenantManagement->first_agent_telephone }}',
                    "second_agent_telephone": '{{ $tenantManagement->second_agent_telephone }}',
                    "lessee_cellphone": '{{ $tenantManagement->lessee_cellphone }}',
                    "first_agent_cellphone": '{{ $tenantManagement->first_agent_cellphone }}',
                    "second_agent_cellphone": '{{ $tenantManagement->second_agent_cellphone }}',
                    "lessee_id_num": '{{ $tenantManagement->lessee_id_num }}',
                    "lessee_hc_num": '{{ $tenantManagement->lessee_hc_num }}',
                    });
                @else
                    const dbData = reactive({
                    "hr_city": '',
                    "hr_cityarea": '',
                    "contact_city": '',
                    "contact_cityarea": '',
                    "agent_bank_name": '',
                    "agent_branch_name": '',
                    "lessee_telephone_d": '',
                    "lessee_telephone_n": '',
                    "first_agent_telephone": '',
                    "second_agent_telephone": '',
                    "lessee_cellphone": '',
                    "first_agent_cellphone": '',
                    "second_agent_cellphone": '',
                    "lessee_id_num": '',
                    "lessee_hc_num": '',
                    });
                @endif

                watch(hr_city_active, (newCity) => {
                    const newArr = hrTwZip.city.filter((city) => city.name === newCity);
                    hrTwZip.area = newArr[0].area;
                });

                watch(contact_city_active, (newCity) => {
                    const newArr = contactTwZip.city.filter((city) => city.name === newCity);
                    contactTwZip.area = newArr[0].area;
                });

                // 銀行部分
                const bankCode = reactive({
                    value: []
                });
                const agent_bank_name_active = ref('');
                const agent_branch_name = ref('');


                watch(agent_bank_name_active, (newBank) => {
                    const newArr = bankCode.value.filter((bank) => bank[2] === newBank);
                    agent_branch_name.value = newArr[0][0];
                });

                // 表單驗證
                const lessee_telephone_d = ref(dbData.lessee_telephone_d);
                const lessee_telephone_n = ref(dbData.lessee_telephone_n);
                const lessee_cellphone = ref(dbData.lessee_cellphone);
                const first_agent_telephone = ref(dbData.first_agent_telephone);
                const second_agent_telephone = ref(dbData.second_agent_telephone);
                const first_agent_cellphone = ref(dbData.first_agent_cellphone);
                const second_agent_cellphone = ref(dbData.second_agent_cellphone);
                const lessee_id_num = ref(dbData.lessee_id_num);
                const lessee_hc_num = ref(dbData.lessee_hc_num);
                const inputFocus = ref(false);

                watch(inputFocus, (newInputFocus) => {
                    if (newInputFocus.value === true) {
                        return;
                    }
                    // 檢查家電號碼
                    if (!/^0\d{1,3}-\d{6,8}(#\d{1,3})?$/.test(lessee_telephone_d.value)) {
                        lessee_telephone_d.value = "";
                    }
                    // 檢查家電號碼
                    if (!/^0\d{1,3}-\d{6,8}(#\d{1,3})?$/.test(lessee_telephone_n.value)) {
                        lessee_telephone_n.value = "";
                    }
                    // 檢查家電號碼
                    if (!/^0\d{1,3}-\d{6,8}(#\d{1,3})?$/.test(first_agent_telephone.value)) {
                        first_agent_telephone.value = "";
                    }
                    // 檢查家電號碼
                    if (!/^0\d{1,3}-\d{6,8}(#\d{1,3})?$/.test(second_agent_telephone.value)) {
                        second_agent_telephone.value = "";
                    }
                    // 檢查手機號碼
                    if (!/^09\d{8}$/.test(lessee_cellphone.value)) {
                        lessee_cellphone.value = "";
                    }
                    // 檢查手機號碼
                    if (!/^09\d{8}$/.test(first_agent_cellphone.value)) {
                        first_agent_cellphone.value = "";
                    }
                    // 檢查手機號碼
                    if (!/^09\d{8}$/.test(second_agent_cellphone.value)) {
                        second_agent_cellphone.value = "";
                    }
                    // 檢查身分證字號
                    if (!/^[A-Za-z][12]\d{8}$/.test(lessee_id_num.value)) {
                        lessee_id_num.value = "";
                    }
                    // 檢查戶口名簿號
                    if (!/^[A-Za-z][0-9A-Za-z]{6}\d{1}$/.test(lessee_hc_num
                            .value)) {
                        lessee_hc_num.value = "";
                    }
                });

                onMounted(() => {
                    axios.get("/json/twZip.json").then((res) => {
                        hrTwZip.city = res.data;
                        // 設定資料庫內的戶籍地址縣市資料
                        hr_city_active.value = dbData.hr_city;
                        // 設定資料庫內的戶籍地址鄉鎮區資料
                        if (dbData.hr_city) {
                            hrTwZip.area = hrTwZip.city.filter((city) => city.name === dbData.hr_city)[0]
                            .area;
                        } else {
                            hrTwZip.area = '';
                        }
                        hr_cityarea_active.value = dbData.hr_cityarea;

                        contactTwZip.city = res.data;
                        // 設定資料庫內的建物坐落縣市資料
                        contact_city_active.value = dbData.contact_city;
                        // 設定資料庫內的建物坐落鄉鎮區資料
                        contactTwZip.area = contactTwZip.city.filter((city) => city.name === dbData
                            .contact_city)[0].area;
                        contact_cityarea_active.value = dbData.contact_cityarea;
                    });

                    axios.get("/json/bankCode.json").then((res) => {
                        bankCode.value = res.data;
                        agent_bank_name_active.value = dbData.agent_bank_name;
                        agent_branch_name.value = dbData.agent_branch_name;
                    });
                });


                // 上傳檔案
                const file_arr = reactive({
                    'tenant_application': [],
                    'identity': [],
                    'household': [],
                    'bank_account': [],
                    'proof': [],
                    'comprehensive': [],
                    'property': [],
                    'abandonment': [],
                    'other': []
                });
                const temp_file = reactive({});
                const file_json = ref(null);
                const upload_file = (e, name) => {
                    temp_file[name] = e.target.files[0].name;

                }
                const add_file = (name) => {
                    if (temp_file[name] != null) {
                        file_arr[name].push(temp_file[name]);
                        file_json.value = JSON.stringify(file_arr);
                        alert('上傳成功');
                    }
                }

                return {
                    hrTwZip,
                    contactTwZip,
                    dbData,
                    hr_city_active,
                    hr_cityarea_active,
                    contact_city_active,
                    contact_cityarea_active,
                    bankCode,
                    agent_bank_name_active,
                    agent_branch_name,
                    lessee_telephone_d,
                    lessee_telephone_n,
                    first_agent_telephone,
                    second_agent_telephone,
                    lessee_cellphone,
                    first_agent_cellphone,
                    second_agent_cellphone,
                    inputFocus,
                    lessee_id_num,
                    lessee_hc_num,
                    upload_file,
                    file_json,
                    add_file,
                };
            }
        }

        Vue.createApp(App).mount("#app");
    </script>
@endpush

@push('scripts')
    <script src="/js/bootstrap-datepicker.js"></script>
    <script src="/js/bootstrap-datepicker.zh-TW.min.js"></script>
    <script type="text/javascript">
        var apply_date = "{{@$tenantManagement}}";
        if (apply_date) {
            $("#apply_date").datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true
            }).on("show", function(e) {
                $("div.box").css({
                    minHeight: "480px"
                });
            }).on("hide", function(e) {
                $("div.box").css({
                    minHeight: "auto"
                });
            });
        }else{
            var dateNative = new Date(),
                dateTW = new Date(
                    dateNative.getFullYear() - 1911,
                    dateNative.getMonth(),
                    dateNative.getDate()
                );
            $("#apply_date").datepicker( "setDate" , dateTW);
        }
    </script>
@endpush

@push('scripts')
    @once
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
        <script>
            (function() {
                @if(isset($show))
                $("input").attr("disabled", true);
                $("select").attr("disabled", true);
                @endif
                var yearTextSelector = '.ui-datepicker-year';

                var dateNative = new Date(),
                    dateTW = new Date(
                        dateNative.getFullYear() - 1911,
                        dateNative.getMonth(),
                        dateNative.getDate()
                    );


                function leftPad(val, length) {
                    var str = '' + val;
                    while (str.length < length) {
                        str = '0' + str;
                    }
                    return str;
                }

                // 應該有更好的做法
                var funcColle = {
                    onSelect: {
                        basic: function(dateText, inst) {
                            /*
                            var yearNative = inst.selectedYear < 1911
                                ? inst.selectedYear + 1911 : inst.selectedYear;*/
                            dateNative = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

                            // 年分小於100會被補成19**, 要做例外處理
                            var yearTW = inst.selectedYear > 1911 ?
                                leftPad(inst.selectedYear - 1911, 4) :
                                inst.selectedYear;
                            var monthTW = leftPad(inst.selectedMonth + 1, 2);
                            var dayTW = leftPad(inst.selectedDay, 2);
                            console.log(monthTW);
                            dateTW = new Date(
                                yearTW + '-' +
                                monthTW + '-' +
                                dayTW + 'T00:00:00.000Z'
                            );
                            console.log(dateTW);
                            return $.datepicker.formatDate(twSettings.dateFormat, dateTW);
                        }
                    }
                };

                var twSettings = {
                    closeText: '關閉',
                    prevText: '上個月',
                    nextText: '下個月',
                    currentText: '今天',
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'
                    ],
                    dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                    dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                    dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                    weekHeader: '周',
                    dateFormat: 'yy/mm/dd',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: true,
                    yearSuffix: '年',
                    yearRange: "1912:2200",

                    onSelect: function(dateText, inst) {
                        $(this).val(funcColle.onSelect.basic(dateText, inst));
                        if (typeof funcColle.onSelect.newFunc === 'function') {
                            funcColle.onSelect.newFunc(dateText, inst);
                        }
                    }
                };

                // 把yearText換成民國
                var replaceYearText = function() {
                    var $yearText = $('.ui-datepicker-year');
                    if (twSettings.changeYear !== true) {
                        $yearText.text('民國' + dateTW.getFullYear());
                    } else {
                        // 下拉選單
                        if ($yearText.prev('span.datepickerTW-yearPrefix').length === 0) {
                            $yearText.before("<span class='datepickerTW-yearPrefix'>民國</span>");
                        }
                        $yearText.children().each(function() {
                            if (parseInt($(this).text()) > 1911) {
                                $(this).text((parseInt($(this).text()) - 1911).toString().padStart(3, '0'));
                            }
                        });
                    }
                };

                $.fn.datepickerTW = function(options) {

                    // setting on init,
                    if (typeof options === 'object') {
                        //onSelect例外處理, 避免覆蓋
                        if (typeof options.onSelect === 'function') {
                            funcColle.onSelect.newFunc = options.onSelect;
                            options.onSelect = twSettings.onSelect;
                        }
                        // year range正規化成西元, 小於1911的數字都會被當成民國年
                        if (options.yearRange) {
                            var temp = options.yearRange.split(':');
                            for (var i = 0; i < temp.length; i += 1) {
                                //民國前處理
                                if (parseInt(temp[i]) < 1) {
                                    temp[i] = parseInt(temp[i]) + 1911;
                                } else {
                                    temp[i] = parseInt(temp[i]) < 1911 ?
                                        parseInt(temp[i]) + 1911 :
                                        temp[i];
                                }
                            }
                            options.yearRange = temp[0] + ':' + temp[1];
                        }
                        // if input val not empty
                        if ($(this).val() !== '') {
                            options.defaultDate = $(this).val();
                        }
                    }

                    // setting after init
                    if (arguments.length > 1) {
                        // 目前還沒想到正常的解法, 先用轉換成init setting obj的形式
                        if (arguments[0] === 'option') {
                            options = {};
                            options[arguments[1]] = arguments[2];
                        }
                    }

                    // override settings
                    $.extend(twSettings, options);

                    // init
                    $(this).datepicker(twSettings);

                    // beforeRender
                    $(this).click(function() {
                        var isFirstTime = ($(this).val() === '');

                        // year range and default date

                        if ((twSettings.defaultDate || twSettings.yearRange) && isFirstTime) {

                            if (twSettings.defaultDate) {
                                $(this).datepicker('setDate', twSettings.defaultDate);
                            }

                            // 當有year range時, select初始化設成range的最末年
                            if (twSettings.yearRange) {
                                var $yearSelect = $('.ui-datepicker-year'),
                                    nowYear = twSettings.defaultDate ?
                                    $(this).datepicker('getDate').getFullYear() :
                                    dateNative.getFullYear();

                                $yearSelect.children(':selected').removeAttr('selected');
                                if ($yearSelect.children('[value=' + nowYear + ']').length > 0) {
                                    $yearSelect.children('[value=' + nowYear + ']').attr('selected',
                                        'selected');
                                } else {
                                    $yearSelect.children().last().attr('selected', 'selected');
                                }
                            }
                        } else {
                            @if(!isset($tenantManagement))
                                $(this).datepicker('setDate', dateNative);
                            @endif
                        }
                        @if(!isset($tenantManagement))
                            $(this).val($.datepicker.formatDate(twSettings.dateFormat, dateTW));
                        @endif
                        replaceYearText();

                        if (isFirstTime) {
                            $(this).val('');
                        }
                    });

                    // afterRender
                    $(this).focus(function() {
                        replaceYearText();
                    });

                    return this;
                };

            })();


            $('.datepickerTW').datepickerTW({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '-10:2018',
                // defaultDate: '86-11-01',

            });
            $('.datepicker').datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                constrainInput: false
                // yearRange: '1911:2018',
                // defaultDate: '2016-11-01'
            });
            $('#sublet').change(function () {
                if ($(this).val() === '0') {
                    $('#rent').val(1)
                    $('#rent').attr('disabled', true);
                }else {
                    $('#rent').val(0)
                    $('#rent').attr('disabled', true);
                }
            })
            $('#rent').change(function () {
                if ($(this).val() === '0') {
                    $('#sublet').val(1)
                    $('#sublet').attr('disabled', true);
                }else {
                    $('#sublet').val(0)
                    $('#sublet').attr('disabled', true);
                }
            })
            $('.maxlength_10').attr('maxlength', 10);

            $("#same_address").click(function() {
                if ($("#same_address").prop("checked")) {
                    $("input[name='contact_city']").val($(
                        "input[name='hr_city']").val());
                    $("input[name='contact_cityarea']").val($(
                        "input[name='hr_cityarea']").val());
                    $("input[name='contact_address']").val($(
                        "input[name='hr_address']").val());
                } else {
                    $(".residence_address_area").css("display", "block");
                }
            });
        </script>
    @endonce
@endpush
