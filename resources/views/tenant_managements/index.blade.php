@extends('layouts.app')
@section('css')
    <style>
        #shadow-form {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
            height: 100%;
            width: 100%;
            display: grid;
            place-items: center;
            background: rgba(0,0,0,.4)
        }
        #shadow-form form {
            max-width: 800px;
            width: 85vw;
            margin: 4rem 0;
            padding: 1.5rem;
            overflow: auto;
            background: #f6f9fa;
            border-radius: 16px;
        }
        .shadow {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
            height: 100%;
            width: 100%;
            display: grid;
            place-items: center;
            background: rgba(0,0,0,.4)
        }
        .shadow form{
            max-width: 800px;
            width: 85vw;
            margin: 4rem 0;
            padding: 1.5rem;
            overflow: auto;
            background: #f6f9fa;
            border-radius: 16px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">@lang('models/tenantManagements.plural')</h3>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;z-index: 100" href="{{ route('tenantManagements.create') }}">@lang('crud.add_new')</a>
        <button class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;z-index: 100; margin-right: 5px;" onclick="showForm()">租客廣播</button>
    </div>
    <div class="container-fluid container-fixed-lg" style="margin-top: 40px;">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <form action="" id="importFile" method="post" enctype="multipart/form-data">
                    @csrf
                    <div id="app">
                        <button type="button" class="btn btn" onclick="select_file()">選擇檔案</button>
                        <input type="hidden" name="type" value="tenant">
                        <input type="file" name="file" class="hide" id="import_file">
                        <button class="btn btn" type="submit">導入</button>
                    </div>
                </form>
                    @include('tenant_managements.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
    <div id="shadow-form" style="display: none">
        {!! Form::open(['url' => 'lineSendMessage', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'messageForm']) !!}
        <div class="row">
            <div class="form-group col-12">
                <label for="message" style="min-width: 100%; min-height: 300px">訊息內容
                <textarea class="form-control" name="message" id="message" style="min-width: 100%; min-height: 300px"></textarea>
                </label>
            </div>
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                <a href="{{ route('tenantManagements.index') }}" class="btn btn-default float-right">@lang('crud.cancel')</a>
                {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary float-right']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection


@push("scripts")
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>

        function qualifications_render(data,type,full,meta){
         
            switch (data) {
                case "1":
                    return "一般戶";
                    break;
                case "2":
                    return "第一類弱勢戶";
                break;
                case "3":
                    return "第二類弱勢戶";
                break;
            
                default:
                    return "未知";
                    break;
            }
            
        }

        function showForm() {
            $('#shadow-form').show();
        }
        $('#messageForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                timeout: 60000,
                url: "/api/line/send/message",
                data: {
                    'message': $('#message').val()
                },
                success: function (data) {
                    Swal.fire({
                        icon: 'success',
                        title: '廣播成功'
                    })
                    $('#shadow-form').hidden();
                },
                error: function (data) {
                    Swal.fire({
                        icon: 'error',
                        title: '廣播失敗',
                    })
                },
            });
        })

        $('#importFile').on('submit', function (e) {
            e.preventDefault();
            const formData = new FormData(this);
            $.ajax({
                type: "POST",
                timeout: 60000,
                url: "/api/batch_upload",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    const html =
                    `<table class="table table-responsive" style="max-height: 180px; overflow: auto">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Case</th>
                                <th scope="col">Result</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${ data.map(row => `
                                <tr>
                                    <th scope="row">${row.line}</th>
                                    <td>${row.case_num}</td>
                                    <td>${row.result}</td>
                                </tr>
                            `)}
                        </tbody>
                    </table>`
                    Swal.fire({
                        icon: 'success',
                        title: '匯入成功',
                        html,
                    })
                },
                error: function (data) {
                    Swal.fire({
                        icon: 'error',
                        title: '導入失敗 請確認檔案',
                    })
                },
            });
        })



        function select_file() {
            $('#import_file').val("");
            $('#import_file').click();
        }

    </script>
@endpush
