<div class="row">
<!-- Tenant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tenant_id', __('models/tenantMinorOffsprings.fields.tenant_id').':') !!}
    {!! Form::number('tenant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/tenantMinorOffsprings.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Foreigner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_foreigner', __('models/tenantMinorOffsprings.fields.is_foreigner').':') !!}
    {!! Form::number('is_foreigner', null, ['class' => 'form-control']) !!}
</div>

<!-- Appellation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('appellation', __('models/tenantMinorOffsprings.fields.appellation').':') !!}
    {!! Form::text('appellation', null, ['class' => 'form-control']) !!}
</div>

<!-- Id No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_no', __('models/tenantMinorOffsprings.fields.id_no').':') !!}
    {!! Form::text('id_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', __('models/tenantMinorOffsprings.fields.birthday').':') !!}
    {!! Form::date('birthday', null, ['class' => 'form-control','id'=>'birthday']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#birthday').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Filers Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filers', __('models/tenantMinorOffsprings.fields.filers').':') !!}
    {!! Form::number('filers', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Modified Person Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_modified_person', __('models/tenantMinorOffsprings.fields.last_modified_person').':') !!}
    {!! Form::number('last_modified_person', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
