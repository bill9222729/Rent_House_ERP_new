<!-- Tenant Id Field -->
<div class="form-group col-12">
    {!! Form::label('tenant_id', __('models/tenantMinorOffsprings.fields.tenant_id').':') !!}
    <p>{{ $tenantMinorOffspring->tenant_id }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/tenantMinorOffsprings.fields.name').':') !!}
    <p>{{ $tenantMinorOffspring->name }}</p>
</div>


<!-- Is Foreigner Field -->
<div class="form-group col-12">
    {!! Form::label('is_foreigner', __('models/tenantMinorOffsprings.fields.is_foreigner').':') !!}
    <p>{{ $tenantMinorOffspring->is_foreigner }}</p>
</div>


<!-- Appellation Field -->
<div class="form-group col-12">
    {!! Form::label('appellation', __('models/tenantMinorOffsprings.fields.appellation').':') !!}
    <p>{{ $tenantMinorOffspring->appellation }}</p>
</div>


<!-- Id No Field -->
<div class="form-group col-12">
    {!! Form::label('id_no', __('models/tenantMinorOffsprings.fields.id_no').':') !!}
    <p>{{ $tenantMinorOffspring->id_no }}</p>
</div>


<!-- Birthday Field -->
<div class="form-group col-12">
    {!! Form::label('birthday', __('models/tenantMinorOffsprings.fields.birthday').':') !!}
    <p>{{ $tenantMinorOffspring->birthday }}</p>
</div>


<!-- Filers Field -->
<div class="form-group col-12">
    {!! Form::label('filers', __('models/tenantMinorOffsprings.fields.filers').':') !!}
    <p>{{ $tenantMinorOffspring->filers }}</p>
</div>


<!-- Last Modified Person Field -->
<div class="form-group col-12">
    {!! Form::label('last_modified_person', __('models/tenantMinorOffsprings.fields.last_modified_person').':') !!}
    <p>{{ $tenantMinorOffspring->last_modified_person }}</p>
</div>


