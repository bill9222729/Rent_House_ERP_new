<div class="row">
<!-- Account Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account', __('models/transactionDetails.fields.account').':') !!}
    {!! Form::text('account', null, ['class' => 'form-control']) !!}
</div>

<!-- T Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('t_date', __('models/transactionDetails.fields.t_date').':') !!}
    {!! Form::text('t_date', null, ['class' => 'form-control','id'=>'t_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#t_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- T Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('t_time', __('models/transactionDetails.fields.t_time').':') !!}
    {!! Form::text('t_time', null, ['class' => 'form-control','id'=>'t_time']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#t_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- A Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('a_date', __('models/transactionDetails.fields.a_date').':') !!}
    {!! Form::text('a_date', null, ['class' => 'form-control','id'=>'a_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#a_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Detail Field -->
<div class="form-group col-sm-6">
    {!! Form::label('detail', __('models/transactionDetails.fields.detail').':') !!}
    {!! Form::text('detail', null, ['class' => 'form-control']) !!}
</div>

<!-- Expense Field -->
<div class="form-group col-sm-6">
    {!! Form::label('expense', __('models/transactionDetails.fields.expense').':') !!}
    {!! Form::number('expense', null, ['class' => 'form-control']) !!}
</div>

<!-- Income Field -->
<div class="form-group col-sm-6">
    {!! Form::label('income', __('models/transactionDetails.fields.income').':') !!}
    {!! Form::number('income', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('balance', __('models/transactionDetails.fields.balance').':') !!}
    {!! Form::number('balance', null, ['class' => 'form-control']) !!}
</div>

<!-- Remark 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remark_1', __('models/transactionDetails.fields.remark_1').':') !!}
    {!! Form::text('remark_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Remark 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remark_2', __('models/transactionDetails.fields.remark_2').':') !!}
    {!! Form::text('remark_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Remark 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remark_3', __('models/transactionDetails.fields.remark_3').':') !!}
    {!! Form::text('remark_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Ticket Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ticket_num', __('models/transactionDetails.fields.ticket_num').':') !!}
    {!! Form::text('ticket_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('transactionDetails.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
