<!-- Account Field -->
<div class="form-group col-12">
    {!! Form::label('account', __('models/transactionDetails.fields.account').':') !!}
    <p>{{ $transactionDetails->account }}</p>
</div>


<!-- T Date Field -->
<div class="form-group col-12">
    {!! Form::label('t_date', __('models/transactionDetails.fields.t_date').':') !!}
    <p>{{ $transactionDetails->t_date }}</p>
</div>


<!-- T Time Field -->
<div class="form-group col-12">
    {!! Form::label('t_time', __('models/transactionDetails.fields.t_time').':') !!}
    <p>{{ $transactionDetails->t_time }}</p>
</div>


<!-- A Date Field -->
<div class="form-group col-12">
    {!! Form::label('a_date', __('models/transactionDetails.fields.a_date').':') !!}
    <p>{{ $transactionDetails->a_date }}</p>
</div>


<!-- Detail Field -->
<div class="form-group col-12">
    {!! Form::label('detail', __('models/transactionDetails.fields.detail').':') !!}
    <p>{{ $transactionDetails->detail }}</p>
</div>


<!-- Expense Field -->
<div class="form-group col-12">
    {!! Form::label('expense', __('models/transactionDetails.fields.expense').':') !!}
    <p>{{ $transactionDetails->expense }}</p>
</div>


<!-- Income Field -->
<div class="form-group col-12">
    {!! Form::label('income', __('models/transactionDetails.fields.income').':') !!}
    <p>{{ $transactionDetails->income }}</p>
</div>


<!-- Balance Field -->
<div class="form-group col-12">
    {!! Form::label('balance', __('models/transactionDetails.fields.balance').':') !!}
    <p>{{ $transactionDetails->balance }}</p>
</div>


<!-- Remark 1 Field -->
<div class="form-group col-12">
    {!! Form::label('remark_1', __('models/transactionDetails.fields.remark_1').':') !!}
    <p>{{ $transactionDetails->remark_1 }}</p>
</div>


<!-- Remark 2 Field -->
<div class="form-group col-12">
    {!! Form::label('remark_2', __('models/transactionDetails.fields.remark_2').':') !!}
    <p>{{ $transactionDetails->remark_2 }}</p>
</div>


<!-- Remark 3 Field -->
<div class="form-group col-12">
    {!! Form::label('remark_3', __('models/transactionDetails.fields.remark_3').':') !!}
    <p>{{ $transactionDetails->remark_3 }}</p>
</div>


<!-- Ticket Num Field -->
<div class="form-group col-12">
    {!! Form::label('ticket_num', __('models/transactionDetails.fields.ticket_num').':') !!}
    <p>{{ $transactionDetails->ticket_num }}</p>
</div>


