@extends('layouts.app')

@section('content')
    {{-- <form id="user_edit_form"> --}}
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">員工管理-編輯</h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="accordion" id="accordionPanelsStayOpenExample">
            @include('users.fields')
        </div>
    </div>

    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            @can('admin')
                <div class="col-sm-auto">
                    <button type="text" class="btn btn-primary" id="save">儲存</button>
                </div>
                @if ($user->lock == 1)
                    <div class="col-sm-auto">
                        <button type="text" class="btn btn-danger" id="unlock_user">解除鎖定</button>
                    </div>
                @else
                    <div class="col-sm-auto">
                        <button type="text" class="btn btn-danger" id="lock_user">鎖定資料</button>
                    </div>
                @endif
            @elseif (Auth::user()->lock == 0)
                <div class="col-sm-auto">
                    <button type="text" class="btn btn-primary" id="save">儲存</button>
                </div>
            @endcannot
            <div class="col-sm-auto">
                <a href="/users" class="btn btn-secondary">取消</a>
            </div>
        </div>
    </div>
    {{-- </form> --}}
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/users.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const userData = @json($user);
        // 初始化頁面元件
        initPageElement();
        // 初始化fileInput
        initFileInput("user", userData["id"], "user_file_input");
        // 把資料庫的資料放到畫面上
        initUserField(userData);
        // 綁定儲存按鈕的事件
        $("#save").on("click", () => {
            updateUserData(userData["id"])
        });
        // 綁定鎖定按鈕
        $("#lock_user").on("click", () => {
            controlUserLock(userData, true)
        });
        // 綁定解鎖案紐
        $("#unlock_user").on("click", () => {
            controlUserLock(userData, false)
        });
    </script>
@endpush
