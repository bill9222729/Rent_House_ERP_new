{{-- 編輯房客承租申請 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
            編輯房客承租申請
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-5">
                    <div class="row col-md-6">
                        <label for="name" class="col-md-12 col-form-label text-nowrap fw-bolder">姓名</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="passport_english_name" class="col-md-12 col-form-label text-nowrap fw-bolder">英文護照姓名</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="passport_english_name" id="passport_english_name">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="english_name" class="col-md-12 col-form-label text-nowrap fw-bolder">英文姓名</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="english_name" id="english_name">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label" for="birthday">出生年月日</label>
                        <div class="col-md-12">
                            <div class="input-group date day">
                                <input type="text" name="birthday" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="birthday">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon4">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label class="col-md-12 col-form-label" for="arrival_date">到職日</label>
                        <div class="col-md-12">
                            <div class="input-group date day">
                                <input type="text" name="arrival_date" value="" class="form-control" autocomplete="off" maxlength="10" size="10" placeholder="YYY-MM-DD" id="arrival_date">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon4">
                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3 CitySelector" data-param='{"title":"*戶籍地址","required":true,"city_select_id":"residence_city","city_area_select_id":"residence_city_area","addr_input_id":"residence_addr", "page_data":{{ $tenantManagementNew ?? '{}' }}, "is_modal":{{ isset($isModal) ? 'true' : 'false' }}}'>
                </div>
                <div class="row mb-3 CitySelector" data-param='{"title":"*通訊地址","required":true,"city_select_id":"mailing_city","city_area_select_id":"mailing_city_area","addr_input_id":"mailing_addr", "page_data":{{ $tenantManagementNew ?? '{}' }}, "is_modal":{{ isset($isModal) ? 'true' : 'false' }}, "ditto" : "true"}'>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="tel" class="col-md-12 col-form-label text-nowrap fw-bolder">市話電話</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="tel" id="tel">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="email" class="col-md-12 col-form-label text-nowrap fw-bolder">電子信箱</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="email" id="email">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="cellphone" class="col-md-12 col-form-label text-nowrap fw-bolder">手機號碼</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="cellphone" id="cellphone">
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="company_cellphone" class="col-md-12 col-form-label text-nowrap fw-bolder">公務機號碼</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="company_cellphone" id="company_cellphone">
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-6">
                        <label for="role" class="col-md-12 col-form-label text-nowrap fw-bolder">權限</label>
                        <div class="col-md-12">
                            <select class="form-select form-control" name="role" id="role">
                                <option value="guest">訪客</option>
                                <option value="sales">業務</option>
                                <option value="admin">管理員</option>
                                <option value="staff">內勤</option>
                                <option value="accounting">會計</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <label for="is_sales" class="col-md-12 col-form-label text-nowrap fw-bolder">是否為業務</label>
                        <div class="col-md-12">
                            <select class="form-select form-control" name="is_sales" id="is_sales">
                                <option value="1">是</option>
                                <option value="2">否</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="row col-md-12 align-items-center">
                        <label class="col-md-12 col-form-label fw-bolder" for="proxy_name_1">銀行帳戶</label>
                        <div class="row col-md-12 bankSelector" id="bankSelector" data-param="{{ isset($user) ? $user : '{}' }}"></div>
                    </div>
                </div>

                {{-- 隱藏的資料 --}}
                <input type="hidden" name="lock" id="lock" value={{$user->lock}} />
            </div>
        </div>
    </div>
</div>
{{-- 備註 --}}
<div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingEight">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseEight" aria-expanded="true" aria-controls="panelsStayOpen-collapseEight">
            備註
        </button>
    </h2>
    <div id="panelsStayOpen-collapseEight" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingEight">
        <div class="accordion-body">
            <div class="container-fluid">
                <div class="row mb-3 mt-3">
                    <div class="row col-md-12">
                        <label class="col-sm-2 col-form-label text-end fw-bolder text-nowrap" for="note">備註</label>
                        <div class="col-sm-10 align-items-center">
                            <textarea type="text" class="form-control" name="note" id="note"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- 檢附文件 --}}
@if (isset($user))
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                檢附文件
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
            <div class="accordion-body">
                <div class="container-fluid">
                    <div class="file-loading">
                        <input id="user_file_input" name="source" type="file" multiple />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
