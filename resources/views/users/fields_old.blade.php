<div class="row">
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/users.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/users.fields.email').':') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Passport English Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('passport_english_name', __('models/users.fields.passport_english_name').':') !!}
    {!! Form::text('passport_english_name', null, ['class' => 'form-control']) !!}
</div>

<!-- English Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('english_name', __('models/users.fields.english_name').':') !!}
    {!! Form::text('english_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', __('models/users.fields.birthday').':') !!}
    {!! Form::date('birthday', null, ['class' => 'form-control','id'=>'birthday']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#birthday').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Id No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_no', __('models/users.fields.id_no').':') !!}
    {!! Form::text('id_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('residence_city', __('models/users.fields.residence_city').':') !!}
    {!! Form::text('residence_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence City Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('residence_city_area', __('models/users.fields.residence_city_area').':') !!}
    {!! Form::text('residence_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Residenceresidence Addr Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('residenceresidence_addr_addr', __('models/users.fields.residenceresidence_addr_addr').':') !!}
    {!! Form::text('residenceresidence_addr_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mailing_city', __('models/users.fields.mailing_city').':') !!}
    {!! Form::text('mailing_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing City Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mailing_city_area', __('models/users.fields.mailing_city_area').':') !!}
    {!! Form::text('mailing_city_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Mailing Addr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mailing_addr', __('models/users.fields.mailing_addr').':') !!}
    {!! Form::text('mailing_addr', null, ['class' => 'form-control']) !!}
</div>

<!-- Tel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tel', __('models/users.fields.tel').':') !!}
    {!! Form::text('tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Cellphone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cellphone', __('models/users.fields.cellphone').':') !!}
    {!! Form::text('cellphone', null, ['class' => 'form-control']) !!}
</div>

<!-- Company Cellphone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_cellphone', __('models/users.fields.company_cellphone').':') !!}
    {!! Form::text('company_cellphone', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_name', __('models/users.fields.bank_name').':') !!}
    {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Branch Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_branch_code', __('models/users.fields.bank_branch_code').':') !!}
    {!! Form::text('bank_branch_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Owner Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_owner_name', __('models/users.fields.bank_owner_name').':') !!}
    {!! Form::text('bank_owner_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Account Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_account', __('models/users.fields.bank_account').':') !!}
    {!! Form::text('bank_account', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Verified At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_verified_at', __('models/users.fields.email_verified_at').':') !!}
    {!! Form::date('email_verified_at', null, ['class' => 'form-control','id'=>'email_verified_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#email_verified_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', __('models/users.fields.password').':') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role', __('models/users.fields.role').':') !!}
    {!! Form::text('role', null, ['class' => 'form-control']) !!}
</div>

<!-- Remember Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remember_token', __('models/users.fields.remember_token').':') !!}
    {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
</div>

<!-- Stoken Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stoken', __('models/users.fields.stoken').':') !!}
    {!! Form::text('stoken', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="javascript:void(0)" onclick="ajaxCU.close()" class="btn btn-default">@lang('crud.cancel')</a>
</div>
</div>
