@extends('layouts.app')
@section('css')
    <style>
        .hr-line-dashed {
            border-top: 1px dashed #ced4da;
            color: #ffffff;
            background-color: #ffffff;
            height: 1px;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .dropdown-menu {
            z-index: 1000 !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <h3 class="page-title">員工管理</h3>
    </div>
    <div class="container-fluid container-fixed-lg">
        <div class="accordion" id="accordionPanelsStayOpenExample">
            <div class="accordion-item search-accordion">
                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                        data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
                        aria-controls="panelsStayOpen-collapseOne">
                        查詢條件
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
                    aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">
                        <div class="container">
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="name" class="col-md-12 col-form-label text-nowrap">姓名</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-md-12 col-form-label" for="birthday">出生年月日</label>
                                        <div class="col-md-12">
                                            <div class="input-group date day">
                                                <input type="text" name="birthday" value=""
                                                    class="form-control" autocomplete="off" maxlength="10" size="10"
                                                    placeholder="YYY-MM-DD" id="birthday">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon4">
                                                    <i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o"
                                                        data-date-icon="fa fa-fw fa-calendar">&nbsp;</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="cellphone"
                                            class="col-md-12 col-form-label text-nowrap">手機號碼</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" id="cellphone">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label for="company_cellphone"
                                            class="col-md-12 col-form-label text-nowrap">公務機號碼</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" id="company_cellphone">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="row col-md-12">
                                    <div class="row col-md-6">
                                        <label for="email"
                                            class="col-md-12 col-form-label text-nowrap">電子信箱</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" id="email">
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <label class="col-md-12 col-form-label" for="role">類別</label>
                                        <div class="col-md-12">
                                            <select class="form-select form-control" id="role">
                                                <option value="" selected>請選擇</option>
                                                <option value="業務">業務</option>
                                                <option value="內勤">內勤</option>
                                                <option value="會計">會計</option>
                                                <option value="管理員">管理員</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="col-sm-9 col-md-10">
                                <button class="btn btn-primary" type="button" id="search">
                                    查詢
                                </button>
                                <button class="btn btn-outline-secondary" type="reset" id="reset"
                                    onclick="resetCheckBox();">
                                    清除條件
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                        data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true"
                        aria-controls="panelsStayOpen-collapseTwo">
                        查詢結果
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show"
                    aria-labelledby="panelsStayOpen-headingTwo">
                    <div class="accordion-body">
                        <div class="container-fluid">
                            <table id="users_table" class="display nowrap" style="width:100%"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/users.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script>
        $.fn.dataTable.ext.search.push(
            // 生日
            function(settings, data, dataIndex) {
                let birthday = $("#birthday").val();
                let birthday_data = data[1];
                if (birthday === birthday_data || birthday === "") {
                    return true;
                }
                return false;
            },
            // 姓名
            function(settings, data, dataIndex) {
                let name = $("#name").val();
                let name_data = data[0];
                if (name === name_data || name === "") {
                    return true;
                }
                return false;
            },
            // 手機
            function(settings, data, dataIndex) {
                let cellphone = $("#cellphone").val();
                let cellphone_data = data[2];
                if (cellphone == cellphone_data || cellphone === "") {
                    return true;
                }
                return false;
            },
            // 公務機
            function(settings, data, dataIndex) {
                let company_cellphone = $("#company_cellphone").val();
                let company_cellphone_data = data[3];
                if (company_cellphone == company_cellphone_data || company_cellphone === "") {
                    return true;
                }
                return false;
            },
            // 電子信箱
            function(settings, data, dataIndex) {
                let email = $("#email").val();
                let email_data = data[4];
                if (email == email_data || email === "") {
                    return true;
                }
                return false;
            },
            // 類別
            function(settings, data, dataIndex) {
                let role = $("#role").val();
                let role_data = data[5];
                if (role == role_data || role === "" || !role) {
                    return true;
                }
                return false;
            },
        );
        $(document).ready(async function() {
            let salesmanData = await getSalesmanData();
            // 設定搜尋部份的業務選項
            salesmanData.forEach((item) => {
                $("#sales").append($("<option></option>").attr("value", item.name).text(item.name));
            });
            // 初始化日期選擇器
            $('.input-group').datepicker({
                format: "twy-mm-dd",
                language: "zh-TW",
                showOnFocus: false,
                todayHighlight: true,
                todayBtn: "linked",
                clearBtn: true,
            });
            // 初始化datatables
            console.log(usersDatatablesConfig);
            const table = $('#users_table').DataTable(usersDatatablesConfig);

            $('#search').on('click', function() {
                table.draw();
            });
        });
    </script>
@endpush
