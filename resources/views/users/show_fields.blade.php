<!-- Name Field -->
<div class="form-group col-12">
    {!! Form::label('name', __('models/users.fields.name').':') !!}
    <p>{{ $users->name }}</p>
</div>


<!-- Email Field -->
<div class="form-group col-12">
    {!! Form::label('email', __('models/users.fields.email').':') !!}
    <p>{{ $users->email }}</p>
</div>


<!-- Passport English Name Field -->
<div class="form-group col-12">
    {!! Form::label('passport_english_name', __('models/users.fields.passport_english_name').':') !!}
    <p>{{ $users->passport_english_name }}</p>
</div>


<!-- English Name Field -->
<div class="form-group col-12">
    {!! Form::label('english_name', __('models/users.fields.english_name').':') !!}
    <p>{{ $users->english_name }}</p>
</div>


<!-- Birthday Field -->
<div class="form-group col-12">
    {!! Form::label('birthday', __('models/users.fields.birthday').':') !!}
    <p>{{ $users->birthday }}</p>
</div>


<!-- Id No Field -->
<div class="form-group col-12">
    {!! Form::label('id_no', __('models/users.fields.id_no').':') !!}
    <p>{{ $users->id_no }}</p>
</div>


<!-- Residence City Field -->
<div class="form-group col-12">
    {!! Form::label('residence_city', __('models/users.fields.residence_city').':') !!}
    <p>{{ $users->residence_city }}</p>
</div>


<!-- Residence City Area Field -->
<div class="form-group col-12">
    {!! Form::label('residence_city_area', __('models/users.fields.residence_city_area').':') !!}
    <p>{{ $users->residence_city_area }}</p>
</div>


<!-- Residenceresidence Addr Addr Field -->
<div class="form-group col-12">
    {!! Form::label('residenceresidence_addr_addr', __('models/users.fields.residenceresidence_addr_addr').':') !!}
    <p>{{ $users->residenceresidence_addr_addr }}</p>
</div>


<!-- Mailing City Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_city', __('models/users.fields.mailing_city').':') !!}
    <p>{{ $users->mailing_city }}</p>
</div>


<!-- Mailing City Area Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_city_area', __('models/users.fields.mailing_city_area').':') !!}
    <p>{{ $users->mailing_city_area }}</p>
</div>


<!-- Mailing Addr Field -->
<div class="form-group col-12">
    {!! Form::label('mailing_addr', __('models/users.fields.mailing_addr').':') !!}
    <p>{{ $users->mailing_addr }}</p>
</div>


<!-- Tel Field -->
<div class="form-group col-12">
    {!! Form::label('tel', __('models/users.fields.tel').':') !!}
    <p>{{ $users->tel }}</p>
</div>


<!-- Cellphone Field -->
<div class="form-group col-12">
    {!! Form::label('cellphone', __('models/users.fields.cellphone').':') !!}
    <p>{{ $users->cellphone }}</p>
</div>


<!-- Company Cellphone Field -->
<div class="form-group col-12">
    {!! Form::label('company_cellphone', __('models/users.fields.company_cellphone').':') !!}
    <p>{{ $users->company_cellphone }}</p>
</div>


<!-- Bank Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_name', __('models/users.fields.bank_name').':') !!}
    <p>{{ $users->bank_name }}</p>
</div>


<!-- Bank Branch Code Field -->
<div class="form-group col-12">
    {!! Form::label('bank_branch_code', __('models/users.fields.bank_branch_code').':') !!}
    <p>{{ $users->bank_branch_code }}</p>
</div>


<!-- Bank Owner Name Field -->
<div class="form-group col-12">
    {!! Form::label('bank_owner_name', __('models/users.fields.bank_owner_name').':') !!}
    <p>{{ $users->bank_owner_name }}</p>
</div>


<!-- Bank Account Field -->
<div class="form-group col-12">
    {!! Form::label('bank_account', __('models/users.fields.bank_account').':') !!}
    <p>{{ $users->bank_account }}</p>
</div>


<!-- Email Verified At Field -->
<div class="form-group col-12">
    {!! Form::label('email_verified_at', __('models/users.fields.email_verified_at').':') !!}
    <p>{{ $users->email_verified_at }}</p>
</div>


<!-- Password Field -->
<div class="form-group col-12">
    {!! Form::label('password', __('models/users.fields.password').':') !!}
    <p>{{ $users->password }}</p>
</div>


<!-- Role Field -->
<div class="form-group col-12">
    {!! Form::label('role', __('models/users.fields.role').':') !!}
    <p>{{ $users->role }}</p>
</div>


<!-- Remember Token Field -->
<div class="form-group col-12">
    {!! Form::label('remember_token', __('models/users.fields.remember_token').':') !!}
    <p>{{ $users->remember_token }}</p>
</div>


<!-- Stoken Field -->
<div class="form-group col-12">
    {!! Form::label('stoken', __('models/users.fields.stoken').':') !!}
    <p>{{ $users->stoken }}</p>
</div>


