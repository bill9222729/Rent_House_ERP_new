<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::match(['get', 'post'], 'Get_Token_User/{stoken}', '\App\Http\Controllers\AppBaseController@Get_Token_User');

Route::resource('salesman_managements', 'SalesmanManagementAPIController');

Route::resource('landlord_managements', 'LandlordManagementAPIController');



Route::resource('tenant_managements', 'TenantManagementAPIController');

Route::resource('building_managements', 'BuildingManagementAPIController');

Route::post('batch_upload', function (Request $request) {
    error_log(print_r($request->input('type'), true));
    if ($request->input('type') === 'landlord') {
        if ($res = \App\Http\Controllers\BuildingManagementController::processExcel($request->file('file')))
            return response()->json($res);
        else
            return response('error', 400);
    } else if ($request->input('type') === 'tenant') {
        if ($res = \App\Http\Controllers\TenantManagementController::processExcel($request->file('file')))
            return response()->json($res);
        else
            return response('error', 400);
    } else if ($request->input('type') === 'contract') {
        if ($res = \App\Http\Controllers\contractController::processExcel($request->file('file')))
            return response()->json($res);
        else
            return response('error', 400);
    } else if ($request->input('type') === 'banks') {
        if ($res = \App\Http\Controllers\BankController::processExcel($request->file('file')))
            return response()->json($res);
        else
            return response('error', 400);
    } else if ($request->input('type') === 'return_format') {
        if ($res = \App\Http\Controllers\ReturnFormatController::processExcel($request->file('file')))
            return response()->json($res);
        else
            return response('error', 400);
    } else if ($request->input('type') === 'business') {
        if ($res = \App\Http\Controllers\business_managementController::processExcel($request->file('file')))
            return response()->json($res);
        else
            return response('error', 400);
    } else if ($request->input('type') === 'transactionDetails') {
        if ($res = \App\Http\Controllers\TransactionDetailsController::processExcel($request->file('file')))
            return response()->json($res);
        else
            return response('error', 400);
    } else {
        return response('error', 400);
    }
});


Route::resource('transaction_details', 'TransactionDetailsAPIController');
Route::post('/line/webhook', 'LineWebhookController@webhook')->name('line.webhook');
Route::get('/line/createRichMenu', 'LineWebhookController@createRichMenu')->name('line.webhook');
Route::post('/line/send/message', 'LineWebhookController@sendMessage')->name('lineSendMessage');


Route::resource('fix_histories', 'FixHistoryAPIController');

Route::resource('house_pay_histories', 'HousePayHistoryAPIController');


Route::resource('building_management_news', 'building_management_newAPIController');
Route::post('building_management_news/upload', 'building_management_newAPIController@upload');

Route::resource('contracts_news', 'contracts_newAPIController');

Route::resource('tenant_management_news', 'tenant_management_newAPIController');

Route::resource('match_managements', 'match_managementAPIController');

Route::resource('contract_managements', 'contract_managementAPIController');

Route::resource('cities', 'cityAPIController');

Route::resource('city_areas', 'city_areaAPIController');


Route::resource('streets', 'streetAPIController');

Route::resource('located_lots', 'located_lotAPIController');

Route::resource('file_managments', 'file_managmentAPIController');

Route::resource('change_histories', 'change_historyAPIController');

Route::resource('tenant_family_infos', 'tenant_family_infoAPIController');

Route::resource('tenant_minor_offsprings', 'tenant_minor_offspringAPIController');

Route::resource('tenant_building_infos', 'tenant_building_infoAPIController');

Route::resource('insurance_managements', 'insurance_managementAPIController');

Route::resource('fix_managements', 'fix_managementAPIController');

Route::resource('fix_detail_managements', 'fix_detail_managementAPIController');

Route::resource('users', 'usersAPIController');