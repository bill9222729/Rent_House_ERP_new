<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'TokenCheck'])->group(function () {
    Route::get('/home', 'HomeController@index')->middleware('verified');
});

Route::middleware(['logging'])->group(function () {
    Route::resource('salesmanManagements', 'SalesmanManagementController');

    Route::resource('landlordManagements', 'LandlordManagementController');

    Route::resource('tenantManagements', 'TenantManagementController');
    Route::get('/tenantManagements/export/{id}', 'TenantManagementController@generateT');

    Route::resource('businessManagements', 'business_managementController');

    Route::resource('buildingManagements', 'BuildingManagementController');
    Route::get('/buildingManagements/export/{id}', 'BuildingManagementController@generateT');

    Route::resource('contracts', 'contractController');
    Route::get('mixed', 'contractController@mixedTable')->name('mixedTable');

    Route::resource('fixHistories', 'FixHistoryController');

    Route::resource('returnFormats', 'ReturnFormatController');

    Route::resource('banks', 'BankController');

    Route::resource('transactionDetails', 'TransactionDetailsController');

    Route::resource('lineConfigs', 'LineConfigController');

    Route::resource('logHistories', 'LogHistoryController');
});

Route::get('buildmag/download', 'BuildingManagementController@generateBill');

Route::get('returnFormat/download', 'ReturnFormatController@generateData');
Route::get('bank/download', 'BankController@generateData');
Route::get('mixed/download', 'contractController@generateMixedData');

Route::post('import/build', 'BuildingManagementController@import_build');

Route::post('contracts/UploadExcel', 'contractController@UploadExcel');

Route::post('landlordManagements/UploadExcel', 'LandlordManagementController@UploadExcel');

Route::post('tenantManagements/UploadExcel', 'TenantManagementController@UploadExcel');

Route::post('buildingManagements/UploadExcel', 'BuildingManagementController@UploadExcel');

Route::get('batch_upload', function () {
    return view('batch_upload');
});

Route::get('reward', 'HomeController@reward');

Route::get('glogin', array('as' => 'glogin', 'uses' => 'HomeController@googleLogin'));


Route::resource('users', 'UserController');

Route::resource('housePayHistories', 'HousePayHistoryController');


Route::resource('buildingManagementNews', 'building_management_newController');

Route::resource('cities', 'cityController');

Route::resource('cityAreas', 'city_areaController');

Route::resource('streets', 'streetController');

Route::resource('locatedLots', 'located_lotController');

Route::resource('contractsNews', 'contracts_newController');

Route::resource('contractsNews', 'contracts_newController');

Route::resource('contractsNews', 'contracts_newController');

Route::resource('buildingManagementNews', 'building_management_newController');

Route::resource('tenantManagementNews', 'tenant_management_newController');

Route::resource('matchManagements', 'match_managementController');

Route::get('contractManagements/create/{match_id}', 'contract_managementController@create');

Route::resource('contractManagements', 'contract_managementController');

// 以下為練習圖片上傳的玩具
Route::get('img_uploads', 'ImageController@index');
Route::get('images', 'ImageController@show');
Route::get('upload', 'ImageController@store');


Route::resource('cityAreas', 'city_areaController');


Route::resource('locatedLots', 'located_lotController');

Route::resource('fileManagments', 'file_managmentController');

Route::resource('changeHistories', 'change_historyController');

Route::resource('tenantFamilyInfos', 'tenant_family_infoController');

Route::resource('tenantMinorOffsprings', 'tenant_minor_offspringController');

Route::resource('tenantBuildingInfos', 'tenant_building_infoController');


Route::resource('insuranceManagements', 'insurance_managementController');

Route::resource('salesmanManagements', 'salesman_managementController');

Route::resource('insuranceManagements', 'insurance_managementController');


Route::resource('fixManagements', 'fix_managementController');

Route::resource('fixDetailManagements', 'fix_detail_managementController');
