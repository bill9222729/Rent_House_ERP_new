<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BuildingManagement;

class BuildingManagementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/building_managements', $buildingManagement
        );

        $this->assertApiResponse($buildingManagement);
    }

    /**
     * @test
     */
    public function test_read_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/building_managements/'.$buildingManagement->id
        );

        $this->assertApiResponse($buildingManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->create();
        $editedBuildingManagement = factory(BuildingManagement::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/building_managements/'.$buildingManagement->id,
            $editedBuildingManagement
        );

        $this->assertApiResponse($editedBuildingManagement);
    }

    /**
     * @test
     */
    public function test_delete_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/building_managements/'.$buildingManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/building_managements/'.$buildingManagement->id
        );

        $this->response->assertStatus(404);
    }
}
