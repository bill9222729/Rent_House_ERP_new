<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FixHistory;

class FixHistoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/fix_histories', $fixHistory
        );

        $this->assertApiResponse($fixHistory);
    }

    /**
     * @test
     */
    public function test_read_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/fix_histories/'.$fixHistory->id
        );

        $this->assertApiResponse($fixHistory->toArray());
    }

    /**
     * @test
     */
    public function test_update_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->create();
        $editedFixHistory = factory(FixHistory::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/fix_histories/'.$fixHistory->id,
            $editedFixHistory
        );

        $this->assertApiResponse($editedFixHistory);
    }

    /**
     * @test
     */
    public function test_delete_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/fix_histories/'.$fixHistory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/fix_histories/'.$fixHistory->id
        );

        $this->response->assertStatus(404);
    }
}
