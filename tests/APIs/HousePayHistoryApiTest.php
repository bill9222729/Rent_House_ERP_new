<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HousePayHistory;

class HousePayHistoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/house_pay_histories', $housePayHistory
        );

        $this->assertApiResponse($housePayHistory);
    }

    /**
     * @test
     */
    public function test_read_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/house_pay_histories/'.$housePayHistory->id
        );

        $this->assertApiResponse($housePayHistory->toArray());
    }

    /**
     * @test
     */
    public function test_update_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->create();
        $editedHousePayHistory = factory(HousePayHistory::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/house_pay_histories/'.$housePayHistory->id,
            $editedHousePayHistory
        );

        $this->assertApiResponse($editedHousePayHistory);
    }

    /**
     * @test
     */
    public function test_delete_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/house_pay_histories/'.$housePayHistory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/house_pay_histories/'.$housePayHistory->id
        );

        $this->response->assertStatus(404);
    }
}
