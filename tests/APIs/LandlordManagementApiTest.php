<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LandlordManagement;

class LandlordManagementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/landlord_managements', $landlordManagement
        );

        $this->assertApiResponse($landlordManagement);
    }

    /**
     * @test
     */
    public function test_read_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/landlord_managements/'.$landlordManagement->id
        );

        $this->assertApiResponse($landlordManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->create();
        $editedLandlordManagement = factory(LandlordManagement::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/landlord_managements/'.$landlordManagement->id,
            $editedLandlordManagement
        );

        $this->assertApiResponse($editedLandlordManagement);
    }

    /**
     * @test
     */
    public function test_delete_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/landlord_managements/'.$landlordManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/landlord_managements/'.$landlordManagement->id
        );

        $this->response->assertStatus(404);
    }
}
