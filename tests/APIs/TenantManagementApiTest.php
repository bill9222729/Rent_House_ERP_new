<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TenantManagement;

class TenantManagementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tenant_managements', $tenantManagement
        );

        $this->assertApiResponse($tenantManagement);
    }

    /**
     * @test
     */
    public function test_read_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/tenant_managements/'.$tenantManagement->id
        );

        $this->assertApiResponse($tenantManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->create();
        $editedTenantManagement = factory(TenantManagement::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tenant_managements/'.$tenantManagement->id,
            $editedTenantManagement
        );

        $this->assertApiResponse($editedTenantManagement);
    }

    /**
     * @test
     */
    public function test_delete_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tenant_managements/'.$tenantManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tenant_managements/'.$tenantManagement->id
        );

        $this->response->assertStatus(404);
    }
}
