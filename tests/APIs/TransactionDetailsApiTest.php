<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TransactionDetails;

class TransactionDetailsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/transaction_details', $transactionDetails
        );

        $this->assertApiResponse($transactionDetails);
    }

    /**
     * @test
     */
    public function test_read_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/transaction_details/'.$transactionDetails->id
        );

        $this->assertApiResponse($transactionDetails->toArray());
    }

    /**
     * @test
     */
    public function test_update_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->create();
        $editedTransactionDetails = factory(TransactionDetails::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/transaction_details/'.$transactionDetails->id,
            $editedTransactionDetails
        );

        $this->assertApiResponse($editedTransactionDetails);
    }

    /**
     * @test
     */
    public function test_delete_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/transaction_details/'.$transactionDetails->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/transaction_details/'.$transactionDetails->id
        );

        $this->response->assertStatus(404);
    }
}
