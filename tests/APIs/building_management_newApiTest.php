<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\building_management_new;

class building_management_newApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/building_management_news', $buildingManagementNew
        );

        $this->assertApiResponse($buildingManagementNew);
    }

    /**
     * @test
     */
    public function test_read_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/building_management_news/'.$buildingManagementNew->id
        );

        $this->assertApiResponse($buildingManagementNew->toArray());
    }

    /**
     * @test
     */
    public function test_update_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->create();
        $editedbuilding_management_new = factory(building_management_new::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/building_management_news/'.$buildingManagementNew->id,
            $editedbuilding_management_new
        );

        $this->assertApiResponse($editedbuilding_management_new);
    }

    /**
     * @test
     */
    public function test_delete_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/building_management_news/'.$buildingManagementNew->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/building_management_news/'.$buildingManagementNew->id
        );

        $this->response->assertStatus(404);
    }
}
