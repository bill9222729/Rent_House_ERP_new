<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\change_history;

class change_historyApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_change_history()
    {
        $changeHistory = factory(change_history::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/change_histories', $changeHistory
        );

        $this->assertApiResponse($changeHistory);
    }

    /**
     * @test
     */
    public function test_read_change_history()
    {
        $changeHistory = factory(change_history::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/change_histories/'.$changeHistory->id
        );

        $this->assertApiResponse($changeHistory->toArray());
    }

    /**
     * @test
     */
    public function test_update_change_history()
    {
        $changeHistory = factory(change_history::class)->create();
        $editedchange_history = factory(change_history::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/change_histories/'.$changeHistory->id,
            $editedchange_history
        );

        $this->assertApiResponse($editedchange_history);
    }

    /**
     * @test
     */
    public function test_delete_change_history()
    {
        $changeHistory = factory(change_history::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/change_histories/'.$changeHistory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/change_histories/'.$changeHistory->id
        );

        $this->response->assertStatus(404);
    }
}
