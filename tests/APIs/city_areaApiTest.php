<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\city_area;

class city_areaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_city_area()
    {
        $cityArea = factory(city_area::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/city_areas', $cityArea
        );

        $this->assertApiResponse($cityArea);
    }

    /**
     * @test
     */
    public function test_read_city_area()
    {
        $cityArea = factory(city_area::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/city_areas/'.$cityArea->id
        );

        $this->assertApiResponse($cityArea->toArray());
    }

    /**
     * @test
     */
    public function test_update_city_area()
    {
        $cityArea = factory(city_area::class)->create();
        $editedcity_area = factory(city_area::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/city_areas/'.$cityArea->id,
            $editedcity_area
        );

        $this->assertApiResponse($editedcity_area);
    }

    /**
     * @test
     */
    public function test_delete_city_area()
    {
        $cityArea = factory(city_area::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/city_areas/'.$cityArea->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/city_areas/'.$cityArea->id
        );

        $this->response->assertStatus(404);
    }
}
