<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\contract_management;

class contract_managementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_contract_management()
    {
        $contractManagement = factory(contract_management::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/contract_managements', $contractManagement
        );

        $this->assertApiResponse($contractManagement);
    }

    /**
     * @test
     */
    public function test_read_contract_management()
    {
        $contractManagement = factory(contract_management::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/contract_managements/'.$contractManagement->id
        );

        $this->assertApiResponse($contractManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_contract_management()
    {
        $contractManagement = factory(contract_management::class)->create();
        $editedcontract_management = factory(contract_management::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/contract_managements/'.$contractManagement->id,
            $editedcontract_management
        );

        $this->assertApiResponse($editedcontract_management);
    }

    /**
     * @test
     */
    public function test_delete_contract_management()
    {
        $contractManagement = factory(contract_management::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/contract_managements/'.$contractManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/contract_managements/'.$contractManagement->id
        );

        $this->response->assertStatus(404);
    }
}
