<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\contracts_new;

class contracts_newApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/contracts_news', $contractsNew
        );

        $this->assertApiResponse($contractsNew);
    }

    /**
     * @test
     */
    public function test_read_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/contracts_news/'.$contractsNew->id
        );

        $this->assertApiResponse($contractsNew->toArray());
    }

    /**
     * @test
     */
    public function test_update_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->create();
        $editedcontracts_new = factory(contracts_new::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/contracts_news/'.$contractsNew->id,
            $editedcontracts_new
        );

        $this->assertApiResponse($editedcontracts_new);
    }

    /**
     * @test
     */
    public function test_delete_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/contracts_news/'.$contractsNew->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/contracts_news/'.$contractsNew->id
        );

        $this->response->assertStatus(404);
    }
}
