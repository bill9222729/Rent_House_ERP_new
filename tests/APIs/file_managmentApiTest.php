<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\file_managment;

class file_managmentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_file_managment()
    {
        $fileManagment = factory(file_managment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/file_managments', $fileManagment
        );

        $this->assertApiResponse($fileManagment);
    }

    /**
     * @test
     */
    public function test_read_file_managment()
    {
        $fileManagment = factory(file_managment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/file_managments/'.$fileManagment->id
        );

        $this->assertApiResponse($fileManagment->toArray());
    }

    /**
     * @test
     */
    public function test_update_file_managment()
    {
        $fileManagment = factory(file_managment::class)->create();
        $editedfile_managment = factory(file_managment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/file_managments/'.$fileManagment->id,
            $editedfile_managment
        );

        $this->assertApiResponse($editedfile_managment);
    }

    /**
     * @test
     */
    public function test_delete_file_managment()
    {
        $fileManagment = factory(file_managment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/file_managments/'.$fileManagment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/file_managments/'.$fileManagment->id
        );

        $this->response->assertStatus(404);
    }
}
