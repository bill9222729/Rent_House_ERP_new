<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\fix_detail_management;

class fix_detail_managementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/fix_detail_managements', $fixDetailManagement
        );

        $this->assertApiResponse($fixDetailManagement);
    }

    /**
     * @test
     */
    public function test_read_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/fix_detail_managements/'.$fixDetailManagement->id
        );

        $this->assertApiResponse($fixDetailManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->create();
        $editedfix_detail_management = factory(fix_detail_management::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/fix_detail_managements/'.$fixDetailManagement->id,
            $editedfix_detail_management
        );

        $this->assertApiResponse($editedfix_detail_management);
    }

    /**
     * @test
     */
    public function test_delete_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/fix_detail_managements/'.$fixDetailManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/fix_detail_managements/'.$fixDetailManagement->id
        );

        $this->response->assertStatus(404);
    }
}
