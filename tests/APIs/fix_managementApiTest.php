<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\fix_management;

class fix_managementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_fix_management()
    {
        $fixManagement = factory(fix_management::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/fix_managements', $fixManagement
        );

        $this->assertApiResponse($fixManagement);
    }

    /**
     * @test
     */
    public function test_read_fix_management()
    {
        $fixManagement = factory(fix_management::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/fix_managements/'.$fixManagement->id
        );

        $this->assertApiResponse($fixManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_fix_management()
    {
        $fixManagement = factory(fix_management::class)->create();
        $editedfix_management = factory(fix_management::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/fix_managements/'.$fixManagement->id,
            $editedfix_management
        );

        $this->assertApiResponse($editedfix_management);
    }

    /**
     * @test
     */
    public function test_delete_fix_management()
    {
        $fixManagement = factory(fix_management::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/fix_managements/'.$fixManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/fix_managements/'.$fixManagement->id
        );

        $this->response->assertStatus(404);
    }
}
