<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\insurance_management;

class insurance_managementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/insurance_managements', $insuranceManagement
        );

        $this->assertApiResponse($insuranceManagement);
    }

    /**
     * @test
     */
    public function test_read_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/insurance_managements/'.$insuranceManagement->id
        );

        $this->assertApiResponse($insuranceManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->create();
        $editedinsurance_management = factory(insurance_management::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/insurance_managements/'.$insuranceManagement->id,
            $editedinsurance_management
        );

        $this->assertApiResponse($editedinsurance_management);
    }

    /**
     * @test
     */
    public function test_delete_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/insurance_managements/'.$insuranceManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/insurance_managements/'.$insuranceManagement->id
        );

        $this->response->assertStatus(404);
    }
}
