<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\located_lot;

class located_lotApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_located_lot()
    {
        $locatedLot = factory(located_lot::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/located_lots', $locatedLot
        );

        $this->assertApiResponse($locatedLot);
    }

    /**
     * @test
     */
    public function test_read_located_lot()
    {
        $locatedLot = factory(located_lot::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/located_lots/'.$locatedLot->id
        );

        $this->assertApiResponse($locatedLot->toArray());
    }

    /**
     * @test
     */
    public function test_update_located_lot()
    {
        $locatedLot = factory(located_lot::class)->create();
        $editedlocated_lot = factory(located_lot::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/located_lots/'.$locatedLot->id,
            $editedlocated_lot
        );

        $this->assertApiResponse($editedlocated_lot);
    }

    /**
     * @test
     */
    public function test_delete_located_lot()
    {
        $locatedLot = factory(located_lot::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/located_lots/'.$locatedLot->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/located_lots/'.$locatedLot->id
        );

        $this->response->assertStatus(404);
    }
}
