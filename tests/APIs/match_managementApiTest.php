<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\match_management;

class match_managementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_match_management()
    {
        $matchManagement = factory(match_management::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/match_managements', $matchManagement
        );

        $this->assertApiResponse($matchManagement);
    }

    /**
     * @test
     */
    public function test_read_match_management()
    {
        $matchManagement = factory(match_management::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/match_managements/'.$matchManagement->id
        );

        $this->assertApiResponse($matchManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_match_management()
    {
        $matchManagement = factory(match_management::class)->create();
        $editedmatch_management = factory(match_management::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/match_managements/'.$matchManagement->id,
            $editedmatch_management
        );

        $this->assertApiResponse($editedmatch_management);
    }

    /**
     * @test
     */
    public function test_delete_match_management()
    {
        $matchManagement = factory(match_management::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/match_managements/'.$matchManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/match_managements/'.$matchManagement->id
        );

        $this->response->assertStatus(404);
    }
}
