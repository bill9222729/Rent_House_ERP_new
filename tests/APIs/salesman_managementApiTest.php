<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\salesman_management;

class salesman_managementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/salesman_managements', $salesmanManagement
        );

        $this->assertApiResponse($salesmanManagement);
    }

    /**
     * @test
     */
    public function test_read_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/salesman_managements/'.$salesmanManagement->id
        );

        $this->assertApiResponse($salesmanManagement->toArray());
    }

    /**
     * @test
     */
    public function test_update_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->create();
        $editedsalesman_management = factory(salesman_management::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/salesman_managements/'.$salesmanManagement->id,
            $editedsalesman_management
        );

        $this->assertApiResponse($editedsalesman_management);
    }

    /**
     * @test
     */
    public function test_delete_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/salesman_managements/'.$salesmanManagement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/salesman_managements/'.$salesmanManagement->id
        );

        $this->response->assertStatus(404);
    }
}
