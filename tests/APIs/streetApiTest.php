<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\street;

class streetApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_street()
    {
        $street = factory(street::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/streets', $street
        );

        $this->assertApiResponse($street);
    }

    /**
     * @test
     */
    public function test_read_street()
    {
        $street = factory(street::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/streets/'.$street->id
        );

        $this->assertApiResponse($street->toArray());
    }

    /**
     * @test
     */
    public function test_update_street()
    {
        $street = factory(street::class)->create();
        $editedstreet = factory(street::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/streets/'.$street->id,
            $editedstreet
        );

        $this->assertApiResponse($editedstreet);
    }

    /**
     * @test
     */
    public function test_delete_street()
    {
        $street = factory(street::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/streets/'.$street->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/streets/'.$street->id
        );

        $this->response->assertStatus(404);
    }
}
