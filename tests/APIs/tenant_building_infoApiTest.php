<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\tenant_building_info;

class tenant_building_infoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tenant_building_infos', $tenantBuildingInfo
        );

        $this->assertApiResponse($tenantBuildingInfo);
    }

    /**
     * @test
     */
    public function test_read_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/tenant_building_infos/'.$tenantBuildingInfo->id
        );

        $this->assertApiResponse($tenantBuildingInfo->toArray());
    }

    /**
     * @test
     */
    public function test_update_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->create();
        $editedtenant_building_info = factory(tenant_building_info::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tenant_building_infos/'.$tenantBuildingInfo->id,
            $editedtenant_building_info
        );

        $this->assertApiResponse($editedtenant_building_info);
    }

    /**
     * @test
     */
    public function test_delete_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tenant_building_infos/'.$tenantBuildingInfo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tenant_building_infos/'.$tenantBuildingInfo->id
        );

        $this->response->assertStatus(404);
    }
}
