<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\tenant_family_info;

class tenant_family_infoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tenant_family_infos', $tenantFamilyInfo
        );

        $this->assertApiResponse($tenantFamilyInfo);
    }

    /**
     * @test
     */
    public function test_read_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/tenant_family_infos/'.$tenantFamilyInfo->id
        );

        $this->assertApiResponse($tenantFamilyInfo->toArray());
    }

    /**
     * @test
     */
    public function test_update_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->create();
        $editedtenant_family_info = factory(tenant_family_info::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tenant_family_infos/'.$tenantFamilyInfo->id,
            $editedtenant_family_info
        );

        $this->assertApiResponse($editedtenant_family_info);
    }

    /**
     * @test
     */
    public function test_delete_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tenant_family_infos/'.$tenantFamilyInfo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tenant_family_infos/'.$tenantFamilyInfo->id
        );

        $this->response->assertStatus(404);
    }
}
