<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\tenant_management_new;

class tenant_management_newApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tenant_management_news', $tenantManagementNew
        );

        $this->assertApiResponse($tenantManagementNew);
    }

    /**
     * @test
     */
    public function test_read_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/tenant_management_news/'.$tenantManagementNew->id
        );

        $this->assertApiResponse($tenantManagementNew->toArray());
    }

    /**
     * @test
     */
    public function test_update_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->create();
        $editedtenant_management_new = factory(tenant_management_new::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tenant_management_news/'.$tenantManagementNew->id,
            $editedtenant_management_new
        );

        $this->assertApiResponse($editedtenant_management_new);
    }

    /**
     * @test
     */
    public function test_delete_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tenant_management_news/'.$tenantManagementNew->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tenant_management_news/'.$tenantManagementNew->id
        );

        $this->response->assertStatus(404);
    }
}
