<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\tenant_minor_offspring;

class tenant_minor_offspringApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tenant_minor_offsprings', $tenantMinorOffspring
        );

        $this->assertApiResponse($tenantMinorOffspring);
    }

    /**
     * @test
     */
    public function test_read_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/tenant_minor_offsprings/'.$tenantMinorOffspring->id
        );

        $this->assertApiResponse($tenantMinorOffspring->toArray());
    }

    /**
     * @test
     */
    public function test_update_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->create();
        $editedtenant_minor_offspring = factory(tenant_minor_offspring::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tenant_minor_offsprings/'.$tenantMinorOffspring->id,
            $editedtenant_minor_offspring
        );

        $this->assertApiResponse($editedtenant_minor_offspring);
    }

    /**
     * @test
     */
    public function test_delete_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tenant_minor_offsprings/'.$tenantMinorOffspring->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tenant_minor_offsprings/'.$tenantMinorOffspring->id
        );

        $this->response->assertStatus(404);
    }
}
