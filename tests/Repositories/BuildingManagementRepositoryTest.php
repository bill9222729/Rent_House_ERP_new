<?php namespace Tests\Repositories;

use App\Models\BuildingManagement;
use App\Repositories\BuildingManagementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BuildingManagementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BuildingManagementRepository
     */
    protected $buildingManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->buildingManagementRepo = \App::make(BuildingManagementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->make()->toArray();

        $createdBuildingManagement = $this->buildingManagementRepo->create($buildingManagement);

        $createdBuildingManagement = $createdBuildingManagement->toArray();
        $this->assertArrayHasKey('id', $createdBuildingManagement);
        $this->assertNotNull($createdBuildingManagement['id'], 'Created BuildingManagement must have id specified');
        $this->assertNotNull(BuildingManagement::find($createdBuildingManagement['id']), 'BuildingManagement with given id must be in DB');
        $this->assertModelData($buildingManagement, $createdBuildingManagement);
    }

    /**
     * @test read
     */
    public function test_read_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->create();

        $dbBuildingManagement = $this->buildingManagementRepo->find($buildingManagement->id);

        $dbBuildingManagement = $dbBuildingManagement->toArray();
        $this->assertModelData($buildingManagement->toArray(), $dbBuildingManagement);
    }

    /**
     * @test update
     */
    public function test_update_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->create();
        $fakeBuildingManagement = factory(BuildingManagement::class)->make()->toArray();

        $updatedBuildingManagement = $this->buildingManagementRepo->update($fakeBuildingManagement, $buildingManagement->id);

        $this->assertModelData($fakeBuildingManagement, $updatedBuildingManagement->toArray());
        $dbBuildingManagement = $this->buildingManagementRepo->find($buildingManagement->id);
        $this->assertModelData($fakeBuildingManagement, $dbBuildingManagement->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_building_management()
    {
        $buildingManagement = factory(BuildingManagement::class)->create();

        $resp = $this->buildingManagementRepo->delete($buildingManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(BuildingManagement::find($buildingManagement->id), 'BuildingManagement should not exist in DB');
    }
}
