<?php namespace Tests\Repositories;

use App\Models\FixHistory;
use App\Repositories\FixHistoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FixHistoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FixHistoryRepository
     */
    protected $fixHistoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->fixHistoryRepo = \App::make(FixHistoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->make()->toArray();

        $createdFixHistory = $this->fixHistoryRepo->create($fixHistory);

        $createdFixHistory = $createdFixHistory->toArray();
        $this->assertArrayHasKey('id', $createdFixHistory);
        $this->assertNotNull($createdFixHistory['id'], 'Created FixHistory must have id specified');
        $this->assertNotNull(FixHistory::find($createdFixHistory['id']), 'FixHistory with given id must be in DB');
        $this->assertModelData($fixHistory, $createdFixHistory);
    }

    /**
     * @test read
     */
    public function test_read_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->create();

        $dbFixHistory = $this->fixHistoryRepo->find($fixHistory->id);

        $dbFixHistory = $dbFixHistory->toArray();
        $this->assertModelData($fixHistory->toArray(), $dbFixHistory);
    }

    /**
     * @test update
     */
    public function test_update_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->create();
        $fakeFixHistory = factory(FixHistory::class)->make()->toArray();

        $updatedFixHistory = $this->fixHistoryRepo->update($fakeFixHistory, $fixHistory->id);

        $this->assertModelData($fakeFixHistory, $updatedFixHistory->toArray());
        $dbFixHistory = $this->fixHistoryRepo->find($fixHistory->id);
        $this->assertModelData($fakeFixHistory, $dbFixHistory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_fix_history()
    {
        $fixHistory = factory(FixHistory::class)->create();

        $resp = $this->fixHistoryRepo->delete($fixHistory->id);

        $this->assertTrue($resp);
        $this->assertNull(FixHistory::find($fixHistory->id), 'FixHistory should not exist in DB');
    }
}
