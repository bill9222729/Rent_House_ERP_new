<?php namespace Tests\Repositories;

use App\Models\HousePayHistory;
use App\Repositories\HousePayHistoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HousePayHistoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HousePayHistoryRepository
     */
    protected $housePayHistoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->housePayHistoryRepo = \App::make(HousePayHistoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->make()->toArray();

        $createdHousePayHistory = $this->housePayHistoryRepo->create($housePayHistory);

        $createdHousePayHistory = $createdHousePayHistory->toArray();
        $this->assertArrayHasKey('id', $createdHousePayHistory);
        $this->assertNotNull($createdHousePayHistory['id'], 'Created HousePayHistory must have id specified');
        $this->assertNotNull(HousePayHistory::find($createdHousePayHistory['id']), 'HousePayHistory with given id must be in DB');
        $this->assertModelData($housePayHistory, $createdHousePayHistory);
    }

    /**
     * @test read
     */
    public function test_read_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->create();

        $dbHousePayHistory = $this->housePayHistoryRepo->find($housePayHistory->id);

        $dbHousePayHistory = $dbHousePayHistory->toArray();
        $this->assertModelData($housePayHistory->toArray(), $dbHousePayHistory);
    }

    /**
     * @test update
     */
    public function test_update_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->create();
        $fakeHousePayHistory = factory(HousePayHistory::class)->make()->toArray();

        $updatedHousePayHistory = $this->housePayHistoryRepo->update($fakeHousePayHistory, $housePayHistory->id);

        $this->assertModelData($fakeHousePayHistory, $updatedHousePayHistory->toArray());
        $dbHousePayHistory = $this->housePayHistoryRepo->find($housePayHistory->id);
        $this->assertModelData($fakeHousePayHistory, $dbHousePayHistory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_house_pay_history()
    {
        $housePayHistory = factory(HousePayHistory::class)->create();

        $resp = $this->housePayHistoryRepo->delete($housePayHistory->id);

        $this->assertTrue($resp);
        $this->assertNull(HousePayHistory::find($housePayHistory->id), 'HousePayHistory should not exist in DB');
    }
}
