<?php namespace Tests\Repositories;

use App\Models\LandlordManagement;
use App\Repositories\LandlordManagementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LandlordManagementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LandlordManagementRepository
     */
    protected $landlordManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->landlordManagementRepo = \App::make(LandlordManagementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->make()->toArray();

        $createdLandlordManagement = $this->landlordManagementRepo->create($landlordManagement);

        $createdLandlordManagement = $createdLandlordManagement->toArray();
        $this->assertArrayHasKey('id', $createdLandlordManagement);
        $this->assertNotNull($createdLandlordManagement['id'], 'Created LandlordManagement must have id specified');
        $this->assertNotNull(LandlordManagement::find($createdLandlordManagement['id']), 'LandlordManagement with given id must be in DB');
        $this->assertModelData($landlordManagement, $createdLandlordManagement);
    }

    /**
     * @test read
     */
    public function test_read_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->create();

        $dbLandlordManagement = $this->landlordManagementRepo->find($landlordManagement->id);

        $dbLandlordManagement = $dbLandlordManagement->toArray();
        $this->assertModelData($landlordManagement->toArray(), $dbLandlordManagement);
    }

    /**
     * @test update
     */
    public function test_update_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->create();
        $fakeLandlordManagement = factory(LandlordManagement::class)->make()->toArray();

        $updatedLandlordManagement = $this->landlordManagementRepo->update($fakeLandlordManagement, $landlordManagement->id);

        $this->assertModelData($fakeLandlordManagement, $updatedLandlordManagement->toArray());
        $dbLandlordManagement = $this->landlordManagementRepo->find($landlordManagement->id);
        $this->assertModelData($fakeLandlordManagement, $dbLandlordManagement->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_landlord_management()
    {
        $landlordManagement = factory(LandlordManagement::class)->create();

        $resp = $this->landlordManagementRepo->delete($landlordManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(LandlordManagement::find($landlordManagement->id), 'LandlordManagement should not exist in DB');
    }
}
