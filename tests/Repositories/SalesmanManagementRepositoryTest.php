<?php namespace Tests\Repositories;

use App\Models\SalesmanManagement;
use App\Repositories\SalesmanManagementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SalesmanManagementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SalesmanManagementRepository
     */
    protected $salesmanManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->salesmanManagementRepo = \App::make(SalesmanManagementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_salesman_management()
    {
        $salesmanManagement = factory(SalesmanManagement::class)->make()->toArray();

        $createdSalesmanManagement = $this->salesmanManagementRepo->create($salesmanManagement);

        $createdSalesmanManagement = $createdSalesmanManagement->toArray();
        $this->assertArrayHasKey('id', $createdSalesmanManagement);
        $this->assertNotNull($createdSalesmanManagement['id'], 'Created SalesmanManagement must have id specified');
        $this->assertNotNull(SalesmanManagement::find($createdSalesmanManagement['id']), 'SalesmanManagement with given id must be in DB');
        $this->assertModelData($salesmanManagement, $createdSalesmanManagement);
    }

    /**
     * @test read
     */
    public function test_read_salesman_management()
    {
        $salesmanManagement = factory(SalesmanManagement::class)->create();

        $dbSalesmanManagement = $this->salesmanManagementRepo->find($salesmanManagement->id);

        $dbSalesmanManagement = $dbSalesmanManagement->toArray();
        $this->assertModelData($salesmanManagement->toArray(), $dbSalesmanManagement);
    }

    /**
     * @test update
     */
    public function test_update_salesman_management()
    {
        $salesmanManagement = factory(SalesmanManagement::class)->create();
        $fakeSalesmanManagement = factory(SalesmanManagement::class)->make()->toArray();

        $updatedSalesmanManagement = $this->salesmanManagementRepo->update($fakeSalesmanManagement, $salesmanManagement->id);

        $this->assertModelData($fakeSalesmanManagement, $updatedSalesmanManagement->toArray());
        $dbSalesmanManagement = $this->salesmanManagementRepo->find($salesmanManagement->id);
        $this->assertModelData($fakeSalesmanManagement, $dbSalesmanManagement->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_salesman_management()
    {
        $salesmanManagement = factory(SalesmanManagement::class)->create();

        $resp = $this->salesmanManagementRepo->delete($salesmanManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(SalesmanManagement::find($salesmanManagement->id), 'SalesmanManagement should not exist in DB');
    }
}
