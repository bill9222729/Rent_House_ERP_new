<?php namespace Tests\Repositories;

use App\Models\TenantManagement;
use App\Repositories\TenantManagementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TenantManagementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TenantManagementRepository
     */
    protected $tenantManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tenantManagementRepo = \App::make(TenantManagementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->make()->toArray();

        $createdTenantManagement = $this->tenantManagementRepo->create($tenantManagement);

        $createdTenantManagement = $createdTenantManagement->toArray();
        $this->assertArrayHasKey('id', $createdTenantManagement);
        $this->assertNotNull($createdTenantManagement['id'], 'Created TenantManagement must have id specified');
        $this->assertNotNull(TenantManagement::find($createdTenantManagement['id']), 'TenantManagement with given id must be in DB');
        $this->assertModelData($tenantManagement, $createdTenantManagement);
    }

    /**
     * @test read
     */
    public function test_read_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->create();

        $dbTenantManagement = $this->tenantManagementRepo->find($tenantManagement->id);

        $dbTenantManagement = $dbTenantManagement->toArray();
        $this->assertModelData($tenantManagement->toArray(), $dbTenantManagement);
    }

    /**
     * @test update
     */
    public function test_update_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->create();
        $fakeTenantManagement = factory(TenantManagement::class)->make()->toArray();

        $updatedTenantManagement = $this->tenantManagementRepo->update($fakeTenantManagement, $tenantManagement->id);

        $this->assertModelData($fakeTenantManagement, $updatedTenantManagement->toArray());
        $dbTenantManagement = $this->tenantManagementRepo->find($tenantManagement->id);
        $this->assertModelData($fakeTenantManagement, $dbTenantManagement->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tenant_management()
    {
        $tenantManagement = factory(TenantManagement::class)->create();

        $resp = $this->tenantManagementRepo->delete($tenantManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(TenantManagement::find($tenantManagement->id), 'TenantManagement should not exist in DB');
    }
}
