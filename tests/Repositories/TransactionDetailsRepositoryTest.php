<?php namespace Tests\Repositories;

use App\Models\TransactionDetails;
use App\Repositories\TransactionDetailsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TransactionDetailsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TransactionDetailsRepository
     */
    protected $transactionDetailsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->transactionDetailsRepo = \App::make(TransactionDetailsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->make()->toArray();

        $createdTransactionDetails = $this->transactionDetailsRepo->create($transactionDetails);

        $createdTransactionDetails = $createdTransactionDetails->toArray();
        $this->assertArrayHasKey('id', $createdTransactionDetails);
        $this->assertNotNull($createdTransactionDetails['id'], 'Created TransactionDetails must have id specified');
        $this->assertNotNull(TransactionDetails::find($createdTransactionDetails['id']), 'TransactionDetails with given id must be in DB');
        $this->assertModelData($transactionDetails, $createdTransactionDetails);
    }

    /**
     * @test read
     */
    public function test_read_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->create();

        $dbTransactionDetails = $this->transactionDetailsRepo->find($transactionDetails->id);

        $dbTransactionDetails = $dbTransactionDetails->toArray();
        $this->assertModelData($transactionDetails->toArray(), $dbTransactionDetails);
    }

    /**
     * @test update
     */
    public function test_update_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->create();
        $fakeTransactionDetails = factory(TransactionDetails::class)->make()->toArray();

        $updatedTransactionDetails = $this->transactionDetailsRepo->update($fakeTransactionDetails, $transactionDetails->id);

        $this->assertModelData($fakeTransactionDetails, $updatedTransactionDetails->toArray());
        $dbTransactionDetails = $this->transactionDetailsRepo->find($transactionDetails->id);
        $this->assertModelData($fakeTransactionDetails, $dbTransactionDetails->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_transaction_details()
    {
        $transactionDetails = factory(TransactionDetails::class)->create();

        $resp = $this->transactionDetailsRepo->delete($transactionDetails->id);

        $this->assertTrue($resp);
        $this->assertNull(TransactionDetails::find($transactionDetails->id), 'TransactionDetails should not exist in DB');
    }
}
