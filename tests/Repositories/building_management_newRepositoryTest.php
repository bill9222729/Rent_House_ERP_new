<?php namespace Tests\Repositories;

use App\Models\building_management_new;
use App\Repositories\building_management_newRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class building_management_newRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var building_management_newRepository
     */
    protected $buildingManagementNewRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->buildingManagementNewRepo = \App::make(building_management_newRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->make()->toArray();

        $createdbuilding_management_new = $this->buildingManagementNewRepo->create($buildingManagementNew);

        $createdbuilding_management_new = $createdbuilding_management_new->toArray();
        $this->assertArrayHasKey('id', $createdbuilding_management_new);
        $this->assertNotNull($createdbuilding_management_new['id'], 'Created building_management_new must have id specified');
        $this->assertNotNull(building_management_new::find($createdbuilding_management_new['id']), 'building_management_new with given id must be in DB');
        $this->assertModelData($buildingManagementNew, $createdbuilding_management_new);
    }

    /**
     * @test read
     */
    public function test_read_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->create();

        $dbbuilding_management_new = $this->buildingManagementNewRepo->find($buildingManagementNew->id);

        $dbbuilding_management_new = $dbbuilding_management_new->toArray();
        $this->assertModelData($buildingManagementNew->toArray(), $dbbuilding_management_new);
    }

    /**
     * @test update
     */
    public function test_update_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->create();
        $fakebuilding_management_new = factory(building_management_new::class)->make()->toArray();

        $updatedbuilding_management_new = $this->buildingManagementNewRepo->update($fakebuilding_management_new, $buildingManagementNew->id);

        $this->assertModelData($fakebuilding_management_new, $updatedbuilding_management_new->toArray());
        $dbbuilding_management_new = $this->buildingManagementNewRepo->find($buildingManagementNew->id);
        $this->assertModelData($fakebuilding_management_new, $dbbuilding_management_new->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_building_management_new()
    {
        $buildingManagementNew = factory(building_management_new::class)->create();

        $resp = $this->buildingManagementNewRepo->delete($buildingManagementNew->id);

        $this->assertTrue($resp);
        $this->assertNull(building_management_new::find($buildingManagementNew->id), 'building_management_new should not exist in DB');
    }
}
