<?php namespace Tests\Repositories;

use App\Models\change_history;
use App\Repositories\change_historyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class change_historyRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var change_historyRepository
     */
    protected $changeHistoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->changeHistoryRepo = \App::make(change_historyRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_change_history()
    {
        $changeHistory = factory(change_history::class)->make()->toArray();

        $createdchange_history = $this->changeHistoryRepo->create($changeHistory);

        $createdchange_history = $createdchange_history->toArray();
        $this->assertArrayHasKey('id', $createdchange_history);
        $this->assertNotNull($createdchange_history['id'], 'Created change_history must have id specified');
        $this->assertNotNull(change_history::find($createdchange_history['id']), 'change_history with given id must be in DB');
        $this->assertModelData($changeHistory, $createdchange_history);
    }

    /**
     * @test read
     */
    public function test_read_change_history()
    {
        $changeHistory = factory(change_history::class)->create();

        $dbchange_history = $this->changeHistoryRepo->find($changeHistory->id);

        $dbchange_history = $dbchange_history->toArray();
        $this->assertModelData($changeHistory->toArray(), $dbchange_history);
    }

    /**
     * @test update
     */
    public function test_update_change_history()
    {
        $changeHistory = factory(change_history::class)->create();
        $fakechange_history = factory(change_history::class)->make()->toArray();

        $updatedchange_history = $this->changeHistoryRepo->update($fakechange_history, $changeHistory->id);

        $this->assertModelData($fakechange_history, $updatedchange_history->toArray());
        $dbchange_history = $this->changeHistoryRepo->find($changeHistory->id);
        $this->assertModelData($fakechange_history, $dbchange_history->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_change_history()
    {
        $changeHistory = factory(change_history::class)->create();

        $resp = $this->changeHistoryRepo->delete($changeHistory->id);

        $this->assertTrue($resp);
        $this->assertNull(change_history::find($changeHistory->id), 'change_history should not exist in DB');
    }
}
