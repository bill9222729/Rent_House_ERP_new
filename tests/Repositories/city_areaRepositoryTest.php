<?php namespace Tests\Repositories;

use App\Models\city_area;
use App\Repositories\city_areaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class city_areaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var city_areaRepository
     */
    protected $cityAreaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->cityAreaRepo = \App::make(city_areaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_city_area()
    {
        $cityArea = factory(city_area::class)->make()->toArray();

        $createdcity_area = $this->cityAreaRepo->create($cityArea);

        $createdcity_area = $createdcity_area->toArray();
        $this->assertArrayHasKey('id', $createdcity_area);
        $this->assertNotNull($createdcity_area['id'], 'Created city_area must have id specified');
        $this->assertNotNull(city_area::find($createdcity_area['id']), 'city_area with given id must be in DB');
        $this->assertModelData($cityArea, $createdcity_area);
    }

    /**
     * @test read
     */
    public function test_read_city_area()
    {
        $cityArea = factory(city_area::class)->create();

        $dbcity_area = $this->cityAreaRepo->find($cityArea->id);

        $dbcity_area = $dbcity_area->toArray();
        $this->assertModelData($cityArea->toArray(), $dbcity_area);
    }

    /**
     * @test update
     */
    public function test_update_city_area()
    {
        $cityArea = factory(city_area::class)->create();
        $fakecity_area = factory(city_area::class)->make()->toArray();

        $updatedcity_area = $this->cityAreaRepo->update($fakecity_area, $cityArea->id);

        $this->assertModelData($fakecity_area, $updatedcity_area->toArray());
        $dbcity_area = $this->cityAreaRepo->find($cityArea->id);
        $this->assertModelData($fakecity_area, $dbcity_area->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_city_area()
    {
        $cityArea = factory(city_area::class)->create();

        $resp = $this->cityAreaRepo->delete($cityArea->id);

        $this->assertTrue($resp);
        $this->assertNull(city_area::find($cityArea->id), 'city_area should not exist in DB');
    }
}
