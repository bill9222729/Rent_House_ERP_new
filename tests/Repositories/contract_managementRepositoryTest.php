<?php namespace Tests\Repositories;

use App\Models\contract_management;
use App\Repositories\contract_managementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class contract_managementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var contract_managementRepository
     */
    protected $contractManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->contractManagementRepo = \App::make(contract_managementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_contract_management()
    {
        $contractManagement = factory(contract_management::class)->make()->toArray();

        $createdcontract_management = $this->contractManagementRepo->create($contractManagement);

        $createdcontract_management = $createdcontract_management->toArray();
        $this->assertArrayHasKey('id', $createdcontract_management);
        $this->assertNotNull($createdcontract_management['id'], 'Created contract_management must have id specified');
        $this->assertNotNull(contract_management::find($createdcontract_management['id']), 'contract_management with given id must be in DB');
        $this->assertModelData($contractManagement, $createdcontract_management);
    }

    /**
     * @test read
     */
    public function test_read_contract_management()
    {
        $contractManagement = factory(contract_management::class)->create();

        $dbcontract_management = $this->contractManagementRepo->find($contractManagement->id);

        $dbcontract_management = $dbcontract_management->toArray();
        $this->assertModelData($contractManagement->toArray(), $dbcontract_management);
    }

    /**
     * @test update
     */
    public function test_update_contract_management()
    {
        $contractManagement = factory(contract_management::class)->create();
        $fakecontract_management = factory(contract_management::class)->make()->toArray();

        $updatedcontract_management = $this->contractManagementRepo->update($fakecontract_management, $contractManagement->id);

        $this->assertModelData($fakecontract_management, $updatedcontract_management->toArray());
        $dbcontract_management = $this->contractManagementRepo->find($contractManagement->id);
        $this->assertModelData($fakecontract_management, $dbcontract_management->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_contract_management()
    {
        $contractManagement = factory(contract_management::class)->create();

        $resp = $this->contractManagementRepo->delete($contractManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(contract_management::find($contractManagement->id), 'contract_management should not exist in DB');
    }
}
