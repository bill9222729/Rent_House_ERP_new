<?php namespace Tests\Repositories;

use App\Models\contracts_new;
use App\Repositories\contracts_newRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class contracts_newRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var contracts_newRepository
     */
    protected $contractsNewRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->contractsNewRepo = \App::make(contracts_newRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->make()->toArray();

        $createdcontracts_new = $this->contractsNewRepo->create($contractsNew);

        $createdcontracts_new = $createdcontracts_new->toArray();
        $this->assertArrayHasKey('id', $createdcontracts_new);
        $this->assertNotNull($createdcontracts_new['id'], 'Created contracts_new must have id specified');
        $this->assertNotNull(contracts_new::find($createdcontracts_new['id']), 'contracts_new with given id must be in DB');
        $this->assertModelData($contractsNew, $createdcontracts_new);
    }

    /**
     * @test read
     */
    public function test_read_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->create();

        $dbcontracts_new = $this->contractsNewRepo->find($contractsNew->id);

        $dbcontracts_new = $dbcontracts_new->toArray();
        $this->assertModelData($contractsNew->toArray(), $dbcontracts_new);
    }

    /**
     * @test update
     */
    public function test_update_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->create();
        $fakecontracts_new = factory(contracts_new::class)->make()->toArray();

        $updatedcontracts_new = $this->contractsNewRepo->update($fakecontracts_new, $contractsNew->id);

        $this->assertModelData($fakecontracts_new, $updatedcontracts_new->toArray());
        $dbcontracts_new = $this->contractsNewRepo->find($contractsNew->id);
        $this->assertModelData($fakecontracts_new, $dbcontracts_new->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_contracts_new()
    {
        $contractsNew = factory(contracts_new::class)->create();

        $resp = $this->contractsNewRepo->delete($contractsNew->id);

        $this->assertTrue($resp);
        $this->assertNull(contracts_new::find($contractsNew->id), 'contracts_new should not exist in DB');
    }
}
