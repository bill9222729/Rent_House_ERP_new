<?php namespace Tests\Repositories;

use App\Models\file_managment;
use App\Repositories\file_managmentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class file_managmentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var file_managmentRepository
     */
    protected $fileManagmentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->fileManagmentRepo = \App::make(file_managmentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_file_managment()
    {
        $fileManagment = factory(file_managment::class)->make()->toArray();

        $createdfile_managment = $this->fileManagmentRepo->create($fileManagment);

        $createdfile_managment = $createdfile_managment->toArray();
        $this->assertArrayHasKey('id', $createdfile_managment);
        $this->assertNotNull($createdfile_managment['id'], 'Created file_managment must have id specified');
        $this->assertNotNull(file_managment::find($createdfile_managment['id']), 'file_managment with given id must be in DB');
        $this->assertModelData($fileManagment, $createdfile_managment);
    }

    /**
     * @test read
     */
    public function test_read_file_managment()
    {
        $fileManagment = factory(file_managment::class)->create();

        $dbfile_managment = $this->fileManagmentRepo->find($fileManagment->id);

        $dbfile_managment = $dbfile_managment->toArray();
        $this->assertModelData($fileManagment->toArray(), $dbfile_managment);
    }

    /**
     * @test update
     */
    public function test_update_file_managment()
    {
        $fileManagment = factory(file_managment::class)->create();
        $fakefile_managment = factory(file_managment::class)->make()->toArray();

        $updatedfile_managment = $this->fileManagmentRepo->update($fakefile_managment, $fileManagment->id);

        $this->assertModelData($fakefile_managment, $updatedfile_managment->toArray());
        $dbfile_managment = $this->fileManagmentRepo->find($fileManagment->id);
        $this->assertModelData($fakefile_managment, $dbfile_managment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_file_managment()
    {
        $fileManagment = factory(file_managment::class)->create();

        $resp = $this->fileManagmentRepo->delete($fileManagment->id);

        $this->assertTrue($resp);
        $this->assertNull(file_managment::find($fileManagment->id), 'file_managment should not exist in DB');
    }
}
