<?php namespace Tests\Repositories;

use App\Models\fix_detail_management;
use App\Repositories\fix_detail_managementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class fix_detail_managementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var fix_detail_managementRepository
     */
    protected $fixDetailManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->fixDetailManagementRepo = \App::make(fix_detail_managementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->make()->toArray();

        $createdfix_detail_management = $this->fixDetailManagementRepo->create($fixDetailManagement);

        $createdfix_detail_management = $createdfix_detail_management->toArray();
        $this->assertArrayHasKey('id', $createdfix_detail_management);
        $this->assertNotNull($createdfix_detail_management['id'], 'Created fix_detail_management must have id specified');
        $this->assertNotNull(fix_detail_management::find($createdfix_detail_management['id']), 'fix_detail_management with given id must be in DB');
        $this->assertModelData($fixDetailManagement, $createdfix_detail_management);
    }

    /**
     * @test read
     */
    public function test_read_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->create();

        $dbfix_detail_management = $this->fixDetailManagementRepo->find($fixDetailManagement->id);

        $dbfix_detail_management = $dbfix_detail_management->toArray();
        $this->assertModelData($fixDetailManagement->toArray(), $dbfix_detail_management);
    }

    /**
     * @test update
     */
    public function test_update_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->create();
        $fakefix_detail_management = factory(fix_detail_management::class)->make()->toArray();

        $updatedfix_detail_management = $this->fixDetailManagementRepo->update($fakefix_detail_management, $fixDetailManagement->id);

        $this->assertModelData($fakefix_detail_management, $updatedfix_detail_management->toArray());
        $dbfix_detail_management = $this->fixDetailManagementRepo->find($fixDetailManagement->id);
        $this->assertModelData($fakefix_detail_management, $dbfix_detail_management->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_fix_detail_management()
    {
        $fixDetailManagement = factory(fix_detail_management::class)->create();

        $resp = $this->fixDetailManagementRepo->delete($fixDetailManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(fix_detail_management::find($fixDetailManagement->id), 'fix_detail_management should not exist in DB');
    }
}
