<?php namespace Tests\Repositories;

use App\Models\fix_management;
use App\Repositories\fix_managementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class fix_managementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var fix_managementRepository
     */
    protected $fixManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->fixManagementRepo = \App::make(fix_managementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_fix_management()
    {
        $fixManagement = factory(fix_management::class)->make()->toArray();

        $createdfix_management = $this->fixManagementRepo->create($fixManagement);

        $createdfix_management = $createdfix_management->toArray();
        $this->assertArrayHasKey('id', $createdfix_management);
        $this->assertNotNull($createdfix_management['id'], 'Created fix_management must have id specified');
        $this->assertNotNull(fix_management::find($createdfix_management['id']), 'fix_management with given id must be in DB');
        $this->assertModelData($fixManagement, $createdfix_management);
    }

    /**
     * @test read
     */
    public function test_read_fix_management()
    {
        $fixManagement = factory(fix_management::class)->create();

        $dbfix_management = $this->fixManagementRepo->find($fixManagement->id);

        $dbfix_management = $dbfix_management->toArray();
        $this->assertModelData($fixManagement->toArray(), $dbfix_management);
    }

    /**
     * @test update
     */
    public function test_update_fix_management()
    {
        $fixManagement = factory(fix_management::class)->create();
        $fakefix_management = factory(fix_management::class)->make()->toArray();

        $updatedfix_management = $this->fixManagementRepo->update($fakefix_management, $fixManagement->id);

        $this->assertModelData($fakefix_management, $updatedfix_management->toArray());
        $dbfix_management = $this->fixManagementRepo->find($fixManagement->id);
        $this->assertModelData($fakefix_management, $dbfix_management->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_fix_management()
    {
        $fixManagement = factory(fix_management::class)->create();

        $resp = $this->fixManagementRepo->delete($fixManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(fix_management::find($fixManagement->id), 'fix_management should not exist in DB');
    }
}
