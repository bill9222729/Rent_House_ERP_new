<?php namespace Tests\Repositories;

use App\Models\insurance_management;
use App\Repositories\insurance_managementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class insurance_managementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var insurance_managementRepository
     */
    protected $insuranceManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->insuranceManagementRepo = \App::make(insurance_managementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->make()->toArray();

        $createdinsurance_management = $this->insuranceManagementRepo->create($insuranceManagement);

        $createdinsurance_management = $createdinsurance_management->toArray();
        $this->assertArrayHasKey('id', $createdinsurance_management);
        $this->assertNotNull($createdinsurance_management['id'], 'Created insurance_management must have id specified');
        $this->assertNotNull(insurance_management::find($createdinsurance_management['id']), 'insurance_management with given id must be in DB');
        $this->assertModelData($insuranceManagement, $createdinsurance_management);
    }

    /**
     * @test read
     */
    public function test_read_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->create();

        $dbinsurance_management = $this->insuranceManagementRepo->find($insuranceManagement->id);

        $dbinsurance_management = $dbinsurance_management->toArray();
        $this->assertModelData($insuranceManagement->toArray(), $dbinsurance_management);
    }

    /**
     * @test update
     */
    public function test_update_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->create();
        $fakeinsurance_management = factory(insurance_management::class)->make()->toArray();

        $updatedinsurance_management = $this->insuranceManagementRepo->update($fakeinsurance_management, $insuranceManagement->id);

        $this->assertModelData($fakeinsurance_management, $updatedinsurance_management->toArray());
        $dbinsurance_management = $this->insuranceManagementRepo->find($insuranceManagement->id);
        $this->assertModelData($fakeinsurance_management, $dbinsurance_management->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_insurance_management()
    {
        $insuranceManagement = factory(insurance_management::class)->create();

        $resp = $this->insuranceManagementRepo->delete($insuranceManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(insurance_management::find($insuranceManagement->id), 'insurance_management should not exist in DB');
    }
}
