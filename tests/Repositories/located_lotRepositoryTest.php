<?php namespace Tests\Repositories;

use App\Models\located_lot;
use App\Repositories\located_lotRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class located_lotRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var located_lotRepository
     */
    protected $locatedLotRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->locatedLotRepo = \App::make(located_lotRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_located_lot()
    {
        $locatedLot = factory(located_lot::class)->make()->toArray();

        $createdlocated_lot = $this->locatedLotRepo->create($locatedLot);

        $createdlocated_lot = $createdlocated_lot->toArray();
        $this->assertArrayHasKey('id', $createdlocated_lot);
        $this->assertNotNull($createdlocated_lot['id'], 'Created located_lot must have id specified');
        $this->assertNotNull(located_lot::find($createdlocated_lot['id']), 'located_lot with given id must be in DB');
        $this->assertModelData($locatedLot, $createdlocated_lot);
    }

    /**
     * @test read
     */
    public function test_read_located_lot()
    {
        $locatedLot = factory(located_lot::class)->create();

        $dblocated_lot = $this->locatedLotRepo->find($locatedLot->id);

        $dblocated_lot = $dblocated_lot->toArray();
        $this->assertModelData($locatedLot->toArray(), $dblocated_lot);
    }

    /**
     * @test update
     */
    public function test_update_located_lot()
    {
        $locatedLot = factory(located_lot::class)->create();
        $fakelocated_lot = factory(located_lot::class)->make()->toArray();

        $updatedlocated_lot = $this->locatedLotRepo->update($fakelocated_lot, $locatedLot->id);

        $this->assertModelData($fakelocated_lot, $updatedlocated_lot->toArray());
        $dblocated_lot = $this->locatedLotRepo->find($locatedLot->id);
        $this->assertModelData($fakelocated_lot, $dblocated_lot->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_located_lot()
    {
        $locatedLot = factory(located_lot::class)->create();

        $resp = $this->locatedLotRepo->delete($locatedLot->id);

        $this->assertTrue($resp);
        $this->assertNull(located_lot::find($locatedLot->id), 'located_lot should not exist in DB');
    }
}
