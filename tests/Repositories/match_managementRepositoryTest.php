<?php namespace Tests\Repositories;

use App\Models\match_management;
use App\Repositories\match_managementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class match_managementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var match_managementRepository
     */
    protected $matchManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->matchManagementRepo = \App::make(match_managementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_match_management()
    {
        $matchManagement = factory(match_management::class)->make()->toArray();

        $createdmatch_management = $this->matchManagementRepo->create($matchManagement);

        $createdmatch_management = $createdmatch_management->toArray();
        $this->assertArrayHasKey('id', $createdmatch_management);
        $this->assertNotNull($createdmatch_management['id'], 'Created match_management must have id specified');
        $this->assertNotNull(match_management::find($createdmatch_management['id']), 'match_management with given id must be in DB');
        $this->assertModelData($matchManagement, $createdmatch_management);
    }

    /**
     * @test read
     */
    public function test_read_match_management()
    {
        $matchManagement = factory(match_management::class)->create();

        $dbmatch_management = $this->matchManagementRepo->find($matchManagement->id);

        $dbmatch_management = $dbmatch_management->toArray();
        $this->assertModelData($matchManagement->toArray(), $dbmatch_management);
    }

    /**
     * @test update
     */
    public function test_update_match_management()
    {
        $matchManagement = factory(match_management::class)->create();
        $fakematch_management = factory(match_management::class)->make()->toArray();

        $updatedmatch_management = $this->matchManagementRepo->update($fakematch_management, $matchManagement->id);

        $this->assertModelData($fakematch_management, $updatedmatch_management->toArray());
        $dbmatch_management = $this->matchManagementRepo->find($matchManagement->id);
        $this->assertModelData($fakematch_management, $dbmatch_management->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_match_management()
    {
        $matchManagement = factory(match_management::class)->create();

        $resp = $this->matchManagementRepo->delete($matchManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(match_management::find($matchManagement->id), 'match_management should not exist in DB');
    }
}
