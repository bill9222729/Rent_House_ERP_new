<?php namespace Tests\Repositories;

use App\Models\salesman_management;
use App\Repositories\salesman_managementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class salesman_managementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var salesman_managementRepository
     */
    protected $salesmanManagementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->salesmanManagementRepo = \App::make(salesman_managementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->make()->toArray();

        $createdsalesman_management = $this->salesmanManagementRepo->create($salesmanManagement);

        $createdsalesman_management = $createdsalesman_management->toArray();
        $this->assertArrayHasKey('id', $createdsalesman_management);
        $this->assertNotNull($createdsalesman_management['id'], 'Created salesman_management must have id specified');
        $this->assertNotNull(salesman_management::find($createdsalesman_management['id']), 'salesman_management with given id must be in DB');
        $this->assertModelData($salesmanManagement, $createdsalesman_management);
    }

    /**
     * @test read
     */
    public function test_read_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->create();

        $dbsalesman_management = $this->salesmanManagementRepo->find($salesmanManagement->id);

        $dbsalesman_management = $dbsalesman_management->toArray();
        $this->assertModelData($salesmanManagement->toArray(), $dbsalesman_management);
    }

    /**
     * @test update
     */
    public function test_update_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->create();
        $fakesalesman_management = factory(salesman_management::class)->make()->toArray();

        $updatedsalesman_management = $this->salesmanManagementRepo->update($fakesalesman_management, $salesmanManagement->id);

        $this->assertModelData($fakesalesman_management, $updatedsalesman_management->toArray());
        $dbsalesman_management = $this->salesmanManagementRepo->find($salesmanManagement->id);
        $this->assertModelData($fakesalesman_management, $dbsalesman_management->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_salesman_management()
    {
        $salesmanManagement = factory(salesman_management::class)->create();

        $resp = $this->salesmanManagementRepo->delete($salesmanManagement->id);

        $this->assertTrue($resp);
        $this->assertNull(salesman_management::find($salesmanManagement->id), 'salesman_management should not exist in DB');
    }
}
