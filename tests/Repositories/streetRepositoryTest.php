<?php namespace Tests\Repositories;

use App\Models\street;
use App\Repositories\streetRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class streetRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var streetRepository
     */
    protected $streetRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->streetRepo = \App::make(streetRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_street()
    {
        $street = factory(street::class)->make()->toArray();

        $createdstreet = $this->streetRepo->create($street);

        $createdstreet = $createdstreet->toArray();
        $this->assertArrayHasKey('id', $createdstreet);
        $this->assertNotNull($createdstreet['id'], 'Created street must have id specified');
        $this->assertNotNull(street::find($createdstreet['id']), 'street with given id must be in DB');
        $this->assertModelData($street, $createdstreet);
    }

    /**
     * @test read
     */
    public function test_read_street()
    {
        $street = factory(street::class)->create();

        $dbstreet = $this->streetRepo->find($street->id);

        $dbstreet = $dbstreet->toArray();
        $this->assertModelData($street->toArray(), $dbstreet);
    }

    /**
     * @test update
     */
    public function test_update_street()
    {
        $street = factory(street::class)->create();
        $fakestreet = factory(street::class)->make()->toArray();

        $updatedstreet = $this->streetRepo->update($fakestreet, $street->id);

        $this->assertModelData($fakestreet, $updatedstreet->toArray());
        $dbstreet = $this->streetRepo->find($street->id);
        $this->assertModelData($fakestreet, $dbstreet->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_street()
    {
        $street = factory(street::class)->create();

        $resp = $this->streetRepo->delete($street->id);

        $this->assertTrue($resp);
        $this->assertNull(street::find($street->id), 'street should not exist in DB');
    }
}
