<?php namespace Tests\Repositories;

use App\Models\tenant_building_info;
use App\Repositories\tenant_building_infoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class tenant_building_infoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var tenant_building_infoRepository
     */
    protected $tenantBuildingInfoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tenantBuildingInfoRepo = \App::make(tenant_building_infoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->make()->toArray();

        $createdtenant_building_info = $this->tenantBuildingInfoRepo->create($tenantBuildingInfo);

        $createdtenant_building_info = $createdtenant_building_info->toArray();
        $this->assertArrayHasKey('id', $createdtenant_building_info);
        $this->assertNotNull($createdtenant_building_info['id'], 'Created tenant_building_info must have id specified');
        $this->assertNotNull(tenant_building_info::find($createdtenant_building_info['id']), 'tenant_building_info with given id must be in DB');
        $this->assertModelData($tenantBuildingInfo, $createdtenant_building_info);
    }

    /**
     * @test read
     */
    public function test_read_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->create();

        $dbtenant_building_info = $this->tenantBuildingInfoRepo->find($tenantBuildingInfo->id);

        $dbtenant_building_info = $dbtenant_building_info->toArray();
        $this->assertModelData($tenantBuildingInfo->toArray(), $dbtenant_building_info);
    }

    /**
     * @test update
     */
    public function test_update_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->create();
        $faketenant_building_info = factory(tenant_building_info::class)->make()->toArray();

        $updatedtenant_building_info = $this->tenantBuildingInfoRepo->update($faketenant_building_info, $tenantBuildingInfo->id);

        $this->assertModelData($faketenant_building_info, $updatedtenant_building_info->toArray());
        $dbtenant_building_info = $this->tenantBuildingInfoRepo->find($tenantBuildingInfo->id);
        $this->assertModelData($faketenant_building_info, $dbtenant_building_info->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tenant_building_info()
    {
        $tenantBuildingInfo = factory(tenant_building_info::class)->create();

        $resp = $this->tenantBuildingInfoRepo->delete($tenantBuildingInfo->id);

        $this->assertTrue($resp);
        $this->assertNull(tenant_building_info::find($tenantBuildingInfo->id), 'tenant_building_info should not exist in DB');
    }
}
