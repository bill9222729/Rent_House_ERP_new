<?php namespace Tests\Repositories;

use App\Models\tenant_family_info;
use App\Repositories\tenant_family_infoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class tenant_family_infoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var tenant_family_infoRepository
     */
    protected $tenantFamilyInfoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tenantFamilyInfoRepo = \App::make(tenant_family_infoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->make()->toArray();

        $createdtenant_family_info = $this->tenantFamilyInfoRepo->create($tenantFamilyInfo);

        $createdtenant_family_info = $createdtenant_family_info->toArray();
        $this->assertArrayHasKey('id', $createdtenant_family_info);
        $this->assertNotNull($createdtenant_family_info['id'], 'Created tenant_family_info must have id specified');
        $this->assertNotNull(tenant_family_info::find($createdtenant_family_info['id']), 'tenant_family_info with given id must be in DB');
        $this->assertModelData($tenantFamilyInfo, $createdtenant_family_info);
    }

    /**
     * @test read
     */
    public function test_read_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->create();

        $dbtenant_family_info = $this->tenantFamilyInfoRepo->find($tenantFamilyInfo->id);

        $dbtenant_family_info = $dbtenant_family_info->toArray();
        $this->assertModelData($tenantFamilyInfo->toArray(), $dbtenant_family_info);
    }

    /**
     * @test update
     */
    public function test_update_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->create();
        $faketenant_family_info = factory(tenant_family_info::class)->make()->toArray();

        $updatedtenant_family_info = $this->tenantFamilyInfoRepo->update($faketenant_family_info, $tenantFamilyInfo->id);

        $this->assertModelData($faketenant_family_info, $updatedtenant_family_info->toArray());
        $dbtenant_family_info = $this->tenantFamilyInfoRepo->find($tenantFamilyInfo->id);
        $this->assertModelData($faketenant_family_info, $dbtenant_family_info->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tenant_family_info()
    {
        $tenantFamilyInfo = factory(tenant_family_info::class)->create();

        $resp = $this->tenantFamilyInfoRepo->delete($tenantFamilyInfo->id);

        $this->assertTrue($resp);
        $this->assertNull(tenant_family_info::find($tenantFamilyInfo->id), 'tenant_family_info should not exist in DB');
    }
}
