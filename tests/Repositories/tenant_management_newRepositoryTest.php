<?php namespace Tests\Repositories;

use App\Models\tenant_management_new;
use App\Repositories\tenant_management_newRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class tenant_management_newRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var tenant_management_newRepository
     */
    protected $tenantManagementNewRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tenantManagementNewRepo = \App::make(tenant_management_newRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->make()->toArray();

        $createdtenant_management_new = $this->tenantManagementNewRepo->create($tenantManagementNew);

        $createdtenant_management_new = $createdtenant_management_new->toArray();
        $this->assertArrayHasKey('id', $createdtenant_management_new);
        $this->assertNotNull($createdtenant_management_new['id'], 'Created tenant_management_new must have id specified');
        $this->assertNotNull(tenant_management_new::find($createdtenant_management_new['id']), 'tenant_management_new with given id must be in DB');
        $this->assertModelData($tenantManagementNew, $createdtenant_management_new);
    }

    /**
     * @test read
     */
    public function test_read_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->create();

        $dbtenant_management_new = $this->tenantManagementNewRepo->find($tenantManagementNew->id);

        $dbtenant_management_new = $dbtenant_management_new->toArray();
        $this->assertModelData($tenantManagementNew->toArray(), $dbtenant_management_new);
    }

    /**
     * @test update
     */
    public function test_update_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->create();
        $faketenant_management_new = factory(tenant_management_new::class)->make()->toArray();

        $updatedtenant_management_new = $this->tenantManagementNewRepo->update($faketenant_management_new, $tenantManagementNew->id);

        $this->assertModelData($faketenant_management_new, $updatedtenant_management_new->toArray());
        $dbtenant_management_new = $this->tenantManagementNewRepo->find($tenantManagementNew->id);
        $this->assertModelData($faketenant_management_new, $dbtenant_management_new->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tenant_management_new()
    {
        $tenantManagementNew = factory(tenant_management_new::class)->create();

        $resp = $this->tenantManagementNewRepo->delete($tenantManagementNew->id);

        $this->assertTrue($resp);
        $this->assertNull(tenant_management_new::find($tenantManagementNew->id), 'tenant_management_new should not exist in DB');
    }
}
