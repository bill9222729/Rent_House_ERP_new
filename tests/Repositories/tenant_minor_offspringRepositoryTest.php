<?php namespace Tests\Repositories;

use App\Models\tenant_minor_offspring;
use App\Repositories\tenant_minor_offspringRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class tenant_minor_offspringRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var tenant_minor_offspringRepository
     */
    protected $tenantMinorOffspringRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tenantMinorOffspringRepo = \App::make(tenant_minor_offspringRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->make()->toArray();

        $createdtenant_minor_offspring = $this->tenantMinorOffspringRepo->create($tenantMinorOffspring);

        $createdtenant_minor_offspring = $createdtenant_minor_offspring->toArray();
        $this->assertArrayHasKey('id', $createdtenant_minor_offspring);
        $this->assertNotNull($createdtenant_minor_offspring['id'], 'Created tenant_minor_offspring must have id specified');
        $this->assertNotNull(tenant_minor_offspring::find($createdtenant_minor_offspring['id']), 'tenant_minor_offspring with given id must be in DB');
        $this->assertModelData($tenantMinorOffspring, $createdtenant_minor_offspring);
    }

    /**
     * @test read
     */
    public function test_read_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->create();

        $dbtenant_minor_offspring = $this->tenantMinorOffspringRepo->find($tenantMinorOffspring->id);

        $dbtenant_minor_offspring = $dbtenant_minor_offspring->toArray();
        $this->assertModelData($tenantMinorOffspring->toArray(), $dbtenant_minor_offspring);
    }

    /**
     * @test update
     */
    public function test_update_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->create();
        $faketenant_minor_offspring = factory(tenant_minor_offspring::class)->make()->toArray();

        $updatedtenant_minor_offspring = $this->tenantMinorOffspringRepo->update($faketenant_minor_offspring, $tenantMinorOffspring->id);

        $this->assertModelData($faketenant_minor_offspring, $updatedtenant_minor_offspring->toArray());
        $dbtenant_minor_offspring = $this->tenantMinorOffspringRepo->find($tenantMinorOffspring->id);
        $this->assertModelData($faketenant_minor_offspring, $dbtenant_minor_offspring->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tenant_minor_offspring()
    {
        $tenantMinorOffspring = factory(tenant_minor_offspring::class)->create();

        $resp = $this->tenantMinorOffspringRepo->delete($tenantMinorOffspring->id);

        $this->assertTrue($resp);
        $this->assertNull(tenant_minor_offspring::find($tenantMinorOffspring->id), 'tenant_minor_offspring should not exist in DB');
    }
}
